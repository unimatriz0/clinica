/*

    Nombre: FormDependencias
    Fecha: 27/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario de abm de dependencias

 */

// declaración del paquete
package dependencias;

// importación de librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

public class FormDependencias extends JDialog {

	// definimos el serial id y declaramos las variables
	private static final long serialVersionUID = -7531404145638665924L;
	private JTextField tId;
	private JTextField tDependencia;
	private JTextField tDescripcion;
	private JTable tDependencias;
	private Dependencias Financiamiento;

	// creamos el diálogo
	public FormDependencias(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// cerramos el formulario al cancelar
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		// establecemos las propiedades
		this.setTitle("Dependencias Institucionales");
		this.setBounds(100, 100, 550, 350);
		this.getContentPane().setLayout(null);

		// instanciamos la clase
		this.Financiamiento = new Dependencias();

		// inicializamos los componentes
		this.initFormDependencias();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa los componentes del formulario
	 */
	@SuppressWarnings("serial")
	protected void initFormDependencias(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Dependencias Registradas");
		lTitulo.setBounds(10, 10, 446, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// pide la clave de la dependencia
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 44, 26);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setFont(Fuente);
		getContentPane().add(this.tId);

		// pide el nombre de la dependencia
		this.tDependencia = new JTextField();
		this.tDependencia.setBounds(66, 45, 300, 26);
		this.tDependencia.setToolTipText("Nombre de la Dependencia");
		this.tDependencia.setFont(Fuente);
		getContentPane().add(this.tDependencia);

		// pide la descripción abreviada
		this.tDescripcion = new JTextField();
		this.tDescripcion.setBounds(380, 45, 120, 26);
		this.tDescripcion.setToolTipText("Abreviatura de la Dependencia");
		this.tDescripcion.setFont(Fuente);
		getContentPane().add(this.tDescripcion);

		// agregamos el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setToolTipText("Graba el registro en la base");
		btnGrabar.setBounds(510, 45, 26, 26);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaDependencia();
			}
		});
		getContentPane().add(btnGrabar);

		// define la tabla
		this.tDependencias = new JTable();
		this.tDependencias.setModel(new DefaultTableModel(new Object[][] { { null,
																null,
																null,
																null,
																null }, },
				new String[] { "ID",
							   "Nombre",
							   "Abr.",
							   "Ed.",
							   "El." }) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] { Integer.class,
												String.class,
												String.class,
												Object.class,
												Object.class };

			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// establecemos el ancho de las columnas
		this.tDependencias.getColumn("ID").setPreferredWidth(30);
		this.tDependencias.getColumn("ID").setMaxWidth(30);
		this.tDependencias.getColumn("Abr.").setPreferredWidth(100);
		this.tDependencias.getColumn("Abr.").setMaxWidth(100);
		this.tDependencias.getColumn("Ed.").setPreferredWidth(35);
		this.tDependencias.getColumn("Ed.").setMaxWidth(35);
		this.tDependencias.getColumn("El.").setPreferredWidth(35);
		this.tDependencias.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tDependencias.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tDependencias.setRowHeight(25);

		// fijamos el evento click
		this.tDependencias.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tDependenciasMouseClicked(evt);
			}
		});

		// fijamos la fuente
		this.tDependencias.setFont(Fuente);

		// agregamos la tabla al scroll
		JScrollPane scrollDependencias = new JScrollPane();
		scrollDependencias.setBounds(10, 80, 525, 225);
		scrollDependencias.setViewportView(this.tDependencias);
		getContentPane().add(scrollDependencias);

		// cargamos la grilla de dependencias
		this.cargaDependencias();

		// fijamos el foco
		this.tDescripcion.requestFocus();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla de dependencias
	 */
	protected void cargaDependencias(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Financiamiento.nominaDependencias();

		// sobrecargamos el renderer de la tabla
		this.tDependencias.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tDependencias.getModel();

		// hacemos la tabla se pueda ordenar
		tDependencias.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[5];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("dependencia");
				fila[2] = Nomina.getString("descripcion");
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

			// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de dependencias
	 */
	protected void tDependenciasMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tDependencias.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tDependencias.rowAtPoint(evt.getPoint());
		int columna = this.tDependencias.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tDependencias.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 3) {

				// cargamos el registro
				this.getDependencia(clave);

				// si pulsó en eliminar
			} else if (columna == 4) {

				// eliminamos
				this.borraDependencia(clave);

			}

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void grabaDependencia(){

		// verifica se halla ingresado la dependencia
		if (this.tDependencia.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre de la dependencia", "Error",
					JOptionPane.ERROR_MESSAGE);
			this.tDependencia.requestFocus();
			return;

		// si esta correcto
		} else {

			// asigna en la clase
			this.Financiamiento.setDependencia(this.tDependencia.getText());

		}

		// si está insertando
		if (this.tId.getText().isEmpty()){

			// verifica que no esté repetido
			if (!this.Financiamiento.validaDependencia(this.tDependencia.getText())){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Esa dependencia ya está declarada", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;

			// si está insertando
			} else {

				// lo marca como una inserción
				this.Financiamiento.setIdDependencia(0);

			}

		// si está editando
		} else {

			// asigna en la clase
			this.Financiamiento.setIdDependencia(Integer.parseInt(this.tId.getText()));

		}

		// verifica se halla ingresado la abreviatura
		if (this.tDescripcion.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese la abreviatura", "Error",
					JOptionPane.ERROR_MESSAGE);
			this.tDescripcion.requestFocus();
			return;

		// si declaró
		} else {

			// asigna en la clase
			this.Financiamiento.setDescripcion(this.tDescripcion.getText());

		}

		// graba el registro
		this.Financiamiento.grabaDependencia();

		// limpia el formulario
		this.limpiaFormulario();

		// recarga la grilla
		this.cargaDependencias();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre la edición de una
	 * dependencia en la grilla
	 */
	protected void getDependencia(int clave){

		// obtenemos el registro
		this.Financiamiento.getDatosDependencia(clave);

		// asignamos los valores en el formulario
		this.tId.setText(Integer.toString(this.Financiamiento.getIdDependencia()));
		this.tDependencia.setText(this.Financiamiento.getDependencia());
		this.tDescripcion.setText(this.Financiamiento.getDescripcion());

		// fijamos el foco
		this.tDependencia.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario luego de grabar
	 */
	protected void limpiaFormulario(){

		// limpiamos los valores
		this.tId.setText("");
		this.tDependencia.setText("");
		this.tDescripcion.setText("");

		// fijamos el foco
		this.tDependencia.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre la eliminación de una
	 * dependencia en la grilla, verifica si puede eliminar
	 * y luego pide confirmación
	 */
	protected void borraDependencia(int clave){

		// verifica si puede eliminar
		if (!this.Financiamiento.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa dependencia tiene registros asignados", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?", "Dependencias", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Financiamiento.borraDependencia(clave);

			// limpiamos el formulario por las dudas
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaDependencias();

		}

	}

}
