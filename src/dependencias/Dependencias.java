/*

    Nombre: dependencias
    Fecha: 18/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: métodos que controlan el abm y listados de la tipos de
                 documentos

 */

// declaración del paquete
package dependencias;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Declaración de la clase
 * 
 */
public class Dependencias {

    // declaración de variables
    protected int IdDependencia;            // clave del registro
    protected String Dependencia;           // nombre de la dependencia
    protected String Descripcion;           // descripción abreviada
    protected String Usuario;               // nombre del usuario
    protected String FechaAlta;             // fecha de alta del registro
    protected int IdUsuario;                // clave del usuario
    
    // el puntero a la base
    protected Conexion Enlace;
    
    // constructor de la clase
    public Dependencias(){
    
        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initDependencias();
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initDependencias(){
    	
        // inicializamos las variables
        this.IdDependencia = 0;
        this.Dependencia = "";
        this.Descripcion = "";
        this.Usuario = "";
        this.FechaAlta = "";
        this.IdUsuario = Seguridad.Id;
    	
    }
    
    // métodos de asignación de valores
    public void setIdDependencia(int iddependencia){
        this.IdDependencia = iddependencia;
    }
    public void setDependencia(String dependencia){
        this.Dependencia = dependencia;
    }
    public void setDescripcion(String descripcion){
        this.Descripcion = descripcion;
    }
    
    // métodos de retorno de valores
    public int getIdDependencia(){
        return this.IdDependencia;
    }
    public String getDependencia(){
        return this.Dependencia;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getDescripcion(){
        return this.Descripcion;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return la nómina de dependencias
     * Método que retorna la nómina de dependencias de las instituciones
     */
    public ResultSet nominaDependencias(){
        
        // declaración de variables
        String Consulta;
        ResultSet listaDependencias;
        
        // componemos la consulta
        Consulta = "SELECT diccionarios.dependencias.id_dependencia AS id, "
                 + "       diccionarios.dependencias.des_abr As descripcion, "
                 + "diccionarios.dependencias.dependencia AS dependencia "
                 + "FROM diccionarios.dependencias;";
        
        // obtenemos el vector
        listaDependencias = this.Enlace.Consultar(Consulta);
        
        // retornamos el vector
        return listaDependencias;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param dependencia una cadena con la dependencia
     * @return entero con la clave de la dependencia
     */
    public int getClaveDependencia(String dependencia){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int claveDependencia = 0;
        
        // componemos la consulta
        Consulta = "SELECT diccionarios.dependencias.ID AS id_dependencia "
                + " FROM diccionarios.dependencias "
                + " WHERE diccionarios.dependencias.DEPENDENCIA = '" + dependencia + "';";
        
        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            claveDependencia = Resultado.getInt("id_dependencia");
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos la clave
        return claveDependencia;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return IdDependencia - clave del registro modificado
     * Método que ejecuta la consulta de insercion o actualización
     * según corresponda
     */
    public int grabaDependencia(){
        
        // si está editando
        if (this.IdDependencia == 0){
            this.nuevaDependencia();
        } else {
            this.editaDependencia();
        }
        
        // retorna la id del registro
        return this.IdDependencia;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaDependencia(){
        
        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "INSERT INTO diccionarios.dependencias "
                 + "       (dependencia, "
                 + "        des_abr, "
                 + "        id_usuario) "
                 + "       VALUES "
                 + "       (?,?,?); ";

        try {
            
            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);
            
            // asignamos los valores
            preparedStmt.setString (1, this.Dependencia);
            preparedStmt.setString (2, this.Descripcion);
            preparedStmt.setInt    (3, this.IdUsuario);
             
            // ejecutamos la consulta
            preparedStmt.execute();
            
            // obtenemos la id
            this.IdDependencia = this.Enlace.UltimoInsertado();
            
        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaDependencia(){
        
        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "UPDATE diccionarios.dependencias SET "
                 + "       dependencia = ?, "
                 + "       des_abr = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diccionarios.dependencias.id_dependencia = ?;";

        try {
            
            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);
            
            // asignamos los valores
            preparedStmt.setString (1, this.Dependencia);
            preparedStmt.setString (2, this.Descripcion);
            preparedStmt.setInt    (3, this.IdUsuario);
            preparedStmt.setInt    (4, this.IdDependencia);
            
            // ejecutamos la consulta
            preparedStmt.execute();
                                            
        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param dependencia - nombre de la dependencia
     * @return Correcto - estado de la verificación
     * Método que recibe un nombre de dependencia y verifica 
     * si se puede insertar, en cuyo caso retorna verdadero
     */
    public boolean validaDependencia(String dependencia){
    
        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.dependencias.id_dependencia) AS registros "
                 + "FROM diccionarios.dependencias "
                 + "WHERE diccionarios.dependencias.dependencia = '" + dependencia + "'; "; 

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos el estado
        return Correcto;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de una dependencia 
     * y asigna en las variables de clase los valores del registro
     */
    public void getDatosDependencia(int clave){

        // declaración de variables 
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.dependencias.id_dependencia AS id, " +
                   "       diccionarios.dependencias.dependencia AS dependencia, " +
                   "       diccionarios.dependencias.des_abr AS descripcion, " +
                   "       diccionarios.dependencias.id_usuario AS id_usuario, " +
                   "       DATE_FORMAT(diccionarios.dependencias.fecha, '%d/%d/%Y') AS fecha_alta, " +
                   "       cce.responsables.usuario AS usuario " + 
                   "FROM diccionarios.dependencias INNER JOIN cce.responsables ON diccionarios.dependencias.id_usuario = cce.responsables.id " +
                   "WHERE diccionarios.dependencias.id_dependencia = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdDependencia = Resultado.getInt("id");
            this.Dependencia = Resultado.getString("dependencia");
            this.Descripcion = Resultado.getString("descripcion");
            this.IdUsuario = Resultado.getInt("id_usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");
            this.Usuario = Resultado.getString("usuario");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave de una dependencia
     * y retorna si esta tiene hijos (en instituciones y laboratorios)
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(cce.laboratorios.id) AS laboratorios, " +
                   "       COUNT(diagnostico.centros_asistenciales.id) AS centros " +
                   "FROM cce.laboratorios, " +
                   "     diagnostico.centros_asistenciales " +
                   "WHERE cce.laboratorios.id = '" + clave + "' OR " +
                   "      diagnostico.centros_asistenciales.id = '" + clave + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si no hay registros
            if (Resultado.getInt("laboratorios") == 0 && Resultado.getInt("centros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave de una dependencia
     * y elimina el registro
     */
    public void borraDependencia(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diccionarios.dependencias " +
                   "WHERE diccionarios.dependencias.id_dependencia = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}
