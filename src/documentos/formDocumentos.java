/*

    Nombre: formDocumentos
    Fecha: 28/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario de tipos de documento

 */

 // definición del paquete
package documentos;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class formDocumentos extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

	// definimos las variables
    protected JTable tDocumentos;
    private JTextField tId;
    private JTextField tDocumento;
    private JTextField tDescripcion;
    private Documentos Credenciales;

    // creamos el diálogo
    public formDocumentos(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

        // fijamos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setBounds(100, 100, 550, 370);
        this.getContentPane().setLayout(null);

        // instanciamos la clase
        this.Credenciales = new Documentos();

        // inicializamos el formulario
        this.initFormDocumentos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa los componentes del formulario
     */
    @SuppressWarnings("serial")
    protected void initFormDocumentos(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // el título del formulario
        JLabel lTitulo = new JLabel("Tipos de Documento");
        lTitulo.setBounds(10, 10, 458, 26);
        lTitulo.setFont(Fuente);
        getContentPane().add(lTitulo);

        // la clave del registro
        this.tId = new JTextField();
        this.tId.setBounds(10, 45, 56, 26);
        this.tId.setToolTipText("Clave del registro");
        this.tId.setEditable(false);
        this.tId.setFont(Fuente);
        getContentPane().add(this.tId);

        // el nombre largo del documento
        this.tDocumento = new JTextField();
        this.tDocumento.setBounds(76, 45, 269, 26);
        this.tDocumento.setToolTipText("Nombre completo del documento");
        this.tDocumento.setFont(Fuente);
        getContentPane().add(this.tDocumento);

        // la descripción abreviada
        this.tDescripcion = new JTextField();
        this.tDescripcion.setBounds(367, 45, 114, 26);
        this.tDescripcion.setToolTipText("Abreviatura o Sigla");
        this.tDescripcion.setFont(Fuente);
        getContentPane().add(this.tDescripcion);

        // presenta el botón grabar
        JButton btnGrabar = new JButton();
        btnGrabar.setToolTipText("Graba el registro en la base");
        btnGrabar.setBounds(500, 45, 26, 26);
        btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaDocumento();
            }
        });
        getContentPane().add(btnGrabar);

        // la grilla de documentos
        this.tDocumentos = new JTable();
        this.tDocumentos.setModel(new DefaultTableModel
                    (new Object[][] {
                                 { null,
                                   null,
                                   null,
                                   null,
                                   null }, },
                new String[] { "ID",
                               "Tipo Documento",
                               "Abr.",
                               "Ed.",
                               "El" }) {
                @SuppressWarnings("rawtypes")
                Class[] columnTypes = new Class[] { Integer.class,
                                                String.class,
                                                String.class,
                                                Object.class,
                                                Object.class };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }
        });

        // establecemos el ancho de las columnas
        this.tDocumentos.getColumn("ID").setPreferredWidth(30);
        this.tDocumentos.getColumn("ID").setMaxWidth(30);
        this.tDocumentos.getColumn("Abr.").setPreferredWidth(100);
        this.tDocumentos.getColumn("Abr.").setMaxWidth(100);
        this.tDocumentos.getColumn("Ed.").setPreferredWidth(35);
        this.tDocumentos.getColumn("Ed.").setMaxWidth(35);
        this.tDocumentos.getColumn("El").setPreferredWidth(35);
        this.tDocumentos.getColumn("El").setMaxWidth(35);

        // establecemos el tooltip
        this.tDocumentos.setToolTipText("Pulse para editar / borrar");

        // fijamos el alto de las filas
        this.tDocumentos.setRowHeight(25);

        // fijamos el evento click
        this.tDocumentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tDocumentosMouseClicked(evt);
            }
        });

        // fijamos la fuente
        this.tDocumentos.setFont(Fuente);

        // agregamos la tabla al scroll
        JScrollPane scrollDocumentos = new JScrollPane();
        scrollDocumentos.setBounds(10, 80, 525, 250);
        scrollDocumentos.setViewportView(this.tDocumentos);
        getContentPane().add(scrollDocumentos);

        // cargamos la grilla
        this.cargaDocumentos();

        // mostramos el formulario
        this.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que carga la grilla con la nòmina de documentos
     */
    protected void cargaDocumentos(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Credenciales.nominaDocumentos();

		// sobrecargamos el renderer de la tabla
		this.tDocumentos.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tDocumentos.getModel();

		// hacemos la tabla se pueda ordenar
		tDocumentos.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[5];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("tipo_documento");
				fila[2] = Nomina.getString("descripcion");
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

			// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que limpia el formulario de documentos
     */
    protected void limpiaFormulario(){

    	// limpiamos los campos
    	this.tId.setText("");
    	this.tDocumento.setText("");
    	this.tDescripcion.setText("");

    	// fijamos el foco
    	this.tDocumento.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la grilla
     */
    protected void tDocumentosMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tDocumentos.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tDocumentos.rowAtPoint(evt.getPoint());
		int columna = this.tDocumentos.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tDocumentos.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 3) {

				// cargamos el registro
				this.getDocumento(clave);

				// si pulsó en eliminar
			} else if (columna == 4) {

				// eliminamos
				this.borraDocumento(clave);

			}

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar
     */
    protected void grabaDocumento(){

    	// verifica se halla ingresado el tipo de documento
    	if (this.tDocumento.getText().isEmpty()){

    		// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el tipo de documento", "Error", JOptionPane.ERROR_MESSAGE);
			this.tDocumento.requestFocus();
			return;

		// si asignó
    	} else {

    		// asigna en la variable de clase
    		this.Credenciales.setTipoDocumento(this.tDocumento.getText());

    	}

    	// si está insertando
    	if (this.tId.getText().isEmpty()){

    		// verificamos que sea vàlido
    		if(!this.Credenciales.validaDocumento(this.tDocumento.getText())){

    			// presenta el mensaje
    			JOptionPane.showMessageDialog(this, "Ese documento ya está declarado", "Error", JOptionPane.ERROR_MESSAGE);
    			return;

            // si puede insertar
    		} else {

                // lo marca como una inserción
                this.Credenciales.setIdDocumento(0);

            }

    	// si està editando
    	} else {

    		// asigna la id en la clase
    		this.Credenciales.setIdDocumento(Integer.parseInt(this.tId.getText()));

    	}

    	// verifica se halla ingresado la descripciòn
    	if (this.tDescripcion.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre abreviado", "Error", JOptionPane.ERROR_MESSAGE);
			this.tDescripcion.requestFocus();
			return;

		// si asignò
    	} else {

    		// asigna en la variable de clase
    		this.Credenciales.setDescripcion(this.tDescripcion.getText());

    	}

    	// graba el registro
    	this.Credenciales.grabaDocumento();

    	// limpia el formulario
    	this.limpiaFormulario();

    	// recarga la grilla
    	this.cargaDocumentos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Mètodo que obtiene los datos del registro y los
     * presenta en el formulario
     */
    protected void getDocumento(int clave){

    	// obtenemos los datos del registro
    	this.Credenciales.getDatosDocumento(clave);

    	// asignamos en el formulario
    	this.tId.setText(Integer.toString(this.Credenciales.getIdDocumento()));
    	this.tDocumento.setText(this.Credenciales.getTipoDocumento());
    	this.tDescripcion.setText(this.Credenciales.getDescripcion());

    	// fijamos el foco
    	this.tDocumento.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que que pide confirmación antes de ejecutar
     * la consulta de eliminación
     */
    protected void borraDocumento(int clave){

		// verifica si puede eliminar
		if (!this.Credenciales.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ese documento está asignado a pacientes", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                    "Está seguro que desea eliminar el registro?",
                                    "Documentos",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Credenciales.borraDocumento(clave);

            // limpiamos el formulario
            this.limpiaFormulario();

			// recargamos la grilla
			this.cargaDocumentos();

		}

    }

}
