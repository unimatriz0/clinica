/*

    Nombre: documentos
    Fecha: 18/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para operar sobre la
                 tabla de tipos de documentos

*/

// declaración del paquete
package documentos;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Constructor de la clase
 */
public class Documentos {

    // declaramos las variables de clase
    protected int IdDocumento;              // clave del registro
    protected String TipoDocumento;         // tipo de documento
    protected String Descripcion;           // abreviatura del tipo
    protected String Usuario;               // nombre del usuario
    protected String FechaAlta;             // fecha de alta del registro

    // el puntero a la base
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Documentos(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initDocumento();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initDocumento(){

        // inicializamos las variables
        this.IdDocumento = 0;
        this.TipoDocumento = "";
        this.Descripcion = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setIdDocumento(int iddocumento){
        this.IdDocumento = iddocumento;
    }
    public void setTipoDocumento(String tipodocumento){
        this.TipoDocumento = tipodocumento;
    }
    public void setDescripcion(String descripcion){
        this.Descripcion = descripcion;
    }

    // métodos de retorno de valores
    public int getIdDocumento(){
        return this.IdDocumento;
    }
    public String getTipoDocumento(){
        return this.TipoDocumento;
    }
    public String getDescripcion(){
        return this.Descripcion;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param documento string con un tipo de documento
     * @return entero con la clave del documento
     */
    public int getClaveDocumento(String documento){

     // declaración de variables
     int claveDocumento = 0;
     String Consulta;
     ResultSet Resultado;

     // componemos y ejecutamos la consulta
     Consulta = "SELECT diccionarios.tipo_documento.id_documento AS clave " +
                "FROM diccionarios.tipo_documento " +
                "WHERE diccionarios.tipo_documento.des_abreviada = '" + documento + "'; ";
     Resultado = this.Enlace.Consultar(Consulta);

     try {

         // obtenemos el registro
         if (Resultado.next()){
             claveDocumento = Resultado.getInt("clave");
         }

     // si hubo un error
     } catch (SQLException e) {
         e.printStackTrace();
     }

     // retornamos la clave
     return claveDocumento;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los tipos de documento abreviados
     * Método que retorna un vector con los tipos de documento
     */
    public ResultSet nominaDocumentos(){

        // declaración de variables
        String Consulta;
        ResultSet tiposDocumentos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diccionarios.tipo_documento.id_documento AS id, " +
                   "       diccionarios.tipo_documento.tipo_documento AS tipo_documento, " +
                   "       diccionarios.tipo_documento.des_abreviada AS descripcion " +
                   "FROM diccionarios.tipo_documento " +
                   "ORDER BY diccionarios.tipo_documento.des_abreviada;";
        tiposDocumentos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return tiposDocumentos;

    }

    /**
     * @param clave entero con la clave de un documento
     * @return cadena con la denominación del documento
     */
    public String nombreDocumento(int clave){

     // declaración de variables
     String Consulta;
     ResultSet Resultado;
     String tipoDocumento = "";

     // componemos y ejecutamos la consulta
     Consulta = "SELECT diccionarios.tipo_documento.des_abreviada AS documento " +
                "FROM diccionarios.tipo_documento " +
                "WHERE diccionarios.tipo_documento.id_documento = '" + clave + "'; ";
     Resultado = this.Enlace.Consultar(Consulta);

     try {

         // obtenemos el registro
         if (Resultado.next()){
             tipoDocumento = Resultado.getString("documento");
         }

     // si hubo un error
     } catch (SQLException e) {
         e.printStackTrace();
     }

     // retornamos el tipo de documento
     return tipoDocumento;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return IdDocumento - clave del registro afectado
     * Método que ejecuta la consulta de actualización o inserción
     * según corresponda
     */
    public int grabaDocumento(){

        // si está insertando
        if (this.IdDocumento == 0){
            this.nuevoDocumento();
        } else {
            this.editaDocumento();
        }

        // retornamos
        return this.IdDocumento;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoDocumento(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "INSERT INTO diccionarios.tipo_documento "
                 + "       (tipo_documento, "
                 + "        des_abreviada, "
                 + "        id_usuario) "
                 + "       VALUES "
                 + "       (?,?,?); ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.TipoDocumento);
            preparedStmt.setString (2, this.Descripcion);
            preparedStmt.setInt    (3, Seguridad.Id);

            // ejecutamos la consulta
            preparedStmt.execute();

            // obtenemos la id
            this.IdDocumento = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaDocumento(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de edición
        Consulta = "UPDATE diccionarios.tipo_documento SET "
                 + "       tipo_documento = ?, "
                 + "       des_abreviada = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diccionarios.tipo_documento.id_documento = ?; ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString (1, this.TipoDocumento);
            preparedStmt.setString (2, this.Descripcion);
            preparedStmt.setInt    (3, Seguridad.Id);
            preparedStmt.setInt    (4, this.IdDocumento);

            // ejecutamos la consulta
            preparedStmt.execute();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tipodocumento - tipo de documento a verificar
     * @return Correcto si puede insertar el registro
     * Método que recibe como parámetro la descripción del
     * tipo de documento y verifica que no se encuentre
     * repetido, si puede insertar retorna verdadero
     */
    public boolean validaDocumento(String tipodocumento){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.tipo_documento.id_documento) AS registros "
                 + "FROM diccionarios.tipo_documento "
                 + "WHERE diccionarios.tipo_documento.tipo_documento = '" + tipodocumento + "' OR "
                 + "      diccionarios.tipo_documento.des_abreviada = '" + tipodocumento + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del documento
     * @return correcto - si puede borrar
     * Método que recibe como parámetro la clave de un documento y
     * verifica si no está asignado a un paciente
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros "
                 + "FROM diagnostico.personas "
                 + "WHERE diagnostico.personas.tipo_documento = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * ejecuta la consulta de eliminación
     */
    public void borraDocumento(int clave){

        // declaración de variables
        String Consulta;

        // componemos la consulta y la ejecutamos
        Consulta = "DELETE FROM diccionarios.tipo_documento " +
                   "WHERE diccionarios.tipo_documento.id_documento = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * asigna los valores a las variables de clase
     */
    public void getDatosDocumento(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.tipo_documento.id_documento AS id_documento, " +
                   "       diccionarios.tipo_documento.tipo_documento AS tipo_documento, " +
                   "       diccionarios.tipo_documento.des_abreviada AS descripcion, " +
                   "       cce.responsables.usuario AS usuario, " +
                   "       DATE_FORMAT(diccionarios.tipo_documento.fecha, '%d/%m/%Y') AS fecha_alta " +
                   "FROM diccionarios.tipo_documento INNER JOIN cce.responsables ON diccionarios.tipo_documento.id_usuario = cce.responsables.id " +
                   "WHERE diccionarios.tipo_documento.id_documento = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro y lo asignamos
            Resultado.next();
            this.IdDocumento = Resultado.getInt("id_documento");
            this.TipoDocumento = Resultado.getString("tipo_documento");
            this.Descripcion = Resultado.getString("descripcion");
            this.Usuario = Resultado.getString("Usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

}
