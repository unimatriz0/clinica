/*

    Nombre: FormTransplantes
    Fecha: 21/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el abm de
	             transplantes recibidos

*/

// definición del paquete
package transplantes;

// importamos las librerías
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import com.toedter.calendar.JDateChooser;
import java.awt.Font;
import funciones.ComboClave;
import funciones.RendererTabla;
import organos.Organos;
import seguridad.Seguridad;
import transplantes.Transplantes;
import funciones.Utilidades;

// definición de la clase
public class FormTransplantes extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -315485615814103181L;

	// definición de las varibles de clase
	private JTextField tId;                        // clave del registro
	private JDateChooser dFecha;                   // fecha del transplante
	private JComboBox<Object> cOrgano;             // órgano transplantado
	private JComboBox<Object> cPositivo;           // órgano positivo para chagas
	private JTextField tUsuario;                   // nombre del usuario
	private JTable tTransplantes;                  // tabla con los transplantes
	public int Protocolo;                          // protocolo del paciente
	private Transplantes Cirugias;                 // clase de la base de datos
	private Utilidades Herramientas;               // funciones de fecha

	// constructor de la clase
	@SuppressWarnings("serial")
	public FormTransplantes(Frame parent, boolean modal) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// inicializamos las variables
		this.Protocolo = 0;
		this.Cirugias = new Transplantes();
		this.Herramientas = new Utilidades();

		// fijamos las propiedades
		this.setBounds(150, 150, 680, 300);
		this.setLayout(null);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Organos Transplantados");

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Transplantes del paciente</b></html>");
		lTitulo.setBounds(12, 12, 199, 17);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presentamos la clave
		this.tId = new JTextField();
		this.tId.setBounds(10, 40, 70, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);
		this.add(this.tId);

		// presenta el select del órgano
		this.cOrgano = new JComboBox<>();
		this.cOrgano.setBounds(95, 40, 220, 26);
		this.cOrgano.setFont(Fuente);
		this.cOrgano.setToolTipText("Seleccione el órgano de la lista");
		this.add(this.cOrgano);

		// cargamos los órganos
		this.elementosOrganos();

		// pide si el órgano es positivo
		this.cPositivo = new JComboBox<>();
		this.cPositivo.setBounds(325, 40, 65, 26);
		this.cPositivo.setFont(Fuente);
		this.cPositivo.setToolTipText("Indique si el órgano era positivo para Chagas");
		this.add(this.cPositivo);

		// cargamos los elementos del combo
		this.elementosPositivo();

		// pide la fecha del transplante
		this.dFecha= new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(415, 40, 110, 26);
		this.dFecha.setFont(Fuente);
		this.dFecha.setToolTipText("Indique la fecha del transplante");
		this.add(this.dFecha);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(530, 40, 90, 26);
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.add(this.tUsuario);

		// fijamos el usuario actual
		this.tUsuario.setText(Seguridad.Usuario);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(630, 40, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaTransplante();
            }
        });
		this.add(btnGrabar);

		// define el scroll
		JScrollPane scrollTransplantes = new JScrollPane();
		scrollTransplantes.setBounds(10, 70, 655, 180);
		getContentPane().add(scrollTransplantes);

		// define la tabla
		this.tTransplantes = new JTable();
		this.tTransplantes.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null},
			},
			new String[] {
				"Id",
				"Organo",
				"Pos.",
				"Fecha",
				"Usuario",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

        // establecemos el ancho de las columnas
        this.tTransplantes.getColumn("Id").setPreferredWidth(30);
        this.tTransplantes.getColumn("Id").setMaxWidth(30);
        this.tTransplantes.getColumn("Pos.").setPreferredWidth(40);
        this.tTransplantes.getColumn("Pos.").setMaxWidth(40);
		this.tTransplantes.getColumn("Fecha").setPreferredWidth(100);
        this.tTransplantes.getColumn("Fecha").setMaxWidth(100);
        this.tTransplantes.getColumn("Usuario").setPreferredWidth(100);
        this.tTransplantes.getColumn("Usuario").setMaxWidth(100);
		this.tTransplantes.getColumn("Ed.").setPreferredWidth(35);
        this.tTransplantes.getColumn("Ed.").setMaxWidth(35);
        this.tTransplantes.getColumn("El.").setPreferredWidth(35);
        this.tTransplantes.getColumn("El.").setMaxWidth(35);

        // establecemos el tooltip
        this.tTransplantes.setToolTipText("Pulse para editar / borrar");

        // fijamos el alto de las filas
        this.tTransplantes.setRowHeight(25);

		// fijamos la fuente
		this.tTransplantes.setFont(Fuente);

		// agrega la tabla al scroll
		scrollTransplantes.setViewportView(this.tTransplantes);

		// agregamos el evento de la tabla
		this.tTransplantes.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tTransplantesMouseClicked(evt);
			}
		});

	}

	// método que carga los elementos del combo de positivo
	private void elementosPositivo(){

		// agregamos los elementos
		this.cPositivo.addItem("");
		this.cPositivo.addItem("Si");
		this.cPositivo.addItem("No");

	}

	// método que carga los elementos del combo de órganos
	private void elementosOrganos(){

		// instanciamos la clase y obtenemos la nómina
		Organos Transplantes = new Organos();
		ResultSet nomina = Transplantes.nominaOrganos();

		// agregamos el primer elemento
		this.cOrgano.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cOrgano.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("organo")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método público que carga los transplantes del paciente
	 */
	public void cargaTransplantes(){

		// obtenemos la nómina
		ResultSet Nomina = this.Cirugias.nominaTransplantes(this.Protocolo);

		// sobrecargamos el renderer de la tabla
		this.tTransplantes.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tTransplantes.getModel();

		// hacemos la tabla se pueda ordenar
		this.tTransplantes.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[7];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("organo");
				fila[2] = Nomina.getString("positivo");
				fila[3] = Nomina.getString("fecha");
				fila[4] = Nomina.getString("usuario");
				fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que verifica los datos del formulario antes
	 * de enviarlo al servidor
	 */
	private void validaTransplante(){

		// si no seleccionó el órgano
		ComboClave item = (ComboClave) this.cOrgano.getSelectedItem();
		if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione de la lista Organo Transplantado",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cOrgano.requestFocus();
			return;

		}

		// si no indicó si era positivo
		if (this.cPositivo.getSelectedItem().toString().equals("")){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
						"Indique si el órgano era\n" +
						"positivo para Chagas",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cPositivo.requestFocus();
			return;

		}

		// si no ingresó la fecha
		if (this.Herramientas.fechaJDate(this.dFecha) == null){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la fecha del transplante",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.dFecha.requestFocus();
			return;

		}

		// grabamos el registro
		this.grabaTransplante();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que ejecuta la consulta de grabación en el
	 * servidor
	 */
	private void grabaTransplante(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Cirugias.setId(0);
		} else {
			this.Cirugias.setId(Integer.parseInt(this.tId.getText()));
		}

		// asignamos el protocolo
		this.Cirugias.setProtocolo(this.Protocolo);

		// obtenemos la clave del órgano
		ComboClave item = (ComboClave) this.cOrgano.getSelectedItem();
		this.Cirugias.setIdOrgano(item.getClave());

		// asignamos si es o no positivo
		this.Cirugias.setPositivo(this.cPositivo.getSelectedItem().toString());

		// asignamos la fecha
		this.Cirugias.setFechaTransplante(this.Herramientas.fechaJDate(this.dFecha));

		// grabamos el registro
		this.Cirugias.grabaTransplante();

		// limpiamos el formulario
		this.limpiaFormulario();

		// recargamos la grilla
		this.cargaTransplantes();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario luego de una
	 * eliminación o grabación
	 */
	private void limpiaFormulario(){

		// limpiamos los campos
		this.tId.setText("");
		ComboClave item = new ComboClave (0, "");
		this.cOrgano.setSelectedItem(item);
		this.cPositivo.setSelectedItem("");
		this.dFecha.setDate(null);
		this.tUsuario.setText(Seguridad.Usuario);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que recibe como parámetro la clave de un
	 * registro y lo presenta en el formulario
	 */
	private void verTransplante(int clave){

		// obtenemos el registro
		this.Cirugias.getDatosTransplante(clave);

		// asignamos en el formulario
		this.tId.setText(Integer.toString(this.Cirugias.getId()));

		// seleccionamos el órgano
		ComboClave item = new ComboClave(this.Cirugias.getIdOrgano(), this.Cirugias.getOrgano());
		this.cOrgano.setSelectedItem(item);

		// asignamos si era positivo
		this.cPositivo.setSelectedItem(this.Cirugias.getPositivo());

		// fijamos la fecha
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Cirugias.getFechaTransplante()));

		// finalmente el usuario
		this.tUsuario.setText(this.Cirugias.getUsuario());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que recibe como parámetro la clave de un
	 * registro y luego de pedir confirmación ejecuta la
	 * consulta de eliminación
	 */
	private void borraTransplante(int clave){

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
		                            "Está seguro que desea eliminar el registro?",
									"Transplantes del Paciente",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Cirugias.borraTransplante(clave);

			// limpiamos el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaTransplantes();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de transplantes
	 */
	protected void tTransplantesMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tTransplantes.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tTransplantes.rowAtPoint(evt.getPoint());
        int columna = tTransplantes.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tTransplantes.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id indec del registro
            int id = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 5){
            	this.verTransplante(id);
            } else if (columna == 6){
            	this.borraTransplante(id);
            }

		}

	}

}
