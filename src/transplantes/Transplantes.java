/*

    Nombre: Agudo
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre los órganos
                 transplantados de los pacientes, esta tabla puede
                 tener mas de una entrada por paciente

*/

 // declaración del paquete
package transplantes;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Transplantes {

    // declaración de variables de clase
    protected Conexion Enlace;               // puntero a la base de datos
    protected int Id;                        // clave del registro
    protected int Protocolo;                 // clave del paciente
    protected int IdOrgano;                  // clave del órgano transplantado
    protected String Organo;                 // nombre del órgano
    protected String Positivo;               // si el órgano era positivo para chagas
    protected String FechaTransplante;       // fecha de realización del transplante
    protected int IdUsuario;                 // clave del usuario
    protected String Usuario;                // nombre del usuario
    protected String Fecha;                  // fecha de alta del registro

    // constructor de la clase
    public Transplantes(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Protocolo = 0;
        this.IdOrgano = 0;
        this.Organo = "";
        this.Positivo = "No";
        this.FechaTransplante = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setProtocolo(int protocolo){
        this.Protocolo = protocolo;
    }
    public void setIdOrgano(int idorgano){
        this.IdOrgano = idorgano;
    }
    public void setPositivo(String positivo){
        this.Positivo = positivo;
    }
    public void setFechaTransplante(String fecha){
        this.FechaTransplante = fecha;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getProtocolo(){
        return this.Protocolo;
    }
    public int getIdOrgano(){
        return this.IdOrgano;
    }
    public String getOrgano(){
        return this.Organo;
    }
    public String getPositivo(){
        return this.Positivo;
    }
    public String getFechaTransplante(){
        return this.FechaTransplante;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idprotocolo entero con la clave del protocolo
     * @return vector con los registros encontrados
     * Método que recibe como parámetro la clave de un protocolo
     * y retorna el vector con los transplantes declarados
     * de ese paciente
     */
    public ResultSet nominaTransplantes(int idprotocolo){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_transplantes.id_transplante AS id, " +
                   "       diagnostico.v_transplantes.protocolo AS protocolo, " +
                   "       diagnostico.v_transplantes.organo AS organo, " +
                   "       diagnostico.v_transplantes.positivo AS positivo, " +
                   "       diagnostico.v_transplantes.fecha_transplante AS fecha, " +
                   "       diagnostico.v_transplantes.usuario AS usuario " +
                   "FROM diagnostico.v_transplantes " +
                   "WHERE diagnostico.v_transplantes.protocolo = '" + idprotocolo + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_transplantes.fecha_transplante, '%d/%m/%Y') DESC; ";

        // obtenemos el vector y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtransplante - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores del mismo
     */
    public void getDatosTransplante(int idtransplante){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_transplantes.id_transplante AS id, " +
                   "       diagnostico.v_transplantes.protocolo AS protocolo, " +
                   "       diagnostico.v_transplantes.id_organo AS idorgano, " +
                   "       diagnostico.v_transplantes.organo AS organo, " +
                   "       diagnostico.v_transplantes.positivo AS positivo, " +
                   "       diagnostico.v_transplantes.fecha_transplante AS fechatransplante, " +
                   "       diagnostico.v_transplantes.usuario AS usuario, " +
                   "       diagnostico.v_transplantes.fecha_alta AS fecha " +
                   "FROM diagnostico.v_transplantes " +
                   "WHERE diagnostico.v_transplantes.id_transplante = '" + idtransplante + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Protocolo = Resultado.getInt("protocolo");
            this.IdOrgano = Resultado.getInt("idorgano");
            this.Organo = Resultado.getString("organo");
            this.Positivo = Resultado.getString("positivo");
            this.FechaTransplante = Resultado.getString("fechatransplante");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y luego retorna la clave del
     * registro afectado
     */
    public int grabaTransplante(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoTransplante();
        } else {
            this.editaTransplante();
        }

        // retorna la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoTransplante(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.transplantes " +
                   "       (id_protocolo, " +
                   "        organo, " +
                   "        positivo, " +
                   "        fecha_transplante, " +
                   "        id_usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Protocolo);
            psInsertar.setInt(2, this.IdOrgano);
            psInsertar.setString(3, this.Positivo);
            psInsertar.setString(4, this.FechaTransplante);
            psInsertar.setInt(5, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaTransplante(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.transplantes SET " +
                   "       organo = ?, " +
                   "       positivo = ?, " +
                   "       fecha_transplante = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       id_usuario = ? " +
                   "WHERE diagnostico.transplantes.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.IdOrgano);
            psInsertar.setString(2, this.Positivo);
            psInsertar.setString(3, this.FechaTransplante);
            psInsertar.setInt(4, this.IdUsuario);
            psInsertar.setInt(5, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtransplante clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public void borraTransplante(int idtransplante){

        // declaración de variables
        String Consulta;

        // compone y ejecuta la consulta
        Consulta = "DELETE FROM diagnostico.transplantes " +
                   "WHERE diagnostico.transplantes.id = '" + idtransplante + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}