/*

    Nombre: FormOrganos
    Fecha: 01/11/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa el formulario del ABM de
                 organos transplantados

 */

// definición del paquete
package organos;

// inclusión de librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormOrganos extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -2258284137886873880L;

	// declaración de variables
	private JTextField tId;
	private JTextField tOrgano;
	private JTable tListado;
	private Organos Transplantes;

	// constructor de la clase
	public FormOrganos(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// cierra el formulario
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		// fijamos las propiedades del formulario
		this.setBounds(120, 170, 380, 350);
		this.getContentPane().setLayout(null);
		this.setTitle("Organos de Transplante");

		// inicializamos el objeto
		this.Transplantes = new Organos();

		// instanciamos los componenetes
		this.initFormOrganos();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que instancia el formulario
	 */
	@SuppressWarnings("serial")
	protected void initFormOrganos(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Organos de Transplante");
		lTitulo.setBounds(10, 10, 286, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presenta la id de la fuente
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 38, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// presenta el nombre de la fuente
		this.tOrgano = new JTextField();
		this.tOrgano.setBounds(62, 45, 253, 26);
		this.tOrgano.setFont(Fuente);
		this.tOrgano.setToolTipText("Nombre del órgano transplantado");
		getContentPane().add(tOrgano);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setToolTipText("Graba el registro en la base");
		btnGrabar.setBounds(325, 45, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaOrgano();
			}
		});
		getContentPane().add(btnGrabar);

		// define la tabla con la nómina
		this.tListado = new JTable();
		this.tListado.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"ID",
				"Organo",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Object.class, Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// fijamos el ancho de las columnas
        this.tListado.getColumn("ID").setPreferredWidth(30);
        this.tListado.getColumn("ID").setMaxWidth(30);
        this.tListado.getColumn("Ed.").setPreferredWidth(35);
        this.tListado.getColumn("Ed.").setMaxWidth(35);
        this.tListado.getColumn("El.").setPreferredWidth(35);
        this.tListado.getColumn("El.").setMaxWidth(35);

		// establecemos el tooltip
		this.tListado.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tListado.setRowHeight(25);

		// establecemos la fuente
		this.tListado.setFont(Fuente);

		// fijamos el evento click
		this.tListado.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tListadoMouseClicked(evt);
			}
		});

		// agregamos la tabla al scroll
		JScrollPane scrollOrganos = new JScrollPane();
		scrollOrganos.setBounds(10, 80, 356, 225);
		scrollOrganos.setViewportView(this.tListado);
		getContentPane().add(scrollOrganos);

		// carga la grilla con las compañías
		this.cargaOrganos();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla con la nómina de fuentes
     * de financiamiento
	 */
	protected void cargaOrganos(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Transplantes.nominaOrganos();

		// sobrecargamos el renderer de la tabla
		this.tListado.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tListado.getModel();

		// hacemos la tabla se pueda ordenar
		tListado.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[4];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("organo");
				fila[2] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica los
	 * datos y ejecuta la consulta
	 */
	protected void grabaOrgano(){

		// verifica se halla ingresado un nombre
		if (this.tOrgano.getText().isEmpty()) {

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre del órgano", "Error", JOptionPane.ERROR_MESSAGE);
			return;

        // si ingresó
		} else {

            // fija los valores en la clase
            this.Transplantes.setOrgano(this.tOrgano.getText());

        }

		// si está dando un alta verifica que no esté repetido
		if (this.tId.getText().isEmpty()) {

			if (!this.Transplantes.validaOrgano(this.tOrgano.getText())) {

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Ese órgano ya está declarado", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;

			// lo marca como un alta
			} else {

				// setea el valor
				this.Transplantes.setIdOrgano(0);;

			}

		// si está editando
		} else {

			// almacena la clave
			this.Transplantes.setIdOrgano(Integer.parseInt(this.tId.getText()));

		}

		// graba el registro
		this.Transplantes.grabaOrgano();

		// limpia el formulario
		this.limpiaOrganos();

		// recarga la grilla
		this.cargaOrganos();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre el ícono de edición
	 * que obtiene los datos del registro y los presenta
	 */
	protected void getOrgano(int clave){

		// obtenemos los datos y los asignamos
		this.tId.setText(Integer.toString(clave));
		this.tOrgano.setText(this.Transplantes.getNombreOrgano(clave));

		// fijamos el foco
		this.tOrgano.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método que pide confirmación y verifica que pueda eliminar
	 * el registro y lo elimina
	 */
	protected void borraOrgano(int clave){

		// verifica si puede eliminar
		if (!this.Transplantes.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa organo tiene pacientes asignados", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                    "Está seguro que desea eliminar el registro?",
                                       "Organos Transplantados",
                                       JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Transplantes.borraOrgano(clave);

			// limpiamos el formulario
			this.limpiaOrganos();

			// recargamos la grilla
            this.cargaOrganos();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia los campos de edición
	 */
	protected void limpiaOrganos(){

		// limpiamos los campos
		this.tId.setText("");
		this.tOrgano.setText("");

		// fijamos el foco
		this.tOrgano.requestFocus();

	}

	// método llamado al pulsar sobre la grilla
	private void tListadoMouseClicked(MouseEvent evt) {

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tListado.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tListado.rowAtPoint(evt.getPoint());
		int columna = this.tListado.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tListado.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 2) {

				// cargamos el registro
				this.getOrgano(clave);

			// si pulsó en eliminar
			} else if (columna == 3) {

				// eliminamos
				this.borraOrgano(clave);

			}

		}

	}

}
