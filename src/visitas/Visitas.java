/*

    Nombre: Visitas
    Fecha: 22/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que provee los datos de la vista de visitas
                 del paciente

 */

// definicion del paquete
package visitas;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// definios la clase
public class Visitas{

    // definimos las variables
    private Conexion Enlace;               // puntero a la base de datos
    private int IdUsuario;                 // clave del usuario
    private int Id;                        // clave del registro
    private String Usuario;                // nombre del usuario
    private String Fecha;                  // fecha de la visita
    private int Paciente;                  // protocolo del paciente
    private String FechaAlta;              // fecha de alta del registro

    // constructor de la clase
    public Visitas(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.IdUsuario = Seguridad.Id;
        this.Id = 0;
        this.Usuario = "";
        this.Fecha = "";
        this.Paciente = 0;

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - clave del paciente
     * @return vector con los registros
     * Método que recibe como parámetro el protocolo de un
     * paciente y retorna el vector con las visitas del mismo
     */
    public ResultSet nominaVisitas(int protocolo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_visitas.id AS id, " +
                   "       diagnostico.v_visitas.fecha AS fecha, " +
                   "       diagnostico.v_visitas.usuario AS usuario, " +
                   "       diagnostico.v_visitas.estadio AS estadio, " +
                   "       diagnostico.v_visitas.paciente AS paciente, " +
                   "       diagnostico.v_visitas.estadio AS estadio, " +
                   "       diagnostico.v_visitas.peso AS peso, " +
                   "       diagnostico.v_visitas.auscultacion AS auscultacion, " +
                   "       diagnostico.v_visitas.soplos AS soplos " +
                   "FROM diagnostico.v_visitas " +
                   "WHERE diagnostico.v_visitas.paciente = '" + protocolo + "'; ";

        // ejecutamos la consulta y retornmos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita - clave del registro
     * Método que recibe como parámetro la clave de la visita
     * y ejecuta la consulta de eliminación en todas las
     * tablas asociadas
     */
    public void borraVisita(int idvisita){

        // declaración de variables
        String Consulta;

        // componemos la consulta para eliminar en cascada
        Consulta = "DELETE diagnostico.visitas, " +
                   "       diagnostico.ecocardiograma, " +
                   "       diagnostico.electrocardiograma, " +
                   "       diagnostico.ergometria, " +
                   "       diagnostico.fisico, " +
                   "       diagnostico.holter, " +
                   "       diagnostico.rx, " +
                   "       diagnostico.cardiovascular, " +
                   "       diagnostico.clasificacion, " +
                   "       diagnostico.sintomas " +
                   "FROM diagnostico.visitas JOIN diagnostico.ecocardiograma ON diagnostico.visitas.id = diagnostico.ecocardiograma.visita " +
                   "                         JOIN diagnostico.electrocardiograma ON diagnostico.visitas.id = diagnostico.electrocardiograma.visita " +
                   "                         JOIN diagnostico.ergometria ON diagnostico.visitas.id = diagnostico.ergometria.visita " +
                   "                         JOIN diagnostico.fisico ON diagnostico.visitas.id = diagnostico.fisico.visita " +
                   "                         JOIN diagnostico.holter ON diagnostico.visitas.id = diagnostico.holter.visita " +
                   "                         JOIN diagnostico.rx ON diagnostico.visitas.id = diagnostico.rx.visita " +
                   "                         JOIN diagnostico.cardiovascular ON diagnostico.visitas.id = diagnostico.cardiovascular.visita " +
                   "                         JOIN diagnostico.clasificacion ON diagnostico.visitas.id = diagnostico.clasificacion.visita " +
                   "                         JOIN diagnostico.sintomas ON diagnostico.visitas.id = diagnostico.sintomas.visita " +
                   "WHERE diagnostico.visitas.id = '" + idvisita + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave entero con la clave del registro
     * Método que recibe como parámetro la clave del
     * registro y asigna en las variables de clase los
     * valores del mismo
     */
    public void getDatosVisita(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_visitas.id AS id, " +
                   "       diagnostico.v_visitas.fecha AS fecha, " +
                   "       diagnostico.v_visitas.usuario AS usuario, " +
                   "       diagnostico.v_visitas.paciente AS paciente, " +
                   "       diagnostico.v_visitas.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.v_visitas " +
                   "WHERE diagnostico.v_visitas.id = '" + clave + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Fecha = Resultado.getString("fecha");
            this.Usuario = Resultado.getString("usuario");
            this.Paciente = Resultado.getInt("paciente");
            this.FechaAlta = Resultado.getString("fecha_alta");

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id - clave del registro afectado
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda
     */
    public int grabaVisita(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaVisita();
        } else {
            this.editaVisita();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    private void nuevaVisita(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.visitas " +
                   "       (fecha, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (STR_TO_DATE(?, '%d/%m/%Y'), ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Fecha);
            psInsertar.setInt(2,    this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    private void editaVisita(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.visitas SET " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       usuario = ? " +
                   "WHERE diagnostico.visitas.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Fecha);
            psInsertar.setInt(2,    this.IdUsuario);
            psInsertar.setInt(3,    this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}