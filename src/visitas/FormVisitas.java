/*

    Nombre: FormVisitas
    Fecha: 22/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario de visitas del paciente

 */

// definicion del paquete
package visitas;

// importamos las librerìas
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import com.toedter.calendar.JDateChooser;
import antecedentes.FormAntecedentes;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import java.awt.Font;
import java.awt.Frame;
import javax.swing.text.MaskFormatter;
import javax.swing.text.DefaultFormatterFactory;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import clasificacion.Clasificacion;
import seguridad.Seguridad;
import funciones.Mensaje;
import funciones.Utilidades;
import cardiovascular.Cardiovascular;
import fisico.Fisico;
import ecocardio.FormEcocardio;
import electro.FormElectro;
import holter.FormHolter;
import rx.FormRx;
import ergometria.FormErgometria;
import sintomas.Sintomas;
import visitas.Visitas;

// definicion de la clase
public class FormVisitas extends javax.swing.JDialog {

    // declaramos el serial id
    private static final long serialVersionUID = 1L;

    // declaraciòn de variables de clase
    private JCheckBox cAortico;
    private JCheckBox cConciencia;
    private JComboBox<Object> cDisnea;
    private JCheckBox cEdema;
    private JCheckBox cEsplenomegalia;
    private JComboBox<Object> cEstadio;
    private JCheckBox cEyectivo;
    private JCheckBox cHepatomegalia;
    private JCheckBox cIngurgitacion;
    private JCheckBox cMitral;
    private JCheckBox cNoTiene;
    private JCheckBox cNormal;
    private JComboBox<Object> cPalpitaciones;
    private JComboBox<Object> cPrecordial;
    private JCheckBox cPresincope;
    private JCheckBox cRegurgitativo;
    private JCheckBox cSinDiastolico;
    private JDateChooser dFecha;
    private JCheckBox c3er;
    private JCheckBox c4tor;
    private JCheckBox cRitmo;
    private JSpinner sFc;
    private JSpinner sSpo;
    private JTextField tAlta;
    private JFormattedTextField tBmi;
    private JCheckBox cSp;
    private JCheckBox cMvDisminuido;
    private JCheckBox cCrepitantes;
    private JCheckBox cSibilancias;
    private JTextArea tObservaciones;
    private JFormattedTextField tPeso;
    private JFormattedTextField tTa;
    private JFormattedTextField tTalla;
    private JTextField tUsuario;

    // variables de clase
    private int IdVisita;                            // clave de la visita
    private int Protocolo;                           // clave del paciente
    private int IdAgudo;                             // clave de la tabla de clasificación
    private int IdSintoma;                           // clave de la tabla de síntomas
    private int IdFisico;                            // clave de la tabla de examen físico
    private int IdCardio;                            // clave de la tabla cardiovascular
    private Clasificacion Estadio;                   // clase de la base de datos
    private Visitas Entradas;                        // clase de la base de datos
    private Sintomas Indicios;                       // clase de la base de datos
    private Fisico Examen;                           // clase de la base de datos
    private Cardiovascular Corazon;                  // clase de la base de datos
    private Utilidades Herramientas;                 // funciones de fecha
    private FormAntecedentes Padre;                  // formulario padre

    // constructor de la clase, recibe como parámetro el protocolo del paciente
    public FormVisitas(Frame parent, boolean modal, FormAntecedentes padre, int protocolo) {

        // fijamos el padre
        super(parent, modal);

        // asignamos el formulario 
        this.Padre = padre;

        // asignamos el protocolo
        this.Protocolo = protocolo;

        // inicializamos el formulario y lo mostramos
        initComponents();

    }

	// inicializamos el formulario
    private void initComponents() {

        // inicializamos la variable de clase
        this.Herramientas = new Utilidades();
        this.Entradas = new Visitas();
        this.Estadio = new Clasificacion();
        this.Indicios = new Sintomas();
        this.Examen = new Fisico();
        this.Corazon = new Cardiovascular();
        this.IdVisita = 0;

        // fija las propiedades del formulario
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Datos de la Visita");
        this.setBounds(50, 50, 1000, 600);
        this.setResizable(false);
        this.setLayout(null);

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta el título
        JLabel lTitulo = new javax.swing.JLabel("Datos de la Visita");
        lTitulo.setBounds(10, 10, 300, 26);
        lTitulo.setFont(Fuente);
        this.add(lTitulo);

        // pide la fecha
        JLabel lFecha = new javax.swing.JLabel("Fecha Visita:");
        lFecha.setBounds(310, 10, 80, 26);
        lFecha.setFont(Fuente);
        this.add(lFecha);
        this.dFecha = new com.toedter.calendar.JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFecha.setBounds(395, 10, 110, 26);
        this.dFecha.setFont(Fuente);
        this.add(this.dFecha);

        // por defecto fijamos la fecha actual
        Calendar fechaActual = new GregorianCalendar();
        this.dFecha.setCalendar(fechaActual);

        // presenta el tìtulo de la claseificación
        JLabel lClasificacion = new javax.swing.JLabel("Clasificación de la Enfermedad según Kuscknir");
        lClasificacion.setBounds(10, 45, 300, 26);
        lClasificacion.setFont(Fuente);
        this.add(lClasificacion);

        // pide el estadio
        JLabel lEstadio = new javax.swing.JLabel("Estadio:");
        lEstadio.setBounds(10, 80, 70, 26);
        lEstadio.setFont(Fuente);
        this.add(lEstadio);
        this.cEstadio = new javax.swing.JComboBox<>();
        this.cEstadio.setBounds(75, 80, 130, 26);
        this.cEstadio.setFont(Fuente);
        this.cEstadio.setToolTipText("Seleccione el estadío de la Enfermedad");
        this.add(this.cEstadio);

        // agrega los elementos al combo
        this.elementosEstadio();

        // presenta el título del examen físico
        JLabel lExamen = new javax.swing.JLabel("Exámen Físico");
        lExamen.setFont(Fuente);
        this.add(lExamen);
        lExamen.setBounds(330, 45, 110, 26);

        // pide la presión
        JLabel lTa = new javax.swing.JLabel("TA:");
        lTa.setBounds(220, 80, 105, 26);
        lTa.setFont(Fuente);
        lTa.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lTa);
        this.tTa = new javax.swing.JFormattedTextField();
        this.tTa.setFont(Fuente);
        this.tTa.setBounds(340, 80, 80, 26);
        this.tTa.setColumns(6);
        try {
            this.tTa.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("###/###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        this.tTa.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        this.tTa.setToolTipText("Indique la presión arterial");
        this.add(this.tTa);

        // pide la frecuencia
        JLabel lFc = new javax.swing.JLabel("FC:");
        lFc.setBounds(420, 80, 30, 26);
        lFc.setFont(Fuente);
        lFc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lFc);
        this.sFc = new javax.swing.JSpinner();
        this.sFc.setModel(new SpinnerNumberModel(50, 50, 180, 1));
        this.sFc.setBounds(450, 80, 45, 26);
        this.sFc.setFont(Fuente);
        this.sFc.setToolTipText("Indique la Frecuencia Cardíaca");
        this.add(this.sFc);

        // pide la saturación de oxígeno
        JLabel lSpo = new javax.swing.JLabel("SpO:");
        lSpo.setBounds(510, 80, 40, 26);
        lSpo.setFont(Fuente);
        lSpo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lSpo);
        this.sSpo = new javax.swing.JSpinner();
        this.sSpo.setBounds(550, 80, 50, 26);
        this.sSpo.setFont(Fuente);
        this.sSpo.setModel(new SpinnerNumberModel(70, 70, 150, 1));
        this.sSpo.setToolTipText("Indique la saturación en porcentaje");
        this.add(this.sSpo);

        // pide el peso
        JLabel lPeso = new javax.swing.JLabel("Peso:");
        lPeso.setFont(Fuente);
        lPeso.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lPeso.setBounds(610, 80, 50, 26);
        this.add(lPeso);
        this.tPeso = new javax.swing.JFormattedTextField();
        this.tPeso.setBounds(670, 80, 80, 26);
        this.tPeso.setFont(Fuente);
        this.tPeso.setColumns(6);
        try {
            this.tPeso.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("###.##")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.tPeso.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        this.tPeso.setToolTipText("Indique el peso en kilogramos");
        this.add(this.tPeso);

        // pide la talla
        JLabel lTalla = new javax.swing.JLabel("Talla:");
        lTalla.setBounds(750, 80, 50, 26);
        lTalla.setFont(Fuente);
        lTalla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lTalla);
        this.tTalla = new javax.swing.JFormattedTextField();
        this.tTalla.setBounds(810, 80, 70, 26);
        this.tTalla.setFont(Fuente);
        this.tTalla.setColumns(4);
        try {
            this.tTalla.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("#.##")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.tTalla.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        this.tTalla.setToolTipText("Indique la altura en metros y centímetros");
        this.add(this.tTalla);

        // pide el índice de masa corporal
        JLabel lBmi = new javax.swing.JLabel("BMI:");
        lBmi.setBounds(890, 80, 30, 26);
        lBmi.setFont(Fuente);
        lBmi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lBmi);
        this.tBmi = new javax.swing.JFormattedTextField();
        this.tBmi.setBounds(930, 80, 60, 26);
        this.tBmi.setFont(Fuente);
        this.tBmi.setColumns(5);
        try {
            this.tBmi.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##.##")));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.tBmi.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        this.tBmi.setToolTipText("Indique el índice de masa corporal");
        this.add(this.tBmi);

        // agrega las opciones de tórax
        JLabel lTorax = new JLabel("Tórax:");
        lTorax.setBounds(315, 115, 60, 26);
        lTorax.setFont(Fuente);
        this.add(lTorax);

        // presenta el check de sp
        this.cSp = new JCheckBox("S/P");
        this.cSp.setBounds(370, 115, 60, 26);
        this.cSp.setFont(Fuente);
        this.cSp.setToolTipText("Marque si corresponde");
        this.add(this.cSp);

        // presenta el check de mvdisminuido
        this.cMvDisminuido = new JCheckBox("MV disminuido");
        this.cMvDisminuido.setBounds(440, 115, 120, 26);
        this.cMvDisminuido.setFont(Fuente);
        this.cMvDisminuido.setToolTipText("Marque si corresponde");
        this.add(this.cMvDisminuido);

        // presenta el check de crepitantes
        this.cCrepitantes = new JCheckBox("Crepitantes");
        this.cCrepitantes.setBounds(560, 115, 115, 26);
        this.cCrepitantes.setFont(Fuente);
        this.cCrepitantes.setToolTipText("Marque si corresponde");
        this.add(this.cCrepitantes);

        // presenta el check de sibilancias
        this.cSibilancias = new JCheckBox("Sibilancias");
        this.cSibilancias.setBounds(670, 115, 120, 26);
        this.cSibilancias.setFont(Fuente);
        this.cSibilancias.setToolTipText("Marque si presenta sibilancias");
        this.add(this.cSibilancias);

        // presenta el título de síntomas
        JLabel lSintomas = new javax.swing.JLabel("Sintomas");
        lSintomas.setBounds(10, 115, 100, 26);
        lSintomas.setFont(Fuente);
        this.add(lSintomas);

        // el combo de dolor precordial
        JLabel lPrecordial = new javax.swing.JLabel("Dolor Precordial:");
        lPrecordial.setBounds(10, 150, 130, 26);
        lPrecordial.setFont(Fuente);
        this.add(lPrecordial);
        this.cPrecordial = new javax.swing.JComboBox<>();
        this.cPrecordial.setBounds(130, 150, 100, 26);
        this.cPrecordial.setFont(Fuente);
        this.cPrecordial.setToolTipText("Indique el tipo de dolor precordial");
        this.add(this.cPrecordial);

        // agrega los elementos al combo
        this.elementosPrecordial();

        // el combo de disnea
        JLabel lDisnea = new javax.swing.JLabel("Disnea:");
        lDisnea.setBounds(250, 150, 60, 26);
        lDisnea.setFont(Fuente);
        this.add(lDisnea);
        this.cDisnea = new javax.swing.JComboBox<>();
        this.cDisnea.setBounds(310, 150, 160, 26);
        this.cDisnea.setFont(Fuente);
        this.cDisnea.setToolTipText("Seleccione la causa de la disnea");
        this.add(this.cDisnea);

        // agrega los elementos al combo
        this.elementosDisnea();

        // el combo de palpitaciones
        JLabel lPalpitaciones = new javax.swing.JLabel("Palpitaciones:");
        lPalpitaciones.setBounds(480, 150, 110, 26);
        lPalpitaciones.setFont(Fuente);
        lPalpitaciones.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lPalpitaciones);
        this.cPalpitaciones = new javax.swing.JComboBox<>();
        this.cPalpitaciones.setBounds(595, 150, 150, 26);
        this.cPalpitaciones.setFont(Fuente);
        this.cPalpitaciones.setToolTipText("Indique si presenta palpitaciones");
        this.add(this.cPalpitaciones);

        // agrega los elementos al combo
        this.elementosPalpitaciones();

        // el check de presincope
        this.cPresincope = new javax.swing.JCheckBox("Presíncope");
        this.cPresincope.setBounds(10, 185, 100, 26);
        this.cPresincope.setFont(Fuente);
        this.cPresincope.setToolTipText("Indique si ha presentado presíncope");
        this.add(this.cPresincope);

        // el chec de pèrdida de conciencia
        this.cConciencia = new javax.swing.JCheckBox("P. de Conciencia");
        this.cConciencia.setBounds(160, 185, 140, 26);
        this.cConciencia.setFont(Fuente);
        this.cConciencia.setToolTipText("Indique si sufrió pérdida de conciencia");
        this.add(this.cConciencia);

        // el chec de edema de miembros inferiores
        this.cEdema = new javax.swing.JCheckBox("Edema de M. Inferiores");
        this.cEdema.setBounds(343, 185, 200, 26);
        this.cEdema.setFont(Fuente);
        this.cEdema.setToolTipText("Indique si presenta edema de miembros inferiores");
        this.add(this.cEdema);

        // el tìtulo de cardiovascular
        JLabel lCardiovascular = new javax.swing.JLabel("Aparato Cardiovascular");
        lCardiovascular.setBounds(10, 220, 200, 26);
        lCardiovascular.setFont(Fuente);
        this.add(lCardiovascular);

        // el tìtulo de auscultación
        JLabel lAuscultacion = new javax.swing.JLabel("Auscultación:");
        lAuscultacion.setBounds(10, 255, 140, 26);
        lAuscultacion.setFont(Fuente);
        lAuscultacion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lAuscultacion);

        // el chec de normal
        this.cNormal = new javax.swing.JCheckBox("Normal");
        this.cNormal.setBounds(202, 255, 80, 26);
        this.cNormal.setFont(Fuente);
        this.cNormal.setToolTipText("Marque si es normal");
        this.add(this.cNormal);

        // el chec de ritmo
        this.cRitmo = new javax.swing.JCheckBox("Ritmo Irregular");
        this.cRitmo.setBounds(343, 255, 140, 26);
        this.cRitmo.setFont(Fuente);
        this.cRitmo.setToolTipText("Marque si el ritmo es irregular");
        this.add(this.cRitmo);

        // el chec de tercer
        this.c3er = new javax.swing.JCheckBox("3er.R");
        this.c3er.setBounds(530, 255, 90, 26);
        this.c3er.setFont(Fuente);
        this.c3er.setToolTipText("Marque si corresponde");
        this.add(this.c3er);

        // el chec de cuarto
        this.c4tor = new javax.swing.JCheckBox("4to.R");
        this.c4tor.setBounds(650, 255, 80, 26);
        this.c4tor.setFont(Fuente);
        this.c4tor.setToolTipText("Marque si corresponde");
        this.add(this.c4tor);

        // el título de soplos
        JLabel lSoplos = new javax.swing.JLabel("Soplos Sistólicos:");
        lSoplos.setBounds(10, 290, 140, 26);
        lSoplos.setFont(Fuente);
        lSoplos.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lSoplos);

        // el check de eyectivo
        this.cEyectivo = new javax.swing.JCheckBox("Eyectivo Aórtico");
        this.cEyectivo.setBounds(202, 290, 130, 26);
        this.cEyectivo.setFont(Fuente);
        this.cEyectivo.setToolTipText("Marque si corresponde");
        this.add(this.cEyectivo);

        // el chec de regurgitativo
        this.cRegurgitativo = new javax.swing.JCheckBox("Regurgitativo Mitral");
        this.cRegurgitativo.setBounds(343, 290, 160, 26);
        this.cRegurgitativo.setFont(Fuente);
        cRegurgitativo.setToolTipText("Marque si corresponde");
        this.add(this.cRegurgitativo);

        // el chec de no tiene
        this.cNoTiene = new javax.swing.JCheckBox("No Tiene");
        this.cNoTiene.setBounds(530, 290, 90, 26);
        this.cNoTiene.setFont(Fuente);
        this.cNoTiene.setToolTipText("Marque si no presenta soplos");
        this.add(this.cNoTiene);

        // el tìtulo de diastólico
        JLabel lDiastolico = new javax.swing.JLabel("Soplos Diastólicos:");
        lDiastolico.setBounds(10, 325, 140, 26);
        lDiastolico.setFont(Fuente);
        lDiastolico.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lDiastolico);

        // el chec de aórtico
        this.cAortico = new javax.swing.JCheckBox("Aórtico");
        this.cAortico.setBounds(202, 325, 120, 26);
        this.cAortico.setFont(Fuente);
        this.cAortico.setToolTipText("Marque si corresponde");
        this.add(this.cAortico);

        // el chec de mitral
        this.cMitral = new javax.swing.JCheckBox("Mitral");
        this.cMitral.setBounds(343, 325, 160, 26);
        this.cMitral.setFont(Fuente);
        this.cMitral.setToolTipText("Marque si corresponde");
        this.add(this.cMitral);

        // el chec de no tiene
        this.cSinDiastolico = new javax.swing.JCheckBox("No Tiene");
        this.cSinDiastolico.setBounds(530, 325, 120, 26);
        this.cSinDiastolico.setFont(Fuente);
        this.cSinDiastolico.setToolTipText("Marque si no presenta soplos");
        this.add(this.cSinDiastolico);

        // el chec de hepatomegalia
        this.cHepatomegalia = new javax.swing.JCheckBox("Hepatomegalia");
        this.cHepatomegalia.setBounds(10, 360, 160, 26);
        this.cHepatomegalia.setFont(Fuente);
        this.cHepatomegalia.setToolTipText("Marque si presenta hepatomegalia");
        this.add(this.cHepatomegalia);

        // el check de esplenomegalia
        this.cEsplenomegalia = new javax.swing.JCheckBox("Esplenomegalia");
        this.cEsplenomegalia.setBounds(202, 360, 141, 26);
        this.cEsplenomegalia.setFont(Fuente);
        this.cEsplenomegalia.setToolTipText("Marque si presenta esplenomegalia");
        this.add(this.cEsplenomegalia);

        // el chec de ingurgitación
        this.cIngurgitacion = new javax.swing.JCheckBox("Ingurgitación Yugular");
        this.cIngurgitacion.setBounds(343, 360, 180, 26);
        this.cIngurgitacion.setFont(Fuente);
        this.cIngurgitacion.setToolTipText("Marque si presenta ingurgitación");
        this.add(this.cIngurgitacion);

        // el label de observaciones
        JLabel lObservaciones = new javax.swing.JLabel("Observaciones");
        lObservaciones.setBounds(10, 395, 100, 26);
        lObservaciones.setFont(Fuente);
        this.add(lObservaciones);

        // el contenedor de observaciones
        JScrollPane scrollObservaciones = new javax.swing.JScrollPane();
        scrollObservaciones.setBounds(110, 395, 600, 110);
        this.add(scrollObservaciones);

        // el área de texto de observaciones
        this.tObservaciones = new javax.swing.JTextArea();
        this.tObservaciones.setFont(Fuente);
        this.tObservaciones.setColumns(20);

        // agregamos el texto al scroll
        scrollObservaciones.setViewportView(this.tObservaciones);

        // el usuario
        JLabel lUsuario = new javax.swing.JLabel("Usuario:");
        lUsuario.setBounds(715, 395, 60, 26);
        lUsuario.setFont(Fuente);
        lUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lUsuario);
        this.tUsuario = new javax.swing.JTextField();
        this.tUsuario.setBounds(780, 395, 90, 26);
        this.tUsuario.setFont(Fuente);
        this.tUsuario.setEditable(false);
        this.tUsuario.setToolTipText("Usuario que ingresó el registro");
        this.add(this.tUsuario);

        // la fecha de alta
        JLabel lAlta = new javax.swing.JLabel("Alta:");
        lAlta.setBounds(715, 430, 60, 26);
        lAlta.setFont(Fuente);
        lAlta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lAlta);
        this.tAlta = new javax.swing.JTextField();
        this.tAlta.setBounds(780, 430, 90, 26);
        this.tAlta.setFont(Fuente);
        this.tAlta.setEditable(false);
        this.tAlta.setToolTipText("Fecha de alta del registro");
        this.add(this.tAlta);

        // el botón grabar
        JButton btnGrabar = new javax.swing.JButton("Grabar");
        btnGrabar.setBounds(880, 395, 110, 26);
        btnGrabar.setFont(Fuente);
        btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.setToolTipText("Pulse para grabar el registro");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaVisita();
            }
        });
        this.add(btnGrabar);

        // el botón cancelar
        JButton btnCancelar = new javax.swing.JButton("Cancelar");
        btnCancelar.setBounds(880, 430, 110, 26);
        btnCancelar.setFont(Fuente);
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mBorrar.png"))); // NOI18N
        btnCancelar.setToolTipText("Cierra el formulario sin grabar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaVisita();
            }
        });
        this.add(btnCancelar);

        // el botón radiografías
        JButton btnRx = new javax.swing.JButton("RX");
        btnRx.setBounds(10, 520, 120, 26);
        btnRx.setFont(Fuente);
        btnRx.setToolTipText("Datos de las Radiografías");
        btnRx.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verRx();
            }
        });
        this.add(btnRx);

        // el botón electrocardiograma
        JButton btnElectro = new javax.swing.JButton("Electro");
        btnElectro.setBounds(450, 520, 120, 26);
        btnElectro.setFont(Fuente);
        btnElectro.setToolTipText("Datos del Electrocardiograma");
        btnElectro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verElectro();
            }
        });
        this.add(btnElectro);

        // el botón ecografía
        JButton btnEco = new javax.swing.JButton("Ecocardio.");
        btnEco.setBounds(150, 520, 120, 26);
        btnEco.setFont(Fuente);
        btnEco.setToolTipText("Datos del electrocardiograma");
        btnEco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verEcocardiograma();
            }
        });
        this.add(btnEco);

        // el botón holter
        JButton btnHolter = new javax.swing.JButton("Holter");
        btnHolter.setBounds(600, 520, 120, 26);
        btnHolter.setFont(Fuente);
        btnHolter.setToolTipText("Datos del Holter");
        btnHolter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verHolter();
            }
        });
        this.add(btnHolter);

        // el botón ergometría
        JButton btnErgo = new javax.swing.JButton("Ergometría");
        btnErgo.setBounds(300, 520, 120, 26);
        btnErgo.setFont(Fuente);
        btnErgo.setToolTipText("Datos de la ergometría");
        btnErgo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verErgometria();
            }
        });
        this.add(btnErgo);

        // fijamos el usuario y la fecha por defecto
        this.tUsuario.setText(Seguridad.Usuario);
        this.tAlta.setText(this.Herramientas.FechaActual());

        // mostramos el formulario
        this.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que agrega los elementos al combo de estadío de la
     * enfermedad
     */
    private void elementosEstadio(){

        // agregamos los elementos
        this.cEstadio.addItem("");
        this.cEstadio.addItem("Estadio 0");
        this.cEstadio.addItem("Estadio I");
        this.cEstadio.addItem("Estadio II");
        this.cEstadio.addItem("Estadio III");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de dolor
     * precordial
     */
    private void elementosPrecordial(){

        // agregamos los elementos
        this.cPrecordial.addItem("");
        this.cPrecordial.addItem("Típico");
        this.cPrecordial.addItem("Atípico");
        this.cPrecordial.addItem("No Tiene");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos del combo de disnea
     */
    private void elementosDisnea(){

        // agregamos los valores
        this.cDisnea.addItem("");
        this.cDisnea.addItem("En Reposo");
        this.cDisnea.addItem("Esfuerzos Leves");
        this.cDisnea.addItem("Esfuerzos Moderados");
        this.cDisnea.addItem("Grandes Esfuerzos");
        this.cDisnea.addItem("DPN");
        this.cDisnea.addItem("No Tiene");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de palpitaciones
     */
    private void elementosPalpitaciones(){

        // agregamos los elementos
        this.cPalpitaciones.addItem("");
        this.cPalpitaciones.addItem("Frecuentemente");
        this.cPalpitaciones.addItem("Raramente");
        this.cPalpitaciones.addItem("No Tiene");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que recibe como parámetro la clave de una 
     * visita, asigna el valor en la variable de clase
     * y muestra el registro
     */
    public void verVisita(int idvisita){

        // asignamos en la variable de clase
        this.IdVisita = idvisita;

        // obtenemos el registro
        this.Entradas.getDatosVisita(this.IdVisita);

        // asignamos la fecha de la visita, la fecha de alta y el usuario
        this.dFecha.setDate(this.Herramientas.StringToDate(this.Entradas.getFecha()));
        this.tUsuario.setText(this.Entradas.getUsuario());
        this.tAlta.setText(this.Entradas.getFechaAlta());

        // obtenemos los datos de la clasificación
        this.Estadio.getDatosClasificacion(this.IdVisita);

        // asignamos en la variable de clase
        this.IdAgudo = this.Estadio.getId();

        // asignamos en el formulario
        this.cEstadio.setSelectedItem(this.Estadio.getEstadio());
        this.tObservaciones.setText(this.Estadio.getObservaciones());

        // obtenemos los datos del examen físico
        this.Examen.getDatosVisita(this.IdVisita);

        // asignamos en la variable de clase
        this.IdFisico = this.Examen.getId();

        // asignamos en el formulario
        this.tTa.setText(this.Examen.getTa());
        this.sFc.setValue(this.Examen.getFc());
        this.sSpo.setValue(this.Examen.getSpo());
        this.tPeso.setText(Float.toString(this.Examen.getPeso()));
        this.tTalla.setText(Float.toString(this.Examen.getTalla()));
        if (this.Examen.getSp() == 1){
            this.cSp.setSelected(true);
        } else {
            this.cSp.setSelected(false);
        }
        if (this.Examen.getMvdisminuido() == 1){
            this.cMvDisminuido.setSelected(true);
        } else {
            this.cMvDisminuido.setSelected(false);
        }
        if (this.Examen.getCrepitantes() == 1){
            this.cCrepitantes.setSelected(true);
        } else {
            this.cCrepitantes.setSelected(false);
        }
        if (this.Examen.getSibilancias() == 1){
            this.cSibilancias.setSelected(true);
        } else {
            this.cSibilancias.setSelected(false);
        }

        // obtenemos los antecedentes cardiovasculares
        this.Corazon.getDatosCardiovascular(this.IdVisita);

        // asignamos en la variable de clase
        this.IdCardio = this.Corazon.getId();

        // asignamos en el formulario
        if (this.Corazon.getAuscultacion() == 1){
            this.cNormal.setSelected(true);
        } else {
            this.cNormal.setSelected(false);
        }
        if (this.Corazon.getIrregular() == 1){
            this.cRitmo.setSelected(true);
        } else {
            this.cRitmo.setSelected(false);
        }
        if (this.Corazon.getTercer() == 1){
            this.c3er.setSelected(true);
        } else {
            this.c3er.setSelected(false);
        }
        if (this.Corazon.getCuarto() == 1){
            this.c4tor.setSelected(true);
        } else {
            this.c4tor.setSelected(false);
        }
        if (this.Corazon.getEyectivo() == 1){
            this.cEyectivo.setSelected(true);
        } else {
            this.cEyectivo.setSelected(false);
        }
        if (this.Corazon.getRegurgitativo() == 1){
            this.cRegurgitativo.setSelected(true);
        } else {
            this.cRegurgitativo.setSelected(false);
        }
        if (this.Corazon.getSinSistolico() == 1){
            this.cNoTiene.setSelected(true);
        } else {
            this.cNoTiene.setSelected(false);
        }
        if (this.Corazon.getAortico() == 1){
            this.cAortico.setSelected(true);
        } else {
            this.cAortico.setSelected(false);
        }
        if (this.Corazon.getDiastolicoMitral() == 1){
            this.cMitral.setSelected(true);
        } else {
            this.cMitral.setSelected(false);
        }
        if (this.Corazon.getSinDiastolico() == 1){
            this.cSinDiastolico.setSelected(true);
        } else {
            this.cSinDiastolico.setSelected(false);
        }
        if (this.Corazon.getHepatomegalia() == 1){
            this.cHepatomegalia.setSelected(true);
        } else {
            this.cHepatomegalia.setSelected(false);
        }
        if (this.Corazon.getEsplenomegalia() == 1){
            this.cEsplenomegalia.setSelected(true);
        } else {
            this.cEsplenomegalia.setSelected(false);
        }
        if (this.Corazon.getIngurgitacion() == 1){
            this.cIngurgitacion.setSelected(true);
        } else {
            this.cIngurgitacion.setSelected(false);
        }

        // obtenemos los antecedentes de la tabla de síntomas
        this.Indicios.getDatosSintoma(this.IdVisita);

        // asignamos en la variable de clase
        this.IdSintoma = this.Indicios.getId();

        // asignamos en el formulario
        this.cPrecordial.setSelectedItem(this.Indicios.getPrecordial());
        this.cDisnea.setSelectedItem(this.Indicios.getDisnea());
        this.cPalpitaciones.setSelectedItem(this.Indicios.getPalpitaciones());
        if (this.Indicios.getPresincope() == 1){
            this.cPresincope.setSelected(true);
        } else {
            this.cPresincope.setSelected(false);
        }
        if (this.Indicios.getConciencia() == 1){
            this.cConciencia.setSelected(true);
        } else {
            this.cConciencia.setSelected(false);
        }
        if (this.Indicios.getEdema() == 1){
            this.cEdema.setSelected(true);
        } else {
            this.cEdema.setSelected(false);
        }

        // fija el usuario y la fecha de alta
        this.tUsuario.setText(this.Indicios.getUsuario());
        this.tAlta.setText(this.Indicios.getFecha());

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que verifica
     * los datos del formulario
     */
    private void validaVisita(){

        // declaración de variables
        Boolean Correcto = false;

        // si no ingresó la fecha de la visita
        if (this.Herramientas.fechaJDate(this.dFecha) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Ingrese la fecha de la visita",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;

        }

        // verifica si ingresó el estadio
        if (this.cEstadio.getSelectedItem().toString().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el Estadio",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cEstadio.requestFocus();
			return;

        }

        // si no ingresó la presión
        if (this.tTa.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la presión arterial",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tTa.requestFocus();
			return;

        }

        // la frecuencia fija valor por defecto

        // la saturación fija valor por defecto

        // si no ingresó el peso
        if (this.tPeso.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el peso en kilogramos",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tPeso.requestFocus();
			return;

        }

        // si no ingresó la altura
        if (this.tTalla.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la talla en metros y centímetros",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tTalla.requestFocus();
			return;

        }

        // si no ingresó el índice de masa corporal
        if (this.tBmi.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el índice de masa corporal",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tBmi.requestFocus();
			return;

        }

        // los elementos de tórax los permite en blanco

        // si no indicó el dolor precordial
        if (this.cPrecordial.getSelectedItem().toString().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si presenta dolor precordial",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cPrecordial.requestFocus();
			return;

        }

        // si no indicó la disnea
        if (this.cDisnea.getSelectedItem().toString().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione el tipo de disnea",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cDisnea.requestFocus();
			return;

        }

        // si no indicó las palpitaciones
        if (this.cPalpitaciones.getSelectedItem().toString().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione si presenta palpitaciones",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cPalpitaciones.requestFocus();
			return;

        }

        // síncope, edema de miembros infreriores y pérdida de
        // conciencia lo permite en blanco

        // verifica que halla seleccionado algún item de auscultación
        if (this.cNormal.isSelected()){
            Correcto = true;
        }
        if (this.cRitmo.isSelected()){
            Correcto = true;
        }
        if (this.c3er.isSelected()){
            Correcto = true;
        }
        if (this.c4tor.isSelected()){
            Correcto = true;
        }

        // si no marcó ninguno
        if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione al menos un elemento\n" +
                        "de la auscultación",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // inicializamos la variable control
        Correcto = false;

        // verifica que halla seleccionado algún item de
        // soplos sistólicos
        if (this.cEyectivo.isSelected()){
            Correcto = true;
        }
        if (this.cRegurgitativo.isSelected()){
            Correcto = true;
        }
        if (this.cNoTiene.isSelected()){
            Correcto = true;
        }

        // si no marcó ninguno
        if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione al menos un elemento\n" +
                        "de los soplos sitólicos.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // inicializa la variable control
        Correcto = false;

        // verifica que halla seleccionado algún item de
        // soplos diastólicos
        if (this.cAortico.isSelected()){
            Correcto = true;
        }
        if (this.cMitral.isSelected()){
            Correcto = true;
        }
        if (this.cSinDiastolico.isSelected()){
            Correcto = true;
        }

        // si no marcó ninguno
        if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione al menos un elemento\n" +
                        "de los soplos diastólicos",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // hepatomegalia, esplenomegalia e ingurgitación
        // lo permite en blanco

        // grabamos el registro
        this.grabaVisita();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en el servidor y luego
     * recarga la grilla de visitas del formulario padre
     */
    private void grabaVisita(){

        // primero grabamos en la tabla de visitas y obtenemos
        // la id de la visita
        this.Entradas.setId(this.IdVisita);
        this.Entradas.setFecha(this.Herramientas.fechaJDate(this.dFecha));
        this.Entradas.setPaciente(this.Protocolo);
        this.IdVisita = this.Entradas.grabaVisita();

        // asignamos y grabamos la tabla de clasificacion
        this.Estadio.setId(this.IdAgudo);
        this.Estadio.setPaciente(this.Protocolo);
        this.Estadio.setVisita(this.IdVisita);
        this.Estadio.setEstadio(this.cEstadio.getSelectedItem().toString());
        this.Estadio.setObservaciones(this.tObservaciones.getText());

        // grabamos y obtenemos la id
        this.IdAgudo = this.Estadio.grabaClasificacion();

        // asignamos y grabamos la tabla de examen físico
        this.Examen.setId(this.IdFisico);
        this.Examen.setProtocolo(this.Protocolo);
        this.Examen.setVisita(this.IdVisita);
        this.Examen.setTa(this.tTa.getText());
        this.Examen.setFc(Integer.parseInt(this.sFc.getValue().toString()));
        this.Examen.setPeso(Float.parseFloat(this.tPeso.getText()));
        this.Examen.setTalla(Float.parseFloat(this.tTalla.getText()));
        this.Examen.setBmi(Float.parseFloat(this.tBmi.getText()));
        if (this.cSp.isSelected()){
            this.Examen.setSp(1);
        } else {
            this.Examen.setSp(0);
        }
        if (this.cMvDisminuido.isSelected()){
            this.Examen.setMvdisminuido(1);
        } else {
            this.Examen.setMvdisminuido(0);
        }
        if (this.cCrepitantes.isSelected()){
            this.Examen.setCrepitantes(1);
        } else {
            this.Examen.setCrepitantes(0);
        }
        if (this.cSibilancias.isSelected()){
            this.Examen.setSibilancias(1);
        } else {
            this.Examen.setSibilancias(0);
        }

        // grabamos el registro y obtenemos la id
        this.IdFisico = this.Examen.grabaVisita();

        // asignamos y grabamos la tabla de síntomas
        this.Indicios.setId(this.IdSintoma);
        this.Indicios.setPaciente(this.Protocolo);
        this.Indicios.setVisita(this.IdVisita);
        this.Indicios.setPrecordial(this.cPrecordial.getSelectedItem().toString());
        this.Indicios.setDisnea(this.cDisnea.getSelectedItem().toString());
        this.Indicios.setPalpitaciones(this.cPalpitaciones.getSelectedItem().toString());
        if (this.cPresincope.isSelected()){
            this.Indicios.setPresincope(1);
        } else {
            this.Indicios.setPresincope(0);
        }
        if (this.cConciencia.isSelected()){
            this.Indicios.setConciencia(1);
        } else {
            this.Indicios.setConciencia(0);
        }
        if (this.cEdema.isSelected()){
            this.Indicios.setEdema(1);
        } else {
            this.Indicios.setEdema(0);
        }

        // grabamos y obtenemos la id
        this.IdSintoma = this.Indicios.grabaSintoma();

        // asignamos y grabamos la tabla de cardiovascular
        this.Corazon.setId(this.IdCardio);
        this.Corazon.setPaciente(this.Protocolo);
        this.Corazon.setVisita(this.IdVisita);
        if(this.cNormal.isSelected()){
            this.Corazon.setAuscultacion(1);
        } else {
            this.Corazon.setAuscultacion(0);
        }
        if (this.cRitmo.isSelected()){
            this.Corazon.setIrregular(1);
        } else {
            this.Corazon.setIrregular(0);
        }
        if (this.c3er.isSelected()){
            this.Corazon.setTercer(1);
        } else {
            this.Corazon.setTercer(0);
        }
        if (this.c4tor.isSelected()){
            this.Corazon.setCuarto(1);
        } else {
            this.Corazon.setCuarto(0);
        }
        if (this.cEyectivo.isSelected()){
            this.Corazon.setEyectivo(1);
        } else {
            this.Corazon.setEyectivo(0);
        }
        if (this.cRegurgitativo.isSelected()){
            this.Corazon.setRegurgitativo(1);
        } else {
            this.Corazon.setRegurgitativo(0);
        }
        if (this.cNoTiene.isSelected()){
            this.Corazon.setSinSistolico(1);
        } else {
            this.Corazon.setSinSistolico(0);
        }
        if (this.cAortico.isSelected()){
            this.Corazon.setAortico(1);
        } else {
            this.Corazon.setAortico(0);
        }
        if (this.cMitral.isSelected()){
            this.Corazon.setDiastolicoMitral(1);
        } else {
            this.Corazon.setDiastolicoMitral(0);
        }
        if (this.cSinDiastolico.isSelected()){
            this.Corazon.setSinDiastolico(1);
        } else {
            this.Corazon.setSinDiastolico(0);
        }
        if (this.cHepatomegalia.isSelected()){
            this.Corazon.setHepatomegalia(1);
        } else {
            this.Corazon.setHepatomegalia(0);
        }
        if (this.cEsplenomegalia.isSelected()){
            this.Corazon.setEsplenomegalia(1);
        } else {
            this.Corazon.setEsplenomegalia(0);
        }
        if (this.cIngurgitacion.isSelected()){
            this.Corazon.setIngurtitacion(1);
        } else {
            this.Corazon.setIngurtitacion(0);
        }

        // grabamos el registro y obtenemos la id
        this.IdCardio = this.Corazon.grabaCardiovascular();

        // presenta el mensaje
        new Mensaje("Visita grabada ...");

        // actualiza en el formulario padre
        this.Padre.cargaVisitas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón electrocardiograma
     */
    private void verElectro(){

        // si no grabó el formulario
        if (this.IdVisita == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        // si grabó 
        } else {

            // instanciamos el formulario
            new FormElectro(this, true, this.Protocolo, this.IdVisita);
            
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón ver ecocardiograma
     */
    private void verEcocardiograma(){

        // si no grabó el formulario
        if (this.IdVisita == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        // si grabó 
        } else {

            // instanciamos el formulario
            new FormEcocardio(this, true, this.Protocolo, this.IdVisita);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón holter
     */
    private void verHolter(){

        // si no grabó el formulario
        if (this.IdVisita == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        // si grabó 
        } else {

            // instanciamos el formulario
            new FormHolter(this, true, this.Protocolo, this.IdVisita);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón radiografìas
     */
    private void verRx(){

        // si no grabó el formulario
        if (this.IdVisita == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        // si ya grabó
        } else {

            // instanciamos el formulario
            new FormRx(this, true, this.Protocolo, this.IdVisita);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botòn ergometría
     */
    private void verErgometria(){

        // si no grabó el formulario
        if (this.IdVisita == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        // si ya grabó
        } else {

            // instanciamos la clase de ergometría
            new FormErgometria(this, true, this.Protocolo, this.IdVisita);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar, si
     * está editando, recarga el registro, en otro caso
     * limpia el formulario
     */
    private void cancelaVisita(){

        // si está insertando
        if (this.IdVisita == 0){
            this.limpiaFormulario();
        } else {
            this.verVisita(this.IdVisita);
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia los campos del formulario
     */
    private void limpiaFormulario(){

        // inicializamos los campos
        this.cEstadio.setSelectedItem("");
        this.tTa.setText("");
        this.sFc.setValue(0);
        this.sSpo.setValue(70);
        this.tPeso.setText("");
        this.tTalla.setText("");
        this.tBmi.setText("");
        this.cSp.setSelected(false);
        this.cMvDisminuido.setSelected(false);
        this.cCrepitantes.setSelected(false);
        this.cSibilancias.setSelected(false);
        this.cPrecordial.setSelectedItem("");
        this.cDisnea.setSelectedItem("");
        this.cPalpitaciones.setSelectedItem("");
        this.cPresincope.setSelected(false);
        this.cConciencia.setSelected(false);
        this.cEdema.setSelected(false);
        this.cNormal.setSelected(false);
        this.cRitmo.setSelected(false);
        this.c3er.setSelected(false);
        this.c4tor.setSelected(false);
        this.cEyectivo.setSelected(false);
        this.cRegurgitativo.setSelected(false);
        this.cNoTiene.setSelected(false);
        this.cAortico.setSelected(false);
        this.cMitral.setSelected(false);
        this.cSinDiastolico.setSelected(false);
        this.cHepatomegalia.setSelected(false);
        this.cEsplenomegalia.setSelected(false);
        this.cIngurgitacion.setSelected(false);

        // fija el usuario y la fecha de alta
        this.tUsuario.setText(Seguridad.Usuario);
        this.tAlta.setText(this.Herramientas.FechaActual());

    }

}
