/*

    Nombre: Etiquetas
    Fecha: 29/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que genera la etiqueda de la heladera

 */

// definición del paquete
package heladeras;

//importamos las librerías
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import funciones.Imagenes;
import laboratorios.Laboratorios;
import java.awt.Desktop;
import java.io.*;

// definición de la clase
public class Etiquetas {

	// declaración de variables de clase
	protected int IdHeladera;            // clave del registro
	protected int Laboratorio;           // clave del laboratorio
	protected String Marca;              // marca de la heladera
	protected String Ubicacion;          // ubicación de la heladera
	protected int Temperatura1;          // primer temperatura
	protected int Temperatura2;          // temperatura 2
	protected int Tolerancia;            // tolerancia en grados
	protected String Usuario;            // usuario que ingresó el registro
    protected String Fecha;              // fecha de alta del registro
    protected java.awt.Image Logo;       // logo del laboratorio
	protected Heladeras Freezer;         // objeto de la base de datos
	protected Document documento;        // el documento pdf
    protected PdfWriter docWriter;       // el puntero del documento
    protected PdfContentByte cb;         // puntero al contenido

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idheladera
	 * Constructor de la clase, recibe como parámetro la
	 * clave de la heladera a imprimir
	 */
	public Etiquetas (int idheladera){

		// instanciamos la clase y obtenemos los datos del registro
		this.Freezer = new Heladeras();
		this.Freezer.getDatosHeladera(idheladera);
		this.IdHeladera = idheladera;
		this.Laboratorio = this.Freezer.getLaboratorio();
		this.Marca = this.Freezer.getMarca();
		this.Ubicacion = this.Freezer.getUbicacion();
		this.Temperatura1 = this.Freezer.getTemperatura1();
		this.Temperatura2 = this.Freezer.getTemperatura2();
		this.Tolerancia = this.Freezer.getTolerancia();
		this.Usuario = this.Freezer.getUsuario();
		this.Fecha = this.Freezer.getFechaAlta();

        // obtenemos el logo del laboratorio
        this.getLogo();

        // creamos el documento
        this.documento = new Document(PageSize.A4);

        // obtenemos la ruta de ejecución y configuramos el path
        // del archivo destino
        String archivo = (new File (".").getAbsolutePath ()) + "/temp/etiqueta.pdf";

        // creamos el archivo
        try {
            this.docWriter = PdfWriter.getInstance(this.documento, new FileOutputStream(archivo));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // establecemos las propiedades del documento
        this.documento.addAuthor("Lic. Claudio Invernizzi");
        this.documento.addCreationDate();
        this.documento.addCreator("http://fatalachaben.info.tm");

        // los márgenes van left - right - top - bottom en puntos
        // 72 puntos es 1 pulgada 2,5 cm (superior lo dejamos en
        // 10 porque el encabezado de página ocupa espacio
        this.documento.setMargins(90, 36, 10, 72);

        // abrimos el documento
        this.documento.open();

        // imprimimos el registro
        try {
			this.imprimeFicha();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

        // Cerramos el documento
        this.documento.close();

        // lo mostramos en el visor por defecto
        try {
            File path = new File (archivo);
            Desktop.getDesktop().open(path);
        }catch (IOException ex) {
            ex.printStackTrace();
        }

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir del usuario activo obtiene el logo
     * del laboratorio
     */
    protected void getLogo() {

        // obtenemos el logo del laboratorio
        Laboratorios Instituciones = new Laboratorios();
        Instituciones.getLogoLaboratorio();
        this.Logo = Instituciones.getLogo();

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @throws DocumentException
	 * Método que imprime los datos de la ficha de la heladera
	 */
	protected void imprimeFicha() throws DocumentException{

		// presenta el encabezado
		this.encabezado();

        // agregamos el título
        Paragraph Parrafo = new Paragraph("Registro de Heladeras / Freezers",
                                FontFactory.getFont("arial", 14));
                                Parrafo.setAlignment(Element.ALIGN_CENTER);
        this.documento.add(Parrafo);

        // la clave de la heladera
        Parrafo = new Paragraph("ID: " + Integer.toString(this.IdHeladera),
                      FontFactory.getFont("arial", 12));
                      Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la marca
        Parrafo = new Paragraph("Marca: " + this.Marca,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la ubicación
        Parrafo = new Paragraph("Ubicación: " + this.Ubicacion,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la primer temperatura
        Parrafo = new Paragraph("Primer Temperatura: " + Integer.toString(this.Temperatura1),
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // si hay segunda temperatura
        if (this.Temperatura2 != 0){

        	// la agregamos
            Parrafo = new Paragraph("Segunda Temperatura: " + Integer.toString(this.Temperatura2),
                      FontFactory.getFont("arial", 12));
                      Parrafo.setAlignment(Element.ALIGN_LEFT);
            this.documento.add(Parrafo);

        }

        // la tolerancia
        Parrafo = new Paragraph("Tolerancia: " + Integer.toString(this.Tolerancia),
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // el usuario y la fecha de alta
        Parrafo = new Paragraph("Usuario: " + this.Usuario,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // agrega los códigos qr y de barras
        this.Codigos();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que define una tabla en la que presenta alineados
	 * el logo y los códigos de barras
	 */
	protected void encabezado(){

        try {

            // creamos el título
            Paragraph Parrafo = new Paragraph("Sistema de Control de Stock",
                                    FontFactory.getFont("arial", 14));
                                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);

            // agregamos el título
            Parrafo = new Paragraph("Entrega de Materiales del Depósito",
                                     FontFactory.getFont("arial", 14));
                                     Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);

            // si el laboratorio no tiene logo
            if (this.Logo == null) {

	            // obtenemos la imagen y la escalamos
	            Image logoFatala = Image.getInstance("Graficos/sin_imagen.jpg");
	            logoFatala.scaleToFit(92f, 67f);
	            logoFatala.setAlignment(Chunk.ALIGN_MIDDLE);
	            logoFatala.setAbsolutePosition(10f, 750f);
	            this.documento.add(logoFatala);

	        // si tiene logo
            } else {

            	// obtenemos la ruta temporal del logo y lo grabamos
                String archivo = (new File (".").getAbsolutePath ()) + "/temp/logo.jpg";
                Imagenes logo = new Imagenes();
                logo.guardarImagen(this.Logo, archivo, 92, 67);

				// agrega la imagen por defecto
	            Image archivologo = Image.getInstance(archivo);
	            archivologo.scaleToFit(92f, 67f);
	            archivologo.setAlignment(Chunk.ALIGN_MIDDLE);
	            archivologo.setAbsolutePosition(10f, 750f);
				this.documento.add(archivologo);

            }

            // insertamos un separador
            Parrafo = new Paragraph(" ",
                    FontFactory.getFont("arial", 18));
                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);

            // agregamos el separador
            Image Separador = Image.getInstance("Graficos/separador.png");
            Separador.setAlignment(Chunk.ALIGN_MIDDLE);
            Separador.scaleToFit(1000f, 5f);
            this.documento.add(Separador);

        } catch (BadElementException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException e) {
			e.printStackTrace();
		}

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega al documento los códigos QR y de barras
     */
    protected void Codigos(){

		// instanciamos el puntero al documento
		PdfContentByte cb = this.docWriter.getDirectContent();
		Barcode39 barcode39 = new Barcode39();
		String clave = Integer.toString(this.IdHeladera) + "-" + Integer.toString(this.Laboratorio);

		// creamos el código y lo convertimos a imagen
		barcode39.setCode(clave);
		Image code39Image = barcode39.createImageWithBarcode(cb, null, null);
        code39Image.scaleToFit(92f, 67f);
        code39Image.setAlignment(Chunk.ALIGN_MIDDLE);
        code39Image.setAbsolutePosition(400f, 600f);

		// inicializamos las variables
		Image codeQrImage = null;
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(clave, 1000, 1000, null);

		// creamos la imagen
		try {
			codeQrImage = barcodeQRCode.getImage();
		} catch (BadElementException e) {
			e.printStackTrace();
		}

		// escalamos la imagen y la agregamos al documento
		codeQrImage.scaleAbsolute(100, 100);
		codeQrImage.setAbsolutePosition(400f, 500f);

		// agregamos las imágenes
        try {
			this.documento.add(code39Image);
			this.documento.add(codeQrImage);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}

    }

}
