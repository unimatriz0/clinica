/*

    Nombre: formHeladeras
    Fecha: 21/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario para el abm de heladeras

 */

// definición del paquete
package heladeras;

// importamos las librerías
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class FormHeladeras extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = 8847668543443811381L;

	// definimos las variables
	private JTextField tId;
	private JTextField tMarca;
	private JTextField tUbicacion;
	private JSpinner sTemperatura1;
	private JSpinner sTemperatura2;
	private JSpinner sTolerancia;
	private JTextField tPatrimonio;
	private JTextField tUsuario;
	private JTextField tAlta;
	private JTable tHeladeras;
	private JButton btnGrabar;
	private JButton btnCancelar;
	private Heladeras freezer;

	/**
	 * @author Claudio Invernizzi
	 * Creamos el diálogo.
	 */
	public FormHeladeras(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

        // inicializamos la clase de la base de datos
		this.freezer = new Heladeras();

		// fijamos el tamaño y el layout
		setBounds(100, 100, 723, 372);
		getContentPane().setLayout(null);

		// inicializamos el formulario
		this.initForm();

		// ahora cargamos la grilla
		this.grillaHeladeras();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
    @SuppressWarnings({ "serial" })
	protected void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// agregamos el título
		JLabel lTitulo = new JLabel("Nómina de Heladeras");
        lTitulo.setBounds(12, 12, 209, 26);
        lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// agregamos la id
		JLabel lId = new JLabel("ID:");
        lId.setBounds(12, 40, 32, 26);
        lId.setFont(Fuente);
		getContentPane().add(lId);
		this.tId = new JTextField();
        this.tId.setBounds(35, 40, 26, 26);
        this.tId.setFont(Fuente);
		getContentPane().add(this.tId);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setEditable(false);

		// agregamos la marca
		JLabel lMarca = new JLabel("Marca:");
        lMarca.setBounds(75, 40, 63, 26);
        lMarca.setFont(Fuente);
		getContentPane().add(lMarca);
		this.tMarca = new JTextField();
        this.tMarca.setBounds(125, 40, 200, 26);
        this.tMarca.setFont(Fuente);
		getContentPane().add(this.tMarca);
		this.tMarca.setToolTipText("Marca de la Heladera");
        this.tMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tMarcaActualKeyPressed(evt);
            }
        });

		// agregamos la ubicación
		JLabel lUbicacion = new JLabel("Ubicación:");
        lUbicacion.setBounds(335, 40, 86, 26);
        lUbicacion.setFont(Fuente);
		getContentPane().add(lUbicacion);
		this.tUbicacion = new JTextField();
        this.tUbicacion.setBounds(410, 40, 220, 26);
        this.tUbicacion.setFont(Fuente);
		getContentPane().add(this.tUbicacion);
		this.tUbicacion.setToolTipText("Indique la ubicaciòn de la heladera");
        this.tUbicacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tUbicacionActualKeyPressed(evt);
            }
        });

		// agrega el patrimonio
		JLabel lPatrimonio = new JLabel("Patrimonio:");
        lPatrimonio.setBounds(13, 75, 86, 26);
        lPatrimonio.setFont(Fuente);
		getContentPane().add(lPatrimonio);
		this.tPatrimonio = new JTextField();
        this.tPatrimonio.setBounds(95, 75, 114, 26);
        this.tPatrimonio.setFont(Fuente);
		getContentPane().add(this.tPatrimonio);
		this.tPatrimonio.setToolTipText("Ingrese el registro de patrimonio");
        this.tPatrimonio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tPatrimonioActualKeyPressed(evt);
            }
        });

		// agrega la primer temperatura
		JLabel lTemperatura = new JLabel("1° Temperatura:");
        lTemperatura.setBounds(239, 75, 119, 26);
        lTemperatura.setFont(Fuente);
		getContentPane().add(lTemperatura);
		this.sTemperatura1 = new JSpinner();
        this.sTemperatura1.setBounds(355, 75, 50, 26);
        this.sTemperatura1.setFont(Fuente);
		getContentPane().add(this.sTemperatura1);
		this.sTemperatura1.setToolTipText("Temperatura deseada de la heladera");
        this.sTemperatura1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sTemperatura1ActualKeyPressed(evt);
            }
        });

		// agrega la segunda temperatura
		JLabel lTemperatura2 = new JLabel("2° Temperatura:");
        lTemperatura2.setBounds(417, 75, 132, 26);
        lTemperatura2.setFont(Fuente);
		getContentPane().add(lTemperatura2);
		this.sTemperatura2 = new JSpinner();
        this.sTemperatura2.setBounds(535, 75, 50, 26);
        this.sTemperatura2.setFont(Fuente);
		getContentPane().add(this.sTemperatura2);
		this.sTemperatura2.setToolTipText("Temperatura deseada de la heladera");
        this.sTemperatura2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sTemperatura2ActualKeyPressed(evt);
            }
        });

		// agrega la tolerancia
		JLabel lTolerancia = new JLabel("Tolerancia:");
        lTolerancia.setBounds(13, 110, 86, 26);
        lTolerancia.setFont(Fuente);
		getContentPane().add(lTolerancia);
		this.sTolerancia = new JSpinner();
        this.sTolerancia.setBounds(95, 110, 40, 26);
        this.sTolerancia.setFont(Fuente);
		getContentPane().add(this.sTolerancia);
		this.sTolerancia.setToolTipText("Indique la tolerancia en grados");
        this.sTolerancia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                sToleranciaActualKeyPressed(evt);
            }
        });

		// el usuario
		JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setBounds(164, 110, 70, 26);
        lUsuario.setFont(Fuente);
		getContentPane().add(lUsuario);
		this.tUsuario = new JTextField();
        this.tUsuario.setBounds(225, 110, 85, 26);
        this.tUsuario.setFont(Fuente);
		getContentPane().add(this.tUsuario);
		this.tUsuario.setToolTipText("Usuario que ingresò el registro");
		this.tUsuario.setEditable(false);

		// la fecha de alta
		JLabel lAlta = new JLabel("Alta:");
        lAlta.setBounds(320, 110, 44, 26);
        lAlta.setFont(Fuente);
		getContentPane().add(lAlta);
		this.tAlta = new JTextField();
        this.tAlta.setBounds(355, 110, 85, 26);
        this.tAlta.setFont(Fuente);
		getContentPane().add(this.tAlta);
		this.tAlta.setToolTipText("Fecha de alta del Registro");
		this.tAlta.setEditable(false);

		// el botón grabar
		this.btnGrabar = new JButton("Grabar");
        this.btnGrabar.setBounds(455, 110, 115, 26);
        this.btnGrabar.setFont(Fuente);
		this.btnGrabar.setToolTipText("Pulse para grabar el registro");
		this.getContentPane().add(this.btnGrabar);
		this.btnGrabar.setIcon(new ImageIcon(FormHeladeras.class.getResource("/Graficos/mgrabar.png")));
        this.btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verificaHeladera(evt);
            }
        });

		// el botón cancelar
		this.btnCancelar = new JButton("Cancelar");
        this.btnCancelar.setBounds(576, 110, 115, 26);
        this.btnCancelar.setFont(Fuente);
		this.btnCancelar.setToolTipText("Pulse para reiniciar el formulario");
		getContentPane().add(this.btnCancelar);
		this.btnCancelar.setIcon(new ImageIcon(FormHeladeras.class.getResource("/Graficos/mBorrar.png")));
        this.btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaHeladera(evt);
            }
        });

		// define el scroll
		JScrollPane scrollHeladeras = new JScrollPane();
		scrollHeladeras.setBounds(13, 145, 677, 188);

		// agrega la grilla de heladeras
		this.tHeladeras = new JTable();
		this.tHeladeras.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {"ID",
					      "Marca",
					      "Temp.1",
					      "Temp.2",
					      "Tol.",
					      "Imp.",
					      "Ver",
					      "El."}
		) { @SuppressWarnings("rawtypes")
            Class[] columnTypes = new Class[] {
			   	    Integer.class,
				    String.class,
				    Integer.class,
				    Integer.class,
				    Integer.class,
				    Object.class,
				    Object.class,
				    Object.class
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// agregamos el tooltip
		this.tHeladeras.setToolTipText("Pulse sobre el ícono");

        // fijamos el alto de las filas
        this.tHeladeras.setRowHeight(25);

        // fijamos la fuente
        this.tHeladeras.setFont(Fuente);

        // fijamos el evento click de la tabla
        this.tHeladeras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tHeladerasMouseClicked(evt);
            }
        });

        // fijamos el ancho de las columnas
        this.tHeladeras.getColumn("ID").setPreferredWidth(30);
        this.tHeladeras.getColumn("ID").setMaxWidth(30);
        this.tHeladeras.getColumn("Temp.1").setMaxWidth(60);
        this.tHeladeras.getColumn("Temp.1").setPreferredWidth(60);
        this.tHeladeras.getColumn("Temp.2").setMaxWidth(60);
        this.tHeladeras.getColumn("Temp.2").setPreferredWidth(60);
        this.tHeladeras.getColumn("Tol.").setMaxWidth(60);
        this.tHeladeras.getColumn("Tol.").setPreferredWidth(60);
        this.tHeladeras.getColumn("Imp.").setPreferredWidth(35);
        this.tHeladeras.getColumn("Imp.").setMaxWidth(35);
        this.tHeladeras.getColumn("Ver").setPreferredWidth(35);
        this.tHeladeras.getColumn("Ver").setMaxWidth(35);
        this.tHeladeras.getColumn("El.").setPreferredWidth(35);
        this.tHeladeras.getColumn("El.").setMaxWidth(35);

		// agregamos la tabla al contenddor
		scrollHeladeras.setViewportView(this.tHeladeras);
		getContentPane().add(scrollHeladeras);

        // mostramos el formulario
        this.setVisible(true);
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga en la tabla la nómina de heladeras
	 */
	protected void grillaHeladeras(){

		// sobrecargamos el renderer de la tabla
		this.tHeladeras.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tHeladeras.getModel();

        // hacemos la tabla se pueda ordenar
        this.tHeladeras.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[8];

        // obtenemos la nómina de heladeras
        ResultSet nomina = this.freezer.nominaHeladeras();

        try {

            // nos desplazamos al inicio del resultset
            nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // fijamos los valores de la fila
                fila[0] = nomina.getInt("id");
                fila[1] = nomina.getString("marca");
                fila[2] = nomina.getInt("temperatura1");
                fila[3] = nomina.getInt("temperatura2");
                fila[4] = nomina.getInt("tolerancia");
                fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
                fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

    // evento al pulsar sobre la marca
    private void tMarcaActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tUbicacion.requestFocus();
        }

    }

    // evento al pulsar sobre la ubicacion
    private void tUbicacionActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tPatrimonio.requestFocus();
        }

    }

    // evento al pulsar sobre el patrimonio
    private void tPatrimonioActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.sTemperatura1.requestFocus();
        }

    }

    // evento al pulsar sobre la temperatura 1
    private void sTemperatura1ActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.sTemperatura2.requestFocus();
        }

    }

    // evento al pulsar sobre la temperatura 2
    private void sTemperatura2ActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.sTolerancia.requestFocus();
        }

    }

    // evento al pulsar sobre la tolerancia
    private void sToleranciaActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.btnGrabar.requestFocus();
        }

    }


    /**
     * Mètodo llamado al pulsar sobre el botòn grabar, verifica
     * el estado del formulario antes de enviarlo a la base
     */
    protected void verificaHeladera(java.awt.event.ActionEvent evt){

    	// verifica se halla ingresado el nombre
    	if (this.tMarca.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Ingrese la marca de la heladra", "Error", JOptionPane.ERROR_MESSAGE);
            this.tMarca.requestFocus();
            return;

    	}

    	// verifica se halla ingresado la ubicación
    	if (this.tUbicacion.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique la Ubicación", "Error", JOptionPane.ERROR_MESSAGE);
            this.tUbicacion.requestFocus();
            return;

    	}

    	// verifica se halla ingresado el patrimonio
    	if (this.tPatrimonio.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Ingrese el registro de patrimonio", "Error", JOptionPane.ERROR_MESSAGE);
            this.tPatrimonio.requestFocus();
            return;

    	}

    	// verifica se halla ingreaado la temperatura 1
    	if (this.sTemperatura1.getValue().equals(0)){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique la temperatura esperada", "Error", JOptionPane.ERROR_MESSAGE);
            this.sTemperatura1.requestFocus();
            return;

    	}

    	// la temperatura 2 la permite en blanco

    	// verifica se halla ingresado la tolerancia
    	if(this.sTolerancia.getValue().equals(0)){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique la tolerancia en grados", "Error", JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();
            return;

    	}

    	// graba el registro
    	this.grabaHeladera();

    }

    /**
     * Método llamado luego de verificar el formulario de datos
     * instancia la clase y graba el registro
     */
    protected void grabaHeladera(){

    	// instanciamos la clase
    	freezer = new Heladeras();

    	// si está editando
    	if (!this.tId.getText().isEmpty()){
    		freezer.setIdHeladera(Integer.parseInt(this.tId.getText()));
    	}

    	// fijamos las propiedades
    	freezer.setMarca(this.tMarca.getText());
    	freezer.setUbicacion(this.tUbicacion.getText());
    	freezer.setPatrimonio(this.tPatrimonio.getText());
    	freezer.setTemperatura1((int) this.sTemperatura1.getValue());
    	freezer.setTemperatura2((int) this.sTemperatura2.getValue());
    	freezer.setTolerancia((int) this.sTolerancia.getValue());

    	// grabamos el registro
    	freezer.grabaHeladera();

    	// recargamos la grilla
    	this.grillaHeladeras();

    	// limpiamos el formulario
    	this.nuevaHeladera();

    }

    /**
     * Método llamado al pulsar sobre el botón cancelar, simplemente
     * reinicia el formulario y setea el foco
     */
    protected void cancelaHeladera(java.awt.event.ActionEvent evt){

    	// reiniciamos el formulario
    	this.nuevaHeladera();

    }

    /**
     * Método que limpia el formulario de heladeras
     */
    protected void nuevaHeladera(){

    	// reiniciamos el formulario
    	this.tId.setText("");
    	this.tMarca.setText("");
    	this.tUbicacion.setText("");
    	this.tPatrimonio.setText("");
    	this.sTemperatura1.setValue(0);
    	this.sTemperatura2.setValue(0);
    	this.sTolerancia.setValue(0);
    	this.tUsuario.setText("");
    	this.tAlta.setText("");;

    	// fijamos el foco
    	this.tUbicacion.requestFocus();

    }

    // método llamado al pulsar sobre una celda de la grilla
    protected void tHeladerasMouseClicked(java.awt.event.MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tHeladeras.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tHeladeras.rowAtPoint(evt.getPoint());
        int columna = tHeladeras.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tHeladeras.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id del usuario
            int idHeladera = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 5){
            	this.imprimeHeladera(idHeladera);
            } else if (columna == 6){
            	this.editaHeladera(idHeladera);
            } else if (columna == 7){
            	this.eliminaHeladera(idHeladera);
            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera
     * Método llamado al pulsar sobre el ícono imprimir de la grilla
     */
    protected void imprimeHeladera(int idheladera){

    	// imprimos las etiquetas
    	new Etiquetas(idheladera);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera
     * Método llamado al pulsar el botón editar de la grilla
     */
    protected void editaHeladera(int idheladera){

    	// obtenemos los datos del registro
    	this.freezer.getDatosHeladera(idheladera);

    	// asignamos los valores en la base
    	this.tId.setText(Integer.toString(this.freezer.getIdHeladera()));
    	this.tMarca.setText(this.freezer.getMarca());
    	this.tUbicacion.setText(this.freezer.getUbicacion());
    	this.tPatrimonio.setText(this.freezer.getPatrimonio());
    	this.sTemperatura1.setValue(this.freezer.getTemperatura1());
    	this.sTemperatura2.setValue(this.freezer.getTemperatura2());
    	this.sTolerancia.setValue(this.freezer.getTolerancia());
    	this.tUsuario.setText(this.freezer.getUsuario());
    	this.tAlta.setText(this.freezer.getFechaAlta());

    	// fijamos el foco
    	this.tMarca.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idheladera
     * Método llamado al pulsar sobre el ícono eliminar heladera
     */
    protected void eliminaHeladera(int idheladera){

    	// verificamos que no tenga hijos
    	if (!this.freezer.hijosHeladera(idheladera)){

    		// presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                    "La heladera / freezer tiene lecturas",
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
    		return;

    	}

        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar la Heladera?",
                                   "Tablas Auxiliares",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.freezer.borraHeladera(idheladera);

            // recargamos la grilla
            this.grillaHeladeras();

        }

    }

}
