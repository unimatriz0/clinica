/*

    Nombre: Cardiovascular
    Fecha: 10/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 datos cardiovasculares

*/

// declaración del paquete
package cardiovascular;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Cardiovascular {

    // declaración de variables
    protected Conexion Enlace;        // puntero a la base de datos
    protected int Id;                 // clave del registro
    protected int Paciente;           // clave del paciente
    protected int Visita;             // clave de la visita
    protected int AuscNormal;         // 0 falso 1 verdadero
    protected int Irregular;          // 0 falso 1 verdadero
    protected int Tercer;             // 0 falso 1 verdadero
    protected int Cuarto;             // 0 falso 1 verdadero
    protected int Eyectivo;           // 0 falso 1 verdadero
    protected int Regurgitativo;      // 0 falso 1 verdadero
    protected int SinSistolico;       // 0 falso 1 verdadero
    protected int Aortico;            // 0 falso 1 verdadero
    protected int DiastolicoMitral;   // 0 falso 1 verdadero
    protected int SinDiastolico;      // 0 falso 1 verdadero
    protected int Hepatomegalia;      // 0 falso 1 verdadero
    protected int Esplenomegalia;     // 0 falso 1 verdadero
    protected int Ingurgitacion;      // 0 falso 1 verdadero
    protected int IdUsuario;          // clave del usuario
    protected String Usuario;         // nombre del usuario
    protected String Fecha;           // fecha de alta del registro

    // constructor de la clase
    public Cardiovascular(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.AuscNormal = 0;
        this.Irregular = 0;
        this.Tercer = 0;
        this.Cuarto = 0;
        this.Eyectivo = 0;
        this.Regurgitativo = 0;
        this.SinSistolico = 0;
        this.Aortico = 0;
        this.DiastolicoMitral = 0;
        this.SinDiastolico = 0;
        this.Esplenomegalia = 0;
        this.Hepatomegalia = 0;
        this.Ingurgitacion = 0;
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setAuscultacion(int auscultacion){
        this.AuscNormal = auscultacion;
    }
    public void setIrregular(int irregular){
        this.Irregular = irregular;
    }
    public void setTercer(int tercer){
        this.Tercer = tercer;
    }
    public void setCuarto(int cuarto){
        this.Cuarto = cuarto;
    }
    public void setEyectivo(int eyectivo){
        this.Eyectivo = eyectivo;
    }
    public void setRegurgitativo(int regurgitativo){
        this.Regurgitativo = regurgitativo;
    }
    public void setSinSistolico(int sistolico){
        this.SinSistolico = sistolico;
    }
    public void setAortico(int aortico){
        this.Aortico = aortico;
    }
    public void setDiastolicoMitral(int diastolico){
        this.DiastolicoMitral = diastolico;
    }
    public void setSinDiastolico(int diastolico){
        this.SinDiastolico = diastolico;
    }
    public void setEsplenomegalia(int esplenomegalia){
        this.Esplenomegalia = esplenomegalia;
    }
    public void setHepatomegalia(int hepatomegalia){
        this.Hepatomegalia = hepatomegalia;
    }
    public void setIngurtitacion(int ingurgitacion){
        this.Ingurgitacion = ingurgitacion;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public int getAuscultacion(){
        return this.AuscNormal;
    }
    public int getIrregular(){
        return this.Irregular;
    }
    public int getTercer(){
        return this.Tercer;
    }
    public int getCuarto(){
        return this.Cuarto;
    }
    public int getEyectivo(){
        return this.Eyectivo;
    }
    public int getRegurgitativo(){
        return this.Regurgitativo;
    }
    public int getSinSistolico(){
        return this.SinDiastolico;
    }
    public int getAortico(){
        return this.Aortico;
    }
    public int getDiastolicoMitral(){
        return this.DiastolicoMitral;
    }
    public int getSinDiastolico(){
        return this.SinDiastolico;
    }
    public int getEsplenomegalia(){
        return this.Esplenomegalia;
    }
    public int getHepatomegalia(){
        return this.Hepatomegalia;
    }
    public int getIngurgitacion(){
        return this.Ingurgitacion;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave de un paciente
     * @return resultset con los registros
     * Método que retorna el array de resultados cardiológicos
     * correspondientes a un paciente
     */
    public ResultSet nominaCardiovascular(int idpaciente){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_cardiovascular.id AS id, " +
                   "       diagnostico.v_cardiovascular.paciente AS paciente, " +
                   "       diagnostico.v_cardiovascular.idvisita AS visita, " +
                   "       diagnostico.v_cardiovascular.ausnormal AS ausnormal, " +
                   "       diagnostico.v_cardiovascular.irregular AS irregular, " +
                   "       diagnostico.v_cardiovascular.3er AS 3er, " +
                   "       diagnostico.v_cardiovascular.4to AS 4to, " +
                   "       diagnostico.v_cardiovascular.eyectivo AS eyectivo, " +
                   "       diagnostico.v_cardiovascular.regurgitativo AS regurgitativo, " +
                   "       diagnostico.v_cardiovascular.sinsistolico AS sinsistolico, " +
                   "       diagnostico.v_cardiovascular.aortico AS aortico, " +
                   "       diagnostico.v_cardiovascular.diastolicomitral AS diastolicomitral, " +
                   "       diagnostico.v_cardiovascular.sindiastolico AS sindiastolico, " +
                   "       diagnostico.v_cardiovascular.hepatomegalia AS hepatomegalia, " +
                   "       diagnostico.v_cardiovascular.esplenomegalia AS esplenomegalia, " +
                   "       diagnostico.v_cardiovascular.ingurgitacion AS ingurgitacion, " +
                   "       diagnostico.v_cardiovascular.usuario AS usuario, " +
                   "       diagnostico.v_cardiovascular.fecha AS fecha " +
                   "FROM diagnostico.v_cardiovascular " +
                   "WHERE diagnostico.v_cardiovascular.paciente = '" + idpaciente + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita clave de la visita
     * Método que recibe como parámetro la clave de un
     * registro y asigna en las variables de clase los
     * valores de la entrevista
     */
    public void getDatosCardiovascular(int idvisita){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_cardiovascular.id AS id, " +
                   "       diagnostico.v_cardiovascular.paciente AS paciente, " +
                   "       diagnostico.v_cardiovascular.idvisita AS visita, " +
                   "       diagnostico.v_cardiovascular.ausnormal AS ausnormal, " +
                   "       diagnostico.v_cardiovascular.irregular AS irregular, " +
                   "       diagnostico.v_cardiovascular.3er AS 3er, " +
                   "       diagnostico.v_cardiovascular.4to AS 4to, " +
                   "       diagnostico.v_cardiovascular.eyectivo AS eyectivo, " +
                   "       diagnostico.v_cardiovascular.regurgitativo AS regurgitativo, " +
                   "       diagnostico.v_cardiovascular.sinsistolico AS sinsistolico, " +
                   "       diagnostico.v_cardiovascular.aortico AS aortico, " +
                   "       diagnostico.v_cardiovascular.diastolicomitral AS diastolicomitral, " +
                   "       diagnostico.v_cardiovascular.sindiastolico AS sindiastolico, " +
                   "       diagnostico.v_cardiovascular.hepatomegalia AS hepatomegalia, " +
                   "       diagnostico.v_cardiovascular.esplenomegalia AS esplenomegalia, " +
                   "       diagnostico.v_cardiovascular.ingurgitacion AS ingurgitacion, " +
                   "       diagnostico.v_cardiovascular.usuario AS usuario, " +
                   "       diagnostico.v_cardiovascular.fecha AS fecha " +
                   "FROM diagnostico.v_cardiovascular " +
                   "WHERE diagnostico.v_cardiovascular.paciente = '" + idvisita + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos los valores
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.Visita = Resultado.getInt("visita");
            this.AuscNormal = Resultado.getInt("ausnormal");
            this.Irregular = Resultado.getInt("irregular");
            this.Tercer = Resultado.getInt("3er");
            this.Cuarto = Resultado.getInt("4to");
            this.Eyectivo = Resultado.getInt("eyectivo");
            this.Regurgitativo = Resultado.getInt("regurgitativo");
            this.SinSistolico = Resultado.getInt("sinsistolico");
            this.Aortico = Resultado.getInt("aortico");
            this.DiastolicoMitral = Resultado.getInt("diastolicomitral");
            this.SinDiastolico = Resultado.getInt("sindiastolico");
            this.Hepatomegalia = Resultado.getInt("hepatomegalia");
            this.Esplenomegalia = Resultado.getInt("esplenomegalia");
            this.Ingurgitacion = Resultado.getInt("ingurgitacion");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la id del registro
     * afectado
     */
    public int grabaCardiovascular(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoCardiovascular();
        } else {
            this.editaCardiovascular();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo registro
     */
    protected void nuevoCardiovascular(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.cardiovascular " +
                   "            (paciente, " +
                   "             visita, " +
                   "             ausnormal, " +
                   "             irregular, " +
                   "             3er, " +
                   "             4to, " +
                   "             eyectivo, " +
                   "             regurgitativo, " +
                   "             sinsistolico, " +
                   "             aortico, " +
                   "             diastolicomitral, " +
                   "             sindiastolico, " +
                   "             hepatomegalia, " +
                   "             esplenomegalia, " +
                   "             ingurgitacion, " +
                   "             usuario) " +
                   "            VALUES " +
                   "            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setInt(3,    this.AuscNormal);
            psInsertar.setInt(4,    this.Irregular);
            psInsertar.setInt(5,    this.Tercer);
            psInsertar.setInt(6,    this.Cuarto);
            psInsertar.setInt(7,    this.Eyectivo);
            psInsertar.setInt(8,    this.Regurgitativo);
            psInsertar.setInt(9,    this.SinSistolico);
            psInsertar.setInt(10,   this.Aortico);
            psInsertar.setInt(11,   this.DiastolicoMitral);
            psInsertar.setInt(12,   this.SinDiastolico);
            psInsertar.setInt(13,    this.Hepatomegalia);
            psInsertar.setInt(14,    this.Esplenomegalia);
            psInsertar.setInt(15,    this.Ingurgitacion);
            psInsertar.setInt(16,    this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición del
     * registro activo
     */
    protected void editaCardiovascular(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.cardiovascular SET " +
                   "       ausnormal = ?, " +
                   "       irregular = ?, " +
                   "       3er = ?, " +
                   "       4to = ?, " +
                   "       eyectivo = ?, " +
                   "       regurgitativo = ?, " +
                   "       sinsistolico = ?, " +
                   "       aortico = ?, " +
                   "       diastolicomitral = ?, " +
                   "       sindiastolico = ?, " +
                   "       hepatomegalia = ?, " +
                   "       esplenomegalia = ?, " +
                   "       ingurgitacion = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.cardiovascular.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.AuscNormal);
            psInsertar.setInt(2, this.Irregular);
            psInsertar.setInt(3, this.Tercer);
            psInsertar.setInt(4, this.Cuarto);
            psInsertar.setInt(5, this.Eyectivo);
            psInsertar.setInt(6, this.Regurgitativo);
            psInsertar.setInt(7, this.SinSistolico);
            psInsertar.setInt(8, this.Aortico);
            psInsertar.setInt(9, this.DiastolicoMitral);
            psInsertar.setInt(10,this.SinDiastolico);
            psInsertar.setInt(11, this.Hepatomegalia);
            psInsertar.setInt(12, this.Esplenomegalia);
            psInsertar.setInt(13, this.Ingurgitacion);
            psInsertar.setInt(14, this.IdUsuario);
            psInsertar.setInt(15, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idcardio entero con la clave del registro
     * Método que ejecuta la consulta de eliminación de una
     * entrevista
     */
    public void borraCardivascular(int idcardio){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.cardiovascular " +
                   "WHERE diagnostico.cardiovascular = '" + idcardio + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}
