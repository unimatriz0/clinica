/*

    Nombre: Transfusiones
    Fecha: 12/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla
                 de transfusiones, cada paciente puede tener varias
                 entradas en esta tabla

*/

 // declaración del paquete
package transfusiones;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Transfusiones {

    // declaración de variables de clase
    protected Conexion Enlace;               // puntero a la base de datos
    protected int Id;                        // clave del registro
    protected int IdProtocolo;               // clave del protocolo
    protected String FechaTransfusion;       // fecha de la transfusion
    protected String Motivo;                 // motivo de la transfusion
    protected int IdUsuario;                 // clave del usuario
    protected String Usuario;                // nombre del usuario
    protected String Fecha;                  // fecha de alta del registro

    // constuctor de la clase
    public Transfusiones(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.IdProtocolo = 0;
        this.FechaTransfusion = "";
        this.Motivo = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setIdProtocolo(int protocolo){
        this.IdProtocolo = protocolo;
    }
    public void setFechaTransfusion(String fecha){
        this.FechaTransfusion = fecha;
    }
    public void setMotivo(String motivo){
        this.Motivo = motivo;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getIdProtocolo(){
        return this.IdProtocolo;
    }
    public String getFechaTransfusion(){
        return this.FechaTransfusion;
    }
    public String getMotivo(){
        return this.Motivo;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idprotocolo - clave del paciente
     * @return vector con los registros encontrados
     * Método que recibe como parámetro la clave del paciente
     * y retorna un vector con la lista de transfusiones
     * realizadas
     */
    public ResultSet nominaTransfusiones(int idprotocolo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_transfusiones.id_transfusion AS id, " +
                   "       diagnostico.v_transfusiones.id_protocolo AS idprotocolo, " +
                   "       diagnostico.v_transfusiones.fecha_transfusion AS fecha, " +
                   "       diagnostico.v_transfusiones.motivo AS motivo, " +
                   "       diagnostico.v_transfusiones.usuario AS usuario " +
                   "FROM diagnostico.v_transfusiones " +
                   "WHERE diagnostico.v_transfusiones.id_protocolo = '" + idprotocolo + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_transfusiones.fecha_transfusion, '%d/%m/%Y') DESC; ";

        // obtenemos el vector y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtransfusion - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores del mismo
     */
    public void getDatosTransfusion(int idtransfusion){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_transfusiones.id_transfusion AS id, " +
                   "       diagnostico.v_transfusiones.id_protocolo AS idprotocolo, " +
                   "       diagnostico.v_transfusiones.fecha_transfusion AS fechatransfusion, " +
                   "       diagnostico.v_transfusiones.motivo AS motivo, " +
                   "       diagnostico.v_transfusiones.usuario AS usuario, " +
                   "       diagnostico.v_transfusiones.fecha_alta AS fecha " +
                   "FROM diagnostico.v_transfusiones " +
                   "WHERE diagnostico.v_transfusiones.id_protocolo = '" + idtransfusion + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            // los valores an las variables de clase
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.IdProtocolo = Resultado.getInt("idprotocolo");
            this.FechaTransfusion = Resultado.getString("fechatransfusion");
            this.Motivo = Resultado.getString("motivo");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return clave del registro
     * Método que llama la consulta de actualización o inserción
     * según corresponda y retorna la clave del registro afectado
     */
    public int grabaTransfusion(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaTransfusion();
        } else {
            this.editaTransfusion();
        }

        // retorna la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaTransfusion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.transfusiones " +
                   "       (id_protocolo, " +
                   "        fecha_transfusion, " +
                   "        motivo, " +
                   "        id_usuario) " +
                   "       VALUES " +
                   "       (?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.IdProtocolo);
            psInsertar.setString(2, this.FechaTransfusion);
            psInsertar.setString(3, this.Motivo);
            psInsertar.setInt(4, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaTransfusion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.transfusiones SET " +
                   "       fecha_transfusion = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       motivo = ?, " +
                   "       id_usuario = ? " +
                   "WHERE diagnostico.transfusiones.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.FechaTransfusion);
            psInsertar.setString(2, this.Motivo);
            psInsertar.setInt(3, this.IdUsuario);
            psInsertar.setInt(4, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtransfusion - clave del registro
     * Método recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     */
    public void borraTransfusion(int idtransfusion){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.transfusiones " +
                   "WHERE diagnostico.transfusiones.id = '" + idtransfusion + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}
