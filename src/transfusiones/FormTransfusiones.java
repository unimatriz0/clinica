/*

    Nombre: FormTransfusiones
    Fecha: 20/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el abm
	             de transfusiones del paciente

 */

// definición del paquete
package transfusiones;

// importamos las librerías
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import com.toedter.calendar.JDateChooser;
import seguridad.Seguridad;
import java.awt.Font;
import funciones.RendererTabla;
import funciones.Utilidades;
import transfusiones.Transfusiones;

// definición de la clase
public class FormTransfusiones extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = 4793119109639734311L;

	// definición de las variables de clase
	private JTextField tId;                 // clave del registro
	private JDateChooser dFecha;            // fecha de la transfusión
	private JTextField tMotivo;             // motivo de la transfusión
	private JTextField tUsuario;            // nombre del usuario
	private JTable tTransfusion;            // tabla con los datos
	public int Protocolo;                   // protocolo del paciente
	private Utilidades Herramientas;        // funciones de fecha
	private Transfusiones Hematologia;      // funciones de la base de datos

	// constructor de la clase
	@SuppressWarnings("serial")
	public FormTransfusiones(Frame parent, boolean modal) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// inicializamos las variables
		this.Protocolo = 0;
		this.Herramientas = new Utilidades();
		this.Hematologia = new Transfusiones();

		// fijamos las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Transfusiones Recibidas");
		this.setBounds(150, 150, 594, 300);
		this.setLayout(null);

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Transfusiones del Paciente</b></html>");
		lTitulo.setBounds(10, 10, 436, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// presenta la id
		this.tId = new JTextField();
		this.tId.setBounds(10, 40, 70, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Número de protocolo");
		this.tId.setEditable(false);
		this.add(this.tId);

		// pide la fecha
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(90, 40, 120, 26);
		this.dFecha.setFont(Fuente);
		this.dFecha.setToolTipText("Indique la fecha de la transfusión");
		this.add(this.dFecha);

		// pide el motivo
		this.tMotivo = new JTextField();
		this.tMotivo.setBounds(220, 40, 225, 26);
		this.tMotivo.setFont(Fuente);
		this.tMotivo.setToolTipText("Motivo de la transfusión");
		this.add(this.tMotivo);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(450, 40, 90, 26);
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.add(this.tUsuario);

		// fijamos el nombre
		this.tUsuario.setText(Seguridad.Usuario);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(545, 40, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				validaTransfusion();
			}
		});
		this.add(btnGrabar);

		// define el scroll
		JScrollPane scrollTabla = new JScrollPane();
		scrollTabla.setBounds(10, 70, 570, 190);
		this.add(scrollTabla);

		// define la tabla
		this.tTransfusion = new JTable();
		this.tTransfusion.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, "" }, },
				new String[] { "ID",
							   "Fecha",
							   "Motivo",
							   "Usuario",
							   "Ed.",
							   "El." }) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
					Integer.class,
					String.class,
					String.class,
					String.class,
					Object.class,
					Object.class };

			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// establecemos el ancho de las columnas
		this.tTransfusion.getColumn("ID").setPreferredWidth(30);
		this.tTransfusion.getColumn("ID").setMaxWidth(30);
		this.tTransfusion.getColumn("Fecha").setPreferredWidth(100);
		this.tTransfusion.getColumn("Fecha").setMaxWidth(100);
		this.tTransfusion.getColumn("Usuario").setPreferredWidth(100);
		this.tTransfusion.getColumn("Usuario").setMaxWidth(100);
		this.tTransfusion.getColumn("Ed.").setPreferredWidth(35);
		this.tTransfusion.getColumn("Ed.").setMaxWidth(35);
		this.tTransfusion.getColumn("El.").setPreferredWidth(35);
		this.tTransfusion.getColumn("El.").setMaxWidth(35);

		// establecemos el tooltip
		this.tTransfusion.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tTransfusion.setRowHeight(25);

		// establecemos la fuente
		this.tTransfusion.setFont(Fuente);

		// agregamos la tabla al scroll
		scrollTabla.setViewportView(this.tTransfusion);
		;

		// agregamos el evento de la tabla
		this.tTransfusion.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tTransfusionMouseClicked(evt);
			}
		});

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método público que carga la grilla con las transfusiones
	 * recibidas por el paciente
	 */
	public void cargaTransfusiones() {

		// obtenemos la nómina
		ResultSet Nomina = this.Hematologia.nominaTransfusiones(this.Protocolo);

		// sobrecargamos el renderer de la tabla
		this.tTransfusion.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tTransfusion.getModel();

		// hacemos la tabla se pueda ordenar
		this.tTransfusion.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[6];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("fecha");
				fila[2] = Nomina.getString("motivo");
				fila[3] = Nomina.getString("usuario");
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que valida el formulario antes de enviarlo
	 * al servidor
	 */
	private void validaTransfusion(){

		// verifica se halla ingresado la fecha
		if (this.Herramientas.fechaJDate(this.dFecha) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la fecha de la transfusión",
                        "Error",
						JOptionPane.ERROR_MESSAGE);
			this.dFecha.requestFocus();
			return;

		}

		// verifica se halla indicado el motivo
		if (this.tMotivo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el motivo de la transfusión",
                        "Error",
						JOptionPane.ERROR_MESSAGE);
			this.tMotivo.requestFocus();
			return;

		}

		// grabamos el registro
		this.grabaTransfusion();
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que graba en la base luego de validar el
	 * formulario
	 */
	private void grabaTransfusion(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Hematologia.setId(0);
		} else {
			this.Hematologia.setId(Integer.parseInt(this.tId.getText()));
		}

		// el protocolo
		this.Hematologia.setIdProtocolo(this.Protocolo);

		// fija el resto de los campos
		this.Hematologia.setFechaTransfusion(this.Herramientas.fechaJDate(this.dFecha));
		this.Hematologia.setMotivo(this.tMotivo.getText());

		// grabamos el registro
		this.Hematologia.grabaTransfusion();

		// limpiamos el formulario
		this.limpiaFormulario();

		// recargamos la grilla
		this.cargaTransfusiones();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave entero con la clave del registro
	 * Mètodo que recibe como parámetro la clave del registro
	 * y lo muestra en el formulario de datos
	 */
	private void verTransfusion(int clave){

		// obtenemos el registro
		this.Hematologia.getDatosTransfusion(clave);

		// cargamos el formulario
		this.tId.setText(Integer.toString(this.Hematologia.getId()));
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Hematologia.getFechaTransfusion()));
		this.tMotivo.setText(this.Hematologia.getMotivo());
		this.tUsuario.setText(this.Hematologia.getUsuario());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Mètodo que luego de pedir confirmación ejecuta la
	 * consulta de eliminación
	 */
	private void borraTransfusion(int clave){

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
		                            "Está seguro que desea eliminar el registro?",
									"Transfusiones del Paciente",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Hematologia.borraTransfusion(clave);

			// limpiamos el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaTransfusiones();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario luego de ejecutar
	 * una edición o una eliminación
	 */
	private void limpiaFormulario(){

		// limpiamos los campos
		this.tId.setText("");
		this.dFecha.setDate(null);
		this.tMotivo.setText("");
		this.tUsuario.setText(Seguridad.Usuario);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de transfusiones
	 */
	protected void tTransfusionMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tTransfusion.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tTransfusion.rowAtPoint(evt.getPoint());
        int columna = tTransfusion.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tTransfusion.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id indec del registro
            int id = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 4){
            	this.verTransfusion(id);
            } else if (columna == 5){
            	this.borraTransfusion(id);
            }

		}

	}

}
