/*

    Nombre: Antecedentes
    Fecha: 09/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 antecedentes tóxicos del paciente

*/

// definición del paquete
package antecedentes;

// importamos las librerías
// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// definición de la clase
class Antecedentes {

    // definición de las variables de clase
    protected Conexion Enlace;            // puntero a la base de datos
    protected int Id;                     // clave del registro
    protected int Paciente;               // clave del paciente
    protected int Fuma;                   // nro de cigarrillos
    protected int Alcohol;                // litros de alcohol
    protected String Bebida;              // tipo de bebida
    protected String Adicciones;          // adicciones del paciente
    protected int IdUsuario;              // clave del usuario
    protected String Usuario;             // nombre del usuario
    protected String Fecha;               // fecha de alta

    // constructor de la clase
    public Antecedentes(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.initAntecedentes();

    }

    // método que inicializa las variables de clase
    private void initAntecedentes(){

        // inicializamos las variables
        this.Id = 0;
        this.Paciente = 0;
        this.Fuma = 0;
        this.Alcohol = 0;
        this.Bebida = "";
        this.Adicciones = "";
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setFuma(int fuma){
        this.Fuma = fuma;
    }
    public void setAlcohol(int alcohol){
        this.Alcohol = alcohol;
    }
    public void setBebida(String bebida){
        this.Bebida = bebida;
    }
    public void setAdicciones(String adicciones){
        this.Adicciones = adicciones;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getFuma(){
        return this.Fuma;
    }
    public int getAlcohol(){
        return this.Alcohol;
    }
    public String getBebida(){
        return this.Bebida;
    }
    public String getAdicciones(){
        return this.Adicciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mëtodo que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los valores
     */
    public void getDatosAntecedente(int id){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_antecedentes.id AS id, " +
                   "       diagnostico.v_antecedentes.paciente AS paciente, " +
                   "       diagnostico.v_antecedentes.fuma AS fuma, " +
                   "       diagnostico.v_antecedentes.alcohol AS alcohol, " +
                   "       diagnostico.v_antecedentes.bebida AS bebida, " +
                   "       diagnostico.v_antecedentes.adicciones AS adicciones, " +
                   "       diagnostico.v_antecedentes.usuario AS usuario, " +
                   "       diagnostico.v_antecedentes.fecha AS fecha " +
                   "FROM diagnostico.v_antecedentes " +
                   "WHERE diagnostico.v_antecedentes.id = '" + id + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            if (Resultado.next()){

                // asignamos los valores
                this.Id = Resultado.getInt("id");
                this.Paciente = Resultado.getInt("paciente");
                this.Fuma = Resultado.getInt("fuma");
                this.Alcohol = Resultado.getInt("alcohol");
                this.Bebida = Resultado.getString("bebida");
                this.Adicciones = Resultado.getString("adicciones");
                this.Usuario = Resultado.getString("usuario");
                this.Fecha = Resultado.getString("fecha");

            // si no encontró el registro
            } else {

                // inicializamos las variables
                this.initAntecedentes();

            }
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id
     * Método que ejecuta la consulta de edición o
     * inserción según corresponda y retorna la clave
     * del registro afectado
     */
    public int grabaAntecedente(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoAntecedente();
        } else {
            this.editaAntecedente();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoAntecedente(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.antecedentes " +
                   "       (paciente, " +
                   "        fuma, " +
                   "        alcohol, " +
                   "        bebida, " +
                   "        adicciones, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Paciente);
            psInsertar.setInt(2, this.Fuma);
            psInsertar.setInt(3, this.Alcohol);
            psInsertar.setString(4, this.Bebida);
            psInsertar.setString(5, this.Adicciones);
            psInsertar.setInt(6, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            ex.printStackTrace();

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaAntecedente(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.antecedentes SET " +
                   "       fuma = ?, " +
                   "       alcohol = ?, " +
                   "       bebida = ?, " +
                   "       adicciones = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.antecedentes.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Fuma);
            psInsertar.setInt(2, this.Alcohol);
            psInsertar.setString(3, this.Bebida);
            psInsertar.setString(4, this.Adicciones);
            psInsertar.setInt(5, this.IdUsuario);
            psInsertar.setInt(6, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            ex.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idantecedente clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     */
    public void borraAntecedente(int idantecedente){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.antecedentes " +
                   "WHERE diagnostico.antecedentes.id = '" + idantecedente + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}