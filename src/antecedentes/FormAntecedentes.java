/*

    Nombre: FormAntecedentes
    Fecha: 22/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario de antecedentes y
	             síntomas del paciente

 */

// definición delk paquete
package antecedentes;

// importamos las librerías
import contenedor.FormContenedor;
import visitas.FormVisitas;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import javax.swing.JTable;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import funciones.Utilidades;
import seguridad.Seguridad;
import funciones.Mensaje;
import funciones.RendererTabla;
import antecedentes.Antecedentes;
import agudo.Agudo;
import disautonomia.Disautonomia;
import digestivo.Digestivo;
import visitas.Visitas;

// definición de la clase
public class FormAntecedentes extends javax.swing.JPanel {

    // define el serial id
    private static final long serialVersionUID = 1L;

    // variables de clase
    private JSpinner cAlcohol;
    private JCheckBox cAstenia;
    private JComboBox<Object> cBebida;
    private JCheckBox cBolo;
    private JCheckBox cBradicardia;
    private JSpinner cCigarrillos;
    private JCheckBox cConstipacion;
    private JCheckBox cDisfagia;
    private JCheckBox cHipotension;
    private JCheckBox cNoTiene;
    private JCheckBox cPirosis;
    private JCheckBox cRegurgitacion;
    private JCheckBox cSinDigestivo;
    private JCheckBox rAgudo;
    private JTextArea tAdicciones;
    private JTextField tAlta;
    private JTextArea tObservaciones;
    private JTextField tUsuario;
    private JTable tVisitas;
    private FormContenedor Contenedor; // frame principal

    // definición de las variables de clase
    private int Protocolo;              // protocolo del paciente
    private int IdPatologico;           // clave de antecedentes agudos
    private int IdDisautonomia;         // clave del registro de disautonomia
    private int IdAntecedentes;         // clave de antecedentes tóxicos
    private int IdDigestivo;            // clave del compromiso digestivo
    private Utilidades Herramientas;    // funciones de fecha
    private Disautonomia Autonomia;     // clase de la base de datos
    private Antecedentes Toxicos;       // clase de la base de datos
    private Digestivo Estomago;         // clase de la base de datos
    private Agudo Patologicos;          // clase de la base de datos
    private Visitas Entradas;           // clase de la base de datos

    // constructor de la clase
    public FormAntecedentes(FormContenedor contenedor) {

        // fijamos el contenedor
        this.Contenedor = contenedor;

        // inicializamos las variables y las clases
        this.Protocolo = 0;
        this.Herramientas = new Utilidades();
        this.Autonomia = new Disautonomia();
        this.Toxicos = new Antecedentes();
        this.Estomago = new Digestivo();
        this.Patologicos = new Agudo();
        this.Entradas = new Visitas();
        this.IdPatologico = 0;
        this.IdDisautonomia = 0;
        this.IdAntecedentes = 0;
        this.IdDigestivo = 0;

        // inicializa el formulario
        initComponents();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario
     */
    @SuppressWarnings("serial")
    private void initComponents() {

        // fija las propiedades del panel
        this.setSize(920, 580);
        this.setLayout(null);

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // instanciamos las clases e inicializamos las variables
        this.Protocolo = 0;
        this.Herramientas = new Utilidades();

        // presenta el título
        JLabel lAntecedentes = new JLabel("Antecedentes Patológicos");
        lAntecedentes.setBounds(10, 10, 250, 26);
        lAntecedentes.setFont(Fuente);
        this.add(lAntecedentes);

        // presenta el check de chagas agudo
        this.rAgudo = new JCheckBox("Antecedentes de Chagas Agudo");
        this.rAgudo.setBounds(10, 45, 270, 26);
        this.rAgudo.setFont(Fuente);
        this.rAgudo.setToolTipText("Marque si presenta antecedentes");
        this.add(this.rAgudo);

        // presenta el label de antecedentes tóxicos
        JLabel lToxicos = new javax.swing.JLabel("Antecedentes Tóxicos");
        lToxicos.setFont(Fuente);
        lToxicos.setBounds(320, 10, 160, 26);
        this.add(lToxicos);

        // pide la cantidad de cigarrillos diarios
        JLabel lFuma = new javax.swing.JLabel("Fuma:");
        lFuma.setBounds(320, 45, 40, 26);
        lFuma.setFont(Fuente);
        lFuma.setFont(Fuente);
        this.add(lFuma);
        this.cCigarrillos = new javax.swing.JSpinner();
        this.cCigarrillos.setBounds(360, 45, 50, 26);
        this.cCigarrillos.setFont(Fuente);
        this.cCigarrillos.setToolTipText("Indique la cantidad de cigarrillos diarios");
        this.add(this.cCigarrillos);
        JLabel lCigarrillos = new javax.swing.JLabel("Cigarrillos por día");
        lCigarrillos.setBounds(415, 45, 110, 26);
        lCigarrillos.setFont(Fuente);
        this.add(lCigarrillos);

        // pide la cantidad de alcohol
        JLabel lAlcohol = new JLabel("Alcohol:");
        lAlcohol.setBounds(530, 45, 50, 26);
        lAlcohol.setFont(Fuente);
        this.add(lAlcohol);
        this.cAlcohol = new JSpinner();
        this.cAlcohol.setBounds(580, 45, 50, 26);
        this.cAlcohol.setFont(Fuente);
        cAlcohol.setToolTipText("Indique los litros a la semana que bebe");
        this.add(this.cAlcohol);
        JLabel lLitros = new JLabel("litros a la semana");
        lLitros.setBounds(640, 45, 120, 26);
        lLitros.setFont(Fuente);
        this.add(lLitros);

        // pide el tipo de bebica
        this.cBebida = new JComboBox<>();
        this.cBebida.setBounds(755, 45, 120, 26);
        this.cBebida.setFont(Fuente);
        this.cBebida.setToolTipText("Seleccione de la lista el tipo de bebida");
        this.add(this.cBebida);

        // agrega los elementos al combo
        this.elementosBebida();

        // pide las adicciones
        JLabel lAdicciones = new JLabel("Adicciones:");
        lAdicciones.setBounds(320, 80, 80, 26);
        lAdicciones.setFont(Fuente);
        this.add(lAdicciones);
        JScrollPane scrollAdicciones = new JScrollPane();
        scrollAdicciones.setBounds(400, 80, 470, 70);
        this.tAdicciones = new JTextArea();
        this.tAdicciones.setColumns(20);
        scrollAdicciones.setViewportView(this.tAdicciones);
        this.add(scrollAdicciones);

        // el título de la disautonomia
        JLabel lDisautonomia = new JLabel("Disautonomía");
        lDisautonomia.setBounds(10, 160, 100, 26);
        lDisautonomia.setFont(Fuente);
        this.add(lDisautonomia);

        // presenta el check de hipotensión
        this.cHipotension = new JCheckBox("Hipotensión");
        this.cHipotension.setBounds(120, 160, 120, 26);
        this.cHipotension.setFont(Fuente);
        this.add(this.cHipotension);

        // presenta el check de bradicardia
        this.cBradicardia = new JCheckBox("Bradicardia");
        this.cBradicardia.setBounds(240, 160, 130, 26);
        this.cBradicardia.setFont(Fuente);
        this.add(this.cBradicardia);

        // presenta el check de astenia
        this.cAstenia = new JCheckBox("Astenia y/o Fatigabilidad");
        this.cAstenia.setBounds(370, 160, 210, 26);
        this.cAstenia.setFont(Fuente);
        this.add(this.cAstenia);

        // presenta el check de no tiene
        this.cNoTiene = new JCheckBox("No Tiene");
        this.cNoTiene.setBounds(580, 160, 88, 26);
        this.cNoTiene.setFont(Fuente);
        this.cNoTiene.setText("No Tiene");
        this.add(this.cNoTiene);

        // el título de aparato digestivo
        JLabel lDigestivo = new JLabel("Compromiso Digestivo");
        lDigestivo.setBounds(10, 195, 170, 26);
        lDigestivo.setFont(Fuente);
        this.add(lDigestivo);

        // el check de disfagia
        this.cDisfagia = new JCheckBox("Disfagia");
        this.cDisfagia.setBounds(180, 195, 90, 26);
        this.cDisfagia.setFont(Fuente);
        this.add(this.cDisfagia);

        // el check de pirosis
        this.cPirosis = new JCheckBox("Pirosis");
        this.cPirosis.setBounds(270, 195, 100, 26);
        this.cPirosis.setFont(Fuente);
        this.add(this.cPirosis);

        // el check de regurgitación
        this.cRegurgitacion = new JCheckBox("Regurgitación");
        this.cRegurgitacion.setBounds(370, 195, 130, 26);
        this.cRegurgitacion.setFont(Fuente);
        this.add(this.cRegurgitacion);

        // el check de constipación
        this.cConstipacion = new JCheckBox("Constipación");
        this.cConstipacion.setBounds(500, 195, 120, 26);
        this.cConstipacion.setFont(Fuente);
        this.add(this.cConstipacion);

        // el check de bolo fecal
        this.cBolo = new JCheckBox("Bolo Fecal");
        this.cBolo.setBounds(630, 195, 130, 26);
        this.cBolo.setFont(Fuente);
        this.add(this.cBolo);

        // el check sin antecedentes
        this.cSinDigestivo = new JCheckBox("No Tiene");
        this.cSinDigestivo.setBounds(780, 195, 100, 26);
        this.cSinDigestivo.setFont(Fuente);
        this.add(this.cSinDigestivo);

        // pide las observaciones y comentarios
        JLabel lObservaciones = new JLabel("Observaciones");
        lObservaciones.setBounds(10, 230, 110, 26);
        lObservaciones.setFont(Fuente);
        this.add(lObservaciones);
        JScrollPane scrollObservaciones = new JScrollPane();
        scrollObservaciones.setBounds(10, 265, 540, 78);
        this.tObservaciones = new JTextArea();
        this.tObservaciones.setFont(Fuente);
        this.tObservaciones.setColumns(20);
        scrollObservaciones.setViewportView(this.tObservaciones);
        this.add(scrollObservaciones);

        // el nombre del usuario
        JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setBounds(570, 260, 60, 26);
        lUsuario.setFont(Fuente);
        lUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lUsuario);
        this.tUsuario = new JTextField();
        this.tUsuario.setBounds(640, 260, 90, 26);
        this.tUsuario.setFont(Fuente);
        this.tUsuario.setToolTipText("Usuario que ingresó el registro");
        this.add(this.tUsuario);

        // fijamos el usuario
        this.tUsuario.setText(Seguridad.Usuario);

        // la fecha de alta
        JLabel lAlta = new JLabel("Alta:");
        lAlta.setBounds(570, 295, 60, 26);
        lAlta.setFont(Fuente);
        lAlta.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        this.add(lAlta);
        this.tAlta = new JTextField();
        this.tAlta.setBounds(640, 295, 90, 26);
        this.tAlta.setFont(Fuente);
        this.add(this.tAlta);

        // fijamos la fecha de alta
        this.tAlta.setText(this.Herramientas.FechaActual());

        // el botón grabar
        JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setBounds(750, 260, 110, 26);
        btnGrabar.setFont(Fuente);
        btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.setToolTipText("Pulse para grabar el registro");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaAntecedentes();
            }
        });
        this.add(btnGrabar);

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(750, 295, 110, 26);
        btnCancelar.setFont(Fuente);
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                reiniciaAntecedentes();
            }
        });
        this.add(btnCancelar);

        // la tabla con las visitas
        JLabel lVisitas = new JLabel("Visitas");
        lVisitas.setBounds(10, 370, 160, 26);
        lVisitas.setFont(Fuente);
        this.add(lVisitas);

        // definimos el scroll
        JScrollPane scrollVisitas = new JScrollPane();
        scrollVisitas.setBounds(10, 400, 850, 160);
        this.add(scrollVisitas);

        // definimos la tabla
        this.tVisitas = new javax.swing.JTable();

        // fijamos las propiedades de la tabla
        tVisitas.setModel(new DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID",
                "Clasificacion",
                "Peso",
                "Ausc.",
                "Soplos",
                "Fecha",
                "Usuario",
                "Ed.",
                "El."
            }
        ) {
            @SuppressWarnings("rawtypes")
            Class[] types = new Class [] {
                java.lang.Integer.class,
                java.lang.String.class,
                java.lang.Float.class,
                java.lang.Boolean.class,
                java.lang.Boolean.class,
                java.lang.String.class,
                java.lang.String.class,
                java.lang.Object.class,
                java.lang.Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        // fijamos el ancho de las columnas
		this.tVisitas.getColumn("ID").setPreferredWidth(30);
		this.tVisitas.getColumn("ID").setMaxWidth(30);
		this.tVisitas.getColumn("Peso").setPreferredWidth(55);
		this.tVisitas.getColumn("Peso").setMaxWidth(55);
		this.tVisitas.getColumn("Ausc.").setPreferredWidth(55);
		this.tVisitas.getColumn("Ausc.").setMaxWidth(55);
		this.tVisitas.getColumn("Soplos").setPreferredWidth(55);
		this.tVisitas.getColumn("Soplos").setMaxWidth(55);
		this.tVisitas.getColumn("Fecha").setPreferredWidth(85);
		this.tVisitas.getColumn("Fecha").setMaxWidth(85);
		this.tVisitas.getColumn("Usuario").setPreferredWidth(85);
		this.tVisitas.getColumn("Usuario").setMaxWidth(85);
		this.tVisitas.getColumn("Ed.").setPreferredWidth(30);
		this.tVisitas.getColumn("Ed.").setMaxWidth(30);
		this.tVisitas.getColumn("El.").setPreferredWidth(30);
		this.tVisitas.getColumn("El.").setMaxWidth(30);

        // determinamos el tooltip
        this.tVisitas.setToolTipText("Pulse para editar / eliminar");

        // fijamos la fuente
        this.tVisitas.setFont(Fuente);

        // fijamos el alto de las filas
        this.tVisitas.setRowHeight(25);

        // agregamos el vieport
        scrollVisitas.setViewportView(this.tVisitas);

        // fijamos el evento click
		this.tVisitas.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tVisitasMouseClicked(evt);
			}
		});

        // agrega el botón nueva visita
        JButton btnVisitas = new JButton();
        btnVisitas.setBounds(170, 370, 26, 26);
        btnVisitas.setFont(Fuente);
        btnVisitas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/m112.png"))); // NOI18N
        btnVisitas.setToolTipText("Pulse para agregar una nueva visita");
        btnVisitas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nuevaVisita();
            }
        });
        this.add(btnVisitas);

    }

    // metodo llamado al pulsar sobre el boton nueva visita
    private void nuevaVisita() {

        // si no grabó el formulario de pacientes
        if (this.Protocolo == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        }

        // instanciamos el formulario de visitas
        new FormVisitas(this.Contenedor, true, this, this.Protocolo);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de tipo de bebida
     */
    private void elementosBebida(){

        // agrega los elementos
        this.cBebida.addItem("");
        this.cBebida.addItem("Vino");
        this.cBebida.addItem("Fermentada");
        this.cBebida.addItem("Destilada");
        this.cBebida.addItem("Otra");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo llamado al pulsar el botón grabar, verifica
     * el formulario antes de enviarlo al servidor
     */
    private void validaAntecedentes(){

        // declaración de variables
        Boolean Correcto = false;

        // si no grabó el formulario de pacientes
        if (this.Protocolo == 0){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe grabar primero los datos\n" +
                        "del paciente.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        }

        // verifica que halla marcado al menos un
        // elemento de disautonomía
        if (this.cHipotension.isSelected()){
            Correcto = true;
        } else if (this.cBradicardia.isSelected()){
            Correcto = true;
        } else if (this.cAstenia.isSelected()){
            Correcto = true;
        } else if (this.cNoTiene.isSelected()){
            Correcto = true;
        }

        // si no marcó ninguno
        if (!Correcto){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe marcar al menos una opción en\n" +
                        "Disautonomía (puede ser No Tiene)",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            return;

        }

        // reiniciamos la variable control
        Correcto = false;

        // verifica que halla marcado al menos un
        // elemento de compromiso digestivo
        if (this.cDisfagia.isSelected()){
            Correcto = true;
        } else if (this.cPirosis.isSelected()){
            Correcto = true;
        } else if (this.cRegurgitacion.isSelected()){
            Correcto = true;
        } else if (this.cConstipacion.isSelected()){
            Correcto = true;
        } else if (this.cBolo.isSelected()){
            Correcto = true;
        } else if (this.cSinDigestivo.isSelected()){
            Correcto = true;
        }

        // si no marcó ninguna
        if (!Correcto){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Debe marcar al menos una opción en \n" +
                        "Compromiso Digestivo (puede ser No Tiene)",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();
            return;

        }

        // si llegò hasta aquí grabamos
        this.grabaAntecedentes();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario que
     * ejecuta la consulta en el servidor
     */
    private void grabaAntecedentes(){

        // asignamos en la clase de chagas agudo
        this.Patologicos.setId(this.IdPatologico);
        this.Patologicos.setPaciente(this.Protocolo);

        // si está marcado
        if (this.rAgudo.isSelected()){
            this.Patologicos.setSintomas(1);
        } else {
            this.Patologicos.setSintomas(0);
        }

        // asigna los comentarios
        this.Patologicos.setObservaciones(this.tObservaciones.getText());

        // asignamos en la clase de antecedentes tóxicos
        this.Toxicos.setId(this.IdAntecedentes);
        this.Toxicos.setPaciente(this.Protocolo);
        this.Toxicos.setFuma((Integer) this.cCigarrillos.getValue());
        this.Toxicos.setAlcohol((Integer) this.cAlcohol.getValue());
        this.Toxicos.setBebida(this.cBebida.getSelectedItem().toString());
        this.Toxicos.setAdicciones(this.tAdicciones.getText());

        // asignamos en la clase de disautonomía
        this.Autonomia.setId(this.IdDisautonomia);
        this.Autonomia.setPaciente(this.Protocolo);

        // según los checkbox
        if (this.cHipotension.isSelected()){
            this.Autonomia.setHipotension(1);
        } else {
            this.Autonomia.setHipotension(0);
        }
        if (this.cBradicardia.isSelected()){
            this.Autonomia.setBradicardia(1);
        } else {
            this.Autonomia.setBradicardia(0);
        }
        if (this.cAstenia.isSelected()){
            this.Autonomia.setAstenia(1);
        } else {
            this.Autonomia.setAstenia(0);
        }
        if (this.cNoTiene.isSelected()){
            this.Autonomia.setNoTiene(1);
        } else {
            this.Autonomia.setNoTiene(0);
        }

        // asignamos en la clase de compromiso digestivo
        this.Estomago.setId(this.IdDigestivo);
        this.Estomago.setPaciente(this.Protocolo);

        // según los checkbox
        if (this.cDisfagia.isSelected()){
            this.Estomago.setDisfagia(1);
        } else {
            this.Estomago.setDisfagia(0);
        }
        if (this.cPirosis.isSelected()){
            this.Estomago.setPirosis(1);
        } else {
            this.Estomago.setPirosis(0);
        }
        if (this.cRegurgitacion.isSelected()){
            this.Estomago.setRegurgitacion(1);
        } else {
            this.Estomago.setRegurgitacion(0);
        }
        if (this.cConstipacion.isSelected()){
            this.Estomago.setConstipacion(1);
        } else {
            this.Estomago.setConstipacion(0);
        }
        if (this.cBolo.isSelected()){
            this.Estomago.setBolo(1);
        } else {
            this.Estomago.setBolo(0);
        }
        if (this.cSinDigestivo.isSelected()){
            this.Estomago.setTiene(1);
        } else {
            this.Estomago.setTiene(0);
        }

        // grabamos en cada una de las clases y obtenemos
        // la clave del registro
        this.IdAntecedentes = this.Patologicos.grabaAgudo();
        this.IdDigestivo = this.Estomago.grabaDigestivo();
        this.IdDisautonomia = this.Autonomia.grabaDisautonia();
        this.IdPatologico = this.Toxicos.grabaAntecedente();

        // presenta el mensaje
        new Mensaje("Registro Grabado ... ");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar que
     * reinicia el formulario
     */
    private void reiniciaAntecedentes(){

        // si estaba editando
        if (this.Protocolo != 0){

            // recargamos el registro
            this.verAntecedentes(this.Protocolo);

        // si era un nuevo registro
        } else {

            // lo limpiamos
            this.limpiaFormulario();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario, es público porque
     * podemos llamarlo desde el menú
     */
    public void limpiaFormulario(){

        // inicializamos los campos
        this.rAgudo.setSelected(false);
        this.cCigarrillos.setValue(0);
        this.cAlcohol.setValue(0);
        this.cBebida.setSelectedItem("");
        this.tAdicciones.setText("");
        this.cHipotension.setSelected(false);
        this.cBradicardia.setSelected(false);
        this.cAstenia.setSelected(false);
        this.cNoTiene.setSelected(false);
        this.cDisfagia.setSelected(false);
        this.cPirosis.setSelected(false);
        this.cRegurgitacion.setSelected(false);
        this.cConstipacion.setSelected(false);
        this.cBolo.setSelected(false);
        this.cSinDigestivo.setSelected(false);
        this.tObservaciones.setText("");
        this.tUsuario.setText(Seguridad.Usuario);
        this.tAlta.setText(this.Herramientas.FechaActual());

        // inicializamos las variables de las claves
        this.IdPatologico = 0;
        this.IdDisautonomia = 0;
        this.IdAntecedentes = 0;
        this.IdDigestivo = 0;
        this.Protocolo = 0;

        // limpiamos la grilla
		DefaultTableModel modeloTabla = (DefaultTableModel) tVisitas.getModel();
		modeloTabla.setRowCount(0);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - entero con la clave del protocolo
     * Método que recibe como parámetro la clave del
     * registro y presenta los datos en el formulario
     * Es público porque lo llamamos al cargar el paciente
     */
    public void verAntecedentes(int clave){

        // asignamos en la variable de clase
        this.Protocolo = clave;

        // obtenemos los registros
        this.Autonomia.getDatosDisautonomia(clave);
        this.Toxicos.getDatosAntecedente(clave);
        this.Estomago.getDatosDigestivo(clave);
        this.Patologicos.getDatosAgudo(clave);

        // almacenamos las claves del registro
        this.IdAntecedentes = this.Toxicos.getId();
        this.IdDisautonomia = this.Autonomia.getId();
        this.IdDigestivo = this.Estomago.getId();
        this.IdPatologico = this.Patologicos.getId();

        // ahora cargamos en el formulario
        if (this.Patologicos.getSintomas() == 0){
            this.rAgudo.setSelected(false);
        } else {
            this.rAgudo.setSelected(true);
        }
        this.cCigarrillos.setValue(this.Toxicos.getFuma());
        this.cAlcohol.setValue(this.Toxicos.getAlcohol());
        this.cBebida.setSelectedItem(this.Toxicos.getBebida());
        this.tAdicciones.setText(this.Toxicos.getAdicciones());

        // según los valores de disautonomía
        if (this.Autonomia.getHipotension() == 0){
            this.cHipotension.setSelected(false);
        } else {
            this.cHipotension.setSelected(true);
        }
        if (this.Autonomia.getBradicardia() == 0){
            this.cBradicardia.setSelected(false);
        } else {
            this.cBradicardia.setSelected(true);
        }
        if (this.Autonomia.getAstenia() == 0){
            this.cAstenia.setSelected(false);
        } else {
            this.cAstenia.setSelected(true);
        }
        if (this.Autonomia.getNoTiene() == 0){
            this.cNoTiene.setSelected(false);
        } else {
            this.cNoTiene.setSelected(true);
        }

        // ahora el compromiso digestivo
        if (this.Estomago.getDisfagia() == 0){
            this.cDisfagia.setSelected(false);
        } else {
            this.cDisfagia.setSelected(true);
        }
        if (this.Estomago.getPirosis() == 0){
            this.cPirosis.setSelected(false);
        } else {
            this.cPirosis.setSelected(true);
        }
        if (this.Estomago.getRegurgitacion() == 0){
            this.cRegurgitacion.setSelected(false);
        } else {
            this.cRegurgitacion.setSelected(true);
        }
        if (this.Estomago.getConstipacion() == 0){
            this.cConstipacion.setSelected(false);
        } else {
            this.cConstipacion.setSelected(true);
        }
        if (this.Estomago.getBolo() == 0){
            this.cBolo.setSelected(false);
        } else {
            this.cBolo.setSelected(true);
        }
        if (this.Estomago.getTiene() == 0){
            this.cSinDigestivo.setSelected(false);
        } else {
            this.cSinDigestivo.setSelected(true);
        }

        // fijamos el usuario y los comentarios
        this.tObservaciones.setText(this.Patologicos.getObservaciones());
        this.tUsuario.setText(this.Patologicos.getUsuario());
        this.tAlta.setText(this.Patologicos.getFechaAlta());

        // ahora cargamos la grilla de visitas
        this.cargaVisitas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo entero con el número de protocolo
     * Método público llamado desde el formulario de pacientes que
     * luego de un alta asigna el número de protocolo
     */
    public void setProtocolo(int protocolo){

        // asignamos en la variable de clase
        this.Protocolo = protocolo;

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de visitas
	 */
	protected void tVisitasMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tVisitas.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tVisitas.rowAtPoint(evt.getPoint());
		int columna = this.tVisitas.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tVisitas.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 7) {

				// cargamos el registro
				this.verVisita(clave);

			// si pulsó en eliminar
			} else if (columna == 8) {

				// eliminamos
				this.borraVisita(clave);

			}

		}

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave entero con la clave de una visita
     * Método llamado al pulsar la grilla de visitas que
     * recibe como parámetro la clave del registro, instancia
     * el formulario emergente y carga los datos de la visita
     */
    private void verVisita(int clave){

        // instanciamos el formulario y mostramos el registro
        FormVisitas Visitas = new FormVisitas(this.Contenedor, true, this, this.Protocolo);
        Visitas.verVisita(clave);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave entero con la clave de una visita
     * Método llamado al pulsar la grilla de visitas que
     * recibe como parámetro la clave del registro, y
     * luego de pedir confirmación, elimina los registros
     * de esa visita
     */
    private void borraVisita(int clave){

		// pide confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                    "Está seguro que desea eliminar el registro?\n" +
                                    "Se eliminarán  además los registros de las\n" +
                                    "tablas asociadas.",
                                    "Visitas",
                                    JOptionPane.YES_NO_OPTION,
                                    JOptionPane.QUESTION_MESSAGE,
                                    null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

            // borramos el registro y recargamos la grilla
            this.Entradas.borraVisita(clave);
            this.cargaVisitas();

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al cargar el formulario, y luego de
     * borrar o grabar una visita, que carga la grilla
     * de visitas a partir del protocolo del paciente
     */
    public void cargaVisitas(){

		// obtenemos la nómina
		ResultSet Nomina = this.Entradas.nominaVisitas(this.Protocolo);

		// sobrecargamos el renderer de la tabla
		this.tVisitas.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tVisitas.getModel();

		// hacemos la tabla se pueda ordenar
		this.tVisitas.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[9];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("estadio");
                fila[2] = Nomina.getFloat("peso");

                // si hubo eventos en auscultación
                if (Nomina.getBoolean("auscultacion")){
                    fila[3] = true;
                } else {
                    fila[3] = false;
                }

                // si hubo soplos
                if (Nomina.getBoolean("soplos")){
                    fila[4] = true;
                } else {
                    fila[4] = false;
                }

                // agregamos el resto
                fila[5] = Nomina.getString("fecha");
                fila[6] = Nomina.getString("usuario");
				fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

    	// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

        }

    }

}