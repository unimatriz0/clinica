/*

    Nombre: formConfiguracion
    Fecha: 27/05/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Formulario que pide los datos de conexión a la base de
                 datos y luego graba en un archivo de configuración

 */

// definición del paquete
package dbApi;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import seguridad.FormIngreso;
import java.awt.Font;

// definición de la clase
public class FormConfiguracion extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // declaración de variables
    JTextField tServidor;
    JTextField tUsuario;
    JTextField tContrasenia;
    JButton okButton;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que crea y configura el diálogo
     */
    public FormConfiguracion () {

        // fijamos la posisión y el layout
        this.setBounds(250, 200, 450, 216);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setIconImage(new ImageIcon(getClass().getResource("/Graficos/logo_fatala.png")).getImage());
        this.setTitle("Acceso a la Base de Datos");
        this.setLayout(null);

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta el título
        JLabel lTitulo = new JLabel("Configuración del Acceso a la Base de Datos");
        lTitulo.setFont(Fuente);
        lTitulo.setBounds(20, 0, 430, 26);
        this.add(lTitulo);

        // presenta el label de servidor
        JLabel lServidor = new JLabel("Servidor:");
        lServidor.setFont(Fuente);
        lServidor.setBounds(20, 34, 70, 26);
        this.add(lServidor);

        // presenta el texto del servidor
        this.tServidor = new JTextField();
        this.tServidor.setFont(Fuente);
        this.tServidor.setBounds(125, 34, 106, 26);
        this.tServidor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    tUsuario.requestFocus();
                }

            }
        });
        this.add(this.tServidor);

        // presenta el label de usuario de la base
        JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setFont(Fuente);
        lUsuario.setBounds(20, 68, 70, 26);
        this.add(lUsuario);

        // presenta el texto de usuario
        this.tUsuario = new JTextField();
        this.tUsuario.setBounds(125, 70, 106, 26);
        this.tUsuario.setFont(Fuente);
        this.tUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    tContrasenia.requestFocus();
                }

            }
        });
        this.add(this.tUsuario);

        // presenta el label de contraseña
        JLabel lContrasenia = new JLabel("Contraseña:");
        lContrasenia.setFont(Fuente);
        lContrasenia.setBounds(20, 104, 88, 26);
        this.add(lContrasenia);

        // presenta el texto de contraseña
        this.tContrasenia = new JTextField();
        this.tContrasenia.setFont(Fuente);
        this.tContrasenia.setBounds(125, 104, 106, 26);
        this.tContrasenia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    okButton.requestFocus();
                }

            }
        });
        this.add(this.tContrasenia);

        // agregamos el botón aceptar
        this.okButton = new JButton("Aceptar");
        this.okButton.setIcon(new ImageIcon(FormConfiguracion.class.getResource("/Graficos/mexito.png")));
        this.okButton.setBounds(150, 137, 125, 30);
        this.okButton.setFont(Fuente);
        this.okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                // verificamos los datos del formulario
                verificaFormulario();

            }
        });
        this.add(this.okButton);

        // agregamos el botón cancelar
        JButton cancelButton = new JButton("Cancelar");
        cancelButton.setIcon(new ImageIcon(FormConfiguracion.class.getResource("/Graficos/merror.png")));
        cancelButton.setBounds(300, 137, 125, 30);
        cancelButton.setFont(Fuente);
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                // cerramos el formulario
                dispose();

            }
        });
        this.add(cancelButton);

        // fijamos el foco
        this.tServidor.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario
     */
    protected void verificaFormulario() {

        // si no ingresó el servidor
        if (this.tServidor.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "La dirección del servidor", "Error", JOptionPane.ERROR_MESSAGE);
            this.tServidor.requestFocus();
            return;

        }

        // si no ingresó el usuario
        if (this.tUsuario.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Ingrese el nombre de usuario", "Error", JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();
            return;

        }

        // si no ingresó la contraseña
        if (this.tContrasenia.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Ingrese la contraseña de conexión", "Error",
                            JOptionPane.ERROR_MESSAGE);
            this.tContrasenia.requestFocus();
            return;

        }

        // llama la función de guardado de datos
        this.guardaConfiguracion();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que guarda los datos de configuración en un archivo de propiedades
     */
    protected void guardaConfiguracion() {

        // definimos las variables
        Properties Configuracion = new Properties();
        OutputStream Salida;

        // asignamos los valores por defecto la cadena ?useServerPrepStmts=true
        // nos sirve para indicarle al driver que vamos a usar statement
        // de otra forma los convierte internamente a consultas
        String Servidor = "jdbc:mysql://" + this.tServidor.getText()
                        + "?useServerPrepStmts=true&useUnicode=yes&characterEncoding=UTF-8";
        String Usuario = this.tUsuario.getText();
        String Contrasenia = this.tContrasenia.getText();

        // creamos el archivo de propiedades
        try {

            // lo abrimos y guardamos los valores
            Salida = new FileOutputStream("Configuracion.properties");
            Configuracion.setProperty("Servidor", Servidor);
            Configuracion.setProperty("Usuario", Usuario);
            Configuracion.setProperty("Password", Contrasenia);

            // guardamos el archivo
            Configuracion.store(Salida, null);

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                            "Configuración Almacenada,\ningrese nuevamente",
                            "Atencion", JOptionPane.INFORMATION_MESSAGE);

            // inicializamos el formulario principal
            // y cerramos el formulario
            new FormIngreso().setVisible(true);
            this.dispose();

            // si hubo un error
        } catch (IOException ex) {

            // presentamos el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}
