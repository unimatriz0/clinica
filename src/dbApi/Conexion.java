/*

    Nombre: Conexion
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para conectarse a la base
                 de datos, crear y leer los archivos de configuración 
                 y efectuar consultas sencillas no parametrizadas

 */

// definición del paquete
package dbApi;

// importamos los archivos
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * 
 * @version 1.0
 * @author Lic. Claudio Invernizzi
 * 
*/
public class Conexion {
    
    // definición de variables
    protected String Servidor;                // servidor y base de datos a conectar
    protected String Usuario;                 // usuario de la base
    protected String Contrasenia;             // password de la base
    protected Connection Link;                // puntero a la base de datos
    protected FormConfiguracion Configurar;   // formulario de parámetros
    
    /**
     * 
     * @author Lic. Claudio Invernizzi
     * Constructor de la clase sin parámetros, establece una conexión 
     * primero intentando leer el archivo de propiedades, si no lo 
     * encuentra lo crea y almacena en el los valores por defecto 
     * 
     */
    public Conexion(){

        // definimos las variables
        Properties Configuracion = new Properties();
        InputStream Entrada;

        // inicializamos el puntero
        this.Link = null;
                
        // si existe el archivo de configuracion
        File Archivo = new File("Configuracion.properties");
        if (Archivo.exists()){

            // tratamos de leer el archivo
            try {

                // abrimos el archivo
                Entrada = new FileInputStream ("Configuracion.properties");

                // leemos el archivo de propiedades
                Configuracion.load(Entrada);

                // si lo pudo abrir leemos los valores
                this.Servidor = Configuracion.getProperty("Servidor");
                this.Usuario = Configuracion.getProperty("Usuario");
                this.Contrasenia = Configuracion.getProperty("Password");

                // establecemos la conexión
                this.Conectar();
                
            } catch (IOException ex) {

                // presentamos el mensaje de error
                System.out.println(ex.getMessage());

            }

        }
        
    }
 
    /**
     * 
     * @author Lic. Claudio Invernizzi
     * @param servidor cadena con el nombre del servidor
     * @param usuario cadena con el nombre de usuario
     * @param contrasenia cadena con el password de conexión
     * Constructor de la clase con parámetros, establece la 
     * conexión de acuerdo a los parámetros recibidos
     */
    public Conexion(String servidor, String usuario, String contrasenia){
        
        // asignamos los valores
        this.Servidor = servidor;
        this.Usuario = usuario;
        this.Contrasenia = contrasenia;
        this.Link = null;
        
        // establecemos la conexión
        this.Conectar();
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que lee los valores de la clase y establece
     * la conexión con la base de datos
     */
    private void Conectar(){

        // intentamos conectar
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.Link = DriverManager.getConnection(this.Servidor, this.Usuario, this.Contrasenia);
         } catch (SQLException ex) {
            System.out.println(ex.getMessage());
         } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
              
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que cierra la conexión con la base 
     */
    public void Cerrar(){
        
        // cerramos la conexión
        try {
            this.Link.close();
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        
    }

    /**
     * 
     * @author Lic. Claudio Invernizzi
     * @param consulta Cadena con la consulta a realizar
     * @return resultado Un resultset con el resultado de la consulta
     * Método que recibe como parámetro una consulta, la ejecuta en la 
     * base y retorna el array con el resultado
     */
    public ResultSet Consultar(String consulta){
    
        // definimos las variables
        ResultSet Resultado = null;
        Statement SQL;
        
        // ejecutamos la consulta
        try {
            SQL = this.Link.createStatement();
            Resultado = SQL.executeQuery(consulta);
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
            System.out.println(consulta);
        }
        
        // retornamos el resultset
        return Resultado;
        
    }
   
    /**
     * 
     * @author Lic. Claudio Invernizzi
     * @param consulta Cadena con la consulta a realizar
     * @return entero con el resultado de la operacion
     * Método que ejecuta una consulta de actualización, inserción o 
     * eliminación (es decir una consulta que no devuelve registros)
     * 
     */
    public int Ejecutar(String consulta){

        // definimos las variables
        Statement SQL;
        int Resultado = 0;

        try {
         
            // ejecutamos la consulta
            SQL = this.Link.createStatement();
            Resultado = SQL.executeUpdate(consulta);
            
        // si hubo un error
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
            System.out.println(consulta);
        }
        
        // retornamos el número de registros afectados
        return Resultado;
        
    }
    
    /**
     * 
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return IdRegistro entero con la clave del registro insertado
     * Método que retorna la id del último registro insertado
     * 
     */
    public int UltimoInsertado(){
        
    	// declaración de variables
        int IdRegistro = 0;
        ResultSet Resultado;
        Statement SQL;
    
        try {

            // ejecutamos la consulta         
            SQL = this.Link.createStatement();
            Resultado = SQL.executeQuery("SELECT LAST_INSERT_ID() AS id_registro;");
            
            // obtenemos el registro
            Resultado.next();
            IdRegistro = Resultado.getInt("id_registro");
            
            // cerramos el resultset
            Resultado.close();
            
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        // retornamos la id del registro
        return IdRegistro;
        
    }
       
    /**
     * @author Lic. Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return enlace a la base de datos
     * Función que retorna el puntero de conexión a la base, utilizada
     * en aquellas consultas parametrizadas del sistema
     */
    public Connection getConexion(){
        
        // retorna el puntero
        return this.Link;
        
    }
    
}
