/*
 *
 * Nombre: formConfig
 * Fecha: 28/11/2017
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Proyecto: diagnostico
 * Licencia: GPL
 * Producido en: INP - Dr. Mario Fatala Chaben
 * Buenos Aires - Argentina
 * Comentarios: Formulario que presenta el combo de selección de tema
 *
 */

// definición del paquete
package dbApi;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import org.jvnet.substance.SubstanceLookAndFeel;
import java.awt.Font;

// definición de la clase
public class FormConfig extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

	// declaración de variables
    protected JComboBox<String> cTema;
    protected Config configuracion;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param parent - la ventana padre
     * @param modal - si el diálogo será modal
     * Creamos el diálogo
     */
    public FormConfig(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

        // fijamos las propiedades
        this.setBounds(200, 200, 395, 178);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);
        this.setTitle("Usuarios Autorizados");
        this.setResizable(false);

        // inicializamos la clase
        this.configuracion = new Config();

        // configuramos el formulario
        initForm();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario
     */
    protected void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta el título
        JLabel lTitulo = new JLabel("<html>Seleccione el tema y pulse <b>Aceptar</b></html>");
        lTitulo.setFont(Fuente);
        lTitulo.setBounds(2, 12, 372, 15);
        getContentPane().add(lTitulo);

        // definimos el select
        this.cTema = new JComboBox<>();
        this.cTema.setFont(Fuente);
        cTema.setBounds(5, 39, 371, 24);
        getContentPane().add(cTema);

        // agregamos los elementos al combo
        this.cTema.addItem("");
        this.cTema.addItem("Otoño");
        this.cTema.addItem("Acero Negro");
        this.cTema.addItem("Acero Azul");
        this.cTema.addItem("Café");
        this.cTema.addItem("Crema");
        this.cTema.addItem("Esmeralda");
        this.cTema.addItem("Verde");
        this.cTema.addItem("Magma");
        this.cTema.addItem("Agua");
        this.cTema.addItem("Plateado");
        this.cTema.addItem("Nebula");
        this.cTema.addItem("Office Azul");
        this.cTema.addItem("Grafito");
        this.cTema.addItem("Sahara");

        // obtenemos el tema actual
        String tema = this.configuracion.getTema();

        // si hay un tema guardado
        if (tema != null) {

            // lo seleccionamos
            switch (tema){

                case "org.jvnet.substance.skin.AutumnSkin":
                    this.cTema.setSelectedItem("Otoño");
                    break;
                case "org.jvnet.substance.skin.BusinessBlackSteelSkin":
                    this.cTema.setSelectedItem("Aero Negro");
                    break;
                case "org.jvnet.substance.skin.BusinessBlueSteelSkin":
                    this.cTema.setSelectedItem("Acero Azul");
                    break;
                case "org.jvnet.substance.skin.CremeCoffeeSkin":
                    this.cTema.setSelectedItem("Café");
                    break;
                case "org.jvnet.substance.skin.CremeSkin":
                    this.cTema.setSelectedItem("Crema");
                    break;
                case "org.jvnet.substance.skin.EmeraldDuskSkin":
                    this.cTema.setSelectedItem("Esmeralda");
                    break;
                case "org.jvnet.substance.skin.GreenMagicSkin":
                    this.cTema.setSelectedItem("Verde");
                    break;
                case "org.jvnet.substance.skin.MagmaSkin":
                    this.cTema.setSelectedItem("Magma");
                    break;
                case "org.jvnet.substance.skin.MistAquaSkin":
                    this.cTema.setSelectedItem("Agua");
                    break;
                case "org.jvnet.substance.skin.MistSilverSkin":
                    this.cTema.setSelectedItem("Plateado");
                    break;
                case "org.jvnet.substance.skin.NebulaSkin":
                    this.cTema.setSelectedItem("Nebula");
                    break;
                case "org.jvnet.substance.skin.OfficeBlue2007Skin":
                    this.cTema.setSelectedItem("Office Azul");
                    break;
                case "org.jvnet.substance.skin.RavenGraphiteSkin":
                    this.cTema.setSelectedItem("Grafito");
                    break;
                case "org.jvnet.substance.skin.SaharaSkin":
                    this.cTema.setSelectedItem("Sahara");
                    break;

            }

        }

        // el botón aceptar
        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.setBounds(46, 83, 117, 25);
        btnAceptar.setFont(Fuente);
        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnAceptar.setToolTipText("Pulse para seleccionar el tema");
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                // actualiza el usuario
                grabarTema();

            }
        });
        getContentPane().add(btnAceptar);

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(220, 83, 117, 25);
        btnCancelar.setFont(Fuente);
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/msalida.png")));
        btnCancelar.setToolTipText("Cierra sin guardar cambios");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                // actualiza el usuario
                cerrarConfig();

            }
        });
        getContentPane().add(btnCancelar);

        // mostramos el formulario
        this.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar, verifica el formulario
     * y luego graba la configuración en el init
     */
    protected void grabarTema(){

        // definición de variables
        String tema = this.cTema.getSelectedItem().toString();
        String seleccionado = "";

        // verifica se halla seleccionado un tema
        if (tema == ""){

            // presenta el mensaje y retorna
            // presenta el mensaje
            String mensaje = "Debe seleccionar un tema a aplicar";
            JOptionPane.showMessageDialog(this, mensaje, "Error", JOptionPane.ERROR_MESSAGE);
            return;

        }

        // según el tema seleccionado
        switch (tema){

            case "Otoño":
                seleccionado = "org.jvnet.substance.skin.AutumnSkin";
                break;
            case "Aero Negro":
                seleccionado = "org.jvnet.substance.skin.BusinessBlackSteelSkin";
                break;
            case "Acero Azul":
                seleccionado = "org.jvnet.substance.skin.BusinessBlueSteelSkin";
                break;
            case "Café":
                seleccionado = "org.jvnet.substance.skin.CremeCoffeeSkin";
                break;
            case "Crema":
                seleccionado = "org.jvnet.substance.skin.CremeSkin";
                break;
            case "Esmeralda":
                seleccionado = "org.jvnet.substance.skin.EmeraldDuskSkin";
                break;
            case "Verde":
                seleccionado = "org.jvnet.substance.skin.GreenMagicSkin";
                break;
            case "Magma":
                seleccionado = "org.jvnet.substance.skin.MagmaSkin";
                break;
            case "Agua":
                seleccionado = "org.jvnet.substance.skin.MistAquaSkin";
                break;
            case "Plateado":
                seleccionado = "org.jvnet.substance.skin.MistSilverSkin";
                break;
            case "Nebula":
                seleccionado = "org.jvnet.substance.skin.NebulaSkin";
                break;
            case "Office Azul":
                seleccionado = "org.jvnet.substance.skin.OfficeBlue2007Skin";
                break;
            case "Grafito":
                seleccionado = "org.jvnet.substance.skin.RavenGraphiteSkin";
                break;
            case "Sahara":
                seleccionado = "org.jvnet.substance.skin.SaharaSkin";
                break;

        }

        // aplicamos el tema
        SubstanceLookAndFeel.setSkin(seleccionado);

        // pide confirmación
        int n = JOptionPane.showConfirmDialog(
                            this,
                            "Desea conservar este esquema?",
                            "Confirme por favor",
                            JOptionPane.YES_NO_OPTION);

        // si respondió que sí
        if(n == JOptionPane.YES_OPTION){

            // guardamos en el archivo de configuración
            this.configuracion.setTema(seleccionado);

            // abandonamos
            this.dispose();

        // si canceló
        } else {

            // restauramos el tema original
            String original = this.configuracion.getTema();
            SubstanceLookAndFeel.setSkin(original);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar, solo cierra
     * el formulario
     */
    protected void cerrarConfig(){

        // eliminamos el formulario
        this.dispose();

    }

}
