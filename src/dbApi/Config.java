/*
*
* Nombre: Config
* Fecha: 28/11/2017
* Autor: Lic. Claudio Invernizzi
* E-Mail: cinvernizzi@gmail.com
* Proyecto: diagnostico
* Licencia: GPL
* Producido en: INP - Dr. Mario Fatala Chaben
* Buenos Aires - Argentina
* Comentarios: Clase que contiene los métodos de lectura y escritura
*              de la configuración de los temas
*/

//definición del paquete
package dbApi;

//importamos las librerías
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

//definición de la clase
public class Config {

	// definición de variables
	String Tema;

    // estas las usamos para restaurar los valores
    // originales de conexión
    protected String Servidor;
    protected String Contrasenia;
    protected String Usuario;

   // constructor de la clase
   public Config(){

       // definimos las variables
       Properties Configuracion = new Properties();
       InputStream Entrada;

       // si existe el archivo de configuracion
       File Archivo = new File("Configuracion.properties");
       if (Archivo.exists()){

           // tratamos de leer el archivo
           try {

               // abrimos el archivo
               Entrada = new FileInputStream ("Configuracion.properties");

               // leemos el archivo de propiedades
               Configuracion.load(Entrada);

               // si lo pudo abrir leemos los valores
               this.Servidor = Configuracion.getProperty("Servidor");
               this.Usuario = Configuracion.getProperty("Usuario");
               this.Contrasenia = Configuracion.getProperty("Password");
               this.Tema = Configuracion.getProperty("Tema");

           } catch (IOException ex) {

               // presentamos el mensaje de error
               System.out.println(ex.getMessage());

           }

       }

   }

   /**
    * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    * @return tema
    * Método que retorna la cadena con el tema seleccionado
    */
   public String getTema(){

       // retornamos el tema
       return this.Tema;

   }

   /**
    * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    * @param tema el nombre del tema
    * Método que recibe como parámetro del nombre de un tema
    * y lo almacena en el archivo de configuración
    */
   public void setTema(String tema){

       // grabamos el tema
       Properties Configuracion = new Properties();
       OutputStream Salida;

       // abrimos el archivo de propiedades
       try {

           // lo abrimos y guardamos los valores y
           // los valores originales de conexión
           Salida = new FileOutputStream("Configuracion.properties");
           Configuracion.setProperty("Tema", tema);
           Configuracion.setProperty("Servidor", Servidor);
           Configuracion.setProperty("Usuario", Usuario);
           Configuracion.setProperty("Password", Contrasenia);

           // guardamos el archivo
           Configuracion.store(Salida, null);

           // si hubo un error
       } catch (IOException ex) {

           // presentamos el mensaje de error
           System.out.println(ex.getMessage());

       }

   }

}
