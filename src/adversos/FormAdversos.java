/*

    Nombre: formIngreso
    Fecha: 15/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Formulario que presenta los datos para el abm
	             del diccionario de efectos adversos

 */

// definición del paquete
package adversos;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import seguridad.Seguridad;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import funciones.RendererTabla;
import funciones.Utilidades;
import adversos.Adversos;

public class FormAdversos extends JDialog {

	// generamos el serial id
	private static final long serialVersionUID = 1L;

	// definimos las variables de clase
	private JTextField tId;                 // clave del registro
	private JTextField tDescripcion;        // descripción del efecto
	private JTextField tUsuario;            // nombre del usuario
	private JTextField tAlta;               // fecha de alta
	private JTable tAdversos;               // grilla con el diccionario
	private Utilidades Herramientas;        // funciones de fecha
	private Adversos Problemas;             // clase de la base de datos

	// constructor de la clase
	@SuppressWarnings("serial")
	public FormAdversos(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// instanciamos las clases
		this.Herramientas = new Utilidades();
		this.Problemas = new Adversos();

		// fijamos las propiedades
		this.setBounds(150, 150, 490, 395);
		this.setLayout(null);
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		this.setTitle("Efectos Adversos");

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Diccionario de Efectos Adversos");
		lTitulo.setBounds(10, 10, 270, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// presenta la id
		JLabel lId = new JLabel("ID");
		lId.setBounds(10, 40, 45, 26);
		lId.setFont(Fuente);
		lId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lId);
		this.tId = new JTextField();
		this.tId.setBounds(10, 70, 45, 26);
		this.tId.setFont(Fuente);
		this.tId.setEditable(false);
		this.tId.setToolTipText("Clave del registro");
		this.add(this.tId);

		// presenta la descripción
		JLabel lDescripcion = new JLabel("Descripción");
		lDescripcion.setBounds(65, 40, 190, 26);
		lDescripcion.setFont(Fuente);
		lDescripcion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lDescripcion);
		this.tDescripcion = new JTextField();
		this.tDescripcion.setBounds(65, 70, 190, 26);
		this.tDescripcion.setFont(Fuente);
		this.tDescripcion.setToolTipText("Describa el efecto adverso");
		this.add(this.tDescripcion);

		// presenta el usuario
		JLabel lUsuario = new JLabel("Usuario");
		lUsuario.setBounds(265, 40, 80, 26);
		lUsuario.setFont(Fuente);
		lUsuario.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lUsuario);
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(265, 70, 80, 26);
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setEditable(false);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.add(this.tUsuario);

		// fijamos el usuario
		this.tUsuario.setText(Seguridad.Usuario);

		// presenta la fecha
		JLabel lFecha = new JLabel("Fecha");
		lFecha.setBounds(360, 40, 85, 26);
		lFecha.setFont(Fuente);
		lFecha.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lFecha);
		this.tAlta = new JTextField();
		this.tAlta.setBounds(360, 70, 85, 26);
		this.tAlta.setFont(Fuente);
		this.tAlta.setEditable(false);
		this.tAlta.setToolTipText("Fecha de alta del registro");
		this.add(this.tAlta);

		// fijamos la fecha
		this.tAlta.setText(this.Herramientas.FechaActual());

		// agregamos el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(455, 70, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaEfecto();
			}
		});
		this.add(btnGrabar);

		// define el scroll
		JScrollPane scrollAdversos = new JScrollPane();
		scrollAdversos.setBounds(10, 100, 470, 250);
		this.add(scrollAdversos);

		// definimos la tabla
		this.tAdversos = new JTable();

		// fijamos las propiedades
		this.tAdversos.setModel(new DefaultTableModel(
			new Object[][] {
				{null, "", null, null, null, null},
			},
			new String[] {
				"ID",
				"Descripcion",
				"Usuario",
				"Alta",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// agregamos la tabla al scroll
		scrollAdversos.setViewportView(this.tAdversos);

		// establecemos la fuente
		this.tAdversos.setFont(Fuente);

		// fijamos el alto de las filas
		this.tAdversos.setRowHeight(25);

		// fijamos el ancho de las columnas
		this.tAdversos.getColumn("ID").setMaxWidth(40);
		this.tAdversos.getColumn("Usuario").setMaxWidth(80);
		this.tAdversos.getColumn("Alta").setMaxWidth(80);
		this.tAdversos.getColumn("Ed.").setMaxWidth(35);
		this.tAdversos.getColumn("El.").setMaxWidth(35);

		// cargamos el diccionario
		this.cargaEfectos();

		// establecemos el evento click
		this.tAdversos.addMouseListener(new MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tAdversosClicked(evt);
			}
		});

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado desde el constructor que carga la nómina
	 * de efectos adversos
	 */
	private void cargaEfectos(){

		// obtenemos la nómina
		ResultSet Nomina = this.Problemas.nominaAdversos();

		// sobrecargamos el renderer de la tabla
		this.tAdversos.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tAdversos.getModel();

		// hacemos la tabla se pueda ordenar
		tAdversos.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[6];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("descripcion");
				fila[2] = Nomina.getString("usuario");
				fila[3] = Nomina.getString("fecha");
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de grabar o borrar que limpia
	 * el formulario de datos
	 */
	private void limpiaFormulario(){

		// limpiamos los campos
		this.tId.setText("");
		this.tDescripcion.setText("");
		this.tUsuario.setText(Seguridad.Usuario);
		this.tAlta.setText(this.Herramientas.FechaActual());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica
	 * y luego ejecuta la consulta en el servidor
	 */
	private void grabaEfecto(){

		// si no ingresó la descripción
		if (this.tDescripcion.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                        "Ingrese la descripción del efecto",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tDescripcion.requestFocus();
            return;

		// si declaró
		} else {

			// asigna la descripción
			this.Problemas.setDescripcion(this.tDescripcion.getText());

		}

		// primero verifica no estar repetido
		// si es un alta
		if (this.tId.getText().isEmpty()){

			// verificamos si existe
			if (!this.Problemas.validaAdverso(this.tDescripcion.getText())){

	            // presenta el mensaje y retorna
				JOptionPane.showMessageDialog(this,
							"Ese efecto ya está declarado",
							"Error",
							JOptionPane.ERROR_MESSAGE);
				this.tDescripcion.requestFocus();
				return;

			// si no existe
			} else {

				// asigna en la clase
				this.Problemas.setId(0);

			}

		// si está editando
		} else {
			this.Problemas.setId(Integer.parseInt(this.tId.getText()));
		}

		// grabamos el registro
		this.Problemas.grabaAdverso();

		// recargamos la grilla
		this.cargaEfectos();

		// limpiamos el formulario
		this.limpiaFormulario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que recibe la clave del registro como parámetro
	 * y lo carga en el formulario
	 */
	private void verAdverso(int clave){

		// obtenemos el registro
		this.Problemas.getDatosAdverso(clave);

		// asignamos en el formulario
		this.tId.setText(Integer.toString(this.Problemas.getId()));
		this.tDescripcion.setText(this.Problemas.getDescripcion());
		this.tUsuario.setText(this.Problemas.getUsuario());
		this.tAlta.setText(this.Problemas.getAlta());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que recibe como parámetro la clave de un registro
	 * y luego de pedir confirmación ejecuta la consulta
	 * en el servidor
	 */
	private void borraAdverso(int clave){

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
		                           "Está seguro que desea eliminar el registro?",
								   "Efectos Adversos",
								   JOptionPane.YES_NO_OPTION,
								   JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Problemas.borraAdverso(clave);

			// limpiamos el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaEfectos();

		}

	}

	// método llamado al pulsar sobre la grilla
	private void tAdversosClicked(MouseEvent evt) {

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tAdversos.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tAdversos.rowAtPoint(evt.getPoint());
		int columna = this.tAdversos.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tAdversos.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 4) {

				// cargamos el registro
				this.verAdverso(clave);

			// si pulsó en eliminar
			} else if (columna == 5) {

				// eliminamos
				this.borraAdverso(clave);

			}

		}

	}

}
