/*

    Nombre: Adversos
    Fecha: 09/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 efectos adversos del tratamiento

 */

// definición del paquete
package adversos;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Adversos {

    // declaración de las variables de clase
    private Conexion Enlace;         // puntero a la base de datos
    private int Id;                  // clave del registro
    private String Descripcion;      // descripción del efecto
    private int IdUsuario;           // clave del usuario
    private String Usuario;          // nombre del usuario
    private String Alta;             // fecha de alta

    // constructor de la clase
    public Adversos(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Descripcion = "";
        this.IdUsuario = Seguridad.Id;

    }

    // métodos de asignación de variables
    public void setId(int id){
        this.Id = id;
    }
    public void setDescripcion(String descripcion){
        this.Descripcion = descripcion;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public String getDescripcion(){
        return this.Descripcion;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getAlta(){
        return this.Alta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de síntomas
     * Método que retorna un resultset con la nómina de síntomas
     */
    public ResultSet nominaAdversos(){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_adversos.id AS id, " +
                   "       diagnostico.v_adversos.descripcion AS descripcion, " +
                   "       diagnostico.v_adversos.usuario AS usuario, " +
                   "       diagnostico.v_adversos.fecha AS fecha " +
                   "FROM diagnostico.v_adversos " +
                   "ORDER BY diagnostico.v_adversos.descripcion; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - entero con la clave del registro
     * Método que recibe la clave del registro como parámetro
     * y asigna los valores del mismo en las variables de clase
     */
    public void getDatosAdverso(int clave){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_adversos.id AS id, " +
                   "       diagnostico.v_adversos.descripcion AS descripcion, " +
                   "       diagnostico.v_adversos.usuario AS usuario, " +
                   "       diagnostico.v_adversos.fecha AS fecha " +
                   "FROM diagnostico.v_adversos " +
                   "WHERE diagnostico.v_adversos.id = '" + clave + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // asignamos los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Descripcion = Resultado.getString("descripcion");
            this.Usuario = Resultado.getString("usuario");
            this.Alta = Resultado.getString("fecha");

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int la clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * en la base y retorna la id del registro afectado
     */
    public int grabaAdverso(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoAdverso();
        } else {
            this.editaAdverso();
        }

        // retornamos la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo síntoma adverso al tratamiento
     */
    protected void nuevoAdverso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.adversos " +
                   "       (descripcion, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?); ";
        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Descripcion);
            psInsertar.setInt(2, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición de un
     * síntoma adverso
     */
    protected void editaAdverso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.adversos SET " +
                   "       descripcion = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.adversos.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Descripcion);
            psInsertar.setInt(2, this.IdUsuario);
            psInsertar.setInt(3, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idadverso clave del registro
     * Método que recibe como parámetro la clave del registro
     * y elimina el efecto de la tabla
     */
    public void borraAdverso(int idadverso){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.adversos " +
                   "WHERE diagnostico.adversos.id = '" + idadverso + "';";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param descripcion - string con la descripcion
     * @return boolean
     * Método llamado en las altas que verifica no se
     * encuentre declarado el efecto adverso
     */
    public Boolean validaAdverso(String descripcion){

        // declaración de variables
        ResultSet Resultado;
        String Consulta;
        Boolean Correcto = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.v_adversos.id) AS registros " +
                   "FROM diagnostico.v_adversos " +
                   "WHERE diagnostico.v_adversos.descripcion = '" + descripcion + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // si encontró registros
            if (Resultado.getInt("registros") != 0){
                Correcto = false;
            } else {
                Correcto = true;
            }

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

        // retornamos
        return Correcto;

    }

}