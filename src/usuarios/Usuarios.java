/*

    Nombre: Usuarios
    Fecha: 28/05/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la base de datos
                 de usuarios autorizados

 */

// definición del paquete
package usuarios;

//importación de archivos
import java.awt.Image;
import java.io.FileInputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import funciones.Utilidades;
import seguridad.Seguridad;

// definición de la clase
public class Usuarios {

	// definición de variables de clase de la tabla de responsables
    protected int Id;                               // clave del registro
    protected String Nombre;                        // nombre del usuario
    protected String Institucion;                   // institución a la que pertenece
    protected String Cargo;                         // cargo del usuario
    protected String EMail;                         // email del responsable
    protected String Telefono;                      // telefono del responsable
    protected String Direccion;                     // dirección postal
    protected String CodigoPostal;                  // código postal correspondiente
    protected String Coordenadas;                   // coordenadas GPS
    protected int IdLaboratorio;                    // clave del laboratorio
    protected String Laboratorio;                   // nombre del laboratorio
    protected String Activo;                        // indica si está activo
    protected String Observaciones;                 // observaciones y comentarios
    protected String Usuario;                       // nombre del usuario
    protected String Password;                      // contraseña de ingreso
    protected String FechaAlta;                     // fecha de alta del registro
    protected String NivelCentral;                  // indica si es de nivel central
    protected String Localidad;                     // nombre de la localidad
    protected String IdLocalidad;                   // clave de la localidad
    protected String Provincia;                     // nombre de la provincia
    protected String Pais;                          // nombre del país
    protected int IdPais;                           // clave del país

    // definición de variables de clase de la tabla de usuarios
    protected int IdDiagnostico;                    // clave de la tabla de usuarios
    protected int IdAutorizo;                       // clave del usuario que autorizó
    protected String Autorizo;                      // usuario que autorizó el alta
    protected String Administrador;                 // indica si es administrador
    protected String Personas;                      // indica si puede modificar la tabla de personas
    protected String Congenito;                     // indica si puede modificar la tabla de congénito
    protected String Resultados;                    // indica si puede cargar resultados
    protected String Muestras;                      // indica si puede cargar muestras
    protected String Entrevistas;                   // indica si puede cargar entrevistas
    protected String Auxiliares;                    // indica si puede modificar tablas auxiliares
    protected String Protocolos;                    // indica si puede imprimir protocolos
    protected String Stock;                         // indica si puede modificar el stock
    protected String Usuarios;                      // indica si puede modificar los usuarios
    protected String Clinica;                       // indica si puede ingresar historias clínicas
    protected Image Firma;                          // firma del usuario
    protected FileInputStream Imagen;               // flujo de entrada de la firma
    protected int LongImagen;                       // longitud de la firma

    // puntero a la base de datos
    protected Conexion Enlace;

    // constructor de la clase
    public Usuarios(){

        // establecemos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.Id = 0;
        this.Nombre = "";
        this.Cargo = "";
        this.IdLaboratorio = 0;
        this.Laboratorio = "";
        this.Usuario = "";
        this.Password = "";
        this.FechaAlta = "";
        this.IdAutorizo = 0;
        this.Autorizo = "";
        this.Administrador = "";
        this.NivelCentral = "";
        this.Personas = "";
        this.Congenito = "";
        this.Resultados = "";
        this.Muestras = "";
        this.Entrevistas = "";
        this.Auxiliares = "";
        this.Protocolos = "";
        this.Stock = "";
        this.Usuarios = "";
        this.Clinica = "";
        this.Localidad = "";
        this.Provincia = "";
        this.Pais = "";
        this.Institucion = "";
        this.EMail = "";
        this.Telefono = "";
        this.Direccion = "";
        this.CodigoPostal = "";
        this.Coordenadas = "";
        this.Activo = "";
        this.Observaciones = "";
        this.IdDiagnostico = 0;
        this.IdLocalidad = "";
        this.IdPais = 0;
        this.LongImagen = 0;
        this.Firma = null;
        this.Imagen = null;

    }

    // métodos de asignación de variables
    public void setId(int id){
    	this.Id = id;
    }
    public void setNombre(String nombre){
    	this.Nombre = nombre;
    }
    public void setCargo(String cargo){
    	this.Cargo = cargo;
    }
    public void setIdLaboratorio(int idlaboratorio){
    	this.IdLaboratorio = idlaboratorio;
    }
    public void setLaboratorio(String laboratorio){
    	this.Laboratorio = laboratorio;
    }
    public void setUsuario(String usuario){
    	this.Usuario = usuario;
    }
    public void setAdministrador(String administrador){
    	this.Administrador = administrador;
    }
    public void setNivelCentral(String nivelcentral){
    	this.NivelCentral = nivelcentral;
    }
    public void setPersonas(String personas){
    	this.Personas = personas;
    }
    public void setCongenito(String congenito){
    	this.Congenito = congenito;
    }
    public void setResultados(String resultados){
    	this.Resultados = resultados;
    }
    public void setMuestras(String muestras){
    	this.Muestras = muestras;
    }
    public void setEntrevistas(String entrevistas){
    	this.Entrevistas = entrevistas;
    }
    public void setAuxiliares(String auxiliares){
    	this.Auxiliares = auxiliares;
    }
    public void setProtocolos(String protocolos){
    	this.Protocolos = protocolos;
    }
    public void setStock(String stock){
    	this.Stock = stock;
    }
    public void setUsuarios(String usuarios){
    	this.Usuarios = usuarios;
    }
    public void setClinica(String clinica){
        this.Clinica = clinica;
    }
    public void setIdLocalidad(String idlocalidad){
        this.IdLocalidad = idlocalidad;
    }
    public void setLocalidad(String localidad){
        this.Localidad = localidad;
    }
    public void setIdPais(int idpais){
        this.IdPais = idpais;
    }
    public void setPais(String pais){
        this.Pais = pais;
    }
    public void setProvincia(String provincia){
        this.Provincia = provincia;
    }
    public void setInstitucion(String institucion){
        this.Institucion = institucion;
    }
    public void setMail(String mail){
        this.EMail = mail;
    }
    public void setTelefono(String telefono){
        this.Telefono = telefono;
    }
    public void setDireccion(String direccion){
        this.Direccion = direccion;
    }
    public void setCodigoPostal(String codigopostal){
        this.CodigoPostal = codigopostal;
    }
    public void setActivo(String activo){
        this.Activo = activo;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }
    public void setIdDiagnostico (int iddiagnostico){
        this.IdDiagnostico = iddiagnostico;
    }
    public void setLongImagen(int longitud){
        this.LongImagen = longitud;
    }
    public void setImagen(FileInputStream imagen){
        this.Imagen = imagen;
    }

    // métodos de lectura de variables
    public String getActivo(){
    	return this.Activo;
    }
    public String getCoordenadas(){
    	return this.Coordenadas;
    }
    public String getCodigoPostal(){
    	return this.CodigoPostal;
    }
    public String getDireccion(){
    	return this.Direccion;
    }
    public String getTelefono(){
    	return this.Telefono;
    }
    public String getEMail(){
    	return this.EMail;
    }
    public String getInstitucion(){
    	return this.Institucion;
    }
    public int getIdDiagnostico(){
        return this.IdDiagnostico;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public int getIdUsuario(){
    	return this.Id;
    }
    public String getNombre(){
    	return this.Nombre;
    }
    public String getCargo(){
    	return this.Cargo;
    }
    public int getIdLaboratorio(){
    	return this.IdLaboratorio;
    }
    public String getLaboratorio(){
    	return this.Laboratorio;
    }
    public String getUsuario(){
    	return this.Usuario;
    }
    public String getFechaAlta(){
    	return this.FechaAlta;
    }
    public String getAutorizo(){
    	return this.Autorizo;
    }
    public String getAdministrador(){
    	return this.Administrador;
    }
    public String getNivelCentral(){
    	return this.NivelCentral;
    }
    public String getPersonas(){
    	return this.Personas;
    }
    public String getCongenito(){
    	return this.Congenito;
    }
    public String getResultados(){
    	return this.Resultados;
    }
    public String getMuestras(){
    	return this.Muestras;
    }
    public String getEntrevistas(){
    	return this.Entrevistas;
    }
    public String getAuxiliares(){
    	return this.Auxiliares;
    }
    public String getProtocolos(){
    	return this.Protocolos;
    }
    public String getStock(){
    	return this.Stock;
    }
    public String getUsuarios(){
    	return this.Usuarios;
    }
    public String getClinica(){
        return this.Clinica;
    }
    public String getLocalidad(){
    	return this.Localidad;
    }
    public String getIdLocalidad(){
        return this.IdLocalidad;
    }
    public String getProvincia(){
    	return this.Provincia;
    }
    public String getPais(){
    	return this.Pais;
    }
    public int getIdPais(){
        return this.IdPais;
    }
    public Image getFirma(){
        return this.Firma;
    }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * @return entero con la id del registro afectado
	  * Método que graba en la base de datos el registro del usuario y en todo caso
	  * actualiza los logs del sistema
	  */
	 public int grabaUsuario(){

		// si está editando la tabla de responsables de CCE
		if (this.Id != 0){
            this.editaUsuario();
        } else {
            this.nuevoUsuario();
		}

        /*
         * si el usuario seleccionó una imagen la actualizamos independientemente
         * esto lo hacemos así porque en una edición puede no haber seleccionado
         * una imagen y así evitamos pisarla con una cadena nula, o también en
         * un alta no es un campo obligatorio
         */
        if (this.Imagen != null){
            this.grabaFirma();
        }

		// retornamos la id del registro
	    return this.Id;

	 }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * Método que ejecuta la consulta de inserción
	  */
	 protected void nuevoUsuario(){

		  // declaración de variables
		  String Consulta = null;
		  PreparedStatement psInsertar = null;
	      Connection Puntero;

	      // primero actualizamos las coordenadas
	      String Domicilio = this.Direccion + " - " + this.Localidad + " - " + this.Provincia + " - " + this.Pais;
	      Utilidades Codificar = new Utilidades();
	      this.Coordenadas = Codificar.getCoordenadas(Domicilio);

	      // obtenemos la conexión a la base
	      Puntero = this.Enlace.getConexion();

	      // compone la consulta de inserción en la tabla de responsables
	      Consulta = "INSERT INTO cce.responsables "
                   + "       (NOMBRE, "
                   + "        INSTITUCION, "
                   + "        CARGO, "
                   + "        E_MAIL, "
                   + "        TELEFONO, "
                   + "        PAIS, "
                   + "        LOCALIDAD, "
                   + "        DIRECCION, "
                   + "        CODIGO_POSTAL, "
                   + "        COORDENADAS, "
                   + "        ACTIVO, "
                   + "        OBSERVACIONES, "
                   + "        USUARIO, "
                   + "        PASSWORD) "
                   + "       VALUES "
                   + "       (?,?,?,?,?,?,?,?,?,?,?,?,?,MD5(?));";

         try {

             // ahora preparamos la consulta
             psInsertar = Puntero.prepareStatement(Consulta);

             // asignamos los parámetros de la consulta
             psInsertar.setString(1, this.Nombre);
             psInsertar.setString(2, this.Institucion);
             psInsertar.setString(3, this.Cargo);
             psInsertar.setString(4, this.EMail);
             psInsertar.setString(5, this.Telefono);
             psInsertar.setInt(6, this.IdPais);
             psInsertar.setString(7, this.IdLocalidad);
             psInsertar.setString(8, this.Direccion);
             psInsertar.setString(9, this.CodigoPostal);
             psInsertar.setString(10, this.Coordenadas);
             psInsertar.setString(11, this.Activo);
             psInsertar.setString(12, this.Observaciones);
             psInsertar.setString(13, this.Usuario);
             psInsertar.setString(14, this.Usuario);

             // ejecuta la consulta
             psInsertar.executeUpdate();

         } catch (SQLException ex) {

             // presenta el mensaje de error
             System.out.println(ex.getMessage());

         }

		  // obtiene la clave del registro y la asigna a la clase
	      this.Id = this.Enlace.UltimoInsertado();

	      // inserta el registro en la base de diagnóstico
	      this.nuevoDiagnostico();

	 }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * Método que inserta un nuevo registro en la tabla de
	  * usuarios autorizados de la tabla de diagnóstico
	  */
	 protected void nuevoDiagnostico() {

		  // declaración de variables
		  String Consulta = null;
		  PreparedStatement psInsertar = null;
	      Connection Puntero;

	      // obtenemos la conexión a la base
	      Puntero = this.Enlace.getConexion();

	      // compone la consulta de inserción
		  Consulta = "INSERT INTO diagnostico.usuarios " +
		             "            (id," +
		             "             autorizo, " +
		             "             laboratorio, " +
		             "             administrador, " +
		             "             personas, " +
		             "             congenito, " +
		             "             resultados, " +
		             "             muestras, " +
		             "             entrevistas, " +
		             "             auxiliares, " +
		             "             protocolos, " +
		             "             stock, " +
                     "             usuarios, " +
                     "             clinica) " +
		             "            VALUES " +
		             "            (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		   try {

	            // asignamos la consulta
			    psInsertar = Puntero.prepareStatement(Consulta);

			    // asignamos los parámetros
			    psInsertar.setInt(1, this.Id);
	            psInsertar.setInt(2, Seguridad.Id);
	            psInsertar.setInt(3, this.IdLaboratorio);
	            psInsertar.setString(4, this.Administrador);
	            psInsertar.setString(5, this.Personas);
	            psInsertar.setString(6, this.Congenito);
	            psInsertar.setString(7, this.Resultados);
	            psInsertar.setString(8, this.Muestras);
	            psInsertar.setString(9, this.Entrevistas);
	            psInsertar.setString(10, this.Auxiliares);
	            psInsertar.setString(11, this.Protocolos);
	            psInsertar.setString(12, this.Stock);
                psInsertar.setString(13, this.Usuarios);
                psInsertar.setString(14, this.Clinica);

	            // ejecuta la consulta
	            psInsertar.executeUpdate();

	        // si hubo un error
           } catch (SQLException ex) {
               System.out.println(ex.getMessage());
           }

	 }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * Método que ejecuta la consulta de edición
	  */
	 protected void editaUsuario(){

		 // declaración de variables
		 String Consulta = null;
		 PreparedStatement psInsertar = null;
	     Connection Puntero;
	     ResultSet Resultado;

	     // primero actualizamos las coordenadas
	     String Domicilio = this.Direccion + " - " + this.Localidad + " - " + this.Provincia + " - " + this.Pais;
	     Utilidades Codificar = new Utilidades();
	     this.Coordenadas = Codificar.getCoordenadas(Domicilio);

	     // obtenemos la conexión a la base
	     Puntero = this.Enlace.getConexion();

         // compone la consulta de edición
         Consulta = "UPDATE cce.responsables SET "
                  + "       NOMBRE = ?, "
                  + "       INSTITUCION = ?, "
                  + "       CARGO = ?, "
                  + "       E_MAIL = ?, "
                  + "       TELEFONO = ?, "
                  + "       PAIS = ?, "
                  + "       LOCALIDAD = ?, "
                  + "       DIRECCION = ?, "
                  + "       CODIGO_POSTAL = ?, "
                  + "       COORDENADAS = ?, "
                  + "       ACTIVO = ?, "
                  + "       OBSERVACIONES = ?, "
                  + "       NIVEL_CENTRAL = ? "
                  + "WHERE cce.responsables.ID = ?;";

         try {

             // ahora preparamos la consulta
             psInsertar = Puntero.prepareStatement(Consulta);

             // asignamos los parámetros de la consulta
             psInsertar.setString(1, this.Nombre);
             psInsertar.setString(2, this.Institucion);
             psInsertar.setString(3, this.Cargo);
             psInsertar.setString(4, this.EMail);
             psInsertar.setString(5, this.Telefono);
             psInsertar.setInt(6, this.IdPais);
             psInsertar.setString(7, this.IdLocalidad);
             psInsertar.setString(8, this.Direccion);
             psInsertar.setString(9, this.CodigoPostal);
             psInsertar.setString(10, this.Coordenadas);
             psInsertar.setString(11, this.Activo);
             psInsertar.setString(12, this.Observaciones);
             psInsertar.setString(13, this.NivelCentral);
             psInsertar.setInt(14, this.Id);

             // ejecuta la consulta
             psInsertar.executeUpdate();

         // si hubo un error
         } catch (SQLException ex) {
             System.out.println(ex.getMessage());
         }

         // verifica si existe el usuario en la
         // tabla de diagnóstico
         Consulta = "SELECT COUNT(diagnostico.usuarios.id) AS registros " +
                    "FROM diagnostico.usuarios " +
        		    "WHERE diagnostico.usuarios.id = '" + this.Id + "'; ";
 		 Resultado = this.Enlace.Consultar(Consulta);

 		 try {

 			// nos desplazamos al primer registro
 			Resultado.next();

 			// si no hubo registros
			if (Resultado.getInt("registros") == 0) {
				this.nuevoDiagnostico();
			} else {
				this.editaDiagnostico();
			}

		 // si hubo un error
		 } catch (SQLException e) {
			e.printStackTrace();
		 }

	 }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que edita el registro de la tabla de usuarios
	 * autorizados de la tabla de diagnóstico
	 */
	protected void editaDiagnostico() {

		  // declaración de variables
		  String Consulta = null;
		  PreparedStatement psInsertar = null;
	      Connection Puntero;

	      // obtenemos la conexión a la base
	      Puntero = this.Enlace.getConexion();

	      // compone la consulta de edición
	      Consulta = "UPDATE diagnostico.usuarios SET " +
       		     	 "       autorizo = ?, " +
                     "       laboratorio = ?, " +
                     "       administrador = ?, " +
                     "       personas = ?, " +
                     "       congenito = ?, " +
                     "       resultados = ?, " +
                     "       muestras = ?, " +
                     "       entrevistas = ?, " +
                     "       auxiliares = ?, " +
                     "       protocolos = ?, " +
                     "       stock = ?, " +
                     "       usuarios = ?, " +
                     "       clinica = ? " +
                     "WHERE diagnostico.usuarios.id = ?;";
          try {

        	  // asignamos la consulta
        	  psInsertar = Puntero.prepareStatement(Consulta);

        	  // asignamos los parámetros
        	  psInsertar.setInt(1, Seguridad.Id);
        	  psInsertar.setInt(2, this.IdLaboratorio);
        	  psInsertar.setString(3, this.Administrador);
        	  psInsertar.setString(4, this.Personas);
        	  psInsertar.setString(5, this.Congenito);
        	  psInsertar.setString(6, this.Resultados);
        	  psInsertar.setString(7, this.Muestras);
        	  psInsertar.setString(8, this.Entrevistas);
        	  psInsertar.setString(9, this.Auxiliares);
        	  psInsertar.setString(10, this.Protocolos);
        	  psInsertar.setString(11, this.Stock);
              psInsertar.setString(12, this.Usuarios);
              psInsertar.setString(13, this.Clinica);
        	  psInsertar.setInt(14, this.Id);

        	  // ejecuta la consulta
        	  psInsertar.executeUpdate();

         // si hubo un error
         } catch (SQLException e) {
        	 e.printStackTrace();
         }

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que actualiza la firma del usuario
     */
    protected void grabaFirma(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // actualizamos en el registro
        Consulta = "UPDATE diagnostico.usuarios SET " +
                   "       firma = ? " +
                   "WHERE diagnostico.usuarios.id = ?;";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setBinaryStream(1,  this.Imagen, this.LongImagen);
            psInsertar.setInt(2, this.Id);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * @param idusuario entero con la clave del usuario
	  * Método que recibe como parámetro la id de un usuario y asigna en las
	  * variables de clase los valores del registro
	  */
	 public void getDatosUsuario(int idusuario){

		// declaración de variables
		ResultSet Resultado;
		String Consulta;
        Blob Archivo;

		 // componemos la consulta de la vista
		 Consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario, " +
		            "       diagnostico.v_usuarios.nombreusuario AS nombreusuario, " +
		            "       diagnostico.v_usuarios.cargousuario AS cargousuario, " +
		            "       diagnostico.v_usuarios.institucion AS institucion, " +
		            "       diagnostico.v_usuarios.telefono AS telefono, " +
		            "       diagnostico.v_usuarios.e_mail AS e_mail, " +
		            "       diagnostico.v_usuarios.direccion AS direccion, " +
		            "       diagnostico.v_usuarios.codigo_postal AS codigo_postal, " +
		            "       diagnostico.v_usuarios.coordenadas AS coordenadas, " +
		            "       diagnostico.v_usuarios.idlaboratorio AS idlaboratorio, " +
		            "       diagnostico.v_usuarios.nombrelaboratorio AS nombrelaboratorio, " +
		            "       diagnostico.v_usuarios.usuario AS usuario, " +
		            "       diagnostico.v_usuarios.fecha_alta AS fecha_alta, " +
		            "       diagnostico.v_usuarios.autorizo AS autorizo, " +
		            "       diagnostico.v_usuarios.administrador AS administrador, " +
		            "       diagnostico.v_usuarios.nivelcentral AS nivelcentral, " +
                    "       diagnostico.v_usuarios.activo AS activo, " +
		            "       diagnostico.v_usuarios.personas AS personas, " +
		            "       diagnostico.v_usuarios.congenito AS congenito, " +
		            "       diagnostico.v_usuarios.resultados AS resultados, " +
		            "       diagnostico.v_usuarios.muestras AS muestras, " +
		            "       diagnostico.v_usuarios.entrevistas AS entrevistas, " +
		            "       diagnostico.v_usuarios.auxiliares AS auxiliares, " +
		            "       diagnostico.v_usuarios.protocolos AS protocolos, " +
		            "       diagnostico.v_usuarios.stock AS stock, " +
                    "       diagnostico.v_usuarios.usuarios AS usuarios, " +
                    "       diagnostico.v_usuarios.clinica AS clinica, " +
                    "       diagnostico.v_usuarios.localidad AS localidad, " +
                    "       diagnostico.v_usuarios.codloc AS codloc, " +
		            "       diagnostico.v_usuarios.provincia AS provincia, " +
                    "       diagnostico.v_usuarios.pais AS pais, " +
                    "       diagnostico.v_usuarios.idpais AS idpais, " +
		            "       diagnostico.v_usuarios.firma AS firma, " +
		            "       diagnostico.v_usuarios.comentarios AS comentarios " +
		            "FROM diagnostico.v_usuarios " +
		            "WHERE diagnostico.v_usuarios.idusuario = '" + idusuario + "'; ";

		 // ejecutamos la consulta
		 Resultado = this.Enlace.Consultar(Consulta);

		 try {

			// si hubo registros
		    Resultado.next();

		    // asignamos a las variables de clase
		    this.Id = Resultado.getInt("idusuario");
		    this.IdDiagnostico = Resultado.getInt("idusuario");
		    this.Nombre = Resultado.getString("nombreusuario");
		    this.Institucion = Resultado.getString("institucion");
		    this.Telefono = Resultado.getString("telefono");
		    this.EMail = Resultado.getString("e_mail");
		    this.Cargo = Resultado.getString("cargousuario");
		    this.Direccion = Resultado.getString("direccion");
		    this.CodigoPostal = Resultado.getString("codigo_postal");
		    this.IdLaboratorio = Resultado.getInt("idlaboratorio");
		    this.Coordenadas = Resultado.getString("coordenadas");
		    this.Laboratorio = Resultado.getString("nombrelaboratorio");
		    this.Usuario = Resultado.getString("usuario");
		    this.FechaAlta = Resultado.getString("fecha_alta");
		    this.Autorizo = Resultado.getString("autorizo");
		    this.Administrador = Resultado.getString("administrador");
            this.Activo = Resultado.getString("activo");
		    this.NivelCentral = Resultado.getString("nivelcentral");
		    this.Personas = Resultado.getString("personas");
		    this.Congenito = Resultado.getString("congenito");
		    this.Resultados = Resultado.getString("resultados");
		    this.Muestras = Resultado.getString("muestras");
		    this.Entrevistas = Resultado.getString("entrevistas");
		    this.Auxiliares = Resultado.getString("auxiliares");
		    this.Protocolos = Resultado.getString("protocolos");
		    this.Stock = Resultado.getString("stock");
            this.Usuarios = Resultado.getString("usuarios");
            this.Clinica = Resultado.getString("clinica");
            this.Pais = Resultado.getString("pais");
            this.IdPais = Resultado.getInt("idpais");
            this.Localidad = Resultado.getString("localidad");
            this.IdLocalidad = Resultado.getString("codloc");
		    this.Provincia = Resultado.getString("provincia");
		    this.Observaciones = Resultado.getString("comentarios");

		    // verificamos que no halla retornado valores nulos
		    if (this.Administrador == null) {
		    	this.Administrador = "No";
		    }
		    if (this.Activo == null) {
		    	this.Activo = "No";
		    }
            if (this.NivelCentral == null) {
            	this.NivelCentral = "No";
            }
		    if (this.Personas == null) {
		    	this.Personas = "No";
		    }
		    if (this.Congenito == null) {
		    	this.Congenito = "No";
		    }
		    if (this.Resultados == null) {
		    	this.Resultados = "No";
		    }
		    if (this.Muestras == null) {
		    	this.Muestras = "No";
		    }
		    if (this.Entrevistas == null) {
		    	this.Entrevistas = "No";
		    }
		    if (this.Auxiliares == null) {
		    	this.Auxiliares = "No";
		    }
		    if (this.Protocolos == null) {
		    	this.Protocolos = "No";
		    }
		    if (this.Stock == null) {
		    	this.Stock = "No";
		    }
		    if (this.Usuarios == null) {
		    	this.Usuarios = "No";
            }
            if (this.Clinica == null){
                this.Clinica = "No";
            }

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("firma") != null){
                Archivo = Resultado.getBlob("firma");
                this.Firma = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            } else {
                this.Firma = null;
            }

		// si ocurrió un error
		} catch (SQLException e) {
			e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

	 }

	/**
	  * @param nuevoPassword string con el nuevo password
	  * @return boolean el resultado de la operacion
	  * Método que actualiza el password de ingreso del usuario
	  */
	 public boolean actualizaPassword(String nuevoPassword){

		// declaración de variables
		int Resultado;
		String Consulta;

		// componemos la consulta y la ejecutamos
		Consulta = "UPDATE cce.responsables SET " +
	               "       password = MD5('" + nuevoPassword + "') " +
	               "WHERE cce.responsables.id = '" + Seguridad.Id + "'; ";
		Resultado = this.Enlace.Ejecutar(Consulta);

		// retorna el resultado de la operación
		if (Resultado == 0){
			return false;
		} else {
			return true;
		}

	 }

	 /**
	  * @param texto string con el texto a buscar
	  * @return vector con los usuarios encontrados
	  * Método que recibe como parámetro un texto y retorna un resultset
	  * con los usuarios que coinciden con la cadena
	  */
	 public ResultSet buscaUsuario(String texto){

		// declaración de variables
		ResultSet Resultado;
		String Consulta;

		// componemos y ejecutamos la consulta
		Consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario, " +
	               "       diagnostico.v_usuarios.nombreusuario AS nombreusuario, " +
	               "       diagnostico.v_usuarios.nombrelaboratorio AS laboratorio, " +
	               "       diagnostico.v_usuarios.provincia AS provincia " +
	               "FROM diagnostico.v_usuarios " +
	               "WHERE diagnostico.v_usuarios.nombreusuario LIKE '%" + texto + "%' OR " +
	               "      diagnostico.v_usuarios.institucion LIKE '%" + texto + "%' OR " +
	               "      diagnostico.v_usuarios.provincia LIKE '%" + texto + "%' " +
	               "ORDER BY diagnostico.v_usuarios.provincia, " +
	               "         diagnostico.v_usuarios.nombreusuario; ";
		Resultado = this.Enlace.Consultar(Consulta);

		// retornamos el vector
		return Resultado;

	 }

    /**
     * @param nombreusuario String el nombre de usuario
     * @return boolean verdadero o falso
     * Método que verifica si existe un nombre de usuario en la base de
     * datos en cuyo caso retorna verdadero
     */
    public boolean VerificaUsuario(String nombreusuario){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // primero verifica que el nombre de usuario no se encuentre
        // repetido en la base de usuarios
        Consulta = "SELECT COUNT(diagnostico.v_usuarios.idusuario) AS registros "
                 + " FROM diagnostico.v_usuarios "
                 + " WHERE diagnostico.v_usuarios.usuario = '" + nombreusuario + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            Resultado.next();

            // si hubo registros
            if (Resultado.getInt("registros") == 0){
            	return true;
            } else {
            	return false;
            }

        // si hubo un error
        } catch (SQLException ex){
            return false;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param email dirección de correo electrónico
     * @return boolean si es correcto o no
     * Método utilizado para evitar los mail duplicados
     */
    public boolean verificaMail(String email){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // primero verifica que el nombre de usuario no se encuentre
        // repetido en la base de usuarios
        Consulta = "SELECT COUNT(diagnostico.v_usuarios.idusuario) AS registros "
                 + " FROM diagnostico.v_usuarios "
                 + " WHERE diagnostico.v_usuarios.e_mail = '" + email + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            Resultado.next();

            // si hubo registros
            if (Resultado.getInt("registros") == 0){
            	return true;
            } else {
            	return false;
            }

        // si hubo un error
        } catch (SQLException ex){
            return false;
        }

    }

	 /**
	  * @param jurisdiccion string con el nombre de la jurisdicción
	  * @param pais string con el nombre del país
	  * @return resultset con la nómina de responsables correspondientes
	  * a esa jurisdicción
	  */
	 public ResultSet getNominaUsuarios(String jurisdiccion, String pais){

		 // declaración de variables
		 String Consulta;
		 ResultSet Resultado;

		 // componemos y ejecutamos la consulta
		 Consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario, " +
		            "       diagnostico.v_usuarios.nombreusuario AS nombreusuario " +
		            "       diagnostico.v_usuarios.provincia AS provincia " +
		            "FROM diagnostico.v_usuarios " +
		            "WHERE diagnostico.v_usuarios.provincia = '" + jurisdiccion + "' AND " +
		            "      diagnostico.v_usuarios.pais = '" + pais + "' " +
		            "ORDER diagnostico.v_usuarios.nombreusuario; ";
		  Resultado = this.Enlace.Consultar(Consulta);

		  // retorna el vector
		  return Resultado;

	 }

	 /**
	  * @param nombre string con el nombre completo
	  * @param pais string con el nombre del país
	  * @param jurisdiccion string con el nombre de la jurisdicción
	  * @return entero con la clave del responsable
	  * Método que recibe el nombre completo del usuario, la jurisdicción y el país
	  * y retorna la clave del usuario, utilizado en el abm de laboratorios para
	  * indicar el responsable de recibir las muestras
	  */
	 public int getIdResponsable(String nombre, String pais, String jurisdiccion){

		 // declaración de variables
		 String Consulta;
		 ResultSet Resultado;
		 int Clave = 0;

		 // componemos la consulta
		 Consulta = "SELECT diagnostico.v_usuarios.idusuario AS id_responsable " +
		            "FROM diagnostico.v_usuarios " +
		            "WHERE diagnostico.v_usuarios.nombreusuario = '" + nombre + "' AND " +
		            "      diagnostico.v_usuarios.provincias = '" + jurisdiccion + "' AND " +
		            "      diagnostico.v_usuarios.pais = '" + pais + "'; ";
		 Resultado = this.Enlace.Consultar(Consulta);

		 try {

			// obtenemos el registro
			Resultado.next();
			Clave = Resultado.getInt("id_responsable");

		// si hubo un error
		} catch (SQLException ex){
		      System.out.println(ex.getMessage());
		}

	  // retornamos la clave
	  return Clave;

	 }

	 /**
	  * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	  * @return resultset con los usuarios del laboratorio activo
	  * Método que retorna la nómina de usuarios autorizados del laboratorio activo
	  * utilizado en el control de stock
	  */
	 public ResultSet nominaUsuariosLaboratorio(){

	     // declaración de variables
	     ResultSet nominaUsuarios;
	     String Consulta;

	     // componemos la consulta
	     Consulta = "SELECT diagnostico.v_usuarios.idusuario AS idusuario, " +
	                "       diagnostico.v_usuarios.usuario AS usuario " +
	                "FROM diagnostico.v_usuarios " +
	                "WHERE diagnostico.v_usuarios.idlaboratorio = '" + Seguridad.Laboratorio + "' " +
	                "ORDER BY diagnostico.v_usuarios.usuario; ";
	     nominaUsuarios = this.Enlace.Consultar(Consulta);

	     // retornamos el vector
	     return nominaUsuarios;

	 }

}
