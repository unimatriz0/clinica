/*

    Nombre: FormUsuarios
    Fecha: 02/03/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario del abm de usuarios

*/

// definición del paquete
package usuarios;

// importamos las librerías
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JTextField;
import contenedor.FormContenedor;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextArea;
import funciones.Imagenes;
import funciones.Mensaje;
import funciones.Utilidades;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import laboratorios.Laboratorios;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import seguridad.Seguridad;
import java.awt.Font;

// definimos la clase heredando el dialog e implementado
public class FormUsuarios extends JPanel {

    // agrega el serial id
    private static final long serialVersionUID = 4044680279842077963L;

    // declaramos las variables
    private JTextField tId;
    private JTextField tNombre;
    private JTextField tUsuario;
    private JTextField tCargo;
    public JTextField tInstitucion;
    private JButton btnBuscaInstitucion;
    private JTextField tTelefono;
    private JTextField tEmail;
    private JTextField tDireccion;
    private JTextField tCodigoPostal;
    private JTextField tAutorizo;
    public JTextField tPais;
    public JTextField tJurisdiccion;
    public JTextField tLocalidad;
    private JTextField tAlta;
    private JCheckBox cActivo;
    private JCheckBox cNivelCentral;
    private JCheckBox cAdministrador;
    private JCheckBox cPersonas;
    private JCheckBox cCongenito;
    private JCheckBox cResultados;
    private JCheckBox cMuestras;
    private JCheckBox cEntrevistas;
    private JCheckBox cAuxiliares;
    private JCheckBox cStock;
    private JCheckBox cUsuarios;
    private JCheckBox cProtocolos;
    private JCheckBox cHistorias;
    private JLabel lFirma;
    private JTextArea tComentarios;
    private JButton btnAceptar;
    public String CodLoc;                       // clave indec de la localidad
    public int IdPais;                          // clave del país
    public int IdInstitucion;                   // clave de la institución
    private String Archivo;                     // puntero del archivo de imagen
    private FileInputStream Contenido;          // contenido del archivo
    private int Longitud;                       // tamaño del archivo
    private Laboratorios Instituciones;         // clase de laboratorios
    private Usuarios Responsables;              // clase de los usuarios
    private FormContenedor Contenedor;          // frame principal

    // constructor de la clase
    public FormUsuarios(FormContenedor contenedor) {

        // fijamos el contenedor
        this.Contenedor = contenedor;

        // fijamos las propiedades
        this.setLayout(null);

        // inicializamos las variables
        this.CodLoc = "";
        this.IdInstitucion = 0;
        this.IdPais = 0;
        this.Archivo = "";
        this.Contenido = null;
        this.Longitud = 0;
        Instituciones = new Laboratorios();
        Responsables = new Usuarios();

        // configuramos el formulario
        this.initForm();

    }

    // método que inicializa los componentes del formulario
    private void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta el título
        JLabel lTitulo = new JLabel("<html><b>Usuarios Registrados</b></html>");
        lTitulo.setBounds(10, 5, 710, 26);
        lTitulo.setFont(Fuente);
        this.add(lTitulo);

        // presenta la clave del registro (solo lectura)
        JLabel lId = new JLabel("<html><b>ID:</b></html>");
        lId.setBounds(10, 40, 25, 26);
        lId.setFont(Fuente);
        this.add(lId);
        this.tId = new JTextField();
        this.tId.setBounds(30, 40, 50, 26);
        this.tId.setFont(Fuente);
        this.tId.setToolTipText("Clave del usuario");
        this.tId.setEditable(false);
        this.add(this.tId);

        // pide el nombre del usuario
        JLabel lNombre = new JLabel("<html><b>Nombre:</b></html>");
        lNombre.setBounds(85, 40, 70, 26);
        lNombre.setFont(Fuente);
        this.add(lNombre);
        this.tNombre = new JTextField();
        this.tNombre.setBounds(152, 40, 470, 26);
        this.tNombre.setFont(Fuente);
        this.tNombre.setToolTipText("Nombre completo del usuario");
        this.tNombre.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaNombre(evt);
            }
        });
        this.add(this.tNombre);

        // pide el usuario
        JLabel lUsuario = new JLabel("<html><b>Usuario:</b></html>");
        lUsuario.setBounds(630, 40, 70, 26);
        lUsuario.setFont(Fuente);
        this.add(lUsuario);
        this.tUsuario = new JTextField();
        this.tUsuario.setBounds(690, 40, 95, 26);
        this.tUsuario.setFont(Fuente);
        this.tUsuario.setToolTipText("Nombre de usuario para el ingreso");
        this.tUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaUsuario(evt);
            }
        });
        this.add(this.tUsuario);

        // la firma
        this.lFirma = new JLabel("");
        this.lFirma.setBounds(800, 40, 130, 130);
        this.lFirma.setToolTipText("Pulse para cargar la firma del usuario");
        this.lFirma.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
        this.lFirma.addMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseClicked(java.awt.event.MouseEvent evt) {
                lFirmaPulsada();
            }
        });
        this.add(this.lFirma);

        // pide el cargo
        JLabel lCargo = new JLabel("<html><b>Cargo:</b></html>");
        lCargo.setBounds(10, 75, 70, 26);
        lCargo.setFont(Fuente);
        this.add(lCargo);
        this.tCargo = new JTextField();
        this.tCargo.setBounds(55, 75, 565, 26);
        this.tCargo.setFont(Fuente);
        this.tCargo.setToolTipText("Cargo que ocupa en el laboratorio");
        this.tCargo.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyPressed(java.awt.event.KeyEvent evt) {
               pulsaCargo(evt);
            }
        });
        this.add(this.tCargo);

        // el usuario que lo autorizó (solo lectura)
        JLabel lAutorizo = new JLabel("<html><b>Autorizó:</b></html>");
        lAutorizo.setBounds(630, 75, 70, 26);
        lAutorizo.setFont(Fuente);
        this.add(lAutorizo);
        this.tAutorizo = new JTextField();
        this.tAutorizo.setBounds(690, 75, 95, 26);
        this.tAutorizo.setFont(Fuente);
        this.tAutorizo.setToolTipText("Usuario que autorizó el ingreso");
        this.tAutorizo.setEditable(false);
        this.add(this.tAutorizo);

        // pide la institución
        JLabel lInstitucion = new JLabel("<html><b>Institución:</b></html>");
        lInstitucion.setBounds(10, 105, 90, 26);
        lInstitucion.setFont(Fuente);
        this.add(lInstitucion);
        this.tInstitucion = new JTextField();
        this.tInstitucion.setBounds(85, 105, 523, 26);
        this.tInstitucion.setFont(Fuente);
        this.tInstitucion.setToolTipText("Institución en la que se desempeña");
        this.tInstitucion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaInstitucion(evt);
            }
        });
        this.add(this.tInstitucion);

        // presenta el botón de buscar institución
        this.btnBuscaInstitucion = new JButton();
        this.btnBuscaInstitucion.setBounds(610, 105, 26, 26);
        this.btnBuscaInstitucion.setFont(Fuente);
        this.btnBuscaInstitucion.setToolTipText("Pulse para buscar la institución");
        this.btnBuscaInstitucion.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        this.btnBuscaInstitucion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                buscaInstitucion();
            }
        });
        this.add(this.btnBuscaInstitucion);

        // la fecha de alta (solo lectura)
        JLabel lAlta = new JLabel("<html><b>Alta:</b></html>");
        lAlta.setBounds(650, 105, 54, 26);
        lAlta.setFont(Fuente);
        this.add(lAlta);
        this.tAlta = new JTextField();
        this.tAlta.setBounds(690, 105, 95, 26);
        this.tAlta.setFont(Fuente);
        this.tAlta.setToolTipText("Fecha de alta del registro");
        this.tAlta.setEditable(false);
        this.add(this.tAlta);

        // pide el teléfono
        JLabel lTelefono = new JLabel("<html><b>Teléfono:</b></html>");
        lTelefono.setBounds(10, 140, 70, 26);
        lTelefono.setFont(Fuente);
        this.add(lTelefono);
        this.tTelefono = new JTextField();
        this.tTelefono.setBounds(85, 140, 145, 26);
        this.tTelefono.setFont(Fuente);
        this.tTelefono.setToolTipText("Teléfono de la institución con prefijo");
        this.tTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTelefono(evt);
            }
        });
        this.add(this.tTelefono);

        // pide el mail
        JLabel lEmail = new JLabel("<html><b>E-Mail:</b></html>");
        lEmail.setBounds(235, 140, 70, 26);
        lEmail.setFont(Fuente);
        this.add(lEmail);
        this.tEmail = new JTextField();
        this.tEmail.setBounds(285, 140, 235, 26);
        this.tEmail.setFont(Fuente);
        this.tEmail.setToolTipText("Correo electrónico del usuario");
        this.tEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaMail(evt);
            }
        });
        this.add(this.tEmail);

        // indica si está activo
        this.cActivo = new JCheckBox("Activo");
        this.cActivo.setBounds(528, 140, 79, 26);
        this.cActivo.setFont(Fuente);
        this.cActivo.setToolTipText("Indica si el usuario está activo");
        this.add(this.cActivo);

        // pide la dirección
        JLabel lDireccion = new JLabel("<html><b>Dirección:</b></html>");
        lDireccion.setBounds(10, 175, 70, 26);
        lDireccion.setFont(Fuente);
        this.add(lDireccion);
        this.tDireccion = new JTextField();
        this.tDireccion.setBounds(85, 175, 434, 26);
        this.tDireccion.setFont(Fuente);
        this.tDireccion.setToolTipText("Dirección postal del laboratorio");
        this.tDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaDireccion(evt);
            }
        });
        this.add(this.tDireccion);

        // pide el código postal
        JLabel lCP = new JLabel("<html><b>CP:</b></html>");
        lCP.setBounds(530, 175, 34, 26);
        lCP.setFont(Fuente);
        this.add(lCP);
        this.tCodigoPostal = new JTextField();
        this.tCodigoPostal.setBounds(560, 175, 95, 26);
        this.tCodigoPostal.setFont(Fuente);
        this.tCodigoPostal.setToolTipText("Código Postal de la dirección");
        this.tCodigoPostal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaCodigo(evt);
            }
        });
        this.add(this.tCodigoPostal);

        // el país (solo lectura)
        JLabel lPais = new JLabel("<html><b>País:</b></html>");
        lPais.setBounds(10, 210, 45, 26);
        lPais.setFont(Fuente);
        this.add(lPais);
        this.tPais = new JTextField();
        this.tPais.setBounds(45, 210, 190, 26);
        this.tPais.setFont(Fuente);
        this.tPais.setToolTipText("País del laboratorio");
        this.tPais.setEditable(false);
        this.add(this.tPais);

        // la jurisdicción (solo lectura)
        JLabel lJurisdiccion = new JLabel("<html><b>Jurisdicción:</b></html>");
        lJurisdiccion.setBounds(250, 210, 95, 26);
        lJurisdiccion.setFont(Fuente);
        this.add(lJurisdiccion);
        this.tJurisdiccion = new JTextField();
        this.tJurisdiccion.setBounds(335, 210, 240, 26);
        this.tJurisdiccion.setFont(Fuente);
        this.tJurisdiccion.setToolTipText("Jurisdicción del laboratorio");
        this.add(this.tJurisdiccion);

        // la localidad
        JLabel lLocalidad = new JLabel("<html><b>Localidad:</b></html>");
        lLocalidad.setBounds(580, 210, 130, 26);
        lLocalidad.setFont(Fuente);
        this.add(lLocalidad);
        this.tLocalidad = new JTextField();
        this.tLocalidad.setBounds(655, 210, 255, 26);
        this.tLocalidad.setFont(Fuente);
        this.tLocalidad.setToolTipText("Ingrese parte del nombre y pulse buscar");
        this.add(this.tLocalidad);

        // el label de tablas autorizadas
        JLabel lTablas = new JLabel("<html><b>Tablas Autorizadas</b></html>");
        lTablas.setBounds(10, 245, 175, 26);
        lTablas.setFont(Fuente);
        this.add(lTablas);

        // si es administrador
        this.cAdministrador = new JCheckBox("Administrador");
        this.cAdministrador.setBounds(10, 280, 130, 26);
        this.cAdministrador.setFont(Fuente);
        this.cAdministrador.setToolTipText("Indique si el usuario será Administrador");
        this.add(this.cAdministrador);

        // si es de nivel central
        this.cNivelCentral = new JCheckBox("Nivel Central");
        this.cNivelCentral.setBounds(140, 280, 130, 26);
        this.cNivelCentral.setFont(Fuente);
        this.cNivelCentral.setToolTipText("Indique si el usuario es de Nivel Central");
        this.add(this.cNivelCentral);

        // si está autorizado para personas
        this.cPersonas = new JCheckBox("Personas");
        this.cPersonas.setBounds(270, 280, 105, 26);
        this.cPersonas.setFont(Fuente);
        this.cPersonas.setToolTipText("Indica si el usuario puede ingresar pacientes");
        this.add(this.cPersonas);

        // si está autorizado para chagas congénito
        this.cCongenito = new JCheckBox("Congénito");
        this.cCongenito.setBounds(390, 280, 105, 26);
        this.cCongenito.setFont(Fuente);
        this.cCongenito.setToolTipText("Indica si puede ingresar chagas congénito");
        this.add(this.cCongenito);

        // si puede ingresar stock
        this.cStock = new JCheckBox("Stock");
        this.cStock.setBounds(500, 280, 75, 26);
        this.cStock.setFont(Fuente);
        this.cStock.setToolTipText("Indica si puede ingresar stock");
        this.add(this.cStock);

        // si puede firmar protocolos
        this.cProtocolos = new JCheckBox("Protocolos");
        this.cProtocolos.setBounds(590, 280, 130, 26);
        this.cProtocolos.setFont(Fuente);
        this.cProtocolos.setToolTipText("Indica si puede firmar protocolos");
        this.add(this.cProtocolos);

        // si está autorizado a cargar resultados
        this.cResultados = new JCheckBox("Resultados");
        this.cResultados.setBounds(10, 315, 130, 26);
        this.cResultados.setFont(Fuente);
        this.cResultados.setToolTipText("Indica si puede cargar resultados");
        this.add(this.cResultados);

        // si está autorizado a tomar muestras
        this.cMuestras = new JCheckBox("Muestras");
        this.cMuestras.setBounds(140, 315, 130, 26);
        this.cMuestras.setFont(Fuente);
        this.cMuestras.setToolTipText("Indique si puede tomar muestras");
        this.add(this.cMuestras);

        // si puede realizar entrevistas
        this.cEntrevistas = new JCheckBox("Entrevistas");
        this.cEntrevistas.setBounds(270, 315, 115, 26);
        this.cEntrevistas.setFont(Fuente);
        this.cEntrevistas.setToolTipText("Indique si puede tomar entrevistas");
        this.add(this.cEntrevistas);

        // si puede modificar tablas auxiliares
        this.cAuxiliares = new JCheckBox("Auxiliares");
        this.cAuxiliares.setBounds(390, 315, 95, 26);
        this.cAuxiliares.setFont(Fuente);
        this.cAuxiliares.setToolTipText("Indica si podrá modificar tablas auxiliares");
        this.add(this.cAuxiliares);

        // si puede editar la tabla usuarios
        this.cUsuarios = new JCheckBox("Usuarios");
        this.cUsuarios.setBounds(500, 315, 105, 26);
        this.cUsuarios.setFont(Fuente);
        this.cUsuarios.setToolTipText("Indica si puede autorizar otros usuarios");
        this.add(this.cUsuarios);

        // si puede grabar clínica médica
        this.cHistorias = new JCheckBox("Historias Clínicas");
        this.cHistorias.setBounds(590, 315, 150, 26);
        this.cHistorias.setFont(Fuente);
        this.cHistorias.setToolTipText("Indica si puede ingresar historias clínicas");
        this.add(this.cHistorias);

        // el label de comentarios
        JLabel lComentarios = new JLabel("<html><b>Comentarios y Observaciones</b></html>");
        lComentarios.setBounds(10, 350, 400, 26);
        lComentarios.setFont(Fuente);
        this.add(lComentarios);

        // definimos el scroll de los comentarios
        JScrollPane scrollComentarios = new JScrollPane();
        scrollComentarios.setBounds(10, 380, 905, 170);
        this.add(scrollComentarios);

        // los comentarios
        this.tComentarios = new JTextArea();
        this.tComentarios.setFont(Fuente);
        this.tComentarios.setToolTipText("Ingrese sus comentarios y observaciones");

        // agregamos los comentarios al scroll
        scrollComentarios.setViewportView(this.tComentarios);

        // el botón grabar
        this.btnAceptar = new JButton("Aceptar");
        this.btnAceptar.setBounds(780, 280, 115, 26);
        this.btnAceptar.setFont(Fuente);
        this.btnAceptar.setToolTipText("Pulse para grabar el registro");
        this.btnAceptar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        this.btnAceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                validaFormulario();
            }
        });
        this.add(this.btnAceptar);

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(780, 315, 115, 26);
        btnCancelar.setFont(Fuente);
        btnCancelar.setToolTipText("Reinicia el formulario");
        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                limpiaFormulario();
            }
        });
        this.add(btnCancelar);

        // si no es administrador
        if (Seguridad.Administrador.equals("No")){

            // desactiva el botón grabar
            btnCancelar.setEnabled(false);

        }

        // por defecto mostramos el usuario actual
        this.verUsuario(Seguridad.Id);

        // fijamos el foco
        this.tNombre.requestFocus();

    }

    // método llamado al pulsar sobre el nombre
    private void pulsaNombre(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tUsuario.requestFocus();
        }

    }

	// método llamado al pulsar sobre el usuario
	private void pulsaUsuario(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCargo.requestFocus();
		}

	}

	// método llamado al pulsar sobre el cargo
	private void pulsaCargo(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tInstitucion.requestFocus();
		}

	}

	// método llamado al pulsar sobre la institución
	private void pulsaInstitucion(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.buscaInstitucion();
		}

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * método llamado al pulsar enter sobre la institucion o al
     * hacer click sobre el botón buscar que abre el cuadro
     * emergente y muestra la lista de instituciones
     */
    private void buscaInstitucion(){

    	// declaración de variables
    	ResultSet nomina;

    	// verificamos que se halla ingresado un texto
    	if (this.tInstitucion.getText().isEmpty()){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Debe ingresar parte de la institución",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

    	}

    	// obtenemos la matriz
    	nomina = this.Instituciones.buscaLaboratorio(this.tInstitucion.getText());

        try {
            // si hay registros
            if (nomina.next()){

                // instanciamos el formulario de búsqueda
                FormBuscaLaboratorio Laboratorios = new FormBuscaLaboratorio(this.Contenedor, true, this);
                Laboratorios.cargaLaboratorios(nomina);
                Laboratorios.setVisible(true);

            } else {

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this,
                                              "No hay registros coincidentes",
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);

            }

        // si hubo un error
        } catch (SQLException ex) {
            Logger.getLogger(FormUsuarios.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

	// método llamado al pulsar sobre el teléfono
	private void pulsaTelefono(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tEmail.requestFocus();
		}

	}

	// método llamado al pulsar sobre el teléfono
	private void pulsaMail(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tDireccion.requestFocus();
		}

	}

	// método llamado al pulsar sobre la dirección
	private void pulsaDireccion(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tCodigoPostal.requestFocus();
		}

	}

	// método llamado al pulsar sobre el código postal
	private void pulsaCodigo(java.awt.event.KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tLocalidad.requestFocus();
		}

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la imagen
     */
    private void lFirmaPulsada() {

        // obtenemos el archivo
        Imagenes foto = new Imagenes();
        foto.leerImagen();

        // si seleccionó un archivo
        if (!foto.getArchivo().isEmpty()){

            // lo asignamos al label redimensionando la imagen
            ImageIcon imagen = foto.cargarImagen(130,130);
            this.lFirma.setIcon(imagen);

            // asignamos la ruta de la imagen
            this.Archivo = foto.getArchivo();

            // obtenemos el archivo y sus propiedades
            foto.obtenerImagen();

            // asignamos en las variables de clase
            this.Contenido = foto.getContenido();
            this.Longitud = foto.getLongitud();

        // si canceló
        } else {

            // inicializamos la variable
            this.Archivo = "";

        }

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón grabar que valida
     * los datos del formulario antes de enviarlo al servidor
     */
    protected void validaFormulario(){

        // verifica se halla seleccionado el nombre
        if (this.tNombre.getText().isEmpty()){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Debe ingresar el nombre completo del usuario",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

        }

        // si está dando un alta verifica el nombre de usuario
        if (this.tId.getText().isEmpty()){

            // verifica que halla ingresado
            if (this.tUsuario.getText().isEmpty()){

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this,
                                              "Debe ingresar el nombre del usuario",
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);
                return;

            // si ingresó
            } else {

                // verifica que el usuario no esté repetido
                if (!this.Responsables.VerificaUsuario(this.tUsuario.getText())){

                    // presenta el mensaje y retorna
                    JOptionPane.showMessageDialog(this,
                                                  "Ese usuario ya está en uso",
                                                  "Error",
                                                  JOptionPane.ERROR_MESSAGE);
                    return;

                }

            }

        }

        // verifica se halla indicado el cargo
        if (this.tCargo.getText().isEmpty()){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Indique el cargo que ocupa",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

        }

        // verifica se halla ingresado el laboratorio
        if (this.IdInstitucion == 0){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Debe indicar la institución o laboratorio",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

        }

        // si cargó el laboratorio también tenemos
        // la dirección, código postal, país
        // provincia y localidad

        // el teléfono lo permite en blanco

        // verifica se halla ingresado el mail
        if (this.tEmail.getText().isEmpty()){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Debe ingresar el mail del usuario",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

        // si ingresó mail
        } else {

            // si está dando un alta
            if (this.tId.getText().isEmpty()){

                // verifica que sea correcto
                Utilidades Herramientas = new Utilidades();
                if (!Herramientas.esEmailCorrecto(this.tEmail.getText())){

                    // presenta el mensaje y retorna
                    JOptionPane.showMessageDialog(this,
                                                  "El formato del mail parece incorrecto",
                                                  "Error",
                                                  JOptionPane.ERROR_MESSAGE);
                    return;

                // verifica que no esté repetido
                } else if (!Responsables.verificaMail(this.tEmail.getText())){

                    // presenta el mensaje y retorna
                    JOptionPane.showMessageDialog(this,
                                                  "Esa dirección de mail está en uso",
                                                  "Error",
                                                  JOptionPane.ERROR_MESSAGE);
                    return;

                }

            }

        }

        // si puede firmar protocolos debe cargar la imagen
        if (this.cProtocolos.isSelected() && this.Archivo.isEmpty()){

    		// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
                                          "Debe registrar la firma del usuario",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
			return;

        }

        // si llegó hasta aquí graba el registro
        this.grabaUsuario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de validar el formulario que
     * ejecuta la consulta en la base
     */
    protected void grabaUsuario(){

        // asignamos en la clase

        // si está dando altas
        if(this.tId.getText().isEmpty()){
            this.Responsables.setUsuario(this.tUsuario.getText());
            this.Responsables.setId(0);
        } else {
            this.Responsables.setId(Integer.parseInt(this.tId.getText()));
        }
        this.Responsables.setNombre(this.tNombre.getText());
        this.Responsables.setCargo(this.tCargo.getText());
        this.Responsables.setIdLaboratorio(this.IdInstitucion);
        this.Responsables.setDireccion(this.tDireccion.getText());
        this.Responsables.setCongenito(this.tCodigoPostal.getText());
        this.Responsables.setIdLocalidad(this.CodLoc);
        this.Responsables.setLocalidad(this.tLocalidad.getText());
        this.Responsables.setProvincia(this.tJurisdiccion.getText());
        this.Responsables.setPais(this.tPais.getText());
        this.Responsables.setIdPais(this.IdPais);
        this.Responsables.setMail(this.tEmail.getText());
        this.Responsables.setTelefono(this.tTelefono.getText());
        this.Responsables.setObservaciones(this.tComentarios.getText());

        // según el estado de los checkbox
        if (this.cNivelCentral.isSelected()){
            this.Responsables.setNivelCentral("Si");
        } else {
            this.Responsables.setNivelCentral("No");
        }
        if (this.cAdministrador.isSelected()){
            this.Responsables.setAdministrador("Si");
        } else {
            this.Responsables.setAdministrador("No");
        }
        if (this.cActivo.isSelected()){
            this.Responsables.setActivo("Si");
        } else {
            this.Responsables.setActivo("No");
        }
        if (this.cPersonas.isSelected()){
            this.Responsables.setPersonas("Si");
        } else {
            this.Responsables.setPersonas("No");
        }
        if (this.cCongenito.isSelected()){
            this.Responsables.setCongenito("Si");
        } else {
            this.Responsables.setCongenito("No");
        }
        if (this.cResultados.isSelected()){
            this.Responsables.setResultados("Si");
        } else {
            this.Responsables.setResultados("No");
        }
        if (this.cMuestras.isSelected()){
            this.Responsables.setMuestras("Si");
        } else {
            this.Responsables.setMuestras("No");
        }
        if (this.cEntrevistas.isSelected()){
            this.Responsables.setEntrevistas("Si");
        } else {
            this.Responsables.setEntrevistas("No");
        }
        if (this.cAuxiliares.isSelected()){
            this.Responsables.setAuxiliares("Si");
        } else {
            this.Responsables.setAuxiliares("No");
        }
        if (this.cStock.isSelected()){
            this.Responsables.setStock("Si");
        } else {
            this.Responsables.setStock("No");
        }
        if (this.cUsuarios.isSelected()){
            this.Responsables.setUsuarios("Si");
        } else {
            this.Responsables.setUsuarios("No");
        }
        if (this.cProtocolos.isSelected()){
            this.Responsables.setProtocolos("Si");
        } else {
            this.Responsables.setProtocolos("No");
        }
        if (this.cHistorias.isSelected()){
            this.Responsables.setClinica("Si");
        } else {
            this.Responsables.setClinica("No");
        }

		// si existe una firma
        if (this.Archivo != null) {

            // fijamos las propiedades de la imagen
            this.Responsables.setImagen(this.Contenido);
            this.Responsables.setLongImagen(this.Longitud);

		// si no cargó
        } else {

			// lo inicializamos
			this.Responsables.setImagen(null);
			this.Responsables.setLongImagen(0);

		}

        // grabamos el registro y actualizamos el formulario
        int idusuario = this.Responsables.grabaUsuario();
        this.tId.setText(Integer.toString(idusuario));

        // presenta el mensaje
        new Mensaje("Registro Grabado ... ");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar o seleccionar
     * nuevo desde el menú, verifica si hay cambios y luego
     * limpia el formulario
     */
    public void limpiaFormulario(){

        // fijamos los valores
        this.tId.setText("");
        this.tNombre.setText("");
        this.tUsuario.setText("");
        this.tCargo.setText("");
        this.tInstitucion.setText("");
        this.tDireccion.setText("");
        this.tCodigoPostal.setText("");
        this.tEmail.setText("");
        this.tTelefono.setText("");
        this.tPais.setText("");
        this.tJurisdiccion.setText("");
        this.tLocalidad.setText("");
        this.CodLoc = "";
        this.tAutorizo.setText(Seguridad.Usuario);
        Utilidades Herramientas = new Utilidades();
        this.tAlta.setText(Herramientas.FechaActual());
        this.tComentarios.setText("");

        // fija los chekbox
        this.cActivo.setSelected(false);
        this.cNivelCentral.setSelected(false);
        this.cAdministrador.setSelected(false);
        this.cPersonas.setSelected(false);
        this.cCongenito.setSelected(false);
        this.cResultados.setSelected(false);
        this.cMuestras.setSelected(false);
        this.cEntrevistas.setSelected(false);
        this.cAuxiliares.setSelected(false);
        this.cStock.setSelected(false);
        this.cUsuarios.setSelected(false);
        this.cProtocolos.setSelected(false);

    	// fijamos la imagen
		this.lFirma.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));

        // fijamos el foco
        this.tNombre.requestFocus();

    }

    // método llamado al pulsar la opción buscar del menú
    public void buscaUsuario(){

		// pide el texto a buscar
		String respuesta = JOptionPane.showInputDialog(this,
								  	  "Ingrese el usuario a buscar",
									  "Buscar",
									  JOptionPane.INFORMATION_MESSAGE);

		// si canceló
		if (respuesta.equals("")){

            // retornamos
            return;

        // si ingresó texto
		} else {

            // llamamos al método protegido
            this.encuentraUsuario(respuesta);

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param texto cadena con el texto a buscar
     * Método que recibe como parámetro una cadena de texto
     * a buscar en la base y si la encuentra presenta la
     * grilla con los resultados
     */
    protected void encuentraUsuario(String texto){

		// busca en la base
		ResultSet nomina = this.Responsables.buscaUsuario(texto);

        try {

            // si hubo registros
            if (nomina.next()){

                // mostramos el diálogo
                FormBuscaUsuarios grillaUsuarios = new FormBuscaUsuarios(this);
                grillaUsuarios.cargaUsuarios(nomina);
                grillaUsuarios.setVisible(true);

            // si no encontró
            } else {

				// presentamos el alerta
				JOptionPane.showMessageDialog(this,
											"No se han encontrado usuarios",
											"Error",
											JOptionPane.ERROR_MESSAGE);
				return;

            }

		// si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idusuario - clave del registro
     * Método que recibe como parámetro la clave de un usuario
     * y muestra el registro en el formulario
     */
    public void verUsuario(int idusuario){

        // buscamos el registro
        this.Responsables.getDatosUsuario(idusuario);

        // presentamos el registro
        this.tId.setText(Integer.toString(this.Responsables.getIdUsuario()));
        this.tNombre.setText(this.Responsables.getNombre());
        this.tUsuario.setText(this.Responsables.getUsuario());
        this.tCargo.setText(this.Responsables.getCargo());
        this.tInstitucion.setText(this.Responsables.getLaboratorio());
        this.IdInstitucion = this.Responsables.getIdLaboratorio();
        this.tDireccion.setText(this.Responsables.getDireccion());
        this.tCodigoPostal.setText(this.Responsables.getCodigoPostal());
        this.tEmail.setText(this.Responsables.getEMail());
        this.tTelefono.setText(this.Responsables.getTelefono());
        this.tPais.setText(this.Responsables.getPais());
        this.IdPais = this.Responsables.getIdPais();
        this.tJurisdiccion.setText(this.Responsables.getProvincia());
        this.tLocalidad.setText(this.Responsables.getLocalidad());
        this.CodLoc = this.Responsables.getIdLocalidad();
        this.tAutorizo.setText(this.Responsables.getAutorizo());
        this.tAlta.setText(this.Responsables.getFechaAlta());
        this.tComentarios.setText(this.Responsables.getObservaciones());

        // según las autorizaciones selecciona los checkbox
        if (this.Responsables.getActivo().equals("Si")){
            this.cActivo.setSelected(true);
        } else {
            this.cActivo.setSelected(false);
        }
        if (this.Responsables.getNivelCentral().equals("Si")){
            this.cNivelCentral.setSelected(true);
        } else {
            this.cNivelCentral.setSelected(false);
        }
        if (this.Responsables.getAdministrador().equals("Si")){
            this.cAdministrador.setSelected(true);
        } else {
            this.cAdministrador.setSelected(false);
        }
        if (this.Responsables.getPersonas().equals("Si")){
            this.cPersonas.setSelected(true);
        } else {
            this.cPersonas.setSelected(false);
        }
        if (this.Responsables.getCongenito().equals("Si")){
            this.cCongenito.setSelected(true);
        } else {
            this.cCongenito.setSelected(false);
        }
        if (this.Responsables.getResultados().equals("Si")){
            this.cResultados.setSelected(true);
        } else {
            this.cResultados.setSelected(false);
        }
        if (this.Responsables.getMuestras().equals("Si")){
            this.cMuestras.setSelected(true);
        } else {
            this.cMuestras.setSelected(false);
        }
        if (this.Responsables.getEntrevistas().equals("Si")){
            this.cEntrevistas.setSelected(true);
        } else {
            this.cEntrevistas.setSelected(false);
        }
        if (this.Responsables.getAuxiliares().equals("Si")){
            this.cAuxiliares.setSelected(true);
        } else {
            this.cAuxiliares.setSelected(false);
        }
        if (this.Responsables.getStock().equals("Si")){
            this.cStock.setSelected(true);
        } else {
            this.cStock.setSelected(false);
        }
        if (this.Responsables.getUsuarios().equals("Si")){
            this.cUsuarios.setSelected(true);
        } else {
            this.cUsuarios.setSelected(false);
        }
        if (this.Responsables.getProtocolos().equals("Si")){
            this.cProtocolos.setSelected(true);
        } else {
            this.cProtocolos.setSelected(false);
        }
        if (this.Responsables.getClinica().equals("Si")){
            this.cHistorias.setSelected(true);
        } else {
            this.cHistorias.setSelected(false);
        }

        // si cargó la firma
		if (this.Responsables.getFirma() != null){

            // leemos de la base de datos
            ImageIcon Foto = new ImageIcon(this.Responsables.getFirma());

            // la escalamos
            Imagenes foto = new Imagenes();
            ImageIcon ajustada = foto.redimensionar(Foto, 130, 130);

            // la asignamos al formulario
            this.lFirma.setIcon(ajustada);

		// si no hay imagen
		} else {

			// fijamos la imagen
			this.lFirma.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));

        }

        // según el nivel de acceso, verificamos desactivar
        // el botón grabar
        if (Seguridad.Administrador.equals("No")){
            this.btnAceptar.setEnabled(false);
        }

        // si no es de nivel central, el checkbox es de solo
        // lectura
        if (Seguridad.NivelCentral.equals("No")){
            this.cNivelCentral.setEnabled(false);
        }

        // fijamos el foco
        this.tNombre.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idlaboratorio clave del registro
     * Método llamado desde el formulario de búsqueda de laboratorios
     * al seleccionar uno, carga en el formulario principal los
     * datos del laboratorio
     */
    public void cargaLaboratorio(int idlaboratorio){

        // instanciamos la clase y obtenemos el registro
        this.Instituciones.getDatosLaboratorio(idlaboratorio);

        // asignamos en el formulario
        this.IdInstitucion = this.Instituciones.getIdLaboratorio();
        this.tInstitucion.setText(this.Instituciones.getNombre());
        this.tDireccion.setText(this.Instituciones.getDireccion());
        this.tCodigoPostal.setText(this.Instituciones.getCodigoPostal());
        this.tPais.setText(this.Instituciones.getPais());
        this.IdPais = this.Instituciones.getIdPais();
        this.tJurisdiccion.setText(this.Instituciones.getProvincia());
        this.tLocalidad.setText(this.Instituciones.getLocalidad());
        this.CodLoc = this.Instituciones.getIdLocalidad();

        // seteamos el foco
        this.tTelefono.requestFocus();

    }

}
