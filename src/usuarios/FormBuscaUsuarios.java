/*
 * Nombre: FormBuscaUsuarios
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 05/03/2019
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma el formulario con la grilla
 *              de usuarios y permite seleccionarlos
 */

// definición del paquete
package usuarios;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormBuscaUsuarios extends JDialog {

	// agrega el serial id
	private static final long serialVersionUID = -3400299628441383372L;

	// inicializamos las variables
	private JTable tUsuarios;
	private FormUsuarios Padre;

	// constructor de la clase
	public FormBuscaUsuarios(FormUsuarios padre) {

		// seteamos el padre
		this.Padre = padre;

		// establecemos las propiedades
		setBounds(100, 100, 593, 292);
		getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Usuarios Encontrados");

		// inicializamos el formulario
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	@SuppressWarnings("serial")
	protected void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// prsentamos el título
		JLabel lTitulo = new JLabel("Usuarios encontrados");
		lTitulo.setBounds(10, 5, 313, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// definimos el scroll
		JScrollPane scrollUsuarios = new JScrollPane();
		scrollUsuarios.setBounds(10, 35, 575, 225);

		// definimos la tabla
		this.tUsuarios = new JTable();
		this.tUsuarios.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"Clave",
				"Usuario",
				"Laboratorio",
				"Provincia",
				"Sel."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				String.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// fijamos el tooltip
		this.tUsuarios.setToolTipText("Pulse para seleccionar el usuario");

        // fijamos el ancho de las columnas
        this.tUsuarios.getColumn("Clave").setPreferredWidth(50);
        this.tUsuarios.getColumn("Clave").setMaxWidth(50);
        this.tUsuarios.getColumn("Laboratorio").setMaxWidth(150);
        this.tUsuarios.getColumn("Laboratorio").setPreferredWidth(150);
        this.tUsuarios.getColumn("Provincia").setMaxWidth(150);
        this.tUsuarios.getColumn("Provincia").setPreferredWidth(150);
        this.tUsuarios.getColumn("Sel.").setMaxWidth(30);
        this.tUsuarios.getColumn("Sel.").setPreferredWidth(85);

		// fijamos el alto de las filas
		this.tUsuarios.setRowHeight(25);

		// fijamos la fuente
		this.tUsuarios.setFont(Fuente);
		
        // agregamos la tabla al scroll
		scrollUsuarios.setViewportView(this.tUsuarios);
		getContentPane().add(scrollUsuarios);

        // fijamos el evento click
        this.tUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tUsuariosMouseClicked(evt);
            }

        });

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param nomina - array con los resultados
	 * Método que recibe como parámetro un resultset y lo
	 * carga en la tabla
	 */
	public void cargaUsuarios(ResultSet nomina){

        // sobrecargamos el renderer de la tabla
        this.tUsuarios.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tUsuarios.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tUsuarios.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[5];

        try {

			// nos aseguramos de estar en primer registro
			nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // fijamos los valores de la fila
                fila[0] = nomina.getInt("idusuario");
                fila[1] = nomina.getString("nombreusuario");
                fila[2] = nomina.getString("laboratorio");
                fila[3] = nomina.getString("provincia");
                fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de localidades
	 */
	private void tUsuariosMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tUsuarios.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tUsuarios.rowAtPoint(evt.getPoint());
        int columna = this.tUsuarios.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tUsuarios.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // si pulsó en seleccionar
            if (columna == 4){

                // cargamos el registro
                this.Padre.verUsuario((int) modeloTabla.getValueAt(indice, 0));

				// cerramos el formulario
				this.dispose();

            }

		}

	}

}
