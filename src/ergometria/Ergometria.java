/*

    Nombre: Ergometria
    Fecha: 27/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que provee los métodos para el abm de
                 la tabla de ergometría

 */

// definición del paquete
package ergometria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// inclusión de librerías
import dbApi.Conexion;
import seguridad.Seguridad;

// definición de la clase
public class Ergometria {

    // definición de variables de clase
    private Conexion Enlace;            // puntero a la base de datos
    private int IdUsuario;              // clave del usuario activo
    private int Id;                     // clave del registro
    private int Paciente;               // clave del protocolo
    private int Visita;                 // clave de la visita
    private String Fecha;               // fecha de administración
    private int Fcbasal;                // frecuencia basal
    private int Fcmax;                  // frecuencia máxima
    private String Tabasal;             // tensiòn arterial basal
    private String Tamax;               // tensión arterial máxima
    private int Kpm;                    // kilòmetros?
    private String Itt;                 // verificar unidad
    private String Observaciones;       // observaciones del usuario
    private String Usuario;             // nombre del usuario
    private String FechaAlta;           // fecha de alta del registro

    // constructor de la clase
    public Ergometria(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;

        // inicializamos las variables
        this.initErgometria();
        
    }

    /** 
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el constructor o desde la consulta
     * que inicializa las variables de clase
     */
    private void initErgometria() {

    	// inicializamos las variables
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.Fecha = "";
        this.Fcbasal = 0;
        this.Fcmax = 0;
        this.Tabasal = "";
        this.Tamax = "";
        this.Kpm = 0;
        this.Itt = "";
        this.Observaciones = "";
        this.Usuario = "";
        this.FechaAlta = "";
    	
    }
    
    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }
    public void setFcbasal(int frecuencia){
        this.Fcbasal = frecuencia;
    }
    public void setFcmax(int frecuencia){
        this.Fcmax = frecuencia;
    }
    public void setTabasal(String tension){
        this.Tabasal = tension;
    }
    public void setTamax(String tension){
        this.Tamax = tension;
    }
    public void setKpm(int kpm){
        this.Kpm = kpm;
    }
    public void setItt(String itt){
        this.Itt = itt;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getFecha(){
        return this.Fecha;
    }
    public int getFcbasal(){
        return this.Fcbasal;
    }
    public int getFcmax(){
        return this.Fcmax;
    }
    public String getTabasal(){
        return this.Tabasal;
    }
    public String getTamax(){
        return this.Tamax;
    }
    public int getKpm(){
        return this.Kpm;
    }
    public String getItt(){
        return this.Itt;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - entero con el protocolo del paciente
     * @return resultset con los registros encontrados
     * Método que recibe como parámetro la clave de un protocolo
     * y retorna todos las ergometrías de ese paciente, utilizado
     * en las historias clínicas agrupadas por estudio
     */
    public ResultSet nominaErgometria(int protocolo){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_ergometria.id AS id, " +
                   "       diagnostico.v_ergometria.paciente AS paciente, " +
                   "       diagnostico.v_ergometria.visita AS visita, " +
                   "       diagnostico.v_ergometria.fecha AS fecha, " +
                   "       diagnostico.v_ergometria.fcbasal AS fcbasal, " +
                   "       diagnostico.v_ergometria.fcmax AS fcmax, " +
                   "       diagnostico.v_ergometria.tabasal AS tabasal, " +
                   "       diagnostico.v_ergometria.tamax AS tamax, " +
                   "       diagnostico.v_ergometria.kpm AS kpm, " +
                   "       diagnostico.v_ergometria.itt AS itt, " +
                   "       diagnostico.v_ergometria.observaciones AS observaciones, " +
                   "       diagnostico.v_ergometria.usuario AS usuario, " +
                   "       diagnostico.v_ergometria.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.ergometria " +
                   "WHERE diagnostico.ergometria.paciente = '" + protocolo + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.ergometria.fecha, '%d/%m/%Y') ASC; ";

        // la ejecutamos y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave de la visita
     * Método que recibe como parámetro la clave de una
     * ergometría y asigna los valores del registro en
     * las variables de clase
     */
    public void getDatosErgometria(int clave){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_ergometria.id AS id, " +
                   "       diagnostico.v_ergometria.paciente AS paciente, " +
                   "       diagnostico.v_ergometria.visita AS visita, " +
                   "       diagnostico.v_ergometria.fecha AS fecha, " +
                   "       diagnostico.v_ergometria.fcbasal AS fcbasal, " +
                   "       diagnostico.v_ergometria.fcmax AS fcmax, " +
                   "       diagnostico.v_ergometria.tabasal AS tabasal, " +
                   "       diagnostico.v_ergometria.tamax AS tamax, " +
                   "       diagnostico.v_ergometria.kpm AS kpm, " +
                   "       diagnostico.v_ergometria.itt AS itt, " +
                   "       diagnostico.v_ergometria.observaciones AS observaciones, " +
                   "       diagnostico.v_ergometria.usuario AS usuario, " +
                   "       diagnostico.v_ergometria.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.ergometria " +
                   "WHERE diagnostico.ergometria.visita = '" + clave + "'; ";

        // la ejecutamos y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            if (Resultado.next()) {
            	
            	// asignamos en las variables
	            this.Id = Resultado.getInt("id");
	            this.Paciente = Resultado.getInt("paciente");
	            this.Visita = Resultado.getInt("visita");
	            this.Fecha = Resultado.getString("fecha");
	            this.Fcbasal = Resultado.getInt("fcbasal");
	            this.Fcmax = Resultado.getInt("fcmax");
	            this.Tabasal = Resultado.getString("tabasal");
	            this.Tamax = Resultado.getString("tamax");
	            this.Kpm = Resultado.getInt("kpm");
	            this.Itt = Resultado.getString("itt");
	            this.Observaciones = Resultado.getString("observaciones");
	            this.Usuario = Resultado.getString("usuario");
	            this.FechaAlta = Resultado.getString("fecha_alta");

	        // si no encontró registros
            } else {
            	
            	// inicializamos las variables
            	this.initErgometria();
            	
            }
            
        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaErgometria(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaErgometria();
        } else {
            this.editaErgometria();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    private void nuevaErgometria(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.ergometria " +
                   "       (paciente, " +
                   "        visita, " +
                   "        fecha, " +
                   "        fcbasal, " +
                   "        fcmax, " +
                   "        tabasal, " +
                   "        tamax, " +
                   "        kpm, " +
                   "        itt, " +
                   "        observaciones, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?. ?. STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?, " +
                   "        ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Fcbasal);
            psInsertar.setInt(5,    this.Fcmax);
            psInsertar.setString(6, this.Tabasal);
            psInsertar.setString(7, this.Tamax);
            psInsertar.setInt(8,    this.Kpm);
            psInsertar.setString(9, this.Itt);
            psInsertar.setString(10,this.Observaciones);
            psInsertar.setInt(11,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    private void editaErgometria(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.ergometria SET " +
                   "       paciente = ?, " +
                   "       visita = ?, " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       fcbasal = ?, " +
                   "       fcmax = ?, " +
                   "       tabasal = ?, " +
                   "       tamax = ?, " +
                   "       kpm = ?, " +
                   "       itt = ?, " +
                   "       observaciones = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.ergometria.id = '" + this.Id + "'; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Fcbasal);
            psInsertar.setInt(5,    this.Fcmax);
            psInsertar.setString(6, this.Tabasal);
            psInsertar.setString(7, this.Tamax);
            psInsertar.setInt(8,    this.Kpm);
            psInsertar.setString(9, this.Itt);
            psInsertar.setString(10,this.Observaciones);
            psInsertar.setInt(11,   this.IdUsuario);
            psInsertar.setInt(12,   this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}