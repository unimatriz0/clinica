/*

    Nombre: FormErgometria
    Fecha: 24/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el abm
	             de las ergometrías

 */

// definición del paquete
package ergometria;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import javax.swing.JSpinner;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import java.awt.Dialog;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;
import ergometria.Ergometria;
import funciones.Utilidades;

// definición de la clase
public class FormErgometria extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -140143633761176525L;

	// definimos las variables de clase
	private JDateChooser dFecha;
	private JSpinner sBasal;
	private JFormattedTextField tBasal;
	private JSpinner cKpm;
	private JSpinner cFmax;
	private JFormattedTextField tTamax;
	private JTextField tItt;
	private JTextArea tComentarios;
	private Ergometria Esfuerzo;
	private Utilidades Herramientas;
	private int Protocolo;
	private int Visita;
	private int IdErgometria;

	// constructor del formulario recibe como parámetros el 
	// protocolo del paciente y la clave de la visita
	public FormErgometria(Dialog parent, boolean modal, int protocolo, int idvisita) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// asignamos en las variables de clase
		this.Protocolo = protocolo;
		this.Visita = idvisita;
		this.IdErgometria = 0;
		this.Esfuerzo = new Ergometria();
		this.Herramientas = new Utilidades();
		
		// fijamos las propiedades
		this.setTitle("Ergometría");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setBounds(150, 150, 450, 305);
		this.setLayout(null);

		// iniciamos el formulario
		this.initForm();

		// mostramos el registro
		this.verErgometria(idvisita);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que configura el formulario
	 */
	private void initForm(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// define el título
		JLabel lTitulo = new JLabel("<html><b>Datos de la Ergometría</b></html>");
		lTitulo.setBounds(10, 10, 240, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// pide la fecha
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setBounds(10, 45, 70, 26);
		lFecha.setFont(Fuente);
		this.add(lFecha);
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(50, 45, 110, 26);
		this.dFecha.setToolTipText("Indique la fecha del estudio");
		this.dFecha.setFont(Fuente);
		this.add(this.dFecha);

        // por defecto fijamos la fecha actual
        Calendar fechaActual = new GregorianCalendar();
        this.dFecha.setCalendar(fechaActual);
		
		// pide la frecuencia basal
		JLabel lFBasal = new JLabel("Fc Basal:");
		lFBasal.setBounds(10, 80, 70, 26);
		lFBasal.setFont(Fuente);
		this.add(lFBasal);
		this.sBasal = new JSpinner();
		this.sBasal.setBounds(70, 80, 55, 26);
		this.sBasal.setFont(Fuente);
        this.sBasal.setModel(new SpinnerNumberModel(50, 50, 180, 1));
		this.add(this.sBasal);

		// pide la tensión basal
		JLabel lTBasal = new JLabel("TA Basal:");
		lTBasal.setBounds(150, 80, 70, 26);
		lTBasal.setFont(Fuente);
		this.add(lTBasal);
		this.tBasal = new JFormattedTextField();
		this.tBasal.setBounds(205, 80, 65, 26);
		this.tBasal.setFont(Fuente);
        try {
            this.tBasal.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("###/###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }		
		this.add(this.tBasal);

		// pide los kilómetros
		JLabel lKpm = new JLabel("kpm:");
		lKpm.setBounds(295, 80, 50, 26);
		lKpm.setFont(Fuente);
		this.add(lKpm);
		this.cKpm = new JSpinner();
		this.cKpm.setBounds(330, 80, 50, 26);
		this.cKpm.setFont(Fuente);
		this.add(this.cKpm);

		// pide la frecuencia máxima
		JLabel lFMax = new JLabel("FC Max.:");
		lFMax.setBounds(10, 115, 70, 26);
		lFMax.setFont(Fuente);
		this.add(lFMax);
		this.cFmax = new JSpinner();
		this.cFmax.setBounds(70, 115, 45, 26);
		this.cFmax.setFont(Fuente);
        this.cFmax.setModel(new SpinnerNumberModel(80, 80, 250, 1));		
		this.add(cFmax);

		// pide la tensión máxima
		JLabel lTMax = new JLabel("TA Max.:");
		lTMax.setBounds(150, 115, 70, 26);
		lTMax.setFont(Fuente);
		this.add(lTMax);
		this.tTamax = new JFormattedTextField();
		this.tTamax.setBounds(205, 115, 65, 26);
		this.tTamax.setFont(Fuente);
        try {
            this.tTamax.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("###/###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }		
		this.add(this.tTamax);

		// pide la itt
		JLabel lItt = new JLabel("ITT Max.:");
		lItt.setBounds(295, 115, 70, 26);
		lItt.setFont(Fuente);
		this.add(lItt);
		this.tItt = new JTextField();
		this.tItt.setBounds(350, 115, 65, 26);
		this.tItt.setFont(Fuente);
		this.add(this.tItt);

		// el label de observaciones
		JLabel lObservaciones = new JLabel("Observaciones:");
		lObservaciones.setBounds(10, 150, 145, 26);
		lObservaciones.setFont(Fuente);
		this.add(lObservaciones);

		// define el scroll de los comentarios
		JScrollPane scrollComentarios = new JScrollPane();
		scrollComentarios.setBounds(10, 180, 300, 85);
		this.add(scrollComentarios);

		// agrega el textarea
		this.tComentarios = new JTextArea();
		this.tComentarios.setToolTipText("Ingrese sus observaciones");
		this.tComentarios.setFont(Fuente);

		// agrega el textarea al scroll
		scrollComentarios.setViewportView(this.tComentarios);

		// presenta el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(320, 180, 110, 26);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaErgometria();
            }
        });
		this.add(btnGrabar);

		// presenta el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(320, 215, 110, 26);
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setToolTipText("Cierra el formulario sin grabar");
		btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaErgometria();
            }
        });
		this.add(btnCancelar);

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idvisita entero con la clave de la visita
	 * Método que recibe como parámetro la clave de un registro
	 * y carga en el formulario los datos
	 */
	private void verErgometria(int idvisita){

		// obtenemos el registro
		this.Esfuerzo.getDatosErgometria(idvisita);
		
		// asignamos en el formulario
		this.IdErgometria = this.Esfuerzo.getId();
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Esfuerzo.getFecha()));
		this.sBasal.setValue(this.Esfuerzo.getFcbasal());
		this.tBasal.setText(this.Esfuerzo.getTabasal());
		this.cKpm.setValue(this.Esfuerzo.getKpm());
		this.cFmax.setValue(this.Esfuerzo.getFcmax());
		this.tTamax.setText(this.Esfuerzo.getTamax());
		this.tItt.setText(this.Esfuerzo.getItt());
		this.tComentarios.setText(this.Esfuerzo.getObservaciones());
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que
	 * verifica el formulario antes de enviarlo al
	 * servidor
	 */
	private void validaErgometria(){

		// si no cargó la fecha
        if (this.Herramientas.fechaJDate(this.dFecha) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Ingrese la fecha de administración",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;

        }
		
		// si no indicó la frecuencia basal
        if (Integer.parseInt(this.sBasal.getValue().toString()) == 0) {
        
            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la frecuencia basal",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.sBasal.requestFocus();
			return;
        	
        }
        
		// si no indicó la tensión basal
        if(this.tBasal.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la tensión basal",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tBasal.requestFocus();
			return;
        	
        }
		
		
		// si no ingresó los kpm
		if (Integer.parseInt(this.cKpm.getValue().toString()) == 0) {
			
            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la distancia recorrida",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cKpm.requestFocus();
			return;
			
		}
		
		// si no indicó la frecuencia máxima
		if (Integer.parseInt(this.cFmax.getValue().toString()) == 0) {
			
            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la frecuencia máxima",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;
			
		}
		
		// si no indicó la tensión máxima
		if (this.tTamax.getText().isEmpty()) {
			
            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la tensión máxima",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;
			
		}
		
		// el itt y los comentrios los 
		// oermite en blanco
		
		// grabamos el registro
		this.grabaErgometria();
		
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de verificar el formulario que
	 * actualiza el registro en el servidor
	 */
	private void grabaErgometria(){

		// asignamos los valores en la clase
		this.Esfuerzo.setId(this.IdErgometria);
		this.Esfuerzo.setPaciente(this.Protocolo);
		this.Esfuerzo.setVisita(this.Visita);
		this.Esfuerzo.setFecha(this.Herramientas.fechaJDate(this.dFecha));
		this.Esfuerzo.setFcbasal(Integer.parseInt(this.sBasal.getValue().toString()));
		this.Esfuerzo.setTabasal(this.tBasal.getText());
		this.Esfuerzo.setKpm(Integer.parseInt(this.cKpm.getValue().toString()));
		this.Esfuerzo.setFcmax(Integer.parseInt(this.cFmax.getValue().toString()));
		this.Esfuerzo.setTamax(this.tTamax.getText());
		this.Esfuerzo.setItt(this.tItt.getText());
		this.Esfuerzo.setObservaciones(this.tComentarios.getText());
		
		// grabamos el registro y obtenemos la id
		this.IdErgometria = this.Esfuerzo.grabaErgometria();
		
		// asignamos en el formulario padre
		
		// cerramos el formulario
		this.dispose();
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * recarga el registro o limpia el formulario según
	 * el estado de la variable control
	 */
	private void cancelaErgometria(){

		// recargamos el registro
		this.verErgometria(this.Visita);
		
	}

}
