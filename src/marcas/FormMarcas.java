/*

    Nombre: formMarcas
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que presenta la nómina de marcas utilizadas para 
                 cada técnica diagnóstica
 */

// definición del paquete
package marcas;

//importamos las librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import tecnicas.Tecnicas;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import funciones.ComboClave;
import funciones.RendererTabla;
import java.awt.Font;

// definimos el formulario
public class FormMarcas extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
    private JComboBox <Object> cTecnica;
    private JScrollPane Scroll;
    private JTable tMarcas;
    
    // declaramos las variables
    protected Marcas Producto;
    protected Tecnicas Procedimiento;
    private JTextField tId;
    private JTextField tMarca;
    
    /**
     * @param parent el formulario padre
     * @param modal valor que indica si será modal
     * Creates new form formMarcas
     */
    public FormMarcas(java.awt.Frame parent, boolean modal) {
    
        // setea el padre e inicia los componentes
        super(parent, modal);

        // instanciamos la clase de marcas
        this.Producto = new Marcas();
        this.Procedimiento = new Tecnicas();
        
        // inicia el formulario
        initComponents();
        
    }

    // inicializamos el formulario
    @SuppressWarnings("serial")
    private void initComponents() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // definimos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Marcas Utilizadas");
        getContentPane().setLayout(null);
        this.setResizable(false);
        this.setBounds(250,200,423,382);                
        
        // fija el título
        JLabel lTitulo = new javax.swing.JLabel("Indique la técnica y obtendrá un listado de las marcas:");
        lTitulo.setBounds(5,5,404,26);
        lTitulo.setFont(Fuente);
        getContentPane().add(lTitulo);

        // el label de técnica
        JLabel lTecnica = new javax.swing.JLabel("Técnica:");
        lTecnica.setBounds(5,50,72,26);
        lTecnica.setFont(Fuente);
        getContentPane().add(lTecnica);

        // el combo de técnica
        this.cTecnica = new javax.swing.JComboBox<>();
        this.cTecnica.setToolTipText("Seleccione la técnica de la lista");
        this.cTecnica.setBounds(65,50,255,26);
        this.cTecnica.setFont(Fuente);
        getContentPane().add(this.cTecnica);

        // cargamos la nómina de técnicas
        this.cargaTecnicas();
                
        // agregamos el listener a las técnicas
        this.cTecnica.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                mostrarMarcas();
            }
        });
        
        // presenta la clave del registro
        this.tId = new JTextField();
        this.tId.setBounds(5, 85, 77, 26);
        this.tId.setToolTipText("Clave del registro");
        this.tId.setFont(Fuente);
        this.tId.setEditable(false);
        getContentPane().add(this.tId);
        
        // presenta el nombre de la marca
        this.tMarca = new JTextField();
        this.tMarca.setBounds(92, 85, 228, 26);
        this.tMarca.setToolTipText("Nombre de la marca");
        this.tMarca.setFont(Fuente);
        getContentPane().add(this.tMarca);
        
        // agrega el botón grabar
        JButton btnGrabar = new JButton();
        btnGrabar.setToolTipText("Graba el registro en la base");
        btnGrabar.setBounds(332, 82, 26, 26);
        btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaMarca();
			}
		});        
        getContentPane().add(btnGrabar);
        
        // define la tabla y sus propiedades
        this.tMarcas = new javax.swing.JTable();
        this.tMarcas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
            	{null, null, null, null},
            },
            new String [] {
                "ID", 
                "Marca", 
                "Ed.", 
                "El"
            }
        ) {
            @SuppressWarnings("rawtypes")
            Class[] types = new Class [] {
                Integer.class, 
                String.class,
                Object.class,
                Object.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        // definimos el método click sobre la grilla
        tMarcas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tMarcasMouseClicked(evt);
            }
        });

        // fijamos el ancho de las columnas
        this.tMarcas.getColumn("ID").setPreferredWidth(30);
        this.tMarcas.getColumn("ID").setMaxWidth(30);
        this.tMarcas.getColumn("Ed.").setPreferredWidth(35);
        this.tMarcas.getColumn("Ed.").setMaxWidth(35);
        this.tMarcas.getColumn("El").setPreferredWidth(35);
        this.tMarcas.getColumn("El").setMaxWidth(35);

        // fijamos el alto de las filas
		this.tMarcas.setRowHeight(25);
        
        // fijamos la fuente
        this.tMarcas.setFont(Fuente);

        // definimos el scroll y agregamos la tabla
        this.Scroll = new javax.swing.JScrollPane(this.tMarcas);
        this.Scroll.setViewportView(this.tMarcas);
        this.Scroll.setBounds(10,120,393,226);
        getContentPane().add(this.Scroll);
                
        // mostramos el formulario 
        this.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al iniciar el formulario que carga la nómina 
     * de técnicas
     */
    protected void cargaTecnicas(){
    	
    	// obtenemos la nómina de técnicas
    	ResultSet nomina = this.Procedimiento.nominaTecnicas();
    	
        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cTecnica.addItem(new ComboClave(0,""));

			// verificamos si está vacío
			if (!nomina.next()){

				// presenta el mensaje y cierra el formulario
				JOptionPane.showMessageDialog(this, "Debe cargar técnicas primero", "Error", JOptionPane.ERROR_MESSAGE);
				return;

			}

            // nos desplazamos al inicio del resultset
            nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // agregamos el registro
				this.cTecnica.addItem(new ComboClave(nomina.getInt("id_tecnica"), nomina.getString("tecnica")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }
    	
    }
    
    // método que actualiza la nómina de marcas de acuerdo 
    // a la técnica seleccionada
    private void mostrarMarcas(){

    	// obtenemos el valor seleccionado
		ComboClave item = (ComboClave) this.cTecnica.getSelectedItem();
		
		// si hay un elemento seleccionado
		if (item != null) {

			// obtenemos la clave
			int clave = item.getClave();

			// sobrecargamos el renderer de la tabla
			this.tMarcas.setDefaultRenderer(Object.class, new RendererTabla());

			// obtenemos el modelo de la tabla
			DefaultTableModel modeloTabla = (DefaultTableModel) this.tMarcas.getModel();

			// hacemos la tabla se pueda ordenar
			this.tMarcas.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

			// limpiamos la tabla
			modeloTabla.setRowCount(0);

			// definimos el objeto de las filas
			Object [] fila = new Object[4];

			// ahora obtenemos la nomina
			ResultSet nomina = this.Producto.nominaMarcas(clave);

			try{

				// nos desplazamos al primer registro
				nomina.beforeFirst();

				// recorremos el vector
				while(nomina.next()){

					// agregamos los elementos al array
					fila[0] = nomina.getInt("idmarca");
					fila[1] = nomina.getString("marca");
					fila[2] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
					fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

					// lo agregamos
					modeloTabla.addRow(fila);

				}

			// si hubo un error
			} catch (SQLException ex){

				// presenta el mensaje
				System.out.println(ex.getMessage());

			}

		}
        
    }
    // evento llamado al pulsar sobre una celda
    private void tMarcasMouseClicked(java.awt.event.MouseEvent evt) {

        // primero verificamos que exista una técnica seleccionada
        String tecnica = this.cTecnica.getSelectedItem().toString();        
        if ("".equals(tecnica)){
            return;
        }   

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tMarcas.getModel();            
        
        // obtenemos la fila y columna pulsados
        int fila = tMarcas.rowAtPoint(evt.getPoint());
        int columna = tMarcas.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tMarcas.convertRowIndexToModel (fila);
        
        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id de la marca
            int idMarca = (int) modeloTabla.getValueAt(indice, 0);
            
            // según la columna pulsada
            if (columna == 2){
            	this.getMarca(idMarca);
            } else if (columna == 3){
            	this.borraMarca(idMarca);
            }
            
        }
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de la marca y 
     * presenta el registro en el formulario
     */
    protected void getMarca(int clave){

    	// obtenemos los datos del registro
    	this.Producto.getDatosMarca(clave);
    	
    	// asignamos en el formulario
    	this.tId.setText(Integer.toString(this.Producto.getIdMarca()));
    	this.tMarca.setText(this.Producto.getMarca());
    	
    	// fijamos el foco
    	this.tMarca.requestFocus();
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y luego de pedir confirmación ejecuta la consulta de 
     * eliminación
     */
    protected void borraMarca(int clave){
    	
        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar la marca?",
                                   "Marcas de Reactivos",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.Producto.borraMarca(clave);

            // recargamos la grilla
            this.mostrarMarcas();

        }
    	
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario y ejecuta 
     * la consulta en el servidor
     */
    protected void grabaMarca(){

    	// declaramos las variables
    	int idtecnica;
    	
    	// asignamos la técnica
		ComboClave item = (ComboClave) this.cTecnica.getSelectedItem();
		
		// si hay un valor seleccionado
		if (item != null){
			
			// obtenemos la clave y asignamos en la clase
			idtecnica = item.getClave();
			this.Producto.setIdTecnica(idtecnica);
			
		} else {
			
            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Seleccione una técnica", "Error", JOptionPane.ERROR_MESSAGE);
            this.cTecnica.requestFocus();
            return;
            
		}
    	
    	// verificamos se halla ingresado una marca
    	if (this.tMarca.getText().isEmpty()){
    		
            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique la marca del reactivo", "Error", JOptionPane.ERROR_MESSAGE);
            this.tMarcas.requestFocus();
            return;
    		
    	// si está correcto
    	} else {
    		
    		// asignamos en la clase
    		this.Producto.setMarca(this.tMarca.getText());
    		
    	}
    	
    	// si está insertando
    	if (this.tId.getText().isEmpty()){
    	
    		// verificamos que no esté repetido
    		if (!this.Producto.validaMarca(this.tMarca.getText(), idtecnica)){

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this, "Esa marca ya está declarada", "Error", JOptionPane.ERROR_MESSAGE);
                this.tMarcas.requestFocus();
                return;
    			
    		}
    		
    		// asigna el valor en la clase
    		this.Producto.setIdMarca(0);
    		
    	// si está editando
    	} else {
    		
    		// asignamos en la clase
    		this.Producto.setIdMarca(Integer.parseInt(this.tId.getText()));
    		
    	}
    	   	
    	// grabamos el registro
		this.Producto.grabaMarca();
    	
    	// limpia el formulario
		this.limpiaFormulario();
    	
		// recargamos la grilla
		this.mostrarMarcas();
		
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario luego de grabar 
     */
    protected void limpiaFormulario(){
    
    	// limpiamos los campos
    	this.tId.setText("");
    	this.tMarca.setText("");
    	
    }
    
}
