/*

    Nombre: Electro
    Fecha: 26/05/2019
    Autor: Lic. Claudi//o Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que provee los métodos para acceder a la tabla
                 de electrocardiogramas

 */

// definición del paquete
package electro;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// definicion de la clase
public class Electro {

    // declaramos las variables
    private Conexion Enlace;                  // puntero a la base de datos
    private int Id;                           // clave del registro
    private int Protocolo;                    // clave del paciente
    private int Visita;                       // clave de la visita
    private String Fecha;                     // fecha de administración
    private int Normal;                       // 0 false - 1 verdadero
    private int Bradicardia;                  // 0 false - 1 verdadero
    private int Fc;                           // la frecuencia cardíaca
    private String Arritmia;                  // tipo de arritmia
    private float Qrs;                        // eje qrs
    private int Ejeqrs;                       // desviación del eje en grados
    private float Pr;                         // ver que significa
    private int Brd;                          // 0 false - 1 verdadero
    private int BrdModerado;                  // 0 false - 1 verdadero
    private int Brcd;                         // 0 false - 1 verdadero
    private int TendenciaHbai;                // 0 false - 1 verdadero
    private int Hbai;                         // 0 false - 1 verdadero
    private int Tciv;                         // 0 false - 1 verdadero
    private int Bcri;                         // 0 false - 1 verdadero
    private int Bav1g;                        // 0 false - 1 verdadero
    private int Bav2g;                        // 0 false - 1 verdadero
    private int Bav3g;                        // 0 false - 1 verdadero
    private int Fibrosis;                     // 0 false - 1 verdadero
    private int AumentoAi;                    // 0 false - 1 verdadero
    private int Wpv;                          // 0 false - 1 verdadero
    private int Hvi;                          // 0 false - 1 verdadero
    private int NoTiene;                      // 0 false - 1 verdadero
    private int SinTanstornos;                // 0 false - 1 verdadero
    private String Repolarización;            // tipo de transtorno de repolarización
    private String Usuario;                   // nombre del usuario
    private int IdUsuario;                    // clave del usuario
    private String FechaAlta;                 // fecha de alta del registro

    // constructor de la clase
    public Electro(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;

        // inicializamos las variables
        this.initElectro();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase cuando
     * es llamado desde el constructor o cuando no existe
     * un registro
     */
    private void initElectro() {

    	// inicializamos las variables
        this.Id = 0;
        this.Protocolo = 0;
        this.Visita = 0;
        this.Fecha = "";
        this.Normal = 0;
        this.Bradicardia = 0;
        this.Fc = 0;
        this.Arritmia = "";
        this.Qrs = 0;
        this.Ejeqrs = 0;
        this.Pr = 0;
        this.Brd = 0;
        this.BrdModerado = 0;
        this.Brcd = 0;
        this.TendenciaHbai = 0;
        this.Hbai = 0;
        this.Tciv = 0;
        this.Bcri = 0;
        this.Bav1g = 0;
        this.Bav2g = 0;
        this.Bav3g = 0;
        this.Fibrosis = 0;
        this.AumentoAi = 0;
        this.Wpv = 0;
        this.Hvi = 0;
        this.NoTiene = 0;
        this.SinTanstornos = 0;
        this.Repolarización = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setProtocolo(int protocolo){
        this.Protocolo = protocolo;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }
    public void setNormal(int normal){
        this.Normal = normal;
    }
    public void setBradicardia(int bradicardia){
        this.Bradicardia = bradicardia;
    }
    public void setFc(int fc){
        this.Fc = fc;
    }
    public void setArritmia(String arritmia){
        this.Arritmia = arritmia;
    }
    public void setQrs(float qrs){
        this.Qrs = qrs;
    }
    public void setEjeqrs(int eje){
        this.Ejeqrs = eje;
    }
    public void setPr(float pr){
        this.Pr = pr;
    }
    public void setBrd(int brd){
        this.Brd = brd;
    }
    public void setBrdModerado(int brdmoderado){
        this.BrdModerado = brdmoderado;
    }
    public void setBrcd(int brcd){
        this.Brcd = brcd;
    }
    public void setTendenciaHbai(int tendencia){
        this.TendenciaHbai = tendencia;
    }
    public void setHbai(int hbai){
        this.Hbai = hbai;
    }
    public void setTciv(int tciv){
        this.Tciv = tciv;
    }
    public void setBcri(int bcri){
        this.Bcri = bcri;
    }
    public void setBav1g(int bav){
        this.Bav1g = bav;
    }
    public void setBav2g(int bav){
        this.Bav2g = bav;
    }
    public void setBav3g(int bav){
        this.Bav3g = bav;
    }
    public void setFibrosis(int fibrosis){
        this.Fibrosis = fibrosis;
    }
    public void setAumentoAi(int aumento){
        this.AumentoAi = aumento;
    }
    public void setWpv(int wpv){
        this.Wpv = wpv;
    }
    public void setHvi(int hvi){
        this.Hvi = hvi;
    }
    public void setNoTiene(int notiene){
        this.NoTiene = notiene;
    }
    public void setSinTranstornos(int sintranstornos){
        this.SinTanstornos = sintranstornos;
    }
    public void setRepolarizacion(String repolarizacion){
        this.Repolarización = repolarizacion;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getProtocolo(){
        return this.Protocolo;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getFecha(){
        return this.Fecha;
    }
    public int getNormal(){
        return this.Normal;
    }
    public int getBradicardia(){
        return this.Bradicardia;
    }
    public int getFc(){
        return this.Fc;
    }
    public String getArritmia(){
        return this.Arritmia;
    }
    public float getQrs(){
        return this.Qrs;
    }
    public int getEjeqrs(){
        return this.Ejeqrs;
    }
    public float getPr(){
        return this.Pr;
    }
    public int getBrd(){
        return this.Brd;
    }
    public int getBrdModerado(){
        return this.BrdModerado;
    }
    public int getBrcd(){
        return this.Brcd;
    }
    public int getTendenciaHbai(){
        return this.TendenciaHbai;
    }
    public int getHbai(){
        return this.Hbai;
    }
    public int getTciv(){
        return this.Tciv;
    }
    public int getBcri(){
        return this.Bcri;
    }
    public int getBav1g(){
        return this.Bav1g;
    }
    public int getBav2g(){
        return this.Bav2g;
    }
    public int getBav3g(){
        return this.Bav3g;
    }
    public int getFibrosis(){
        return this.Fibrosis;
    }
    public int getAumentoAi(){
        return this.AumentoAi;
    }
    public int getWpv(){
        return this.Wpv;
    }
    public int getHvi(){
        return this.Hvi;
    }
    public int getNotiene(){
        return this.NoTiene;
    }
    public int getSinTranstornos(){
        return this.SinTanstornos;
    }
    public String getRepolarizacion(){
        return this.Repolarización;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - clave del protocolo del paciente
     * @return resultset - vector con los registros encontrados
     * Método utilizado en la impresión de historias clínicas
     * clasificadas por estudio, que recibe como parámetro el
     * protocolo y retorna todos los electros realizados
     */
    public ResultSet nominaElectro(int protocolo){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_electro.id AS id, " +
                   "       diagnostico.v_electro.protocolo AS protocolo, " +
                   "       diagnostico.v_electro.visita AS visita, " +
                   "       diagnostico.v_electro.fecha AS fecha, " +
                   "       diagnostico.v_electro.normal AS normal, " +
                   "       diagnostico.v_electro.bradicardia AS bradicardia, " +
                   "       diagnostico.v_electro.fc AS fc, " +
                   "       diagnostico.v_electro.arritmia AS arritmia, " +
                   "       diagnostico.v_electro.qrs AS qrs, " +
                   "       diagnostico.v_electro.ejeqrs AS ejeqrs, " +
                   "       diagnostico.v_electro.pr AS pr, " +
                   "       diagnostico.v_electro.brd AS brd, " +
                   "       diagnostico.v_electro.brdmoderado AS brdmoderado, " +
                   "       diagnostico.v_electro.bcrd AS bcrd, " +
                   "       diagnostico.v_electro.tendenciahbai AS tendenciahbai, " +
                   "       diagnostico.v_electro.hbai AS hbai, " +
                   "       diagnostico.v_electro.tciv AS tciv, " +
                   "       diagnostico.v_electro.bcri AS bcri, " +
                   "       diagnostico.v_electro.bav1g AS bav1g, " +
                   "       diagnostico.v_electro.bav2g AS bav2g, " +
                   "       diagnostico.v_electro.bav3g AS bav3g, " +
                   "       diagnostico.v_electro.fibrosis AS fibrosis, " +
                   "       diagnostico.v_electro.aumentoai AS aumentoai, " +
                   "       diagnostico.v_electro.wpv AS wpv, " +
                   "       diagnostico.v_electro.hvi AS hvi, " +
                   "       diagnostico.v_electro.notiene AS notiene, " +
                   "       diagnostico.v_electro.sintranstornos AS sintranstornos, " +
                   "       diagnostico.v_electro.repolarizacion AS repolarizacion, " +
                   "       diagnostico.v_electro.usuario AS usuario, " +
                   "       diagnostico.v_electro.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.v_electro " +
                   "WHERE diagnostico.v_electro.protocolo = '" + protocolo + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_electro.fecha, '%d/%m/%Y'); ";

        // obtenemos el vector y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita - entero con la clave de la visita
     * Método que recibe como parámetro la clave del registro
     * y asigna en las variables de clase los valores de la
     * base
     */
    public void getDatosElectro(int idvisita){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_electro.id AS id, " +
                   "       diagnostico.v_electro.protocolo AS protocolo, " +
                   "       diagnostico.v_electro.visita AS visita, " +
                   "       diagnostico.v_electro.fecha AS fecha, " +
                   "       diagnostico.v_electro.normal AS normal, " +
                   "       diagnostico.v_electro.bradicardia AS bradicardia, " +
                   "       diagnostico.v_electro.fc AS fc, " +
                   "       diagnostico.v_electro.arritmia AS arritmia, " +
                   "       diagnostico.v_electro.qrs AS qrs, " +
                   "       diagnostico.v_electro.ejeqrs AS ejeqrs, " +
                   "       diagnostico.v_electro.pr AS pr, " +
                   "       diagnostico.v_electro.brd AS brd, " +
                   "       diagnostico.v_electro.brdmoderado AS brdmoderado, " +
                   "       diagnostico.v_electro.bcrd AS bcrd, " +
                   "       diagnostico.v_electro.tendenciahbai AS tendenciahbai, " +
                   "       diagnostico.v_electro.hbai AS hbai, " +
                   "       diagnostico.v_electro.tciv AS tciv, " +
                   "       diagnostico.v_electro.bcri AS bcri, " +
                   "       diagnostico.v_electro.bav1g AS bav1g, " +
                   "       diagnostico.v_electro.bav2g AS bav2g, " +
                   "       diagnostico.v_electro.bav3g AS bav3g, " +
                   "       diagnostico.v_electro.fibrosis AS fibrosis, " +
                   "       diagnostico.v_electro.aumentoai AS aumentoai, " +
                   "       diagnostico.v_electro.wpv AS wpv, " +
                   "       diagnostico.v_electro.hvi AS hvi, " +
                   "       diagnostico.v_electro.notiene AS notiene, " +
                   "       diagnostico.v_electro.sintranstornos AS sintranstornos, " +
                   "       diagnostico.v_electro.repolarizacion AS repolarizacion, " +
                   "       diagnostico.v_electro.usuario AS usuario, " +
                   "       diagnostico.v_electro.fecha_alta AS fecha_alta " +
                   "FROM diagnostico.v_electro " +
                   "WHERE diagnostico.v_electro.visita = '" + idvisita + "'; ";

        // obtenemos el vector
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // si encontró el registro
            if (Resultado.next()) {

            	this.Id = Resultado.getInt("id");
	            this.Protocolo = Resultado.getInt("protocolo");
	            this.Visita = Resultado.getInt("visita");
	            this.Fecha = Resultado.getString("fecha");
	            this.Normal = Resultado.getInt("normal");
	            this.Bradicardia = Resultado.getInt("bradicardia");
	            this.Fc = Resultado.getInt("fc");
	            this.Arritmia = Resultado.getString("arritmia");
	            this.Qrs = Resultado.getFloat("qrs");
	            this.Ejeqrs = Resultado.getInt("ejeqrs");
	            this.Pr = Resultado.getFloat("pr");
	            this.Brd = Resultado.getInt("brd");
	            this.BrdModerado = Resultado.getInt("brdmoderado");
	            this.Brcd = Resultado.getInt("brcd");
	            this.TendenciaHbai = Resultado.getInt("tendenciahbai");
	            this.Hbai = Resultado.getInt("hbai");
	            this.Tciv = Resultado.getInt("tciv");
	            this.Bcri = Resultado.getInt("bcri");
	            this.Bav1g = Resultado.getInt("bav1g");
	            this.Bav2g = Resultado.getInt("bav2g");
	            this.Bav3g = Resultado.getInt("bav3g");
	            this.Fibrosis = Resultado.getInt("fibrosis");
	            this.AumentoAi = Resultado.getInt("aumentoai");
	            this.Wpv = Resultado.getInt("wpv");
	            this.Hvi = Resultado.getInt("hvi");
                this.NoTiene = Resultado.getInt("notiene");
                this.SinTanstornos = Resultado.getInt("sintranstornos");
	            this.Repolarización = Resultado.getString("repolarizacion");
	            this.Usuario = Resultado.getString("usuario");
	            this.FechaAlta = Resultado.getString("fecha_alta");

	        // si no encontró
            } else {

            	// inicializa las variables
            	this.initElectro();

            }

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero la clave del registro afectado
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda
     */
    public int grabaElectro(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoElectro();
        } else {
            this.editaElectro();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    public void nuevoElectro(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.electrocardiograma " +
                   "       (protocolo, " +
                   "        visita, " +
                   "        fecha, " +
                   "        normal, " +
                   "        bradicardia, " +
                   "        fc, " +
                   "        arritmia, " +
                   "        qrs, " +
                   "        ejeqrs, " +
                   "        pr, " +
                   "        brd, " +
                   "        brdmoderado, " +
                   "        bcrd, " +
                   "        tendenciahbai, " +
                   "        hbai, " +
                   "        tciv, " +
                   "        bcri, " +
                   "        bav1g, " +
                   "        bav2g, " +
                   "        bav3g, " +
                   "        fibrosis, " +
                   "        aumentoai, " +
                   "        wpv, " +
                   "        hvi, " +
                   "        notiene, " +
                   "        sintranstornos, " +
                   "        repolarizacion, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?. ?, " +
                   "        ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                   "        ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Protocolo);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Normal);
            psInsertar.setInt(5,    this.Bradicardia);
            psInsertar.setInt(6,    this.Fc);
            psInsertar.setString(7, this.Arritmia);
            psInsertar.setFloat(8,  this.Qrs);
            psInsertar.setInt(9,    this.Ejeqrs);
            psInsertar.setFloat(10, this.Pr);
            psInsertar.setInt(11,   this.Brd);
            psInsertar.setInt(12,   this.BrdModerado);
            psInsertar.setInt(13,   this.Brcd);
            psInsertar.setInt(14,   this.TendenciaHbai);
            psInsertar.setInt(15,   this.Hbai);
            psInsertar.setInt(16,   this.Tciv);
            psInsertar.setInt(17,   this.Bcri);
            psInsertar.setInt(18,   this.Bav1g);
            psInsertar.setInt(19,   this.Bav2g);
            psInsertar.setInt(20,   this.Bav3g);
            psInsertar.setInt(21,   this.Fibrosis);
            psInsertar.setInt(22,   this.AumentoAi);
            psInsertar.setInt(23,   this.Wpv);
            psInsertar.setInt(24,   this.Hvi);
            psInsertar.setInt(25,   this.NoTiene);
            psInsertar.setInt(26,   this.SinTanstornos);
            psInsertar.setString(27,this.Repolarización);
            psInsertar.setInt(28,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    public void editaElectro(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.electrocardiograma SET " +
                   "       protocolo = ?, " +
                   "       visita = ?, " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       normal = ?, " +
                   "       bradicardia = ?, " +
                   "       fc = ?, " +
                   "       arritmia = ?, " +
                   "       qrs = ?, " +
                   "       ejeqrs = ?, " +
                   "       pr = ?, " +
                   "       brd = ?, " +
                   "       brdmoderado = ?, " +
                   "       bcrd = ?, " +
                   "       tendenciahbai = ?, " +
                   "       hbai = ?, " +
                   "       tciv = ?, " +
                   "       bcri = ?, " +
                   "       bav1g = ?, " +
                   "       bav2g = ?, " +
                   "       bav3g = ?, " +
                   "       fibrosis = ?, " +
                   "       aumentoai = ?, " +
                   "       wpv = ?, " +
                   "       hvi = ?, " +
                   "       notiene = ?, " +
                   "       sintranstornos = ?, " +
                   "       repolarizacion = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.electrocardiograma.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Protocolo);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Normal);
            psInsertar.setInt(5,    this.Bradicardia);
            psInsertar.setInt(6,    this.Fc);
            psInsertar.setString(7, this.Arritmia);
            psInsertar.setFloat(8,  this.Qrs);
            psInsertar.setInt(9,    this.Ejeqrs);
            psInsertar.setFloat(10, this.Pr);
            psInsertar.setInt(11,   this.Brd);
            psInsertar.setInt(12,   this.BrdModerado);
            psInsertar.setInt(13,   this.Brcd);
            psInsertar.setInt(14,   this.TendenciaHbai);
            psInsertar.setInt(15,   this.Hbai);
            psInsertar.setInt(16,   this.Tciv);
            psInsertar.setInt(17,   this.Bcri);
            psInsertar.setInt(18,   this.Bav1g);
            psInsertar.setInt(19,   this.Bav2g);
            psInsertar.setInt(20,   this.Bav3g);
            psInsertar.setInt(21,   this.Fibrosis);
            psInsertar.setInt(22,   this.AumentoAi);
            psInsertar.setInt(23,   this.Wpv);
            psInsertar.setInt(24,   this.Hvi);
            psInsertar.setInt(25,   this.NoTiene);
            psInsertar.setInt(26,   this.SinTanstornos);
            psInsertar.setString(27,this.Repolarización);
            psInsertar.setInt(28,   this.IdUsuario);
            psInsertar.setInt(29,   this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}