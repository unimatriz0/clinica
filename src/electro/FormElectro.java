/*

    Nombre: FormElectro
    Fecha: 24/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que arma el formulario para el abm de
	             datos de los electrocardiogramas

*/

// definición del paquete
package electro;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import java.awt.Dialog;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;
import funciones.Mensaje;
import funciones.Utilidades;
import electro.Electro;

// definición de la clase
public class FormElectro extends JDialog {

	// generamos el serial id
	private static final long serialVersionUID = -6288391699990558094L;

	// definición de variables de clase
	private JDateChooser dFecha;
	private JComboBox<Object> cNormal;
	private JComboBox<Object> cBradicardia;
	private JComboBox<Object> cArritmia;
	private JFormattedTextField tFc;
	private JFormattedTextField tQrs;
	private JSpinner sEjeQrs;
	private JTextField tPr;
	private JCheckBox cBrd;
	private JCheckBox cBrdModerado;
	private JCheckBox cBcrd;
	private JCheckBox cTendencia;
	private JCheckBox cHbai;
	private JCheckBox cTciv;
	private JCheckBox cBcri;
	private JCheckBox cBav1;
	private JCheckBox cBav2;
	private JCheckBox cBav3;
	private JCheckBox cNoTiene;
	private JComboBox<Object> cFibrosis;
	private JCheckBox cAumento;
	private JCheckBox cWpw;
	private JCheckBox cHvi;
	private JCheckBox cSinTranstornos;
	private JComboBox<Object> cRepolarizacion;
	private int Protocolo;
	private int Visita;
	private int IdElectro;
	private Utilidades Herramientas;
	private Electro Cardio;

	// constructor de la clase que recibe como parámetro el
	// protocolo del paciente y la clave de la visita
	public FormElectro(Dialog parent, boolean modal, int protocolo, int idvisita) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// definimos las propiedades
		this.setTitle("Electrocardiogramas");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setBounds(150, 150, 640, 405);
		this.setLayout(null);

		// asignamos el protocolo e inicializamos las variables
		this.Protocolo = protocolo;
		this.Visita = idvisita;
		this.IdElectro = 0;
		this.Herramientas = new Utilidades();
		this.Cardio = new Electro();

		// iniciamos los componentes
		this.initForm();

		// mostramos el registro
		this.verElectro(idvisita);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicia los componente del formulario
	 */
	private void initForm(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Datos del Electrocardiograma</b></html>");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(10, 10, 490, 26);
		this.add(lTitulo);

		// pide la fecha
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setFont(Fuente);
		lFecha.setBounds(10, 45, 55, 26);
		this.add(lFecha);
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(55, 45, 110, 26);
		this.dFecha.setFont(Fuente);
		this.dFecha.setToolTipText("Indique la fecha del estudio");
		this.add(this.dFecha);

        // por defecto fijamos la fecha actual
        Calendar fechaActual = new GregorianCalendar();
        this.dFecha.setCalendar(fechaActual);

		// presenta el combo de normal
		JLabel lNormal = new JLabel("Normal:");
		lNormal.setBounds(10, 80, 55, 26);
		lNormal.setFont(Fuente);
		this.add(lNormal);
		this.cNormal = new JComboBox<>();
		this.cNormal.setBounds(60, 80, 55, 26);
		this.cNormal.setToolTipText("Indique si el resultado es normal");
		this.cNormal.setFont(Fuente);
		this.add(this.cNormal);

		// agrega los elementos al combo
		this.comboSiNo(this.cNormal);

		// presenta el combo de bradicardia
		JLabel lBradicardia = new JLabel("Bradicardia Sinusal:");
		lBradicardia.setBounds(145, 80, 150, 26);
		lBradicardia.setFont(Fuente);
		this.add(lBradicardia);
		this.cBradicardia = new JComboBox<>();
		this.cBradicardia.setBounds(270, 80, 55, 26);
		this.cBradicardia.setFont(Fuente);
		this.cBradicardia.setToolTipText("Indique si presenta bradicardia");
		this.add(cBradicardia);

		// agrega los elementos al combo
		this.comboSiNo(this.cBradicardia);

		// presenta el combo de arritmia
		JLabel lArritmia = new JLabel("Arritmia:");
		lArritmia.setBounds(345, 80, 55, 26);
		lArritmia.setFont(Fuente);
		this.add(lArritmia);
		this.cArritmia = new JComboBox<>();
		this.cArritmia.setBounds(405, 80, 100, 26);
		this.cArritmia.setToolTipText("Indique si presenta arritmia");
		this.cArritmia.setFont(Fuente);
		this.add(this.cArritmia);

		// agrega los elementos al combo
		this.elementosArritmia();

		// presenta el label de fc
		JLabel lFc = new JLabel("FC:");
		lFc.setBounds(515, 80, 55, 26);
		lFc.setFont(Fuente);
		this.add(lFc);
		this.tFc = new JFormattedTextField();
		this.tFc.setBounds(540, 80, 90, 26);
		this.tFc.setFont(Fuente);
		this.add(this.tFc);

		// presenta el título de transtornos de la conducción
		JLabel lTranstornos = new JLabel("<html><b>Transtornos de Conducción:</b></html>");
		lTranstornos.setBounds(10, 115, 200, 26);
		lTranstornos.setFont(Fuente);
		this.add(lTranstornos);

		// pide el qrs
		JLabel lQrs = new JLabel("QRS");
		lQrs.setBounds(10, 150, 55, 26);
		lQrs.setFont(Fuente);
		this.add(lQrs);
		this.tQrs = new JFormattedTextField();
		this.tQrs.setBounds(50, 150, 60, 26);
		this.tQrs.setFont(Fuente);
		this.add(tQrs);

		// pide el eje qrs
		JLabel lEjeQrs = new JLabel("Eje QRS:");
		lEjeQrs.setBounds(140, 150, 55, 26);
		lEjeQrs.setFont(Fuente);
		this.add(lEjeQrs);
		this.sEjeQrs = new JSpinner();
		this.sEjeQrs.setBounds(200, 150, 55, 26);
		this.sEjeQrs.setFont(Fuente);
		this.add(sEjeQrs);

		// pide el pr
		JLabel lPr = new JLabel("PR:");
		lPr.setBounds(285, 150, 55, 26);
		lPr.setFont(Fuente);
		this.add(lPr);
		this.tPr = new JTextField();
		this.tPr.setBounds(320, 150, 70, 26);
		this.tPr.setFont(Fuente);
		this.add(this.tPr);

		// presenta los checbox de transtornos de la construcción
		this.cBrd = new JCheckBox("BRD de BG");
		this.cBrd.setBounds(10, 185, 110, 26);
		this.cBrd.setFont(Fuente);
		this.add(this.cBrd);
		this.cBrdModerado = new JCheckBox("BRD Moderado");
		this.cBrdModerado.setBounds(140, 185, 130, 26);
		this.cBrdModerado.setFont(Fuente);
		this.add(this.cBrdModerado);
		this.cBcrd = new JCheckBox("BCRD");
		this.cBcrd.setBounds(300, 185, 110, 26);
		this.cBcrd.setFont(Fuente);
		this.add(this.cBcrd);
		this.cTendencia = new JCheckBox("Tendencia al HBAI");
		this.cTendencia.setBounds(420, 185, 170, 26);
		this.cTendencia.setFont(Fuente);
		this.add(this.cTendencia);
		this.cHbai = new JCheckBox("HBAI");
		this.cHbai.setBounds(10, 220, 110, 26);
		this.cHbai.setFont(Fuente);
		this.add(this.cHbai);
		this.cTciv = new JCheckBox("TCIV");
		this.cTciv.setBounds(140, 220, 110, 26);
		this.cTciv.setFont(Fuente);
		this.add(this.cTciv);
		this.cBcri = new JCheckBox("BCRI");
		this.cBcri.setBounds(300, 220, 110, 26);
		this.cBcri.setFont(Fuente);
		this.add(this.cBcri);
		this.cBav1 = new JCheckBox("BAV 1° G");
		this.cBav1.setBounds(420, 220, 110, 26);
		this.cBav1.setFont(Fuente);
		this.add(this.cBav1);
		this.cBav2 = new JCheckBox("BAV 2° G");
		this.cBav2.setBounds(10, 255, 110, 26);
		this.cBav2.setFont(Fuente);
		this.add(this.cBav2);
		this.cBav3 = new JCheckBox("BAV 3° G");
		this.cBav3.setBounds(140, 255, 110, 26);
		this.cBav3.setFont(Fuente);
		this.add(this.cBav3);
		this.cNoTiene = new JCheckBox("No Tiene");
		this.cNoTiene.setBounds(300, 255, 110, 26);
		this.cNoTiene.setFont(Fuente);
		this.add(this.cNoTiene);

		// pregunta por la fibrosis
		JLabel lFibrosis = new JLabel("Fibrosis:");
		lFibrosis.setBounds(10, 290, 85, 26);
		lFibrosis.setFont(Fuente);
		this.add(lFibrosis);
		this.cFibrosis = new JComboBox<>();
		this.cFibrosis.setBounds(65, 290, 55, 26);
		this.cFibrosis.setToolTipText("Indique si presenta fibrosis");
		this.cFibrosis.setFont(Fuente);
		this.add(this.cFibrosis);

		// agrega los elementos del combo
		this.comboSiNo(this.cFibrosis);

		// presenta los checkbox de otros
		this.cAumento = new JCheckBox("Aumento AI");
		this.cAumento.setBounds(150, 290, 110, 26);
		this.cAumento.setFont(Fuente);
		this.add(this.cAumento);
		this.cWpw = new JCheckBox("WPW");
		this.cWpw.setBounds(270, 290, 70, 26);
		this.cWpw.setFont(Fuente);
		this.add(this.cWpw);
		this.cHvi = new JCheckBox("HVI");
		this.cHvi.setBounds(360, 290, 60, 26);
		this.cHvi.setFont(Fuente);
		this.add(cHvi);
		this.cSinTranstornos = new JCheckBox("No Tiene");
		this.cSinTranstornos.setBounds(450, 290, 110, 26);
		this.cSinTranstornos.setFont(Fuente);
		this.add(this.cSinTranstornos);

		// presenta el label de transtornos de repolarización
		JLabel lRepolarizacion = new JLabel("Transtornos de Repolarización:");
		lRepolarizacion.setBounds(10, 325, 205, 26);
		lRepolarizacion.setFont(Fuente);
		this.add(lRepolarizacion);

		// presenta las opciones de los transtornos
		this.cRepolarizacion = new JComboBox<>();
		this.cRepolarizacion.setBounds(210, 325, 140, 26);
		this.cRepolarizacion.setToolTipText("Seleccione de la lista el valor correcto");
		this.cRepolarizacion.setFont(Fuente);
		this.add(this.cRepolarizacion);

		// agrega las opciones al combo
		this.elementosRepolarizacion();

		// presenta el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(390, 325, 110, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaElectro();
            }
        });
		this.add(btnGrabar);

		// presenta el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(510, 325, 110, 26);
		btnCancelar.setToolTipText("Cierra el formulario sin grabar");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaElectro();
            }
        });
		this.add(btnCancelar);

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idvisita - clave de la visita
	 * Método que recibe como parámetro la clave de un registro
	 * y lo presenta en el formulario de datos
	 */
	private void verElectro(int idvisita){

		// obtenemos el registro
		this.Cardio.getDatosElectro(idvisita);

		// asignamos en las variables de clase
		this.Visita = this.Cardio.getVisita();
		this.Protocolo = this.Cardio.getProtocolo();
		this.IdElectro = this.Cardio.getId();

		// presenta carga los elementos del formulario
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Cardio.getFecha()));
		if (this.Cardio.getNormal() == 1){
			this.cNormal.setSelectedItem("Si");
		} else {
			this.cNormal.setSelectedItem("No");
		}
		if (this.Cardio.getBradicardia() == 1){
			this.cBradicardia.setSelectedItem("Si");
		} else {
			this.cBradicardia.setSelectedItem("No");
		}
		this.cArritmia.setSelectedItem(this.Cardio.getArritmia());
		this.tFc.setText(Integer.toString(this.Cardio.getFc()));
		this.tQrs.setText(Float.toString(this.Cardio.getQrs()));
		this.sEjeQrs.setValue(this.Cardio.getEjeqrs());
		this.tPr.setText(Float.toString(this.Cardio.getPr()));
		if (this.Cardio.getBrd() == 1){
			this.cBrd.setSelected(true);
		} else {
			this.cBrd.setSelected(false);
		}
		if (this.Cardio.getBrdModerado() == 1){
			this.cBrdModerado.setSelected(true);
		} else {
			this.cBrdModerado.setSelected(false);
		}
		if (this.Cardio.getBrcd() == 1){
			this.cBcrd.setSelected(true);
		} else {
			this.cBcrd.setSelected(false);
		}
		if (this.Cardio.getTendenciaHbai() == 1){
			this.cTendencia.setSelected(true);
		} else {
			this.cTendencia.setSelected(false);
		}
		if (this.Cardio.getHbai() == 1){
			this.cHbai.setSelected(true);
		} else {
			this.cHbai.setSelected(false);
		}
		if (this.Cardio.getTciv() == 1){
			this.cTciv.setSelected(true);
		} else {
			this.cTciv.setSelected(false);
		}
		if (this.Cardio.getBcri() == 1){
			this.cBcri.setSelected(true);
		} else {
			this.cBcri.setSelected(false);
		}
		if (this.Cardio.getBav1g() == 1){
			this.cBav1.setSelected(true);
		} else {
			this.cBav1.setSelected(false);
		}
		if (this.Cardio.getBav2g() == 1){
			this.cBav2.setSelected(true);
		} else {
			this.cBav2.setSelected(false);
		}
		if (this.Cardio.getBav3g() == 1){
			this.cBav3.setSelected(true);
		} else {
			this.cBav3.setSelected(false);
		}
		if (this.Cardio.getNotiene() == 1){
			this.cNoTiene.setSelected(true);
		} else {
			this.cNoTiene.setSelected(false);
		}
		if (this.Cardio.getFibrosis() == 1){
			this.cFibrosis.setSelectedItem("Si");
		} else {
			this.cFibrosis.setSelectedItem("No");
		}
		if (this.Cardio.getAumentoAi() == 1){
			this.cAumento.setSelected(true);
		} else {
			this.cAumento.setSelected(false);
		}
		if (this.Cardio.getWpv() == 1){
			this.cWpw.setSelected(true);
		} else {
			this.cWpw.setSelected(false);
		}
		if (this.Cardio.getHvi() == 1){
			this.cHvi.setSelected(true);
		} else {
			this.cHvi.setSelected(false);
		}
		if (this.Cardio.getSinTranstornos() == 1){
			this.cSinTranstornos.setSelected(true);
		} else {
			this.cSinTranstornos.setSelected(false);
		}
		this.cRepolarizacion.setSelectedItem(this.Cardio.getRepolarizacion());
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param combo el objeto combobox
	 * Método que recibe como parámetro un combo del formulario
	 * y agrega los elementos si / no
	 */
	private void comboSiNo(JComboBox<Object> combo){

		// agregamos las opciones
		combo.addItem("");
		combo.addItem("Si");
		combo.addItem("No");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que agrega los elementos al combo de tipo
	 * de arritmia
	 */
	private void elementosArritmia(){

		// agregamos las opciones
		this.cArritmia.addItem("");
		this.cArritmia.addItem("FA");
		this.cArritmia.addItem("EVF");
		this.cArritmia.addItem("Pares");
		this.cArritmia.addItem("TV");
		this.cArritmia.addItem("No Tiene");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que agrega las opciones al combo de transtornos
	 * de repolarización
	 */
	private void elementosRepolarizacion(){

		// agregamos las opciones
		this.cRepolarizacion.addItem("");
		this.cRepolarizacion.addItem("Trast. Lábil");
		this.cRepolarizacion.addItem("Tras. Isquémico");
		this.cRepolarizacion.addItem("Difusos Primarios");
		this.cRepolarizacion.addItem("No Tiene");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que
	 * verifica el formulario antes de enviarlo al
	 * servidor
	 */
	private void validaElectro(){

		// declaración de variables
		Boolean Correcto = false;

		// si no indicó la fecha
        if (this.Herramientas.fechaJDate(this.dFecha) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Ingrese la fecha de administración",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;

        }

		// si no indicó si es normal
        if (this.cNormal.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si es normal",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cNormal.requestFocus();
			return;

        }

		// si no indicó la bradicardia
		if (this.cBradicardia.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si presenta bradicardia",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cBradicardia.requestFocus();
			return;

		}

		// si no indicó arritmia
		if (this.cArritmia.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si presenta arritmia",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cArritmia.requestFocus();
			return;

		}

		// si no ingresó la frecuencia
		if (this.tFc.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la frecuencia cardíaca",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tFc.requestFocus();
			return;

		}

		// si no indicó el QRS
		if (this.tQrs.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el valor de QRS",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tQrs.requestFocus();
			return;

		}

		// si no indicó el eje QRS
		if (Integer.parseInt(this.sEjeQrs.getValue().toString()) == 0) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el valor del Eje QRS",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.sEjeQrs.requestFocus();
			return;

		}

		// si no ingresó el valor de pr
		if (this.tPr.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el valor de PR",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.tPr.requestFocus();
			return;

		}

		// verifica que al menos halla marcado uno
		// de los checbox
		if (this.cBrd.isSelected()){
			Correcto = true;
		} else if (this.cBrdModerado.isSelected()){
			Correcto = true;
		} else if (this.cBcrd.isSelected()){
			Correcto = true;
		} else if (this.cTendencia.isSelected()){
			Correcto = true;
		} else if (this.cHbai.isSelected()){
			Correcto = true;
		} else if (this.cTciv.isSelected()){
			Correcto = true;
		} else if (this.cBcri.isSelected()){
			Correcto = true;
		} else if (this.cBav1.isSelected()){
			Correcto = true;
		} else if (this.cBav2.isSelected()){
			Correcto = true;
		} else if (this.cBav3.isSelected()){
			Correcto = true;
		} else if (this.cNoTiene.isSelected()){
			Correcto = true;
		}

		// si no seleccionó ningún checkbox
		if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
						"Debe marcar al menos una opción\n" +
						"(puede ser No Tiene)",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

		}

		// si no seleccionó la fibrosis
		if (this.cFibrosis.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si presenta fibrosis",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.cFibrosis.requestFocus();
			return;

		}

		// inicializamos la variable control
		Correcto = false;

		// verifica que al menos halla marcado uno de los checbox
		if (this.cAumento.isSelected()){
			Correcto = true;
		} else if (this.cWpw.isSelected()){
			Correcto = true;
		} else if (this.cHvi.isSelected()){
			Correcto = true;
		} else if (this.cSinTranstornos.isSelected()){
			Correcto = true;
		}

		// si no marcó ninguna opción
		if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
						"Debe marcar al menos una opción\n" +
						"(puede ser No Tiene)",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

		}

		// si no indicó los transtornos de repolarización
		if (this.cRepolarizacion.getSelectedItem().toString().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique si presenta transtornos \n" +
                        "de repolarización",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;

		}

		// grabamos el registro
		this.grabaElectro();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de validar el formulario que
	 * actualiza el registro en el servidor
	 */
	private void grabaElectro(){

		// asignamos en la clase
		this.Cardio.setId(this.IdElectro);
		this.Cardio.setProtocolo(this.Protocolo);
		this.Cardio.setVisita(this.Visita);
		this.Cardio.setFecha(this.Herramientas.fechaJDate(this.dFecha));

		// según el valor del combo
		if (this.cNormal.getSelectedItem().toString().equals("Si")){
			this.Cardio.setNormal(1);
		} else {
			this.Cardio.setNormal(0);
		}

		// según el valor de bradicardia
		if (this.cBradicardia.getSelectedItem().toString().equals("Si")){
			this.Cardio.setBradicardia(1);
		} else {
			this.Cardio.setBradicardia(0);
		}

		// fija el valor de arritmia y continuamos
		this.Cardio.setArritmia(this.cArritmia.getSelectedItem().toString());
		this.Cardio.setFc(Integer.parseInt(this.tFc.getText()));
		this.Cardio.setQrs(Float.parseFloat(this.tQrs.getText()));
		this.Cardio.setEjeqrs(Integer.parseInt(this.sEjeQrs.getValue().toString()));
		this.Cardio.setPr(Float.parseFloat(this.tPr.getText()));

		// según el estado de los combos
		if (this.cBrd.isSelected()){
			this.Cardio.setBrd(1);
		} else {
			this.Cardio.setBrd(0);
		}
		if (this.cBrdModerado.isSelected()){
			this.Cardio.setBrdModerado(1);
		} else {
			this.Cardio.setBrdModerado(0);
		}
		if (this.cBcrd.isSelected()){
			this.Cardio.setBrcd(1);
		} else {
			this.Cardio.setBrcd(0);
		}
		if (this.cTendencia.isSelected()){
			this.Cardio.setTendenciaHbai(1);
		} else {
			this.Cardio.setTendenciaHbai(0);
		}
		if (this.cHbai.isSelected()){
			this.Cardio.setHbai(1);
		} else {
			this.Cardio.setHbai(0);
		}
		if (this.cTciv.isSelected()){
			this.Cardio.setTciv(1);
		} else {
			this.Cardio.setTciv(0);
		}
		if (this.cBcri.isSelected()){
			this.Cardio.setBcri(1);
		} else {
			this.Cardio.setBcri(0);
		}
		if (this.cBav1.isSelected()){
			this.Cardio.setBav1g(1);
		} else {
			this.Cardio.setBav1g(0);
		}
		if (this.cBav2.isSelected()){
			this.Cardio.setBav2g(1);
		} else {
			this.Cardio.setBav2g(0);
		}
		if (this.cBav3.isSelected()){
			this.Cardio.setBav3g(1);
		} else {
			this.Cardio.setBav3g(0);
		}
		if (this.cNoTiene.isSelected()){
			this.Cardio.setNoTiene(1);
		} else {
			this.Cardio.setNoTiene(0);
		}

		// asignamos el valor de fibrosis
		if (this.cFibrosis.getSelectedItem().toString().equals("Si")){
			this.Cardio.setFibrosis(1);
		} else {
			this.Cardio.setFibrosis(0);
		}

		// según el valor de los checkbox
		if (this.cAumento.isSelected()){
			this.Cardio.setAumentoAi(1);
		} else {
			this.Cardio.setAumentoAi(0);
		}
		if (this.cWpw.isSelected()){
			this.Cardio.setWpv(1);
		} else {
			this.Cardio.setWpv(0);
		}
		if (this.cHvi.isSelected()){
			this.Cardio.setHvi(1);
		} else {
			this.Cardio.setHvi(0);
		}
		if (this.cSinTranstornos.isSelected()){
			this.Cardio.setSinTranstornos(1);
		} else {
			this.Cardio.setSinTranstornos(0);
		}

		// almacenamos la repolarización
		this.Cardio.setRepolarizacion(this.cRepolarizacion.getSelectedItem().toString());

		// grabamos el registro
		this.IdElectro = this.Cardio.grabaElectro();

		// presentamos el mensaje
		new Mensaje("Registro grabado ...");

		// actualizamos en el formulario padre

		// cerramos el formulario
		this.dispose();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * recarga el registro o limpia el formulario según
	 * el valor de la variable control
	 */
	private void cancelaElectro(){

		// recargamos el registro
		this.verElectro(this.IdElectro);

	}

}
