/*

    Nombre: FormDrogas
    Fecha: 16/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que implementa el formulario para el
	             ABM de drogas utilzadas en el tratamiento

 */

// definición del paquete
package drogas;

// importamos las librerías
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import drogas.Drogas;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormDrogas extends JDialog {

	// declara el serial id
	private static final long serialVersionUID = 500956862139312086L;

	// declaración de variables
	private JTextField tId;;
	private JTextField tDroga;
	private JTextField tDosis;
	private JTextField tComercial;
	private JTextField tUsuario;
	private JTextField tFecha;
	private JTable tablaDrogas;
	private Drogas Medicacion;

	// crea el formulario y fija las propiedades
	@SuppressWarnings("serial")
	public FormDrogas(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// instanciamos la clase de la base
		this.Medicacion = new Drogas();

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// define las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setBounds(120, 150, 785, 326);
		getContentPane().setLayout(null);

		// presenta el título
		JLabel lTitulo = new JLabel("Diccionario de Drogas Utilizadas");
		lTitulo.setBounds(10, 10, 550, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presenta la clave
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 40, 26);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setFont(Fuente);
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// pide el nombre de la droga
		this.tDroga = new JTextField();
		this.tDroga.setBounds(65, 45, 220, 26);
		this.tDroga.setToolTipText("Indique el nombre de la droga");
		this.tDroga.setFont(Fuente);
		getContentPane().add(this.tDroga);

		// pide la dosis
		this.tDosis = new JTextField();
		this.tDosis.setBounds(295, 45, 60, 26);
		this.tDosis.setToolTipText("Indique la dosis en miligramos");
		this.tDosis.setFont(Fuente);
		getContentPane().add(this.tDosis);

		// pide el nombre comercial
		this.tComercial = new JTextField();
		this.tComercial.setBounds(360, 45, 210, 26);
		this.tComercial.setToolTipText("Nombre comercial de la droga");
		this.tComercial.setFont(Fuente);
		getContentPane().add(this.tComercial);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(575, 45, 70, 26);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.tUsuario.setFont(Fuente);
		getContentPane().add(this.tUsuario);

		// presenta la fecha de alta
		this.tFecha = new JTextField();
		this.tFecha.setBounds(650, 45, 85, 26);
		this.tFecha.setToolTipText("Fecha de alta del registro");
		this.tFecha.setEditable(false);
		this.tFecha.setFont(Fuente);
		getContentPane().add(this.tFecha);

		// presenta el botón grabar
		JButton btnGrabar = new JButton("");
		btnGrabar.setBounds(745, 45, 26, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				verificaDroga();
			}
		});
		getContentPane().add(btnGrabar);

		// define el scroll
		JScrollPane scrollDrogas = new JScrollPane();
		scrollDrogas.setBounds(10, 80, 760, 215);
		getContentPane().add(scrollDrogas);

		// define la tabla
		this.tablaDrogas = new JTable();
		this.tablaDrogas.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null},
			},
			new String[] {
				"ID",
				"Droga",
				"Dosis",
				"Comercial",
				"Usuario",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				Integer.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// establecemos el ancho de las columnas
		this.tablaDrogas.getColumn("ID").setPreferredWidth(30);
		this.tablaDrogas.getColumn("ID").setMaxWidth(30);
		this.tablaDrogas.getColumn("Droga").setPreferredWidth(200);
		this.tablaDrogas.getColumn("Droga").setMaxWidth(200);
		this.tablaDrogas.getColumn("Dosis").setPreferredWidth(70);
		this.tablaDrogas.getColumn("Dosis").setMaxWidth(70);
		this.tablaDrogas.getColumn("Usuario").setPreferredWidth(70);
		this.tablaDrogas.getColumn("Usuario").setMaxWidth(70);
		this.tablaDrogas.getColumn("Ed.").setPreferredWidth(35);
		this.tablaDrogas.getColumn("Ed.").setMaxWidth(35);
		this.tablaDrogas.getColumn("El.").setPreferredWidth(35);
		this.tablaDrogas.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tablaDrogas.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tablaDrogas.setRowHeight(25);

		// definimos la fuente
		this.tablaDrogas.setFont(Fuente);

		// agregamos la tabla al scroll
		scrollDrogas.setViewportView(this.tablaDrogas);

		// fijamos el evento click
		this.tablaDrogas.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tDrogasMouseClicked(evt);
			}
		});

		// cargamos las drogas declaradas
		this.cargaDrogas();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla de drogas
	 */
	protected void cargaDrogas(){

		// obtenemos la nómina de drogas
		ResultSet Nomina = this.Medicacion.nominaDrogas();

		// sobrecargamos el renderer de la tabla
		this.tablaDrogas.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tablaDrogas.getModel();

		// hacemos la tabla se pueda ordenar
		this.tablaDrogas.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[7];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("droga");
				fila[2] = Nomina.getInt("dosis");
				fila[3] = Nomina.getString("comercial");
				fila[4] = Nomina.getString("usuario");
				fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

			// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica el
	 * registro antes de enviarlo al servidor
	 */
	protected void verificaDroga(){

		// verifica el nombre de la droga
		if (this.tDroga.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el nombre de la droga",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tDroga.requestFocus();
			return;

		}

		// verifica la dosis
		if (this.tDosis.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Indique la dosis en miligramos",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tDroga.requestFocus();
			return;

		}

		// si está dando altas
		if (this.tId.getText().isEmpty()){

			// verificamos que no esté repetido
			if (this.Medicacion.validaDroga(this.tDroga.getText(), Integer.parseInt(this.tDosis.getText()))){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this,
							"Esa droga ya se encuentra ingresada",
							"Error",
						    JOptionPane.ERROR_MESSAGE);
				this.tDroga.requestFocus();
				return;

			}

		}

		// el nombre comercial lo permite en blanco

		// graba el registro
		this.grabaDroga();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de verificar el formulario y
	 * ejecuta la consulta de actualización
	 */
	protected void grabaDroga(){

		// si está insertando
		if (!this.tId.getText().isEmpty()){
			this.Medicacion.setId(Integer.parseInt(this.tId.getText()));
		} else {
			this.Medicacion.setId(0);
		}

		// asigna el resto de los valores
		this.Medicacion.setDroga(this.tDroga.getText());
		this.Medicacion.setDosis(Integer.parseInt(this.tDosis.getText()));
		this.Medicacion.setComercial(this.tComercial.getText());

		// grabamos el registro
		this.Medicacion.grabaDroga();

		// recargamos la grilla
		this.cargaDrogas();

		// limpiamos el registro
		this.limpiaDroga();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param iddroga entero con la clave del registro
	 * Método que recibe como parámetro la clave del registro
	 * y obtiene los datos del servidor para presentarlos en
	 * el formulario
	 */
	protected void verDatosDroga(int iddroga){

		// obtenemos los datos del registro
		this.Medicacion.getDatosDroga(iddroga);

		// los presentamos
		this.tId.setText(Integer.toString(this.Medicacion.getId()));
		this.tDroga.setText(this.Medicacion.getDroga());
		this.tDosis.setText(Integer.toString(this.Medicacion.getDosis()));
		this.tComercial.setText(this.Medicacion.getComercial());
		this.tUsuario.setText(this.Medicacion.getUsuario());
		this.tFecha.setText(this.Medicacion.getFecha());

		// fijamos el foco
		this.tDroga.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param iddroga entero con la clave del registro
	 * Método que recibe como parámetro la clave del
	 * registro y luego de pedir confirmación, ejecuta la
	 * consulta de eliminación
	 */
	protected void borraDroga(int iddroga){

		// verifica si puede borrar
		if (!this.Medicacion.puedeBorrar(iddroga)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Esa droga está asignada a un paciente",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
									"Está seguro que desea eliminar el registro?",
									"Drogas",
									JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
									null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Medicacion.borraDroga(iddroga);

			// limpiamos el formulario
			this.limpiaDroga();

			// recargamos la grilla
			this.cargaDrogas();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario de datos
	 */
	protected void limpiaDroga(){

		// inicializamos el formulario
		this.tId.setText("");
		this.tDroga.setText("");
		this.tDosis.setText("");
		this.tComercial.setText("");
		this.tUsuario.setText("");
		this.tFecha.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de drogas
	 */
	protected void tDrogasMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tablaDrogas.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tablaDrogas.rowAtPoint(evt.getPoint());
		int columna = this.tablaDrogas.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tablaDrogas.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 5) {

				// cargamos el registro
				this.verDatosDroga(clave);

				// si pulsó en eliminar
			} else if (columna == 6) {

				// eliminamos
				this.borraDroga(clave);

			}

		}

	}

}
