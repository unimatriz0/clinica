/*

    Nombre: Drogas
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 drogas utilizadas en el tratamiento

*/

// declaración del paquete
package drogas;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Drogas {

    // declaración de variables
    protected Conexion Enlace;               // puntero a la base de datos
    protected int Id;                        // clave del registro
    protected String Droga;                  // nombre de la droga
    protected int Dosis;                     // dosis en miligramos
    protected String Comercial;              // nombre comercial de la droga
    protected int IdUsuario;                 // clave del usuario
    protected String Usuario;                // nombre del usuario
    protected String Fecha;                  // fecha de alta del registro

    // constructor de la clase
    public Drogas (){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Droga = "";
        this.Dosis = 0;
        this.Comercial = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setDroga(String droga){
        this.Droga = droga;
    }
    public void setDosis(int dosis){
        this.Dosis = dosis;
    }
    public void setComercial(String comercial){
        this.Comercial = comercial;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public String getDroga(){
        return this.Droga;
    }
    public int getDosis(){
        return this.Dosis;
    }
    public String getComercial(){
        return this.Comercial;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga entero con la clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * asigna en las variables de clase los valores del mismo
     */
    public void getDatosDroga(int iddroga){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_drogas.id AS id, " +
                   "       diagnostico.v_drogas.droga AS droga, " +
                   "       diagnostico.v_drogas.dosis AS dosis, " +
                   "       diagnostico.v_drogas.comercial AS comercial, " +
                   "       diagnostico.v_drogas.usuario AS usuario, " +
                   "       diagnostico.v_drogas.fecha AS fecha " +
                   "FROM diagnostico.v_drogas " +
                   "WHERE diagnostico.v_drogas.id = '" + iddroga + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            // los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Droga = Resultado.getString("droga");
            this.Dosis = Resultado.getInt("dosis");
            this.Comercial = Resultado.getString("comercial");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de registros
     * Método que retorna un resultset con el listado de
     * todas las drogas utilizadas, este es utilizado para
     * completar los combos ya que concatena la dosis
     */
    public ResultSet listaDrogas(){

        // declaración de variables
        ResultSet Resultado;
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_drogas.id AS id, " +
                   "       CONCAT(diagnostico.v_drogas.droga, ' - ', diagnostico.v_drogas.dosis) AS droga " +
                   "FROM diagnostico.v_drogas " +
                   "ORDER BY diagnostico.v_drogas.droga; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return resultset con la nómina de registros
     * Método que retorna un resultset con el listado de
     * todas las drogas utilizadas
     */
    public ResultSet nominaDrogas(){

        // declaración de variables
        ResultSet Resultado;
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_drogas.id AS id, " +
                   "       diagnostico.v_drogas.droga AS droga, " +
                   "       diagnostico.v_drogas.dosis AS dosis, " +
                   "       diagnostico.v_drogas.comercial AS comercial, " +
                   "       diagnostico.v_drogas.usuario AS usuario " +
                   "FROM diagnostico.v_drogas " +
                   "ORDER BY diagnostico.v_drogas.droga; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro afectado
     */
    public int grabaDroga(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaDroga();
        } else {
            this.editaDroga();
        }

        // retorna la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaDroga(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.drogas " +
                   "            (droga, " +
                   "             dosis, " +
                   "             comercial, " +
                   "             usuario) " +
                   "            VALUES " +
                   "            (?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Droga);
            psInsertar.setInt(2, this.Dosis);
            psInsertar.setString(3, this.Comercial);
            psInsertar.setInt(4, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaDroga(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.drogas SET " +
                   "       droga = ?, " +
                   "       dosis = ?, " +
                   "       comercial = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.drogas.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Droga);
            psInsertar.setInt(2, this.Dosis);
            psInsertar.setString(3, this.Comercial);
            psInsertar.setInt(4, this.IdUsuario);
            psInsertar.setInt(5, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga clave de la droga
     * @return boolean si puede borrar
     * Método llamado antes de borrar que verifica si en la
     * tabla de tratamiento existe algún registro para la
     * droga que recibe como parámetro
     */
    public boolean puedeBorrar(int iddroga){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Borrar = false;

        // componemos la consulta sobre la tabla de tratamiento
        Consulta = "SELECT COUNT(diagnostico.tratamiento.id) AS registros " +
                   "FROM diagnostico.tratamiento " +
                   "WHERE diagnostico.tratamiento.droga = '" + iddroga + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // según los registros
            if (Resultado.getInt("registros") == 0){
                Borrar = true;
            } else {
                Borrar = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // retornamos
        return Borrar;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iddroga entero con la clave del registro
     * M{etodo que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public void borraDroga(int iddroga){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.drogas " +
                   "WHERE diagnostico.drogas.id = '" + iddroga + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param droga cadena con el nombre de la droga
     * @param dosis entero con la dosis
     * @return boolean si existe la droga en la base
     * Método utilizado para evitar el ingreso de registros
     * duplicados recibe como parámetros el nombre de la
     * droga y la dosis y retorna si ya está declarada
     * en la base
     */
    public Boolean validaDroga(String droga, int dosis){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Existe = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.drogas.id) AS registros " +
                   "FROM diagnostico.drogas " +
                   "WHERE diagnostico.drogas.droga = '" + droga + "' AND " +
                   "      diagnostico.drogas.dosis = '" + dosis + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // según los registros
            if (Resultado.getInt("registros") == 0){
                Existe = false;
            } else {
                Existe = true;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // retornamos
        return Existe;

    }

}
