/*
 *
 * Nombre: FormDeterminaciones
 * Fecha: 05/01/2019
 * Autor: Lic. Claudio Invernizzi
 * E-Mail: cinvernizzi@gmail.com
 * Proyecto: Diagnostico
 * Licencia: GPL
 * Producido en: INP - Dr. Mario Fatala Chaben
 * Buenos Aires - Argentina
 * Comentarios: Formulario de presentación de datos de las determinaciones
 *
 */

// definición del paquete 
package determinaciones;

// importamos las librerías
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

// definimos la clase
public class FormDeterminaciones extends JPanel {

	// declara el serial id
	private static final long serialVersionUID = 1L;

	// declaramos las variables
	JPanel DatosDeterminacion;
	
	// creamos el panel
	public FormDeterminaciones(JTabbedPane Tabulador) {

		// inicializamos el contenedor
		this.DatosDeterminacion = new JPanel();
		this.DatosDeterminacion.setBounds(0, 0, 945, 620);
		this.DatosDeterminacion.setLayout(null);
		this.DatosDeterminacion.setToolTipText("Datos de las Determinaciones");

		 // agregamos el panel al tabulador
        Tabulador.addTab("Determinaciones", 
                          new javax.swing.ImageIcon(getClass().getResource("/Graficos/mblood.png")), 
                          this.DatosDeterminacion, 
                          "Datos de las Determinaciones");
		
	}

}
