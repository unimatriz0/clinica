/*

    Nombre: FormHolter
    Fecha: 25/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que arma el formulario para el abm de
	             los holter

 */

// definición del paquete
package holter;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import java.awt.Dialog;
import java.awt.Font;

// definición de la clase
public class FormHolter extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = 1544259878262502141L;

	// definición de las variables de clase
	private JDateChooser dFecha;            // fecha del holter
	private JSpinner sLatidos;              // latidos totales
	private JSpinner sMedia;                // media de latidos
	private JSpinner sMin;                  // fecuencia mínima
	private JSpinner sMax;                  // frecuencia máxima
	private JCheckBox cEv;                  // si tuvo eventos ventriculares
	private JSpinner sNev;                  // número de eventos ventriculares
	private JTextField tTasaEv;             // tasa de eventos ventriculares
	private JCheckBox cEsv;                 // si tuvo episodios extraventriculares
	private JSpinner sNesv;                 // número de episodios extraventriculares
	private JTextField tTasaNesv;           // tasa de episodios extraventriculares
	private JCheckBox cNormal;              // si el resultado es normal
	private JCheckBox cBradicardia;         // si presentó bradicardia
	private JCheckBox cCrono;               // si hubo respuesta cronotrópica
	private JCheckBox cArritmiaSevera;      // si hubo arritmia severa
	private JCheckBox cArritmiaSimple;      // si presentó arritmia simple
	private JCheckBox cArritmiaSupra;       // si hubo arritmia supraventricular
	private JCheckBox cBloqueo;             // si hubo bloqueo de rama
	private JCheckBox cBav;                 // hubo bloqueo
	private JCheckBox cDisociacion;         // si presentó disociación

	// declaramos las variables de clase
	private int Paciente;             // clave del paciente
	private int Visita;               // clave de la visita
	private int Id;                   // clave del registro

	// constructor de la clase recibe como parámetros el 
	// protocolo y la clave de la visita
	public FormHolter(Dialog parent, boolean modal, int protocolo, int idvisita) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// fijamos las propiedades
		this.setBounds(150, 150, 585, 370);
		this.setLayout(null);
		this.setTitle("Holter");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// asignamos en la variable de clase
		this.Paciente = protocolo;

		// iniciamos el formulario
		this.initForm();

		// mostramos el registro
		this.verHolter(idvisita);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa los controles del formulario
	 */
	private void initForm(){

		// define las fuentes
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Datos del Holter</b></html>");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(10, 10, 200, 26);
		this.add(lTitulo);

		// pide la fecha
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setBounds(10, 45, 70, 26);
		lFecha.setFont(Fuente);
		this.add(lFecha);
		this.dFecha = new JDateChooser();
		this.dFecha.setBounds(55, 45, 100, 26);
		this.dFecha.setToolTipText("Indique la fecha del estudio");
		this.dFecha.setFont(Fuente);
		this.add(this.dFecha);

		// pide los latidos totales
		JLabel lLatidos = new JLabel("Latidos Totales:");
		lLatidos.setBounds(180, 45, 140, 26);
		lLatidos.setFont(Fuente);
		this.add(lLatidos);
		this.sLatidos = new JSpinner();
		this.sLatidos.setBounds(280, 45, 70, 26);
		this.sLatidos.setFont(Fuente);
		this.sLatidos.setToolTipText("Indique el número total de latidos");
		this.add(this.sLatidos);

		// label de la frecuencia
		JLabel lFc = new JLabel("FC:");
		lFc.setBounds(10, 80, 70, 26);
		lFc.setFont(Fuente);
		this.add(lFc);

		// pide la media
		JLabel lMedia = new JLabel("Media:");
		lMedia.setBounds(45, 80, 70, 26);
		lMedia.setFont(Fuente);
		getContentPane().add(lMedia);
		this.sMedia = new JSpinner();
		this.sMedia.setBounds(90, 80, 50, 26);
		this.sMedia.setFont(Fuente);
		this.sMedia.setToolTipText("Indique el promedio de frecuencia");
		this.add(this.sMedia);

		// pide la mínima
		JLabel lMin = new JLabel("Min:");
		lMin.setBounds(170, 80, 44, 26);
		lMin.setFont(Fuente);
		this.add(lMin);
		this.sMin = new JSpinner();
		this.sMin.setBounds(200, 80, 50, 26);
		this.sMin.setFont(Fuente);
		this.sMin.setToolTipText("Indique la frecuencia mínima");
		this.add(this.sMin);

		// pide la máxima
		JLabel lMax = new JLabel("Max:");
		lMax.setBounds(275, 80, 44, 26);
		lMax.setFont(Fuente);
		this.add(lMax);
		this.sMax = new JSpinner();
		this.sMax.setBounds(305, 80, 50, 26);
		this.sMax.setFont(Fuente);
		this.sMax.setToolTipText("Indique la frecuencia máxima");
		this.add(this.sMax);

		// pide el ev
		this.cEv = new JCheckBox("Eventos:");
		this.cEv.setBounds(10, 115, 70, 26);
		this.cEv.setToolTipText("SIndique si hubo eventos");
		this.cEv.setFont(Fuente);
		this.add(this.cEv);

		// el número
		JLabel lN = new JLabel("N°:");
		lN.setBounds(140, 115, 37, 26);
		lN.setFont(Fuente);
		this.add(lN);
		this.sNev = new JSpinner();
		this.sNev.setBounds(150, 115, 40, 26);
		this.sNev.setFont(Fuente);
		this.sNev.setToolTipText("Indique el número de eventos");
		this.add(this.sNev);

		// verificar máscara
		this.tTasaEv = new JTextField();
		this.tTasaEv.setBounds(275, 115, 80, 26);
		this.tTasaEv.setFont(Fuente);
		this.tTasaEv.setToolTipText("Tasa de eventos ventriculares");
		this.tTasaEv.setEditable(false);
		this.add(this.tTasaEv);

		// pide los eventos supraventriculares
		this.cEsv = new JCheckBox("ESV:");
		this.cEsv.setBounds(10, 150, 70, 26);
		this.cEsv.setToolTipText("Indique si hubo eventos supraventriculares");
		this.cEsv.setFont(Fuente);
		this.add(this.cEsv);

		// pide el número de eventos supraventriculares
		JLabel lNEsv = new JLabel("N°:");
		lNEsv.setBounds(140, 150, 40, 26);
		lNEsv.setFont(Fuente);
		getContentPane().add(lNEsv);
		this.sNesv = new JSpinner();
		this.sNesv.setBounds(175, 150, 80, 26);
		this.sNesv.setFont(Fuente);
		this.sNesv.setToolTipText("Indique el número de eventos supraventriculares");
		this.add(this.sNesv);

		// presenta la tasa
		this.tTasaNesv = new JTextField();
		this.tTasaNesv.setBounds(275, 150, 80, 26);
		this.tTasaNesv.setFont(Fuente);
		this.tTasaNesv.setToolTipText("Tasa de eventos supraventriculares");
		this.tTasaNesv.setEditable(false);
		this.add(this.tTasaNesv);

		// presenta el checbox de normal
		this.cNormal = new JCheckBox("Normal");
		this.cNormal.setBounds(10, 185, 115, 26);
		this.cNormal.setFont(Fuente);
		this.cNormal.setToolTipText("Marque si el resultado fue normal");
		this.add(this.cNormal);

		// el checkbox de bradicardia
		this.cBradicardia = new JCheckBox("Bradicardia Sinusal");
		this.cBradicardia.setBounds(170, 185, 160, 26);
		this.cBradicardia.setFont(Fuente);
		this.cBradicardia.setToolTipText("Marque si presentó bradicardia");
		this.add(this.cBradicardia);

		// el checkbox de respuestra cronotrópica
		this.cCrono = new JCheckBox("Rta. Cronotrópica");
		this.cCrono.setBounds(330, 185, 140, 26);
		this.cCrono.setFont(Fuente);
		this.cCrono.setToolTipText("Marque si presentó rta. cronotrópica");
		this.add(this.cCrono);

		// el check de arritmia ventricular
		this.cArritmiaSevera = new JCheckBox("Arritmia Vent. Severa");
		this.cArritmiaSevera.setBounds(10, 220, 165, 26);
		this.cArritmiaSevera.setFont(Fuente);
		this.cArritmiaSevera.setToolTipText("Maruqe si presentó arritmia severa");
		this.add(this.cArritmiaSevera);

		// el check de arritmia simple
		this.cArritmiaSimple = new JCheckBox("Arritmia Vent. Simple");
		this.cArritmiaSimple.setBounds(170, 220, 160, 26);
		this.cArritmiaSimple.setFont(Fuente);
		this.cArritmiaSimple.setToolTipText("Marque si presentó arritmia simple");
		this.add(this.cArritmiaSimple);

		// el check de arritmia supraventricular
		this.cArritmiaSupra = new JCheckBox("Arritmia Supraventricular");
		this.cArritmiaSupra.setBounds(330, 220, 200, 26);
		this.cArritmiaSupra.setFont(Fuente);
		this.cArritmiaSupra.setToolTipText("Marque si presentó arritmia supraventricular");
		this.add(this.cArritmiaSupra);

		// el check de bloqueo de rama
		this.cBloqueo = new JCheckBox("B. de Rama");
		this.cBloqueo.setBounds(10, 255, 115, 26);
		this.cBloqueo.setFont(Fuente);
		this.cBloqueo.setToolTipText("Indique si presenta bloqueo de rama");
		this.add(this.cBloqueo);

		// el check de bav
		this.cBav = new JCheckBox("BAV 2° G");
		this.cBav.setBounds(170, 255, 115, 26);
		this.cBav.setFont(Fuente);
		this.add(this.cBav);

		// el check de disociación
		this.cDisociacion = new JCheckBox("Disociación A-V");
		this.cDisociacion.setBounds(330, 255, 150, 26);
		this.cDisociacion.setFont(Fuente);
		this.cDisociacion.setToolTipText("Indique si presenta disociación");
		this.add(this.cDisociacion);

		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(330, 290, 110, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaHolter();
            }
        });
		this.add(btnGrabar);

		// el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(450, 290, 110, 26);
		btnCancelar.setToolTipText("Cierra el formulario sin grabar");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaHolter();
            }
        });
		this.add(btnCancelar);

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idvisita - clave de la visita
	 * Método llamado desde el constructor que presenta en el
	 * formulario el registro que recibe como parámetro
	 */
	private void verHolter(int idvisita){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que valida
	 * el formulario antes de enviarlo al servidor
	 */
	private void validaHolter(){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de validar el formulario que
	 * actualiza el registro de la base de datos
	 */
	private void grabaHolter(){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * limpia el formulario o recarga el registro según
	 * el estado de la variable control
	 */
	private void cancelaHolter(){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario
	 */
	private void limpiaHolter(){

	}

}
