/*

    Nombre: Holter
    Fecha: 22/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que provee los métodos para la tabla de holter

 */

// definición del paquete
package holter;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// definimos la clase
public class Holter {

    // definimos las variables
    private Conexion Enlace;               // puntero a la base de datos
    private int Id;                        // clave del registro
    private int Paciente;                  // clave del paciente
    private int Visita;                    // clave de la visita
    private String Fecha;                  // fecha de la toma
    private int Media;                     // frecuencia cardíaca media
    private int Minima;                    // frecuencia cardíaca mínima
    private int Maxima;                    // frecuencia cardíaca máxima
    private int Latidos;                   // número de latidos totales
    private int Ev;                        // si hubo eventos 0 no - 1 si
    private int Nev;                       // número de eventos
    private float Tasaev;                  // tasa de eventos
    private int Esv;                       // eventos extra entriculares 0 no - 1 si
    private int Nesv;                      // número de eventos
    private float Tasaesv;                 // tasa de eventos
    private int Normal;                    // si es normal 0 no - 1 si
    private int Bradicardia;               // si hubo bradicardia 0 no - 1 si
    private int RtaCronotopica;            // si hubo respuesta cronotópica
    private int ArritmiaSevera;            // si hubo arritmia severa
    private int ArritmiaSimple;            // si hubo arritmia simple
    private int ArritmiaSupra;             // si hubo arritmia supraventricular
    private int BdeRama;                   // si presenta bloqueo de rama
    private int Bav2g;                     // 0 no - 1 si
    private int Disociacion;               // si hubo disociación
    private String Comentarios;            // comentarios del usuario
    private int IdUsuario;                 // clave del usuario
    private String Usuario;                // nombre del usuario
    private String FechaAlta;              // fecha de alta del registro

    // constructor de la clase
    public Holter(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.Fecha = "";
        this.Media = 0;
        this.Minima = 0;
        this.Maxima = 0;
        this.Latidos = 0;
        this.Ev = 0;
        this.Nev = 0;
        this.Tasaev = 0;
        this.Esv = 0;
        this.Nesv = 0;
        this.Tasaesv = 0;
        this.Normal = 0;
        this.Bradicardia = 0;
        this.RtaCronotopica = 0;
        this.ArritmiaSevera = 0;
        this.ArritmiaSimple = 0;
        this.ArritmiaSupra = 0;
        this.BdeRama = 0;
        this.Bav2g = 0;
        this.Disociacion = 0;
        this.Comentarios = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de variables
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }
    public void setMedia(int media){
        this.Media = media;
    }
    public void setMinima(int minima){
        this.Minima = minima;
    }
    public void setMaxima(int maxima){
        this.Maxima = maxima;
    }
    public void setLatidos(int latidos){
        this.Latidos = latidos;
    }
    public void setEv(int ev){
        this.Ev = ev;
    }
    public void setNev(int nev){
        this.Nev = nev;
    }
    public void setTasaEv(float tasa){
        this.Tasaev = tasa;
    }
    public void setEsv(int esv){
        this.Esv = esv;
    }
    public void setNesv(int nesv){
        this.Nesv = nesv;
    }
    public void setTasaNesv(float tasa){
        this.Tasaesv = tasa;
    }
    public void setNormal(int normal){
        this.Normal = normal;
    }
    public void setBradicardia(int bradicardia){
        this.Bradicardia = bradicardia;
    }
    public void setRtaCronotopica(int cronotopica){
        this.RtaCronotopica = cronotopica;
    }
    public void setArritmiaSevera(int arritmia){
        this.ArritmiaSevera = arritmia;
    }
    public void setArritmiaSimple(int arritmia){
        this.ArritmiaSimple = arritmia;
    }
    public void setArritmiaSupra(int arritmia){
        this.ArritmiaSupra = arritmia;
    }
    public void setBdeRama(int bloqueo){
        this.BdeRama = bloqueo;
    }
    public void setBav2g(int bav){
        this.Bav2g = bav;
    }
    public void setDisociacion(int disociacion){
        this.Disociacion = disociacion;
    }
    public void setComentarios(String comentarios){
        this.Comentarios = comentarios;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getFecha(){
        return this.Fecha;
    }
    public int getMedia(){
        return this.Media;
    }
    public int getMinima(){
        return this.Minima;
    }
    public int getMaxima(){
        return this.Maxima;
    }
    public int getLatidos(){
        return this.Latidos;
    }
    public int getEv(){
        return this.Ev;
    }
    public int getNev(){
        return this.Nev;
    }
    public float getTasaEv(){
        return this.Tasaev;
    }
    public int getEsv(){
        return this.Esv;
    }
    public int getNesv(){
        return this.Nesv;
    }
    public float getTasaesv(){
        return this.Tasaesv;
    }
    public int getNormal(){
        return this.Normal;
    }
    public int getBradicardia(){
        return this.Bradicardia;
    }
    public int getRtaCronotopica(){
        return this.RtaCronotopica;
    }
    public int getArritmiaSevera(){
        return this.getArritmiaSevera();
    }
    public int getArritmiaSimple(){
        return this.ArritmiaSimple;
    }
    public int getArritmiaSupra(){
        return this.ArritmiaSupra;
    }
    public int getBdeRama(){
        return this.BdeRama;
    }
    public int getBav2g(){
        return this.Bav2g;
    }
    public int getDisociacion(){
        return this.Disociacion;
    }
    public String getComentarios(){
        return this.Comentarios;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param paciente - clave del paciente
     * @return resultset con los registros encontrados
     * Método utilizado en la impresión de historias agrupadas
     * por estudio que recibe como parámetro el protocolo de
     * un paciente y retorna el vector con todos los registros
     */
    public ResultSet nominaVisitas(int paciente){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_holter.id AS id, " +
                   "       diagnostico.v_holter.paciente AS paciente, " +
                   "       diagnostico.v_holter.visita AS visita, " +
                   "       diagnostico.v_holter.fecha AS fecha, " +
                   "       diagnostico.v_holter.media AS media, " +
                   "       diagnostico.v_holter.minima AS minima, " +
                   "       diagnostico.v_holter.maxima AS maxima, " +
                   "       diagnostico.v_holter.laticos AS latidos, " +
                   "       diagnostico.v_holter.ev AS ev, " +
                   "       diagnostico.v_holter.nev AS nev, " +
                   "       diagnostico.v_holter.tasaev AS tasaev, " +
                   "       diagnostico.v_holter.esv AS esv, " +
                   "       diagnostico.v_holter.nesv AS nesv, " +
                   "       diagnostico.v_holter.tasaesv AS tasaesv, " +
                   "       diagnostico.v_holter.normal AS normal, " +
                   "       diagnostico.v_holter.bradicardia AS bradicardia, " +
                   "       diagnostico.v_holter.rtacronotropica AS rtacronotropica, " +
                   "       diagnostico.v_holter.arritmiasevera AS arritmiasevera, " +
                   "       diagnostico.v_holter.arritmiasimple AS arritmiasimple, " +
                   "       diagnostico.v_holter.arritmiasupra AS arritmiasupra, " +
                   "       diagnostico.v_holter.bderama AS bderama, " +
                   "       diagnostico.v_holter.bav2g AS bav2g, " +
                   "       diagnostico.v_holter.disociacion AS disociacion, " +
                   "       diagnostico.v_holter.comentarios AS comentarios, " +
                   "       diagnostico.v_holter.usuario AS usuario " +
                   "FROM diagnostico.v_holter " +
                   "WHERE diagnostico.v_holter.paciente = '" + paciente + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_holter, '%d/%m/%Y') ASC; ";

        // obtenemos el vector y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param visita clave del registro
     * Método que recibe como parámetro la clave del registro
     * y asigna en las variables de clase los valores de la
     * base
     */
    public void getDatosHolter(int visita){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_holter.id AS id, " +
                   "       diagnostico.v_holter.paciente AS paciente, " +
                   "       diagnostico.v_holter.visita AS visita, " +
                   "       diagnostico.v_holter.fecha AS fecha, " +
                   "       diagnostico.v_holter.media AS media, " +
                   "       diagnostico.v_holter.minima AS minima, " +
                   "       diagnostico.v_holter.maxima AS maxima, " +
                   "       diagnostico.v_holter.laticos AS latidos, " +
                   "       diagnostico.v_holter.ev AS ev, " +
                   "       diagnostico.v_holter.nev AS nev, " +
                   "       diagnostico.v_holter.tasaev AS tasaev, " +
                   "       diagnostico.v_holter.esv AS esv, " +
                   "       diagnostico.v_holter.nesv AS nesv, " +
                   "       diagnostico.v_holter.tasaesv AS tasaesv, " +
                   "       diagnostico.v_holter.normal AS normal, " +
                   "       diagnostico.v_holter.bradicardia AS bradicardia, " +
                   "       diagnostico.v_holter.rtacronotropica AS rtacronotropica, " +
                   "       diagnostico.v_holter.arritmiasevera AS arritmiasevera, " +
                   "       diagnostico.v_holter.arritmiasimple AS arritmiasimple, " +
                   "       diagnostico.v_holter.arritmiasupra AS arritmiasupra, " +
                   "       diagnostico.v_holter.bderama AS bderama, " +
                   "       diagnostico.v_holter.bav2g AS bav2g, " +
                   "       diagnostico.v_holter.disociacion AS disociacion, " +
                   "       diagnostico.v_holter.comentarios AS comentarios, " +
                   "       diagnostico.v_holter.usuario AS usuario " +
                   "FROM diagnostico.v_holter " +
                   "WHERE diagnostico.v_holter.visita = '" + visita + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.Visita = Resultado.getInt("visita");
            this.Fecha = Resultado.getString("fecha");
            this.Media = Resultado.getInt("media");
            this.Minima = Resultado.getInt("minima");
            this.Maxima = Resultado.getInt("maxima");
            this.Latidos = Resultado.getInt("latidos");
            this.Ev = Resultado.getInt("ev");
            this.Tasaev = Resultado.getFloat("tasaev");
            this.Esv = Resultado.getInt("esv");
            this.Nesv = Resultado.getInt("nesv");
            this.Tasaesv = Resultado.getFloat("tasaesv");
            this.Normal = Resultado.getInt("normal");
            this.Bradicardia = Resultado.getInt("bradicardia");
            this.RtaCronotopica = Resultado.getInt("rtacronotropica");
            this.ArritmiaSevera = Resultado.getInt("arritmiasevera");
            this.ArritmiaSimple = Resultado.getInt("arritmiasimple");
            this.ArritmiaSupra = Resultado.getInt("arritmiasupra");
            this.BdeRama = Resultado.getInt("bderama");
            this.Bav2g = Resultado.getInt("bav2g");
            this.Disociacion = Resultado.getInt("disociacion");
            this.Comentarios = Resultado.getString("comentarios");
            this.Usuario = Resultado.getString("usuario");

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id del registro afectado
     * Método que ejecuta la consulta de edicion o eliminación
     * según corresponda, retorna la clave del registro afectado
     */
    public int grabaHolter(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoHolter();
        } else {
            this.editaHolter();
        }

        // retorna la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoHolter(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.holter " +
                   "       (paciente, " +
                   "        visita, " +
                   "        fecha, " +
                   "        media, " +
                   "        minima, " +
                   "        maxima, " +
                   "        latidos, " +
                   "        ev, " +
                   "        nev, " +
                   "        tasaev, " +
                   "        esv, " +
                   "        nesv, " +
                   "        tasaesv, " +
                   "        normal, " +
                   "        bradicardia, " +
                   "        rtacronotropica, " +
                   "        arritmiasevera, " +
                   "        arritmiasimple, " +
                   "        arritmiasupra, " +
                   "        bderama, " +
                   "        bav2g, " +
                   "        disociacion, " +
                   "        comentarios, " +
                   "        usuario) " +
                   "        VALUES " +
                   "        (?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?, ?, " +
                   "         ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Media);
            psInsertar.setInt(5,    this.Minima);
            psInsertar.setInt(6,    this.Maxima);
            psInsertar.setInt(7,    this.Latidos);
            psInsertar.setInt(8,    this.Ev);
            psInsertar.setInt(9,    this.Nev);
            psInsertar.setFloat(10, this.Tasaev);
            psInsertar.setInt(11,   this.Esv);
            psInsertar.setInt(12,   this.Nesv);
            psInsertar.setFloat(13, this.Tasaesv);
            psInsertar.setInt(14,   this.Normal);
            psInsertar.setInt(15,   this.Bradicardia);
            psInsertar.setInt(16,   this.RtaCronotopica);
            psInsertar.setInt(17,   this.ArritmiaSevera);
            psInsertar.setInt(18,   this.ArritmiaSimple);
            psInsertar.setInt(19,   this.ArritmiaSupra);
            psInsertar.setInt(20,   this.BdeRama);
            psInsertar.setInt(21,   this.Bav2g);
            psInsertar.setInt(22,   this.Disociacion);
            psInsertar.setString(23,this.Comentarios);
            psInsertar.setInt(24,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Metodo que ejecuta la consulta de edición
     */
    protected void editaHolter(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.holter SET " +
                   "       paciente = ?, " +
                   "       visita = ?, " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       media = ?, " +
                   "       minima = ?, " +
                   "       maxima = ?, " +
                   "       latidos = ?, " +
                   "       eventos = ?, " +
                   "       nev = ?, " +
                   "       tasaev = ?, " +
                   "       esv = ?, " +
                   "       nesv = ?, " +
                   "       tasanesv = ?, " +
                   "       normal = ?, " +
                   "       bradicardia = ?, " +
                   "       rtacronotropica = ?, " +
                   "       arritmiasevera = ?, " +
                   "       arritmiasimple = ?, " +
                   "       arritmiasupra = ?, " +
                   "       bderama = ?, " +
                   "       bav2g = ?, " +
                   "       disociacion = ?, " +
                   "       comentarios = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.holter.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Media);
            psInsertar.setInt(5,    this.Minima);
            psInsertar.setInt(6,    this.Maxima);
            psInsertar.setInt(7,    this.Latidos);
            psInsertar.setInt(8,    this.Ev);
            psInsertar.setInt(9,    this.Nev);
            psInsertar.setFloat(10, this.Tasaev);
            psInsertar.setInt(11,   this.Esv);
            psInsertar.setInt(12,   this.Nesv);
            psInsertar.setFloat(13, this.Tasaesv);
            psInsertar.setInt(14,   this.Normal);
            psInsertar.setInt(15,   this.Bradicardia);
            psInsertar.setInt(16,   this.RtaCronotopica);
            psInsertar.setInt(17,   this.ArritmiaSevera);
            psInsertar.setInt(18,   this.ArritmiaSimple);
            psInsertar.setInt(19,   this.ArritmiaSupra);
            psInsertar.setInt(20,   this.BdeRama);
            psInsertar.setInt(21,   this.Bav2g);
            psInsertar.setInt(22,   this.Disociacion);
            psInsertar.setString(23,this.Comentarios);
            psInsertar.setInt(24,   this.IdUsuario);
            psInsertar.setInt(25,   this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}
