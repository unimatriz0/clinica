/*

    Nombre: Clasificacion
    Fecha: /04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 clasificación del estadío de la enfermedad

 */

// declaración del paquete
package clasificacion;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Clasificacion {

    // declaración de las variables
    protected Conexion Enlace;               // puntero a la base de datos
    protected int Id;                        // clave del registro
    protected int Paciente;                  // clave del paciente
    protected int Visita;                    // clave de la visita
    protected String Estadio;                // estadío de la enfermedad
    protected String Observaciones;          // observaciones del usuario
    protected int IdUsuario;                 // clave del usuario
    protected String Usuario;                // nombre del usuario
    protected String Fecha;                  // fecha de alta del registro

    // constructor de la clase
    public Clasificacion(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.Estadio = "";
        this.Observaciones = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setEstadio(String estadio){
        this.Estadio = estadio;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }

    // métodos de devolución de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getEstadio(){
        return this.Estadio;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita clave de la visita
     * Método que recibe como parámetro la clave de un paciente
     * y asigna en las variables de clase los valores del
     * registro (se supone que solo puede haber una entrada
     * para cada paciente)
     */
    public void getDatosClasificacion(int idvisita){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta sobre la vista
        Consulta = "SELECT diagnostico.v_clasificacion.id AS id, " +
                   "       diagnostico.v_clasificacion.paciente AS paciente, " +
                   "       diagnostico.v_clasificacion.estadio AS estadio, " +
                   "       diagnostico.v_clasificacion.observaciones AS observaciones, " +
                   "       diagnostico.v_clasificacion.usuario AS usuario, " +
                   "       diagnostico.v_clasificacion.fecha AS fecha " +
                   "FROM diagnostico.v_clasificacion " +
                   "WHERE diagnostico.v_clasificacion.idvisita = '" + idvisita + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos los valores
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.Visita = Resultado.getInt("visita");
            this.Estadio = Resultado.getString("estadio");
            this.Observaciones = Resultado.getString("observaciones");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero, clave del registro afectado
     * Método que ejecuta la consulta de inserción o
     * edición según corresponda
     */
    public int grabaClasificacion(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaClasificacion();
        } else {
            this.editaClasificacion();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaClasificacion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.clasificacion " +
                   "       (paciente, " +
                   "        visita, " +
                   "        estadio, " +
                   "        observaciones, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Estadio);
            psInsertar.setString(4, this.Observaciones);
            psInsertar.setInt(5,    this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaClasificacion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.clasificacion SET " +
                   "       estadio = ?, " +
                   "       observaciones = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.clasificacion.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Estadio);
            psInsertar.setString(2, this.Observaciones);
            psInsertar.setInt(3, this.IdUsuario);
            psInsertar.setInt(4, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

}