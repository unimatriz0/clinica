/*

    Nombre: formValores
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que permite editar los valores admitidos para
                 cada técnica, opcionalmente, indicar máscara de entrada

 */

// declaración del paquete
package valores;

// importamos las librerías
import tecnicas.Tecnicas;
import valores.Valores;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.ImageIcon;
import funciones.RendererTabla;
import funciones.ComboClave;
import java.awt.Font;

/**
 * @author Lic. Claudio Invernizzi
 */

// declaración de la clase
public class FormValores extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // declaracion de las variables de clase
    private JScrollPane Scroll;
    private JComboBox <Object> cTecnica;
    private JTable tValores;
    private JTextField tId;
    private JTextField tValor;

    // declaramos las variables
    protected Tecnicas Metodos;
    protected Valores Medidas;

    /**
     * @param parent el formulario padre
     * @param modal valor que indica si será modal
     * Creates new form formValores
     */
    public FormValores(java.awt.Frame parent, boolean modal) {

        // definimos el constructor e iniciamos el formulario
        super(parent, modal);

        // definimos las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setLayout(null);
        this.setResizable(false);
        this.setBounds(250,200,400,400);

        // instanciamos la clase de tecnicas y de valores
        this.Metodos = new Tecnicas();
        this.Medidas = new Valores();

        // inicializamos el formulario
        this.initFormValores();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa los componentes del formulario
     */
    @SuppressWarnings("serial")
    protected void initFormValores(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // fijamos el título
        this.setTitle("valores de las Determinaciones");

        // definimos el titulo
        JLabel lTitulo = new JLabel("<html><b>Indique una técnica y luego los valores válidos</b></html>");
        lTitulo.setBounds(10,10,430,26);
        lTitulo.setFont(Fuente);
        this.add(lTitulo);

        // define el label de tecnica
        JLabel lTecnica = new JLabel("Tecnica:");
        lTecnica.setBounds(10,45,100,26);
        lTecnica.setFont(Fuente);
        this.add(lTecnica);

        // define el combo de tecnica
        this.cTecnica = new JComboBox<>();
        this.cTecnica.setToolTipText("Seleccione la técnica de la lista");
        this.cTecnica.setBounds(70,45,280,26);
        this.cTecnica.setFont(Fuente);
        this.add(this.cTecnica);

        // cargamos las técnicas
        this.cargaTecnicas();

        // definimos el evento onchange de la lista
        this.cTecnica.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cTecnicaItemStateChanged(evt);
            }
        });

        // presenta el texto de la id
        this.tId = new JTextField();
        this.tId.setBounds(10, 80, 50, 26);
        this.tId.setFont(Fuente);
        this.tId.setEditable(false);
        this.tId.setToolTipText("Clave del registro");
        this.add(this.tId);

        // presenta el texto del valor
        this.tValor = new JTextField();
        this.tValor.setBounds(70,80,100,26);
        this.tValor.setFont(Fuente);
        this.tValor.setToolTipText("Valor de lectura aceptado");
        this.add(this.tValor);

        // presenta el botón grabar
		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setBounds(180, 80, 115, 26);
        btnGrabar.setFont(Fuente);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		this.add(btnGrabar);
		btnGrabar.setIcon(new ImageIcon(FormValores.class.getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        // define la tabla
        this.tValores = new JTable();
        this.tValores.setToolTipText("Pulse para editar el valor");
        this.tValores.setColumnSelectionAllowed(true);
        this.tValores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tValoresMouseClicked(evt);
            }
        });

        // fija las propiedades de la tabla
        this.tValores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {null, null, null, null},
            new String [] {
                "ID", 
                "Valor o Máscara", 
                "Ed.", 
                "El."
            }
        ) {
            @SuppressWarnings("rawtypes")
            Class[] types = new Class [] {
                Integer.class, String.class, Object.class, Object.class
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        this.tValores.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);

        // fijamos el ancho de las columnas
        this.tValores.getColumn("ID").setPreferredWidth(35);
        this.tValores.getColumn("ID").setMaxWidth(35);
        this.tValores.getColumn("Ed.").setPreferredWidth(30);
        this.tValores.getColumn("Ed.").setMaxWidth(30);
        this.tValores.getColumn("El.").setPreferredWidth(30);
        this.tValores.getColumn("El.").setMaxWidth(30);

        // establece la fuente
        this.tValores.setFont(Fuente);

    	// establecemos el tooltip
		this.tValores.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tValores.setRowHeight(25);

        // agregamos la tabla al panel
        this.Scroll = new JScrollPane();
        this.Scroll.setBounds(15,120,330,235);
        this.add(this.Scroll);
        this.Scroll.setViewportView(this.tValores);

        // mostramos el formulario
        this.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga los elementos en el combo de técnicas
     */
    protected void cargaTecnicas(){

        // cargamos el combo de técnicas
        ResultSet listaTecnicas = Metodos.nominaTecnicas();

        // agregamos el primer elemento en blanco
        this.cTecnica.addItem(new ComboClave(0, ""));

        try {

            // nos aseguramos de estar al principio
            listaTecnicas.beforeFirst();

            // recorremos el vector
            while (listaTecnicas.next()){

                // lo agregamos al combo si es distinta de otra
                if (!"OTRA".equals(listaTecnicas.getString("tecnica"))){
                    this.cTecnica.addItem(new ComboClave(listaTecnicas.getInt("id_tecnica"), listaTecnicas.getString("tecnica")));
                }

            }

        // si hubo un error
        } catch (SQLException ex) {

            // presentamos el mensaje
            System.out.println(ex.getMessage());

        }

    }

    // evento llamado al pulsar sobre la grilla
    private void tValoresMouseClicked(java.awt.event.MouseEvent evt) {

        // primero verificamos que exista una técnica seleccionada
		ComboClave item = (ComboClave) this.cTecnica.getSelectedItem();
		if (item == null){

            // retornamos
            return;

		}

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tValores.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tValores.rowAtPoint(evt.getPoint());
        int columna = this.tValores.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tValores.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id de la marca
            int idValor = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 2){
            	this.getDatosTecnica(idValor);
            } else if (columna == 3){
            	this.eliminaValor(idValor);
            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvalor - clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * carga en el formulario los datos del mismo
     */
    private void getDatosTecnica(int idvalor){

        // obtenemos el valor del registro
        this.Medidas.getDatosValor(idvalor);

        // fijamos los valores del formulario
        this.tId.setText(Integer.toString(this.Medidas.getIdValor()));
        this.tValor.setText(this.Medidas.getValor());

        // fijamos el foco
        this.tValor.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvalor - clave del registro
     * Método que recibe como parámetro la clave de un registro y
     * luego de pedir confirmación ejecuta la consulta de eliminación
     */
    private void eliminaValor(int idvalor){

        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar el valor?",
                                   "Valores de Lectura",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.Medidas.borraValor(idvalor);

            // limpiamos el formulario
            this.limpiaFormValores();

            // recargamos la grilla
            this.muestraValores();

        }

    }

    // método llamado al cambiar el combo de técnica
    private void cTecnicaItemStateChanged(java.awt.event.ItemEvent evt) {

        // actualiza la grilla
        this.muestraValores();

    }

    // método que recarga la grilla de acuerdo al valor del combo
    private void muestraValores(){

        // primero verificamos que exista una técnica seleccionada
		ComboClave item = (ComboClave) this.cTecnica.getSelectedItem();
		if (item == null){

            // retornamos
            return;

		}

        // obtenemos la id de la tecnica
        int idtecnica = item.getClave();

        // obtenemos la nómina de marcas
        ResultSet listaValores = Medidas.nominaValores(idtecnica);

        // sobrecargamos el renderer de la tabla
        this.tValores.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tValores.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tValores.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[4];

        try {

            // iniciamos un bucle recorriendo el vector
            while (listaValores.next()){

                // fijamos los valores de la fila
                fila[0] = listaValores.getInt("id_valor");
                fila[1] = listaValores.getString("valor_tecnica");
                fila[2] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void btnGrabarActionPerformed(java.awt.event.ActionEvent evt){

        // obtenemos la clave de la técnica
        ComboClave item = (ComboClave) this.cTecnica.getSelectedItem();
        int tecnica = item.getClave();

        // si no hay una técnica seleccionada
        if (tecnica == 0){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Debe seleccionar una técnica", "Error", JOptionPane.ERROR_MESSAGE);
            return;

        } else {

            // asigna en la clase
            this.Medidas.setIdTecnica(tecnica);

        }

        // verificamos se halla ingresado un valor
        if (this.tValor.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, "Debe indicar un valor de lectura", "Error", JOptionPane.ERROR_MESSAGE);
            return;

        // si ingresó
        } else {

            // lo asigna en la clase
            this.Medidas.setValor(this.tValor.getText());

        }

        // si está insertando verificamos que no esté repetido
        if (this.tId.getText().isEmpty()){

            // si está repetido
            if (!this.Medidas.validaValor(tecnica, this.tValor.getText())){

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this, "Ese valor de lectura ya está declarado", "Error", JOptionPane.ERROR_MESSAGE);
                return;

            }

        // si está editando
        } else {

            // asigna en la clase
            this.Medidas.setIdValor(Integer.parseInt(this.tId.getText()));

        }

        // grabamos el registro
        this.Medidas.grabaValores();

        // limpiamos el formulario
        this.limpiaFormValores();

        // recarga la grilla
        this.muestraValores();

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado después de grabar o borrar que limpia
     * el formulario de datos
     */
    protected void limpiaFormValores(){

        // reiniciamos los campos
        this.tId.setText("");
        this.tValor.setText("");

    }

}
