/*

    Nombre: Digestivo
    Fecha: /04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla
                 del aparato digestivo

*/

// declaración del paquete
package digestivo;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Digestivo {

    // declaración de variables
    protected Conexion Enlace;          // puntero a la base de datos
    protected int Id;                   // clave del registro
    protected int Paciente;             // clave del paciente
    protected int Disfagia;             // 0 no 1 si
    protected int Pirosis;              // 0 no 1 si
    protected int Regurgitacion;        // 0 no 1 si
    protected int Constipacion;         // o no 1 si
    protected int Bolo;                 // 0 no 1 si
    protected int NoTiene;              // 0 no 1 si
    protected int IdUsuario;            // clave del usuario
    protected String Usuario;           // nombre del usuario
    protected String Fecha;             // fecha de alta del registro

    // constructor de la clase
    public Digestivo(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.initDigestivo();

    }

    // metodo que inicializa las variables de clase
    private void initDigestivo(){

        this.Id = 0;
        this.Paciente = 0;
        this.Disfagia = 0;
        this.Pirosis = 0;
        this.Regurgitacion = 0;
        this.Constipacion = 0;
        this.Bolo = 0;
        this.NoTiene = 0;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setDisfagia(int disfagia){
        this.Disfagia = disfagia;
    }
    public void setPirosis(int pirosis){
        this.Pirosis = pirosis;
    }
    public void setRegurgitacion(int regurgitacion){
        this.Regurgitacion = regurgitacion;
    }
    public void setConstipacion(int constipacion){
        this.Constipacion = constipacion;
    }
    public void setBolo(int bolo){
        this.Bolo = bolo;
    }
    public void setTiene(int tiene){
        this.NoTiene = tiene;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getDisfagia(){
        return this.Disfagia;
    }
    public int getPirosis(){
        return this.Pirosis;
    }
    public int getRegurgitacion(){
        return this.Regurgitacion;
    }
    public int getConstipacion(){
        return this.Constipacion;
    }
    public int getBolo(){
        return this.Bolo;
    }
    public int getTiene(){
        return this.NoTiene;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * Método que recibe como parámetro la clave del paciente y
     * asigna en las variables de clase los datos del registro,
     * se asume que un paciente solo puede tener una entrada
     * en los antecedentes digestivos
     */
    public void getDatosDigestivo(int idpaciente){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_digestivo.id AS id, " +
                   "       diagnostico.v_digestivo.paciente AS paciente, " +
                   "       diagnostico.v_digestivo.disfagia AS disfagia, " +
                   "       diagnostico.v_digestivo.pirosis AS pirosis, " +
                   "       diagnostico.v_digestivo.regurgitacion AS regurgitacion, " +
                   "       diagnostico.v_digestivo.constipacion AS constipacion, " +
                   "       diagnostico.v_digestivo.bolo AS bolo, " +
                   "       diagnostico.v_digestivo.notiene AS notiene, " +
                   "       diagnostico.v_digestivo.usuario AS usuario, " +
                   "       diagnostico.v_digestivo.fecha AS fecha " +
                   "FROM diagnostico.v_digestivo " +
                   "WHERE diagnostico.v_digestivo.paciente = '" + idpaciente + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            if (Resultado.next()){

                // asignamos los valores
                this.Id = Resultado.getInt("id");
                this.Paciente = Resultado.getInt("paciente");
                this.Disfagia = Resultado.getInt("disfagia");
                this.Pirosis = Resultado.getInt("pirosis");
                this.Regurgitacion = Resultado.getInt("regurgitacion");
                this.Constipacion = Resultado.getInt("constipacion");
                this.Bolo = Resultado.getInt("bolo");
                this.NoTiene = Resultado.getInt("notiene");
                this.Usuario = Resultado.getString("usuario");
                this.Fecha = Resultado.getString("fecha");

            // si no encontró registros
            } else {

                // inicializamos las variables
                this.initDigestivo();

            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaDigestivo(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoDigestivo();
        } else {
            this.editaDigestivo();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoDigestivo(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.digestivo " +
                   "            (paciente, " +
                   "             disfagia, " +
                   "             pirosis, " +
                   "             regurgitacion, " +
                   "             constipacion, " +
                   "             bolo, " +
                   "             notiene, " +
                   "             usuario) " +
                   "            VALUES " +
                   "            (?, ?, ?, ?, ?, ?, ?, ?);";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Paciente);
            psInsertar.setInt(2, this.Disfagia);
            psInsertar.setInt(3, this.Pirosis);
            psInsertar.setInt(4, this.Regurgitacion);
            psInsertar.setInt(5, this.Constipacion);
            psInsertar.setInt(6, this.Bolo);
            psInsertar.setInt(7, this.NoTiene);
            psInsertar.setInt(8, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaDigestivo(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.digestivo SET " +
                   "       disfagia = ?, " +
                   "       pirosis = ?, " +
                   "       regurgitacion = ?, " +
                   "       constipacion = ?, " +
                   "       bolo = ?, " +
                   "       notiene = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.digestivo.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Disfagia);
            psInsertar.setInt(2, this.Pirosis);
            psInsertar.setInt(3, this.Regurgitacion);
            psInsertar.setInt(4, this.Constipacion);
            psInsertar.setInt(5, this.Bolo);
            psInsertar.setInt(6, this.NoTiene);
            psInsertar.setInt(7, this.IdUsuario);
            psInsertar.setInt(8, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}