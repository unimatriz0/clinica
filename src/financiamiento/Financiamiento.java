/*

    Nombre: Financiamiento
    Fecha: 20/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que contiene los métodos para operar sobre la 
                 tabla de fuentes de financiamiento

*/

// definición del paquete
package financiamiento;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 *
 */
public class Financiamiento {

	// definición de las variables de clase
	protected int IdFuente;                // clave del registro
	protected String Financiamiento;       // descripción de la fuente
	protected int Laboratorio;             // clave del laboratorio declarante
    protected String Usuario;              // nombre del usuario
    protected String FechaAlta;            // fecha de alta del registro
	
    // el puntero a la base de datos
    protected Conexion Enlace;

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase
     */
    public Financiamiento(){
    
        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initFinanciamiento();
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initFinanciamiento(){

        // inicializamos las variables
        this.IdFuente = 0;
        this.Financiamiento = "";
        this.Laboratorio = Seguridad.Laboratorio;
        this.Usuario = ""; 
        this.FechaAlta = "";
    	
    }
    // métodos de asignación de valores
    public void setIdFuente(int idfuente){
    	this.IdFuente = idfuente;
    }
    public void setFinanciamiento(String financiamiento){
    	this.Financiamiento = financiamiento;
    }

    // métodos de retorno de valores
    public int getIdFinanciamiento(){
        return this.IdFuente;
    }
    public String getFinanciamiento(){
        return this.Financiamiento;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getUsuario(){
        return this.Usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idfinanciamiento - entero con la clave del registro
     * @return Fuente - string con el tipo
     * Método que recibe como parámetro la clave de una fuente de 
     * financiamiento y retorna su nombre
     */
    public String getFinanciamiento(int idfinanciamiento){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        String Fuente = "";
        
        // componemos la consulta 
        Consulta = "SELECT diagnostico.financiamiento.fuente AS fuente " +
                   "FROM diagnostico.financiamiento " +
                   "WHERE diagnostico.financiamiento.id = '" + idfinanciamiento + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);
        

        try {
            
            // obtenemos el registro            
            Resultado.next();
            Fuente = Resultado.getString("fuente");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        // retornamos el nombre
        return Fuente;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param financiamiento - string con el nombre de la fuente
     * @return id de la fuente de financiamiento
     * Método que recibe como parámetro el nombre de la fuente 
     * de financiamiento y el laboratorio y retorna la 
     * clave del mismo
     */
    public int getIdFinanciamiento(String financiamiento){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int clave = 0;
        
        // componemos la consulta 
        Consulta = "SELECT diagnostico.financiamiento.id AS clave " +
                   "FROM diagnostico.financiamiento " +
                   "WHERE diagnostico.financiamiento.fuente = '" + financiamiento + "' AND " +
                   "      diagnostico.financiamiento.id_laboratorio = '" + this.Laboratorio + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);
        
        try {
            
            // obtenemos el registro            
            Resultado.next();
            clave = Resultado.getInt("clave");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        // retornamos el nombre
        return clave;
        
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de fuentes de financiamiento
     * Método que retorna un resultset con las fuentes de financiamiento
     */
    public ResultSet nominaFinanciamiento(){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta 
        Consulta = "SELECT diagnostico.financiamiento.id AS id, " + 
                   "       diagnostico.financiamiento.fuente AS fuente " +
                   "FROM diagnostico.financiamiento " +
                   "WHERE diagnostico.financiamiento.id_laboratorio = '" + this.Laboratorio + "' " +
                   "ORDER BY diagnostico.financiamiento.fuente; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos 
        return Resultado;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int clave del registro afectado
     * Método que ejecuta según corresponda la consulta de inserción
     * o edición y retorna la id del registro afectado
     */
    public int grabaFinanciamiento(){
    	
    	// si está insertando
    	if (this.IdFuente == 0){
    		this.nuevaFuente();
    	} else {
    		this.editaFuente();
    	}
    	
    	// retorna la id
    	return this.IdFuente;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un nuevo registro
     */
    protected void nuevaFuente(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "INSERT INTO diagnostico.financiamiento "
    			+ "        (fuente, "
    			+ "         id_laboratorio, "
    			+ "         id_usuario) "
    			+ "        VALUES "
    			+ "        (?,?,?)";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Financiamiento);
	        preparedStmt.setInt    (2, this.Laboratorio);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        
	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	        // obtenemos la id
	        this.IdFuente = this.Enlace.UltimoInsertado();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la edición del registro
     */
    protected void editaFuente(){
    	
    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "UPDATE diagnostico.financiamiento SET "
    			+ "        fuente = ?, "
    			+ "        id_laboratorio = ?, "
    			+ "        id_usuario = ? "
    			+ " WHERE diagnostico.financiamiento.id = ?; ";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Financiamiento);
	        preparedStmt.setInt    (2, this.Laboratorio);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        preparedStmt.setInt    (4, this.IdFuente);

	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param financiamiento - cadena con el nombre de la fuente
     * @return boolean
     * Método que verifica no se encuentre declarado una fuente de 
     * financiamiento para un laboratorio, el que recibe como parámetro, 
     * si se puede insertar retorna verdadero
     */
    public boolean validaFinanciamiento(String financiamiento){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;
    
        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.financiamiento.id) AS registros "
                 + "FROM diagnostico.financiamiento "
                 + "WHERE diagnostico.financiamiento.fuente = '" + financiamiento + "' AND "
                 + "      diagnostico.financiamiento.id_laboratorio = '" + this.Laboratorio + "'; ";
        
        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }

        // retornamos el estado
        return Correcto;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param int clave - clave del registro
     * @return boolean 
     * Método que verifica si puede eliminar una fuente de 
     * financiamiento
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.ingresos.id_financiamiento) AS registros " + 
                   "FROM diagnostico.ingresos " + 
                   "WHERE diagnostico.ingresos.id_financiamiento = '" + clave + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave del registro 
     * y ejecuta la consulta de eliminación
     */
    public void borraFinanciamiento(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.financiamiento " + 
                   "WHERE diagnostico.financiamiento.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}
