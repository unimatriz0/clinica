/*

    Nombre: FormLocalidades
    Fecha: 08/11/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario para el abm
                 de localidades

 */

// definición del paquete
package localidades;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import paises.Paises;
import provincias.Provincias;
import funciones.ComboClave;
import java.awt.Font;

public class FormLocalidades extends JDialog {

	// creamos el serial id y las variables
	private static final long serialVersionUID = 1L;
	private JTextField tId;
	private JTextField tCodigo;
	private JTextField tLocalidad;
	private JFormattedTextField tPoblacion;
	private JTable tNomina;
	private JComboBox<Object> cPaises;
	private JComboBox<Object> cJurisdiccion;
	private Localidades Ciudades;
	private Provincias Jurisdicciones;

	// creamos el díalogo
	public FormLocalidades(Frame parent, boolean modal) {

		// fijamos el formulario padre
		super(parent, modal);

		// fijamos las propiedades
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 551, 463);
		setLayout(null);

		// instanciamos las clases
		this.Ciudades = new Localidades();
		this.Jurisdicciones = new Provincias();

		// inicializamos el formulario
		this.initFormLocalidades();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario y sus componentes
	 */
	@SuppressWarnings({ "serial" })
	private void initFormLocalidades(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Seleccione el País y la Jurisdicción de la lista");
		lTitulo.setBounds(10, 10, 459, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// agregamos el combo del país
		JLabel lPais = new JLabel("País:");
		lPais.setBounds(10, 45, 41, 26);
		lPais.setFont(Fuente);
		getContentPane().add(lPais);
		this.cPaises = new JComboBox<>();
		this.cPaises.setBounds(50, 45, 144, 26);
		this.cPaises.setFont(Fuente);
		this.cPaises.setToolTipText("Seleccione el país");
		getContentPane().add(this.cPaises);

		// cargamos los elementos del combo
		this.cargaPaises();

		// agrega el combo de jurisdicción
		JLabel lJurisdiccion = new JLabel("Jurisdicción:");
		lJurisdiccion.setBounds(209, 45, 94, 26);
		lJurisdiccion.setFont(Fuente);
		getContentPane().add(lJurisdiccion);
		this.cJurisdiccion = new JComboBox<>();
		this.cJurisdiccion.setBounds(299, 45, 231, 26);
		this.cJurisdiccion.setFont(Fuente);
		this.cJurisdiccion.setToolTipText("Seleccione la jurisdicción");
		getContentPane().add(cJurisdiccion);

		// agregamos la id
		this.tId = new JTextField();
		this.tId.setBounds(10, 80, 63, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// carga el código indec
		this.tCodigo = new JTextField();
		this.tCodigo.setBounds(87, 80, 77, 26);
		this.tCodigo.setFont(Fuente);
		this.tCodigo.setToolTipText("Clave de georreferenciación");
		getContentPane().add(this.tCodigo);

		// carga el nombre de la localidad
		this.tLocalidad = new JTextField();
		this.tLocalidad.setBounds(176, 80, 219, 26);
		this.tLocalidad.setFont(Fuente);
		this.tLocalidad.setToolTipText("Nombre completo de la localidad");
		getContentPane().add(this.tLocalidad);

		// presenta la población
		this.tPoblacion = new JFormattedTextField();
		this.tPoblacion.setBounds(407, 80, 70, 26);
		this.tPoblacion.setFont(Fuente);
		this.tPoblacion.setToolTipText("Población de la localidad");
		getContentPane().add(this.tPoblacion);

		// presenta el botón grabar
		JButton btnGrabar = new JButton("");
		btnGrabar.setBounds(490, 80, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaLocalidad();
            }
        });
		getContentPane().add(btnGrabar);

		// define la tabla
		this.tNomina = new JTable();
		this.tNomina.setModel(new DefaultTableModel(
			new Object[][] {
				{null,
				 null,
				 null,
				 null,
				 null,
				 null},
			},
			new String[] {
				"ID",
				"COD.",
				"Localidad",
				"Pob.",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				Integer.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// establecemos el ancho de las columnas
		this.tNomina.getColumn("ID").setPreferredWidth(30);
		this.tNomina.getColumn("ID").setMaxWidth(30);
		this.tNomina.getColumn("COD.").setPreferredWidth(80);
		this.tNomina.getColumn("COD.").setMaxWidth(80);
		this.tNomina.getColumn("Pob.").setPreferredWidth(70);
		this.tNomina.getColumn("Pob.").setMaxWidth(70);
		this.tNomina.getColumn("Ed.").setPreferredWidth(35);
		this.tNomina.getColumn("Ed.").setMaxWidth(35);
		this.tNomina.getColumn("El.").setPreferredWidth(35);
		this.tNomina.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tNomina.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tNomina.setRowHeight(25);

		// definimos la fuente
		this.tNomina.setFont(Fuente);

		JScrollPane scrollLocalidades = new JScrollPane();
		scrollLocalidades.setBounds(10, 115, 518, 317);
		scrollLocalidades.setViewportView(this.tNomina);
		getContentPane().add(scrollLocalidades);

		// fijamos el evento change del combo de país
        this.cPaises.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cargaJurisdicciones();
            }
        });

		// fijamos el evento change del combo de provincia
        this.cJurisdiccion.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                grillaLocalidades();
            }
        });

		// agregamos el evento de la tabla
		this.tNomina.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tNominaMouseClicked(evt);
            }
	    });

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al cargar el formulario que carga la nómina
	 * de países
	 */
	private void cargaPaises(){

		// instanciamos la clase
		Paises Naciones = new Paises();

		// obtenemos la nómina
		ResultSet nomina = Naciones.nominaPaises();

        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cPaises.addItem(new ComboClave(0,""));

            // nos desplazamos al inicio del resultset
            nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // agregamos el registro
				this.cPaises.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("pais")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al cambiar el combo de países que actualiza
	 * la nómina de jurisdicciones
	 */
	private void cargaJurisdicciones(){

		// obtenemos la clave del pais estamos usando el
		// combo con clave así que tenemos que hacer un cast
		// para obtener el objeto seleccionado
		ComboClave item = (ComboClave) this.cPaises.getSelectedItem();

		// si hay un elemento seleccionado
		if (item != null) {

			// obtenemos la clave del país
			int idpais = item.getClave();

			// obtenemos la nómina de jurisdicciones
			ResultSet nomina = this.Jurisdicciones.listaProvincias(idpais);

			try{

				// por las dudas eliminamos todos los elementos previos
				this.cJurisdiccion.removeAllItems();

				// agregamos el primer elemento usamos la clase comboclave
				// para almacenar tanto la id como el texto
				this.cJurisdiccion.addItem(new ComboClave("0",""));

				// nos desplazamos al primer registro
				nomina.beforeFirst();

				// recorremos el vector
				while(nomina.next()){

					// agregamos el elemento
					this.cJurisdiccion.addItem(new ComboClave(nomina.getString("cod_prov"),nomina.getString("nombre_provincia")));

				}

			// si hubo un error
			} catch (SQLException ex){

				// presenta el mensaje
				System.out.println(ex.getMessage());

			}

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al cambiar el combo de jurisdicciones que
	 * actualiza la grilla de localdidades
	 */
	private void grillaLocalidades(){

		// obtenemos la clave de la jurisdicción estamos usando el
		// combo con clave así que tenemos que hacer un cast
		// para obtener el objeto seleccionado
		ComboClave item = (ComboClave) this.cJurisdiccion.getSelectedItem();

		// si hay un elemento seleccionado
		if (item != null) {

			// obtenemos la clave
			String clave = item.getCodigo();

			// sobrecargamos el renderer de la tabla
			this.tNomina.setDefaultRenderer(Object.class, new RendererTabla());

			// obtenemos el modelo de la tabla
			DefaultTableModel modeloTabla = (DefaultTableModel) this.tNomina.getModel();

			// hacemos la tabla se pueda ordenar
			this.tNomina.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

			// limpiamos la tabla
			modeloTabla.setRowCount(0);

			// definimos el objeto de las filas
			Object [] fila = new Object[6];

			// ahora obtenemos la nomina
			ResultSet nomina = this.Ciudades.listaLocalidades(clave);

			try{

				// nos desplazamos al primer registro
				nomina.beforeFirst();

				// recorremos el vector
				while(nomina.next()){

					// agregamos los elementos al array
					fila[0] = nomina.getInt("id");
					fila[1] = nomina.getString("codloc");
					fila[2] = nomina.getString("localidad");
					fila[3] = nomina.getInt("poblacion");
					fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
					fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

					// lo agregamos
					modeloTabla.addRow(fila);

				}

			// si hubo un error
			} catch (SQLException ex){

				// presenta el mensaje
				System.out.println(ex.getMessage());

			}

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void grabaLocalidad(){

		// declaración de variables
		String jurisdiccion = "";

		// obtenemos la clave de la jurisdicción
		ComboClave item = (ComboClave) this.cJurisdiccion.getSelectedItem();

		// si no hay ningún elemento seleccionado
		if (item == null){
			return;
		} else {
			jurisdiccion = item.getCodigo();
			this.Ciudades.setCodPcia(jurisdiccion);
		}

		// verificamos que halla ingresado el nombre
		if (this.tLocalidad.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre de la localidad", "Error", JOptionPane.ERROR_MESSAGE);
			this.tLocalidad.requestFocus();
			return;

		// si ingresó
		} else {

			// asigna en la variable
			this.Ciudades.setNomLoc(this.tLocalidad.getText());

		}

		// verificamos que halla ingresado el código indec
		if (this.tCodigo.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el código de georreferenciación", "Error", JOptionPane.ERROR_MESSAGE);
			this.tCodigo.requestFocus();
			return;

		// si ingresó
		} else {

			// asigna en la variable de clase
			this.Ciudades.setCodLoc(this.tCodigo.getText());

		}

		// verificamos que halla ingresado la población
		if (this.tPoblacion.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese la población de la localidad", "Error", JOptionPane.ERROR_MESSAGE);
			this.tPoblacion.requestFocus();
			return;

		// si ingresó
		} else {

			// asigna en la variable de clase
			this.Ciudades.setPoblacion(Integer.parseInt(this.tPoblacion.getText()));

		}

		// si está dando un alta
		if (this.tId.getText().isEmpty()){

			// verifica que no esté repetido
			if (!this.Ciudades.validaLocalidad(this.tLocalidad.getText(), jurisdiccion)){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Esa localidad ya está declarada", "Error", JOptionPane.ERROR_MESSAGE);
				this.tLocalidad.requestFocus();
				return;

			// sio está correcto
			} else {

				// asigna en la clase
				this.Ciudades.setIdLocalidad(0);

			}

		// si está editando
		} else {

			// asigna en la clase
			this.Ciudades.setIdLocalidad(Integer.parseInt((this.tId.getText())));

		}

		// grabamos el registro
		this.Ciudades.grabaLocalidad();

		// limpiamos el formulario
		this.limpiaFormulario();

		// recargamos la grilla
		this.grillaLocalidades();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de localidades
	 */
	protected void tNominaMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tNomina.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tNomina.rowAtPoint(evt.getPoint());
        int columna = tNomina.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tNomina.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id indec del registro
            String id = (String) modeloTabla.getValueAt(indice, 1);

            // según la columna pulsada
            if (columna == 4){
            	this.getDatosLocalidad(id);
            } else if (columna == 5){
            	this.borraLocalidad(id);
            }

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de grabar que limpia el formulario
	 * de datos
	 */
	private void limpiaFormulario(){

		// inijcializamos los campos
		this.tId.setText("");
		this.tCodigo.setText("");
		this.tLocalidad.setText("");
		this.tPoblacion.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param id - clave indec de la localidad
	 * Método llamado al pulsar sobre la grilla que carga los
	 * datos de la localidad en el formulario
	 */
	private void getDatosLocalidad(String id){

		// obtenemos los datos de la localidad
		this.Ciudades.getDatosLocalidad(id);

		// asignamos en el formulario
		this.tId.setText(Integer.toString((this.Ciudades.getIdLocalidad())));
		this.tCodigo.setText(this.Ciudades.getCodLoc());
		this.tLocalidad.setText(this.Ciudades.getNomLoc());
		this.tPoblacion.setText(Integer.toString(this.Ciudades.getPoblacion()));

		// fijamos el foco
		this.tLocalidad.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param id - clave indec de la localidad
	 * Método llamado al pulsar sobre la grilla, verifica que
	 * pueda eliminar la localidad y luego pide confirmación
	 */
	private void borraLocalidad(String id){

		// verifica si puede eliminar
		if (!this.Ciudades.puedeBorrar(id)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa localidad se encuentra asignada", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?", "Localidades", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Ciudades.borraLocalidad(id);

			// nos aseguramos de limpiar el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.grillaLocalidades();

		}

	}

}
