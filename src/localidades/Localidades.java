/*

    Nombre: localidades
    Fecha: 19/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que ofrece los métodos para el control de la tabla
                 de localidades

 */

// definición del paquete
package localidades;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class Localidades {

    // definición de variables
    protected Conexion Enlace;

    // definición de las variables de clase
    protected int IdLocalidad;             // clave del registro
    protected String CodPcia;              // código indec de la provincia
    protected String NomLoc;               // nombre de la localidad
    protected String CodLoc;               // código indec de la localidad
    protected int Poblacion;               // número de habitantes
    protected String Usuario;              // nombre del usuario
    protected String FechaAlta;            // fecha de alta del registro

    /**
     * Constructor de la clase
     */
    public Localidades(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initLocalidades();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initLocalidades(){

        // inicializamos las variables
        this.IdLocalidad = 0;
    	this.CodPcia = "";
    	this.NomLoc = "";
    	this.CodLoc = "";
    	this.Poblacion = 0;
    	this.Usuario = "";

    }

    // métodos de asignación de valores
    public void setIdLocalidad(int idpcia){
        this.IdLocalidad = idpcia;
    }
    public void setCodPcia(String codpcia){
    	this.CodPcia = codpcia;
    }
    public void setNomLoc(String nomloc){
    	this.NomLoc = nomloc;
    }
    public void setCodLoc(String codloc){
    	this.CodLoc = codloc;
    }
    public void setPoblacion(int poblacion){
    	this.Poblacion = poblacion;
    }

    // métodos de retorno de valores
    public int getIdLocalidad(){
        return this.IdLocalidad;
    }
    public String getCodPcia(){
        return this.CodPcia;
    }
    public String getNomLoc(){
        return this.NomLoc;
    }
    public String getCodLoc(){
        return this.CodLoc;
    }
    public int getPoblacion(){
    	return this.Poblacion;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getUsuario(){
        return this.Usuario;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param provincia clave de la jurisdicción o también el nombre de la jurisdicción
     * @return nominaLocalides vector con las localidades
     * Método que recibe como parámetro la id de una provincia y retorna
     * la nómina de localidades en esa provincia junto con su clave
     */
    public ResultSet listaLocalidades(String provincia){

        // inicializamos laa variables
        ResultSet nominaLocalidades;
        String Consulta;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_localidades.id AS id, " +
        		   "       diccionarios.v_localidades.codloc AS codloc, " +
                   "       diccionarios.v_localidades.localidad AS localidad, " +
        		   "       diccionarios.v_localidades.poblacion AS poblacion " +
                   " FROM diccionarios.v_localidades " +
                   " WHERE diccionarios.v_localidades.codpcia = '" + provincia + "'" +
                   " ORDER BY diccionarios.v_localidades.localidad; ";

        // obtenemos el vector
        nominaLocalidades = this.Enlace.Consultar(Consulta);

        // retorna el vector
        return nominaLocalidades;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param localidad cadena con el nombre de la localidad
     * @param provincia cadena con el nombre de la provincia
     * @return idlocalidad cadena con la clave INDEC de la localidad
     * Método que recibe como parámetros el nombre de la provincia y de
     * la localidad y retorna la clave del registro
     */
    public String getClaveLocalidad(String provincia, String localidad){

        // declaración de variables
        ResultSet Resultado;
        String Consulta;
        String idlocalidad = "";

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_localidades.codloc AS id_localidad "
                + " FROM diccionarios.v_localidades "
                + " WHERE diccionarios.v_localidades.provincia = '" + provincia + "' AND "
                + "       diccionarios.v_localidades.localidad = '" + localidad + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            idlocalidad = Resultado.getString("id_localidad");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos la clave
        return idlocalidad;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método público que ejecuta la consulta de edición
     * o inserción según corresponda
     */
    public void grabaLocalidad(){

    	// si está insertando
    	if (this.IdLocalidad == 0){
    		this.nuevaLocalidad();
    	} else {
    		this.editaLocalidad();
    	}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de inserción
     * a partir de las variables de clase
     */
    protected void nuevaLocalidad() {

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "INSERT INTO diccionarios.localidades "
    			+ "        (codpcia, "
    			+ "         nomloc, "
    			+ "         codloc, "
    			+ "         poblacion, "
    			+ "         usuario) "
    			+ "        VALUES "
    			+ "        (?,?,?,?,?)";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.CodPcia);
	        preparedStmt.setString (2, this.NomLoc);
	        preparedStmt.setString (3, this.CodLoc);
	        preparedStmt.setInt    (4, this.Poblacion);
	        preparedStmt.setInt    (5, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de edición
     * a partir de las variables de clase
     */
    protected void editaLocalidad(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "UPDATE diccionarios.localidades SET"
    			+ "        codpcia = ?, "
    			+ "        nomloc = ?, "
    			+ "        poblacion = ?, "
    			+ "        usuario = ? "
    			+ " WHERE diccionarios.localidades.codloc = ?; ";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.CodPcia);
	        preparedStmt.setString (2, this.NomLoc);
	        preparedStmt.setInt    (3, this.Poblacion);
	        preparedStmt.setInt    (4, Seguridad.Id);
	        preparedStmt.setString (5, this.CodLoc);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param localidad - nombre de la localidad
     * @param provincia - nombre de la provincia
     * @return Correcto - si el registro está o no repetido
     * Método público que recibe como parámetros el nombre
     * de una provincia y una localidad y retorna correcto
     * si puede insertar o falso si está repetido
     */
    public boolean validaLocalidad(String localidad, String provincia){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.v_localidades.localidad) AS registros "
                 + "FROM diccionarios.v_localidades "
                 + "WHERE diccionarios.v_localidades.localidad = '" + localidad + "' AND "
                 + "      diccionarios.v_localidades.provincia = '" + provincia + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave de la localidad
     * @return boolean - resultado de la operación
     * Método que verifica si una localidad puede ser
     * eliminada o tiene hijos
     */
    public boolean puedeBorrar(String clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        boolean Personas = false;
        boolean Laboratorios = false;
        boolean Instituciones = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros " +
                   "FROM diagnostico.personas " +
                   "WHERE diagnostico.personas.localidad_nacimiento = '" + clave + "' OR " +
                   "      diagnostico.personas.localidad_residencia = '" + clave + "' OR " +
                   "      diagnostico.personas.localidad_madre = '" + clave + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Personas = true;
            } else {
                Personas = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // ahora verificamos si la localidad no está asignada
        // a un laboratorio
        Consulta = "SELECT COUNT(cce.laboratorios.id) AS registros " +
                   "FROM cce.laboratorios " +
                   "WHERE cce.laboratorios.localidad = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Laboratorios = true;
            } else {
                Laboratorios = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // ahora verificamos si la localidad no está asignada
        // a una institución
        Consulta = "SELECT COUNT(diagnostico.centros_asistenciales.id) AS registros " +
                   "FROM diagnostico.centros_asistenciales " +
                   "WHERE diagnostico.centros_asistenciales.localidad = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Instituciones = true;
            } else {
                Instituciones = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // si los tres pueden borrar
        if (Personas && Laboratorios && Instituciones){
            Correcto = true;
        } else {
            Correcto = false;
        }

        // retornamos
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param localidad - parte del nombre de una localidad
     * @return resultset con las localidades encontradas
     * Método utilizado al seleccionar una localidad, recibe
     * como parámetro parte del nombre de una localidad y
     * retorna los registros coincidentes
     */
    public ResultSet buscaLocalidad(String localidad){

        // declaración de variables
        ResultSet Resultado;
        String Consulta;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_localidades.codloc AS id_localidad, "
                + "        diccionarios.v_localidades.localidad AS localidad, "
                + "        diccionarios.v_localidades.provincia AS provincia, "
                + "        diccionarios.v_localidades.pais AS pais"
                + " FROM diccionarios.v_localidades "
                + " WHERE diccionarios.v_localidades.localidad LIKE '%" + localidad + "%';";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el resultset
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - string clave de la localidad
     * Método que recibe como parámetro la clave de la localidad
     * y asigna los valores del registro en las variables de clase
     */
    public void getDatosLocalidad(String clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_localidades.id AS id, " +
                   "       diccionarios.v_localidades.localidad AS localidad, " +
        		   "       diccionarios.v_localidades.codloc AS codloc, " +
                   "       diccionarios.v_localidades.poblacion AS poblacion " +
        		   "FROM diccionarios.v_localidades " +
                   "WHERE diccionarios.v_localidades.codloc = '" + clave + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdLocalidad = Resultado.getInt("id");
            this.NomLoc = Resultado.getString("localidad");
            this.CodLoc = Resultado.getString("codloc");
            this.Poblacion = Resultado.getInt("poblacion");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - string con la clave de la localidad
     * Método que ejecuta la consulta de eliminación
     */
    public void borraLocalidad(String clave){

        // declaración de variables
        String Consulta;

        // compone y ejecuta la consulta
        Consulta = "DELETE FROM diccionarios.localidades " +
                   "WHERE diccionarios.localidades.codloc = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}
