/*

    Nombre: Sintomas
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 síntomas del paciente, en teoría puede haber mas de
                 una entrada si varían los síntomas

*/

 // declaración del paquete
package sintomas;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Sintomas {

    // declaración de variables de clase
    protected Conexion Enlace;             // puntero a la base de datos
    protected int Id;                      // clave del registro
    protected int Paciente;                // clave del paciente
    protected int Visita;                  // clave de la visita
    protected String Disnea;               // tipo de disnea
    protected String Palpitaciones;        // tipo de palpitaciones
    protected String Precordial;           // tipo de dolor
    protected int Conciencia;              // 0 no 1 si
    protected int Presincope;              // 0 no 1 si
    protected int Edema;                   // 0 no 1 si
    protected String Observaciones;        // observaciones del usuario
    protected int IdUsuario;               // clave del usuario
    protected String Usuario;              // nombre del usuario
    protected String Fecha;                // fecha de alta del registro

    // constructor de la clase
    public Sintomas(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.Disnea = "";
        this.Palpitaciones = "";
        this.Precordial = "";
        this.Conciencia = 0;
        this.Presincope = 0;
        this.Edema = 0;
        this.Observaciones = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setDisnea(String disnea){
        this.Disnea = disnea;
    }
    public void setPalpitaciones(String palpitaciones){
        this.Palpitaciones = palpitaciones;
    }
    public void setPrecordial(String precordial){
        this.Precordial = precordial;
    }
    public void setConciencia(int conciencia){
        this.Conciencia = conciencia;
    }
    public void setPresincope(int presincope){
        this.Presincope = presincope;
    }
    public void setEdema(int edema){
        this.Edema = edema;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getDisnea(){
        return this.Disnea;
    }
    public String getPalpitaciones(){
        return this.Palpitaciones;
    }
    public String getPrecordial(){
        return this.Precordial;
    }
    public int getConciencia(){
        return this.Conciencia;
    }
    public int getPresincope(){
        return this.Presincope;
    }
    public int getEdema(){
        return this.Edema;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * @return vector con los registros encontrados
     * Método que recibe como parámetro la clave de un paciente
     * y retorna un vector con los registros de síntomas de
     * ese paciente
     */
    public ResultSet nominaSintomas(int idpaciente){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_sintomas.id AS id, " +
                   "       diagnostico.v_sintomas.paciente AS paciente, " +
                   "       diagnostico.v_sintomas.idvisita AS visita, " +
                   "       diagnostico.v_sintomas.disnea AS disnea, " +
                   "       diagnostico.v_sintomas.palpitaciones AS palpitaciones, " +
                   "       diagnostico.v_sintomas.precordial AS precordial, " +
                   "       diagnostico.v_sintomas.conciencia AS conciencia, " +
                   "       diagnostico.v_sintomas.presincope AS presincope, " +
                   "       diagnostico.v_sintomas.edema AS edema, " +
                   "       diagnostico.v_sintomas.usuario AS usuario, " +
                   "       diagnostico.v_sintomas.fecha AS fecha " +
                   "FROM diagnostico.v_sintomas " +
                   "WHERE diagnostico.v_sintomas.paciente = '" + idpaciente + "'; ";

        // obtenemos el vector y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita clave de la visita
     * Método que recibe como parámetro la clave de un registro
     * y asigna los valores del mismo a las variables de clase
     */
    public void getDatosSintoma(int idvisita){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_sintomas.id AS id, " +
                   "       diagnostico.v_sintomas.paciente AS paciente, " +
                   "       diagnostico.v_sintomas.idvisita AS visita, " +
                   "       diagnostico.v_sintomas.disnea AS disnea, " +
                   "       diagnostico.v_sintomas.palpitaciones AS palpitaciones, " +
                   "       diagnostico.v_sintomas.precordial AS precordial, " +
                   "       diagnostico.v_sintomas.conciencia AS conciencia, " +
                   "       diagnostico.v_sintomas.presincope AS presincope, " +
                   "       diagnostico.v_sintomas.edema AS edema, " +
                   "       diagnostico.v_sintomas.observaciones AS observaciones, " +
                   "       diagnostico.v_sintomas.usuario AS usuario, " +
                   "       diagnostico.v_sintomas.fecha AS fecha " +
                   "FROM diagnostico.v_sintomas " +
                   "WHERE diagnostico.v_sintomas.idvisita = '" + idvisita + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            // en las variables de clase
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.Visita = Resultado.getInt("visita");
            this.Disnea = Resultado.getString("disnea");
            this.Palpitaciones = Resultado.getString("palpitaciones");
            this.Precordial = Resultado.getString("precordial");
            this.Conciencia = Resultado.getInt("conciencia");
            this.Presincope = Resultado.getInt("presincope");
            this.Edema = Resultado.getInt("edema");
            this.Observaciones = Resultado.getString("observaciones");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return clave del registro
     * Método que según el caso ejecuta la consulta de edición
     * o inserción, retorna el valor del registro afectado
     */
    public int grabaSintoma(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoSintoma();
        } else {
            this.editaSintoma();
        }

        // retornamos la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inerción
     */
    protected void nuevoSintoma(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.sintomas " +
                   "       (paciente, " +
                   "        visita, " +
                   "        disnea, " +
                   "        palpitaciones, " +
                   "        precordial, " +
                   "        conciencia, " +
                   "        presincope, " +
                   "        edema, " +
                   "        observaciones, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Disnea);
            psInsertar.setString(4, this.Palpitaciones);
            psInsertar.setString(5, this.Precordial);
            psInsertar.setInt(6,    this.Conciencia);
            psInsertar.setInt(7,    this.Presincope);
            psInsertar.setInt(8,    this.Edema);
            psInsertar.setString(9, this.Observaciones);
            psInsertar.setInt(10,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaSintoma(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.sintomas SET " +
                   "       disnea = ?, " +
                   "       palpitaciones = ?, " +
                   "       precordial = ?, " +
                   "       conciencia = ?, " +
                   "       presincope = ?, " +
                   "       edema = ?, " +
                   "       observaciones = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.sintomas.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Disnea);
            psInsertar.setString(2, this.Palpitaciones);
            psInsertar.setString(3, this.Precordial);
            psInsertar.setInt(4,    this.Conciencia);
            psInsertar.setInt(5,    this.Presincope);
            psInsertar.setInt(6,    this.Edema);
            psInsertar.setString(7, this.Observaciones);
            psInsertar.setInt(8,    this.IdUsuario);
            psInsertar.setInt(9,    this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idsintoma entero con la clave del registro
     * Método que recibe como parámetro la clave de un registro
     * y ejecuta la consulta de eliminación
     */
    public void borraSintoma(int idsintoma){

        // declaramos las variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.sintomas " +
                   "WHERE diagnostico.sintomas.id = '" + idsintoma + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}