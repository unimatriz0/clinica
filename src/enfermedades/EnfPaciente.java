/*

    Nombre: EnfPaciente
	Fecha: 20/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario de las enfermedades
	             del paciente

*/

// declaración del paquete
package enfermedades;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import com.toedter.calendar.JDateChooser;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import enfermedades.Enfermedades;
import funciones.ComboClave;
import funciones.RendererTabla;
import funciones.Utilidades;
import seguridad.Seguridad;
import java.awt.Font;
import enfermedades.OtrasEnfermedades;

// declaración de la clase
public class EnfPaciente extends JDialog {

	// declaramos el serial id
	private static final long serialVersionUID = -8148954782751579405L;

	// declaramos las variables de clase
	private JTextField tId;                     // clave del registro
	private JComboBox<Object> cEnfermedad;      // combo de las enfermedades
	private JDateChooser dFecha;                // fecha de la enfermedad
	private JTextField tUsuario;                // nombre del usuario
	private JTable tEnfermedades;               // tabla con las enfermedades
	public int Protocolo;                       // protocolo del paciente
	private OtrasEnfermedades Sufridas;         // clase de las enfermedades del paciente
	private Utilidades Herramientas;            // herramientas para las fechas

	// constructor del paqueta
	@SuppressWarnings("serial")
	public EnfPaciente(Frame parent, boolean modal) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// inicializamos las variables
		this.Protocolo = 0;
		this.Sufridas = new OtrasEnfermedades();
		this.Herramientas = new Utilidades();

		// fijamos las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Enfermedades del Paciente");
		this.setBounds(150, 150, 590, 380);
		this.setLayout(null);

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// define el título
		JLabel lTitulo = new JLabel("Enfermedades del Paciente");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(10, 10, 530, 26);
		this.add(lTitulo);

		// presenta la id
		this.tId = new JTextField();
		this.tId.setBounds(10, 40, 75, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Protocolo del paciente");
		this.tId.setEditable(false);
		this.add(this.tId);

		// presenta el combo con las enfermedades
		this.cEnfermedad = new JComboBox<>();
		this.cEnfermedad.setBounds(95, 40, 220, 26);
		this.cEnfermedad.setFont(Fuente);
		this.cEnfermedad.setToolTipText("Seleccione la enfermedad de la lista");
		this.add(this.cEnfermedad);

		// pide la fecha de la enfermedad
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(330, 40, 110, 26);
		this.dFecha.setToolTipText("Indique la fecha de la enfermedad");
		this.dFecha.setFont(Fuente);
		this.add(this.dFecha);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(440, 40, 90, 26);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.tUsuario.setFont(Fuente);
		this.add(this.tUsuario);

		// agregamos el usuario actual
		this.tUsuario.setText(Seguridad.Usuario);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(550, 40, 26, 26);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setToolTipText("Pulse para agregar la enfermedad");
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaEnfermedad();
            }
        });
		this.add(btnGrabar);

		// agrega el scroll
		JScrollPane scrollEnfermedades = new JScrollPane();
		scrollEnfermedades.setBounds(10, 70, 565, 270);
		this.add(scrollEnfermedades);

		// agrega la tabla
		this.tEnfermedades = new JTable();
		this.tEnfermedades.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
			},
			new String[] {
				"ID",
				"Enfermedad",
				"Fecha",
				"Usuario",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

        // establecemos el ancho de las columnas
        this.tEnfermedades.getColumn("ID").setPreferredWidth(30);
        this.tEnfermedades.getColumn("ID").setMaxWidth(30);
        this.tEnfermedades.getColumn("Fecha").setPreferredWidth(100);
        this.tEnfermedades.getColumn("Fecha").setMaxWidth(100);
        this.tEnfermedades.getColumn("Usuario").setPreferredWidth(100);
        this.tEnfermedades.getColumn("Usuario").setMaxWidth(100);
		this.tEnfermedades.getColumn("Ed.").setPreferredWidth(35);
        this.tEnfermedades.getColumn("Ed.").setMaxWidth(35);
        this.tEnfermedades.getColumn("El.").setPreferredWidth(35);
        this.tEnfermedades.getColumn("El.").setMaxWidth(35);

        // establecemos el tooltip
        this.tEnfermedades.setToolTipText("Pulse para editar / borrar");

        // fijamos el alto de las filas
        this.tEnfermedades.setRowHeight(25);

		// fijamos la fuente
		this.tEnfermedades.setFont(Fuente);

		// agregamos la tabla al scroll
		scrollEnfermedades.setViewportView(this.tEnfermedades);

        // fijamos el evento click
        this.tEnfermedades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tEnfermedadesMouseClicked(evt);
            }
        });

		// cargamos las enfermedades declaradas
		this.cargaEnfermedades();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga el combo con las enfermedades declaradas
	 */
	protected void cargaEnfermedades(){

		// instanciamos la clase y obtenemos el vector
		Enfermedades Dolencias = new Enfermedades();
		ResultSet nomina = Dolencias.nominaEnfermedades();

		// agregamos el primer elemento
		this.cEnfermedad.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cEnfermedad.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("enfermedad")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método pùblico que carga la grilla con las enfermedades
	 * del paciente
	 */
	public void grillaEnfermedades(){

		// obtenemos la nómina
		ResultSet Nomina = this.Sufridas.nominaEnfermedades(this.Protocolo);

        // sobrecargamos el renderer de la tabla
        this.tEnfermedades.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tEnfermedades.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tEnfermedades.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[6];

        try {

			// nos aseguramos de estar en primer registro
			Nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (Nomina.next()){

                // fijamos los valores de la fila
                fila[0] = Nomina.getInt("id");
                fila[1] = Nomina.getString("enfermedad");
                fila[2] = Nomina.getString("fecha");
				fila[3] = Nomina.getString("usuario");
                fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método privado que verifica el formulario antes de
	 * enviarlo al servidor
	 */
	private void validaEnfermedad(){

		// verificamos que halla seleccionado una enfermedad
		ComboClave item = (ComboClave) this.cEnfermedad.getSelectedItem();
		if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione de la lista la enfermedad",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cEnfermedad.requestFocus();
			return;

		}

		// verificamos la fecha
		// si no indicó la fecha de inicio del tratamiento
		if (this.Herramientas.fechaJDate(this.dFecha)  == null){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la fecha de inicio del tratamiento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.dFecha.requestFocus();
			return;

		}

		// grabamos el registro
		this.grabaEnfermedad();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método privado que ejecuta la consulta en el servidor
	 */
	private void grabaEnfermedad(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Sufridas.setId(0);
		} else {
			this.Sufridas.setId(Integer.parseInt(this.tId.getText()));
		}

		// asignamos el protocolo
		this.Sufridas.setProtocolo(this.Protocolo);

		// obtenemos la clave de la enfermedad
		ComboClave item = (ComboClave) this.cEnfermedad.getSelectedItem();
		this.Sufridas.setIdEnfermedad(item.getClave());

		// asignamos la fecha
		this.Sufridas.setFecha(this.Herramientas.fechaJDate(this.dFecha));

		// grabamos el registro
		this.Sufridas.grabaEnfermedad();

		// recarga la grilla
		this.grillaEnfermedades();
		
		// limpiamos el formulario
		this.limpiaFormulario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero clave del registro
	 * Método que recibe como parámetro la clave del registro
	 * y carga el mismo en el formulario de datos
	 */
	private void verEnfermedad(int clave){

		// obtenemos el registro
		this.Sufridas.getDatosEnfermedad(clave);

		// lo presentamos en el formulario
		this.tId.setText(Integer.toString(this.Sufridas.getId()));
		ComboClave item = new ComboClave(this.Sufridas.getIdEnfermedad(), this.Sufridas.getEnfermedad());
		this.cEnfermedad.setSelectedItem(item);
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Sufridas.getFecha()));
		this.tUsuario.setText(this.Sufridas.getUsuario());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de grabar o eliminar que limpia
	 * el formulario de datos
	 */
	private void limpiaFormulario(){

		// inicializamos los campos
		this.tId.setText("");
		ComboClave item = new ComboClave(0, "");
		this.cEnfermedad.setSelectedItem(item);
		this.dFecha.setDate(null);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método público que recibe como parámetro la clave
	 * del registro y luego de pedir confirmación ejecuta
	 * la consulta de eliminación
	 */
	private void borraEnfermedad(int clave){

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
		                            "Está seguro que desea eliminar el registro?",
									"Enfermedades del Paciente",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Sufridas.borraEnfermedad(clave);

			// limpiamos el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.grillaEnfermedades();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de enfermedades
	 */
	private void tEnfermedadesMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tEnfermedades.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tEnfermedades.rowAtPoint(evt.getPoint());
        int columna = this.tEnfermedades.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tEnfermedades.convertRowIndexToModel (fila);

		// obtenemos el protocolo
		int clave = (Integer) modeloTabla.getValueAt(indice, 0);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // si pulsó en editar
            if (columna == 4){

				// editamos el registro
				this.verEnfermedad(clave);

			// si pulsó en eliminar
			} else if (columna == 5){

				// eliminamos el registro
				this.borraEnfermedad(clave);

			}

		}

	}

}
