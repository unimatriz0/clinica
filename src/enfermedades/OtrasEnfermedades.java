/*

    Nombre: OtrasEnfermedades
    Fecha: 22/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa los métodos para el abm
                 de la tabla enfermedades del paciente

 */

// declaración del paquete
package enfermedades;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class OtrasEnfermedades {

    // declaración de variables
    private Conexion Enlace;             // puntero a la base de datos
    private int Id;                      // clave del registro
    private int Protocolo;               // protocolo del paciente
    private int IdEnfermedad;            // clave de la enfermedad
    private String Enfermedad;           // nombre de la enfermedad
    private int IdUsuario;               // clave del usuario
    private String Usuario;              // nombre del usuario
    private String Fecha;                // fecha de la enfermedad

    // constructor de la clase
    public OtrasEnfermedades(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Protocolo = 0;
        this.IdEnfermedad = 0;
        this.Enfermedad = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setProtocolo(int protocolo){
        this.Protocolo = protocolo;
    }
    public void setIdEnfermedad(int idenfermedad){
        this.IdEnfermedad = idenfermedad;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getProtocolo(){
        return this.Protocolo;
    }
    public int getIdEnfermedad(){
        return this.IdEnfermedad;
    }
    public String getEnfermedad(){
        return this.Enfermedad;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idprotocolo - clave del protocolo del paciente
     * @return resultset con la nómina de enfermedades
     * Método que recibe como parámetro la clave de un paciente
     * (el protocolo) y retorna un vector con las enfermedades
     * declaradas
     */
    public ResultSet nominaEnfermedades(int idprotocolo){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_otras_enfermedades.id_registro AS id, " +
                   "       diagnostico.v_otras_enfermedades.protocolo AS protocolo, " +
                   "       diagnostico.v_otras_enfermedades.enfermedad AS enfermedad, " +
                   "       diagnostico.v_otras_enfermedades.usuario AS usuario, " +
                   "       diagnostico.v_otras_enfermedades.fecha AS fecha " +
                   "FROM diagnostico.v_otras_enfermedades " +
                   "WHERE diagnostico.v_otras_enfermedades.protocolo = '" + idprotocolo + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_otras_enfermedades.fecha_alta, '%d/%m/%Y') DESC; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param id - clave del registro
     * Método que recibe como parámetro la clave del registro y
     * asigna en las variables de clase los valores del mismo
     */
    public void getDatosEnfermedad(int id){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_otras_enfermedades.id_registro AS id, " +
                   "       diagnostico.v_otras_enfermedades.protocolo AS protocolo, " +
                   "       diagnostico.v_otras_enfermedades.id_enfermedad AS idenfermedad, " +
                   "       diagnostico.v_otras_enfermedades.enfermedad AS enfermedad, " +
                   "       diagnostico.v_otras_enfermedades.usuario AS usuario, " +
                   "       diagnostico.v_otras_enfermedades.fecha AS fecha " +
                   "FROM diagnostico.v_otras_enfermedades " +
                   "WHERE diagnostico.v_otras_enfermedades.id_registro = '" + id + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Protocolo = Resultado.getInt("protocolo");
            this.IdEnfermedad = Resultado.getInt("idenfermedad");
            this.Enfermedad = Resultado.getString("enfermedad");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id del registro afectado
     * Método que ejecuta la consulta de actualización o de
     * inserción según corresponda y retorna la clave del
     * registro afectado
     */
    public int grabaEnfermedad(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaEnfermedad();
        } else {
            this.editaEnfermedad();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaEnfermedad(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.otras_enfermedades " +
                   "       (id_protocolo, " +
                   "        id_enfermedad, " +
                   "        id_usuario, " +
                   "        fecha) " +
                   "       VALUES " +
                   "       (?, ?, ?, STR_TO_DATE(?, '%d/%m/%Y')); ";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.Protocolo);
            psInsertar.setInt(2, this.IdEnfermedad);
            psInsertar.setInt(3, this.IdUsuario);
            psInsertar.setString(4, this.Fecha);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaEnfermedad(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.otras_enfermedades SET " +
                   "       id_enfermedad = ?, " +
                   "       id_usuario = ?, " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y') " +
                   "WHERE id = ?; ";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdEnfermedad);
            psInsertar.setInt(2, this.IdUsuario);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4, this.Id);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param id - clave del registro
     * Método que ejcuta la consulta de eliminación
     */
    public void borraEnfermedad(int id){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.otras_enfermedades " +
                   "WHERE diagnostico.otras_enfermedades.id = '" + id + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }
}
