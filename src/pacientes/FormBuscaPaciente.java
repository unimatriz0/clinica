/*

    Nombre: FormBuscaPaciente
    Fecha: 12/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario con la grilla de resultados
	             en la búsqueda de pacientes

 */

// definición del paquete
package pacientes;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;

// declaración de la clase
public class FormBuscaPaciente extends JDialog {

	// agrega el serial id
	private static final long serialVersionUID = 5963099378228173183L;

	// definimos las variables de clase
	private JTable tPacientes;
	private FormPacientes Padre;

	// constructor de la clase
	@SuppressWarnings({ "serial" })
	public FormBuscaPaciente(Frame parent, boolean modal, FormPacientes padre) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// seteamos el padre
		this.Padre = padre;

		// definimos las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Pacientes Encontrados");
		this.setBounds(150, 100, 945, 435);
		this.setLayout(null);

		// definimos el scroll
		JScrollPane scrollPacientes = new JScrollPane();
		scrollPacientes.setBounds(10, 10, 915, 385);
		this.add(scrollPacientes);

		// definimos la tabla de resultados
		this.tPacientes = new JTable();
		this.tPacientes.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"Protocolo",
				"Laboratorio",
				"Nombre",
				"Documento",
				"Edad",
				"Residencia",
				"Motivo",
				"Trat.",
				"Alta",
				"Sel."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				String.class,
				Integer.class,
				String.class,
				String.class,
				String.class,
				String.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// fijamos el tooltip
		this.tPacientes.setToolTipText("Pulse para seleccionar la localidad");

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);
		this.tPacientes.setFont(Fuente);

		// fijamos el ancho de las columnas
		this.tPacientes.getColumn("Protocolo").setMaxWidth(70);
		this.tPacientes.getColumn("Edad").setMaxWidth(40);
		this.tPacientes.getColumn("Trat.").setMaxWidth(35);
		this.tPacientes.getColumn("Alta").setMaxWidth(85);
		this.tPacientes.getColumn("Sel.").setMaxWidth(30);

		// agregamos la tabla al scroll
		scrollPacientes.setViewportView(this.tPacientes);

        // fijamos el evento click
        this.tPacientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tPacientesMouseClicked(evt);
            }
        });

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param nomina - vector con los registros
	 * Método que recibe como parámetro un vector y agrega los
	 * registros en la tabla
	 */
	public void cargaPacientes (ResultSet nomina){

        // sobrecargamos el renderer de la tabla
        this.tPacientes.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tPacientes.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tPacientes.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[10];

        try {

			// nos aseguramos de estar en primer registro
			nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // fijamos los valores de la fila
                fila[0] = nomina.getInt("protocolo");
                fila[1] = nomina.getString("laboratorio");
                fila[2] = nomina.getString("nombre");
				fila[3] = nomina.getString("documento");
				fila[4] = nomina.getInt("edad");
				fila[5] = nomina.getString("localidad");
				fila[6] = nomina.getString("motivo");
				fila[7] = nomina.getString("tratamiento");
				fila[8] = nomina.getString("alta");
                fila[9] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de pacientes
	 */
	private void tPacientesMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tPacientes.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tPacientes.rowAtPoint(evt.getPoint());
        int columna = this.tPacientes.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tPacientes.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // si pulsó en seleccionar
            if (columna == 9){

				// obtenemos el protocolo
				int protocolo = (Integer) modeloTabla.getValueAt(indice, 0);

				// cargamos el registro
				this.Padre.verPaciente(protocolo);

                // cerramos el formulario
				this.dispose();

			}

		}

	}

}
