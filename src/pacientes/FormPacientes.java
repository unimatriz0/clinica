/*

    Nombre: FormPacientes
    Fecha: 17/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa el formulario de datos personales
                 de los pacientes

 */

// definimos el paquete
package pacientes;

// importamos las librerías
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import com.toedter.calendar.JDateChooser;
import antecedentes.FormAntecedentes;
import javax.swing.JSpinner;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import documentos.Documentos;
import enfermedades.EnfPaciente;
import estados.Estados;
import familiares.FormFamiliares;
import celulares.Celulares;
import motivos.Motivos;
import transfusiones.FormTransfusiones;
import transplantes.FormTransplantes;
import tratamiento.FormTratamiento;
import derivacion.Derivacion;
import funciones.ComboClave;
import funciones.Mensaje;
import contenedor.FormContenedor;
import java.awt.Font;
import java.awt.event.KeyEvent;
import pacientes.Pacientes;
import seguridad.Seguridad;
import funciones.Utilidades;
import localidades.Localidades;

// definición de la clase
public class FormPacientes extends javax.swing.JPanel {

    // define el serial id
    private static final long serialVersionUID = 1L;

    // definimos las variables de clase
    private JLabel lLaboratorio;                   // laboratorio propietario del registro
    private JTextField tProtocolo;                 // clave del registro
    private JTextField tHistoria;                  // número de historia clínica
    private JTextField tApellido;                  // apellido del paciente
    private JTextField tNombre;                    // nombre del paciente
    private JFormattedTextField tDocumento;        // número de documento
    private JComboBox<Object> cDocumento;          // tipo de documento
    private JDateChooser dNacimiento;              // fecha de nacimiento
    private JSpinner sEdad;                        // edad al momento del alta
    private JComboBox<Object> cSexo;               // clave del sexo
    private JComboBox<Object> cEstado;             // clave del estado civil
    private JSpinner sHijos;                       // número de hijos
    private JTextField tTelefono;                  // número de teléfono
    private JComboBox<Object> cCompania;           // clave de la compañía
    private JTextField tCelular;                   // número de celular
    private JTextField tDireccion;                 // dirección postal
    private JTextField tMail;                      // dirección de correo
    public JTextField tNacionalidad;               // país de nacimiento
    public JTextField tProvincia;                  // provincia de nacimiento
    public JTextField tLocalidad;                  // localidad de nacimiento
    public JTextField tResidencia;                 // país de residencia
    public JTextField tProvResidencia;             // provincia de residencia
    public JTextField tLocResidencia;              // ciudad de residencia
    public JTextField tNacMadre;                   // nacionalidad de la madre
    public JTextField tProvMadre;                  // provincia de nac. de la madre
    public JTextField tLocMadre;                   // localidad de nac. de la madre
    private JComboBox<Object> cMadrePositiva;      // si la madre era positiva
    private JTextField tOcupacion;                 // ocupación del paciente
    private JTextField tObraSocial;                // obra social del paciente
    private JComboBox<Object> cMotivo;             // clave del motivo de consulta
    private JComboBox<Object> cDerivacion;         // clave de la derivación
    private JComboBox<Object> cTratamiento;        // si realizó tratamiento
    private JTextField tUsuario;                   // nombre del usuario
    private JTextField tFecha;                     // fecha de alta del registro
    private JTextArea tComentarios;                // comentarios del usuario
    private FormContenedor Contenedor;             // frame principal

    // las variables públicas de las claves
    public String IdLocNacimiento;                 // clave de la localidad de nacimiento
    public String IdLocResidencia;                 // clave de la localidad de residencia
    public String IdLocMadre;                      // clave de la localidad de la madre

    // los objetos de la base de datos
    protected Pacientes Personas;                  // clase de la base de datos
    protected Utilidades Herramientas;             // funciones para el manejo de fechas
    protected Localidades Ciudades;                // localidades censales

    // el panel de los antecedentes
    public FormAntecedentes Antecedentes;       // acceso al formulario de antecedentes

    // instanciamos las propiedades
    public FormPacientes(FormContenedor contenedor) {

        // fijamos el contenedor
        this.Contenedor = contenedor;

        // inicializamos las variables
        this.IdLocMadre = "";
        this.IdLocNacimiento = "";
        this.IdLocResidencia = "";
        this.Personas = new Pacientes();
        this.Herramientas = new Utilidades();
        this.Ciudades = new Localidades();

        // fijamos el layout
        this.setLayout(null);

        // iniciamos el formulario
        initComponents();

    }

    // método que instancia el formulario
    private void initComponents() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta el título
        JLabel lTitulo = new JLabel("<html><b>Datos Personales del Paciente</b></html");
        lTitulo.setBounds(10, 10, 300, 26);
        lTitulo.setFont(Fuente);
        this.add(lTitulo);

        // presenta el laboratorio (que luego actualiza)
        this.lLaboratorio = new JLabel("");
        lLaboratorio.setBounds(300, 10, 580, 26);
        lLaboratorio.setFont(Fuente);
        this.add(lLaboratorio);

        // presenta el número de protocolo
        JLabel lProtocolo = new JLabel("<html><b>Protocolo:</b></html>");
        lProtocolo.setBounds(10, 45, 100, 26);
        lProtocolo.setFont(Fuente);
        this.add(lProtocolo);
        this.tProtocolo = new JTextField();
        this.tProtocolo.setBounds(85, 45, 90, 26);
        this.tProtocolo.setFont(Fuente);
        this.tProtocolo.setEditable(false);
        this.tProtocolo.setToolTipText("Protocolo del paciente");
        this.add(this.tProtocolo);

        // presenta la historia clínica
        JLabel lHistoria = new JLabel("<html><b>Historia Clínica: </b></html>");
        lHistoria.setBounds(180, 45, 150, 26);
        lHistoria.setFont(Fuente);
        this.add(lHistoria);
        this.tHistoria = new JTextField();
        this.tHistoria.setBounds(290, 45, 90, 26);
        this.tHistoria.setFont(Fuente);
        this.tHistoria.setEditable(false);
        this.tHistoria.setToolTipText("Historia Clínica del paciente");
        this.add(this.tHistoria);

        // pide el apellido
        JLabel lApellido = new JLabel ("<html><b>Apellido:</b></html>");
        lApellido.setBounds(385, 45, 100, 26);
        lApellido.setFont(Fuente);
        this.add(lApellido);
        this.tApellido = new JTextField();
        this.tApellido.setBounds(445, 45, 190, 26);
        this.tApellido.setFont(Fuente);
        this.tApellido.setToolTipText("Ingrese el apellido del paciente");
        this.tApellido.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(tApellido);

        // pide el nombre
        JLabel lNombre = new JLabel("<html><b>Nombre:</b></html>");
        lNombre.setBounds(640, 45, 100, 26);
        lNombre.setFont(Fuente);
        this.add(lNombre);
        this.tNombre = new JTextField();
        this.tNombre.setBounds(700, 45, 220, 26);
        this.tNombre.setFont(Fuente);
        this.tNombre.setToolTipText("Ingrese el nombre del paciente");
        this.tNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(this.tNombre);

        // pide el documento
        JLabel lDocumento = new JLabel("<html><b>Documento:</b></html>");
        lDocumento.setBounds(10, 80, 100, 26);
        lDocumento.setFont(Fuente);
        this.add(lDocumento);

        // presenta el combo con el tipo de documento
        this.cDocumento = new JComboBox<>();
        this.cDocumento.setBounds(95, 80, 70, 26);
        this.cDocumento.setFont(Fuente);
        this.cDocumento.setToolTipText("Seleccione el tipo de documento");
        this.cDocumento.setEditable(true);
        this.add(this.cDocumento);

        // carga el combo con el tipo de documento
        this.elementosDocumento();

        // pide el número de documento
        this.tDocumento = new JFormattedTextField();
        this.tDocumento.setBounds(170, 80, 100, 26);
        this.tDocumento.setFont(Fuente);
        this.tDocumento.setToolTipText("Número de documento sin puntos");
        this.tDocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(this.tDocumento);

        // pide la fecha de nacimiento
        JLabel lNacimiento = new JLabel("<html><b>Fecha Nacimiento:</b></html>");
        lNacimiento.setBounds(280, 80, 150, 26);
        lNacimiento.setFont(Fuente);
        this.add(lNacimiento);
        this.dNacimiento = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dNacimiento.setBounds(410, 80, 120, 26);
        this.dNacimiento.setFont(Fuente);
        this.dNacimiento.setToolTipText("Indique la fecha de nacimiento");
        this.add(this.dNacimiento);

        // pide la edad
        JLabel lEdad = new JLabel("<html><b>Edad:</b></html>");
        lEdad.setBounds(540, 80, 100, 26);
        lEdad.setFont(Fuente);
        this.add(lEdad);
        this.sEdad = new JSpinner();
        this.sEdad.setBounds(580, 80, 60, 26);
        this.sEdad.setFont(Fuente);
        this.sEdad.setToolTipText("Indique la edad del paciente");
        this.add(this.sEdad);

        // pide el sexo
        JLabel lSexo = new JLabel("<html><b>Sexo:</b></html>");
        lSexo.setBounds(650, 80, 100, 26);
        lSexo.setFont(Fuente);
        this.add(lSexo);
        this.cSexo = new JComboBox<>();
        this.cSexo.setBounds(690, 80, 120, 26);
        this.cSexo.setFont(Fuente);
        this.cSexo.setEditable(true);
        this.cSexo.setToolTipText("Seleccione el sexo del paciente");
        this.add(this.cSexo);

        // carga el combo con los sexos
        this.elementosSexo();

        // pide el estado civil
        JLabel lEstado = new JLabel("<html><b>Estado Civil:</b></html>");
        lEstado.setBounds(10, 115, 100, 26);
        lEstado.setFont(Fuente);
        this.add(lEstado);
        this.cEstado = new JComboBox<>();
        this.cEstado.setBounds(100, 115, 100, 26);
        this.cEstado.setFont(Fuente);
        this.cEstado.setEditable(true);
        this.cEstado.setToolTipText("Seleccione el Estado Civil del paciente");
        this.add(this.cEstado);

        // carga los estados civiles
        this.elementosEstado();

        // pide si tiene hijos
        JLabel lHijos = new JLabel("<html><b>Hijos: </b></html>");
        lHijos.setBounds(210, 115, 100, 26);
        lHijos.setFont(Fuente);
        this.add(lHijos);
        this.sHijos = new JSpinner();
        this.sHijos.setBounds(255, 115, 40, 26);
        this.sHijos.setFont(Fuente);
        this.sHijos.setToolTipText("Indique el número de hijos");
        this.add(this.sHijos);

        // pide el teléfono
        JLabel lTelefono = new JLabel("<html><b>Teléfono: </b></html>");
        lTelefono.setBounds(310, 115, 100, 26);
        lTelefono.setFont(Fuente);
        this.add(lTelefono);
        this.tTelefono = new JTextField();
        this.tTelefono.setBounds(380, 115, 100, 26);
        this.tTelefono.setFont(Fuente);
        this.tTelefono.setToolTipText("Indique el telefono con prefijo de área");
        this.tTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(this.tTelefono);

        // pide el celular
        JLabel lCelular = new JLabel("<html><b>Celular:</b></html>");
        lCelular.setBounds(490, 115, 100, 26);
        lCelular.setFont(Fuente);
        this.add(lCelular);

        // el combo con la compañía de celular
        this.cCompania = new JComboBox<>();
        this.cCompania.setBounds(550, 115, 120, 26);
        this.cCompania.setFont(Fuente);
        this.cCompania.setEditable(true);
        this.cCompania.setToolTipText("Seleccione la compañía de celular");
        this.add(this.cCompania);

        // agrega los elementos
        this.elementosCelular();

        // el número de celular
        this.tCelular = new JTextField();
        this.tCelular.setBounds(680, 115, 120, 26);
        this.tCelular.setFont(Fuente);
        this.tCelular.setToolTipText("Indique el número de celular");
        this.tCelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(this.tCelular);

        // pide la dirección postal
        JLabel lDireccion = new JLabel("<html><b>Dirección: </b></html>");
        lDireccion.setBounds(10, 150, 100, 26);
        lDireccion.setFont(Fuente);
        this.add(lDireccion);
        this.tDireccion = new JTextField();
        this.tDireccion.setBounds(80, 150, 370, 26);
        this.tDireccion.setFont(Fuente);
        this.tDireccion.setToolTipText("Indique la dirección postal");
        this.tDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
                }
            });
        this.add(this.tDireccion);

        // pide el mail
        JLabel lMail = new JLabel("<html><b>E-Mail:</b></html>");
        lMail.setBounds(460, 150, 100, 26);
        lMail.setFont(Fuente);
        this.add(lMail);
        this.tMail = new JTextField();
        this.tMail.setBounds(510, 150, 210, 26);
        this.tMail.setFont(Fuente);
        this.tMail.setToolTipText("Indique la dirección de correo electrónico");
        this.tMail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
            }
        });
        this.add(this.tMail);

        // presenta la nacionalidad
        JLabel lNacionalidad = new JLabel("<html><b>Nacionalidad:</b></html>");
        lNacionalidad.setBounds(10, 185, 100, 26);
        lNacionalidad.setFont(Fuente);
        this.add(lNacionalidad);
        this.tNacionalidad = new JTextField();
        this.tNacionalidad.setBounds(110, 185, 140, 26);
        this.tNacionalidad.setFont(Fuente);
        this.tNacionalidad.setToolTipText("País de nacimiento");
        this.tNacionalidad.setEditable(false);
        this.add(this.tNacionalidad);

        // la provincia de nacimiento
        JLabel lProvincia = new JLabel ("<html><b>Prov.Nacimiento:</b></html>");
        lProvincia.setBounds(260, 185, 150, 26);
        lProvincia.setFont(Fuente);
        this.add(lProvincia);
        this.tProvincia = new JTextField();
        this.tProvincia.setBounds(380, 185, 190, 26);
        this.tProvincia.setFont(Fuente);
        this.tProvincia.setToolTipText("Jurisdicción de nacimiento");
        this.tProvincia.setEditable(false);
        this.add(this.tProvincia);

        // la localidad de nacimiento
        JLabel lLocalidad = new JLabel("<html><b>Cdad.Nacimiento:</b></html>");
        lLocalidad.setBounds(580, 185, 150, 26);
        lLocalidad.setFont(Fuente);
        this.add(lLocalidad);
        this.tLocalidad = new JTextField();
        this.tLocalidad.setBounds(705, 185, 185, 26);
        this.tLocalidad.setFont(Fuente);
        this.tLocalidad.setToolTipText("Ingrese parte del nombre y pulse buscar");
        this.add(this.tLocalidad);

        // el botón buscar localidad de nacimiento
        JButton btnBuscaLocalidad = new JButton();
        btnBuscaLocalidad.setBounds(890, 185, 26, 26);
        btnBuscaLocalidad.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        btnBuscaLocalidad.setToolTipText("Pulse para buscar la localidad de nacimiento");
        btnBuscaLocalidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscaLocalidad("Nacimiento");
            }
        });
        this.add(btnBuscaLocalidad);

        // el país de residencia
        JLabel lResidencia = new JLabel("<html><b>Residencia:</b></html>");
        lResidencia.setBounds(10, 225, 100, 26);
        lResidencia.setFont(Fuente);
        this.add(lResidencia);
        this.tResidencia = new JTextField();
        this.tResidencia.setBounds(100, 225, 150, 26);
        this.tResidencia.setFont(Fuente);
        this.tResidencia.setToolTipText("País de residencia del paciente");
        this.tResidencia.setEditable(false);
        this.add(this.tResidencia);

        // la provincia de residencia
        JLabel lProvResidencia = new JLabel("<html><b>Prov.Residencia:</b></html>");
        lProvResidencia.setBounds(260, 225, 150, 26);
        lProvResidencia.setFont(Fuente);
        this.add(lProvResidencia);
        this.tProvResidencia = new JTextField();
        this.tProvResidencia.setBounds(380, 225, 190, 26);
        this.tProvResidencia.setFont(Fuente);
        this.tProvResidencia.setToolTipText("Jurisdicción de residencia");
        this.tProvResidencia.setEditable(false);
        this.add(this.tProvResidencia);

        // la ciudad de residencia
        JLabel lCiudadResidencia = new JLabel("<html><b>Cdad.Residencia:</b></html>");
        lCiudadResidencia.setBounds(580, 225, 150, 26);
        lCiudadResidencia.setFont(Fuente);
        this.add(lCiudadResidencia);
        this.tLocResidencia = new JTextField();
        this.tLocResidencia.setBounds(705, 225, 185, 26);
        this.tLocResidencia.setFont(Fuente);
        this.tLocResidencia.setToolTipText("Ingrese parte del nombre y pulse buscar");
        this.add(this.tLocResidencia);

        // el botón buscar localidad de residencia
        JButton btnBuscaResidencia = new JButton();
        btnBuscaResidencia.setBounds(890, 225, 26, 26);
        btnBuscaResidencia.setToolTipText("Pulse para buscar la ciudad de residencia");
        btnBuscaResidencia.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        btnBuscaResidencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscaLocalidad("Residencia");
            }
        });
        this.add(btnBuscaResidencia);

        // la nacionalidad de la madre
        JLabel lMadre = new JLabel("<html><b>Madre:</b></html>");
        lMadre.setBounds(10, 260, 100, 26);
        lMadre.setFont(Fuente);
        this.add(lMadre);
        this.tNacMadre = new JTextField();
        this.tNacMadre.setBounds(65, 260, 185, 26);
        this.tNacMadre.setFont(Fuente);
        this.tNacMadre.setToolTipText("Nacionalidad de la madre");
        this.tNacMadre.setEditable(false);
        this.add(this.tNacMadre);

        // la jurisdicción de residencia de la madre
        JLabel lProvMadre = new JLabel("<html><b>Prov.Madre:</b></html>");
        lProvMadre.setBounds(260, 260, 100, 26);
        lProvMadre.setFont(Fuente);
        this.add(lProvMadre);
        this.tProvMadre = new JTextField();
        this.tProvMadre.setBounds(350, 260, 220, 26);
        this.tProvMadre.setFont(Fuente);
        this.tProvMadre.setToolTipText("Provincia de nacimiento de la madre");
        this.tProvMadre.setEditable(false);
        this.add(this.tProvMadre);

        // la localidad de residencia de la madre
        JLabel lLocMadre = new JLabel("<html><b>Cdad.Madre:</b></html>");
        lLocMadre.setBounds(580, 260, 100, 26);
        lLocMadre.setFont(Fuente);
        this.add(lLocMadre);
        this.tLocMadre = new JTextField();
        this.tLocMadre.setBounds(675, 260, 210, 26);
        this.tLocMadre.setFont(Fuente);
        this.tLocMadre.setToolTipText("Ingrese parte del nombre y pulse buscar");
        this.add(this.tLocMadre);

        // el botón buscar localidad
        JButton btnLocMadre = new JButton();
        btnLocMadre.setBounds(890, 260, 26, 26);
        btnLocMadre.setToolTipText("Pulse para buscar la ciudad de la madre");
        btnLocMadre.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbusqueda.png")));
        btnLocMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscaLocalidad("Madre");
            }
        });
        this.add(btnLocMadre);

        // pregunta si la madre es positiva
        JLabel lMadrePositiva = new JLabel("<html><b>Madre Positiva:</b></html>");
        lMadrePositiva.setBounds(10, 295, 150, 26);
        lMadrePositiva.setFont(Fuente);
        this.add(lMadrePositiva);
        this.cMadrePositiva = new JComboBox<>();
        this.cMadrePositiva.setBounds(120, 295, 90, 26);
        this.cMadrePositiva.setFont(Fuente);
        this.cMadrePositiva.setToolTipText("Indique si la madre es positiva para chagas");
        this.add(this.cMadrePositiva);

        // agrega los elementos al combo
        this.elementosMadre();

        // pide la ocupación
        JLabel lOcupacion = new JLabel("<html><b>Ocupación:</b></html>");
        lOcupacion.setBounds(220, 295, 100, 26);
        lOcupacion.setFont(Fuente);
        this.add(lOcupacion);
        this.tOcupacion = new JTextField();
        this.tOcupacion.setBounds(305, 295, 260, 26);
        this.tOcupacion.setFont(Fuente);
        this.tOcupacion.setToolTipText("Ingrese la ocupación del paciente");
        this.tOcupacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
                }
            });
        this.add(this.tOcupacion);

        // pide la obra social
        JLabel lObraSocial = new JLabel("<html><b>Obra Social:</b></html>");
        lObraSocial.setBounds(580, 295, 100, 26);
        lObraSocial.setFont(Fuente);
        this.add(lObraSocial);
        this.tObraSocial = new JTextField();
        this.tObraSocial.setBounds(670, 295, 250, 26);
        this.tObraSocial.setFont(Fuente);
        this.tObraSocial.setToolTipText("Indique la Obra Social si posee");
        this.tObraSocial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                pulsaTecla(evt);
                }
            });
        this.add(this.tObraSocial);

        // pide el motivo de consulta
        JLabel lMotivo = new JLabel("<html><b>Motivo de Consulta:</b></html>");
        lMotivo.setBounds(10, 340, 150, 26);
        lMotivo.setFont(Fuente);
        this.add(lMotivo);
        this.cMotivo = new JComboBox<>();
        this.cMotivo.setBounds(155, 340, 150, 26);
        this.cMotivo.setFont(Fuente);
        this.cMotivo.setEditable(true);
        this.cMotivo.setToolTipText("Seleccione de la lista el motivo de consulta");
        this.add(this.cMotivo);

        // carga los motivos de consulta
        this.elementosMotivos();

        // pide la derivación
        JLabel lDerivacion = new JLabel("<html><b>Derivado por:</b></html>");
        lDerivacion.setBounds(320, 340, 100, 26);
        lDerivacion.setFont(Fuente);
        this.add(lDerivacion);
        this.cDerivacion = new JComboBox<>();
        this.cDerivacion.setBounds(420, 340, 160, 26);
        this.cDerivacion.setFont(Fuente);
        this.cDerivacion.setEditable(true);
        this.cDerivacion.setToolTipText("Seleccione quien derivó al paciente");
        this.add(this.cDerivacion);

        // carga las fuentes de derivación
        this.elementosDerivacion();

        // pregunta si recibió tratamiento
        JLabel lTratamiento = new JLabel("<html><b>Recibió Tratamiento:</b></html>");
        lTratamiento.setBounds(590, 340, 150, 26);
        lTratamiento.setFont(Fuente);
        this.add(lTratamiento);
        this.cTratamiento = new JComboBox<>();
        this.cTratamiento.setBounds(740, 340, 90, 26);
        this.cTratamiento.setFont(Fuente);
        this.cTratamiento.setToolTipText("Indique si recibió tratamiento");
        this.add(this.cTratamiento);

        // agrega las opciones del tratamiento
        this.elementosTratamiento();

        // agrega el botón ver tratameinto
        JButton btnVerTratamiento = new JButton();
        btnVerTratamiento.setBounds(835, 340, 26, 26);
        btnVerTratamiento.setToolTipText("Pulse para ver los detalles del tratamiento");
        btnVerTratamiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
        btnVerTratamiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verTratamiento();
            }
        });
        this.add(btnVerTratamiento);

        // el título de comentarios y observaciones
        JLabel lComentarios = new JLabel("<html><b>Comentarios y Observaciones</b></html>");
        lComentarios.setBounds(10, 375, 300, 26);
        lComentarios.setFont(Fuente);
        this.add(lComentarios);

        // el panel de los comentarios
        JScrollPane scrollComentarios = new JScrollPane();
        scrollComentarios.setBounds(10, 410, 560, 145);
        this.add(scrollComentarios);

        // el texto de comentarios
        this.tComentarios = new JTextArea();
        this.tComentarios.setFont(Fuente);
        this.tComentarios.setToolTipText("Ingrese sus observaciones");

        // agegamos el viewport
        scrollComentarios.setViewportView(this.tComentarios);

        // el botón de transfusiones
        JButton btnTransfusiones = new JButton("Transfusiones");
        btnTransfusiones.setBounds(580, 410, 140, 26);
        btnTransfusiones.setFont(Fuente);
        btnTransfusiones.setToolTipText("Transfusiones que ha recibido el paciente");
        btnTransfusiones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mblood.png")));
        btnTransfusiones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verTransfusiones();
            }
        });
        this.add(btnTransfusiones);

        // el botón de transplantes
        JButton btnTransplantes = new JButton("Transplantes");
        btnTransplantes.setBounds(730, 410, 140, 26);
        btnTransplantes.setFont(Fuente);
        btnTransplantes.setToolTipText("Organos que ha recibido el paciente");
        btnTransplantes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mtransplante.png")));
        btnTransplantes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verTransplantes();
            }
        });
        this.add(btnTransplantes);

        // el botón de enfermedades
        JButton btnEnfermedades = new JButton("Enfermedades");
        btnEnfermedades.setBounds(580, 445, 140, 26);
        btnEnfermedades.setFont(Fuente);
        btnEnfermedades.setToolTipText("Enfermedades que ha padecido el paciente");
        btnEnfermedades.setIcon(new ImageIcon(getClass().getResource("/Graficos/mHospital.png")));
        btnEnfermedades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verEnfermedades();
            }
        });
        this.add(btnEnfermedades);

        // el botón de antecedentes familiares
        JButton btnFamiliares = new JButton("Ant. Familiares");
        btnFamiliares.setBounds(730, 445, 140, 26);
        btnFamiliares.setFont(Fuente);
        btnFamiliares.setToolTipText("Antecedentes Familiares del paciente");
        btnFamiliares.setIcon(new ImageIcon(getClass().getResource("/Graficos/mfamilia.jpg")));
        btnFamiliares.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                verFamiliares();
            }
        });
        this.add(btnFamiliares);

        // presenta el nombre del usuario
        JLabel lUsuario = new JLabel("<html><b>Usuario:</b></html>");
        lUsuario.setBounds(580, 480, 100, 26);
        lUsuario.setFont(Fuente);
        this.add(lUsuario);
        this.tUsuario = new JTextField();
        this.tUsuario.setBounds(645, 480, 90, 26);
        this.tUsuario.setFont(Fuente);
        this.tUsuario.setToolTipText("Usuario que ingresó el registro");
        this.tUsuario.setEditable(false);
        this.add(this.tUsuario);

        // presenta la fecha de alta
        JLabel lAlta = new JLabel("<html><b>Fecha Alta:</b></html>");
        lAlta.setBounds(580, 515, 100, 26);
        lAlta.setFont(Fuente);
        this.add(lAlta);
        this.tFecha = new JTextField();
        this.tFecha.setBounds(665, 515, 90, 26);
        this.tFecha.setFont(Fuente);
        this.tFecha.setToolTipText("Fecha de alta del registro");
        this.tFecha.setEditable(false);
        this.add(this.tFecha);

        // fijamos la fecha de alta y el usuario por defecto
        this.tUsuario.setText(Seguridad.Usuario);
        this.tFecha.setText(this.Herramientas.FechaActual());
        
        // presenta el botón grabar
        JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setBounds(810, 480, 110, 26);
        btnGrabar.setFont(Fuente);
        btnGrabar.setToolTipText("Graba el registro en la base");
        btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verificaPaciente();
            }
        });
        this.add(btnGrabar);

        // presenta el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(810, 515, 110, 26);
        btnCancelar.setFont(Fuente);
        btnCancelar.setToolTipText("Reinicia el formulario");
        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        this.add(btnCancelar);

        // fijamos el foco 
        this.tApellido.requestFocus();
        
    }

    // método que agrega los elementos al combo de
    // madre positiva
    private void elementosMadre(){

        // agregamos los elementos
        this.cMadrePositiva.addItem("");
        this.cMadrePositiva.addItem("Si");
        this.cMadrePositiva.addItem("No");
        this.cMadrePositiva.addItem("No Sabe");

    }

    // método que agrega los elementos al combo de
    // recibió tratamiento
    private void elementosTratamiento(){

        // agregamos las opciones
        this.cTratamiento.addItem("");
        this.cTratamiento.addItem("Si");
        this.cTratamiento.addItem("No");

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de tipo de documento
     */
    private void elementosDocumento(){

        // instanciamos el objeto y obtenemos la nómina
        Documentos Credenciales = new Documentos();
        ResultSet nomina = Credenciales.nominaDocumentos();

        // agregamos el primer elemento
        this.cDocumento.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cDocumento.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("descripcion")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de sexo
     */
    private void elementosSexo(){

        // agregamos los elementos
        this.cSexo.addItem(new ComboClave(0, ""));
        this.cSexo.addItem(new ComboClave(1, "Masculino"));
        this.cSexo.addItem(new ComboClave(2, "Femenino"));
        this.cSexo.addItem(new ComboClave(3, "S/E"));

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de estado civil
     */
    private void elementosEstado(){

        // instanciamos la clase y obtenemos la nómina
        Estados Civiles = new Estados();
        ResultSet nomina = Civiles.nominaEstados();

        // agregamos el primer elemento
        this.cEstado.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cEstado.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("estadocivil")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de companías
     * de celular
     */
    private void elementosCelular(){

        // instanciamos la clase y obtenemos la nómina
        Celulares Companias = new Celulares();
        ResultSet nomina = Companias.nominaCelulares();

        // agregamos el primer elemento
        this.cCompania.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cCompania.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("compania")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de motivos
     * de consulta
     */
    private void elementosMotivos(){

        // instanciamos la clase y obtenemos la nómina
        Motivos Origen = new Motivos();
        ResultSet nomina = Origen.nominaMotivos();

        // agregamos el primer elemento
        this.cMotivo.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cMotivo.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("motivo")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que agrega los elementos al combo de derivación
     */
    private void elementosDerivacion(){

        // instanciamos la clase y obtenemos la nómina
        Derivacion Origen = new Derivacion();
        ResultSet nomina = Origen.nominaDerivacion();

        // agregamos el primer elemento
        this.cDerivacion.addItem(new ComboClave(0, ""));

        try {

            // nos movemos al primer registro
            nomina.beforeFirst();

            // recorremos el vector
            while (nomina.next()){

                // agregamos el elemento
                this.cDerivacion.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("derivacion")));

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de enfermedades
     */
    private void verEnfermedades(){

        // si no grabó el registro
        if (this.tProtocolo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Debe grabar el registro primero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // instanciamos el formulario
        EnfPaciente Dolencias = new EnfPaciente(this.Contenedor, true);
        Dolencias.Protocolo = Integer.parseInt(this.tProtocolo.getText());
        Dolencias.grillaEnfermedades();
        Dolencias.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de transfusiones
     */
    private void verTransfusiones(){

        // si no grabó el registro
        if (this.tProtocolo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Debe grabar el registro primero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // instanciamos el formulario y cargamos los registros
        FormTransfusiones Transfusiones = new FormTransfusiones(this.Contenedor, true);
        Transfusiones.Protocolo = Integer.parseInt(this.tProtocolo.getText());
        Transfusiones.cargaTransfusiones();
        Transfusiones.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de enfermedades
     */
    private void verTransplantes(){

        // si no grabó el registro
        if (this.tProtocolo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Debe grabar el registro primero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // instanciamos el formulario
        FormTransplantes Transplantes = new FormTransplantes(this.Contenedor, true);
        Transplantes.Protocolo = Integer.parseInt(this.tProtocolo.getText());
        Transplantes.cargaTransplantes();
        Transplantes.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el formulario de antecedentes familiares
     */
    private void verFamiliares(){

        // si no grabó el registro
        if (this.tProtocolo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Debe grabar el registro primero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // instanciamos el formulario
        FormFamiliares Familiares = new FormFamiliares(this.Contenedor, true);
        Familiares.Protocolo = Integer.parseInt(this.tProtocolo.getText());
        Familiares.verFamiliares();
        Familiares.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que presenta el formulario de tratamiento
     */
    private void verTratamiento(){

        // si no grabó el registro
        if (this.tProtocolo.getText().isEmpty()){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Debe grabar el registro primero",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

        }

        // instanciamos el formulario y asignamos el protocolo
        FormTratamiento Tratamiento = new FormTratamiento(this.Contenedor, true);
        Tratamiento.Protocolo = Integer.parseInt(this.tProtocolo.getText());
        Tratamiento.cargaTratamientos();
        Tratamiento.setVisible(true);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que verifica los datos del formulario antes de
     * enviarlo al servidor
     */
    private void verificaPaciente(){

        // si no ingresó el apellido
        if (this.tApellido.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Ingrese el apellido del paciente",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tApellido.requestFocus();
			return;

        // si ingresó   
        } else {

            // lo convertimos a mayúsculas
            this.tApellido.setText(this.tApellido.getText().toUpperCase());

        }

        // si no ingresó el nombre
        if (this.tNombre.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Ingrese el nombre del paciente",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tNombre.requestFocus();
			return;

        // si ingresó 
        } else {

            // lo convertimos a mayúsculas
            this.tNombre.setText(this.tNombre.getText().toUpperCase());

        }

        // si no seleccionó el tipo de documento
        ComboClave item = (ComboClave) this.cDocumento.getSelectedItem();
        if (item == null){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Seleccione de la lista el tipo de documento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cDocumento.requestFocus();
			return;

        }

        // si no ingresó el número de documento
        if (this.tDocumento.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique el número de documento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tDocumento.requestFocus();
			return;

        }

        // si está dando un alta
        if (this.tProtocolo.getText().isEmpty()){

            // por las dudas verificamos que no esté repetido
            boolean Encontrado = this.Personas.existePaciente(this.tDocumento.getText());

            // si lo encontró
            if (Encontrado){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "Ya existe un paciente declarado con\n" +
                            "ese número de documento",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tApellido.requestFocus();
                return;

            }

        }

        // si no ingresó ni la fecha de nacimiento ni la edad
        int edad = (Integer) this.sEdad.getValue();
        if(this.Herramientas.fechaJDate(this.dNacimiento) == null && edad == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Ingrese la fecha de nacimiento o la edad",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tDocumento.requestFocus();
			return;

        }

        // si ingresó la fecha de nacimiento y es un alta
        // calcula la edad

        // si no indicó el sexo
        item = (ComboClave) this.cSexo.getSelectedItem();
        if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Seleccione el sexo de la lista",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cSexo.requestFocus();
			return;

        }

        // si no indicó el estado civil
        item = (ComboClave) this.cEstado.getSelectedItem();
        if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Seleccione el estado civil de la lista",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cEstado.requestFocus();
			return;

        }

        // los hijos los permite en cero

        // si no ingresó ni teléfono ni celular ni mail
        if (this.tTelefono.getText().isEmpty() && this.tCelular.getText().isEmpty() && this.tMail.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "No hay forma de comunicarse con el paciente\n" +
                        "indique Teléfono, Celular o Mail al menos.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tTelefono.requestFocus();
			return;

        }

        // si ingresó celular
        if (!this.tCelular.getText().isEmpty()){

            // verifica la compañía
            item = (ComboClave) this.cCompania.getSelectedItem();
            if (item == null){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "Seleccione de la lista la" +
                            "compañía de celular",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tCelular.requestFocus();
                return;

            }

        }

        // si no ingresó la dirección
        if (this.tDireccion.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique la dirección del paciente",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tDireccion.requestFocus();
			return;

        // si ingresó 
        } else {

            // lo convertimos a mayúsculas
            this.tDireccion.setText(this.tDireccion.getText().toUpperCase());

        }

        // si ingresó el mail
        if (!this.tMail.getText().isEmpty()){

            // verifica que sea correcto
            if (!this.Herramientas.esEmailCorrecto(this.tMail.getText())){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "La dirección de mail parece incorrecta",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tDireccion.requestFocus();
                return;

            // si está correcto
            } else {

                // lo pasamos a minúsculas
                this.tMail.setText(this.tMail.getText().toLowerCase());

            }

        }

        // si no seleccionó la localidad de nacimiento
        if (this.IdLocNacimiento.equals("")){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique la localidad de nacimiento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tLocalidad.requestFocus();
			return;

        }

        // si no seleccionó la localidad de residencia
        if (this.IdLocResidencia.equals("")){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique la localidad de residencia",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tLocResidencia.requestFocus();
			return;

        }

        // si no indicó la localidad de la madre
        if (this.IdLocMadre.equals("")){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique la localidad de la madre",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tLocMadre.requestFocus();
			return;

        }

        // si no indicó si la madre es positiva
        if (this.cMadrePositiva.getSelectedItem().toString().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique si la madre es positiva para Chagas",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cMadrePositiva.requestFocus();
			return;

        }

        // si no ingresó la ocupación
        if (this.tOcupacion.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Ingrese la ocupación del paciente",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tOcupacion.requestFocus();
			return;

        }

        // si no ingresó la obra social
        if (this.tObraSocial.getText().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique la Obra Social del paciente",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.tObraSocial.requestFocus();
			return;

        }
        // si no indicó el motivo de consulta
        item = (ComboClave) this.cMotivo.getSelectedItem();
        if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Seleccione de la lista el motivo de consulta",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cMotivo.requestFocus();
			return;

        }

        // si no seleccionó la derivación
        item = (ComboClave) this.cDerivacion.getSelectedItem();
        if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Seleccione de la lista la derivación",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cDerivacion.requestFocus();
			return;

        }

        // si no indicó si recibió tratamiento
        if (this.cTratamiento.getSelectedItem().toString().isEmpty()){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this.Contenedor,
                        "Indique si el paciente recibió tratamiento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cTratamiento.requestFocus();
			return;

        }

        // si llegó hasta aquí envía los datos
        this.grabaPaciente();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado luego de verificar el formulario que
     * ejecuta la consulta en el servidor
     */
    private void grabaPaciente(){

        // si está insertando
        if (this.tProtocolo.getText().isEmpty()){
            this.Personas.setProtocolo(0);
        } else {
            this.Personas.setProtocolo(Integer.parseInt(this.tProtocolo.getText()));
        }

        // asignamos en la clase
        this.Personas.setApellido(this.tApellido.getText());
        this.Personas.setNombre(this.tNombre.getText());

        // obtenemos la clave del tipo de documento
        ComboClave item = (ComboClave) this.cDocumento.getSelectedItem();
        this.Personas.setTipoDocumento(item.getClave());

        // seguimos asignando
        this.Personas.setDocumento(this.tDocumento.getText());
        this.Personas.setFechaNacimiento(this.Herramientas.fechaJDate(this.dNacimiento));
        this.Personas.setEdad((Integer) this.sEdad.getValue());

        // obtenemos la clave del sexo
        item = (ComboClave) this.cSexo.getSelectedItem();
        this.Personas.setIdSexo(item.getClave());

        // obtenemos la clave del estado civil
        item = (ComboClave) this.cEstado.getSelectedItem();
        this.Personas.setIdEstado(item.getClave());

        // seguimos asignando
        this.Personas.setHijos((Integer) this.sHijos.getValue());
        this.Personas.setTelefono(this.tTelefono.getText());
        this.Personas.setCelular(this.tCelular.getText());

        // obtenemos la clave de la compañía
        item = (ComboClave) this.cCompania.getSelectedItem();
        this.Personas.setIdCompania(item.getClave());

        // seguimos asignando
        this.Personas.setDireccion(this.tDireccion.getText());
        this.Personas.setEmail(this.tMail.getText());
        this.Personas.setIdLocNacimiento(this.IdLocNacimiento);
        this.Personas.setIdLocResidencia(this.IdLocResidencia);
        this.Personas.setIdLocMadre(this.IdLocMadre);
        this.Personas.setMadrePositiva(this.cMadrePositiva.getSelectedItem().toString());
        this.Personas.setOcupacion(this.tOcupacion.getText());
        this.Personas.setObraSocial(this.tObraSocial.getText());

        // obtenemos la clave del motivo
        item = (ComboClave) this.cMotivo.getSelectedItem();
        this.Personas.setIdMotivo(item.getClave());

        // obtenemos la clave de la derivación
        item = (ComboClave) this.cDerivacion.getSelectedItem();
        this.Personas.setIdDerivacion(item.getClave());

        // terminamos de asignar
        this.Personas.setTratamiento(this.cTratamiento.getSelectedItem().toString());
        this.Personas.setComentarios(this.tComentarios.getText());

        // grabamos el registro
        int clave = this.Personas.grabaPaciente();

        // si es distinto de cero
        if (clave != 0){

            // asignamos el protocolo y la historia clínica
            this.tProtocolo.setText(Integer.toString(this.Personas.getProtocolo()));
            this.tHistoria.setText(this.Personas.getHistoriaClinica());

            // asignamos en el formulario de antecedentes
            this.Antecedentes.setProtocolo(this.Personas.getProtocolo());

            // presenta el mensaje
            new Mensaje("Registro grabado");

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tipo - string con el tipo de localidad a buscar
     * Método llamado al pulsar el botón buscar localidad,
     * recibe como parámetro si está buscando la localidad
     * de nacimiento, de residencia o de la madre.
     * El método busca la localidad en la base de datos y
     * luego pasa el array de resultados al formulario
     */
    private void buscaLocalidad(String tipo){

        // definimos las variables
        String Busqueda = "";
        ResultSet Nomina;

        // si está buscando la localidad de nacimiento
        if (tipo.equals("Nacimiento")){

            // verificamos que halla ingresado texto
            if (this.tLocalidad.getText().isEmpty()){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "Ingrese parte del nombre de la localidad",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tLocalidad.requestFocus();
                return;

            } else {

                // asignamos en la variable
                Busqueda = this.tLocalidad.getText();

            }

        // si está buscando la residencia
        } else if (tipo.equals("Residencia")){

            // verificamos que halla ingresado texto
            if (this.tLocResidencia.getText().isEmpty()){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "Ingrese parte del nombre de la localidad",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tLocResidencia.requestFocus();
                return;

            } else {

                // asignamos en la variable
                Busqueda = this.tLocResidencia.getText();

            }

        // si está buscando la madre
        } else if (tipo.equals("Madre")){

            // verificamos que halla ingresado texto
            if (this.tLocMadre.getText().isEmpty()){

                // presenta el mensaje
                JOptionPane.showMessageDialog(this.Contenedor,
                            "Ingrese parte del nombre de la localidad",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                this.tLocMadre.requestFocus();
                return;

            } else {

                // asignamos en la variable
                Busqueda = this.tLocMadre.getText();

            }

        }

        // buscamos la localidad en la base
        Nomina = this.Ciudades.buscaLocalidad(Busqueda)        ;

        try {

            // si hay registros
            if (Nomina.next()){

                // instanciamos el formulario y le pasamos el vector
                FormBuscaLocalidad Localidades = new FormBuscaLocalidad(this.Contenedor, true, this, tipo);
                Localidades.cargaLocalidades(Nomina);
                Localidades.setVisible(true);

            // si no hubo registros
            } else {

                // presenta el mensaje y retorna
                JOptionPane.showMessageDialog(this,
                                              "No hay registros coincidentes",
                                              "Error",
                                              JOptionPane.ERROR_MESSAGE);

            }

        // si hubo un error
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario y lo prepara para la
     * carga de un nuevo paciente
     */
    public void nuevoPaciente(){

        // inicializamos los campos
        this.tProtocolo.setText("");
        this.tHistoria.setText("");
        this.tApellido.setText("");
        this.tNombre.setText("");
        this.tDocumento.setText("");

        // instanciamos el nuevo item para los combos
        ComboClave item = new ComboClave(0, "");

        // asignamos en los campos
        this.cDocumento.setSelectedItem(item);
        this.dNacimiento.setDate(null);
        this.sEdad.setValue(0);
        this.cSexo.setSelectedItem(item);
        this.cEstado.setSelectedItem(item);
        this.sHijos.setValue(0);
        this.tTelefono.setText("");
        this.tCelular.setText("");
        this.cCompania.setSelectedItem(item);
        this.tDireccion.setText("");
        this.tMail.setText("");
        this.tNacionalidad.setText("");
        this.tProvincia.setText("");
        this.tLocalidad.setText("");
        this.tResidencia.setText("");
        this.tProvResidencia.setText("");
        this.tLocResidencia.setText("");
        this.tNacMadre.setText("");
        this.tProvMadre.setText("");
        this.tLocMadre.setText("");
        this.cMadrePositiva.setSelectedItem("");
        this.tOcupacion.setText("");
        this.tObraSocial.setText("");
        this.cMotivo.setSelectedItem(item);
        this.cDerivacion.setSelectedItem(item);
        this.cTratamiento.setSelectedItem("");
        this.tComentarios.setText("");
        this.tUsuario.setText(Seguridad.Usuario);
        this.tFecha.setText(this.Herramientas.FechaActual());

        // limpiamos el formulario de antecedentes
        this.Antecedentes.limpiaFormulario();

        // seteamos el foco
        this.tApellido.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el cuadro de diálogo de búsqueda y
     * luego presenta la grilla con los resultados
     */
    public void buscaPaciente(){

        // pedimos el texto a buscar
		// pide el texto a buscar
		String respuesta = JOptionPane.showInputDialog(this,
								  	  "Ingrese el texto a buscar",
									  "Buscar",
									  JOptionPane.INFORMATION_MESSAGE);

		// si canceló
		if (respuesta.equals("")){
			return;
		}

		// busca en la base
		ResultSet nomina = this.Personas.buscaPaciente(respuesta);

		try {

			// si hubo registros
			if (nomina.next()){

				// instanciamos el formulario y cargamos el resultado
				FormBuscaPaciente Encontrados = new FormBuscaPaciente(this.Contenedor, true, this);
				Encontrados.cargaPacientes(nomina);
				Encontrados.setVisible(true);

            // si no hubo registros
			} else {

				// presentamos el alerta
				JOptionPane.showMessageDialog(this,
											"No se han encontrado laboratorios",
											"Error",
											JOptionPane.ERROR_MESSAGE);
				return;

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - entero con la clave del registro
     * Método llamado desde la grilla de resultados que
     * recibe como parámetro la clave del registro y carga
     * los datos del mismo en el formulario
     */
    public void verPaciente(int protocolo){

        // obtenemos el registro
        this.Personas.getDatosPaciente(protocolo);

        // ahora cargamos el formulario
        this.tProtocolo.setText(Integer.toString(this.Personas.getProtocolo()));
        this.tHistoria.setText(this.Personas.getHistoriaClinica());
        this.tApellido.setText(this.Personas.getApellido());
        this.tNombre.setText(this.Personas.getNombre());

        // asignamos el tipo de documento
        ComboClave item = new ComboClave(this.Personas.getTipoDocumento(), this.Personas.getDescDocumento());
        this.cDocumento.setSelectedItem(item);

        // seguimos asignando
        this.tDocumento.setText(this.Personas.getDocumento());
        this.dNacimiento.setDate(this.Herramientas.StringToDate(this.Personas.getFechaNacimiento()));
        this.sEdad.setValue(this.Personas.getEdad());

        // asignamos el sexo y el estado civil
        item = new ComboClave(this.Personas.getIdSexo(), this.Personas.getSexo());
        this.cSexo.setSelectedItem(item);
        item = new ComboClave(this.Personas.getIdEstado(), this.Personas.getEstado());
        this.cEstado.setSelectedItem(item);

        // seguimos asignando
        this.sHijos.setValue(this.Personas.getHijos());
        this.tTelefono.setText(this.Personas.getTelefono());
        this.tCelular.setText(this.Personas.getCelular());

        // asignamos la clave del celular
        item = new ComboClave(this.Personas.getIdCompania(), this.Personas.getCompania());
        this.cCompania.setSelectedItem(item);

        // seguimos asignando
        this.tDireccion.setText(this.Personas.getDireccion());
        this.tMail.setText(this.Personas.getEmail());
        this.tNacionalidad.setText(this.Personas.getPaisNacimiento());
        this.tProvincia.setText(this.Personas.getJurNacimiento());
        this.tLocalidad.setText(this.Personas.getLocNacimiento());
        this.IdLocNacimiento = this.Personas.getIdLocNacimiento();
        this.tResidencia.setText(this.Personas.getPaisResidencia());
        this.tProvResidencia.setText(this.Personas.getJurResidencia());
        this.tLocResidencia.setText(this.Personas.getLocResidencia());
        this.IdLocResidencia = this.Personas.getIdLocResidencia();
        this.tNacMadre.setText(this.Personas.getPaisMadre());
        this.tProvMadre.setText(this.Personas.getJurMadre());
        this.tLocMadre.setText(this.Personas.getLocMadre());
        this.IdLocMadre = this.Personas.getIdLocMadre();
        this.cMadrePositiva.setSelectedItem(this.Personas.getMadrePositiva());
        this.tOcupacion.setText(this.Personas.getOcupacion());
        this.tObraSocial.setText(this.Personas.getObraSocial());

        // asignamos la clave del motivo y la derivación
        item = new ComboClave(this.Personas.getIdMotivo(), this.Personas.getMotivo());
        this.cMotivo.setSelectedItem(item);
        item = new ComboClave(this.Personas.getIdDerivacion(), this.Personas.getDerivacion());
        this.cDerivacion.setSelectedItem(item);

        // terminamos de asignar
        this.cTratamiento.setSelectedItem(this.Personas.getTratamiento());
        this.tComentarios.setText(this.Personas.getComentarios());
        this.tUsuario.setText(this.Personas.getUsuario());
        this.tFecha.setText(this.Personas.getFechaAlta());

        // mostramos los datos de los antecedentes y cargamos
        // la grilla de visitas
        this.Antecedentes.verAntecedentes(protocolo);
        this.Antecedentes.cargaVisitas();

        // fijamos el foco
        this.tApellido.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar una tecla sobre un campo
     * si se pulsó enter, avanza al siguiente campo
     */
    private void pulsaTecla(KeyEvent evt){

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            // según en que campo esté
            if (this.tApellido.hasFocus()){
                this.tNombre.requestFocus();
            } else if (this.tNombre.hasFocus()){
                this.cDocumento.requestFocus();
            } else if (this.tDocumento.hasFocus()){
                this.dNacimiento.requestFocus();
            } else if (this.tTelefono.hasFocus()){
                this.cCompania.requestFocus();
            } else if (this.tCelular.hasFocus()){
                this.tDireccion.requestFocus();
            } else if (this.tDireccion.hasFocus()){
                this.tMail.requestFocus();
            } else if (this.tMail.hasFocus()){
                this.tLocalidad.requestFocus();
            } else if (this.tOcupacion.hasFocus()){
                this.tObraSocial.requestFocus();
            } else if (this.tObraSocial.hasFocus()){
                this.cMotivo.requestFocus();
            }

        }

    }

}
