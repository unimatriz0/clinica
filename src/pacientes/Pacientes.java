/*

    Nombre: Pacientes
    Fecha: 09/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla el abm de pacientes

 */

// definimos el paquete
package pacientes;

// importamos las librerìas
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// definición de la clase
public class Pacientes{

    // declaraciòn de las variables de clase
    private Conexion Enlace;         // puntero a la base de datos
    private int Protocolo;           // clave del registro
    private int IdLaboratorio;       // laboratorio del usuario
    private String Laboratorio;      // nombre del laboratorio
    private String HistoriaClinica;  // historia clìnica del paciente
    private String Apellido;         // apellido del paciente
    private String Nombre;           // nombre del paciente
    private String Documento;        // nùmero de documento
    private int TipoDocumento;       // clave del tipo de documento
    private String DescDocumento;    // descripción (nombre) del tipo de documento
    private String FechaNacimiento;  // fecha de nacimiento del paciente
    private int Edad;                // edad en años del paciente
    private int IdSexo;              // clave del sexo
    private String Sexo;             // descripción del sexo
    private int IdEstado;            // clave del estado civil
    private String Estado;           // nombre del estado civil
    private int Hijos;               // número de hijos del paciente
    private String Direccion;        // dirección postal del paciente
    private String Telefono;         // numero de teléfono fijo
    private String Celular;          // número de celular
    private int IdCompania;          // clave de la prestadora del celular
    private String Compania;         // nombre de la prestadora del celular
    private String EMail;            // dirección de correo electrónico
    private String IdLocNacimiento;  // clave indec de la localidad de nacimiento
    private String LocNacimiento;    // nombre de la localidad de nacimiento
    private String CoordNacimiento;  // coordenadas de nacimiento
    private String JurNacimiento;    // nombre de la jurisdicciòn de nacimiento
    private String PaisNacimiento;   // nombre del país de nacimiento
    private String IdLocResidencia;  // clave indec de la localidad de residencia
    private String LocResidencia;    // nombre de la localidad de residencia
    private String CoordResidencia;  // coordenadas indec de las coordenadas de residencia
    private String JurResidencia;    // nombre de la jurisdicción de residencia
    private String PaisResidencia;   // nombre del país de residencia
    private String IdLocMadre;       // coordenadas indec de la localidad de la madre
    private String LocMadre;         // nombre de la localidad de la madre
    private String CoordMadre;       // coordenadas de la localidad de la madre
    private String JurMadre;         // nombre de la jurisdicción de la madre
    private String PaisMadre;        // país de la madre
    private String MadrePositiva;    // si la madre es positiva para chagas
    private String Ocupacion;        // ocupación del paciente
    private String ObraSocial;       // nombre de la obra social
    private int IdMotivo;            // clave del motivo de consulta
    private String Motivo;           // descripción del motivo de consulta
    private int IdDerivacion;        // clave de la derivación
    private String Derivacion;       // descripción de la derivación
    private String Tratamiento;      // si ha recibido o no tratamiento
    private int IdUsuario;           // clave del usuario actual
    private String Usuario;          // nombre del usuario
    private String FechaAlta;        // fecha de alta del registro
    private String Comentarios;      // comentarios y observaciones

    // constructor de la clase
    public Pacientes(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Protocolo = 0;
        this.IdLaboratorio = Seguridad.Laboratorio;
        this.Laboratorio = "";
        this.HistoriaClinica = "";
        this.Apellido = "";
        this.Nombre = "";
        this.Documento = "";
        this.TipoDocumento = 0;
        this.FechaNacimiento = "";
        this.DescDocumento = "";
        this.Edad = 0;
        this.IdSexo = 0;
        this.Sexo = "";
        this.IdEstado = 0;
        this.Estado = "";
        this.Hijos = 0;
        this.Direccion = "";
        this.Telefono = "";
        this.Celular = "";
        this.IdCompania = 0;
        this.Compania = "";
        this.EMail = "";
        this.IdLocNacimiento = "";
        this.LocNacimiento = "";
        this.CoordNacimiento = "";
        this.JurNacimiento = "";
        this.PaisNacimiento = "";
        this.IdLocResidencia = "";
        this.LocResidencia = "";
        this.CoordResidencia = "";
        this.JurResidencia = "";
        this.PaisResidencia = "";
        this.IdLocMadre = "";
        this.LocMadre = "";
        this.CoordMadre = "";
        this.JurMadre = "";
        this.PaisMadre = "";
        this.MadrePositiva = "";
        this.Ocupacion = "";
        this.ObraSocial = "";
        this.IdMotivo = 0;
        this.Motivo = "";
        this.IdDerivacion = 0;
        this.Derivacion = "";
        this.Tratamiento = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.FechaAlta = "";
        this.Comentarios = "";

    }

    // métodos de asignación de valores
    public void setProtocolo(int protocolo){
        this.Protocolo = protocolo;
    }
    public void setApellido(String apellido){
        this.Apellido = apellido;
    }
    public void setNombre(String nombre){
        this.Nombre = nombre;
    }
    public void setDocumento(String documento){
        this.Documento = documento;
    }
    public void setTipoDocumento(int tipo){
        this.TipoDocumento = tipo;
    }
    public void setFechaNacimiento(String fecha){
        this.FechaNacimiento = fecha;
    }
    public void setEdad(int edad){
        this.Edad = edad;
    }
    public void setIdSexo(int idsexo){
        this.IdSexo = idsexo;
    }
    public void setIdEstado(int idestado){
        this.IdEstado = idestado;
    }
    public void setHijos(int hijos){
        this.Hijos = hijos;
    }
    public void setDireccion(String direccion){
        this.Direccion = direccion;
    }
    public void setTelefono(String telefono){
        this.Telefono = telefono;
    }
    public void setCelular(String celular){
        this.Celular = celular;
    }
    public void setIdCompania(int idcompania){
        this.IdCompania = idcompania;
    }
    public void setEmail(String mail){
        this.EMail = mail;
    }
    public void setIdLocNacimiento(String idlocalidad){
        this.IdLocNacimiento = idlocalidad;
    }
    public void setCoordNacimiento(String coordenadas){
        this.CoordNacimiento = coordenadas;
    }
    public void setIdLocResidencia(String idlocalidad){
        this.IdLocResidencia = idlocalidad;
    }
    public void setCoordResidencia(String coordenadas){
        this.CoordResidencia = coordenadas;
    }
    public void setIdLocMadre(String idlocalidad){
        this.IdLocMadre = idlocalidad;
    }
    public void setCoordMadre(String coordenadas){
        this.CoordMadre = coordenadas;
    }
    public void setMadrePositiva( String positiva){
        this.MadrePositiva = positiva;
    }
    public void setOcupacion(String ocupacion){
        this.Ocupacion = ocupacion;
    }
    public void setObraSocial(String obra){
        this.ObraSocial = obra;
    }
    public void setIdMotivo(int idmotivo){
        this.IdMotivo = idmotivo;
    }
    public void setIdDerivacion(int idderivacion){
        this.IdDerivacion = idderivacion;
    }
    public void setTratamiento(String tratamiento){
        this.Tratamiento = tratamiento;
    }
    public void setComentarios(String comentarios){
        this.Comentarios = comentarios;
    }

    // métodos de retorno de valores
    public int getProtocolo(){
        return this.Protocolo;
    }
    public String getLaboratorio(){
        return this.Laboratorio;
    }
    public String getHistoriaClinica(){
        return this.HistoriaClinica;
    }
    public String getApellido(){
        return this.Apellido;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getDocumento(){
        return this.Documento;
    }
    public int getTipoDocumento(){
        return this.TipoDocumento;
    }
    public String getFechaNacimiento(){
        return this.FechaNacimiento;
    }
    public String getDescDocumento(){
        return this.DescDocumento;
    }
    public int getEdad(){
        return this.Edad;
    }
    public int getIdSexo(){
        return this.IdSexo;
    }
    public String getSexo(){
        return this.Sexo;
    }
    public int getIdEstado(){
        return this.IdEstado;
    }
    public String getEstado(){
        return this.Estado;
    }
    public int getHijos(){
        return this.Hijos;
    }
    public String getDireccion(){
        return this.Direccion;
    }
    public String getTelefono(){
        return this.Telefono;
    }
    public String getCelular(){
        return this.Celular;
    }
    public int getIdCompania(){
        return this.IdCompania;
    }
    public String getCompania(){
        return this.Compania;
    }
    public String getEmail(){
        return this.EMail;
    }
    public String getIdLocNacimiento(){
        return this.IdLocNacimiento;
    }
    public String getLocNacimiento(){
        return this.LocNacimiento;
    }
    public String getCoordNacimiento(){
        return this.CoordNacimiento;
    }
    public String getJurNacimiento(){
        return this.JurNacimiento;
    }
    public String getPaisNacimiento(){
        return this.PaisNacimiento;
    }
    public String getIdLocResidencia(){
        return this.IdLocResidencia;
    }
    public String getLocResidencia(){
        return this.LocResidencia;
    }
    public String getCoordResidencia(){
        return this.CoordResidencia;
    }
    public String getJurResidencia(){
        return this.JurResidencia;
    }
    public String getPaisResidencia(){
        return this.PaisResidencia;
    }
    public String getIdLocMadre(){
        return this.IdLocMadre;
    }
    public String getLocMadre(){
        return this.LocMadre;
    }
    public String getCoordMadre(){
        return this.CoordMadre;
    }
    public String getJurMadre(){
        return this.JurMadre;
    }
    public String getPaisMadre(){
        return this.PaisMadre;
    }
    public String getMadrePositiva(){
        return this.MadrePositiva;
    }
    public String getOcupacion(){
        return this.Ocupacion;
    }
    public String getObraSocial(){
        return this.ObraSocial;
    }
    public int getIdMotivo(){
        return this.IdMotivo;
    }
    public String getMotivo(){
        return this.Motivo;
    }
    public int getIdDerivacion(){
        return this.IdDerivacion;
    }
    public String getDerivacion(){
        return this.Derivacion;
    }
    public String getTratamiento(){
        return this.Tratamiento;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getComentarios(){
        return this.Comentarios;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente - clave del registro
     * Método que recibe como parámetro la clave de un
     * paciente y obtiene los valores del registro para
     * asignarlos a las variables de clase
     */
    public void getDatosPaciente(int idpaciente) {

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo, " +
                   "       diagnostico.v_personas.laboratorio AS laboratorio, " +
                   "       diagnostico.v_personas.historia_clinica AS historiaclinica, " +
                   "       diagnostico.v_personas.apellido AS apellido, " +
                   "       diagnostico.v_personas.nombre AS nombre, " +
                   "       diagnostico.v_personas.documento AS documento, " +
                   "       diagnostico.v_personas.id_documento AS iddocumento, " +
                   "       diagnostico.v_personas.tipo_documento AS descdocumento, " +
                   "       diagnostico.v_personas.fecha_nacimiento AS fechanacimiento, " +
                   "       diagnostico.v_personas.edad AS edad, " +
                   "       diagnostico.v_personas.id_sexo AS idsexo, " +
                   "       diagnostico.v_personas.sexo AS sexo, " +
                   "       diagnostico.v_personas.id_estado_civil AS idestado, " +
                   "       diagnostico.v_personas.estado_civil AS estadocivil, " +
                   "       diagnostico.v_personas.hijos AS hijos, " +
                   "       diagnostico.v_personas.direccion AS direccion, " +
                   "       diagnostico.v_personas.telefono AS telefono, " +
                   "       diagnostico.v_personas.celular AS celular, " +
                   "       diagnostico.v_personas.id_compania AS idcompania, " +
                   "       diagnostico.v_personas.compania AS compania, " +
                   "       diagnostico.v_personas.mail AS mail, " +
                   "       diagnostico.v_personas.id_localidad_nacimiento AS idlocnacimiento, " +
                   "       diagnostico.v_personas.localidad_nacimiento AS locnacimiento, " +
                   "       diagnostico.v_personas.jurisdiccion_nacimiento AS jurnacimiento, " +
                   "       diagnostico.v_personas.pais_nacimiento AS paisnacimiento, " +
                   "       diagnostico.v_personas.coordenadas_nacimiento AS coordnacimiento, " +
                   "       diagnostico.v_personas.id_localidad_residencia AS idlocresidencia, " +
                   "       diagnostico.v_personas.localidad_residencia AS locresidencia, " +
                   "       diagnostico.v_personas.jurisdiccion_residencia AS jurresidencia, " +
                   "       diagnostico.v_personas.pais_residencia AS paisresidencia, " +
                   "       diagnostico.v_personas.coordenadas_residencia AS coordresidencia, " +
                   "       diagnostico.v_personas.id_localidad_madre AS idlocmadre, " +
                   "       diagnostico.v_personas.localidad_madre AS localidadmadre, " +
                   "       diagnostico.v_personas.jurisdiccion_madre AS jurmadre, " +
                   "       diagnostico.v_personas.pais_madre AS paismadre, " +
                   "       diagnostico.v_personas.madre_positiva AS madrepositiva, " +
                   "       diagnostico.v_personas.ocupacion AS ocupacion, " +
                   "       diagnostico.v_personas.obra_social AS obrasocial, " +
                   "       diagnostico.v_personas.id_motivo AS idmotivo, " +
                   "       diagnostico.v_personas.motivo AS motivo, " +
                   "       diagnostico.v_personas.id_derivacion AS idderivacion, " +
                   "       diagnostico.v_personas.derivacion AS derivacion, " +
                   "       diagnostico.v_personas.tratamiento AS tratamiento, " +
                   "       diagnostico.v_personas.usuario AS usuario, " +
                   "       diagnostico.v_personas.fecha_alta AS fechaalta, " +
                   "       diagnostico.v_personas.comentarios AS comentarios " +
                   "FROM diagnostico.v_personas " +
                   "WHERE diagnostico.v_personas.protocolo = '" + idpaciente + "'; ";

        // la ejecutamos
        Resultado = this.Enlace.Consultar(Consulta);

        // obtenemos el registro
        try {

            // asignamos los valores en el registro
            Resultado.next();
            this.Protocolo = Resultado.getInt("protocolo");
            this.Laboratorio = Resultado.getString("laboratorio");
            this.HistoriaClinica = Resultado.getString("historiaclinica");
            this.Apellido = Resultado.getString("apellido");
            this.Nombre = Resultado.getString("nombre");
            this.Documento = Resultado.getString("documento");
            this.TipoDocumento = Resultado.getInt("iddocumento");
            this.DescDocumento = Resultado.getString("descdocumento");
            this.FechaNacimiento = Resultado.getString("fechanacimiento");
            this.Edad = Resultado.getInt("edad");
            this.IdSexo = Resultado.getInt("idsexo");
            this.Sexo = Resultado.getString("sexo");
            this.IdEstado = Resultado.getInt("idestado");
            this.Estado = Resultado.getString("estadocivil");
            this.Hijos = Resultado.getInt("hijos");
            this.Direccion = Resultado.getString("direccion");
            this.Telefono = Resultado.getString("telefono");
            this.Celular = Resultado.getString("celular");
            this.IdCompania = Resultado.getInt("idcompania");
            this.Compania = Resultado.getString("compania");
            this.EMail = Resultado.getString("mail");
            this.IdLocNacimiento = Resultado.getString("idlocnacimiento");
            this.LocNacimiento = Resultado.getString("locnacimiento");
            this.JurNacimiento = Resultado.getString("jurnacimiento");
            this.PaisNacimiento = Resultado.getString("paisnacimiento");
            this.CoordNacimiento = Resultado.getString("coordnacimiento");
            this.IdLocResidencia = Resultado.getString("idlocresidencia");
            this.LocResidencia = Resultado.getString("locresidencia");
            this.JurResidencia = Resultado.getString("jurresidencia");
            this.PaisResidencia = Resultado.getString("paisresidencia");
            this.CoordResidencia = Resultado.getString("coordresidencia");
            this.IdLocMadre = Resultado.getString("idlocmadre");
            this.LocMadre = Resultado.getString("localidadmadre");
            this.JurMadre = Resultado.getString("jurmadre");
            this.PaisMadre = Resultado.getString("paismadre");
            this.MadrePositiva = Resultado.getString("madrepositiva");
            this.Ocupacion = Resultado.getString("ocupacion");
            this.ObraSocial = Resultado.getString("obrasocial");
            this.IdMotivo = Resultado.getInt("idmotivo");
            this.Motivo = Resultado.getString("motivo");
            this.IdDerivacion = Resultado.getInt("idderivacion");
            this.Derivacion = Resultado.getString("derivacion");
            this.Tratamiento = Resultado.getString("tratamiento");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fechaalta");
            this.Comentarios = Resultado.getString("comentarios");

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param texto - cadena con el texto a buscar
     * @return resultset con los registros hallados
     * Método que recibe como parámetro una cadena de texto
     * y busca en la base la ocurrencia de la misma, retorna
     * un vector con los registros encontrados
     */
    public ResultSet buscaPaciente(String texto){

        // definimos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_personas.protocolo AS protocolo, " +
                   "       diagnostico.v_personas.laboratorio AS laboratorio, " +
                   "       diagnostico.v_personas.id_laboratorio AS id_laboratorio, " +
                   "       CONCAT(diagnostico.v_personas.apellido, ', ', diagnostico.v_personas.nombre) AS nombre, " +
                   "       CONCAT(diagnostico.v_personas.tipo_documento, ' - ', diagnostico.v_personas.documento) AS documento, " +
                   "       diagnostico.v_personas.edad AS edad, " +
                   "       diagnostico.v_personas.localidad_residencia AS localidad, " +
                   "       diagnostico.v_personas.motivo AS motivo, " +
                   "       diagnostico.v_personas.tratamiento AS tratamiento, " +
                   "       diagnostico.v_personas.fecha_alta AS alta " +
                   "FROM diagnostico.v_personas " +
                   "WHERE diagnostico.v_personas.protocolo = '" + texto + "' OR " +
                   "      diagnostico.v_personas.apellido LIKE '%" + texto + "%' OR " +
                   "      diagnostico.v_personas.nombre LIKE '%" + texto + "%' OR " +
                   "      diagnostico.v_personas.documento = '" + texto + "' OR " +
                   "      diagnostico.v_personas.localidad_residencia LIKE '%" + texto + "%' " +
                   "ORDER BY diagnostico.v_personas.apellido, " +
                   "         diagnostico.v_personas.nombre, " +
                   "         diagnostico.v_personas.documento; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de inserción o edición
     * a partir de los valores de las variables de clase,
     * retorna la clave del registro afectado
     */
    public int grabaPaciente(){

        // si está insertando
        if (this.Protocolo == 0){
            this.nuevoPaciente();
        } else {
            this.editaPaciente();
        }

        // retornamos la clave
        return this.Protocolo;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoPaciente(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.personas " +
                   "       (id_laboratorio, " +
                   "        apellido, " +
                   "        nombre, " +
                   "        documento, " +
                   "        tipo_documento, " +
                   "        fecha_nacimiento, " +
                   "        edad, " +
                   "        sexo, " +
                   "        estado_civil, " +
                   "        hijos, " +
                   "        direccion, " +
                   "        telefono, " +
                   "        celular, " +
                   "        compania, " +
                   "        mail, " +
                   "        localidad_nacimiento, " +
                   "        coordenadas_nacimiento, " +
                   "        localidad_residencia, " +
                   "        coordenadas_residencia, " +
                   "        localidad_madre, " +
                   "        madre_positiva, " +
                   "        ocupacion, " +
                   "        obra_social, " +
                   "        motivo, " +
                   "        derivacion, " +
                   "        tratamiento, " +
                   "        usuario, " +
                   "        comentarios) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                   "        ?, ?, ?, ?, ?, ?, ?, ?, ?); ";
        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setInt   (1, this.IdLaboratorio);
            preparedStmt.setString(2, this.Apellido);
            preparedStmt.setString(3, this.Nombre);
            preparedStmt.setString(4, this.Documento);
            preparedStmt.setInt   (5, this.TipoDocumento);
            preparedStmt.setString(6, this.FechaNacimiento);
            preparedStmt.setInt   (7, this.Edad);
            preparedStmt.setInt   (8, this.IdSexo);
            preparedStmt.setInt   (9, this.IdEstado);
            preparedStmt.setInt   (10, this.Hijos);
            preparedStmt.setString(11, this.Direccion);
            preparedStmt.setString(12, this.Telefono);
            preparedStmt.setString(13, this.Celular);
            preparedStmt.setInt   (14, this.IdCompania);
            preparedStmt.setString(15, this.EMail);
            preparedStmt.setString(16, this.IdLocNacimiento);
            preparedStmt.setString(17, this.CoordNacimiento);
            preparedStmt.setString(18, this.IdLocResidencia);
            preparedStmt.setString(19, this.CoordResidencia);
            preparedStmt.setString(20, this.IdLocMadre);
            preparedStmt.setString(21, this.MadrePositiva);
            preparedStmt.setString(22, this.Ocupacion);
            preparedStmt.setString(23, this.ObraSocial);
            preparedStmt.setInt   (24, this.IdMotivo);
            preparedStmt.setInt   (25, this.IdDerivacion);
            preparedStmt.setString(26, this.Tratamiento);
            preparedStmt.setInt   (27, this.IdUsuario);
            preparedStmt.setString(28, this.Comentarios);

            // ejecutamos la consulta
            preparedStmt.execute();

            // obtenemos la id
            this.Protocolo = this.Enlace.UltimoInsertado();

            // actualizamos la historia clínica
            this.nuevaHistoria();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaPaciente(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.personas SET " +
                   "       apellido = ?, " +
                   "       nombre = ?, " +
                   "       documento = ?, " +
                   "       tipo_documento = ?, " +
                   "       fecha_nacimiento = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       edad = ?, " +
                   "       sexo = ?, " +
                   "       estado_civil = ?, " +
                   "       hijos = ?, " +
                   "       direccion = ?, " +
                   "       telefono = ?, " +
                   "       celular = ?, " +
                   "       compania = ?, " +
                   "       mail = ?, " +
                   "       localidad_nacimiento = ?, " +
                   "       coordenadas_nacimiento = ?, " +
                   "       localidad_residencia = ?, " +
                   "       coordenadas_residencia = ?, " +
                   "       localidad_madre = ?, " +
                   "       madre_positiva = ?, " +
                   "       ocupacion = ?, " +
                   "       obra_social = ?, " +
                   "       motivo = ?, " +
                   "       derivacion = ?, " +
                   "       tratamiento = ?, " +
                   "       usuario = ?, " +
                   "       comentarios = ? " +
                   "WHERE diagnostico.personas.protocolo = ?; ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // asignamos los valores
            preparedStmt.setString(1, this.Apellido);
            preparedStmt.setString(2, this.Nombre);
            preparedStmt.setString(3, this.Documento);
            preparedStmt.setInt   (4, this.TipoDocumento);
            preparedStmt.setString(5, this.FechaNacimiento);
            preparedStmt.setInt   (6, this.Edad);
            preparedStmt.setInt   (7, this.IdSexo);
            preparedStmt.setInt   (8, this.IdEstado);
            preparedStmt.setInt   (9, this.Hijos);
            preparedStmt.setString(10, this.Direccion);
            preparedStmt.setString(11, this.Telefono);
            preparedStmt.setString(12, this.Celular);
            preparedStmt.setInt   (13, this.IdCompania);
            preparedStmt.setString(14, this.EMail);
            preparedStmt.setString(15, this.IdLocNacimiento);
            preparedStmt.setString(16, this.CoordNacimiento);
            preparedStmt.setString(17, this.IdLocResidencia);
            preparedStmt.setString(18, this.CoordResidencia);
            preparedStmt.setString(19, this.IdLocMadre);
            preparedStmt.setString(20, this.MadrePositiva);
            preparedStmt.setString(21, this.Ocupacion);
            preparedStmt.setString(22, this.ObraSocial);
            preparedStmt.setInt   (23, this.IdMotivo);
            preparedStmt.setInt   (24, this.IdDerivacion);
            preparedStmt.setString(25, this.Tratamiento);
            preparedStmt.setInt   (26, this.IdUsuario);
            preparedStmt.setString(27, this.Comentarios);
            preparedStmt.setInt   (28, this.Protocolo);

            // ejecutamos la consulta
            preparedStmt.execute();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que actualiza la historia clínica luego de la 
     * inserción
     */
    private void nuevaHistoria(){

        // declaración de variables
        String Consulta;
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.personas SET " +
                   "       historia_clinica = ? " +
                   "WHERE diagnostico.personas.protocolo = ?; ";

        try {

            // asignamos el puntero a la consulta
            preparedStmt = Puntero.prepareStatement(Consulta);

            // componemos la historia
            this.HistoriaClinica = Integer.toString(this.Protocolo) + "-" + Integer.toString(this.IdLaboratorio);

            // asignamos los valores
            preparedStmt.setString(1, this.HistoriaClinica);
            preparedStmt.setInt(2,    this.Protocolo);

            // ejecutamos la consulta
            preparedStmt.execute();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param documento - cadena con el número de documento
     * @return boolean - si encontró o no al paciente
     * Método utilizado en las altas, recibe como parámetro
     * el número de documento del paciente y verifica que
     * no se encuentre declarado
     */
    public boolean existePaciente(String documento){

        // declaramos las variables
        String Consulta;
        boolean Encontrado = false;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT COUNT(diagnostico.v_personas.protocolo) AS registros " +
                   "FROM diagnostico.v_personas " +
                   "WHERE diagnostico.v_personas.documento = '" + documento + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // según el valor
            Resultado.next();
            if (Resultado.getInt("registros") == 0){
                Encontrado = false;
            } else {
                Encontrado = true;
            }

        // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

        // retornamos
        return Encontrado;

    }

}