/*

    Nombre: Paises
    Fecha: 19/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que ofrece los métodos para el control de la tabla 
                 de paises

 */

// definición del paquete
package paises;

//importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 *
 */
public class Paises {

	// definición de las variables
    protected Conexion Enlace;
    protected String Nombre;                  // nombre del país
    protected int IdPais;                     // clave del país
    protected String Usuario;                 // nombre del usuario
    protected String FechaAlta;               // fecha de alta del registro

	// constructor de la clase
	public Paises(){
	
		// instanciamos la conexión
		this.Enlace = new Conexion();
	
		// inicializamos las variables
		this.initPaises();
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa las variables de clase
	 */
	protected void initPaises(){
		
		// inicializamos las variables
		this.IdPais = 0;
        this.Nombre = "";
        this.FechaAlta = "";

	}
	
	// métodos de asignación de valores
	public void setIdPais(int idpais){
		this.IdPais = idpais;
	}
	public void setNombre(String nombre){
		this.Nombre = nombre;
	}
    
    // métodos de retorno de valores
    public int getIdPais(){
        return this.IdPais;
    }
    public String getPais(){
        return this.Nombre;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return array con la nómina de países
     * Método que retorna un vector con la nómina de países de la base
     * de datos de calidad
     */
    public ResultSet nominaPaises(){
        
        // declaración de variables
        String Consulta;
        ResultSet listaPaises;
        
        // componemos la consulta
        Consulta = "SELECT diccionarios.paises.id AS id, " 
                + " diccionarios.paises.nombre AS pais " 
                + " FROM diccionarios.paises "
                + " ORDER BY diccionarios.paises.nombre;";
        
        // obtenemos el vector
        listaPaises = this.Enlace.Consultar(Consulta);
        
        // retornamos el vector
        return listaPaises;

    }

    /** 
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param pais string con el nombre de un país
     * @return la clave de ese país
     * Método que recibe el nombre de un país y retorna la clave 
     * del mismo
     */
    public int getClavePais(String pais){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int clavePais = 0;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.v_jurisdicciones.idpais AS clave_pais "
                + " FROM diagnostico.v_jurisdicciones "
                + " WHERE diagnostico.v_jurisdicciones.pais = '" + pais + "';";
        
        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            clavePais = Resultado.getInt("clave_pais");
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos la clave
        return clavePais;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave entero con la clave del país
     * @return string nombre del pais
     * Método que recibe la clave de un país y retorna el 
     * nombre del mismo
     */
    public String getNombrePais(int clave) {

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        String nombre = "";

        // componemos la consulta
        Consulta = "SELECT diccionarios.paises.nombre AS nombre " + 
                   "FROM diccionarios.paises " +
                   " WHERE diccionarios.paises.id = '" + clave + "';";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            nombre = Resultado.getString("nombre");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos la clave
        return nombre;

    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición o inserción 
     * según corresponda
     */
    public int grabaPais(){
    
    	// si está insertando
    	if (this.IdPais == 0){
    		this.nuevoPais();
    	} else {
    		this.editaPais();
    	}
    	
    	// retornamos la clave 
    	return this.IdPais;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción en la base
     */
    protected void nuevoPais(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "INSERT INTO diccionarios.paises "
    			+ "        (nombre,"
    			+ "         usuario) "
    			+ "        VALUES "
    			+ "        (?,?)";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Nombre);
	        preparedStmt.setInt    (2, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	        // obtenemos la clave del registro
	        this.IdPais = this.Enlace.UltimoInsertado();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición en la base
     */
    protected void editaPais(){
    	
    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "UPDATE diccionarios.paises SET "
    			+ "        nombre = ?, "
    			+ "        usuario = ? "
    			+ " WHERE diccionarios.paises.id = ?; ";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Nombre);
	        preparedStmt.setInt    (2, Seguridad.Id);
	        preparedStmt.setInt    (3, this.IdPais);

	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param pais - nombre del país
     * @return boolean - si puede insertar
     * Método que verifica si el país ya se encuentra declarado 
     * o si puede insertar (en cuyo caso retorna true)
     */
    public boolean validaPais(String pais){
    	
        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;
    
        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.v_paises.id) AS registros "
                 + "FROM diccionarios.v_paises "
                 + "WHERE diccionarios.v_paises.pais = '" + pais + "'; ";
        
        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }
        
        // retornamos el estado
        return Correcto;
        
    }
 
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * @return boolean - resultado de la operación
     * Método que verifica si un registro tiene hijos y puede 
     * ser eliminado
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.provincias.pais) AS registros " + 
                   "FROM diccionarios.provincias " +
                   "WHERE diccionarios.provincias.pais = '" + clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

            // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe la clave de un país y ejecuta la 
     * consulta de eliminación
     */
    public void borraPais(int clave){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diccionarios.paises " +
                   "WHERE diccionarios.paises.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}