/*

    Nombre: FormPaises
    Fecha: 27/10//2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa el formulario del ABM de
                 paises

 */

// definición del paquete
package paises;

// inclusión de librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormPaises extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -2258284137886873880L;

	// declaración de variables
	private JTextField tId;
	private JTextField tPais;
	private JTable tListado;
	private Paises Naciones;

	// constructor de la clase
	public FormPaises(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// cierra el formulario
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		// fijamos las propiedades del formulario
		this.setBounds(100, 100, 380, 350);
		this.getContentPane().setLayout(null);
		this.setTitle("Diccionario de Países");

		// inicializamos el objeto
		this.Naciones = new Paises();

		// instanciamos los componenetes
		this.initFormPaises();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que instancia el formulario
	 */
	@SuppressWarnings({ "serial" })
	protected void initFormPaises(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Diccionario de Países");
		lTitulo.setBounds(10, 10, 286, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presenta la id de la compañía
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 38, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// presenta el nombre del país
		this.tPais = new JTextField();
		this.tPais.setBounds(62, 45, 253, 26);
		this.tPais.setFont(Fuente);
		this.tPais.setToolTipText("Nombre del País");
		getContentPane().add(tPais);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setToolTipText("Graba el registro en la base");
		btnGrabar.setBounds(325, 45, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaPais();
			}
		});
		getContentPane().add(btnGrabar);

		// define la tabla con la nómina
		this.tListado = new JTable();
		this.tListado.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"ID", "Pais", "Ed.", "El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, 
				String.class, 
				Object.class, 
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// fijamos el ancho de las columnas
        this.tListado.getColumn("ID").setPreferredWidth(30);
        this.tListado.getColumn("ID").setMaxWidth(30);
        this.tListado.getColumn("Ed.").setPreferredWidth(35);
        this.tListado.getColumn("Ed.").setMaxWidth(35);
        this.tListado.getColumn("El.").setPreferredWidth(35);
        this.tListado.getColumn("El.").setMaxWidth(35);

		// establecemos el tooltip
		this.tListado.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tListado.setRowHeight(25);

		// establecemos la fuente
		this.tListado.setFont(Fuente);
		
		// fijamos el evento click
		this.tListado.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tListadoMouseClicked(evt);
			}
		});

		// agregamos la tabla al scroll
		JScrollPane scrollPaises = new JScrollPane();
		scrollPaises.setBounds(10, 80, 356, 225);
		scrollPaises.setViewportView(this.tListado);
		getContentPane().add(scrollPaises);

		// carga la grilla con las compañías
		this.cargaPaises();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla con la nómina de compañías
	 */
	protected void cargaPaises(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Naciones.nominaPaises();

		// sobrecargamos el renderer de la tabla
		this.tListado.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tListado.getModel();

		// hacemos la tabla se pueda ordenar
		tListado.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[4];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("pais");
				fila[2] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica los
	 * datos y ejecuta la consulta
	 */
	protected void grabaPais(){

		// verifica se halla ingresado un nombre
		if (this.tPais.getText().isEmpty()) {

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre del país", "Error", JOptionPane.ERROR_MESSAGE);
			return;

        // si ingresó 
		} else {

            // fija los valores en la clase
            this.Naciones.setNombre(this.tPais.getText());

        }

		// si está dando un alta verifica que no esté repetido
		if (this.tId.getText().isEmpty()) {

			if (!this.Naciones.validaPais(this.tPais.getText())) {

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Ese país ya está declarado", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;

			// lo marca como un alta
			} else {

				// setea el valor
				this.Naciones.setIdPais(0);;

			}

		// si está editando
		} else {

			// almacena la clave
			this.Naciones.setIdPais(Integer.parseInt(this.tId.getText()));

		}

		// graba el registro
		this.Naciones.grabaPais();

		// limpia el formulario
		this.limpiaPais();

		// recarga la grilla
		this.cargaPaises();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre el ícono de edición
	 * que obtiene los datos del registro y los presenta
	 */
	protected void getPais(int clave){

		// obtenemos los datos y los asignamos
		this.tId.setText(Integer.toString(clave));
		this.tPais.setText(Naciones.getNombrePais(clave));

		// fijamos el foco
		this.tPais.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método que pide confirmación y verifica que pueda eliminar
	 * el registro y lo elimina
	 */
	protected void borraPais(int clave){

		// verifica si puede eliminar
		if (!this.Naciones.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa país tiene registros asignados", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?",
   				        "Diccionario de Países", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Naciones.borraPais(clave);

			// limpiamos el formulario por las dudas
			this.limpiaPais();
			
			// recargamos la grilla
			this.cargaPaises();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia los campos de edición
	 */
	protected void limpiaPais(){

		// limpiamos los campos
		this.tId.setText("");
		this.tPais.setText("");

		// fijamos el foco
		this.tPais.requestFocus();

	}

	// método llamado al pulsar sobre la grilla
	private void tListadoMouseClicked(MouseEvent evt) {

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tListado.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tListado.rowAtPoint(evt.getPoint());
		int columna = this.tListado.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tListado.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 2) {

				// cargamos el registro
				this.getPais(clave);

			// si pulsó en eliminar
			} else if (columna == 3) {

				// eliminamos
				this.borraPais(clave);

			}

		}

	}

}
