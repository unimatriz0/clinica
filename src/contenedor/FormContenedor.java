/*

    Nombre: FormContenedor
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que arma la pantalla principal con el menú y el
                 tabulador

 */

// definición del paquete
package contenedor;

// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import laboratorios.FormLaboratorios;
import localidades.FormLocalidades;
import marcas.FormMarcas;
import motivos.FormMotivos;
import organos.FormOrganos;
import pacientes.FormPacientes;
import paises.FormPaises;
import provincias.FormProvincias;
import seguridad.Seguridad;
import stock.FormStock;
import tecnicas.FormTecnicas;
import usuarios.FormUsuarios;
import valores.FormValores;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.JPopupMenu.Separator;
import adversos.FormAdversos;
import antecedentes.FormAntecedentes;
import celulares.FormCelulares;
import dbApi.FormConfig;
import dependencias.FormDependencias;
import derivacion.FormDerivacion;
import documentos.formDocumentos;
import drogas.FormDrogas;
import enfermedades.FormEnfermedades;
import estados.FormEstados;
import financiamiento.FormFinanciamiento;
import heladeras.FormHeladeras;
import java.awt.Font;

// definición de la clase
public class FormContenedor extends JFrame {

	// agregamos el serial id
	private static final long serialVersionUID = 9178063416534321878L;

	// declaramos las variables
	private JTabbedPane tabDatos;
    private FormLaboratorios tLaboratorios;
    private FormPacientes tPacientes;
    private FormAntecedentes tAntecedentes;
    private FormUsuarios tResponsables;
    private FormStock tInventario;

    /**
     * Constructor de la clase
     */
    public FormContenedor () {

        // establecemos las propiedades del formulario
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 20, 950, 700);
        this.setTitle("Sistema de Laboratorio y Seguimiento");
        this.setLayout(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/Graficos/logo_fatala.png")).getImage());

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // definimos el tabulador y lo posicionamos
        this.tabDatos = new JTabbedPane();
        this.tabDatos.setBounds(5,30,930,590);
        this.tabDatos.setFont(Fuente);

        // agregamos el tabulador
        getContentPane().add(this.tabDatos);

        // instanciamos el formulario de pacientes
        this.tPacientes = new FormPacientes(this);

        // agregamos el panel de datos de los pacientes
        this.tabDatos.addTab("Pacientes",
                      new javax.swing.ImageIcon(getClass().getResource("/Graficos/mPacientes.png")),
                      this.tPacientes,
                      "Datos personales de los pacientes");

        // instanciamos el formulario de antecedentes
        this.tAntecedentes = new FormAntecedentes(this);

        // agregamos el panel de antecedentes
        this.tabDatos.addTab("Antecedentes",
                      new javax.swing.ImageIcon(getClass().getResource("/Graficos/mnurse.png")),
                      this.tAntecedentes,
                      "Antecedentes Tóxicos y Patológicos");

        // seteamos el formulario de antecedentes como objeto
        // de pacientes
        this.tPacientes.Antecedentes = this.tAntecedentes;

        // instanciamos el formulario de laboratorios
        this.tLaboratorios = new FormLaboratorios();

        // agregamos el panel al tabulador
        this.tabDatos.addTab("Laboratorios",
                      new javax.swing.ImageIcon(getClass().getResource("/Graficos/mblood.png")),
                      this.tLaboratorios,
                      "Datos de los Laboratorios");

        // agregamos el panel de instituciones

        // instanciamos el formulario de stock
        this.tInventario = new FormStock(this);

        // agregamos el panel al tabulador
        this.tabDatos.addTab("Stock",
                      new javax.swing.ImageIcon(getClass().getResource("/Graficos/minventario.png")),
                      this.tInventario,
                      "Control de Stock e Inventario");

        // instanciamos el formulario
        this.tResponsables = new FormUsuarios(this);

        // agregamos el panel al tabulador
        this.tabDatos.addTab("Usuarios",
                      new javax.swing.ImageIcon(getClass().getResource("/Graficos/m54.png")),
                      this.tResponsables,
                      "Datos de los Responsables");

        // define la barra de menu
        JMenuBar barraMenu = new JMenuBar();

        // el primer elemento es el menú de pacientes
        JMenu mPacientes = new JMenu("Pacientes");
        mPacientes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mPacientes.png")));

        // si es administrador
        if (Seguridad.Personas.equals("Si")){

            // agregamos el nuevo laboratorio
            JMenuItem mNuevoPaciente = new JMenuItem("Nuevo Paciente");
            mNuevoPaciente.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
            mNuevoPaciente.setToolTipText("Ingresa un nuevo paciente en la base");
            mNuevoPaciente.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    NuevoPaciente();
                }

            });
            mPacientes.add(mNuevoPaciente);

        }

        // agregamos el menú de buscar paciente
        JMenuItem mBuscaPaciente = new JMenuItem("Buscar");
        mBuscaPaciente.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbinoculares.png")));
        mBuscaPaciente.setToolTipText("Busca un paciente en la base");
        mBuscaPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscaPaciente();
            }
        });
        mPacientes.add(mBuscaPaciente);

        // agregamos un separador
        Separator Separador4 = new JPopupMenu.Separator();
        mPacientes.add(Separador4);

        // la opción imprimir historia por estudio
        JMenuItem mImpHistEstudio = new JMenuItem("Imprimir por Estudio");
        mImpHistEstudio.setIcon(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
        mImpHistEstudio.setToolTipText("Imprime la Historia agrupada por Estudio");
        mImpHistEstudio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImpHistoriaEstudio();
            }
        });
        mPacientes.add(mImpHistEstudio);

        // la opción imprimir historia por visita
        JMenuItem mImpHistVisita = new JMenuItem("Imprimir por Visita");
        mImpHistVisita.setIcon(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
        mImpHistVisita.setToolTipText("Imprime la Historia agrupada por Visita");
        mImpHistVisita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImpHistoriaVisita();
            }
        });
        mPacientes.add(mImpHistVisita);

        // la opción imprimir concentimiento informado
        JMenuItem mConsentimiento = new JMenuItem("Consentimiento Informado");
        mConsentimiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
        mConsentimiento.setToolTipText("Imprime el Consentimiento Informado");
        mConsentimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Consentimiento();
            }
        });
        mPacientes.add(mConsentimiento);

        // agregamos un separador
        Separator Separador = new JPopupMenu.Separator();
        mPacientes.add(Separador);

        // agregamos la opción configurar
        JMenuItem mConfiguracion = new JMenuItem("Configurar");
        mConfiguracion.setIcon(new ImageIcon(getClass().getResource("/Graficos/majustes.png")));
        mConfiguracion.setToolTipText("Configura la apariencia de la aplicación");
        mConfiguracion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Configurar();
            }
        });
        mPacientes.add(mConfiguracion);

        // agregamos un separador
        Separator Separador2 = new JPopupMenu.Separator();
        mPacientes.add(Separador2);

        // el menú salir
        JMenuItem mSalir = new JMenuItem("Salir");
        mSalir.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
        mSalir.setToolTipText("Salir de la Aplicación");
        mSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Salir();
            }
        });
        mPacientes.add(mSalir);

        // agregamos el menú a la barra
        barraMenu.add(mPacientes);

        // agregamos el menú de laboratorios
        JMenu mLaboratorios = new JMenu("Laboratorios");
        mLaboratorios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mblood.png")));

        // si es administrador
        if (Seguridad.Administrador.equals("Si")){

            // agregamos el nuevo laboratorio
            JMenuItem mNuevoLaboratorio = new JMenuItem("Nuevo Laboratorio");
            mNuevoLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
            mNuevoLaboratorio.setToolTipText("Ingresa un nuevo laboratorio en la base");
            mNuevoLaboratorio.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {

                }

            });
            mLaboratorios.add(mNuevoLaboratorio);

        }

        // agregamos el menú de buscar laboratorio
        JMenuItem mBuscaLaboratorio = new JMenuItem("Buscar");
        mBuscaLaboratorio.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbinoculares.png")));
        mBuscaLaboratorio.setToolTipText("Busca un laboratorio en la base");
        mBuscaLaboratorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            }
        });
        mLaboratorios.add(mBuscaLaboratorio);

        // agregamos el menú a la barra
        barraMenu.add(mLaboratorios);

        // define el menu de usuarios
        JMenu mUsuarios = new JMenu("Usuarios");
        mUsuarios.setIcon(new ImageIcon(getClass().getResource("/Graficos/musuarios.png")));

        // si es administrador
        if (Seguridad.Administrador.equals("Si")){

            // el elemento nuevo usuario
            JMenuItem mNuevoUsuario = new JMenuItem("Nuevo Usuario");
            mNuevoUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
            mNuevoUsuario.setToolTipText("Ingresa un nuevo usuario en la base");
            mNuevoUsuario.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    NuevoUsuario();
                }

            });
            mUsuarios.add(mNuevoUsuario);

        }

        // el elemento buscar usuario
        JMenuItem mBuscaUsuario = new JMenuItem("Buscar");
        mBuscaUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbinoculares.png")));
        mBuscaUsuario.setToolTipText("Busca un usuario en la base");
        mBuscaUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarUsuario();
            }
        });
        mUsuarios.add(mBuscaUsuario);

        // agregamos el menú a la barra
        barraMenu.add(mUsuarios);

        // agregamos el menú de diccionarios de datos
        JMenu mDiccionarios = new JMenu("Diccionarios");
        mDiccionarios.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbook.png")));

        // agregamos el menú de fuentes de financiamiento
        JMenuItem mFinanciamiento = new JMenuItem("Financiamiento");
        mFinanciamiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbillete.png")));
        mFinanciamiento.setToolTipText("Fuentes de Financiamiento");
        mFinanciamiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Financiamiento();
            }
        });
        mDiccionarios.add(mFinanciamiento);

        // agregamos el menú de heladeras
        JMenuItem mHeladeras = new JMenuItem("Heladeras");
        mHeladeras.setIcon(new ImageIcon(getClass().getResource("/Graficos/mheladera.png")));
        mHeladeras.setToolTipText("Heladeras y Freezers");
        mHeladeras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Heladeras();
            }
        });
        mDiccionarios.add(mHeladeras);

        // agregamos el menú de técnicas
        JMenuItem mTecnicas = new JMenuItem("Técnicas");
        mTecnicas.setIcon(new ImageIcon(getClass().getResource("/Graficos/mquimica.png")));
        mTecnicas.setToolTipText("Técnicas diagnósticas utilizadas");
        mTecnicas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Tecnicas();
            }
        });
        mDiccionarios.add(mTecnicas);

        // agregamos el menú de marcas
        JMenuItem mMarcas = new JMenuItem("Marcas");
        mMarcas.setIcon(new ImageIcon(getClass().getResource("/Graficos/minventario.png")));
        mMarcas.setToolTipText("Marcas de reactivos utilizadas");
        mMarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Marcas();
            }
        });
        mDiccionarios.add(mMarcas);

        // agregamos el menú de valores
        JMenuItem mValores = new JMenuItem("Valores");
        mValores.setIcon(new ImageIcon(getClass().getResource("/Graficos/mchecklist.png")));
        mValores.setToolTipText("Valores aceptados para cada técnica");
        mValores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Valores();
            }
        });
        mDiccionarios.add(mValores);

        // agregamos un separador
        Separator Separador0 = new JPopupMenu.Separator();
        mDiccionarios.add(Separador0);

        // agregamos el menú de efectos adversos
        JMenuItem mAdversos = new JMenuItem("Efectos Adversos");
        mAdversos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mCapsule.png")));
        mAdversos.setToolTipText("Efectos adversos de la medicación");
        mAdversos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Adversos();
            }
        });
        mDiccionarios.add(mAdversos);

        // agregamos el item de fuentes de derivación
        JMenuItem mDerivacion = new JMenuItem("Derivaciones");
        mDerivacion.setIcon(new ImageIcon(getClass().getResource("/Graficos/mDerivacion.png")));
        mDerivacion.setToolTipText("Tipos de Derivación");
        mDerivacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Derivacion();
            }
        });
        mDiccionarios.add(mDerivacion);

        // agregamos el item de drogas utilizadas
        JMenuItem mDrogas = new JMenuItem("Drogas");
        mDrogas.setIcon(new ImageIcon(getClass().getResource("/Graficos/mSyringe.png")));
        mDrogas.setToolTipText("Drogas utilizadas en el tratamiento");
        mDrogas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Drogas();
            }
        });
        mDiccionarios.add(mDrogas);

        // agregamos el item de enfermedades
        JMenuItem mEnfermedades = new JMenuItem("Enfermedades");
        mEnfermedades.setIcon(new ImageIcon(getClass().getResource("/Graficos/mHospital.png")));
        mEnfermedades.setToolTipText("Enfermedades preexistentes");
        mEnfermedades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Enfermedades();
            }
        });
        mDiccionarios.add(mEnfermedades);

        // agregamos el de motivos de consulta
        JMenuItem mMotivos = new JMenuItem("Motivos de Consulta");
        mMotivos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mnurse.png")));
        mMotivos.setToolTipText("Nómina de motivos de consulta");
        mMotivos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Motivos();
            }
        });
        mDiccionarios.add(mMotivos);

        // el item de órganos de transplante
        JMenuItem mOrganos = new JMenuItem("Organos de Transplante");
        mOrganos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mtransplante.png")));
        mOrganos.setToolTipText("Nómina de organos transplantados");
        mOrganos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Organos();
            }
        });
        mDiccionarios.add(mOrganos);

        // agregamos un separador
        Separator Separador1 = new JPopupMenu.Separator();
        mDiccionarios.add(Separador1);

        // agregamos el item de tipos de documento
        JMenuItem mDocumentos = new JMenuItem("Documentos");
        mDocumentos.setIcon(new ImageIcon(getClass().getResource("/Graficos/Vistageneral.png")));
        mDocumentos.setToolTipText("Tipos de Documentos");
        mDocumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Documentos();
            }
        });
        mDiccionarios.add(mDocumentos);

        // agregamos el item de celulares
        JMenuItem mCelulares = new JMenuItem("Celulares");
        mCelulares.setIcon(new ImageIcon(getClass().getResource("/Graficos/mTelefono.png")));
        mCelulares.setToolTipText("Compañias de telefonía celular");
        mCelulares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Celulares();
            }
        });
        mDiccionarios.add(mCelulares);

        // agregamos el item de estados civiles
        JMenuItem mEstados = new JMenuItem("Estados Civiles");
        mEstados.setIcon(new ImageIcon(getClass().getResource("/Graficos/mcalendario.png")));
        mEstados.setToolTipText("Nómina de estados civiles");
        mEstados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Estados();
            }
        });
        mDiccionarios.add(mEstados);

        // agregamos un separador
        Separator Separador3 = new JPopupMenu.Separator();
        mDiccionarios.add(Separador3);

        // agregamos el item de dependencias
        JMenuItem mDependencias = new JMenuItem("Dependencias");
        mDependencias.setIcon(new ImageIcon(getClass().getResource("/Graficos/mcomprobado.png")));
        mDependencias.setToolTipText("Dependencias de las Instituciones");
        mDependencias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Dependencias();
            }
        });
        mDiccionarios.add(mDependencias);

        // el item de localidades
        JMenuItem mLocalidades = new JMenuItem("Localidades Censales");
        mLocalidades.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador-de-posicion.png")));
        mLocalidades.setToolTipText("Nómina de Localidades por Jurisdicción");
        mLocalidades.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Localidades();
            }
        });
        mDiccionarios.add(mLocalidades);

        // el item de jurisdicciones
        JMenuItem mJurisdicciones = new JMenuItem("Jurisdicciones");
        mJurisdicciones.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador-de-posicion.png")));
        mJurisdicciones.setToolTipText("Nómina de Jurisdicciones por País");
        mJurisdicciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Jurisdicciones();
            }
        });
        mDiccionarios.add(mJurisdicciones);

        // el item de países
        JMenuItem mPaises = new JMenuItem("Países");
        mPaises.setIcon(new ImageIcon(getClass().getResource("/Graficos/mubicacion.png")));
        mPaises.setToolTipText("Nómina de Países o Nacionalidades");
        mPaises.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Paises();
            }
        });
        mDiccionarios.add(mPaises);

        // agregamos el menú de diccionarios
        barraMenu.add(mDiccionarios);

        // agregamos el menú
        this.setJMenuBar(barraMenu);

        // definimos la barra de herramientas
        JToolBar Herramientas = new JToolBar();
        Herramientas.setRollover(true);
        Herramientas.setFont(Fuente);

        // el boton nuevo usuario
        JButton btnNuevoUsuario = new JButton();
        btnNuevoUsuario.setIcon(new ImageIcon(getClass().getResource("/Graficos/usuarios.png")));
        btnNuevoUsuario.setToolTipText("Agrega un nuevo usuario");
        btnNuevoUsuario.setFocusable(false);
        Herramientas.add(btnNuevoUsuario);

        // fijamos el tamaño, la posición y agregamos la barra al formulario
        Herramientas.setBounds(0, 0, 940, 30);
        this.add(Herramientas);

        // mostramos el formulario
        this.setVisible(true);

    }

    // método llamado al pulsar la configuración
    private void Configurar() {

    	new FormConfig(this, true);
    }

    // método llamado al pulsar sobre nuevo paciente
    private void NuevoPaciente(){

        // llamamos la rutina
        this.tPacientes.nuevoPaciente();

        // fijamos el foco en el primer tabulador
        this.tabDatos.setSelectedIndex(0);

    }

    // método llamado al pulsar sobre el botón buscar paciente
    private void BuscaPaciente(){

        // fijamos el foco en el primer tabulador
        this.tabDatos.setSelectedIndex(0);
    	
        // instanciamos la rutina
        this.tPacientes.buscaPaciente();

    }

    // método llamado al pulsar sobre buscar usuario
    private void BuscarUsuario(){

        // llamamos la rutina
        this.tResponsables.buscaUsuario();

    }

    // método llamado al pulsar sobre nuevo usuario
    private void NuevoUsuario(){

        // llamamos la rutina
        this.tResponsables.limpiaFormulario();

    }

    // método llamado al pulsar sobre las dependencias
    private void Dependencias() {

        // instanciamos el formulario
        new FormDependencias(this, true);

    }

    // método llamado al pulser sobre los tipos de documento
    private void Documentos() {

        // instanciamos el formulario
        new formDocumentos(this, true);

    }

    // método llamado al pulsar sobre efectos adversos
    private void Adversos(){

        // instanciamos el formulario
        new FormAdversos(this, true);

    }

    // método llamado al pulsar sobre estados civiles
    private void Estados(){

        // instanciamos el formulario
        new FormEstados(this, true);

    }

    // método llamado al pulsar sobre las enfermedades
    private void Enfermedades(){

        // instanciamos el formulario
        new FormEnfermedades(this, true);

    }

    // método llamado al pulsar sobre los órganos
    private void Organos(){

        // instanciamos el formulario
        new FormOrganos(this, true);

    }

    // método llamado al pulsar sobre motivos de consulta
    private void Motivos(){

        // instanciamos el formulario
        new FormMotivos(this, true);

    }

    // método llamado al pulsar sobre las derivaciones
    private void Derivacion(){

        // instanciamos el formulario
        new FormDerivacion(this, true);

    }

    // método llamado al pulsar sobre las drogas
    private void Drogas(){

        // instanciamos el formulario
        new FormDrogas(this, true);

    }

    // método llamado al pulsar sobre celulares
    private void Celulares(){

        // instanciamos el formulario
        new FormCelulares(this, true);

    }

    // método llamado al pulsar sobre localidades
    private void Localidades(){

        // instanciamos el formulario
        new FormLocalidades(this, true);

    }

    // método llamado al pulsar sobre jurisdicciones
    private void Jurisdicciones(){

        // instanciamos el formulario
        new FormProvincias(this, true);

    }

    // método llamado al pulsar sobre los países
    private void Paises(){

        // instanciamos el formulario
        new FormPaises(this, true);

    }

    // método llamado al pulsar el botón salir
    private void Salir(){

        // directamente eliminamos el formulario
        this.dispose();

    }

    // método llamado al pulsar el botón financiamiento
    private void Financiamiento(){

        // instanciamos el formulario
        new FormFinanciamiento(this, true);

    }

    // método llamado al pulsar sobre el botón heladeras
    private void Heladeras(){

        // instanciamos el formulario
        new FormHeladeras(this, true);

    }

    // método llamado al pulsar sobre las técnicas
    private void Tecnicas(){

        // instanciamos el formulario
        new FormTecnicas(this, true);

    }

    // método llamado al pulsar sobre las marcas
    private void Marcas(){

        // instanciamos el formulario
        new FormMarcas(this, true);

    }

    // método llamado al pulsar sobre valores
    private void Valores(){

        // instanciamos el formulario
        new FormValores(this, true);

    }

    // método que imprime la historia clínica
    // agrupada por estudio
    private void ImpHistoriaEstudio(){

    }

    // método que imprime la historia clínica
    // agrupada por visita
    private void ImpHistoriaVisita(){

    }

    // método que imprime el consentimiento informado
    private void Consentimiento(){

    }

}
