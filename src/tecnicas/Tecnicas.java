/*

    Nombre: tecnicas
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: métodos que controlan el abm y listados de la tabla de
                 técnicas diagnósticas

 */

// declaración del paquete
package tecnicas;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Lic. Claudio Invernizzi
 * Declaraciòn de la clase
 */
public class Tecnicas {

    // declaración de las variables de clase
    protected int IdTecnica;                // clave del registro
    protected String Tecnica;               // nombre de la técnica
    protected String Nombre;                // nombre completo de la técnica
    protected String Usuario;               // nombre del usuario
    protected String FechaAlta;             // fecha de alta del registro

    // definición de variables
    protected Conexion Enlace;

    /**
     * Constructor de la clase
     */
    public Tecnicas(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initTecnicas();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initTecnicas(){

        // inicializamos las variables de clase
        this.IdTecnica = 0;
        this.Tecnica = "";
        this.Nombre = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setIdTecnica(int idtecnica){
        this.IdTecnica = idtecnica;
    }
    public void setTecnica(String tecnica){
        this.Tecnica = tecnica;
    }
    public void setNombre(String nombre){
        this.Nombre = nombre;
    }

    // método de retorno de valores
    public int getIdTecnica(){
        return this.IdTecnica;
    }
    public String getTecnica(){
        return this.Tecnica;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @return vector con la lista de técnicas
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que retorna la id y el nombre de cada una de las
     * técnicas, utilizado en los combos y en las grillas de
     * abm de técnicas
     */
    public ResultSet nominaTecnicas(){

        // declaración de variables
        String Consulta;
        ResultSet listaTecnicas;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_tecnicas.tecnica AS tecnica, "
                 + "       diagnostico.v_tecnicas.id_tecnica AS id_tecnica,"
                 + "       diagnostico.v_tecnicas.nombre AS nombre_tecnica "
                 + "FROM diagnostico.v_tecnicas;";

        // ejecutamos la consulta y retornamos el vector
        listaTecnicas = this.Enlace.Consultar(Consulta);
        return listaTecnicas;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tecnica - cadena con el nombre de la técnica
     * @return la clave de la técnica
     * Método que recibe como parámetro la cadena con el
     * nombre de la técnica y retorna su clave
     */
    public int getClaveTecnica(String tecnica){

        // definimos las variables
        String Consulta;
        ResultSet Resultado;
        int claveTecnica = 0;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_tecnicas.id_tecnica AS idtecnica "
                 + "FROM diagnostico.v_tecnicas "
                 + "WHERE diagnostico.v_tecnicas.tecnica = '" + tecnica + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            claveTecnica = Resultado.getInt("idtecnica");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos la clave
        return claveTecnica;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la id del registro afectado
     * Método que graba los datos del registro en la base de datos
     */
    public int grabaTecnica(){

        // si es un alta
        if (this.IdTecnica == 0) {
            this.nuevaTecnica();
        } else {
            this.editaTecnica();
        }

        // retorna la clave
        return this.IdTecnica;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido que inserta un nuevo registro
     */
    protected void nuevaTecnica(){

        // declaración de variables
        String Consulta = "";
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de inserción
        Consulta = "INSERT INTO diagnostico.tecnicas "
                 + "       (tecnica, "
                 + "        nombre, "
                 + "        id_usuario) "
                 + "       VALUES "
                 + "       (?, ?, ?)";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Tecnica);
	        preparedStmt.setString (2, this.Nombre);
	        preparedStmt.setInt    (3, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	        // obtenemos la id del registro
	        this.IdTecnica = this.Enlace.UltimoInsertado();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo protegido que edita el registro de tècnicas
     */
    protected void editaTecnica(){

        // declaración de variables
        String Consulta = "";
        PreparedStatement preparedStmt;
        Connection Puntero = this.Enlace.getConexion();

        // compone la consulta de actualización
        Consulta = "UPDATE diagnostico.tecnicas SET "
                 + "       tecnica = ?, "
                 + "       nombre = ?, "
                 + "       id_usuario = ? "
                 + "WHERE diagnostico.tecnicas.id = ?;";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Tecnica);
	        preparedStmt.setString (2, this.Nombre);
	        preparedStmt.setInt    (3, Seguridad.Id);
	        preparedStmt.setInt    (4, this.IdTecnica);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y asigna en las varables de clase los
     * datos del registro
     */
    public void getDatosTecnica(int clave){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_tecnicas.id_tecnica AS id, "  +
                   "       diagnostico.v_tecnicas.tecnica AS tecnica, " +
                   "       diagnostico.v_tecnicas.nombre AS nombre, " +
                   "       diagnostico.v_tecnicas.fecha AS fecha, " +
                   "       diagnostico.v_tecnicas.usuario AS usuario " +
                   "FROM diagnostico.v_tecnicas " +
                   "WHERE diagnostico.v_tecnicas.id_tecnica = '" +  clave + "'; ";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdTecnica = Resultado.getInt("id");
            this.Tecnica = Resultado.getString("tecnica");
            this.Nombre = Resultado.getString("nombre");
            this.FechaAlta = Resultado.getString("fecha");
            this.Usuario = Resultado.getString("usuario");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param tecnica - nombre de la tècnica a verificar
     * Mètodo pùblico que verifica que una tecnica no se
     * encuentre repetida antes de grabar
     */
    public boolean validaTecnica(String tecnica){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.v_tecnicas.tecnica) AS registros "
                 + "FROM diagnostico.v_tecnicas "
                 + "WHERE diagnostico.v_tecnicas.tecnica = '" + tecnica + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que verifica que la técnica no tenga hijos
     * antes de eliminarla
     */
    public boolean puedeBorrar(int clave){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.resultados.id_resultado) AS registros " +
                   "FROM diagnostico.resultados " +
                   "WHERE diagnostico.resultados.id_tecnica = '" + clave + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

            // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de una técnica
     * y como ya sabemos que no tiene resultados asignados también
     * elimina los registros de la tabla de marcas y valores
     */
    public void borraTecnica(int clave){

        // declaración de variables
        String Consulta;

        // eliminamos la tabla de valores
        Consulta = "DELETE FROM diagnostico.valores_tecnicas " +
                   "WHERE diagnostico.valores_tecnicas.id_tecnica = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

        // eliminamos la tabla de marcas
        Consulta = "DELETE FROM diagnostico.marcas " +
                   "WHERE diagnostico.marcas.tecnica = '" + clave + "'";
        this.Enlace.Ejecutar(Consulta);

        // eliminamos el registro de la tabla de técnicas
        Consulta = "DELETE FROM diagnostico.tecnicas " +
                   "WHERE diagnostico.tecnicas.id = '" + clave + "';";
        this.Enlace.Ejecutar(Consulta);

    }

}
