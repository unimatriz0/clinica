/*

    Nombre: FormTecnicas
    Fecha: 01/11/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario de abm de técnicas diagnósticas

 */

// declaración del paquete
package tecnicas;

// importación de librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

public class FormTecnicas extends JDialog {

	// definimos el serial id y declaramos las variables
	private static final long serialVersionUID = -7531404145638665924L;
	private JTextField tId;
	private JTextField tTecnica;
	private JTextField tDescripcion;
	private JTable tListado;
	private Tecnicas Metodos;

	// creamos el diálogo
	public FormTecnicas(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// cerramos el formulario al cancelar
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		// establecemos las propiedades
		this.setTitle("Técnicas Diagnósticas");
		this.setBounds(100, 100, 550, 350);
		this.getContentPane().setLayout(null);

		// instanciamos la clase
		this.Metodos = new Tecnicas();

		// inicializamos los componentes
		this.initFormTecnicas();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa los componentes del formulario
	 */
	@SuppressWarnings("serial")
	protected void initFormTecnicas(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Técnicas Diagnósticas");
		lTitulo.setBounds(10, 10, 446, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// pide la clave de la técnica
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 44, 26);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setFont(Fuente);
		getContentPane().add(this.tId);

		// pide el nombre de la técnica
		this.tTecnica = new JTextField();
        this.tTecnica.setBounds(66, 45, 120, 26);
		this.tTecnica.setToolTipText("Nombre de la Técnica");
		this.tTecnica.setFont(Fuente);
		getContentPane().add(this.tTecnica);

		// pide la descripción
		this.tDescripcion = new JTextField();
        this.tDescripcion.setBounds(200, 45, 300, 26);
		this.tDescripcion.setToolTipText("Descripción de la Técnica");
		this.tDescripcion.setFont(Fuente);
		getContentPane().add(this.tDescripcion);

		// agregamos el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setToolTipText("Graba el registro en la base");
		btnGrabar.setBounds(510, 45, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaTecnica();
			}
		});
		getContentPane().add(btnGrabar);

		// define la tabla
		this.tListado = new JTable();
		this.tListado.setModel(new DefaultTableModel(
			new Object[][] { { null,
							   null,
							   null,
							   null,
							   null }, },
				new String[] { "ID",
							   "Tecnica",
							   "Descripción",
							   "Ed.",
							   "El." }) 
			{
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] { Integer.class,
												String.class,
												String.class,
												Object.class,
												Object.class };
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// establecemos el ancho de las columnas
		this.tListado.getColumn("ID").setPreferredWidth(30);
		this.tListado.getColumn("ID").setMaxWidth(30);
		this.tListado.getColumn("Tecnica").setPreferredWidth(100);
		this.tListado.getColumn("Tecnica").setMaxWidth(100);
		this.tListado.getColumn("Ed.").setPreferredWidth(35);
		this.tListado.getColumn("Ed.").setMaxWidth(35);
		this.tListado.getColumn("El.").setPreferredWidth(35);
		this.tListado.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tListado.setToolTipText("Pulse para editar / borrar");

		// establecemos la fuente
		this.tListado.setFont(Fuente);

		// fijamos el alto de las filas
		this.tListado.setRowHeight(25);

		// fijamos el evento click
		this.tListado.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tListadoMouseClicked(evt);
			}
		});

		// agregamos la tabla al scroll
		JScrollPane scrollTecnicas = new JScrollPane();
		scrollTecnicas.setBounds(10, 80, 525, 225);
		scrollTecnicas.setViewportView(this.tListado);
		getContentPane().add(scrollTecnicas);

		// cargamos la grilla de tecnicas
		this.cargaTecnicas();

		// mostramos el formulario
		this.setVisible(true);

		// fijamos el foco
        this.tTecnica.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla de técnicas diagnósticasd
	 */
	protected void cargaTecnicas(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Metodos.nominaTecnicas();

		// sobrecargamos el renderer de la tabla
		this.tListado.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tListado.getModel();

		// hacemos la tabla se pueda ordenar
		this.tListado.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[5];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id_tecnica");
				fila[1] = Nomina.getString("tecnica");
				fila[2] = Nomina.getString("nombre_tecnica");
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de técnicas
	 */
	protected void tListadoMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tListado.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tListado.rowAtPoint(evt.getPoint());
		int columna = this.tListado.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tListado.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 3) {

				// cargamos el registro
				this.getTecnica(clave);

				// si pulsó en eliminar
			} else if (columna == 4) {

				// eliminamos
				this.borraTecnica(clave);

			}

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void grabaTecnica(){

		// verifica se halla ingresado la técnica
		if (this.tTecnica.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre de la técnica", "Error",
					JOptionPane.ERROR_MESSAGE);
			this.tTecnica.requestFocus();
			return;

		// si esta correcto
		} else {

			// asigna en la clase
			this.Metodos.setTecnica(this.tTecnica.getText());

		}

		// si está insertando
		if (this.tId.getText().isEmpty()){

			// verifica que no esté repetido
			if (!this.Metodos.validaTecnica(this.tTecnica.getText())){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Esa técnica ya está declarada", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;

			// si está insertando
			} else {

				// lo marca como una inserción
				this.Metodos.setIdTecnica(0);

			}

		// si está editando
		} else {

			// asigna en la clase
			this.Metodos.setIdTecnica(Integer.parseInt(this.tId.getText()));

		}

		// verifica se halla ingresado la abreviatura
		if (this.tDescripcion.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre completo", "Error",
					JOptionPane.ERROR_MESSAGE);
			this.tDescripcion.requestFocus();
			return;

		// si declaró
		} else {

			// asigna en la clase
			this.Metodos.setNombre(this.tDescripcion.getText());

		}

		// graba el registro
		this.Metodos.grabaTecnica();

		// limpia el formulario
		this.limpiaFormulario();

		// recarga la grilla
		this.cargaTecnicas();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre la edición de una
	 * tecnica en la grilla
	 */
	protected void getTecnica(int clave){

		// obtenemos el registro
		this.Metodos.getDatosTecnica(clave);

		// asignamos los valores en el formulario
		this.tId.setText(Integer.toString(this.Metodos.getIdTecnica()));
		this.tTecnica.setText(this.Metodos.getTecnica());
		this.tDescripcion.setText(this.Metodos.getNombre());

		// fijamos el foco
		this.tTecnica.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario luego de grabar
	 */
	protected void limpiaFormulario(){

		// limpiamos los valores
		this.tId.setText("");
		this.tTecnica.setText("");
		this.tDescripcion.setText("");

		// fijamos el foco
		this.tTecnica.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre la eliminación de una
	 * técnica en la grilla, verifica si puede eliminar
	 * y luego pide confirmación
	 */
	protected void borraTecnica(int clave){

		// verifica si puede eliminar
		if (!this.Metodos.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa técnica tiene registros asignados", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?", "Técnicas Diagnósticas", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Metodos.borraTecnica(clave);

			// recargamos la grilla
			this.cargaTecnicas();

		}

	}

}
