/*

    Nombre: FormCelulares
    Fecha: 26/10//2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa el formulario del ABM de
                 celulares

 */

// definición del paquete
package celulares;

// inclusión de librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormCelulares extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -2258284137886873880L;

	// declaración de variables
	private JTextField tId;
	private JTextField tCompania;
	private JTable tListado;
	private Celulares Companias;

	// constructor de la clase
	public FormCelulares(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// cierra el formulario
		this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		// fijamos las propiedades del formulario
		this.setBounds(150, 150, 380, 350);
		this.getContentPane().setLayout(null);
		this.setTitle("Compañías de Celular");

		// inicializamos el objeto
		this.Companias = new Celulares();

		// instanciamos los componenetes
		this.initFormCelulares();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que instancia el formulario
	 */
	@SuppressWarnings("serial")
	protected void initFormCelulares(){

		// definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Compañías de Celulares");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(10, 10, 286, 26);
		getContentPane().add(lTitulo);

		// presenta la id de la compañía
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 38, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del Registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// presenta el nombre de la compania
		this.tCompania = new JTextField();
		this.tCompania.setBounds(62, 45, 253, 26);
		this.tCompania.setFont(Fuente);
		this.tCompania.setToolTipText("Nombre de la Empresa");
		getContentPane().add(tCompania);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setToolTipText("Graba el registro en la base");
		btnGrabar.setBounds(325, 45, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				grabaCompania();
			}
		});
		getContentPane().add(btnGrabar);

		// define la tabla con la nómina
		this.tListado = new JTable();
		this.tListado.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
			},
			new String[] {
				"ID",
				"Empresa",
				"Ed.",
				"El."
			}
		) { @SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Object.class, Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// fijamos el ancho de las columnas
        this.tListado.getColumn("ID").setPreferredWidth(30);
        this.tListado.getColumn("ID").setMaxWidth(30);
        this.tListado.getColumn("Ed.").setPreferredWidth(35);
        this.tListado.getColumn("Ed.").setMaxWidth(35);
        this.tListado.getColumn("El.").setPreferredWidth(35);
        this.tListado.getColumn("El.").setMaxWidth(35);

		// establecemos el tooltip
		this.tListado.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tListado.setRowHeight(25);

		// fijamos el evento click
		this.tListado.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tListadoMouseClicked(evt);
			}
		});

		// fijamos la fuente
		this.tListado.setFont(Fuente);

		// agregamos la tabla al scroll
		JScrollPane scrollCelulares = new JScrollPane();
		scrollCelulares.setBounds(10, 80, 356, 225);
		scrollCelulares.setViewportView(this.tListado);
		getContentPane().add(scrollCelulares);

		// carga la grilla con las compañías
		this.cargaCompanias();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla con la nómina de compañías
	 */
	protected void cargaCompanias(){

		// definimos las variables
		ResultSet Nomina;

		// obtenemos la nómina
		Nomina = this.Companias.nominaCelulares();

		// sobrecargamos el renderer de la tabla
		this.tListado.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tListado.getModel();

		// hacemos la tabla se pueda ordenar
		tListado.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[4];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("compania");
				fila[2] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica los
	 * datos y ejecuta la consulta
	 */
	protected void grabaCompania(){

		// verifica se halla ingresado un nombre
		if (this.tCompania.getText().isEmpty()) {

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Ingrese el nombre de la empresa", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// si está dando un alta verifica que no esté repetido
		if (this.tId.getText().isEmpty()) {

			if (!this.Companias.validaCelular(this.tCompania.getText())) {

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Esa empresa ya está declarada", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;

			// lo marca como un alta
			} else {

				// setea el valor
				this.Companias.setIdCompania(0);

			}

		// si está editando
		} else {

			// almacena la clave
			this.Companias.setIdCompania(Integer.parseInt(this.tId.getText()));

		}

		// fija los valores en la clase
		this.Companias.setCompania(this.tCompania.getText());

		// graba el registro
		this.Companias.grabaCompania();

		// limpia el formulario
		this.limpiaCompania();

		// recarga la grilla
		this.cargaCompanias();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método llamado al pulsar sobre el ícono de edición
	 * que obtiene los datos del registro y los presenta
	 */
	protected void getCompania(int clave){

		// obtenemos los datos y los asignamos
		this.tId.setText(Integer.toString(clave));
		this.tCompania.setText(Companias.getNombreCompania(clave));

		// fijamos el foco
		this.tCompania.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave del registro
	 * Método que pide confirmación y verifica que pueda eliminar
	 * el registro y lo elimina
	 */
	protected void borraCompania(int clave){

		// verifica si puede eliminar
		if (!this.Companias.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa empresa tiene pacientes asignados", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?",
   				        "Empresas de Telefonía", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Companias.borraCompania(clave);

			// recargamos la grilla
			this.cargaCompanias();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia los campos de edición
	 */
	protected void limpiaCompania(){

		// limpiamos los campos
		this.tId.setText("");
		this.tCompania.setText("");

		// fijamos el foco
		this.tCompania.requestFocus();

	}

	// método llamado al pulsar sobre la grilla
	private void tListadoMouseClicked(MouseEvent evt) {

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tListado.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tListado.rowAtPoint(evt.getPoint());
		int columna = this.tListado.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tListado.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 2) {

				// cargamos el registro
				this.getCompania(clave);

			// si pulsó en eliminar
			} else if (columna == 3) {

				// eliminamos
				this.borraCompania(clave);

			}

		}

	}

}
