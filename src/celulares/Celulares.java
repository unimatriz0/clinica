/*

    Nombre: celulares
    Fecha: 19/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que ofrece los métodos para obtener la nómina de las tablas 
                 de celulares

 */

// definición del paquete
package celulares;

//importamos las librerías
import dbApi.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import seguridad.Seguridad;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 *
 */
public class Celulares {

	// declaración de variables
	protected Conexion Enlace;

	// declaración de las variables de clase
	protected int IdCompania;                // clave de la compañía
	protected String Compania;               // nombre de la compañia
	
	// constructor de la clase
	public Celulares(){
		
        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initCelulares();
        
	}

	/**
	 * @autor Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa las variables de clase
	 */
	protected void initCelulares(){
		
        // inicializamos las variables
        this.IdCompania = 0;
        this.Compania = "";
		
	}

	// métodos de asignación de valores
	public void setIdCompania(int idcompania){
		this.IdCompania = idcompania;
	}
	public void setCompania(String compania){
		this.Compania = compania;
	}
    
    // métodos de retorno de valores
    public int getIdCompania(){
        return this.IdCompania;
    }
    public String getCompania(){
        return this.Compania;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clavecompania entero con la clave de la compañia
     * @return nombre de la compañía
     * Método que recibe como parámetro la clave de una compañia de celular
     * y retorna su nombre
     */
    public String getNombreCompania(int clavecompania){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        String compania = "";
        
        // componemos la consulta 
        Consulta = "SELECT diccionarios.celulares.compania AS compania " +
                   "FROM diccionarios.celulares " +
                   "WHERE diccionarios.celulares.id = '" + clavecompania + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);
        

        try {
            
            // obtenemos el registro            
            Resultado.next();
            compania = Resultado.getString("compania");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        // retornamos el nombre
        return compania;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param compania string con el nombre de la compañia
     * @return id de la compania
     * Método que recibe como parámetro el nombre de una compañía y 
     * retorna la clave de la misma
     */
    public int getClaveCompania(String compania){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int clave = 0;
        
        // componemos la consulta 
        Consulta = "SELECT diccionarios.celulares.id AS id_compania " +
                   "FROM diccionarios.celulares " +
                   "WHERE diccionarios.celulares.compania = '" + compania + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);
        

        try {
            
            // obtenemos el registro            
            Resultado.next();
            clave = Resultado.getInt("id_compania");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        
        // retornamos el nombre
        return clave;
        
    }

    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de compañias
     * Método que retorna un resultset con la nómina de compañías celulares
     */
    public ResultSet nominaCelulares(){
        
        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta 
        Consulta = "SELECT diccionarios.celulares.id AS id, " +
                   "       diccionarios.celulares.compania AS compania " +
                   "FROM diccionarios.celulares " +
                   "ORDER BY diccionarios.celulares.compania; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos 
        return Resultado;
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int clave del registro afectado
     * Método que ejecuta según corresponda la consulta de inserción
     * o edición y retorna la id del registro afectado
     */
    public int grabaCompania(){
    	
    	// si está insertando
    	if (this.IdCompania == 0){
    		this.nuevaCompania();
    	} else {
    		this.editaCompania();
    	}
    	
    	// retorna la id
    	return this.IdCompania;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un nuevo registro
     */
    protected void nuevaCompania(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "INSERT INTO diccionarios.celulares "
    			+ "        (compania, "
    			+ "         id_usuario) "
    			+ "        VALUES "
    			+ "        (?,?)";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Compania);
	        preparedStmt.setInt    (2, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	        // obtenemos la id
	        this.IdCompania = this.Enlace.UltimoInsertado();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la edición del registro
     */
    protected void editaCompania(){
    	
    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();
    	
    	// componemos la consulta
    	Consulta = "UPDATE diccionarios.celulares SET "
    			+ "        compania = ?, "
    			+ "        id_usuario = ? "
    			+ " WHERE diccionarios.celulares.id = ?; ";

    	try {
    	
    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);
			
	    	// asignamos los valores
	    	preparedStmt.setString (1, this.Compania);
	        preparedStmt.setInt    (2, Seguridad.Id);
	        preparedStmt.setInt    (3, this.IdCompania);

	        // ejecutamos la consulta
	        preparedStmt.execute();
	        
	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param compania - cadena con el nombre de una compañia de celular
     * @return boolean
     * Método que verifica no se encuentre declarado un nombre de 
     * compañía, el que recibe como parámetro, si se puede insertar
     * retorna verdadero
     */
    public boolean validaCelular(String compania){

        // declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;
    
        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.celulares.id) AS registros "
                 + "FROM diccionarios.celulares "
                 + "WHERE diccionarios.celulares.compania = '" + compania + "'; ";
        
        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {
            
            // obtenemos el registro
            Resultado.next();
            
            // si hay registros
            if (Resultado.getInt("registros") == 0){
                Correcto = true;
            } else {
                Correcto = false;
            }
            
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());
            
        }

        // retornamos el estado
        return Correcto;
    	
    }
  
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idcompania - clave de la companía
     * @return boolean - resultado de la operación
     * Método que recibe como parámetro la clave de la companía 
     * y verifica que esta no tenga hijos
     */
    public boolean puedeBorrar(int idcompania){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        boolean Correcto = true;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros " +
                   "FROM diagnostico.personas " +
                   "WHERE diagnostico.personas.compania = '" + idcompania + "';";

        // ejecutamos la consulta y retornamos la clave
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") == 0) {
                Correcto = true;
            } else {
                Correcto = false;
            }

            // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el estado
        return Correcto;
                   
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idcompania - clave de la compania
     * Método que recibe como parámetro la clave de una consulta 
     * y ejecuta la consulta de eliminación
     */
    public void borraCompania(int idcompania){

        // declaración de variables
        String Consulta;

        // componemos la consulta y la ejecutamos
        Consulta = "DELETE FROM diccionarios.celulares " +
                   "WHERE diccionarios.celulares.id = '" + idcompania + "';";
        this.Enlace.Ejecutar(Consulta);
        
    }

}
