/*

    Nombre: FormTratamiento
    Fecha: 08/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el abm del
	             tratamiento recibido por el paciente

 */

// definición del paquete
package tratamiento;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JSpinner;
import com.toedter.calendar.JDateChooser;
import drogas.Drogas;
import funciones.ComboClave;
import funciones.RendererTabla;

import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import tratamiento.Tratamiento;
import funciones.Utilidades;

// definimos la clase
public class FormTratamiento extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = 1L;

	// definimos las variables de clase
	private JTextField tId;
	private JComboBox<Object> cDroga;
	private JDateChooser dInicio;
	private JDateChooser dFinal;
	private JTextField tComercial;
	private JSpinner sComprimidos;
	private JTable tTratamiento;
	public int Protocolo;
	private Tratamiento Medicamentos;
	private Utilidades Herramientas;

	// constructor de la clase
	public FormTratamiento(java.awt.Frame parent, boolean modal) {

        // fijamos el padre
        super(parent, modal);

		// configuramos el formulario
		this.setBounds(150, 150, 830, 330);
		this.setLayout(null);
		this.setTitle("Tratamientos del Paciente");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// instanciamos las clases
		this.Medicamentos = new Tratamiento();
		this.Herramientas = new Utilidades();

		// inicializamos los componentes
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que configura los elementos del formulario
	 */
	@SuppressWarnings("serial")
	private void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("Tratamientos Recibidos por el Paciente");
		lTitulo.setBounds(10, 10, 550, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// presenta la id
		JLabel lId = new JLabel("ID");
		lId.setBounds(10, 35, 65, 26);
		lId.setFont(Fuente);
		lId.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lId);
		this.tId = new JTextField();
		this.tId.setBounds(10, 60, 65, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);
		this.add(this.tId);

		// pide la droga
		JLabel lDroga = new JLabel("Droga");
		lDroga.setBounds(90, 35, 190, 26);
		lDroga.setFont(Fuente);
		lDroga.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lDroga);
		this.cDroga = new JComboBox<>();
		this.cDroga.setBounds(90, 60, 190, 26);
		this.cDroga.setFont(Fuente);
		this.cDroga.setToolTipText("Seleccione la droga de la lista");
		this.add(this.cDroga);

		// carga las drogas
		this.cargaDrogas();

		// presenta el nombre comercial
		JLabel lComercial = new JLabel("Nombre Comercial");
		lComercial.setBounds(290, 35, 190, 26);
		lComercial.setFont(Fuente);
		lComercial.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lComercial);
		this.tComercial = new JTextField();
		this.tComercial.setBounds(290, 60, 190, 26);
		this.tComercial.setFont(Fuente);
		this.tComercial.setToolTipText("Nombre comercial de la droga");
		this.tComercial.setEditable(false);
		this.add(this.tComercial);

		// pide los comprimidos por día
		JLabel lComprimidos = new JLabel("Comp.");
		lComprimidos.setBounds(490, 35, 50, 26);
		lComprimidos.setFont(Fuente);
		lComprimidos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lComprimidos);
		this.sComprimidos = new JSpinner();
		this.sComprimidos.setBounds(490, 60, 50, 26);
		this.sComprimidos.setFont(Fuente);
		this.sComprimidos.setToolTipText("Indique el número de comprimidos diarios");
		this.add(this.sComprimidos);

		// pide la fecha de inicio
		JLabel lInicio = new JLabel("Inicio");
		lInicio.setBounds(550, 35, 110, 26);
		lInicio.setFont(Fuente);
		lInicio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lInicio);
		this.dInicio = new JDateChooser();
		this.dInicio.setBounds(550, 60, 110, 26);
		this.dInicio.setFont(Fuente);
		this.dInicio.setToolTipText("Indique el inicio del tratamiento");
		this.add(this.dInicio);

		// pide la fecha de finalización
		JLabel lFin = new JLabel("Fin");
		lFin.setBounds(670, 35, 110, 20);
		lFin.setFont(Fuente);
		lFin.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		this.add(lFin);
		this.dFinal = new JDateChooser();
		this.dFinal.setBounds(670, 60, 110, 26);
		this.dFinal.setFont(Fuente);
		this.dFinal.setToolTipText("Indique el fin del tratamiento");
		this.add(this.dFinal);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(785, 60, 26, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaTratamiento();
            }
        });
		this.add(btnGrabar);

		// define el scroll
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 95, 800, 200);
		getContentPane().add(scrollPane);

		// define la tabla
		this.tTratamiento = new JTable();
		this.tTratamiento.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null, null, null},
			},
			new String[] {
				"ID",
				"Droga",
				"Comercial",
				"Comp.",
				"Inicio",
				"Fin",
				"Usuario",
				"Alta",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				Integer.class,
				String.class,
				String.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		scrollPane.setViewportView(this.tTratamiento);

		// fija el ancho de las columnas
        this.tTratamiento.getColumn("ID").setPreferredWidth(50);
        this.tTratamiento.getColumn("ID").setMaxWidth(50);
        this.tTratamiento.getColumn("Comercial").setMaxWidth(150);
		this.tTratamiento.getColumn("Comercial").setPreferredWidth(150);
        this.tTratamiento.getColumn("Comp.").setMaxWidth(50);
        this.tTratamiento.getColumn("Comp.").setPreferredWidth(50);
        this.tTratamiento.getColumn("Inicio").setMaxWidth(70);
        this.tTratamiento.getColumn("Inicio").setPreferredWidth(70);
        this.tTratamiento.getColumn("Fin").setMaxWidth(70);
        this.tTratamiento.getColumn("Fin").setPreferredWidth(70);
        this.tTratamiento.getColumn("Usuario").setMaxWidth(70);
        this.tTratamiento.getColumn("Usuario").setPreferredWidth(70);
        this.tTratamiento.getColumn("Alta").setMaxWidth(70);
        this.tTratamiento.getColumn("Alta").setPreferredWidth(75);
        this.tTratamiento.getColumn("Ed.").setMaxWidth(30);
        this.tTratamiento.getColumn("Ed.").setPreferredWidth(30);
        this.tTratamiento.getColumn("El.").setMaxWidth(30);
        this.tTratamiento.getColumn("El.").setPreferredWidth(30);

		// establece el alto de las filas
		this.tTratamiento.setRowHeight(25);

		// setea la fuente
		this.tTratamiento.setFont(Fuente);

		// fija el tooltip
		this.tTratamiento.setToolTipText("Pulse para editar / eliminar el registro");

        // fijamos el evento click
        this.tTratamiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tTratamientoMouseClicked(evt);
            }
        });

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Mètodo que carga el combo de las drogas
	 */
	private void cargaDrogas(){

		// instanciamos la clase
		Drogas Farmaco = new Drogas();
		ResultSet Nomina = Farmaco.listaDrogas();

		// agregamos el primer elemento
		this.cDroga.addItem(new ComboClave(0, ""));

		// recorremos el vector
		try {
			while (Nomina.next()){
				this.cDroga.addItem(new ComboClave(Nomina.getInt("id"), Nomina.getString("droga")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que a partir de la variable de clase carga la
	 * nómina de tratamientos del paciente
	 */
	public void cargaTratamientos(){

		// obtenemos la nómina
		ResultSet Nomina = this.Medicamentos.nominaTratamiento(this.Protocolo);

        // sobrecargamos el renderer de la tabla
        this.tTratamiento.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tTratamiento.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tTratamiento.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[10];

        try {

			// nos aseguramos de estar en primer registro
			Nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (Nomina.next()){

                // fijamos los valores de la fila
                fila[0] = Nomina.getInt("id");
                fila[1] = Nomina.getString("droga");
                fila[2] = Nomina.getString("comercial");
				fila[3] = Nomina.getInt("comprimidos");
				fila[4] = Nomina.getString("inicio");
				fila[5] = Nomina.getString("fin");
				fila[6] = Nomina.getString("usuario");
				fila[7] = Nomina.getString("alta");
                fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[9] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que valida el formulario antes de enviarlo
	 * al servidor
	 */
	private void validaTratamiento(){

		// si no seleccionó la droga
		ComboClave item = (ComboClave) this.cDroga.getSelectedItem();
		if (item == null || item.getClave() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Seleccione de la lista la droga utilizada",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.cDroga.requestFocus();
			return;

		}

		// si no indicó el número de comprimidos
		if ((Integer) this.sComprimidos.getValue() == 0){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique el número de comprimidos diarios",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.sComprimidos.requestFocus();
			return;

		}

		// si no indicó la fecha de inicio del tratamiento
		if (this.Herramientas.fechaJDate(this.dInicio)  == null){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Indique la fecha de inicio del tratamiento",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			this.dInicio.requestFocus();
			return;

		}

		// la fecha de finalización la permite en blanco

		// grabamos el registro
		this.grabaTratamiento();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que ejecuta la consulta en el servidor
	 */
	private void grabaTratamiento(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Medicamentos.setId(0);
		} else {
			this.Medicamentos.setId(Integer.parseInt(this.tId.getText()));
		}

		// obtenemos la clave de la droga
		ComboClave item = (ComboClave) this.cDroga.getSelectedItem();
		this.Medicamentos.setIdDroga(item.getClave());

		// terminamos de asignar
		this.Medicamentos.setComprimidos((Integer) this.sComprimidos.getValue());
		this.Medicamentos.setInicio(this.Herramientas.fechaJDate(this.dInicio));

		// si ingresó la fecha de finalización
		if (this.Herramientas.fechaJDate(this.dFinal) != null){
			this.Medicamentos.setFin(this.Herramientas.fechaJDate(this.dFinal));
		} else {
			this.Medicamentos.setFin("");
		}

		// grabamos el registro
		this.Medicamentos.grabaTratamiento();

		// recargamos la grilla
		this.cargaTratamientos();

		// limpiamos el formulario
		this.limpiaFormulario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario de datos luego de
	 * grabar o eliminar
	 */
	private void limpiaFormulario(){

		// limpiamos los campos
		this.tId.setText("");
		ComboClave item = new ComboClave(0, "");
		this.cDroga.setSelectedItem(item);
		this.tComercial.setText("");
		this.sComprimidos.setValue(0);
		this.dInicio.setDate(null);
		this.dFinal.setDate(null);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que presenta en el formulario los datos
	 * del tratamiento
	 */
	private void verTratamiento(int clave){

		// obtenemos el registro
		this.Medicamentos.getDatosTratamiento(clave);

		// cargamos en el formulario
		this.tId.setText(Integer.toString(this.Medicamentos.getId()));
		ComboClave item = new ComboClave(this.Medicamentos.getIdDroga(), this.Medicamentos.getDroga());
		this.cDroga.setSelectedItem(item);
		this.tComercial.setText(this.Medicamentos.getComercial());
		this.sComprimidos.setValue(this.Medicamentos.getComprimidos());
		this.dInicio.setDate(this.Herramientas.StringToDate(this.Medicamentos.getInicio()));
		this.dFinal.setDate(this.Herramientas.StringToDate(this.Medicamentos.getFin()));

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - entero con la clave del registro
	 * Método que pide confirmación antes de eliminar el
	 * registro en la base
	 */
	private void eliminaTratamiento(int clave){

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
		                            "Está seguro que desea eliminar el registro?",
									"Tratamientos del Paciente",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Medicamentos.borraTratamiento(clave);

			// limpiamos el formulario
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaTratamientos();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de pacientes
	 */
	private void tTratamientoMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tTratamiento.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tTratamiento.rowAtPoint(evt.getPoint());
        int columna = this.tTratamiento.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tTratamiento.convertRowIndexToModel (fila);

		// obtenemos el protocolo
		int clave = (Integer) modeloTabla.getValueAt(indice, 0);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // si pulsó en editar
            if (columna == 8){

				// editamos el registro
				this.verTratamiento(clave);

			// si pulsó en eliminar
			} else if (columna == 9){

				// eliminamos el registro
				this.eliminaTratamiento(clave);

			}

		}

	}

}
