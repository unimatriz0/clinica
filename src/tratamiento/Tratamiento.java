/*

    Nombre: Tratamiento
    Fecha: 22/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa los métodos para el abm
                 de la tabla de tratamiento

 */

// declaración del paquete
package tratamiento;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Tratamiento {

    // definición de variables
    private Conexion Enlace;        // puntero a la base de datos
    private int Id;                 // clave del registro
    private int Paciente;           // clave del paciente
    private int IdDroga;            // clave de la droga
    private String Droga;           // nombre de la droga
    private String Comercial;       // nombre comercial de la droga
    private int Dosis;              // dosis en miligramos
    private int Comprimidos;        // número de comprimidos al día
    private String Inicio;          // fecha de inicio del tratamiento
    private String Fin;             // fecha de finalización del tratamiento
    private int IdUsuario;          // clave del usuario
    private String Usuario;         // nombre del usuario
    private String Fecha;           // fecha de alta del registro

    // constructor de la clase
    public Tratamiento() {

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Paciente = 0;
        this.IdDroga = 0;
        this.Droga = "";
        this.Comercial = "";
        this.Dosis = 0;
        this.Comprimidos = 0;
        this.Inicio = "";
        this.Fin = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setIdDroga(int iddroga){
        this.IdDroga = iddroga;
    }
    public void setDosis(int dosis){
        this.Dosis = dosis;
    }
    public void setComprimidos(int comprimidos){
        this.Comprimidos = comprimidos;
    }
    public void setInicio(String inicio){
        this.Inicio = inicio;
    }
    public void setFin(String fin){
        this.Fin = fin;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getIdDroga(){
        return this.IdDroga;
    }
    public String getDroga(){
        return this.Droga;
    }
    public int getComprimidos(){
        return this.Comprimidos;
    }
    public String getComercial(){
        return this.Comercial;
    }
    public int getDosis(){
        return this.Dosis;
    }
    public String getInicio(){
        return this.Inicio;
    }
    public String getFin(){
        return this.Fin;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente - clave del paciente
     * @return resultset con los registros
     * Método que retorna la nómina con los tratamientos que
     * ha recibido un paciente
     */
    public ResultSet nominaTratamiento(int idpaciente){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_tratamiento.id AS id, " +
                   "       diagnostico.v_tratamiento.droga AS droga, " +
                   "       diagnostico.v_tratamiento.dosis AS dosis, " +
                   "       diagnostico.v_tratamiento.comercial AS comercial, "+
                   "       diagnostico.v_tratamiento.comprimidos AS comprimidos, " +
                   "       diagnostico.v_tratamiento.inicio AS inicio, " +
                   "       diagnostico.v_tratamiento.fin AS fin, " +
                   "       diagnostico.v_tratamiento.usuario AS usuario, " +
                   "       diagnostico.v_tratamiento.fecha AS alta " +
                   "FROM diagnostico.v_tratamiento " +
                   "WHERE diagnostico.v_tratamiento.paciente = '" + idpaciente + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_tratamiento.inicio, '%d/%m/%Y') DESC; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y asigna los valores del mismo en las
     * variables de clase
     */
    public void getDatosTratamiento(int idtratamiento){

        // declaración de variables
        String Consulta = "";
        ResultSet Resultado = null;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_tratamiento.id AS id, " +
                   "       diagnostico.v_tratamiento.paciente AS paciente, " +
                   "       diagnostico.v_tratamiento.iddroga AS iddroga, " +
                   "       diagnostico.v_tratamiento.droga AS droga, " +
                   "       diagnostico.v_tratamiento.dosis AS dosis, " +
                   "       diagnostico.v_tratamiento.comercial AS comercial, " +
                   "       diagnostico.v_tratamiento.comprimidos AS comprimidos, " +
                   "       diagnostico.v_tratamiento.inicio AS inicio, " +
                   "       diagnostico.v_tratamiento.fin AS fin, " +
                   "       diagnostico.v_tratamiento.usuario AS usuario, " +
                   "       diagnostico.v_tratamiento.fecha AS fecha " +
                   "FROM diagnostico.v_tratamiento " +
                   "WHERE diagnostico.v_tratamiento.id = '" + idtratamiento + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.IdDroga = Resultado.getInt("iddroga");
            this.Droga = Resultado.getString("droga");
            this.Dosis = Resultado.getInt("dosis");
            this.Comercial = Resultado.getString("comercial");
            this.Comprimidos = Resultado.getInt("comprimidos");
            this.Inicio = Resultado.getString("inicio");
            this.Fin = Resultado.getString("fin");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return idtratamiento clave del registro
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda, retorna la clave del registro afectado
     */
    public int grabaTratamiento(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoTratamiento();
        } else {
            this.editaTratamiento();
        }

        // retorna la id
        return this.Id;

    }

     /**
      * @author Claudio Invernizzi <cinvernizzi@gmail.com>
      * Método que ejecuta la consulta de inserción
      */
    protected void nuevoTratamiento(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.tratamiento " +
                   "       (paciente, " +
                   "        droga, " +
                   "        comprimidos, " +
                   "        inicio, " +
                   "        fin, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?. ?, ?, " +
                   "        STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "        STR_TO_DATE(?, '%d/%m/%Y'), ?); ";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.Paciente);
            psInsertar.setInt(2, this.IdDroga);
            psInsertar.setInt(3, this.Comprimidos);
            psInsertar.setString(4, this.Inicio);
            psInsertar.setString(5, this.Fin);
            psInsertar.setInt(6, this.IdUsuario);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

            // obtenemos la id
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaTratamiento(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.tratamiento SET " +
                   "       droga = ?, " +
                   "       comprimidos = ?, " +
                   "       inicio = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       fin = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       usuario = ? " +
                   "WHERE diagnostico.tratamiento.id = ?; ";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdDroga);
            psInsertar.setInt(2, this.Comprimidos);
            psInsertar.setString(3, this.Inicio);
            psInsertar.setString(4, this.Fin);
            psInsertar.setInt(5, this.IdUsuario);
            psInsertar.setInt(6, this.Id);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idtratamiento clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public void borraTratamiento(int idtratamiento){

        // declaración de variables
        String Consulta;

        // compone y ejecuta la consulta
        Consulta = "DELETE FROM diagnostico.tratamiento " +
                   "WHERE diagnostico.tratamiento.id = '" + idtratamiento + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}