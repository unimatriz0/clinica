/*
 * Nombre: FormBuscaLocalidad
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 28/02/2019
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma el formulario con la grilla
 *              de localidades y permite seleccionar la correcta
 *              para luego cargar los datos en el formulario padre
 *              de laboratorios
 */

// definición del paquete
package laboratorios;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import javax.swing.ImageIcon;
import java.awt.event.MouseEvent;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormBuscaLocalidad extends JDialog {

	// agrega el serial id
	private static final long serialVersionUID = -5980739059055697864L;

	// inicializamos las variables
	private JTable tLocalidades;
	private FormLaboratorios Padre;

	// constructor de la clase
	public FormBuscaLocalidad(java.awt.Frame parent, boolean modal, FormLaboratorios padre) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// seteamos el padre
		this.Padre = padre;

		// establecemos las propiedades
		setBounds(100, 100, 593, 292);
		getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Localidades Coincidentes");

		// inicializamos el formulario
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	@SuppressWarnings({ "serial" })
	protected void initForm(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// prsentamos el título
		JLabel lTitulo = new JLabel("Seleccione la Localidad");
		lTitulo.setBounds(10, 5, 313, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// definimos el scroll
		JScrollPane scrollLocalidades = new JScrollPane();
		scrollLocalidades.setBounds(10, 35, 575, 225);

		// definimos la tabla
		this.tLocalidades = new JTable();
		this.tLocalidades.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"Clave",
				"Pais",
				"Jurisdiccion",
				"Localidad",
				"Sel."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				String.class,
				String.class,
				String.class,
				String.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

		// fijamos el tooltip
		this.tLocalidades.setToolTipText("Pulse para seleccionar la localidad");

        // fijamos el ancho de las columnas
        this.tLocalidades.getColumn("Clave").setPreferredWidth(50);
        this.tLocalidades.getColumn("Clave").setMaxWidth(50);
        this.tLocalidades.getColumn("Pais").setMaxWidth(150);
        this.tLocalidades.getColumn("Pais").setPreferredWidth(150);
        this.tLocalidades.getColumn("Jurisdiccion").setMaxWidth(150);
        this.tLocalidades.getColumn("Jurisdiccion").setPreferredWidth(150);
        this.tLocalidades.getColumn("Sel.").setMaxWidth(30);
        this.tLocalidades.getColumn("Sel.").setPreferredWidth(85);

		// fijamos el alto de las filas
		this.tLocalidades.setRowHeight(25);

		// definimos la fuente
		this.tLocalidades.setFont(Fuente);

        // agregamos la tabla al scroll
		scrollLocalidades.setViewportView(this.tLocalidades);
		getContentPane().add(scrollLocalidades);

        // fijamos el evento click
        this.tLocalidades.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tLocalidadesMouseClicked(evt);
            }

        });

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param nomina array con los resultados
	 * Método que recibe como parámetro un resultset y lo
	 * carga en la tabla
	 */
	public void cargaLocalidades(ResultSet nomina){

        // sobrecargamos el renderer de la tabla
        this.tLocalidades.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tLocalidades.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tLocalidades.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[5];

        try {

			// nos aseguramos de estar en primer registro
			nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // fijamos los valores de la fila
                fila[0] = nomina.getString("id_localidad");
                fila[1] = nomina.getString("pais");
                fila[2] = nomina.getString("provincia");
                fila[3] = nomina.getString("localidad");
                fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de localidades
	 */
	private void tLocalidadesMouseClicked(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tLocalidades.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tLocalidades.rowAtPoint(evt.getPoint());
        int columna = this.tLocalidades.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tLocalidades.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // si pulsó en seleccionar
            if (columna == 4){

                // fijamos los valores en el padre
                this.Padre.CodLoc = (String) modeloTabla.getValueAt(indice, 0);
				this.Padre.tPais.setText((String) modeloTabla.getValueAt(indice, 1));
				this.Padre.tJurisdiccion.setText((String) modeloTabla.getValueAt(indice, 2));
				this.Padre.tLocalidad.setText((String) modeloTabla.getValueAt(indice, 3));

				// fijamos el foco
				this.Padre.tEmail.requestFocus();

				// cerramos el formulario
				this.dispose();

            }

		}

	}

}
