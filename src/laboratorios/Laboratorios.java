/*

    Nombre: laboratorios
    Fecha: 31/05/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnóstico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones de la base de datos de la
                 tabla de laboratorios

 */

// definición del paquete
package laboratorios;

//importamos las librerías
import java.sql.Blob;
import java.awt.Image;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import funciones.Utilidades;
import seguridad.Seguridad;

// declaración de la clase
public class Laboratorios {

	// declaración de variables de clase
    protected Conexion Enlace;               // puntero a la base de datos
    protected int IdLaboratorio;             // clave del laboratorio
    protected String Nombre;                 // nombre del laboratorio
    protected String Responsable;            // responsable del laboratorio
    protected String Pais;                   // país donde queda
    protected int IdPais;                    // clave del país
    protected String Localidad;              // nombre de la localidad
    protected String IdLocalidad;            // clave indec de la localidad
    protected String Provincia;              // nombre de la provincia
    protected String IdProvincia;            // clave indec de la provincia
    protected String Direccion;              // dirección postal
    protected String CodigoPostal;           // código postal correspondiente
    protected String Coordenadas;            // coordenadas gps del laboratorio
    protected String Dependencia;            // nombre de la dependencia
    protected int IdDependencia;             // clave de la dependencia
    protected String EMail;                  // dirección de correo electrónico
    protected String FechaAlta;              // fecha de alta del registro
    protected String Observaciones;          // observaciones y comentarios
    protected String Usuario;                // usuario que actualizó / insertó
    protected int IdUsuario;                 // clave del responsable
    protected Image Logo;                    // logo del laboratorio
    protected FileInputStream Imagen;        // flujo de entrada del logo
    protected int LongImagen;                // longitud del logo

	// constructor de la clase
	public Laboratorios(){

		// abrimos la conexión con la base
        this.Enlace = new Conexion();

		// inicializamos las variables
        this.IdLaboratorio = 0;
        this.Nombre = "";
        this.Responsable = "";
        this.Pais = "";
        this.IdPais = 0;
        this.Localidad = "";
        this.IdLocalidad = "";
        this.Provincia = "";
        this.IdProvincia = "";
        this.Direccion = "";
        this.Coordenadas = "";
        this.CodigoPostal = "";
        this.Dependencia = "";
        this.IdDependencia = 0;
        this.EMail = "";
        this.FechaAlta = "";
        this.Observaciones = "";
        this.Usuario = "";
        this.IdUsuario = 0;
        this.LongImagen = 0;
        this.Imagen = null;
        this.Logo = null;

	}

    // métodos de asignación de valores
    public void setIdLaboratorio(int idlaboratorio) {
        this.IdLaboratorio = idlaboratorio;
    }
    public void setNombre(String nombre){
        this.Nombre = nombre;
    }
    public void setResponsable(String responsable){
        this.Responsable = responsable;
    }
    public void setIdPais(int idpais){
        this.IdPais = idpais;
    }
    public void setPais(String pais){
        this.Pais = pais;
    }
    public void setProvincia(String provincia){
        this.Provincia = provincia;
    }
    public void setIdLocalidad(String idlocalidad){
        this.IdLocalidad = idlocalidad;
    }
    public void setLocalidad(String localidad){
        this.Localidad = localidad;
    }
    public void setDireccion(String direccion){
        this.Direccion = direccion;
    }
    public void setCodigoPostal(String codigopostal){
        this.CodigoPostal = codigopostal;
    }
    public void setIdDependencia(int iddependencia){
        this.IdDependencia = iddependencia;
    }
    public void setEmail(String email){
        this.EMail = email;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }
    public void setIdUsuario(int idusuario){
        this.IdUsuario = idusuario;
    }
    public void setLongImagen(int longitud){
        this.LongImagen = longitud;
    }
    public void setImagen(FileInputStream imagen){
        this.Imagen = imagen;
    }

    // métodos de obtención de datos
    public int getIdLaboratorio(){
        return this.IdLaboratorio;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getResponsable(){
        return this.Responsable;
    }
    public String getPais(){
        return this.Pais;
    }
    public int getIdPais(){
        return this.IdPais;
    }
    public String getLocalidad(){
        return this.Localidad;
    }
    public String getIdLocalidad(){
        return this.IdLocalidad;
    }
    public String getProvincia(){
        return this.Provincia;
    }
    public String getIdProvincia(){
        return this.IdProvincia;
    }
    public String getDireccion(){
        return this.Direccion;
    }
    public String getCodigoPostal(){
        return this.CodigoPostal;
    }
    public String getCoordenadas(){
        return this.Coordenadas;
    }
    public String getDependencia(){
        return this.Dependencia;
    }
    public int getIdDependencia(){
        return this.IdDependencia;
    }
    public String getEmail(){
        return this.EMail;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public Image getLogo(){
        return this.Logo;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param texto string con la cadena a buscar
     * @return recordset con los registros hallados
     * Método que recibe un texto y lo busca en la tabla de laboratorios
     * retorna el vector con los registros encontrados
     */
    public ResultSet buscaLaboratorio(String texto){

        // definición de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_laboratorios.id AS idlaboratorio, "
                 + "       diagnostico.v_laboratorios.laboratorio AS laboratorio, "
                 + "       diagnostico.v_laboratorios.responsable AS responsable, "
                 + "       diagnostico.v_laboratorios.localidad AS localidad_laboratorio, "
                 + "       diagnostico.v_laboratorios.provincia AS jurisdiccion "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.laboratorio LIKE '%" + texto + "%' OR "
                 + "      diagnostico.v_laboratorios.responsable LIKE '%" + texto + "%' OR "
                 + "      diagnostico.v_laboratorios.localidad LIKE '%" + texto + "%' OR "
                 + "      diagnostico.v_laboratorios.provincia LIKE '%" + texto + "%' "
                 + "ORDER BY diagnostico.v_laboratorios.laboratorio; ";

        // ejecutamos la consulta y retornamos el array
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idlaboratorio la clave del registro
     * Método que recibe la clave del laboratorio e instancia las variables
     * de clase con los valores del registro
     */
    public void getDatosLaboratorio(int idlaboratorio){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Blob Archivo;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_laboratorios.id AS id_laboratorio, "
                 + "       diagnostico.v_laboratorios.laboratorio AS laboratorio, "
                 + "       diagnostico.v_laboratorios.responsable AS responsable, "
                 + "       diagnostico.v_laboratorios.pais AS pais, "
                 + "       diagnostico.v_laboratorios.idpais AS idpais, "
                 + "       diagnostico.v_laboratorios.localidad AS localidad, "
                 + "       diagnostico.v_laboratorios.codloc AS codloc, "
                 + "       diagnostico.v_laboratorios.provincia AS provincia, "
                 + "       diagnostico.v_laboratorios.codprov AS codprov, "
                 + "       diagnostico.v_laboratorios.direccion AS direccion, "
                 + "       diagnostico.v_laboratorios.codigo_postal AS codigo_postal, "
                 + "       diagnostico.v_laboratorios.coordenadas AS coordenadas, "
                 + "       diagnostico.v_laboratorios.dependencia AS dependencia, "
                 + "       diagnostico.v_laboratorios.iddependencia AS iddependencia, "
                 + "       diagnostico.v_laboratorios.email AS e_mail, "
                 + "       diagnostico.v_laboratorios.fecha_alta AS fecha_alta, "
                 + "       diagnostico.v_laboratorios.observaciones AS observaciones, "
                 + "       diagnostico.v_laboratorios.usuario AS usuario, "
                 + "       diagnostico.v_laboratorios.id_usuario AS idusuario, "
                 + "       diagnostico.v_laboratorios.logo AS logo "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.id = '" + idlaboratorio + "';";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos los valores a las variables de clase
            this.IdLaboratorio = Resultado.getInt("id_laboratorio");
            this.Nombre = Resultado.getString("laboratorio");
            this.Responsable = Resultado.getString("responsable");
            this.Pais = Resultado.getString("pais");
            this.IdPais = Resultado.getInt("idpais");
            this.Localidad = Resultado.getString("localidad");
            this.IdLocalidad = Resultado.getString("codloc");
            this.Provincia = Resultado.getString("provincia");
            this.Direccion = Resultado.getString("direccion");
            this.CodigoPostal = Resultado.getString("codigo_postal");
            this.Coordenadas = Resultado.getString("coordenadas");
            this.Dependencia = Resultado.getString("dependencia");
            this.IdDependencia = Resultado.getInt("iddependencia");
            this.EMail = Resultado.getString("e_mail");
            this.FechaAlta = Resultado.getString("fecha_alta");
            this.Observaciones = Resultado.getString("observaciones");
            this.Usuario = Resultado.getString("usuario");
            this.IdUsuario = Resultado.getInt("idusuario");

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("logo") != null){
                Archivo = Resultado.getBlob("logo");
                this.Logo = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            } else {
                this.Logo = null;
            }

        // si hubo un error o no pudo leer la imagen
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método utilizado en los remitos y documentación que 
     * a partir de la clave del usuario actual obtiene el 
     * logo del laboratorio
     */
    public void getLogoLaboratorio(){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Blob Archivo;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_laboratorios.logo AS logo "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.id = '" + Seguridad.Laboratorio + "';";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("logo") != null){
                Archivo = Resultado.getBlob("logo");
                this.Logo = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            } else {
                this.Logo = null;
            }

        // si hubo un error o no pudo leer la imagen
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id del registro afectado
     * Método que produce la consulta de inserción o edición según
     * el caso y retorna la id del registro afectado
     * Usamos la sentencia preparedstatement para evitar conflictos
     * con las cadenas de 8 bits
     */
    public int grabarLaboratorio(){

        // primero actualizamos las coordenadas
        String Domicilio = this.Direccion + " - " + this.Localidad + " - " + this.Provincia + " - " + this.Pais;
        Utilidades Codificar = new Utilidades();
        this.Coordenadas = Codificar.getCoordenadas(Domicilio);

        // si está insertando
        if (this.IdLaboratorio == 0){
            this.nuevoLaboratorio();
        } else {
            this.editaLaboratorio();
        }

        /*
         * si el usuario seleccionó una imagen la actualizamos independientemente
         * esto lo hacemos así porque en una edición puede no haber seleccionado
         * una imagen y así evitamos pisarla con una cadena nula, o también en
         * un alta no es un campo obligatorio
         */
        if (this.Imagen != null){
            this.grabaImagen();
        }

        // retorna la id
        return this.IdLaboratorio;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoLaboratorio(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // produce la consulta de inserción
        Consulta = "INSERT INTO cce.laboratorios "
                    + "        (NOMBRE, "
                    + "         RESPONSABLE, "
                    + "         PAIS, "
                    + "         LOCALIDAD, "
                    + "         DIRECCION, "
                    + "         CODIGO_POSTAL, "
                    + "         COORDENADAS, "
                    + "         DEPENDENCIA, "
                    + "         E_MAIL, "
                    + "         OBSERVACIONES, "
                    + "         USUARIO) "
                    + "        VALUES "
                    + "        (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Nombre);
            psInsertar.setString(2, this.Responsable);
            psInsertar.setInt(3, this.IdPais);
            psInsertar.setString(4, this.IdLocalidad);
            psInsertar.setString(5, this.Direccion);
            psInsertar.setString(6, this.CodigoPostal);
            psInsertar.setString(7, this.Coordenadas);
            psInsertar.setInt(8, this.IdDependencia);
            psInsertar.setString(9, this.EMail);
            psInsertar.setString(10, this.Observaciones);
            psInsertar.setInt(11, Seguridad.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.IdLaboratorio = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaLaboratorio(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // produce la consulta de edición
        Consulta = "UPDATE cce.laboratorios SET "
                    + "        NOMBRE = ?, "
                    + "        RESPONSABLE = ?, "
                    + "        PAIS = ?, "
                    + "        LOCALIDAD = ?, "
                    + "        DIRECCION = ?, "
                    + "        CODIGO_POSTAL = ?, "
                    + "        COORDENADAS = ?, "
                    + "        DEPENDENCIA = ?, "
                    + "        E_MAIL = ?, "
                    + "        OBSERVACIONES = ?, "
                    + "        USUARIO = ? "
                    + "WHERE cce.laboratorios.ID = ?;";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Nombre);
            psInsertar.setString(2, this.Responsable);
            psInsertar.setInt(3, this.IdPais);
            psInsertar.setString(4, this.IdLocalidad);
            psInsertar.setString(5, this.Direccion);
            psInsertar.setString(6, this.CodigoPostal);
            psInsertar.setString(7, this.Coordenadas);
            psInsertar.setInt(8, this.IdDependencia);
            psInsertar.setString(9, this.EMail);
            psInsertar.setString(10, this.Observaciones);
            psInsertar.setInt(11, Seguridad.Id);
            psInsertar.setInt(12, this.IdLaboratorio);

            // ejecutamos la edición
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que actualiza la imagen en la base
     */
    protected void grabaImagen(){

        // declaración de variables
        String Consulta;
        Connection Puntero;
        PreparedStatement psInsertar;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // actualizamos en el registro
        Consulta = "UPDATE cce.laboratorios SET " +
                   "       logo = ? " +
                   "WHERE cce.laboratorios.id = ?;";

        try {

            // asignamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setBinaryStream(1,  this.Imagen, this.LongImagen);
            psInsertar.setInt(2, this.IdLaboratorio);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param provincia texto con la provincia del laboratorio
     * @param laboratorio texto con el nombre del laboratorio
     * @return entero con la clave del laboratorio
     * Método que recibe como parámetro el nombre de un laboratorio y la
     * provincia del mismo (puede haber mas de un laboratorio con el mismo
     * nombre) y retorna la clave del laboratorio
     */
    public int getClaveLaboratorio(String provincia, String laboratorio){

        // declaración de variables
        String Consulta;
        int claveLaboratorio = 0;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_laboratorios.id AS id_laboratorio "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.laboratorio = '" + laboratorio + "' AND "
                 + "      diagnostico.v_laboratorios.provincia = '" + provincia + "'; ";

        // ejecutamos la consulta
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            claveLaboratorio = Resultado.getInt("id_laboratorio");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos la clave
        return claveLaboratorio;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param laboratorio parte del nombre del laboratorio
     * @param provincia nombre de la provincia
     * @return resultset con los registros hallados
     * Método utilizado en el abm de usuarios para autorizar a cargar
     * específicamente un laboratorio, recibe parte del nombre del
     * laboratorio y la provincia y retorna los registros coincidentes
     */
    public ResultSet nominaLaboratorios(String laboratorio, String provincia){

        // definimos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_laboratorios.id AS id_laboratorio, "
                 + "       diagnostico.v_laboratorios.laboratorio AS laboratorio "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.provincia = '" + provincia + "' AND "
                 + "      diagnostico.v_laboratorios.laboratorio LIKE '" + laboratorio + "' "
                 + "ORDER BY diagnostico.v_laboratorios.laboratorio; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param mail cadena con el correo del laboratorio
     * @return boolean si encontró el correo
     * Método que retorna verdadero si el correo ya se encuentra declarado
     * en la base
     */
    public boolean verificaMail(String mail){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        boolean encontrado = false;

        // compone la consulta
        Consulta = "SELECT COUNT(*) AS registros "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.email = '" + mail + "';";

        // obtenemos los registros
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // verificamos si encontró
            if (Resultado.getInt("registros") == 0){

                // retorna verdadero
                encontrado = true;

            // si lo encontró
            } else {

                // retorna falso
                encontrado = false;

            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retorna el resultado de la operación
        return encontrado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param laboratorio - cadena con el nombre del laboratorio
     * @param localidad - cadena con la clave de la localidad
     * @return bolean con el número de registros encontrados
     * Método que recibe como parámetro el nombre del laboratorio
     * y la jurisdicción y retorna el número de registros
     * encontrados, utilizado para evitar el ingreso de
     * laboratorios duplicados
     *
     */
    public boolean verificaLaboratorio(String laboratorio, String localidad){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        boolean encontrado = false;

        // compone la consulta
        Consulta = "SELECT COUNT(*) AS registros "
                 + "FROM diagnostico.v_laboratorios "
                 + "WHERE diagnostico.v_laboratorios.laboratorio = '" + laboratorio + "' AND "
                 + "      diagnostico.v_laboratorios.codloc = '" + localidad + "'; ";

        // obtenemos los registros
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // verificamos si encontró
            if (Resultado.getInt("registros") == 0){

                // retorna verdadero
                encontrado = true;

            // si lo encontró
            } else {

                // retorna falso
                encontrado = false;

            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retorna el resultado de la operación
        return encontrado;

    }

}
