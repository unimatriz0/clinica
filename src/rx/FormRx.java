/*

    Nombre: FormRx
    Fecha: 24/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que presenta los datos de las
	             radiografías de tórax del paciente

 */

// definición del paquete
package rx;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import java.awt.Dialog;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;
import funciones.Mensaje;
import funciones.Utilidades;
import rx.Rx;

// declaración de la clase
public class FormRx extends JDialog {

	// declaramos el serial id
	private static final long serialVersionUID = -6963395292085090664L;

	// definición de variables
	private JDateChooser dFecha;
	private JCheckBox cIct;
	private JCheckBox cCardiomegalia;
	private JCheckBox cPleuro;
	private JCheckBox cEpoc;
	private JCheckBox cCalcificaciones;
	private JCheckBox cNoTiene;
	private int Id;
	private int Protocolo;
	private int IdVisita;
	private Utilidades Herramientas;
	private Rx Radiografias;

	// constructor de la clase recibe como parámetro el protocolo
	// y la clave de la visita
	public FormRx(Dialog parent, boolean modal, int protocolo, int idvisita) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// inicializamos los objetos
		this.Herramientas = new Utilidades();
		this.Radiografias = new Rx();

		// fijamos las propiedades
		this.setBounds(150, 150, 490, 230);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Radiografías del Paciente");
		this.setLayout(null);

		// asignamos el protocolo del paciente
		this.Protocolo = protocolo;
		this.IdVisita = idvisita;
		this.Id = 0;

		// cargamos el registro
		this.verRadiografia(idvisita);

		// configuramos el formulario
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que configura el formulario
	 */
	private void initForm(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>RX de Tórax</b></html>");
		lTitulo.setBounds(10, 10, 150, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// pide la fecha
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setBounds(10, 45, 60, 26);
		lFecha.setFont(Fuente);
		this.add(lFecha);
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(55, 45, 120, 26);
		this.dFecha.setFont(Fuente);
		this.dFecha.setToolTipText("Fecha de toma de la Placa");
		this.add(this.dFecha);

		// fijamos la fecha actual por defecto
        Calendar fechaActual = new GregorianCalendar();
        this.dFecha.setCalendar(fechaActual);

		// presenta el checkbox de itc
		this.cIct = new JCheckBox("ICT Conservada");
		this.cIct.setBounds(10, 80, 140, 26);
		this.cIct.setFont(Fuente);
		this.cIct.setToolTipText("Marque si presenta ICT convervada");
		this.add(this.cIct);

		// presenta el checbox de cardiomegalia
		this.cCardiomegalia = new JCheckBox("Cardiomegalia");
		this.cCardiomegalia.setBounds(160, 80, 120, 26);
		this.cCardiomegalia.setFont(Fuente);
		this.cCardiomegalia.setToolTipText("Marque si presenta cardiomegalia");
		this.add(this.cCardiomegalia);

		// presenta el checkbox de pleuro
		this.cPleuro = new JCheckBox("Sec. Pleuro / Pulmonar");
		this.cPleuro.setBounds(295, 80, 185, 26);
		this.cPleuro.setFont(Fuente);
		this.add(this.cPleuro);

		// presenta el checbox de signos de epoc
		this.cEpoc = new JCheckBox("Signos de EPOC");
		this.cEpoc.setBounds(10, 115, 130, 26);
		this.cEpoc.setFont(Fuente);
		this.cEpoc.setToolTipText("Marque si presenta signos de EPOC");
		this.add(this.cEpoc);

		// presenta el checkbox de calcificaciones
		this.cCalcificaciones = new JCheckBox("Calcificaciones");
		this.cCalcificaciones.setBounds(160, 115, 125, 26);
		this.cCalcificaciones.setFont(Fuente);
		this.cCalcificaciones.setToolTipText("Marque si presenta calcificaciones");
		this.add(this.cCalcificaciones);

		// presenta el checkbox de no tiene
		this.cNoTiene = new JCheckBox("No Tiene");
		this.cNoTiene.setBounds(295, 115, 115, 26);
		this.cNoTiene.setFont(Fuente);
		this.cNoTiene.setToolTipText("Marque si no presenta signos");
		this.add(cNoTiene);

		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(200, 150, 110, 26);
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setToolTipText("Pulse para grabar el registro");
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaRx();
            }
        });
		this.add(btnGrabar);

		// el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(320, 150, 110, 26);
		btnCancelar.setFont(Fuente);
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setToolTipText("Cierra el formulario sin grabar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaRx();
            }
        });
		this.add(btnCancelar);

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idvisita - entero con la clave de la visita
	 * Método que recibe como parámetro la clave de un
	 * registro y lo presenta en el formulario
	 */
	private void verRadiografia(int idvisita){

		// obtenemos el registro
		this.Radiografias.getDatosRx(idvisita);

		// asignamos en las variables de clase
		// (protocolo y visita no porque pisaríamos)
		this.Id = this.Radiografias.getId();
		
		// asignamos en el formulario
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Radiografias.getFecha()));
		if (this.Radiografias.getIct() == 1){
			this.cIct.setSelected(true);
		} else {
			this.cIct.setSelected(false);
		}
		if (this.Radiografias.getCardiomegalia() == 1){
			this.cCardiomegalia.setSelected(true);
		} else {
			this.cCardiomegalia.setSelected(false);
		}
		if (this.Radiografias.getPleuro() == 1){
			this.cPleuro.setSelected(true);
		} else {
			this.cPleuro.setSelected(false);
		}
		if (this.Radiografias.getCalcificaciones() == 1){
			this.cCalcificaciones.setSelected(true);
		} else {
			this.cCalcificaciones.setSelected(false);
		}
		if (this.Radiografias.getNotiene() == 1){
			this.cNoTiene.setSelected(true);
		} else {
			this.cNoTiene.setSelected(false);
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que valida el formulario antes de enviarlo
	 * al servidor
	 */
	private void validaRx(){

		// inicializamos las variables
		Boolean Correcto = false;

		// si no indicó la fecha 
        if (this.Herramientas.fechaJDate(this.dFecha) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Ingrese la fecha de la visita",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            this.dFecha.requestFocus();
			return;

        }

		// verifica que al menos halla marcado una opción
		if (this.cIct.isSelected()){
			Correcto = true;
		} else if (this.cCardiomegalia.isSelected()){
			Correcto = true;
		} else if (this.cPleuro.isSelected()){
			Correcto = true;
		} else if (this.cEpoc.isSelected()){
			Correcto = true;
		} else if (this.cCalcificaciones.isSelected()){
			Correcto = true;
		} else if (this.cNoTiene.isSelected()){
			Correcto = true;
		}

		// si no marcó nada
		if (!Correcto){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
                        "Debe marcar al menos una opción",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
			return;

		// si marcó algo
		} else {

			// grabamos el registro
			this.grabaRx();

		}
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que ejecuta la consulta de actualización
	 * en el servidor
	 */
	private void grabaRx(){

		// asignamos en la clase
		this.Radiografias.setId(this.IdVisita);
		this.Radiografias.setPaciente(this.Protocolo);
		this.Radiografias.setId(this.Id);
		this.Radiografias.setFecha(this.Herramientas.fechaJDate(this.dFecha));

		// según el estado de los checkbox
		if (this.cIct.isSelected()){
			this.Radiografias.setIct(1);
		} else {
			this.Radiografias.setIct(0);
		}
		if (this.cCardiomegalia.isSelected()){
			this.Radiografias.setCardiomegalia(1);
		} else {
			this.Radiografias.setCardiomegalia(0);
		}
		if (this.cPleuro.isSelected()){
			this.Radiografias.setPleuro(1);
		} else {
			this.Radiografias.setPleuro(0);
		}
		if (this.cEpoc.isSelected()){
			this.Radiografias.setEpoc(1);
		} else {
			this.Radiografias.setEpoc(0);
		}
		if (this.cCalcificaciones.isSelected()){
			this.Radiografias.setCalcificaciones(1);
		} else {
			this.Radiografias.setCalcificaciones(0);
		}
		if (this.cNoTiene.isSelected()){
			this.Radiografias.setNoTiene(1);
		} else {
			this.Radiografias.setNoTiene(0);
		}

		// grabamos el registro
		this.Id = this.Radiografias.grabaRx();

		// presenta el mensaje
		new Mensaje("Registro grabado ... ");
		
		// asignamos la id en el formulario padre

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * recarga el formulario o lo limpia según el
	 * valor de la variable control
	 */
	private void cancelaRx(){

		// recargamos el registro
		this.verRadiografia(this.IdVisita);

	}

}
