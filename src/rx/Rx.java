/*

    Nombre: Rx
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 radiografías, esta tabla puede tener mas de una entrada
                 por cada paciente

*/

 // declaración del paquete
package rx;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Rx {

    // declaración de variables de clase
    protected Conexion Enlace;            // puntero a la base de datos
    protected int Id;                     // clave del registro
    protected int Paciente;               // clave del paciente
    protected int Visita;                 // clave de la visita
    protected String Fecha;               // fecha de toma
    protected int Ict;                    // 0 no 1 si
    protected int Cardiomegalia;          // 0 no 1 si
    protected int Pleuro;                 // 0 no 1 si
    protected int Epoc;                   // 0 no 1 si
    protected int Calcificaciones;        // 0 no 1 si
    protected int NoTiene;                // 0 no 1 si
    protected int IdUsuario;              // clave del usuario
    protected String Usuario;             // nombre del usuario
    protected String FechaAlta;           // fecha de alta del registro

    // constructor de la clase
    public Rx(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.initRadiografias();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase desde 
     * el constructor o llamado cuando no encontró registros
     */
    private void initRadiografias(){

        // inicializamos las variables
        this.Id = 0;
        this.Paciente = 0;
        this.Visita = 0;
        this.Fecha = "";
        this.Ict = 0;
        this.Cardiomegalia = 0;
        this.Pleuro = 0;
        this.Epoc = 0;
        this.Calcificaciones = 0;
        this.NoTiene = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setVisita(int visita){
        this.Visita = visita;
    }
    public void setFecha(String fecha){
        this.Fecha = fecha;
    }
    public void setIct(int ict){
        this.Ict = ict;
    }
    public void setCardiomegalia(int cardiomegalia){
        this.Cardiomegalia = cardiomegalia;
    }
    public void setPleuro(int pleuro){
        this.Pleuro = pleuro;
    }
    public void setEpoc(int epoc){
        this.Epoc = epoc;
    }
    public void setCalcificaciones(int calcificaciones){
        this.Calcificaciones = calcificaciones;
    }
    public void setNoTiene(int notiene){
        this.NoTiene = notiene;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getVisita(){
        return this.Visita;
    }
    public String getFecha(){
        return this.Fecha;
    }
    public int getIct(){
        return this.Ict;
    }
    public int getCardiomegalia(){
        return this.Cardiomegalia;
    }
    public int getPleuro(){
        return this.Pleuro;
    }
    public int getEpoc(){
        return this.Epoc;
    }
    public int getCalcificaciones(){
        return this.Calcificaciones;
    }
    public int getNotiene(){
        return this.NoTiene;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * @return vector con los registros encontrados
     * Método que recibe como parámetro la clave de un
     * paciente y retorna el vector con todos los registros
     * de radiografías de ese paciente
     */
    public ResultSet nominaRx(int idpaciente){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_rx.id AS id, " +
                   "       diagnostico.v_rx.fecha AS fecha, " +
                   "       diagnostico.v_rx.paciente AS paciente, " +
                   "       diagnostico.v_rx.idvisita AS visita, " +
                   "       diagnostico.v_rx.ict AS ict, " +
                   "       diagnostico.v_rx.cardiomegalia AS cardiomegalia, " +
                   "       diagnostico.v_rx.pleuro AS pleuro, " +
                   "       diagnostico.v_rx.epoc AS epoc, " +
                   "       diagnostico.v_rx.calcificaciones AS calcificaciones, " +
                   "       diagnostico.v_rx.notiene AS notiene, " +
                   "       diagnostico.v_rx.usuario AS usuario, " +
                   "       diagnostico.v_rx.fecha_alta AS fechaalta " +
                   "FROM diagnostico.v_rx " +
                   "WHERE diagnostico.v_rx.paciente = '" + idpaciente + "' " +
                   "ORDER BY STR_TO_DATE(diagnostico.v_rx.fecha_alta, '%d/%m/%Y') DESC; ";
        Resultado = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idvisita entero con la clave de la visita
     * Método que recibe como parámetro la clave de un registro
     * y asigna en las variables de clase los datos del mismo
     */
    public void getDatosRx(int idvisita){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_rx.id AS id, " +
                   "       diagnostico.v_rx.paciente AS paciente, " +
                   "       diagnostico.v_rx.idvisita AS visita, " +
                   "       diagnostico.v_rx.fecha AS fecha, " +
                   "       diagnostico.v_rx.ict AS ict, " +
                   "       diagnostico.v_rx.cardiomegalia AS cardiomegalia, " +
                   "       diagnostico.v_rx.pleuro AS pleuro, " +
                   "       diagnostico.v_rx.epoc AS epoc, " +
                   "       diagnostico.v_rx.calcificaciones AS calcificaciones, " +
                   "       diagnostico.v_rx.notiene AS notiene, " +
                   "       diagnostico.v_rx.usuario AS usuario, " +
                   "       diagnostico.v_rx.fecha_alta AS fechaalta " +
                   "FROM diagnostico.v_rx.idvisita '" + idvisita + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos
            // los valores (puede no haber datos)
            if (Resultado.next()){

                // asignamos en las variables de clase
                this.Id = Resultado.getInt("id");
                this.Paciente = Resultado.getInt("paciente");
                this.Visita = Resultado.getInt("visita");
                this.Fecha = Resultado.getString("fecha");
                this.Ict = Resultado.getInt("ict");
                this.Cardiomegalia = Resultado.getInt("cardiomegalia");
                this.Pleuro = Resultado.getInt("pleuro");
                this.Epoc = Resultado.getInt("epoc");
                this.Calcificaciones = Resultado.getInt("calcificaciones");
                this.NoTiene = Resultado.getInt("notiene");
                this.Usuario = Resultado.getString("usuario");
                this.FechaAlta = Resultado.getString("fechaalta");

            // si no hay registros
            } else {

                // las inicializamos
                this.initRadiografias();
                
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id entero con la clave del registro afectado
     * Método que ejectua la consulta de actualización o
     * edición según corresponda
     */
    public int grabaRx(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoRx();
        } else {
            this.editaRx();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la inserción de un nuevo registro
     */
    public void nuevoRx(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.rx " +
                   "       (paciente, " +
                   "        visita, " +
                   "        fecha, " +
                   "        ict, " +
                   "        cardiomegalia, " +
                   "        pleuro, " +
                   "        epoc, " +
                   "        calcificaciones, " +
                   "        notiene, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Fecha);
            psInsertar.setInt(4,    this.Ict);
            psInsertar.setInt(5,    this.Cardiomegalia);
            psInsertar.setInt(6,    this.Pleuro);
            psInsertar.setInt(7,    this.Epoc);
            psInsertar.setInt(8,    this.Calcificaciones);
            psInsertar.setInt(9,    this.NoTiene);
            psInsertar.setInt(10,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la edición del registro
     */
    public void editaRx(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.rx SET " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       ict = ?, " +
                   "       cardiomegalia = ?, " +
                   "       pleuro = ?, " +
                   "       epoc = ?, " +
                   "       calcificaciones = ?, " +
                   "       notiene = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.rx.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Fecha);
            psInsertar.setInt(2, this.Ict);
            psInsertar.setInt(3, this.Cardiomegalia);
            psInsertar.setInt(4, this.Pleuro);
            psInsertar.setInt(5, this.Epoc);
            psInsertar.setInt(6, this.Calcificaciones);
            psInsertar.setInt(7, this.NoTiene);
            psInsertar.setInt(8, this.IdUsuario);
            psInsertar.setInt(9, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idrx entero con la clave del registro
     * Método que recibe como parámetro la clave de un
     * registro y ejecuta la consulta de eliminación
     */
    public void borraRx(int idrx){

        // declaramos las variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diagnostico.rx " +
                   "WHERE diagnostico.rx.id = '" + idrx + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

}
