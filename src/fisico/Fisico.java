/*

    Nombre: Fisico
    Fecha: 22/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que provee los métodos para la tabla de
                 examen físico

 */


// declaración del paquete
package fisico;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.ResultSet;
import java.sql.SQLException;

// definición de la clase
public class Fisico {

    // declaración de las variables de clase
    private Conexion Enlace;                // puntero a la base de datos
    private int IdUsuario;                  // clave del usuario
    private int Id;                         // clave del registro
    private int Protocolo;                  // clave del protocolo del paciente
    private int Visita;                     // clave de la visita
    private String Ta;                      // tensión arterial
    private float Peso;                     // peso del paciente
    private int Fc;                         // frecuencia cardíaca
    private int Spo;                        // saturación de oxígeno
    private float Talla;                    // altura en metros
    private float Bmi;                      // índice de masa corporal
    private int Edema;                      // 0 falso - 1 verdadero
    private int Sp;                         // 0 falso - 1 verdadero
    private int Mvdisminuido;               // 0 falso - 1 verdadero
    private int Crepitantes;                // 0 falso - 1 verdadero
    private int Sibilancias;                // 0 falso - 1 verdadero
    private String Usuario;                 // nombre del usuario
    private String Fecha;                   // fecha de alta del registro

    // constructor de la clase
    public Fisico() {

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.Id = 0;
        this.Protocolo = 0;
        this.Visita = 0;
        this.Ta = "";
        this.Peso = 0;
        this.Fc = 0;
        this.Spo = 0;
        this.Talla = 0;
        this.Bmi = 0;
        this.Edema = 0;
        this.Sp = 0;
        this.Mvdisminuido = 0;
        this.Crepitantes = 0;
        this.Sibilancias = 0;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id) {
        this.Id = id;
    }

    public void setProtocolo(int protocolo) {
        this.Protocolo = protocolo;
    }

    public void setVisita(int visita) {
        this.Visita = visita;
    }

    public void setTa(String ta) {
        this.Ta = ta;
    }

    public void setPeso(float peso) {
        this.Peso = peso;
    }

    public void setFc(int fc) {
        this.Fc = fc;
    }

    public void setSpo(int spo) {
        this.Spo = spo;
    }

    public void setTalla(float talla) {
        this.Talla = talla;
    }

    public void setBmi(float bmi) {
        this.Bmi = bmi;
    }

    public void setEdema(int edema) {
        this.Edema = edema;
    }

    public void setSp(int sp) {
        this.Sp = sp;
    }

    public void setMvdisminuido(int mvdisminuido) {
        this.Mvdisminuido = mvdisminuido;
    }

    public void setCrepitantes(int crepitantes) {
        this.Crepitantes = crepitantes;
    }

    public void setSibilancias(int sibilancias) {
        this.Sibilancias = sibilancias;
    }

    // métodos de retorno de valores
    public int getId() {
        return this.Id;
    }

    public int getProtocolo() {
        return this.Protocolo;
    }

    public int getVisita() {
        return this.Visita;
    }

    public String getTa() {
        return this.Ta;
    }

    public float getPeso() {
        return this.Peso;
    }

    public int getFc() {
        return this.Fc;
    }

    public int getSpo() {
        return this.Spo;
    }

    public float getTalla() {
        return this.Talla;
    }

    public float getBmi() {
        return this.Bmi;
    }

    public int getEdema() {
        return this.Edema;
    }

    public int getSp() {
        return this.Sp;
    }

    public int getMvdisminuido() {
        return this.Mvdisminuido;
    }

    public int getCrepitantes() {
        return this.Crepitantes;
    }

    public int getSibilancias() {
        return this.Sibilancias;
    }

    public String getUsuario() {
        return this.Usuario;
    }

    public String getFecha() {
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param visita - entero con la clave del registro
     * Método que recibe como parámetro la clave de una visita
     * y asigna los valores de la misma en las variables de clase
     */
    public void getDatosVisita(int visita) {

        // declaración de variables
        ResultSet Resultado;
        String Consulta;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_fisico.id AS id, "
                + "       diagnostico.v_fisico.protocolo AS protocolo, "
                + "       diagnostico.v_fisico.idvisita AS visita, "
                + "       diagnostico.v_fisico.fechavisita AS fecha, "
                + "       diagnostico.v_fisico.ta AS ta, "
                + "       diagnostico.v_fisico.peso AS peso, "
                + "       diagnostico.v_fisico.fc AS fc, "
                + "       diagnostico.v_fisico.spo AS spo, "
                + "       diagnostico.v_fisico.talla AS talla, "
                + "       diagnostico.v_fisico.bmi AS bmi, "
                + "       diagnostico.v_fisico.edema AS edema, "
                + "       diagnostico.v_fisico.sp AS sp, "
                + "       diagnostico.v_fisico.mvdisminuido AS mvdisminuido, "
                + "       diagnostico.v_fisico.crepitantes AS crepitantes, "
                + "       diagnostico.v_fisico.sibilancias AS sibilancias, "
                + "       diagnostico.v_fisico.usuario AS usuario "
                + "FROM diagnostico.v_fisico "
                + "WHERE diagnostico.v_fisico.idvisita = '" + visita + "'; ";

        // obtenemos el registro
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro y asignamos los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Protocolo = Resultado.getInt("protocolo");
            this.Visita = Resultado.getInt("visita");
            this.Fecha = Resultado.getString("fecha");
            this.Ta = Resultado.getString("ta");
            this.Peso = Resultado.getFloat("peso");
            this.Fc = Resultado.getInt("fc");
            this.Spo = Resultado.getInt("spo");
            this.Talla = Resultado.getFloat("talla");
            this.Bmi = Resultado.getFloat("bmi");
            this.Edema = Resultado.getInt("edema");
            this.Sp = Resultado.getInt("sp");
            this.Mvdisminuido = Resultado.getInt("mvdisminuido");
            this.Crepitantes = Resultado.getInt("crepitantes");
            this.Sibilancias = Resultado.getInt("sibilancias");
            this.Usuario = Resultado.getString("usuario");

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id - clave del registro afectado
     * Método público que ejecuta la consulta de edición
     * o eliminación según corresponda
     */
    public int grabaVisita(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaVisita();
        } else {
            this.editaVisita();
        }

        // retornamos la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de inserción
     */
    protected void nuevaVisita(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.fisico " +
                   "       (protocolo, " +
                   "        visita, " +
                   "        ta, " +
                   "        peso, " +
                   "        fc, " +
                   "        spo, " +
                   "        talla, " +
                   "        bmi, " +
                   "        edema, " +
                   "        sp, " +
                   "        mvdisminuido, " +
                   "        crepitantes, " +
                   "        sibilancias, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Protocolo);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Ta);
            psInsertar.setFloat(4,  this.Peso);
            psInsertar.setInt(5,    this.Fc);
            psInsertar.setInt(6,    this.Spo);
            psInsertar.setFloat(7,  this.Talla);
            psInsertar.setFloat(8,  this.Bmi);
            psInsertar.setInt(9,   this.Edema);
            psInsertar.setInt(10,   this.Sp);
            psInsertar.setInt(11,   this.Mvdisminuido);
            psInsertar.setInt(12,   this.Crepitantes);
            psInsertar.setInt(13,   this.Sibilancias);
            psInsertar.setInt(14,   this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

            // asigna la id del registro
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método protegido que ejecuta la consulta de edición
     */
    protected void editaVisita(){

        // declaración de variables
        String Consulta;
        java.sql.PreparedStatement psInsertar;
        java.sql.Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.fisico SET " +
                   "       id = ?, " +
                   "       protocolo = ?, " +
                   "       visita = ?, " +
                   "       ta = ?, " +
                   "       peso = ?, " +
                   "       fc = ?, " +
                   "       spo = ?, " +
                   "       talla = ?, " +
                   "       bmi = ?, " +
                   "       edema + ?, " +
                   "       sp = ?, " +
                   "       mvdisminuido = ?, " +
                   "       crepitantes = ?, " +
                   "       sibilancias = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.fisico.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Protocolo);
            psInsertar.setInt(2,    this.Visita);
            psInsertar.setString(3, this.Ta);
            psInsertar.setFloat(4,  this.Peso);
            psInsertar.setInt(5,    this.Fc);
            psInsertar.setInt(6,    this.Spo);
            psInsertar.setFloat(7,  this.Talla);
            psInsertar.setFloat(8,  this.Bmi);
            psInsertar.setInt(9,   this.Edema);
            psInsertar.setInt(10,   this.Sp);
            psInsertar.setInt(11,   this.Mvdisminuido);
            psInsertar.setInt(12,   this.Crepitantes);
            psInsertar.setInt(13,   this.Sibilancias);
            psInsertar.setInt(14,   this.IdUsuario);
            psInsertar.setInt(15,   this.Id);

            // ejecutamos la edición
            psInsertar.execute();

            // asigna la id del registro
            this.Id = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {

            // presenta el mensaje de error
            e.printStackTrace();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo entero con el protocolo del paciente
     * @return resultset con los registros encontrados
     * Método protegido que retorna un vector con la nómina
     * de visitas del paciente, utilizado en la impresión
     * de historias agrupadas por estudio
     */
    public ResultSet nominaVisitas(int protocolo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_fisico.id AS id, " +
                   "       diagnostico.v_fisico.protocolo AS protocolo, " +
                   "       diagnostico.v_fisico.idvisita AS idvisita, " +
                   "       diagnostico.v_fisico.fechavisita AS fecha, " +
                   "       diagnostico.v_fisico.tamax AS ta, " +
                   "       diagnostico.v_fisico.peso AS peso, " +
                   "       diagnostico.v_fisico.fc AS fc, " +
                   "       diagnostico.v_fisico.spo AS spo, " +
                   "       diagnostico.v_fisico.talla AS talla, " +
                   "       diagnostico.v_fisico.bmi AS bmi, " +
                   "       diagnostico.v_fisico.edema AS edema, " +
                   "       diagnostico.v_fisico.sp AS sp, " +
                   "       diagnostico.v_fisico.mvdisminuido AS mvdisminuido, " +
                   "       diagnostico.v_fisico.crepitantes AS crepitantes, " +
                   "       diagnostico.v_fisico.sibilancias AS sibilancias, " +
                   "       diagnostico.v_fisico.usuario AS usuario, " +
                   "       diagnostico.v_fisico.firma AS firma, " +
                   "       diagnostico.v_fisico.nombre AS nombre " +
                   "FROM diagnostico.v_fisico " +
                   "WHERE diagnostico.v_fisico.protocolo = '" + protocolo + "'; ";

        // ejecutamos la consulta y retornamos
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

}