/*

    Nombre: formBuscaLaboratorios
    Fecha: 04/06/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones de la tabla de instituciones

 */

// definimos el paquete
package instituciones;

// inclusión de archivos
import dbApi.Conexion;
import seguridad.Seguridad;

// definición de la clase
public class Instituciones {

    // declaración de variables
    protected Conexion Enlace;

    // declaración de las variables de la tabla
    protected int Id;                    // clave del registro
    protected String Nombre;             // nombre de la institución
    protected String IdLocalidad;        // clave de la localidad
    protected String Localidad;          // nombre de la localidad
    protected String Jurisdiccion;       // nombre de la jurisdicciòn
    protected String Pais;               // nombre del paìs
    protected String Direccion;          // dirección postal de la instituciòn
    protected String CodigoPostal;       // código postal de la institución
    protected String Telefono;           // teléfono de la institución
    protected String EMail;              // mail de la institución
    protected String Responsable;        // nombre del responsable
    protected int IdUsuario;             // usuario que ingresò el registro
    protected String Usuario;            // nombre del usuario
    protected int IdDependencia;         // clave de la dependencia
    protected String Dependencia;        // nombre de la dependencia
    protected String FechaAlta;          // fecha de alta del registro
    protected String Comentarios;        // comentarios del usuario
    protected String Coordenadas;        // coordenadas gps

    // constructor de la clase
    public Instituciones(){

        // abrimos el puntero
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.Id = 0;
        this.Nombre = "";
        this.IdLocalidad = "";
        this.Localidad = "";
        this.Jurisdiccion = "";
        this.Pais = "";
        this.Direccion = "";
        this.CodigoPostal = "";
        this.Telefono = "";
        this.EMail = "";
        this.Responsable = "";
        this.Usuario = "";
        this.IdDependencia = 0;
        this.Dependencia = "";
        this.FechaAlta = "";
        this.Comentarios = "";
        this.Coordenadas = "";

        // fijamos el usuario actual
        this.IdUsuario = Seguridad.Id;

    }

    // mètodos públicos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setNombre(String nombre){
        this.Nombre = nombre;
    }
    public void setIdLocalidad(String idlocalidad){
        this.IdLocalidad = idlocalidad;
    }
    public void setDireccion(String direccion){
        this.Direccion = direccion;
    }
    public void setCodigoPostal(String codigopostal){
        this.CodigoPostal = codigopostal;
    }
    public void setTelefono(String telefono){
        this.Telefono = telefono;
    }
    public void setEmail(String email){
        this.EMail = email;
    }
    public void setResponsable(String responsable){
        this.Responsable = responsable;
    }
    public void setIdDependencia(int iddependencia){
        this.IdDependencia = iddependencia;
    }
    public void setComentarios(String comentarios){
        this.Comentarios = comentarios;
    }
    public void setCoordenadas(String coordenadas){
        this.Coordenadas = coordenadas;
    }

    // métodos públicos de obtención de valores
    public int getId(){
        return this.Id;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getLocalidad(){
        return this.Localidad;
    }
    public String getJurisdiccion(){
        return this.Jurisdiccion;
    }
    public String getPais(){
        return this.Pais;
    }
    public String getDireccion(){
        return this.Direccion;
    }
    public String getCodigoPostal(){
        return this.CodigoPostal;
    }
    public String getTelefono(){
        return this.Telefono;
    }
    public String getEmail(){
        return this.EMail;
    }
    public String getResponsable(){
        return this.Responsable;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getDependencia(){
        return this.Dependencia;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }
    public String getComentarios(){
        return this.Comentarios;
    }
    public String getCoordenadas(){
        return this.Coordenadas;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idinstitucion entero con la clave de la institución
     * Método que recibe como paràmetro la clave de la instituciòn
     * y asigna en las variables de clase los valores del registro
     */
    public void getDatosInstitucion(int idinstitucion){

    }

    /**
     * @param texto, una cadena con el texto a buscar
     * @return un resultset con los registros encontrados
     * Método que recibe como parámetro una cadena con el texto
     * a buscar, explora la base buscando en el nombre de la
     * instituciòn, el responsable, la provincia y retorna un
     * vector con los registros encontrados
     */

    /**
     * @return clave del registro afectado
     * Método que produce la consulta de ediciòn o alta segùn
     * el caso y retorna la id del registro afectado
     */

}