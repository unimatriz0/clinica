/*

    Nombre: FormInstituciones
    Fecha: 04/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que arma el tabulador con los datos de las
                 Instituciones asistenciales

 */

// definición del paquete
package instituciones;

// importamos las librerías
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

// definición de la clase
public class FormInstituciones extends JPanel {

	// agregamos el serial id
	private static final long serialVersionUID = 1L;

	// declaramos las variables
	protected JPanel Institucion;

	// creamos el diálogo que recibe como parámetro el
	// tabulador
	public FormInstituciones(JTabbedPane Tabulador) {

		// inicializamos el contenedor
		this.Institucion = new JPanel();
		this.Institucion.setBounds(0, 0, 945, 620);
		this.Institucion.setLayout(null);
		this.Institucion.setToolTipText("Datos de las Instituciones Asistenciales");

		 // agregamos el panel al tabulador
        Tabulador.addTab("Instituciones",
                          new javax.swing.ImageIcon(getClass().getResource("/Graficos/mCaduceus.png")),
                          this.Institucion,
                          "Datos de las Instituciones Asistenciales");

	}

}
