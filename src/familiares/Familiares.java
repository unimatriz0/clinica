/*

    Nombre: Familiares
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 antecedentes familiares, se asume que esta tabla tendrá
                 solo una entrada por cada paciente

*/

 // declaración del paquete
package familiares;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Familiares {

    // declaración de variables
    protected Conexion Enlace;          // puntero a la base de datos
    protected int Id;                   // clave del registro
    protected int Paciente;             // clave del paciente
    protected int Subita;               // muerte súbita
    protected int Cardiopatia;          // 0 no 1 si
    protected int Disfagia;             // 0 no 1 si
    protected int NoSabe;               // 0 no 1 si
    protected int NoTiene;              // 0 no 1 si
    protected String Otra;              // descripción de otro antecedente
    protected int IdUsuario;            // clave del usuario
    protected String Usuario;           // nombre del usuario
    protected String Fecha;             // fecha de alta del registro

    // constructor de la clase
    public Familiares(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Paciente = 0;
        this.Subita = 0;
        this.Cardiopatia = 0;
        this.Disfagia = 0;
        this.NoSabe = 0;
        this.NoTiene = 0;
        this.Otra = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setSubita(int subita){
        this.Subita = subita;
    }
    public void setCardiopatia(int cardio){
        this.Cardiopatia = cardio;
    }
    public void setDisfagia(int disfagia){
        this.Disfagia = disfagia;
    }
    public void setNoSabe(int nosabe){
        this.NoSabe = nosabe;
    }
    public void setNoTiene(int notiene){
        this.NoTiene = notiene;
    }
    public void setOtra(String otra){
        this.Otra = otra;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getSubita(){
        return this.Subita;
    }
    public int getCardiopatia(){
        return this.Cardiopatia;
    }
    public int getDisfagia(){
        return this.Disfagia;
    }
    public int getNoSabe(){
        return this.NoSabe;
    }
    public int getNoTiene(){
        return this.NoTiene;
    }
    public String getOtra(){
        return this.Otra;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idpaciente entero con la clave del paciente
     * Método que recibe como parámetro la clave del paciente
     * y asigna en las variables de clase los valores del registro
     */
    public void getDatosFamiliares(int idpaciente){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_familiares.id AS id, " +
                   "       diagnostico.v_familiares.paciente AS paciente, " +
                   "       diagnostico.v_familiares.subita AS subita, " +
                   "       diagnostico.v_familiares.cardiopatia AS cardiopatia, " +
                   "       diagnostico.v_familiares.disfagia AS disfagia, " +
                   "       diagnostico.v_familiares.nosabe AS nosabe, " +
                   "       diagnostico.v_familiares.notiene AS notiene, " +
                   "       diagnostico.v_familiares.otra AS otra, " +
                   "       diagnostico.v_familiares.usuario AS usuario, " +
                   "       diagnostico.v_familiares.fecha AS fecha " +
                   "FROM diagnostico.v_familiares " +
                   "WHERE diagnostico.v_familiares.paciente = '" + idpaciente + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Paciente = Resultado.getInt("paciente");
            this.Subita = Resultado.getInt("subita");
            this.Cardiopatia = Resultado.getInt("cardiopatia");
            this.Disfagia = Resultado.getInt("disfagia");
            this.NoSabe = Resultado.getInt("nosabe");
            this.NoTiene = Resultado.getInt("notiene");
            this.Otra = Resultado.getString("otra");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero clave del registro afectado
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaFamiliares(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoFamiliares();
        } else {
            this.editaFamiliares();
        }

        // retorna la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoFamiliares(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.familiares " +
                   "            (paciente, " +
                   "             subita, " +
                   "             cardiopatia, " +
                   "             disfagia, " +
                   "             nosabe, " +
                   "             notiene, " +
                   "             otra, " +
                   "             usuario) " +
                   "            VALUES " +
                   "            (?, ?, ?, ?, ?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Paciente);
            psInsertar.setInt(2, this.Subita);
            psInsertar.setInt(3, this.Cardiopatia);
            psInsertar.setInt(4, this.Disfagia);
            psInsertar.setInt(5, this.NoSabe);
            psInsertar.setInt(6, this.NoTiene);
            psInsertar.setString(7, this.Otra);
            psInsertar.setInt(8, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaFamiliares(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.familiares SET " +
                   "       subita = ?, " +
                   "       cardiopatia = ?, " +
                   "       disfagia = ?, " +
                   "       nosabe = ?, " +
                   "       notiene = ?, " +
                   "       otra = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.familiares.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Subita);
            psInsertar.setInt(2, this.Cardiopatia);
            psInsertar.setInt(3, this.Disfagia);
            psInsertar.setInt(4, this.NoSabe);
            psInsertar.setInt(5, this.NoTiene);
            psInsertar.setString(6, this.Otra);
            psInsertar.setInt(7, this.IdUsuario);
            psInsertar.setInt(8, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param protocolo - entero con la clave del protocolo
     * @return boolean si existen antecedentes
     * Método utilizado al cargar el formulario que verifica
     * si ya se declararon los datos del familiar, retorna
     * verdadero si ya fueron declarados o falso si no
     * hay datos declarados
     */
    public Boolean existeFamiliar(int protocolo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Correcto = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.familiares.id) AS registros " +
                   "FROM diagnostico.familiares " +
                   "WHERE diagnostico.familiares.paciente = '" + protocolo + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();

            // si hay resultados
            if (Resultado.getInt("registros") != 0){

                // fijamos la variable, si no hay ya
                // está en false
                Correcto = true;

                // ya cargamos el registro en memoria
                this.getDatosFamiliares(protocolo);

            }

		} catch (SQLException e) {
			e.printStackTrace();
		}

        // retornamos
        return Correcto;

    }
}
