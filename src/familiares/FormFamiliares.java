/*

    Nombre: FormFamiliares
    Fecha: 21/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el abm de
	             antecedentes familiares

*/

// definición del paquete
package familiares;

// importamos las librerías
import java.awt.Frame;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import funciones.Utilidades;
import seguridad.Seguridad;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import familiares.Familiares;

// definición de la clase
public class FormFamiliares extends JDialog {

	// declaramos el serial id
	private static final long serialVersionUID = 262270721688832930L;

	// declaración de las variables de clase
	private JTextField tFecha;                 // fecha de alta
	private JTextField tUsuario;               // usuario de la base
	private JTextArea tOtras;                  // otras enfermedades
	private JCheckBox cSubita;                 // checkbox de muerte súbita
	private JCheckBox cCardiopatia;            // checkbox de cardiopatía
	private JCheckBox cDisfagia;               // checkbox de disfagia
	private JCheckBox cNoSabe;                 // checkbox de no sabe
	private JCheckBox cNoTiene;                // checkbox de no tiene
	public int Protocolo;                      // protocolo del paciente
	private int Id;                            // clave del registro
	private Familiares Parientes;              // clase de la base de datos

	// constructor de la clase
	public FormFamiliares(Frame parent, boolean modal) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// inicializamos las variables
		this.Protocolo = 0;
		this.Id = 0;
		this.Parientes = new Familiares();

		// fijamos las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setTitle("Antecedentes Familiares");
		this.setBounds(150, 150, 590, 349);
		this.setLayout(null);

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Antecedentes Familiares del Paciente</b></html>");
		lTitulo.setBounds(10, 10, 530, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// check de muerte súbita
		this.cSubita = new JCheckBox("Muerte Súbita");
		this.cSubita.setBounds(10, 40, 140, 26);
		this.cSubita.setToolTipText("Marque si hay antecedentes de muerte súbita");
		this.cSubita.setFont(Fuente);
		this.add(this.cSubita);

		// check de cardiopatía
		this.cCardiopatia = new JCheckBox("Cardiopatía");
		this.cCardiopatia.setBounds(150, 40, 120, 26);
		this.cCardiopatia.setToolTipText("Marque si hay antecedentes de cardipatía");
		this.cCardiopatia.setFont(Fuente);
		this.add(this.cCardiopatia);

		// check de disfagia
		this.cDisfagia = new JCheckBox("Disfagia");
		this.cDisfagia.setBounds(290, 40, 110, 26);
		this.cDisfagia.setToolTipText("Marque si hay antecedentes de disfagia");
		this.cDisfagia.setFont(Fuente);
		this.add(this.cDisfagia);

		// check de no sabe
		this.cNoSabe = new JCheckBox("No Sabe");
		this.cNoSabe.setBounds(10, 75, 120, 26);
		this.cNoSabe.setToolTipText("Marque si el paciente no conoce de antecedentes");
		this.cNoSabe.setFont(Fuente);
		this.add(this.cNoSabe);

		// check de no tiene
		this.cNoTiene = new JCheckBox("No Tiene");
		this.cNoTiene.setBounds(150, 75, 120, 26);
		this.cNoTiene.setToolTipText("Marque si el paciente no tiene antecedentes");
		this.cNoTiene.setFont(Fuente);
		this.add(this.cNoTiene);

		// el usuario
		JLabel lUsuario = new JLabel("Usuario: ");
		lUsuario.setBounds(400, 40, 70, 26);
		lUsuario.setFont(Fuente);
		this.add(lUsuario);
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(485, 40, 90, 26);
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.add(this.tUsuario);

		// la fecha de alta
		JLabel lFecha = new JLabel("Fecha Alta:");
		lFecha.setBounds(400, 75, 96, 26);
		lFecha.setFont(Fuente);
		this.add(lFecha);
		this.tFecha = new JTextField();
		this.tFecha.setBounds(485, 75, 90, 26);
		this.tFecha.setFont(Fuente);
		this.tFecha.setToolTipText("Fecha de alta del registro");
		this.tFecha.setEditable(false);
		this.add(this.tFecha);

		// fijamos el usuario y la fecha de alta
		Utilidades Herramientas = new Utilidades();
		this.tUsuario.setText(Seguridad.Usuario);
		this.tFecha.setText(Herramientas.FechaActual());

		// el label de otras enfermedades
		JLabel lOtras = new JLabel("<html><b>Otras Enfermedades</b></html>");
		lOtras.setBounds(10, 110, 200, 26);
		lOtras.setFont(Fuente);
		this.add(lOtras);

		// el scroll de otras enfermedades
		JScrollPane scrollOtras = new JScrollPane();
		scrollOtras.setBounds(10, 145, 400, 160);
		this.add(scrollOtras);

		// el área de texto
		this.tOtras = new JTextArea();
		this.tOtras.setFont(Fuente);
		scrollOtras.setViewportView(this.tOtras);

		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(445, 145, 120, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaFamiliar();
            }
        });
		this.add(btnGrabar);

		// el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(445, 180, 120, 26);
		btnCancelar.setToolTipText("Cierra sin grabar los cambios");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaFamiliar();
            }
        });
		this.add(btnCancelar);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Mètodo público que carga en el formulario los
	 * antecedentes familiares
	 */
	public void verFamiliares(){

		// verificamos si existe y obtenemos los datos
		if (this.Parientes.existeFamiliar(this.Protocolo)){

			// cargamos en el formulario
			this.Id = this.Parientes.getId();

			// según el estado de los check
			if (this.Parientes.getSubita() == 1){
				this.cSubita.setSelected(true);
			} else {
				this.cSubita.setSelected(false);
			}
			if (this.Parientes.getDisfagia() == 1){
				this.cDisfagia.setSelected(true);
			} else {
				this.cDisfagia.setSelected(false);
			}
			if (this.Parientes.getCardiopatia() == 1){
				this.cCardiopatia.setSelected(true);
			} else {
				this.cCardiopatia.setSelected(false);
			}
			if (this.Parientes.getNoSabe() == 1){
				this.cNoSabe.setSelected(true);
			} else {
				this.cNoSabe.setSelected(false);
			}
			if (this.Parientes.getNoTiene() == 1){
				this.cNoTiene.setSelected(true);
			} else {
				this.cNoTiene.setSelected(false);
			}

			// cargamos las otras enfermedades
			this.tOtras.setText(this.Parientes.getOtra());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que valida el formulario antes de enviarlo
	 * al servidor
	 */
	private void validaFamiliar(){

		// verifica que al menos halla marcado un elemento
		Boolean Correcto = false;
		if (this.cSubita.isSelected()){
			Correcto = true;
		} else if (this.cCardiopatia.isSelected()){
			Correcto = true;
		} else if (this.cDisfagia.isSelected()){
			Correcto = true;
		} else if (this.cNoSabe.isSelected()){
			Correcto = true;
		} else if (this.cNoTiene.isSelected()){
			Correcto = true;
		} else if (!this.tOtras.getText().isEmpty()){
			Correcto = true;
		}

		// si no marcó nada y no ingresó texto
		if (!Correcto){

			// presenta el mensaje
            JOptionPane.showMessageDialog(this,
						"Debe marcar una enfermedad preexistente\n" +
						"o al menos ingresar un comentario",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);

		// si grabó alguna
		} else {

			// grabamos el registro
			this.grabaFamiliar();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de validar el formulario que
	 * ejecuta la consulta en el servidor
	 */
	private void grabaFamiliar(){

		// los datos
		this.Parientes.setId(this.Id);
		this.Parientes.setPaciente(this.Protocolo);

		// según los checbox
		if (this.cSubita.isSelected()){
			this.Parientes.setSubita(1);
		} else {
			this.Parientes.setSubita(0);
		}
		if (this.cCardiopatia.isSelected()){
			this.Parientes.setCardiopatia(1);
		} else {
			this.Parientes.setCardiopatia(0);
		}
		if (this.cDisfagia.isSelected()){
			this.Parientes.setDisfagia(1);
		} else {
			this.Parientes.setDisfagia(0);
		}
		if (this.cNoSabe.isSelected()){
			this.Parientes.setNoSabe(1);
		} else {
			this.Parientes.setNoSabe(0);
		}
		if (this.cNoTiene.isSelected()){
			this.Parientes.setNoTiene(1);
		} else {
			this.Parientes.setNoTiene(0);
		}

		// fijamos los comentarios
		this.Parientes.setOtra(this.tOtras.getText());

		// grabamos el registro
		this.Id = this.Parientes.grabaFamiliares();

		// cerramos el formulario
		this.dispose();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * simplemente cierra el formulario
	 */
	private void cancelaFamiliar(){

		// cerramos el formulario
		this.dispose();

	}

}
