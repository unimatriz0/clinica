/*

    Nombre: formIngreso
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Formulario que solicita al usuario se acredite antes de
                 permitirle ingresar al sistema

 */

// definición del paquete
package seguridad;

// importamos las librerías
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import contenedor.FormContenedor;
import javax.swing.JFrame;
import dbApi.FormConfiguracion;
import funciones.Utilidades;
import org.jvnet.substance.*;
import dbApi.Config;
import java.awt.Font;

/**
 * @author Lic. Claudio Invernizzi
 */
public class FormIngreso extends javax.swing.JFrame {

	// añadimos el serial uid
	private static final long serialVersionUID = -98832082761435182L;

	// declaración de variables
    private JButton BtnIngresar;
    private JPasswordField tPassword;
    private JTextField tUsuario;

    /**
     * Creamos el formulario
     */
    public FormIngreso () {

    	// antes de crear el formulario verificamos que exista
    	// el directorio temporal y el de gráficos
    	File directorio = new File("temp");
    	directorio.mkdir();
    	directorio = new File("Graficos");
    	directorio.mkdir();

    	// verificamos que existan los gráficos
    	String logo = (new File (".").getAbsolutePath ()) + "/Graficos/logo_fatala.jpg";
    	File archivo = new File(logo);
    	if (!archivo.exists()) {

    		// descargamos el logo
    		Utilidades Herramientas = new Utilidades();
    		Herramientas.descargaArchivo("http://fatalachaben.info.tm/sitracha/imagenes/logo_fatala.jpg", logo);

    		// descargamos el separador
    		String separador = (new File (".").getAbsolutePath ()) + "/Graficos/separador.png";
    		Herramientas.descargaArchivo("http://fatalachaben.info.tm/sitracha/imagenes/separador.png", separador);

    		// descargamos el imagen no disponible
    		String imagen = (new File (".").getAbsolutePath ()) + "/Graficos/sin_imagen.jpg";
    		Herramientas.descargaArchivo("http://fatalachaben.info.tm/sitracha/imagenes/sin_imagen.jpg", imagen);

    	}

        // definimos el formato
        this.setLayout(null);
        this.setBounds(200, 200, 400, 230);

        // establecemos el icono del formulario y la salida
        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        this.setTitle("Sistema de Diagnóstico");
        this.setIconImage(new ImageIcon(getClass().getResource("/Graficos/logo_fatala.png")).getImage());

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // creamos el label de titulo
        JLabel lTitulo = new JLabel("<html><b>Indique sus credenciales para acceder<b></html>");
        lTitulo.setFont(Fuente);
        lTitulo.setBounds(6, 6, 300, 26);
        this.add(lTitulo);

        // el label con el nombre de usuario
        JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setFont(Fuente);
        lUsuario.setBounds(160, 50, 100, 26);
        this.add(lUsuario);

        // el texto del usuario y agregamos el evento keypress
        this.tUsuario = new JTextField();
        this.tUsuario.setFont(Fuente);
        this.tUsuario.setToolTipText("Su nombre de usuario");
        this.tUsuario.setBounds(250, 50, 100, 26);
        this.add(this.tUsuario);
        this.tUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tUsuarioKeyPressed(evt);
            }
        });

        // el label de contraseña
        JLabel lPassword = new JLabel("Contraseña:");
        lPassword.setFont(Fuente);
        lPassword.setBounds(160, 100, 100, 26);
        this.add(lPassword);

        // el texto del password y el evento keypress
        this.tPassword = new JPasswordField();
        this.tPassword.setToolTipText("Ingrese su contraseña");
        this.tPassword.setFont(Fuente);
        this.tPassword.setBounds(250, 100, 100, 26);
        this.add(this.tPassword);
        this.tPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tPasswordKeyPressed(evt);
            }
        });

        // el boton ingresar
        this.BtnIngresar = new JButton("Ingresar");
        this.BtnIngresar.setToolTipText("Pulse para ingresar");
        this.BtnIngresar.setFont(Fuente);
        this.BtnIngresar.setBounds(190, 150, 125, 30);
        this.add(this.BtnIngresar);
        this.BtnIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnIngresarActionPerformed(evt);
            }
        });

        // el icono del boton ingresar
        BtnIngresar.setIcon(new ImageIcon(FormIngreso.class.getResource("/Graficos/musuarios.png")));

        // el logo del instituto
        JLabel lLogo = new JLabel();
        lLogo.setBounds(10, 50, 140, 120);
        lLogo.setIcon(new ImageIcon(FormIngreso.class.getResource("/Graficos/logo_fatala.png")));
        this.add(lLogo);

        // fijamos el foco
        this.tUsuario.requestFocus();

    }

    // evento al hacer click sobre el botón ingresar
    private void BtnIngresarActionPerformed(java.awt.event.ActionEvent evt) {

        // llamamos la rutina de verificación
        this.VerificaIngreso();

    }

    // rutina que verifica las credenciales del usuario
    @SuppressWarnings("deprecation")
	private void VerificaIngreso() {

        // verificamos que halla ingresado el nombre de usuario
        if (this.tUsuario.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                                          "Ingrese su nombre de usuario",
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();
            return;

        }

        // verificamos que halla ingresado la contraseña
        if (this.tPassword.getText().isEmpty()) {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                                         "Debe ingresar su contraseña",
                                         "Error",
                                         JOptionPane.ERROR_MESSAGE);
            this.tPassword.requestFocus();
            return;

        }

        // instanciamos la clase seguridad
        Seguridad Acceso = new Seguridad();

        // si no hubo un error
        if (Acceso.Validar(this.tUsuario.getText(), this.tPassword.getText())) {

            // instanciamos el formulario de datos y ocultamos este
        	new FormContenedor();

            // ocultamos este formulario
            this.dispose();

            // si hubo error
        } else {

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this,
                                         "Hay un error en las credenciales",
                                         "Error",
                                         JOptionPane.ERROR_MESSAGE);
            this.tUsuario.requestFocus();

        }

    }

    // evento al pulsar una tecla sobre el nombre de usuario
    private void tUsuarioKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.tPassword.requestFocus();
        }

    }

    // evento al pulsar enter sobre el password
    private void tPasswordKeyPressed(java.awt.event.KeyEvent evt) {

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            this.BtnIngresar.requestFocus();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param args the command line arguments
     * Método que inicializa la aplicación y muestra el formulario
     * de acceso
     */
    public static void main(String args[]) {

    	// fijamos el tema sin afectar el frame
        JFrame.setDefaultLookAndFeelDecorated(false);

        // creamos y mostramos el formulario
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                // primero verificamos si existe un archivo de configuración
                File Archivo = new File("Configuracion.properties");
                if (!Archivo.exists()) {

                	// fijamos el tema por defecto
                	SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.RavenGraphiteSkin");

                    // llamamos el formulario de configuración y lo mostramos
                    // pasándole este formulario como argumento
                    FormConfiguracion Configurar = new FormConfiguracion();
                    Configurar.setVisible(true);

                // si hay un archivo de configuración
                } else {

                    // verificamos si existe un tema por defecto
                    Config configuracion = new Config();
                    String tema = configuracion.getTema();

                    // si hay un tema seleccionado
                    if (tema != null){
                        SubstanceLookAndFeel.setSkin(tema);
                    } else {
                        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.RavenGraphiteSkin");
                    }

                    // muestra el formulario de ingreso
                    new FormIngreso().setVisible(true);

                }

            }

        });

    }

}
