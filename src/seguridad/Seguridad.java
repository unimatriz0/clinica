/*

    Nombre: seguridad
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones de acceso de los usuarios
                 autorizados

 */

// declaración del paquete
package seguridad;

// importación de archivos
import dbApi.Conexion;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Lic. Claudio Invernizzi
 */
public class Seguridad {

    // declaración de variables
    protected Conexion Enlace;
    public static int Id;                  // clave del registro
    public static String Usuario;          // nombre del usuario
    public static String Nombre;           // nombre completo del usuario
    public static String Password;         // contraseña de acceso sin encriptar
    public static String Jurisdiccion;     // jurisdicción del usuario
    public static String Pais;             // pais del usuario
    public static String NivelCentral;     // indica si es de nivel central
    public static int Laboratorio;         // laboratorio al que pertenece
    public static String Administrador;    // si es administrador
    public static String Personas;         // si está autorizado a editar personas
    public static String Congenito;        // si está autorizado a editar chagas congenito
    public static String Resultados;       // si puede cargar resultados
    public static String Muestras;         // si puede cargar muestras
    public static String Entrevistas;      // si puede cargar entrevistas
    public static String Auxiliares;       // si puede editar tablas auxiliares
    public static String Protocolos;       // si puede imprimir protocolos
    public static String Stock;            // si puede editar el stock
    public static String Usuarios;         // si puede editar usuarios del sistema
    public static String Clinica;          // si puede cargar datos de clínica médica

    // constructor de la clase
    public Seguridad(){

        // establecemos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        Seguridad.Id = 0;
        Seguridad.Usuario = null;
        Seguridad.Nombre = null;
        Seguridad.Password = null;
        Seguridad.Jurisdiccion = null;
        Seguridad.Pais = null;
        Seguridad.NivelCentral = null;
        Seguridad.Laboratorio = 0;
        Seguridad.Administrador = "";
        Seguridad.Personas = "";
        Seguridad.Congenito = "";
        Seguridad.Resultados = "";
        Seguridad.Muestras = "";
        Seguridad.Entrevistas = "";
        Seguridad.Auxiliares = "";
        Seguridad.Protocolos = "";
        Seguridad.Stock = "";
        Seguridad.Usuarios = "";
        Seguridad.Clinica = "";

    }

    /**
     * @param usuario el usuario ingresado
     * @param password el password ingresado
     * @return boolean, el estado de la validación
     * Método que verifica que exista el usuario en la base y que la
     * contraseña de ingreso sea correcta, en cuyo caso retorna
     * verdadero
     */
    public boolean Validar(String usuario, String password){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // buscamos el usuario en la base
        Consulta = "SELECT diagnostico.v_usuarios.idusuario AS id, "
                + "        diagnostico.v_usuarios.nombreusuario AS nombre, "
                + "        diagnostico.v_usuarios.usuario AS usuario, "
                + "        diagnostico.v_usuarios.idlaboratorio AS laboratorio, "
                + "        diagnostico.v_usuarios.provincia AS provincia, "
                + "        diagnostico.v_usuarios.pais AS pais, "
                + "        diagnostico.v_usuarios.nivelcentral AS nivel_central, "
                + "        diagnostico.v_usuarios.administrador AS administrador, "
                + "        diagnostico.v_usuarios.personas AS personas, "
                + "        diagnostico.v_usuarios.congenito AS congenito, "
                + "        diagnostico.v_usuarios.resultados AS resultados, "
                + "        diagnostico.v_usuarios.muestras AS muestras, "
                + "        diagnostico.v_usuarios.entrevistas AS entrevistas, "
                + "        diagnostico.v_usuarios.auxiliares AS auxiliares, "
                + "        diagnostico.v_usuarios.protocolos AS protocolos, "
                + "        diagnostico.v_usuarios.stock AS stock, "
                + "        diagnostico.v_usuarios.usuarios AS usuarios, "
                + "        diagnostico.v_usuarios.clinica AS clinica "
                + " FROM diagnostico.v_usuarios "
                + " WHERE diagnostico.v_usuarios.usuario = '" + usuario + "' AND "
                + "       diagnostico.v_usuarios.password = MD5('" + password + "') AND "
                + "       diagnostico.v_usuarios.activo = 'Si';";
        Resultado = this.Enlace.Consultar(Consulta);

        // si encontró registros
        try {

            // si hay registros
            if (Resultado.next()){

                // obtenemos el password de la base
                Seguridad.Id = Resultado.getInt("id");
                Seguridad.Nombre = Resultado.getString("nombre");
                Seguridad.Usuario = Resultado.getString("usuario");
                Seguridad.Pais = Resultado.getString("pais");
                Seguridad.Laboratorio = Resultado.getInt("laboratorio");
                Seguridad.Jurisdiccion = Resultado.getString("provincia");
                Seguridad.NivelCentral = Resultado.getString("nivel_central");
                Seguridad.Administrador = Resultado.getString("administrador");;
                Seguridad.Personas = Resultado.getString("personas");
                Seguridad.Congenito = Resultado.getString("congenito");
                Seguridad.Resultados = Resultado.getString("resultados");
                Seguridad.Muestras = Resultado.getString("muestras");
                Seguridad.Entrevistas = Resultado.getString("entrevistas");
                Seguridad.Auxiliares = Resultado.getString("auxiliares");
                Seguridad.Protocolos = Resultado.getString("protocolos");
                Seguridad.Stock = Resultado.getString("stock");
                Seguridad.Usuarios = Resultado.getString("usuarios");
                Seguridad.Clinica = Resultado.getString("clinica");
                Seguridad.Password = password;

                // retornamos verdadero
                return true;

            // si no hay registros
            } else {

                // retorna false
                return false;

            }

        // si hubo un errror
        } catch (SQLException ex){

            // retorna falso
            System.out.println(ex);
            return false;

        }

    }

}