/*

    Nombre: formNuevoPass
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Formulario que permite al usuario modificar su contraseña
                 de ingreso

 */

// definición del paquete
package seguridad;

// importación de librerías
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import usuarios.Usuarios;
import java.awt.Font;

/**
 * @author Claudio Invernizzi
 * Definición del formulario
 */
public class formNuevoPass extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 8503108035714978012L;
		
	// Declaración de variables
    protected javax.swing.JPasswordField tNuevoPassword;
    protected javax.swing.JPasswordField tPasswordActual;
    protected javax.swing.JPasswordField tVerificacion;
    
    /**
     * @param parent el formulario padre
     * @param modal valor que indica si será modal
     * Creates new form formNuevoPass
     */
    public formNuevoPass(java.awt.Frame parent, boolean modal) {
        super(parent, modal);

        // definimos el formato
        this.setLayout(null);

        // definimos el título y la operación
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Nueva Contraseña");
        this.setResizable(false);
        this.setBounds(100,100,400,280);        
        
        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presentamos el título
        JLabel lTitulo = new JLabel("<html><b>Cambio de Contraseña</b></html>");
        lTitulo.setBounds(20, 10, 350, 26);
        lTitulo.setFont(Fuente);
        this.add(lTitulo);
        
        // el label de password actual
        JLabel lPasswordActual = new JLabel("Contraseña Actual:");
        lPasswordActual.setBounds(50, 50, 150, 26);
        lPasswordActual.setFont(Fuente);
        this.add(lPasswordActual);

        // el texto de password actual
        this.tPasswordActual = new javax.swing.JPasswordField();
        this.tPasswordActual.setBounds(210,50,110,26);
        this.tPasswordActual.setFont(Fuente);
        this.add(this.tPasswordActual);
        this.tPasswordActual.setToolTipText("Contraseña Actual de Ingreso");
        this.tPasswordActual.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tPasswordActualKeyPressed(evt);
            }
        });

        // el label de nuevo password
        JLabel lNuevoPassword = new JLabel("Nueva Contraseña:");
        lNuevoPassword.setBounds(50, 100, 150, 26);
        lNuevoPassword.setFont(Fuente);
        this.add(lNuevoPassword);
        
        // el texto de nuevo password
        this.tNuevoPassword = new javax.swing.JPasswordField();
        this.tNuevoPassword.setBounds(210, 100, 110, 26);
        this.tNuevoPassword.setFont(Fuente);
        this.add(this.tNuevoPassword);
        this.tNuevoPassword.setToolTipText("Nueva Contraseña de Ingreso");
        this.tNuevoPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tNuevoPasswordKeyPressed(evt);
            }
        });
        
        // el label de verificación
        JLabel lVerificacion = new JLabel("Verificación:");
        lVerificacion.setBounds(50,150,150,26);
        lVerificacion.setFont(Fuente);
        this.add(lVerificacion);
        
        // el texto de verificación
        this.tVerificacion = new javax.swing.JPasswordField();
        this.tVerificacion.setBounds(210,150,110,26);
        this.tVerificacion.setFont(Fuente);
        this.add(this.tVerificacion);
        this.tVerificacion.setToolTipText("Repita su nueva contraseña");

        // los botones de acción
        JButton btnConfirmar = new JButton("Actualizar");
        btnConfirmar.setBounds(50,200,135,30);
        btnConfirmar.setFont(Fuente);
        this.add(btnConfirmar);
        btnConfirmar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/mexito.png")));
        btnConfirmar.setToolTipText("Pulse para actualizar su password");
        btnConfirmar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

                // actualiza el usuario
                actualizaPassword();
                            	
            }
        });

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setBounds(210,200,135,30);
        btnCancelar.setFont(Fuente);
        this.add(btnCancelar);
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Graficos/merror.png")));
        btnCancelar.setToolTipText("Cierra este formulario");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	
                // simplemente cerramos el formulario
                dispose();
                
            }
        });

    }

    // evento al pulsar sobre el password actual
    private void tPasswordActualKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            this.tNuevoPassword.requestFocus();
        }
        
    }

    // evento al pulsar enter sobre el nuevo password
    private void tNuevoPasswordKeyPressed(java.awt.event.KeyEvent evt) {

        // si pulsó enter
        if(evt.getKeyCode() == KeyEvent.VK_ENTER){
            this.tVerificacion.requestFocus();
        }
        
    }

    // método que verifica el formulario y luego actualiza
    // el ingreso del usuario
    @SuppressWarnings("deprecation")
	private void actualizaPassword(){

        // verificamos se halla ingresado el password
        if (this.tPasswordActual.getText().isEmpty()){
            
            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
                                          "Debe ingresar su contraseña actual", 
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            this.tPasswordActual.requestFocus();
            return;
            
        }
        
        // verificamos que se halla ingresado la  nueva contraseña
        if (this.tNuevoPassword.getText().isEmpty()){
            
            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
                                          "Debe ingresar su nueva contraseña", 
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            this.tNuevoPassword.requestFocus();
            return;
            
        }
        
        // verificamos que la nueva contraseña sea consistente
        if(!this.tNuevoPassword.getText().equals(this.tVerificacion.getText())){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
                                          "Las contraseñas no coinciden, Verifique", 
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            this.tNuevoPassword.requestFocus();
            return;
            
        }
        
        // verificamos que el password ingresado coincida
        // con la acreditación
        if (!Seguridad.Password.equals(this.tPasswordActual.getText())){
            
            // presenta el mensaje
            JOptionPane.showMessageDialog(this, 
                                          "La contraseña actual no coincide", 
                                          "Error",
                                          JOptionPane.ERROR_MESSAGE);
            this.tPasswordActual.requestFocus();
            return;
            
        }
        
        // asignamos el valor en la clase de usuarios
        Usuarios nuevoPass = new Usuarios();
        boolean Resultado = nuevoPass.actualizaPassword(this.tNuevoPassword.getText());
               
        if (Resultado){
        	
	        // si anduvo todo bien presenta el mensaje y 
	        // cierra el diálogo
	        JOptionPane.showMessageDialog(this, 
	                                      "Contraseña Actualizada", 
	                                      "Exito",
	                                      JOptionPane.INFORMATION_MESSAGE);
	        this.dispose();

	    // si hubo un error
        } else {

        	// presenta el mensaje
	        JOptionPane.showMessageDialog(this, 
                    "No se ha actualizado la contraseña", 
                    "Error",
                    JOptionPane.ERROR_MESSAGE);
        	
        }
        
    }
    
}
