/*

    Nombre: FormTemperaturas
    Fecha: 28/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que instancia el formulario de carga de temperaturas

 */

// definición del paquete
package temperaturas;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JOptionPane;
import com.toedter.calendar.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import javax.swing.JScrollPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import heladeras.Heladeras;
import javax.swing.JTextField;
import funciones.ComboClave;
import funciones.Utilidades;
import java.awt.Font;

public class FormTemperaturas extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;
	
	// declaración de variables
	private JTable tLecturas;
	private JDateChooser dFechaLectura;
	private JButton btnGrabar;
	private JButton btnCancelar;
	private JSpinner sTemperatura1;
	private JSpinner sTemperatura2;
	private JButton btnExportar;
	private JComboBox<Object> cNomina;
	private JTextField tId;
	private JTextField tPatrimonio;
	private JTextField tControlo;
	private Heladeras Freezer;
	private Temperaturas Lecturas;
	private Utilidades Herramientas;

	/**
	 * Definimos el diálogo
	 * @param parent - la ventana padre
	 * @param modal  - el modo de presentación
	 */
	public FormTemperaturas(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// posicionamos el formulario y fijamos las propiedades
		setBounds(100, 100, 705, 486);
		getContentPane().setLayout(null);

		// iniciamos el formulario
		this.initFormTemperaturas();

		// cargamos la nómina de heladeras
		this.cargaHeladeras();

	}

	/**
	 * @author Claudio Invernizzi <vinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	@SuppressWarnings("serial")
	protected void initFormTemperaturas(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// instanciamos las clases
		this.Freezer = new Heladeras();
		this.Lecturas = new Temperaturas();
		this.Herramientas = new Utilidades();

		// presentamos el título
		JLabel lTitulo = new JLabel("Nómina de Temperaturas");
		lTitulo.setBounds(200, 10, 179, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presenta el select
		JLabel lNomina = new JLabel("Seleccione la Heladera:");
		lNomina.setBounds(12, 45, 179, 26);
		lNomina.setFont(Fuente);
		getContentPane().add(lNomina);
		this.cNomina = new JComboBox<>();
		this.cNomina.setBounds(180, 45, 175, 26);
		this.cNomina.setFont(Fuente);
		this.cNomina.setToolTipText("Seleccione la heladera de la lista");
		getContentPane().add(this.cNomina);

		// el evento onchange del select
        this.cNomina.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cNominaItemStateChanged(evt);
            }
        });

		// presenta el número de patrimonio
		JLabel lPatrimonio = new JLabel("Patrimonio:");
		lPatrimonio.setBounds(364, 45, 89, 26);
		lPatrimonio.setFont(Fuente);
		getContentPane().add(lPatrimonio);
		this.tPatrimonio = new JTextField();
		this.tPatrimonio.setBounds(445, 45, 147, 26);
		this.tPatrimonio.setFont(Fuente);
		this.tPatrimonio.setEditable(false);
		this.tPatrimonio.setToolTipText("Número de registro de patrimonio");
		getContentPane().add(this.tPatrimonio);

		// presenta la id de la lectura
		JLabel lId = new JLabel("ID:");
		lId.setBounds(12, 80, 35, 26);
		lId.setFont(Fuente);
		getContentPane().add(lId);
		this.tId = new JTextField();
		this.tId.setBounds(37, 80, 60, 26);
		this.tId.setFont(Fuente);
		this.tId.setEditable(false);
		this.tId.setToolTipText("Clave de la lectura");
		getContentPane().add(this.tId);

		// la primer lectura
		JLabel lTemperatura1 = new JLabel("1° Lectura:");
		lTemperatura1.setBounds(108, 80, 89, 26);
		lTemperatura1.setFont(Fuente);
		getContentPane().add(lTemperatura1);
		this.sTemperatura1 = new JSpinner();
		this.sTemperatura1.setBounds(191, 80, 50, 26);
		this.sTemperatura1.setFont(Fuente);
		this.sTemperatura1.setToolTipText("Indique la temperatura obtenida");
		getContentPane().add(this.sTemperatura1);

		// la segunda lectura
		JLabel lTemperatura2 = new JLabel("2° Lectura:");
		lTemperatura2.setBounds(245, 80, 83, 26);
		lTemperatura2.setFont(Fuente);
		getContentPane().add(lTemperatura2);
		this.sTemperatura2 = new JSpinner();
		this.sTemperatura2.setBounds(327, 80, 50, 26);
		this.sTemperatura2.setFont(Fuente);
		this.sTemperatura2.setToolTipText("Indique la temperatura obtenida");
		getContentPane().add(this.sTemperatura2);

		// la fecha de lectura
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setBounds(389, 80, 60, 26);
		lFecha.setFont(Fuente);
		getContentPane().add(lFecha);
        this.dFechaLectura = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaLectura.setToolTipText("Fecha de toma de lectura");
		this.dFechaLectura.setBounds(442,80,105,26);
		this.dFechaLectura.setFont(Fuente);
        getContentPane().add(this.dFechaLectura);

		// el usuario que controló
		JLabel lControlo = new JLabel("Controló: ");
		lControlo.setBounds(12, 115, 70, 26);
		lControlo.setFont(Fuente);
		getContentPane().add(lControlo);
		this.tControlo = new JTextField();
		this.tControlo.setBounds(85, 115, 115, 26);
		this.tControlo.setFont(Fuente);
		this.tControlo.setToolTipText("Usuario que realizó el control");
		this.tControlo.setEditable(false);
		getContentPane().add(this.tControlo);

		// presenta el botón exportar
		this.btnExportar = new JButton("Exportar");
		this.btnExportar.setBounds(301, 115, 115, 26);
		this.btnExportar.setFont(Fuente);
		this.btnExportar.setToolTipText("Genera una hoja de cálculo");
		getContentPane().add(this.btnExportar);
		this.btnExportar.setIcon(new ImageIcon(FormTemperaturas.class.getResource("/Graficos/mestadistica.png")));
        this.btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });

		// el botón grabar
		this.btnGrabar = new JButton("Grabar");
		this.btnGrabar.setBounds(436, 115, 115, 26);
		this.btnGrabar.setFont(Fuente);
		this.btnGrabar.setToolTipText("Pulse para grabar el registro");
		getContentPane().add(this.btnGrabar);
		this.btnGrabar.setIcon(new ImageIcon(FormTemperaturas.class.getResource("/Graficos/mgrabar.png")));
        this.btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

		// el botón cancelar
		this.btnCancelar = new JButton("Cancelar");
		this.btnCancelar.setBounds(562, 115, 115, 26);
		this.btnCancelar.setFont(Fuente);
		btnCancelar.setToolTipText("Pulse para reiniciar el formulario");
		getContentPane().add(this.btnCancelar);
		btnCancelar.setIcon(new ImageIcon(FormTemperaturas.class.getResource("/Graficos/mBorrar.png")));
        this.btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

		// la tabla de lecturas
		this.tLecturas = new JTable();
		this.tLecturas.setModel(new DefaultTableModel(
			new Object[][] {},
			new String[] {"ID",
					      "Temp.1",
					      "Temp.2",
					      "Tol.",
					      "Lec.1",
					      "Lec.2",
					      "Fecha",
						  "Contr.",
						  "Ver",
						  "El."}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				Integer.class,
				Integer.class,
				Integer.class,
				Integer.class,
				Integer.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// agregamos el tooltip
		this.tLecturas.setToolTipText("Pulse sobre el ícono");

		// fijamos la fuente
		this.tLecturas.setFont(Fuente);

        // fijamos el ancho de las columnas
        this.tLecturas.getColumn("ID").setPreferredWidth(35);
        this.tLecturas.getColumn("ID").setMaxWidth(35);
        this.tLecturas.getColumn("Temp.1").setMaxWidth(70);
        this.tLecturas.getColumn("Temp.1").setPreferredWidth(70);
        this.tLecturas.getColumn("Temp.2").setMaxWidth(70);
		this.tLecturas.getColumn("Temp.2").setPreferredWidth(70);
        this.tLecturas.getColumn("Lec.1").setMaxWidth(70);
		this.tLecturas.getColumn("Lec.1").setPreferredWidth(70);
        this.tLecturas.getColumn("Lec.2").setMaxWidth(70);
		this.tLecturas.getColumn("Lec.2").setPreferredWidth(70);
		this.tLecturas.getColumn("Tol.").setMaxWidth(70);
        this.tLecturas.getColumn("Tol.").setPreferredWidth(70);
        this.tLecturas.getColumn("Fecha").setPreferredWidth(80);
        this.tLecturas.getColumn("Fecha").setMaxWidth(80);
		this.tLecturas.getColumn("Ver").setPreferredWidth(35);
        this.tLecturas.getColumn("Ver").setMaxWidth(35);
		this.tLecturas.getColumn("El.").setPreferredWidth(35);
        this.tLecturas.getColumn("El.").setMaxWidth(35);

		// agregamos el evento
		this.tLecturas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tLecturasMouseClicked(evt);
            }
	    });

		// define el scroll
		JScrollPane scrollTemperaturas = new JScrollPane();
		scrollTemperaturas.setBounds(12, 150, 670, 281);

		// agregamos la tabla al contenddor
		scrollTemperaturas.setViewportView(this.tLecturas);
		getContentPane().add(scrollTemperaturas);

		// mostramos el formulario
		this.setVisible(true);
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la nómina de heladeras en el combo
	 */
	protected void cargaHeladeras(){

		// obtenemos la nómina
		ResultSet nomina = this.Freezer.nominaHeladeras();

        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cNomina.addItem(new ComboClave(0,""));

			// verificamos si está vacío
			if (!nomina.next()){

				// presenta el mensaje y cierra el formulario
				JOptionPane.showMessageDialog(this, "Debe cargar heladeras / freezers primero", "Error", JOptionPane.ERROR_MESSAGE);
				return;

			}

            // nos desplazamos al inicio del resultset
            nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // agregamos el registro
				this.cNomina.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("marca")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón exportar
	 */
	protected void btnExportarActionPerformed(java.awt.event.ActionEvent evt){

		// instanciamos el diálogo
		FormReporte Reporte = new FormReporte(this, true);
		Reporte.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void btnGrabarActionPerformed(java.awt.event.ActionEvent evt){

		// verifica que se halla seleccionado una heladera
		ComboClave item = (ComboClave) this.cNomina.getSelectedItem();
		if (item == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe seleccionar la heladera / freezer", "Error", JOptionPane.ERROR_MESSAGE);
            this.cNomina.requestFocus();
            return;

		}

		// verifica se halla seleccionado una fecha
		if (this.Herramientas.fechaJDate(this.dFechaLectura) == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe indicar una fecha", "Error", JOptionPane.ERROR_MESSAGE);
            this.dFechaLectura.requestFocus();
            return;
			
		}

		// verifica se halla indicado una lectura
		if (this.sTemperatura1.getValue().equals(0)){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique la lectura obtenida", "Error", JOptionPane.ERROR_MESSAGE);
            this.sTemperatura1.requestFocus();
            return;

		}

		// la segunda lectura la permite en blanco
		if (this.Herramientas.fechaJDate(this.dFechaLectura) == null){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe indicar la fecha de lectura", "Error", JOptionPane.ERROR_MESSAGE);
            this.sTemperatura1.requestFocus();
            return;

		}

		// grabamos el registro
		this.grabaLectura();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de verificar el registro que actualiza
	 * la base
	 */
	protected void grabaLectura(){

		// asignamos los valores en el objeto
		ComboClave item = (ComboClave) this.cNomina.getSelectedItem();
		this.Lecturas.setHeladera(item.getClave());

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Lecturas.setIdTemperatura(0);
		} else {
			this.Lecturas.setIdTemperatura(Integer.parseInt(this.tId.getText()));
		}

		// termina de asignar
		this.Lecturas.setFecha(this.Herramientas.fechaJDate(this.dFechaLectura));
		this.Lecturas.setTemperatura1(Integer.parseInt(this.sTemperatura1.getValue().toString()));
		this.Lecturas.setTemperatura2(Integer.parseInt(this.sTemperatura2.getValue().toString()));

		// grabamos el registro
		this.Lecturas.grabaLectura();

		// recargamos la grilla
		this.grillaTemperaturas();

		// limpiamos el formulario
		this.limpiaFormulario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar
	 */
	protected void btnCancelarActionPerformed(java.awt.event.ActionEvent evt){

		// limpiamos el formulario
		this.limpiaFormulario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de temperaturas
	 */
	protected void tLecturasMouseClicked(java.awt.event.MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tLecturas.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tLecturas.rowAtPoint(evt.getPoint());
        int columna = tLecturas.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tLecturas.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id del usuario
            int idlectura = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 8){
            	this.getDatosLectura(idlectura);
            } else if (columna == 9){
            	this.eliminaLectura(idlectura);
            }

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idlectura - clave de la lectura
	 * Método que recibe como parámetro la clave de una lectura
	 * y luego de pedir confirmación, elimina el registro
	 */
	protected void eliminaLectura(int idlectura){

        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar la Lectura?",
                                   "Tablas Auxiliares",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.Lecturas.borraLectura(idlectura);

            // recargamos la grilla
            this.grillaTemperaturas();

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idlectura - entero con la clave del registro
	 * Método que a partir de la clave del registro obtiene los
	 * datos del registro y los presenta en el formulario
	 */
	protected void getDatosLectura(int idlectura){

		// obtenemos el registro
		this.Lecturas.getDatosLectura(idlectura);

		// fijamos los valores en el formulario
		this.tId.setText(Integer.toString(this.Lecturas.getIdTemperatura()));
		this.sTemperatura1.setValue(this.Lecturas.getTemperatura1());
		this.sTemperatura2.setValue(this.Lecturas.getTemperatura2());
		this.tControlo.setText(this.Lecturas.getUsuario());
		this.dFechaLectura.setDate(this.Herramientas.StringToDate(this.Lecturas.getFecha()));

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que reinicia el formulario, llamado al pulsar el botón cancelar
	 * o al grabar un registro
	 */
	protected void limpiaFormulario(){

		// reiniciamos el formulario
		this.cNomina.setSelectedItem("");
		this.tId.setText("");
		this.sTemperatura1.setValue(0);
		this.sTemperatura2.setValue(0);
		this.tControlo.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado en el evento onchange del combo de heladeras
	 * carga en la grilla la nómina de temperaturas
	 */
	protected void cNominaItemStateChanged(java.awt.event.ItemEvent evt){

		// recargamos la grilla
		this.grillaTemperaturas();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que recarga la grilla de temperaturas al cambiar el combo
	 * o al grabar un registro
	 */
	protected void grillaTemperaturas(){

		// obtenemos la clave de la heladera estamos usando el
		// combo con clave así que tenemos que hacer un cast
		// para obtener el objeto seleccionado
		ComboClave item = (ComboClave) this.cNomina.getSelectedItem();

		// si hay un elemento seleccionado
		if (item != null) {

			// obtenemos la clave
			int clave = item.getClave();

			// obtenemos los datos del freezer seleccionado
			this.Freezer.getDatosHeladera(clave);

			// presentamos el patrimonio y la ubicación
			this.tPatrimonio.setText(this.Freezer.getPatrimonio());

			// sobrecargamos el renderer de la tabla
			this.tLecturas.setDefaultRenderer(Object.class, new RendererTabla());

			// obtenemos el modelo de la tabla
			DefaultTableModel modeloTabla = (DefaultTableModel) this.tLecturas.getModel();

			// hacemos la tabla se pueda ordenar
			this.tLecturas.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

			// limpiamos la tabla
			modeloTabla.setRowCount(0);

			// definimos el objeto de las filas
			Object [] fila = new Object[10];

			// ahora obtenemos la nomina
			ResultSet nomina = this.Lecturas.nominaTemperaturas(clave);

			try{

				// nos desplazamos al primer registro
				nomina.beforeFirst();

				// recorremos el vector
				while(nomina.next()){

					// agregamos los elementos al array
					fila[0] = nomina.getInt("idregistro");
					fila[1] = nomina.getInt("temperatura1");
					fila[2] = nomina.getInt("temperatura2");
					fila[3] = nomina.getInt("tolerancia");
					fila[4] = nomina.getInt("lectura1");
					fila[5] = nomina.getInt("lectura2");
					fila[6] = nomina.getString("fecha");
					fila[7] = nomina.getString("controlado");
					fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
					fila[9] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

					// lo agregamos
					modeloTabla.addRow(fila);

				}

			// si hubo un error
			} catch (SQLException ex){

				// presenta el mensaje
				System.out.println(ex.getMessage());

			}

		}

	}

}
