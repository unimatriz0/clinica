/*

    Nombre: XLSTemperaturas
    Fecha: 10/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que genera el reporte de temperaturas

 */

// definición del paquete
package temperaturas;

// importamos las librerías
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import java.sql.ResultSet;
import java.sql.SQLException;
import funciones.Utilidades;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class XLSTemperaturas {

    // declaración de variables
    protected ResultSet Nomina;
    protected Sheet Hoja;
    protected int Contador; 
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicial - cadena con la fecha inicial a reportar
     * @param fechafinal - cadena con la fecha final a reportar
     * Constructor de la clase, recibe como parámetros la fecha inicial 
     * y final a reportar
     */
    public XLSTemperaturas(String fechainicial, String fechafinal){

    	// instanciamos la clase y obtenemos la nómina
    	Temperaturas Lecturas = new Temperaturas();
    	this.Nomina = Lecturas.nominaTemperaturas(fechainicial, fechafinal);
    	
    	// llamamos al método de impresión
    	this.imprimirReporte();
    	
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el constructor que pide el nombre 
     * del archivo a grabar y luego lo genera
     */
    protected void imprimirReporte(){

    	// instanciamos el objeto
    	Utilidades Herramientas = new Utilidades();
        String rutaArchivo = Herramientas.grabaArchivo();
        int IdHeladera = 0;
    	
    	// inicializamos el contador de filas
    	this.Contador = 5;
    	
    	// si canceló
    	if (rutaArchivo == null){
    		return;
    	}
    	
        // abrimos el archivo 
        File archivoXLS = new File(rutaArchivo);
        FileOutputStream archivo = null;

        // si existe el archivo lo elimina
        if(archivoXLS.exists()) archivoXLS.delete();

        // creamos el archivo
        try {
            archivoXLS.createNewFile();
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        // creamos el libro
        Workbook libro = new HSSFWorkbook();

        // abrimos la salida 
        try {
            archivo = new FileOutputStream(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

        // creamos la hoja
        this.Hoja = libro.createSheet("Temperaturas");

        // fijamos el ancho de las columnas
        this.Hoja.setColumnWidth(0, 3500);
        this.Hoja.setColumnWidth(1, 3500);
        this.Hoja.setColumnWidth(2, 2700);
        this.Hoja.setColumnWidth(3, 2700);
        this.Hoja.setColumnWidth(4, 2700);
        this.Hoja.setColumnWidth(5, 2700);
        this.Hoja.setColumnWidth(6, 2000);
        this.Hoja.setColumnWidth(7, 2500);
               
        // imprimimos los encabezados de columna
        this.imprimeEncabezado();
        
        // recorremos el vector
        try {
        	
			while (Nomina.next()){
        
                // si cambió la heladera
                if (Nomina.getInt("id_heladera") != IdHeladera){

                    // imprimimos la heladera y los encabezados
                    this.imprimeHeladera();

                    // asignamos el nuevo valor
                    IdHeladera = Nomina.getInt("id_heladera");

                }

				// agregamos la fila
				Row fila = this.Hoja.createRow(this.Contador);
				
				// presenta los datos 
				Cell celda = fila.createCell(0);
				celda.setCellValue(Nomina.getInt("temperatura1"));
				celda = fila.createCell(1);
				celda.setCellValue(Nomina.getInt("temperatura2"));
				celda = fila.createCell(2);
				celda.setCellValue(Nomina.getInt("tolerancia"));
				celda = fila.createCell(3);
				celda.setCellValue(Nomina.getInt("lectura1"));
				celda = fila.createCell(4);
				celda.setCellValue(Nomina.getInt("lectura2"));
				celda = fila.createCell(5);
				celda.setCellValue(Nomina.getString("fecha"));
				celda = fila.createCell(6);
				celda.setCellValue(Nomina.getString("hora_lectura"));
				celda = fila.createCell(7);
				celda.setCellValue(Nomina.getString("controlado"));
				
				// incrementamos el contador
				this.Contador++;
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
        
        // cerramos el archivo y lo mostramos 
        // con la aplicación predeterminada
        try {
            libro.write(archivo);
            archivo.close();
            Desktop.getDesktop().open(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime el encabezado de la hoja de cálculo
     */
    protected void imprimeEncabezado(){
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime los datos de la heladera / freezer y 
     * luego presenta los títulos de columnas
     */
    protected void imprimeHeladera(){

        // agregamos la fila
        Row fila = this.Hoja.createRow(this.Contador);

        // presentamos la marca de la heladera
        Cell celda = fila.createCell(0);
        try {
        	
        	// agregamos la marca
			celda.setCellValue("Marca: " + this.Nomina.getString("marca"));
			
	        // incrementamos el contador
	        this.Contador++;

	        // agregamos la fila
	        fila = this.Hoja.createRow(this.Contador);
	        
	        // presentamos el registro de patrimonio
	        celda = fila.createCell(0);
	        celda.setCellValue("Patrimonio: " + this.Nomina.getString("patrimonio"));
	        
	        // presentamos la ubicación
	        celda = fila.createCell(4);
	        celda.setCellValue("Ubicación: " + this.Nomina.getString("ubicacion"));
	        
	        // incrementamos el contador
	        this.Contador++;
	        
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
                
        // incrementamos el contador
        this.Contador++;

        // agregamos la fila
        fila = this.Hoja.createRow(this.Contador);
        
        // presenta los encabezados
        celda = fila.createCell(0);
        celda.setCellValue("Temperatura 1");
        celda = fila.createCell(1);
        celda.setCellValue("Temperatura 2");
        celda = fila.createCell(2);
        celda.setCellValue("Tolerancia");
        celda = fila.createCell(3);
        celda.setCellValue("1° Lectura");
        celda = fila.createCell(4);
        celda.setCellValue("2° Lectura");
        celda = fila.createCell(5);
        celda.setCellValue("Fecha");
        celda = fila.createCell(6);
        celda.setCellValue("Hora");
        celda = fila.createCell(7);
        celda.setCellValue("Controló");

        // incrementamos el contador
        this.Contador++;
        
    }
    
}
