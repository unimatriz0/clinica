/*

    Nombre: provincias
    Fecha: 19/09/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que ofrece los métodos para el control de la tabla
                 de provincias

 */

// definición del paquete
package provincias;

// inclusión de librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Claudio Invernizzi
 * Definición de la clase
 *
 */
public class Provincias {

    // definición de variables
    protected Conexion Enlace;

    // definición de las variables
    protected int IdProvincia;              // clave del registro
    protected int IdPais;                   // clave del país
    protected String Pais;                  // nombre del país
    protected String Nombre;                // nombre de la provincia
    protected String CodProv;               // código indec de la provincia
    protected int Poblacion;                // población de la provincia
    protected String Usuario;               // nombre del usuario
    protected String FechaAlta;             // fecha de alta del registro

    // constructor de la clase
    public Provincias(){

        // instanciamos la conexión
        this.Enlace = new Conexion();

        // inicializamos las variables
        this.initProvincias();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa las variables de clase
     */
    protected void initProvincias(){

        // inicializamos las variables
    	this.IdProvincia = 0;
        this.IdPais = 0;
        this.Pais = "";
        this.Nombre = "";
        this.CodProv = "";
        this.Poblacion = 0;
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setIdProvincia(int idprovincia){
    	this.IdProvincia = idprovincia;
    }
    public void setIdPais(int idpais){
    	this.IdPais = idpais;
    }
    public void setNombre(String nombre){
    	this.Nombre = nombre;
    }
    public void setCodProv(String codprov){
    	this.CodProv = codprov;
    }
    public void setPoblacion(int poblacion){
    	this.Poblacion = poblacion;
    }

    // métodos de retorno de valores
    public int getIdProvincia(){
    	return this.IdProvincia;
    }
    public int getIdPais(){
        return this.IdPais;
    }
    public String getPais(){
        return this.Pais;
    }
    public String getNombre(){
        return this.Nombre;
    }
    public String getCodProv(){
        return this.CodProv;
    }
    public int getPoblacion(){
        return this.Poblacion;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param pais - clave del país
     * @return array con las provincias de ese país
     * Método que recibe como parámetro el nombre de un país y retorna
     * un array con la nómina de jurisdicciones
     */
    public ResultSet listaProvincias(int pais){

        // declaración de variables
        ResultSet nominaProvincias;
        String Consulta;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_provincias.id AS id, " +
                   "diccionarios.v_provincias.provincia AS nombre_provincia, " +
                   "       diccionarios.v_provincias.cod_prov AS cod_prov, " +
                   "       diccionarios.v_provincias.poblacion AS poblacion " +
                   "FROM diccionarios.v_provincias " +
                   "WHERE diccionarios.v_provincias.idpais = '" + pais + "' " +
                   "ORDER BY diccionarios.v_provincias.provincia; ";

        // obtenemos el vector
        nominaProvincias = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return nominaProvincias;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param pais - string con el nombre de un país
     * @param provincia - string con el nombre de una provincia
     * @return codprov - string con la clave indec de la provincia
     * Método que recibe como parámetros la el nombre de un país y
     * de una provincia y retorna la clave indec de esa provincia
     */
    public String getCodProv(String pais, String provincia){

    	// declaración de variables
        String Consulta;
        String codprov = "";
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_provincias.cod_prov AS cod_prov " +
                   "FROM diccionarios.v_provincias " +
                   "WHERE diccionarios.v_provincias.pais = '" + pais + "' AND " +
                   "      diccionarios.v_provincias.provincia = '" + provincia + "' " +
                   "ORDER BY diccionarios.v_provincias.provincia; ";

        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            codprov = Resultado.getString("cod_prov");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el código
        return codprov;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción o edición segun corresponda
     */
    public void grabaProvincia(){

    	// si está insertando
    	if (this.IdProvincia == 0){
    		this.nuevaProvincia();
    	} else {
    		this.editaProvincia();
    	}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción en la base
     */
    protected void nuevaProvincia(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "INSERT INTO diccionarios.provincias "
    			+ "        (pais, "
    			+ "         nom_prov, "
    			+ "         cod_prov, "
    			+ "         poblacion, "
    			+ "         usuario) "
    			+ "        VALUES "
    			+ "        (?,?,?,?,?)";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.IdPais);
	        preparedStmt.setString (2, this.Nombre);
	        preparedStmt.setString (3, this.CodProv);
	        preparedStmt.setInt    (4, this.Poblacion);
	        preparedStmt.setInt    (5, Seguridad.Id);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @uthor Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición en la base
     */
    protected void editaProvincia(){

    	// declaración de variables
    	String Consulta;
    	PreparedStatement preparedStmt;
    	Connection Puntero = this.Enlace.getConexion();

    	// componemos la consulta
    	Consulta = "UPDATE diccionarios.provincias SET "
    			+ "        pais = ?, "
    			+ "        nom_prov = ?, "
    			+ "        poblacion = ?, "
    			+ "        usuario = ? "
    			+ " WHERE diccionarios.provincias.cod_prov = ?; ";

    	try {

    		// asignamos el puntero a la consulta
			preparedStmt = Puntero.prepareStatement(Consulta);

	    	// asignamos los valores
	    	preparedStmt.setInt    (1, this.IdPais);
	        preparedStmt.setString (2, this.Nombre);
	        preparedStmt.setInt    (3, this.Poblacion);
	        preparedStmt.setInt    (4, Seguridad.Id);
	        preparedStmt.setString (5, this.CodProv);

	        // ejecutamos la consulta
	        preparedStmt.execute();

	    // si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param pais - nombre del país
     * @param provincia - nombre de la provincia
     * @return Correcto - si puede insertar
     * Método que recibe como parámetros el nombre de un país y
     * de una provincia y verifica que no se encuentre repetido
     * si puede insertar retorna correcto
     */
    public boolean validaProvincia(int pais, String provincia){

    	// declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT COUNT(diccionarios.v_provincias.cod_prov) AS registros " +
                   "FROM diccionarios.v_provincias " +
                   "WHERE diccionarios.v_provincias.idpais = '" + pais + "' AND " +
                   "      diccionarios.v_provincias.provincia = '" + provincia + "'; ";

        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            if (Resultado.getInt("registros") == 0){
            	Correcto = true;
            } else {
            	Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el código
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave indec de la provincia
     * Mètodo que recibe como paràmetro la clave indec y asigna
     * en las variables de clase los valores del registro
     */
    public void getDatosProvincia(String clave){

    	// declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_provincias.id AS id, " +
                   "       diccionarios.v_provincias.idpais AS idpais, " +
                   "       diccionarios.v_provincias.pais AS pais, " +
                   "       diccionarios.v_provincias.provincia AS provincia, " +
                   "       diccionarios.v_provincias.cod_prov AS cod_prov, " +
                   "       diccionarios.v_provincias.poblacion AS poblacion, " +
                   "       diccionarios.v_provincias.fecha_alta AS fecha_alta, " +
                   "       diccionarios.v_provincias.usuario AS usuario " +
                   "FROM diccionarios.v_provincias " +
                   "WHERE diccionarios.v_provincias.cod_prov = '" + clave + "';";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro y lo asignamos
            Resultado.next();
            this.IdProvincia = Resultado.getInt("id");
            this.IdPais = Resultado.getInt("idpais");
            this.Pais = Resultado.getString("pais");
            this.Nombre = Resultado.getString("provincia");
            this.CodProv = Resultado.getString("cod_prov");
            this.Poblacion = Resultado.getInt("poblacion");
            this.Usuario = Resultado.getString("usuario");
            this.FechaAlta = Resultado.getString("fecha_alta");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave indec de la provincia
     * @return boolean - resultado de la operaciòn
     * Método que recibe como parámetro la clave indec de la
     * provincia y verifica que no tenga hijos
     */
    public boolean puedeBorrar(String clave){

    	// declaración de variables
        String Consulta;
        boolean Correcto = false;
        ResultSet Resultado;

        // componemos la consulta verificando si tiene alguna localidad asignada
        Consulta = "SELECT COUNT(diccionarios.localidades.codpcia) AS registros " +
                   "FROM diccionarios.localidades " +
                   "WHERE diccionarios.localidades.codpcia = '" + clave + "'; ";

        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // obtenemos el registro
            Resultado.next();
            if (Resultado.getInt("registros") == 0){
            	Correcto = true;
            } else {
            	Correcto = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

        // retornamos el código
        return Correcto;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave indec de la jurisdicciòn
     * Método que recibe como parámetro la clave indec y
     * simplemente ejecuta la consulta de eliminación
     */
    public void borraProvincia(String clave){

    	// declaración de variables
    	String Consulta;

    	// componemos y ejecutamos la consulta
    	Consulta = "DELETE FROM diccionarios.provincias " +
    	           "WHERE diccionarios.provincias.cod_prov = '" + clave + "';";
    	this.Enlace.Ejecutar(Consulta);

    }

}
