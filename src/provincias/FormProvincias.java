/*

    Nombre: FormProvincias
    Fecha: 06/11/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que arma el formulario para el abm
                 de provincias

 */

// definición del paquete
package provincias;

// importamos las librerías
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseEvent;
import java.awt.Frame;
import javax.swing.JOptionPane;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.TableRowSorter;
import funciones.RendererTabla;
import paises.Paises;
import funciones.ComboClave;
import java.awt.Font;

// definición de la clase
public class FormProvincias extends JDialog {

	// implementa el serial id
	private static final long serialVersionUID = 1230732612483881070L;

	// define las variables de clase
	private JTextField tId;
	private JTextField tCodigo;
	private JTextField tProvincia;
	private JTable tNomina;
	private JComboBox<Object> cPaises;
	private JFormattedTextField tPoblacion;
	private Provincias Jurisdicciones;

	// constructor de la clase
	public FormProvincias(Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// definimos las propiedades
		setBounds(100, 100, 500, 377);
		getContentPane().setLayout(null);
		this.setTitle("Jurisdicciones Registradas");

		// instanciamos la clase
		Jurisdicciones = new Provincias();

		// inicializamos el formulario
		this.initFormProvincias();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa los componentes del formulario
	 */
	@SuppressWarnings({ "serial" })
	private void initFormProvincias(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título y el combo de país
		JLabel lPais = new JLabel("Seleccione el país de la lista");
		lPais.setBounds(10, 10, 250, 26);
		lPais.setFont(Fuente);
		getContentPane().add(lPais);
		this.cPaises = new JComboBox<>();
		this.cPaises.setBounds(218, 10, 212, 26);
		this.cPaises.setFont(Fuente);
		getContentPane().add(this.cPaises);

		// agregamos los elementos al combo
		this.cargaPaises();

		// fijamos el evento change del combo
        this.cPaises.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cPaisesItemStateChanged(evt);
            }
        });

		// presenta la clave del registro
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 44, 26);
		this.tId.setFont(Fuente);
		getContentPane().add(this.tId);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);

		// presenta la clave indec
		this.tCodigo = new JTextField();
		this.tCodigo.setBounds(63, 45, 44, 26);
		this.tCodigo.setFont(Fuente);
		this.tCodigo.setToolTipText("Clave de georreferenciación");
		getContentPane().add(this.tCodigo);

		// presenta el nombre de la jurisdicción
		this.tProvincia = new JTextField();
		this.tProvincia.setBounds(119, 45, 220, 26);
		this.tProvincia.setFont(Fuente);
		this.tProvincia.setToolTipText("Nombre de la jurisdicción");
		getContentPane().add(this.tProvincia);

		// pide la población
		this.tPoblacion = new JFormattedTextField();
		this.tPoblacion.setBounds(353, 45, 81, 26);
		this.tPoblacion.setFont(Fuente);
	    this.tPoblacion.setToolTipText("Población de la jurisdicción");
		getContentPane().add(this.tPoblacion);

		// presenta el botón grabar
		JButton btnGrabar = new JButton();
		btnGrabar.setBounds(446, 45, 26, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setFont(Fuente);
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		getContentPane().add(btnGrabar);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

		// define la tabla
		this.tNomina = new JTable();
		this.tNomina.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null},
			},
			new String[] {
				"ID", 
				"COD.", 
				"Nombre", 
				"Pob.", 
				"Ed.", 
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, 
				String.class, 
				String.class, 
				Integer.class, 
				Object.class, 
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// establecemos el ancho de las columnas
		this.tNomina.getColumn("ID").setPreferredWidth(30);
		this.tNomina.getColumn("ID").setMaxWidth(30);
		this.tNomina.getColumn("COD.").setPreferredWidth(40);
		this.tNomina.getColumn("COD.").setMaxWidth(40);
		this.tNomina.getColumn("Pob.").setPreferredWidth(90);
		this.tNomina.getColumn("Pob.").setMaxWidth(90);
		this.tNomina.getColumn("Ed.").setPreferredWidth(35);
		this.tNomina.getColumn("Ed.").setMaxWidth(35);
		this.tNomina.getColumn("El.").setPreferredWidth(35);
		this.tNomina.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tNomina.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tNomina.setRowHeight(25);

		// establecemos la fuente
		this.tNomina.setFont(Fuente);
		
		// agregamos el evento
		this.tNomina.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tNominaMouseClicked(evt);
            }
	    });

		// agrega el vieport de la tabla
		JScrollPane scrollProvincias = new JScrollPane();
		scrollProvincias.setBounds(10, 80, 470, 256);
		scrollProvincias.setViewportView(this.tNomina);
		getContentPane().add(scrollProvincias);

		// mostramos el formulario
		this.setVisible(true);
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado desde el constructor que carga la nómina
	 * de países
	 */
	private void cargaPaises(){

		// instanciamos la clase
		Paises Naciones = new Paises();

		// obtenemos la nómina de países
		ResultSet nomina = Naciones.nominaPaises();

        try {

			// agregamos el primer elemento usamos la clase comboclave
			// para almacenar tanto la id como el texto
			this.cPaises.addItem(new ComboClave(0,""));

            // nos desplazamos al inicio del resultset
            nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // agregamos el registro
				this.cPaises.addItem(new ComboClave(nomina.getInt("id"), nomina.getString("pais")));

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado en el evento onchange del combo de heladeras
	 * carga en la grilla la nómina de jurisdicciones
	 */
	protected void cPaisesItemStateChanged(java.awt.event.ItemEvent evt){

		// recargamos la grilla
		this.grillaJurisdicciones();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la nómina de jurisdicciones de acuerdo
	 * al valor seleccionado en el combo
	 */
	private void grillaJurisdicciones(){

		// obtenemos la clave del pais estamos usando el
		// combo con clave así que tenemos que hacer un cast
		// para obtener el objeto seleccionado
		ComboClave item = (ComboClave) this.cPaises.getSelectedItem();

		// si hay un elemento seleccionado
		if (item != null) {

			// obtenemos la clave
			int clave = item.getClave();

			// sobrecargamos el renderer de la tabla
			this.tNomina.setDefaultRenderer(Object.class, new RendererTabla());

			// obtenemos el modelo de la tabla
			DefaultTableModel modeloTabla = (DefaultTableModel) this.tNomina.getModel();

			// hacemos la tabla se pueda ordenar
			this.tNomina.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

			// limpiamos la tabla
			modeloTabla.setRowCount(0);

			// definimos el objeto de las filas
			Object [] fila = new Object[6];

			// ahora obtenemos la nomina
			ResultSet nomina = this.Jurisdicciones.listaProvincias(clave);

			try{

				// nos desplazamos al primer registro
				nomina.beforeFirst();

				// recorremos el vector
				while(nomina.next()){

					// agregamos los elementos al array
					fila[0] = nomina.getInt("id");
					fila[1] = nomina.getString("cod_prov");
					fila[2] = nomina.getString("nombre_provincia");
					fila[3] = nomina.getInt("poblacion");
					fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
					fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

					// lo agregamos
					modeloTabla.addRow(fila);

				}

			// si hubo un error
			} catch (SQLException ex){

				// presenta el mensaje
				System.out.println(ex.getMessage());

			}

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de jurisdicciones
	 */
	protected void tNominaMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tNomina.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tNomina.rowAtPoint(evt.getPoint());
        int columna = tNomina.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tNomina.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id indec del registro
            String id = (String) modeloTabla.getValueAt(indice, 1);

            // según la columna pulsada
            if (columna == 4){
            	this.getDatosProvincia(id);
            } else if (columna == 5){
            	this.borraProvincia(id);
            }

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - string con la clave indec de la jurissicción
	 * Mètodo que recibe la clave indec y obtiene los datos del
	 * registro para presentarlos en el formulario
	 */
	protected void getDatosProvincia(String clave){

		// obtenemos los datos
		this.Jurisdicciones.getDatosProvincia(clave);

		// asignamos en los campos
		this.tId.setText(Integer.toString(this.Jurisdicciones.getIdProvincia()));
		this.tCodigo.setText(this.Jurisdicciones.getCodProv());
		this.tProvincia.setText(this.Jurisdicciones.getNombre());
		this.tPoblacion.setText(Integer.toString(this.Jurisdicciones.getPoblacion()));

		// fijamos el foco
		this.tCodigo.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param clave - la clave indec de la provinica
	 * Mètodo llamado al eliminar, que primero verifica
	 * que la jurisdicciòn no tenga hijos
	 */
	protected void borraProvincia(String clave){

		// verifica si puede eliminar
		if (!this.Jurisdicciones.puedeBorrar(clave)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this, "Esa jurisdicción tiene localidades asignadas", "Error", JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this, "Está seguro que desea eliminar el registro?", "Jurisdicciones", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Jurisdicciones.borraProvincia(clave);

			// recargamos la grilla
			this.grillaJurisdicciones();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario de datos
	 */
	protected void limpiaFormulario(){

		// reiniciamos los campos
		this.tId.setText("");
		this.tCodigo.setText("");
		this.tProvincia.setText("");
		this.tPoblacion.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar
	 */
	protected void btnGrabarActionPerformed(java.awt.event.ActionEvent evt){

		// fijamos la clave del país en la clase
		ComboClave item = (ComboClave) this.cPaises.getSelectedItem();
		if (item == null){

            // retornamos
            return;

		// si hay un elemento seleccionado
		} else {

			// asignamos en la clase
			this.Jurisdicciones.setIdPais(item.getClave());

		}

		// verificamos se halla ingresado el código indec
		if (this.tCodigo.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Debe indicar el código de georreferenciación", "Error", JOptionPane.ERROR_MESSAGE);
            this.tCodigo.requestFocus();
            return;

		// verificamos la longitud
		} else if (this.tCodigo.getText().length() > 4){

            // presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this, "El código de georreferenciación debe\n" +
												"tener un máximo de cuatro(4) dígitos",
												"Error", JOptionPane.ERROR_MESSAGE);
            this.tCodigo.requestFocus();
            return;

		// si ingresó
		} else {

			// asigna en la clase
			this.Jurisdicciones.setCodProv(this.tCodigo.getText());

		}

		// verificamos se halla ingresado el nombre
		if (this.tProvincia.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "Indique el nombre de la jurisdicción", "Error", JOptionPane.ERROR_MESSAGE);
            this.tProvincia.requestFocus();
            return;

		// si ingresó
		} else {

			// asigna en la clase
			this.Jurisdicciones.setNombre(this.tProvincia.getText());

		}

		// si no ingresó la población avisa
		if (this.tPoblacion.getText().isEmpty()){

            // presenta el mensaje y retorna
            JOptionPane.showMessageDialog(this, "No hay datos de población, se graba igualmente", "Error", JOptionPane.ERROR_MESSAGE);

			// asigna en la variable de clase
			this.Jurisdicciones.setPoblacion(0);

		// si ingresó
		} else {

			// asigna en la variable de clase
			this.Jurisdicciones.setPoblacion(Integer.parseInt(this.tPoblacion.getText()));

		}

		// si está dando un alta
		if (this.tId.getText().isEmpty()){

			// verifica que no está repetido
			if (!this.Jurisdicciones.validaProvincia(item.getClave(), this.tProvincia.getText())){

				// presenta el mensaje
				JOptionPane.showMessageDialog(this, "Esa jurisdicción ya existe", "Error", JOptionPane.ERROR_MESSAGE);
				this.tProvincia.requestFocus();
				return;

			// si puede agregar
			} else {

				// asigna en la clase
				this.Jurisdicciones.setIdProvincia(0);

			}

		// si está editando
		} else {

			// asigna en la clase
			this.Jurisdicciones.setIdProvincia(Integer.parseInt(this.tId.getText()));

		}

		// graba el registro
		this.Jurisdicciones.grabaProvincia();

		// limpia el formulario
		this.limpiaFormulario();

		// recarga la grilla
		this.grillaJurisdicciones();

	}

}
