/*

    Nombre: Agudo
    Fecha: 09/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 chagas agudo

*/

 // declaración del paquete
package agudo;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Agudo {

    // declaración de variables
    protected Conexion Enlace;       // puntero a la base de datos
    protected int Id;                // clave del registro
    protected int Paciente;          // clave del paciente
    protected int Sintomas;          // indica si tuvo síntomas
    protected String Observaciones;  // observaciones del usuario
    protected int IdUsuario;         // clave del usuario
    protected String Usuario;        // nombre del usuario
    protected String FechaAlta;      // fecha de alta del registro

    // constructor de la clase
    public Agudo(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.IdUsuario = Seguridad.Id;
        this.initAgudo();

    }

    // método que inicializa las variables de clase
    private void initAgudo(){

        // inicializamos las variables
        this.Id = 0;
        this.Paciente = 0;
        this.Sintomas = 0;
        this.Observaciones = "";
        this.Usuario = "";
        this.FechaAlta = "";

    }

    // métodos de asignación de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setSintomas(int sintomas){
        this.Sintomas = sintomas;
    }
    public void setObservaciones(String observaciones){
        this.Observaciones = observaciones;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getSintomas(){
        return this.Sintomas;
    }
    public String getObservaciones(){
        return this.Observaciones;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFechaAlta(){
        return this.FechaAlta;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param vista clave de la visita
     * Método que recibe como parámetro la clave de un
     * registro y asigna en las variables de clase los
     * valores del mismo
     */
    public void getDatosAgudo(int protocolo){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta y la ejecutamos
        Consulta = "SELECT diagnostico.v_agudo.id AS id, " +
                   "       diagnostico.v_agudo.paciente AS paciente, " +
                   "       diagnostico.v_agudo.sintomas AS sintomas, " +
                   "       diagnostico.v_agudo.observaciones AS observaciones, " +
                   "       diagnostico.v_agudo.usuario AS usuario, " +
                   "       diagnostico.v_agudo.fecha AS fecha " +
                   "FROM diagnostico.v_agudo " +
                   "WHERE diagnostico.v_agudo.paciente = '" + protocolo + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos desplazamos al primer registro
            if (Resultado.next()){

                // asignamos los valores en las variables de clase
                this.Id = Resultado.getInt("id");
                this.Paciente = Resultado.getInt("paciente");
                this.Sintomas = Resultado.getInt("sintomas");
                this.Observaciones = Resultado.getString("observaciones");
                this.Usuario = Resultado.getString("usuario");
                this.FechaAlta = Resultado.getString("fecha");

            // si no encontró registros
            } else {

                // inicializamos las variables
                this.initAgudo();

            }
        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return id clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaAgudo(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoAgudo();
        } else {
            this.editaAgudo();
        }

        // retornamos la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un
     * nuevo registro
     */
    protected void nuevoAgudo(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.agudo " +
                   "       (paciente, " +
                   "        sintomas, " +
                   "        observaciones, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Paciente);
            psInsertar.setInt(2,    this.Sintomas);
            psInsertar.setString(3, this.Observaciones);
            psInsertar.setInt(4,    this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            ex.printStackTrace();

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaAgudo(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.agudo SET " +
                   "       sintomas = ?, " +
                   "       observaciones = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.agudo.id = ?;";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1,    this.Sintomas);
            psInsertar.setString(2, this.Observaciones);
            psInsertar.setInt(3,    this.IdUsuario);
            psInsertar.setInt(4,    this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            ex.printStackTrace();

        }

    }

}