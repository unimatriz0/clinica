/*

    Nombre: Disautonomia
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 disautonomìa, deberìa haber solo una entrada por cada
                 paciente

*/

// declaración del paquete
package disautonomia;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Disautonomia {

    // declaraciòn de variables de clase
    protected Conexion Enlace;            // puntero a la base de datos
    protected int Id;                     // clave del registro
    protected int Paciente;               // clave del paciente
    protected int Hipotension;            // 0 No 1 Si
    protected int Bradicardia;            // 0 no 1 Si
    protected int Astenia;                // 0 No 1 Si
    protected int NoTiene;                // 0 No 1 Si
    protected int IdUsuario;              // clave del usuario
    protected String Usuario;             // nombre del usuario
    protected String Fecha;               // fecha de alta del registro

    // constructor de la clase
    public Disautonomia(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.initDisautonomia();

    }

    // método que inicializa las variables de la clase
    private void initDisautonomia(){

        this.Id = 0;
        this.Paciente = 0;
        this.Hipotension = 0;
        this.Bradicardia = 0;
        this.Astenia = 0;
        this.NoTiene = 0;
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // mètodos de asignaciòn de valores
    public void setId(int id){
        this.Id = id;
    }
    public void setPaciente(int paciente){
        this.Paciente = paciente;
    }
    public void setHipotension(int hipotension){
        this.Hipotension = hipotension;
    }
    public void setBradicardia(int bradicardia){
        this.Bradicardia = bradicardia;
    }
    public void setAstenia(int astenia){
        this.Astenia = astenia;
    }
    public void setNoTiene(int notiene){
        this.NoTiene = notiene;
    }

    // mètodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public int getPaciente(){
        return this.Paciente;
    }
    public int getHipotension(){
        return this.Hipotension;
    }
    public int getBradicardia(){
        return this.Bradicardia;
    }
    public int getAstenia(){
        return this.Astenia;
    }
    public int getNoTiene(){
        return this.NoTiene;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param paciente entero con la clave del registro
     * Mètodo que recibe como paràmetro la clave de un paciente
     * y asigna en las variables de clase los valores del
     * registro
     */
    public void getDatosDisautonomia(int paciente){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_disautonomia.id AS id, " +
                   "       diagnostico.v_disautonomia.paciente AS paciente, " +
                   "       diagnostico.v_disautonomia.hipotension AS hipotension, " +
                   "       diagnostico.v_disautonomia.bradicardia AS bradicardia, " +
                   "       diagnostico.v_disautonomia.astenia AS astenia, " +
                   "       diagnostico.v_disautonomia.notiene AS notiene, " +
                   "       diagnostico.v_disautonomia.usuario AS usuario, " +
                   "       diagnostico.v_disautonomia.fecha AS fecha " +
                   "FROM diagnostico.v_disautonomia " +
                   "WHERE diagnostico.v_disautonomia.paciente = '" + paciente + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            if (Resultado.next()){

                // asignamos los valores del registro
                this.Id = Resultado.getInt("id");
                this.Paciente = Resultado.getInt("paciente");
                this.Hipotension = Resultado.getInt("hipotension");
                this.Bradicardia = Resultado.getInt("bradicardia");
                this.Astenia = Resultado.getInt("astenia");
                this.NoTiene = Resultado.getInt("notiene");
                this.Usuario = Resultado.getString("usuario");
                this.Fecha = Resultado.getString("fecha");

            // si no hay registros
            } else {

                // inicializamos para estar seguros
                this.initDisautonomia();

            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero clave del registro
     * Mètodo que ejecuta la consulta de inserciòn o ediciòn
     * segùn corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaDisautonia() {

    	// si está insertando
    	if (this.Id == 0) {
    		this.nuevaDisautonomia();
    	} else {
    		this.editaDisautonomia();
    	}

    	// retorna la id
    	return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserciòn
     */
    protected void nuevaDisautonomia() {

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // compone la consulta
        Consulta = "INSERT INTO diagnostico.disautonomia " +
                   "            (paciente, " +
       		       "             hipotension, " +
                   "             bradicardia, " +
       		       "             astenia, " +
                   "             notiene, " +
       		       "             usuario) " +
                   "            VALUES " +
       		       "            (?, ?, ?, ?, ?, ?);";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Paciente);
            psInsertar.setInt(2, this.Hipotension);
            psInsertar.setInt(3, this.Bradicardia);
            psInsertar.setInt(4, this.Astenia);
            psInsertar.setInt(5, this.NoTiene);
            psInsertar.setInt(6, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Mètodo que ejecuta la consulta de edición
     */
    protected void editaDisautonomia() {

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.disautonomia SET " +
                   "       hipotension = ?, " +
                   "       bradicardia = ?, " +
                   "       astenia = ?, " +
                   "       notiene = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.disautonomia.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setInt(1, this.Hipotension);
            psInsertar.setInt(2, this.Bradicardia);
            psInsertar.setInt(3, this.Astenia);
            psInsertar.setInt(4, this.NoTiene);
            psInsertar.setInt(5, this.IdUsuario);
            psInsertar.setInt(6, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

}
