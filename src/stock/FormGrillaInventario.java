/*
 * Nombre: formBuscaItemEgreso
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 30/11/2017
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma la grilla con los items coincidentes
 *              en la salida del depósito
 */

// definición del paquete
package stock;

// importamos las librerías
import java.awt.Frame;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.awt.Font;

// Definición de la clase
public class FormGrillaInventario extends javax.swing.JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // creamos el formulario
    public FormGrillaInventario(Frame parent, boolean modal) {

        // fijamos el formulario padre
        super(parent, modal);

        // iniciamos los componentes
        initComponents();

    }

    // iniciamos los componentes
    @SuppressWarnings({ "serial" })
    private void initComponents() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // fijamos las propiedades del formulario
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Inventario");
        setBounds(200,150,720, 520);
        getContentPane().setLayout(null);

        // presentamos el título
        JLabel lTitulo = new JLabel("Existencias en el Depósito");
        lTitulo.setBounds(6,6,450,26);
        lTitulo.setFont(Fuente);
        getContentPane().add(lTitulo);

        // agregamos el botón reporte
        JButton btnReporte = new JButton("Reporte");
        btnReporte.setBounds(610, 10, 90, 26);
        btnReporte.setFont(Fuente);
        btnReporte.setIcon(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
        btnReporte.setToolTipText("Genera un documento con las existencias");
        btnReporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generarReporte();
            }
        });
        getContentPane().add(btnReporte);

        // definimos la tabla
        JTable tInventario = new javax.swing.JTable();

        // fijamos las propiedades de la tabla
        tInventario.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Descripción",
                "Crítico",
                "Existentes",
                "Valor"
            }
        ) {
            @SuppressWarnings("rawtypes")
            Class[] types = new Class [] {
                String.class,
                Integer.class,
                Integer.class,
                Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });

        // impedimos reordenar las columnas
        tInventario.getTableHeader().setReorderingAllowed(false);

        // fijamos el tamaño de las columnas
        tInventario.getColumn("Crítico").setPreferredWidth(85);
        tInventario.getColumn("Crítico").setMaxWidth(85);
        tInventario.getColumn("Existentes").setPreferredWidth(85);
        tInventario.getColumn("Existentes").setMaxWidth(85);
        tInventario.getColumn("Valor").setPreferredWidth(85);
        tInventario.getColumn("Valor").setMaxWidth(85);

        // definimos la fuente
        tInventario.setFont(Fuente);

        // definimos el scroll y agregamos la tabla
        JScrollPane sInventario = new JScrollPane();
        sInventario.setViewportView(tInventario);

        // fijamos el tamaño y lo agregamos
        sInventario.setBounds(10, 43, 690, 430);
        getContentPane().add(sInventario);

        // ahora obtenemos el inventario
        Stock Deposito = new Stock();
        ResultSet inventario = Deposito.getInventario();

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) tInventario.getModel();

        // hacemos la tabla se pueda ordenar
        tInventario.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[4];

        try {

            // nos desplazamos al inicio del resultset
            inventario.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (inventario.next()){

                // fijamos los valores de la fila
                fila[0] = inventario.getString("item");
                fila[1] = inventario.getInt("critico");
                fila[2] = inventario.getInt("existencia");
                fila[3] = inventario.getFloat("importe");

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    // método que genera el reporte con las existencias de stock
    private void generarReporte() {

    	// instanciamos la clase
    	new XLSInventario();

    }

}
