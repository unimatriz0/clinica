/*
 * Nombre: formGrillaItems
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 01/08/2017
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma la grilla de items del inventario
 */

// definición del paquete
package stock;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import java.io.FileInputStream;
import javax.swing.JOptionPane;
import funciones.RendererTabla;
import funciones.Utilidades;
import seguridad.Seguridad;
import funciones.Imagenes;
import funciones.Mensaje;
import java.awt.Font;

// definición de la clase
public class FormGrillaItems extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // definición de las variables
    private JTable tElementos;
    private JScrollPane Scroll;
    private JTextField tId;
    private JTextField tDescripcion;
    private JTextField tUsuario;
    private JTextField tAlta;
    private JTextField tCodigoSop;
    private JSpinner sValor;
    private JSpinner sCritico;
    private JLabel lImagen;
    Stock Elementos;                          // clase de stock
    Utilidades Herramientas;                  // utilidades del sistema
    String Archivo;                           // puntero del archivo de imagen
    FileInputStream Contenido;                // contenido del archivo
    int Longitud;                             // tamaño del archivo

    // creamos el formulario
    public FormGrillaItems (java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

        // instanciamos las clases y las variables
        this.Elementos = new Stock();
        this.Herramientas = new Utilidades();
        this.Archivo = "";

        // fijamos el tamaño y las propiedades
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        this.setTitle("Diccionario de Artículos");
        this.setResizable(false);
        setBounds(200, 150, 709, 547);
        getContentPane().setLayout(null);

        // inicializamos el formualrio
        this.initItems();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario
     */
    @SuppressWarnings({ "serial" })
    protected void initItems(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // presenta la clave del item
        JLabel lId = new JLabel("ID:");
        lId.setBounds(12, 10, 40, 26);
        lId.setFont(Fuente);
        getContentPane().add(lId);
        this.tId = new JTextField();
        this.tId.setToolTipText("Clave del registro");
        this.tId.setEditable(false);
        this.tId.setBounds(34, 10, 40, 26);
        this.tId.setFont(Fuente);
        getContentPane().add(this.tId);

        // presenta la descripción
        JLabel lDescripcion = new JLabel("Descripción:");
        lDescripcion.setBounds(82, 10, 120, 26);
        lDescripcion.setFont(Fuente);
        getContentPane().add(lDescripcion);
        this.tDescripcion = new JTextField();
        this.tDescripcion.setToolTipText("Ingrese la descripción del item");
        this.tDescripcion.setBounds(171, 10, 385, 26);
        this.tDescripcion.setFont(Fuente);
        getContentPane().add(this.tDescripcion);

        // pide la cantidad crítica
        JLabel lCritico = new JLabel("Crítico:");
        lCritico.setBounds(592, 10, 60, 26);
        lCritico.setFont(Fuente);
        getContentPane().add(lCritico);
        this.sCritico = new JSpinner();
        this.sCritico.setToolTipText("Cantidad mínima del depósito");
        this.sCritico.setBounds(645, 10, 50, 26);
        this.sCritico.setFont(Fuente);
        getContentPane().add(sCritico);

        // pide la imagen del item y fijamos la imagen por defecto
        this.lImagen = new JLabel();
        this.lImagen.setToolTipText("Pulse para cargar la imagen del item");
        this.lImagen.setBounds(12, 40, 110, 110);
        this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
        getContentPane().add(this.lImagen);
        this.lImagen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lImagenMouseClicked(evt);
            }
        });

        // presenta el valor
        JLabel lValor = new JLabel("Valor:");
        lValor.setBounds(130, 45, 50, 26);
        lValor.setFont(Fuente);
        getContentPane().add(lValor);
        this.sValor = new JSpinner();
        this.sValor.setToolTipText("Valor unitario del item");
        this.sValor.setBounds(170, 45, 73, 26);
        this.sValor.setFont(Fuente);
        getContentPane().add(this.sValor);

        // el código sop
        JLabel lCodigoSop = new JLabel("Sop:");
        lCodigoSop.setBounds(255, 45, 50, 26);
        lCodigoSop.setFont(Fuente);
        getContentPane().add(lCodigoSop);
        this.tCodigoSop = new JTextField();
        this.tCodigoSop.setToolTipText("Código SOP del Artículo");
        this.tCodigoSop.setBounds(285, 45, 115, 26);
        this.tCodigoSop.setFont(Fuente);
        getContentPane().add(this.tCodigoSop);

        // pide la fecha de alta
        JLabel lAlta = new JLabel("Fecha Alta:");
        lAlta.setBounds(410, 45, 86, 26);
        lAlta.setFont(Fuente);
        getContentPane().add(lAlta);
        this.tAlta = new JTextField();
        this.tAlta.setToolTipText("Fecha de alta del registro");
        this.tAlta.setEditable(false);
        this.tAlta.setBounds(480, 45, 79, 26);
        this.tAlta.setFont(Fuente);
        getContentPane().add(this.tAlta);

        // presenta el usuario
        JLabel lUsuario = new JLabel("Usuario:");
        lUsuario.setBounds(410, 80, 60, 26);
        lUsuario.setFont(Fuente);
        getContentPane().add(lUsuario);
        this.tUsuario = new JTextField();
        this.tUsuario.setToolTipText("Usuario que ingresó el registro");
        this.tUsuario.setEditable(false);
        this.tUsuario.setBounds(480, 80, 79, 26);
        this.tUsuario.setFont(Fuente);
        getContentPane().add(this.tUsuario);

        // presentamos el botón nuevo
        JButton btnNuevo = new JButton("Nuevo");
        btnNuevo.setBounds(578, 45, 110, 26);
        btnNuevo.setFont(Fuente);
        btnNuevo.setToolTipText("Pulse para agregar un nuevo item");
        btnNuevo.setIcon(new ImageIcon(getClass().getResource("/Graficos/manadir.png")));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);

        // el botón grabar
        JButton btnGrabar = new JButton("Grabar");
        btnGrabar.setToolTipText("Graba el registro en la base");
        btnGrabar.setBounds(578, 80, 110, 26);
        btnGrabar.setFont(Fuente);
        btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        getContentPane().add(btnGrabar);
        btnGrabar.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });

        // definimos la tabla
        tElementos = new JTable();
        tElementos.setToolTipText("Pulse sobre un item para editarlo");
        tElementos.setModel(new DefaultTableModel(
                        new Object[][] {{ null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null},
                        },
                        new String[] {"ID",
                        		      "Item",
                        		      "Valor",
                        		      "Critico",
                        		      "Usuario",
                        		      "Ver",
                        		      "Imp."
                        }) {
            @SuppressWarnings("rawtypes")
            Class[] columnTypes = new Class[] {
                                      Integer.class,
                                      String.class,
                                      Float.class,
                                      Integer.class,
                                      String.class,
                                      Object.class,
                                      Object.class
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }
        });

        // fijamos el alto de las filas
        this.tElementos.setRowHeight(25);

        // fijamos el ancho de las columnas y dejamos que la
        // descripción ocupe el resto del espacio
        this.tElementos.getColumn("ID").setPreferredWidth(0);
        this.tElementos.getColumn("ID").setMaxWidth(0);
        this.tElementos.getColumn("Valor").setPreferredWidth(75);
        this.tElementos.getColumn("Valor").setMaxWidth(75);
        this.tElementos.getColumn("Critico").setPreferredWidth(80);
        this.tElementos.getColumn("Critico").setMaxWidth(80);
        this.tElementos.getColumn("Usuario").setPreferredWidth(85);
        this.tElementos.getColumn("Usuario").setMaxWidth(85);
        this.tElementos.getColumn("Imp.").setMaxWidth(35);
        this.tElementos.getColumn("Imp.").setPreferredWidth(35);
        this.tElementos.getColumn("Ver").setMaxWidth(35);
        this.tElementos.getColumn("Ver").setPreferredWidth(35);

        // definimos la fuente
        this.tElementos.setFont(Fuente);

        // definimos el scrollpane y le pasamos la tabla
        this.Scroll = new JScrollPane(this.tElementos);
        this.Scroll.setViewportView(this.tElementos);
        this.Scroll.setBounds(12, 155, 678, 350);
        getContentPane().add(this.Scroll);

        // cargamos el diccionario
        this.cargaElementos();

        // fijamos el evento click de la tabla
        this.tElementos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tElementosMouseClicked(evt);
            }
        });

    }

    // método llamado al pulsar sobre la grilla
    private void tElementosMouseClicked(java.awt.event.MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tElementos.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tElementos.rowAtPoint(evt.getPoint());
        int columna = this.tElementos.columnAtPoint(evt.getPoint());

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // obtenemos la clave del item
            int clave = (int) modeloTabla.getValueAt(fila, 0);

            // si pulsó en editar
            if (columna == 5){

                // muestra el item
                this.muestraItem(clave);

            // si pulsó en imprimir
            } else if (columna == 6){

            	// genera la etiqueta
            	this.generaEtiqueta(clave);

            }

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param iditem - entero con la clave del item
     * Método que recibe como parámetro la clave de un item y
     * genera la etiqueta
     */
    protected void generaEtiqueta(int iditem) {

    	// generamos la etiqueta
    	new Etiquetas(iditem);

    }

    // evento que carga el diccionario de items en la tabla
    protected void cargaElementos() {

        // sobrecargamos el renderer de la tabla
        this.tElementos.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) tElementos.getModel();

        // hacemos la tabla se pueda ordenar
        this.tElementos.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object[] fila = new Object[7];

        // instanciamos la clase y obtenemos la nómina de items
        ResultSet nominaItems = this.Elementos.getNominaItems();

        try {

            // nos aseguramos de estar al principio
            nominaItems.beforeFirst();

            // recorremos el vector
            while (nominaItems.next()) {

                // fijamos los valores de la fila
                fila[0] = nominaItems.getInt("iditem");
                fila[1] = nominaItems.getString("descripcion");
                fila[2] = nominaItems.getFloat("valor");
                fila[3] = nominaItems.getInt("critico");
                fila[4] = nominaItems.getString("usuario");
                fila[5] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

            // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    // evento llamado al pulsar sobre el botón nuevo
    protected void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {

        // llamamos la rutina de limpiar el formulario
        this.limpiaFormulario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que limpia el formulario
     */
    private void limpiaFormulario(){

        // eliminamos los valores de los campos
        this.tId.setText("");
        this.tDescripcion.setText("");
        this.sCritico.setValue(0);
        this.sValor.setValue(0);
        this.tCodigoSop.setText("");
        this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
        this.tAlta.setText(this.Herramientas.FechaActual());
        this.tUsuario.setText(Seguridad.Usuario);
        this.Archivo = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave del registro
     * Método que recibe como parámetro la clave de un artículo y
     * lo muestra en el formulario de datos
     */
    private void muestraItem(int clave){

        // obtenemos el registro
        this.Elementos.getDatosItem(clave);

        // mostramos el registro
        this.tId.setText(Integer.toString(this.Elementos.getIdItem()));
        this.tDescripcion.setText(this.Elementos.getItem());
        this.sCritico.setValue(this.Elementos.getCritico());
        this.sValor.setValue(this.Elementos.getValor());
        this.tCodigoSop.setText(this.Elementos.getCodigoSop());
        this.tAlta.setText(this.Elementos.getFechaAlta());
        this.tUsuario.setText(this.Elementos.getUsuario());

        // si hay una imagen cargada
        if (Elementos.getImagen() != null) {

            // leemos de la base de datos
            ImageIcon Foto = new ImageIcon(Elementos.getImagen());

            // la escalamos
            Imagenes foto = new Imagenes();
            ImageIcon ajustada = foto.redimensionar(Foto, 110, 110);

            // la asignamos al formulario
            this.lImagen.setIcon(ajustada);

        } else {
            this.lImagen.setIcon(new ImageIcon(FormGrillaItems.class.getResource("/Graficos/sin_imagen.jpg")));
        }

        // fijamos el foco
        this.tDescripcion.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar sobre la imagen
     */
    private void lImagenMouseClicked(java.awt.event.MouseEvent evt) {

        // obtenemos el archivo
        Imagenes foto = new Imagenes();
        foto.leerImagen();

        // si seleccionó un archivo
        if (!foto.getArchivo().isEmpty()){

            // lo asignamos al label redimensionando la imagen
            ImageIcon imagen = foto.cargarImagen(110,110);
            this.lImagen.setIcon(imagen);

            // asignamos la ruta de la imagen
            this.Archivo = foto.getArchivo();

            // obtenemos el archivo y sus propiedades
            foto.obtenerImagen();

            // asignamos en las variables de clase
            this.Contenido = foto.getContenido();
            this.Longitud = foto.getLongitud();

        // si canceló
        } else {

            // inicializamos la variable
            this.Archivo = "";

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param evt el evento generado (no lo usamos)
     * Método ejecutado cuando se pulsa el botón grabar
     */
    public void btnGrabarActionPerformed(java.awt.event.ActionEvent evt){

        // si está editando
        if (!this.tId.getText().equals("")) {

            // asignamos en la clase
            this.Elementos.setIdItem(Integer.parseInt(this.tId.getText()));

        }

        // verificamos se halla ingresado la descripción
        if (this.tDescripcion.getText().equals("")) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Ingrese la Descripción del Item",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.tDescripcion.requestFocus();
            return;

        // si está dando altas
        } else if (this.tId.getText().equals("")) {

            // verifica que no esté declarado
            if (Elementos.verificaDescripcion(this.tDescripcion.getText())) {

                // presenta el mensaje
                JOptionPane.showMessageDialog(this,
                		                      "Ese item ya se encuentra declarado",
                		                      "Error",
                		                      JOptionPane.ERROR_MESSAGE);
                this.tDescripcion.requestFocus();
                return;

            }

        }

        // asigna en la clase
        this.Elementos.setItem(this.tDescripcion.getText());

        // si ingresó el costo
        if (!this.sValor.getValue().toString().equals("0")) {

            // asignamos en la clase
            Elementos.setValor(Float.parseFloat(this.sValor.getValue().toString()));

        }

        // el crítico lo permitimos en cero
        this.Elementos.setCritico(Integer.parseInt(this.sCritico.getValue().toString()));

        // el sop lo permite en blanco
        this.Elementos.setCodigoSop(this.tCodigoSop.getText());

        // si cargó una imagen
        if (this.Archivo != null) {

            // fijamos las propiedades de la imagen
            this.Elementos.setImagen(this.Contenido);
            this.Elementos.setLongitud(this.Longitud);

        }

        // grabamos el item en la base
        this.Elementos.grabaItem();

        // mostramos el mensaje
        new Mensaje("Registro grabado ...");

        // recargamos la grilla para reflejar los cambios
        this.cargaElementos();

        // limpiamos el formulario
        this.limpiaFormulario();

    }

/**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param evt el evento generado (no lo usamos)
     * Método ejecutado cuando se pulsa el botón exportar
     * y genera un excel con los elementos del stock
     */
    public void btnExportarActionPerformed(java.awt.event.ActionEvent evt){

        // generamos el reporte
        new XLSItems();

    }

}
