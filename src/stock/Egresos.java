/*
 * Nombre: Egresos
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 16/10/2018
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnóstico
 * Comentarios: Clase que controla las operaciones de egresos
 *              de materiales al inventario
 */

// definición del paquete
package stock;

//inclusión de librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Image;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import dbApi.Conexion;
import seguridad.Seguridad;
import java.util.logging.Level;
import java.util.logging.Logger;

// definición de la clase
public class Egresos {

    // definición de variables de clase
    protected Conexion Enlace;

    // de la vista de egresos
    protected int IdEgreso;             // clave del registro
    protected int IdItem;               // clave del registro
    protected String Item;              // nombre del item
    protected Image Foto;               // imagen del artículo
    protected String CodigoSop;         // clave para la apn del artículo
    protected int IdLaboratorio;        // clave del laboratorio
    protected String Laboratorio;       // nombre del laboratorio
    protected int Remito;               // número de remito
    protected int Cantidad;             // número de elementos
    protected String FechaEgreso;       // fecha del egreso
    protected String Entrego;           // usuario que entregó
    protected int IdEntrego;            // clave del usuario que entregó
    protected String Recibio;           // usuario que recibió
    protected int IdRecibio;            // clave del registro
    protected String Comentarios;       // comentarios y observaciones

    // constructor de la clase
    public Egresos(){

        // las variables de los egresos
        this.IdEgreso = 0;
        this.Entrego = "";
        this.IdEntrego = 0;
        this.Recibio = "";
        this.IdRecibio = 0;
        this.IdItem = 0;
        this.Item = "";
        this.Foto = null;
        this.CodigoSop = "";
        this.FechaEgreso = "";
        this.Laboratorio = "";
        this.Remito = 0;
        this.Cantidad = 0;
        this.Comentarios = "";

        // inicializamos el usuario y el laboratorio
        this.IdLaboratorio = Seguridad.Laboratorio;
        this.IdEntrego = Seguridad.Id;

        // nos conectamos a la base
        this.Enlace = new Conexion();

    }

    // métodos de asignación de valores
    public void setIdEgreso(int idegreso){
        this.IdEgreso = idegreso;
    }
    public void setIdRecibio(int idrecibio){
        this.IdRecibio = idrecibio;
    }
    public void setIdItem(int iditem){
        this.IdItem = iditem;
    }
    public void setRemito(int remito){
        this.Remito = remito;
    }
    public void setFechaEgreso(String fechaegreso){
        this.FechaEgreso = fechaegreso;
    }
    public void setCantidad(int cantidad){
        this.Cantidad = cantidad;
    }
    public void setComentarios(String comentarios){
        this.Comentarios = comentarios;
    }

    // métodos de retorno de valores
    public int getIdEgreso(){
        return this.IdEgreso;
    }
    public int getIdItem(){
        return this.IdItem;
    }
    public String getItem(){
        return this.Item;
    }
    public Image getFoto() {
    	return this.Foto;
    }
    public int getCantidad(){
        return this.Cantidad;
    }
    public String getEntrego(){
        return this.Entrego;
    }
    public int getIdEntrego(){
        return this.IdEntrego;
    }
    public int getRemito(){
        return this.Remito;
    }
    public String getFechaEgreso(){
        return this.FechaEgreso;
    }
    public String getRecibio(){
        return this.Recibio;
    }
    public int getIdRecibio(){
        return this.IdRecibio;
    }
    public String getComentarios(){
        return this.Comentarios;
    }
    public String getLaboratorio(){
    	return this.Laboratorio;
    }
    public String getCodigoSop(){
    	return this.CodigoSop;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los egresos del inventario
     * Método que retorna los egresos del inventario
     */
    public ResultSet getEgresos(){

        // declaración de variables
        String Consulta;
        ResultSet Egresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_egresos.idegreso AS id_egreso, " +
                   "       diagnostico.v_egresos.descripcion AS item, " +
                   "       diagnostico.v_egresos.remito AS remito, " +
                   "       diagnostico.v_egresos.cantidad AS cantidad, " +
                   "       diagnostico.v_egresos.entrego AS entrego, " +
                   "       diagnostico.v_egresos.recibio AS recibio, " +
                   "       diagnostico.v_egresos.fecha_egreso AS fecha " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idlaboratorio = '" + this.IdLaboratorio + "' " +
                   "ORDER BY diagnostico.v_egresos.descripcion; ";
        Egresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Egresos;

    }


    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio string con la fecha inicial
     * @param fechafin string con la fecha final
     * @return resultset con los egresos
     * Método que retorna los egresos del inventario en un período de tiempo
     */
    public ResultSet getEgresos(String fechainicio, String fechafin){

        // declaración de variables
        String Consulta;
        ResultSet Egresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_egresos.idegreso AS id_egreso, " +
                   "       diagnostico.v_egresos.descripcion AS item, " +
                   "       diagnostico.v_egresos.remito AS remito, " +
                   "       diagnostico.v_egresos.cantidad AS cantidad, " +
                   "       diagnostico.v_egresos.entrego AS entrego, " +
                   "       diagnostico.v_egresos.recibio AS recibio, " +
                   "       diagnostico.v_egresos.fecha_egreso AS fecha " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') " +
                   "ORDER BY diagnostico.v_egresos.descripcion; ";
        Egresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Egresos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio string con la fecha de inicio
     * @param fechafin string con la fecha de finalizacion
     * @param item cadena con la descripcion del item
     * @return resultset con los egresos
     * Método que retorna los egresos del inventario en un período de tiempo
     * para un determinado item
     */
    public ResultSet getEgresos(String fechainicio, String fechafin, String item){

        // declaración de variables
        String Consulta;
        ResultSet Egresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_egresos.idegreso AS id_egreso, " +
                   "       diagnostico.v_egresos.descripcion AS item, " +
                   "       diagnostico.v_egresos.remito AS remito, " +
                   "       diagnostico.v_egresos.cantidad AS cantidad, " +
                   "       diagnostico.v_egresos.entrego AS entrego, " +
                   "       diagnostico.v_egresos.recibio AS recibio, " +
                   "       diagnostico.v_egresos.fecha_egreso AS fecha " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') AND " +
                   "      diagnostico.v_egresos.item LIKE '%" + item + "%' " +
                   "ORDER BY diagnostico.v_egresos.descripcion; ";
        Egresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Egresos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio string con la fecha inicial
     * @param fechafin string con la fecha final
     * @return resultset con los egresos
     * Método que retorna los egresos del inventario en un período de tiempo
     * agrupados por artículo y fuente de financiamiento
     */
    public ResultSet conciliadoEgresos(String fechainicio, String fechafin){

        // declaración de variables
        String Consulta;
        ResultSet Egresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_egresos.descripcion AS item, " +
                   "       SUM(diagnostico.v_egresos.cantidad) AS cantidad, " +
        		   "       diagnostico.v_egresos.codigosop AS codigosop " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND " +
                   "      STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') " +
                   "GROUP BY diagnostico.v_egresos.descripcion " +
                   "ORDER BY diagnostico.v_egresos.descripcion; ";
        Egresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Egresos;

    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param item cadena con la descripcion del item
     * @return vector con los egresos de ese item
     * Método que retorna los egresos del inventario de un item
     */
    public ResultSet getEgresos(String item){

        // declaración de variables
        String Consulta;
        ResultSet Egresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_egresos.idegreso AS id_egreso, " +
                   "       diagnostico.v_egresos.item AS item, " +
                   "       diagnostico.v_egresos.remito AS remito, " +
                   "       diagnostico.v_egresos.cantidad AS cantidad, " +
                   "       diagnostico.v_egresos.entrego AS entrego, " +
                   "       diagnostico.v_egresos.recibio AS recibio, " +
                   "       diagnostico.v_egresos.fecha_egreso AS fecha " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idlaboratorio = '" + Seguridad.Laboratorio + "' AND " +
                   "      diagnostico.v_egresos.descripcion LIKE '%" + item + "%' " +
                   "ORDER BY diagnostico.v_egresos.descripcion; ";
        Egresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Egresos;

    }

    /**
     * @return entero con la clave del registro
     * Método que graba el egreso del inventario retorna la clave del registro
     */
    public int grabaEgreso(){

        // si está editando
        if (this.IdEgreso != 0){
            this.editaEgreso();
        } else {
            this.nuevoEgreso();
        }

        // retornamos la id
        return this.IdEgreso;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de un nuevo
     * egreso el inventario lo actualiza por los triggers de la
     * base
     */
    protected void nuevoEgreso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.egresos " +
                   "       (item, " +
                   "        id_laboratorio, " +
                   "        remito, " +
                   "        cantidad, " +
                   "        fecha, " +
                   "        id_entrego, " +
                   "        id_recibio, " +
                   "        comentarios) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?);";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdItem);
            psInsertar.setInt(2, this.IdLaboratorio);
            psInsertar.setInt(3, this.Remito);
            psInsertar.setInt(4, this.Cantidad);
            psInsertar.setString(5, this.FechaEgreso);
            psInsertar.setInt(6, this.IdEntrego);
            psInsertar.setInt(7, this.IdRecibio);
            psInsertar.setString(8, this.Comentarios);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // obtenemos la id del registro
        this.IdEgreso = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de actualización de la
     * tabla de egresos, el inventario lo actualiza por triggers
     */
    protected void editaEgreso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta estamos editando así que el
        // remito no lo tocamos
        Consulta = "UPDATE diagnostico.egresos SET " +
                   "       item = ?, " +
                   "       cantidad = ?, " +
                   "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                   "       id_entrego = ?, " +
                   "       id_recibio = ?, " +
                   "       comentarios = ? " +
                   "WHERE diagnostico.egresos.id = ?;";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdItem);
            psInsertar.setInt(2, this.Cantidad);
            psInsertar.setString(3, this.FechaEgreso);
            psInsertar.setInt(4, this.IdEntrego);
            psInsertar.setInt(5, this.IdRecibio);
            psInsertar.setString(6, this.Comentarios);
            psInsertar.setInt(7, this.IdEgreso);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return remito un entero con el número de remito
     * Método que retorna un entero con el próximo número de remito, utilizado en
     * las altas de egresos
     */
    public int getRemitoEgreso(){

        // declaración de variables
        String Consulta;
        int Remito = 0;
        ResultSet Resultado;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT MAX(diagnostico.egresos.remito) + 1 AS remito " +
                   "FROM diagnostico.egresos " +
                   "WHERE diagnostico.egresos.id_laboratorio = '" + Seguridad.Laboratorio + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // si no es el primer registro
            if (Resultado.getString("remito") != null){
                Remito = Resultado.getInt("remito");
            } else {
                Remito = 1;
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos
        return Remito;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param usuario el nombre de un usuario
     * @return int la clave del usuario
     * @deprecated - ahora utilizamos el combo con clave y no
     * es necesario
     * Método utilizado en los select de stock para obtener la clave
     * del usuario que recibió la mercadería en los egresos
     */
    public int getClaveUsuario(String usuario){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        int idUsuario = 0;

        // componemos la consulta
        Consulta = "SELECT cce.responsables.id AS isusuario " +
                   "FROM cce.responsables " +
                   "WHERE cce.responsables.usuario = '" + usuario + "'; ";

        // obtenemos el registro
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // si hay registros
            if (Resultado.next()){

                // asignamos el valor
                idUsuario = Resultado.getInt("idusuario");

            // si no lo encontró
            } else {

                // asignamos
                idUsuario = 0;

            }

        // si hubo un error
        } catch (SQLException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

        // retornamos
        return idUsuario;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idegreso
     * Método que recibe como parámetro la id de un egreso y asigna
     * en las variables de clase los datos del registro
     */
    public void getDatosEgreso(int idegreso){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Blob Archivo;

        // componemos la consulta sobre la vista
        Consulta = "SELECT diagnostico.v_egresos.idegreso AS idegreso, " +
                   "       diagnostico.v_egresos.descripcion AS descripcion, " +
        		   "       diagnostico.v_egresos.imagen AS imagen, " +
                   "       diagnostico.v_egresos.iditem AS iditem, " +
        		   "       diagnostico.v_egresos.codigosop AS codigosop, " +
                   "       diagnostico.v_egresos.remito AS remito, " +
                   "       diagnostico.v_egresos.cantidad AS cantidad, " +
                   "       diagnostico.v_egresos.fecha_egreso AS fecha_egreso, " +
                   "       diagnostico.v_egresos.entrego AS entrego, " +
                   "       diagnostico.v_egresos.recibio AS recibio, " +
                   "       diagnostico.v_egresos.idrecibio AS idrecibio, " +
                   "       diagnostico.v_egresos.comentarios AS comentarios " +
                   "FROM diagnostico.v_egresos " +
                   "WHERE diagnostico.v_egresos.idegreso = '" + idegreso + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdEgreso = Resultado.getInt("idegreso");
            this.Entrego = Resultado.getString("entrego");
            this.Recibio = Resultado.getString("recibio");
            this.IdRecibio = Resultado.getInt("idrecibio");
            this.IdItem = Resultado.getInt("iditem");
            this.Item = Resultado.getString("descripcion");
            this.CodigoSop = Resultado.getString("codigosop");
            this.FechaEgreso = Resultado.getString("fecha_egreso");
            this.Remito = Resultado.getInt("remito");
            this.Cantidad = Resultado.getInt("cantidad");
            this.Comentarios = Resultado.getString("comentarios");

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("imagen") != null){
                Archivo = Resultado.getBlob("imagen");
                this.Foto = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            }
            
        // si hubo un error
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del egreso
     * Mètodo que ejecuta la consulta de eliminaciòn de un egreso
     */
    public void borraEgreso(int clave){

    	// definición de variables
    	String Consulta;

    	// componemos y ejecutamos la consulta
    	Consulta = "DELETE FROM diagnostico.egresos " +
    	           "WHERE diagnostico.egresos.id = '" + clave + "';";
    	this.Enlace.Ejecutar(Consulta);

    }

}
