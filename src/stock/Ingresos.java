/*
 * Nombre: Ingresos
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 16/10/2018
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnóstico
 * Comentarios: Clase que controla las operaciones de ingresos
 *              de materiales al inventario
 */

// definición del paquete
package stock;

// inclusión de librerías
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.Image;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import dbApi.Conexion;
import seguridad.Seguridad;
import java.util.logging.Level;
import java.util.logging.Logger;

// definición de la clase
public class Ingresos {

    // de la vista de ingresos
    protected int IdIngreso;          // clave del registro
    protected int IdItem;             // clave del item
    protected String Item;            // descripción del item
    protected Image Foto;             // imagen del artículo
    protected String CodigoSop;       // código de la apn del item
    protected float Importe;          // importe abonado
    protected int IdLaboratorio;      // clave del laboratorio
    protected String Laboratorio;     // nombre del laboratorio
    protected int IdFinanciamiento;   // clave del financiamiento de la compra
    protected String Financiamiento;  // nombre de la fuente de financiamiento
    protected String Remito;          // número de remito
    protected int Cantidad;           // número de elementos salientes
    protected String Vencimiento;     // fecha de vencimiento
    protected String Lote;            // número de lote
    protected String Ubicacion;       // ubicación del almacenamiento
    protected String FechaIngreso;    // fecha de alta del registro
    protected String Comentarios;     // comentarios y observaciones
    protected int IdUsuario;          // clave del usuario que dió el ingreso
    protected String Usuario;         // nombre del usuario

    // definición de variables de clase
    protected Conexion Enlace;

    // constructor de la clase
    public Ingresos(){

        // nos conectamos a la base
        this.Enlace = new Conexion();

        // inicializamos las variables
        // las variables de los ingresos
        this.IdIngreso = 0;
        this.IdItem = 0;
        this.Item = "";
        this.CodigoSop = "";
        this.Importe = 0;
        this.Laboratorio = "";
        this.IdFinanciamiento = 0;
        this.Remito = "";
        this.Cantidad = 0;
        this.Vencimiento = "";
        this.Lote = "";
        this.Ubicacion = "";
        this.FechaIngreso = "";
        this.Comentarios = "";
        this.Usuario = "";

        // obtenemos la clave del usuario y del laboratorio
        this.IdLaboratorio = Seguridad.Laboratorio;
        this.IdUsuario = Seguridad.Id;

    }

    // métodos de asignación de valores
    public void setIdIngreso(int idingreso){
        this.IdIngreso = idingreso;
    }
    public void setIdItem(int iditem){
    	this.IdItem = iditem;
    }
    public void setImporte(float importe){
        this.Importe = importe;
    }
    public void setIdFinanciamiento(int idfinanciamiento){
    	this.IdFinanciamiento = idfinanciamiento;
    }
    public void setRemito(String remito){
        this.Remito = remito;
    }
    public void setCantidad(int cantidad){
        this.Cantidad = cantidad;
    }
    public void setFechaIngreso(String fechaingreso){
        this.FechaIngreso = fechaingreso;
    }
    public void setVencimiento(String vencimiento){
        this.Vencimiento = vencimiento;
    }
    public void setComentarios(String comentarios){
        this.Comentarios = comentarios;
    }
    public void setLote(String lote){
    	this.Lote = lote;
    }
    public void setUbicacion(String ubicacion){
    	this.Ubicacion = ubicacion;
    }

    // métodos de retorno de valores
    public int getIdIngreso(){
        return this.IdIngreso;
    }
    public int getIdItem(){
    	return this.IdItem;
    }
    public String getItem(){
    	return this.Item;
    }
    public Image getFoto() {
    	return this.Foto;
    }
    public String getCodigoSop(){
    	return this.CodigoSop;
    }
    public float getImporte(){
        return this.Importe;
    }
    public String getFinanciamiento(){
    	return this.Financiamiento;
    }
    public int getIdFinanciamiento(){
    	return this.IdFinanciamiento;
    }
    public int getIdLaboratorio(){
        return this.IdLaboratorio;
    }
    public String getLaboratorio(){
        return this.Laboratorio;
    }
    public String getRemito(){
        return this.Remito;
    }
    public int getCantidad(){
        return this.Cantidad;
    }
    public String getVencimiento(){
        return this.Vencimiento;
    }
    public String getFechaIngreso(){
        return this.FechaIngreso;
    }
    public String getComentarios(){
        return this.Comentarios;
    }
    public String getUsuario(){
    	return this.Usuario;
    }
    public String getLote(){
    	return this.Lote;
    }
    public String getUbicacion(){
    	return this.Ubicacion;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con la nomina de ingresos
     * Método que retorna los ingresos al inventario
     */
    public ResultSet getIngresos(){

        // declaración de variables
        String Consulta;
        ResultSet Ingresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.iditem AS iditem, " +
                   "       diagnostico.v_ingresos.descripcion AS item, " +
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " +
        		   "       diagnostico.v_ingresos.idingreso AS id_ingreso, " +
                   "       diagnostico.v_ingresos.importe AS importe, " +
                   "       diagnostico.v_ingresos.remito AS remito, " +
                   "       diagnostico.v_ingresos.id_financiamiento AS id_financiamiento, " +
                   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " +
                   "       diagnostico.v_ingresos.cantidad AS cantidad, " +
                   "       diagnostico.v_ingresos.lote AS lote, " +
                   "       diagnostico.v_ingresos.vencimiento AS vencimiento, " +
                   "       diagnostico.v_ingresos.usuario AS usuario " +
                   "FROM diagnostico.v_ingresos " +
                   "WHERE diagnostico.v_ingresos.idlaboratorio = '" + this.IdLaboratorio + "' " +
                   "ORDER BY diagnostico.v_ingresos.descripcion; ";
        Ingresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Ingresos;

    }


    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio string con la fecha inicial
     * @param fechafin string con la fecha final
     * @return vector con los egresos
     * Método que retorna los ingresos al inventario en un período de tiempo
     */
    public ResultSet getIngresos(String fechainicio, String fechafin){

        // declaración de variables
        String Consulta;
        ResultSet Ingresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.iditem AS iditem, " +
                   "       diagnostico.v_ingresos.descripcion AS item, " +
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " +
        		   "       diagnostico.v_ingresos.idingreso AS id_ingreso, " +
                   "       diagnostico.v_ingresos.importe AS importe, " +
                   "       diagnostico.v_ingresos.remito AS remito, " +
                   "       diagnostico.v_ingresos.id_financiamiento AS id_financiamiento, " +
                   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " +
                   "       diagnostico.v_ingresos.cantidad AS cantidad, " +
                   "       diagnostico.v_ingresos.lote AS lote, " +
                   "       diagnostico.v_ingresos.vencimiento AS vencimiento, " +
                   "       diagnostico.v_ingresos.usuario AS usuario " +
                   "FROM diagnostico.v_ingresos " +
                   "WHERE diagnostico.v_ingresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND " +
                   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') " +
                   "ORDER BY diagnostico.v_ingresos.descripcion; ";

        // ejecutamos la consulta
        Ingresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Ingresos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio cadena con la fecha de inicio
     * @param fechafin cadena con la fecha de finalizacion
     * @param item cadena con el nombre del item
     * @return vector con la nomina de egresos
     * Método que retorna los ingresos al inventario en un período de tiempo
     * para un cierto item
     */
    public ResultSet getIngresos(String fechainicio, String fechafin, String item){

        // declaración de variables
        String Consulta;
        ResultSet Ingresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.iditem AS iditem, " +
                   "       diagnostico.v_ingresos.descripcion AS item, " +
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " +
        		   "       diagnostico.v_ingresos.idingreso AS id_ingreso, " +
                   "       diagnostico.v_ingresos.importe AS importe, " +
                   "       diagnostico.v_ingresos.remito AS remito, " +
                   "       diagnostico.v_ingresos.id_financiamiento AS id_financiamiento, " +
                   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " +
                   "       diagnostico.v_ingresos.cantidad AS cantidad, " +
                   "       diagnostico.v_ingresos.lote AS lote, " +
                   "       diagnostico.v_ingresos.vencimiento AS vencimiento, " +
                   "       diagnostico.v_ingresos.usuario AS usuario " +
                   "FROM diagnostico.v_ingresos " +
                   "WHERE diagnostico.v_ingresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND " +
                   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') AND " +
                   "      diagnostico.v_ingresos.descripcion LIKE '%" + item + "%' " +
                   "ORDER BY diagnostico.v_ingresos.descripcion; ";

        // ejecutamos la consulta
        Ingresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Ingresos;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fechainicio - string con la fecha inicial 
     * @param fechafin - string con la fecha final
     * @return vector con los registros encontrados
     * Método que recibe como parámetros una fecha inicial y una
     * fecha final y retorna los ingresos agrupados por artículo 
     * y fuente de financiamiento en el período solicitado
     */
    public ResultSet conciliadoIngresos(String fechainicio, String fechafin) {

        // declaración de variables
        String Consulta;
        ResultSet Ingresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.descripcion AS item, " + 
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " + 
        		   "       SUM(diagnostico.v_ingresos.importe) AS importe, " + 
        		   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " + 
        		   "       SUM(diagnostico.v_ingresos.cantidad) AS cantidad " + 
        		   "FROM diagnostico.v_ingresos " + 
        		   "WHERE diagnostico.v_ingresos.idlaboratorio = '" + Seguridad.Laboratorio + "' AND " + 
        		   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') >= STR_TO_DATE('" + fechainicio + "', '%d/%m/%Y') AND \n" + 
        		   "      STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%d/%m/%Y') <= STR_TO_DATE('" + fechafin + "', '%d/%m/%Y') \n" + 
        		   "GROUP BY diagnostico.v_ingresos.descripcion," + 
        		   "         diagnostico.v_ingresos.financiamiento " + 
        		   "ORDER BY diagnostico.v_ingresos.descripcion, " + 
        		   "         diagnostico.v_ingresos.financiamiento;";

        // ejecutamos la consulta
        Ingresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Ingresos;
    	
    }
    
    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param item nombre del item
     * @return vector con la nomina de ingresos
     * Método que retorna los ingresos al inventario de un determinado item
     */
    public ResultSet getIngresos(String item){

        // declaración de variables
        String Consulta;
        ResultSet Ingresos;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.iditem AS iditem, " +
                   "       diagnostico.v_ingresos.descripcion AS item, " +
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " +
        		   "       diagnostico.v_ingresos.idingreso AS id_ingreso, " +
                   "       diagnostico.v_ingresos.importe AS importe, " +
                   "       diagnostico.v_ingresos.remito AS remito, " +
                   "       diagnostico.v_ingresos.id_financiamiento AS id_financiamiento, " +
                   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " +
                   "       diagnostico.v_ingresos.cantidad AS cantidad, " +
                   "       diagnostico.v_ingresos.lote AS lote, " +
                   "       diagnostico.v_ingresos.vencimiento AS vencimiento, " +
                   "       diagnostico.v_ingresos.usuario AS usuario " +
                   "FROM diagnostico.v_ingresos " +
                   "WHERE diagnostico.v_ingresos.idlaboratorio = '" + this.IdLaboratorio + "' AND " +
                   "      diagnostico.v_ingresos.descripcion LIKE '%" + item + "%' " +
                   "ORDER BY diagnostico.v_ingresos.descripcion; ";
        Ingresos = this.Enlace.Consultar(Consulta);

        // retornamos el vector
        return Ingresos;

    }

    /**
     * @return id del registro afectado
     * Método que público que ejecuta la consulta de actualización
     * de la tabla de ingresos
     */
    public int grabaIngreso(){

        // si está insertando
        if (this.IdIngreso == 0){

            // insertamos el registro
            this.nuevoIngreso();

        // si está editando
        } else {

            // ejecutamos la edición
            this.editaIngreso();

        }

        // retornamos la id
        return this.IdIngreso;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción de la tabla
     * de ingresos, el inventario se actualiza automáticamente
     * por triggers
     */
    protected void nuevoIngreso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.ingresos " +
                   "       (item, " +
                   "        importe, " +
                   "        id_laboratorio, " +
                   "        id_financiamiento, " +
                   "        remito, " +
                   "        cantidad, " +
                   "        vencimiento, " +
                   "        lote, " +
                   "        ubicacion, "  +
                   "        fecha, " +
                   "        id_usuario, " +
                   "        comentarios) " +
                   "       VALUES " +
                   "       (?, ?, ?, ?, ?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?);";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdItem);
            psInsertar.setFloat(2, this.Importe);
            psInsertar.setInt(3, this.IdLaboratorio);
            psInsertar.setInt(4, this.IdFinanciamiento);
            psInsertar.setString(5, this.Remito);
            psInsertar.setInt(6, this.Cantidad);
            psInsertar.setString(7, this.Vencimiento);
            psInsertar.setString(8, this.Lote);
            psInsertar.setString(9, this.Ubicacion);
            psInsertar.setString(10, this.FechaIngreso);
            psInsertar.setInt(11, this.IdUsuario);
            psInsertar.setString(12, this.Comentarios);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

            // obtenemos la id del registro
            this.IdIngreso = this.Enlace.UltimoInsertado();

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición de la tabla
     * de ingresos, el inventario se actualiza automáticamente
     * por los triggers
     */
    protected void editaIngreso(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta estamos editando
        Consulta = "UPDATE diagnostico.ingresos SET " +
                "       item = ?, " +
                "       importe = ?, " +
                "       id_financiamiento = ?, " +
                "       remito = ?, " +
                "       cantidad = ?, " +
                "       vencimiento = STR_TO_DATE(?, '%d/%m/%Y'), " +
                "       lote = ?, " +
                "       ubicacion = ?, " +
                "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                "       id_usuario = ?, " +
                "       comentarios = ? " +
                "WHERE diagnostico.ingresos.id = ?;";

        try {

            // asignamos la consulta a la conexión
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros
            psInsertar.setInt(1, this.IdItem);
            psInsertar.setFloat(2, this.Importe);
            psInsertar.setInt(3, this.IdFinanciamiento);
            psInsertar.setString(4, this.Remito);
            psInsertar.setInt(5, this.Cantidad);
            psInsertar.setString(6, this.Vencimiento);
            psInsertar.setString(7, this.Lote);
            psInsertar.setString(8, this.Ubicacion);
            psInsertar.setString(9, this.FechaIngreso);
            psInsertar.setInt(10, this.IdUsuario);
            psInsertar.setString(11, this.Comentarios);
            psInsertar.setInt(12, this.IdIngreso);

            // ejecutamos la consulta
            psInsertar.executeUpdate();

            // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - la clave del ingreso
     * Método que recibe como parámetro la clave de un ingreso
     * y ejecuta la consulta de eliminación, el inventario se
     * actualiza por los triggers de la base
     */
    public void borraIngreso(int clave){

    	// declaración de variables
    	String Consulta;

    	// componemos la consulta
    	Consulta = "DELETE FROM diagnostico.ingresos " +
    	           "WHERE diagnostico.ingresos.id = '" + clave + "';";
    	this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idingreso
     * Método que recibe como parámetro la id de un ingreso y
     * asigna en las variables de clase los datos del registro
     */
    public void getDatosIngreso(int idingreso){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Blob Archivo;
        
        // componemos la consulta
        Consulta = "SELECT diagnostico.v_ingresos.idingreso AS idingreso, " +
                   "       diagnostico.v_ingresos.descripcion AS descripcion, " +
        		   "       diagnostico.v_ingresos.codigosop AS codigosop, " +
                   "       diagnostico.v_ingresos.importe AS importe, " +
        		   "       diagnostico.v_ingresos.id_financiamiento AS idfinanciamiento, " +
                   "       diagnostico.v_ingresos.imagen AS foto, " +
        		   "       diagnostico.v_ingresos.financiamiento AS financiamiento, " +
                   "       diagnostico.v_ingresos.remito AS remito, " +
                   "       diagnostico.v_ingresos.cantidad AS cantidad, " +
                   "       diagnostico.v_ingresos.vencimiento AS vencimiento, " +
                   "       diagnostico.v_ingresos.fecha_ingreso AS fecha_ingreso, " +
                   "       diagnostico.v_ingresos.lote AS lote, " +
                   "       diagnostico.v_ingresos.ubicacion AS ubicacion, " +
                   "       diagnostico.v_ingresos.usuario AS usuario, " +
                   "       diagnostico.v_ingresos.comentarios AS comentarios " +
                   "FROM diagnostico.v_ingresos " +
                   "WHERE diagnostico.v_ingresos.idingreso = '" + idingreso + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // vamos al primer registro
            Resultado.next();

            // asignamos en las variables de clase
            this.IdIngreso = Resultado.getInt("idingreso");
            this.Item = Resultado.getString("descripcion");
            this.CodigoSop = Resultado.getString("codigosop");
            this.Importe = Resultado.getFloat("importe");
            this.Financiamiento = Resultado.getString("financiamiento");
            this.IdFinanciamiento = Resultado.getInt("idfinanciamiento");
            this.Remito = Resultado.getString("remito");
            this.Cantidad = Resultado.getInt("cantidad");
            this.Vencimiento = Resultado.getString("vencimiento");
            this.FechaIngreso = Resultado.getString("fecha_ingreso");
            this.Lote = Resultado.getString("lote");
            this.Ubicacion = Resultado.getString("ubicacion");
            this.Usuario = Resultado.getString("usuario");
            this.Comentarios = Resultado.getString("comentarios");

            // ahora leemos el campo blob imagen y lo convertimos
            // verificando que no sea nulo
            if (Resultado.getBlob("foto") != null){
                Archivo = Resultado.getBlob("foto");
                this.Foto = javax.imageio.ImageIO.read(Archivo.getBinaryStream());
            }

        // si hubo un error
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Stock.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
