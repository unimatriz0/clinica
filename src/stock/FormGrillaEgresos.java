/*
 * Nombre: formGrillaEgresos
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 04/08/2017
 * E-Mail: cinvernizzi@gmail.com
 * Licencia: GPL
 * Proyecto: Diagnostico
 * Comentarios: Procedimiento que arma la grilla de egresos del stock
 *              y permite llamar el formulario de nuevo egreso
 */

// definición del paquete
package stock;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import com.toedter.calendar.JDateChooser;
import javax.swing.table.DefaultTableModel;
import java.awt.event.MouseEvent;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.table.TableRowSorter;
import funciones.Utilidades;
import funciones.ComboClave;
import usuarios.Usuarios;
import seguridad.Seguridad;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormGrillaEgresos extends JDialog {

    // agregamos el serial id
	private static final long serialVersionUID = 1L;

    // definimos las variables
    private JTable tEgresos;
    private JTextField tId;
    private JComboBox<Object> cDescripcion;
    private JDateChooser dFechaAlta;
    private JTextField tEntrego;
    private JTextField tRemito;
    private JSpinner sCantidad;
    private JComboBox<Object> cRecibio;
    private JTextArea tComentarios;
    private Egresos Salidas;
    private Utilidades Herramientas;

    // definimos y configuramos el formulario
    public FormGrillaEgresos (java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

        // instanciamos la clase
        this.Salidas = new Egresos();
        this.Herramientas = new Utilidades();

        // fijamos el tamaño y el layout
        this.setBounds(100, 100, 830, 543);
        getContentPane().setLayout(null);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Salidas del Depósito");

        // inicializamos el formulario
        this.initEgresos();

        // cargamos la nómina de usuarios
        this.cargaUsuarios();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que inicializa el formulario
     */
    @SuppressWarnings({ "serial" })
    private void initEgresos(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

        // define el título
        JLabel lTitulo = new JLabel("<html><b>Salidas del Depósito</b></html>");
        lTitulo.setBounds(10, 5, 179, 26);
        lTitulo.setFont(Fuente);
        getContentPane().add(lTitulo);

        // la clave del egreso
        JLabel lId = new JLabel("ID:");
        lId.setBounds(10, 40, 26, 26);
        lId.setFont(Fuente);
        getContentPane().add(lId);
        this.tId = new JTextField();
        this.tId.setToolTipText("Clave del Registro");
        this.tId.setEditable(false);
        this.tId.setBounds(35, 40, 70, 26);
        this.tId.setFont(Fuente);
        getContentPane().add(this.tId);

        // la descripción del item
        JLabel lDescripcion = new JLabel("Descripción:");
        lDescripcion.setBounds(110, 40, 150, 26);
        lDescripcion.setFont(Fuente);
        getContentPane().add(lDescripcion);
        this.cDescripcion = new JComboBox<>();
        this.cDescripcion.setToolTipText("Seleccione el artículo de la lista");
        this.cDescripcion.setEditable(true);
        this.cDescripcion.setBounds(190, 40, 300, 26);
        this.cDescripcion.setFont(Fuente);
        getContentPane().add(this.cDescripcion);

        // cargamos los artículos
        this.cargaArticulos();

        // el usuario actual que entrega el artículo
        JLabel lEntrego = new JLabel("Entrego:");
        lEntrego.setBounds(548, 40, 70, 26);
        getContentPane().add(lEntrego);
        this.tEntrego = new JTextField();
        this.tEntrego.setToolTipText("Usuario que entregó el elemento");
        this.tEntrego.setEditable(false);
        this.tEntrego.setBounds(605, 40, 114, 26);
        this.tEntrego.setFont(Fuente);
        getContentPane().add(tEntrego);

        // fijamos el usuario por defecto
        this.tEntrego.setText(Seguridad.Usuario);

        // el número de remito
        JLabel lRemito = new JLabel("Remito: ");
        lRemito.setBounds(12, 75, 59, 26);
        lRemito.setFont(Fuente);
        getContentPane().add(lRemito);
        this.tRemito = new JTextField();
        this.tRemito.setToolTipText("Número de Remito de Egreso");
        this.tRemito.setEditable(false);
        this.tRemito.setBounds(65, 75, 85, 26);
        this.tRemito.setFont(Fuente);
        getContentPane().add(this.tRemito);

        // al cargar asumimos que está dando un alta
        this.tRemito.setText(Integer.toString(this.Salidas.getRemitoEgreso()));

        // el spiner con la cantidad
        JLabel lCantidad = new JLabel("Cantidad:");
        lCantidad.setBounds(160, 75, 70, 26);
        lCantidad.setFont(Fuente);
        getContentPane().add(lCantidad);
        this.sCantidad = new JSpinner();
        this.sCantidad.setBounds(220, 75, 59, 26);
        this.sCantidad.setFont(Fuente);
        getContentPane().add(this.sCantidad);

        // la fecha de alta
        JLabel lFechaAlta = new JLabel("Fecha:");
        lFechaAlta.setBounds(290, 75, 53, 26);
        lFechaAlta.setFont(Fuente);
        getContentPane().add(lFechaAlta);
        this.dFechaAlta = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaAlta.setToolTipText("Fecha de Entrega del Item");
        this.dFechaAlta.setBounds(335, 75, 110, 26);
        this.dFechaAlta.setFont(Fuente);
        getContentPane().add(this.dFechaAlta);

        // por defecto fijamos la fecha actual
        Calendar fechaActual = new GregorianCalendar();
        this.dFechaAlta.setCalendar(fechaActual);

        // el combo con los usuarios
        JLabel lRecibio = new JLabel("Recibió:");
        lRecibio.setBounds(455, 75, 70, 26);
        lRecibio.setFont(Fuente);
        getContentPane().add(lRecibio);
        this.cRecibio = new JComboBox<>();
        this.cRecibio.setToolTipText("Indique el usuario que recibe el artículo");
        this.cRecibio.setBounds(510, 75, 250, 26);
        this.cRecibio.setFont(Fuente);
        getContentPane().add(this.cRecibio);

        // los comentarios
        JLabel lComentarios = new JLabel("Comentarios y Observaciones");
        lComentarios.setBounds(10, 110, 236, 26);
        lComentarios.setFont(Fuente);
        getContentPane().add(lComentarios);
        this.tComentarios = new JTextArea();
        this.tComentarios.setToolTipText("Comentarios que considere necesarios");
        this.tComentarios.setBounds(10, 136, 630, 75);
        this.tComentarios.setFont(Fuente);
        getContentPane().add(this.tComentarios);

        // el botón grabar
        JButton btnAceptar = new JButton("Aceptar");
        btnAceptar.setToolTipText("Genera el remito de Salida");
        btnAceptar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnAceptar.setBounds(680, 120, 117, 26);
        btnAceptar.setFont(Fuente);
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabaRemito();
            }
        });
        getContentPane().add(btnAceptar);

        // el botón cancelar
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.setToolTipText("Limpia el formulario sin grabar");
        btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.setBounds(680, 155, 117, 26);
        btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                limpiaFormulario();
            }
        });
        getContentPane().add(btnCancelar);

        // define el scroll
        JScrollPane scrollEgresos = new JScrollPane();
        scrollEgresos.setBounds(10, 220, 790, 275);

        // define la tabla y la agrega al scroll
        this.tEgresos = new JTable();
        this.tEgresos.setModel(new DefaultTableModel(
                        new Object[][] {
                                        { null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null },
                        },
                        new String[] {
                                        "ID",
                                        "Item",
                                        "Remito",
                                        "Cant.",
                                        "Entrego",
                                        "Recibio",
                                        "Fecha",
                                        "Ed.",
                                        "Imp.",
                                        "El."
                        }) {
            @SuppressWarnings("rawtypes")
            Class[] columnTypes = new Class[] {
                            Integer.class,
                            String.class,
                            String.class,
                            Integer.class,
                            String.class,
                            String.class,
                            String.class,
                            Object.class,
                            Object.class,
                            Object.class
            };
            @SuppressWarnings({ "unchecked", "rawtypes" })
            public Class getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
            }
            boolean[] columnEditables = new boolean[] {
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false,
                            false
            };
            public boolean isCellEditable(int row, int column) {
                return columnEditables[column];
            }
        });
        this.tEgresos.setToolTipText("Pulse para ver el detalle del remito");

		// agregamos el evento
		this.tEgresos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tEgresosMouseClicked(evt);
            }
	    });

        // fijamos el ancho de las columnas
        this.tEgresos.getColumn("ID").setPreferredWidth(25);
        this.tEgresos.getColumn("ID").setMaxWidth(25);
        this.tEgresos.getColumn("Cant.").setMaxWidth(50);
        this.tEgresos.getColumn("Cant.").setPreferredWidth(50);
        this.tEgresos.getColumn("Entrego").setMaxWidth(85);
        this.tEgresos.getColumn("Entrego").setPreferredWidth(85);
        this.tEgresos.getColumn("Remito").setMaxWidth(85);
        this.tEgresos.getColumn("Remito").setPreferredWidth(85);
        this.tEgresos.getColumn("Recibio").setMaxWidth(85);
        this.tEgresos.getColumn("Recibio").setPreferredWidth(85);
        this.tEgresos.getColumn("Fecha").setMaxWidth(80);
        this.tEgresos.getColumn("Fecha").setPreferredWidth(80);
        this.tEgresos.getColumn("Ed.").setMaxWidth(30);
        this.tEgresos.getColumn("Ed.").setPreferredWidth(30);
        this.tEgresos.getColumn("Imp.").setMaxWidth(30);
        this.tEgresos.getColumn("Imp.").setPreferredWidth(30);
        this.tEgresos.getColumn("El.").setMaxWidth(30);
        this.tEgresos.getColumn("El.").setPreferredWidth(30);

		// fijamos el alto de las filas
		this.tEgresos.setRowHeight(25);

        // fijamos la fuente
        this.tEgresos.setVisible(true);

        // agregamos la tabla al scroll
		scrollEgresos.setViewportView(this.tEgresos);
		getContentPane().add(scrollEgresos);

        // cargamos la nómina de egresos
        this.grillaEgresos();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el combo de artículos los items del
     * diccionario
     */
    protected void cargaArticulos() {

    	// instanciamos la clase y obtenemos la nómina
    	Stock Articulos = new Stock();
    	ResultSet nominaArticulos = Articulos.getNominaItems();

    	// agregamos el primer elemento
    	this.cDescripcion.addItem(new ComboClave(0, ""));

    	// recorremos el vector
    	try {

			while (nominaArticulos.next()) {

				// agregamos el elemento
				this.cDescripcion.addItem(new ComboClave(nominaArticulos.getInt("iditem"), nominaArticulos.getString("descripcion")));

			}

		// si hubo un error
		} catch (SQLException e) {
			e.printStackTrace();
		}

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga en el combo de usuario que recibe el
     * material la nómina de usuarios
     */
    protected void cargaUsuarios(){

        // instanciamos la clase de usuarios
        Usuarios Responsables = new Usuarios();

        // obtenemos la nómina de usuarios del laboratorio activo
        ResultSet Nomina = Responsables.nominaUsuariosLaboratorio();

        try {

            // agregamos el primer elemento usamos la clase comboclave
            // para almacenar tanto la id como el texto
            this.cRecibio.addItem(new ComboClave(0, ""));

            // nos desplazamos al inicio del resultset
            Nomina.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (Nomina.next()) {

                // agregamos el registro
                this.cRecibio.addItem(new ComboClave(Nomina.getInt("idusuario"), Nomina.getString("usuario")));

            }

            // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del remito a imprimir
     * Método que llama la impresión del remito del registro activo puede
     * ser llamado desde el botón del formulario o también al grabar un
     * nuevo registro
     */
    protected void imprimeRemito(int clave){

        // instanciamos la clase
        new RemitoEgreso(clave);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pular el botón grabar que verifica los
     * valores del formulario y envía los datos al servidor
     */
    protected void grabaRemito(){

    	// si està editando
    	if (!this.tId.getText().isEmpty()){
    		this.Salidas.setIdEgreso(Integer.parseInt(this.tId.getText()));
    	} else {
    		this.Salidas.setIdEgreso(0);
    	}

		// si no seleccionó artículo
		ComboClave articulo = (ComboClave) this.cDescripcion.getSelectedItem();
		if (articulo == null) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "Seleccione el artículo a pedir",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.cDescripcion.requestFocus();
            return;

        // verificamos que el id no sea 0
		} else if (articulo.getClave() == 0) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "Debe seleccionar un artículo",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.cDescripcion.requestFocus();
            return;

        // si seleccionó
		} else {

			// asigna en la clase
			this.Salidas.setIdItem(articulo.getClave());

		}

    	// verifica se halla indicado la cantidad
    	if (this.sCantidad.getValue().toString() == "0"){

            // presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this, "Indique la cantidad a entregar", "Error", JOptionPane.ERROR_MESSAGE);
			return;

        // si indicò
    	} else {

            // asigna en la clase
            this.Salidas.setCantidad(Integer.parseInt(this.sCantidad.getValue().toString()));

        }

    	// verifica se halla indicado a que usuario se entregò
        ComboClave usuario = (ComboClave) this.cRecibio.getSelectedItem();
        if (usuario == null){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this, "Indique quien recibe el artículo", "Error", JOptionPane.ERROR_MESSAGE);
			return;

        // si seleccionò
        } else {

            // asigna en la clase
            this.Salidas.setIdRecibio(usuario.getClave());

        }

        // verifica se halla seleccionado la fecha de entrega
        if(this.Herramientas.fechaJDate(this.dFechaAlta) == null){

            // presenta el mensaje
			JOptionPane.showMessageDialog(this, "Indique la fecha de entrega", "Error", JOptionPane.ERROR_MESSAGE);
			return;

        // si seleccionó
        } else {

            // asigna en la clase
            this.Salidas.setFechaEgreso(this.Herramientas.fechaJDate(this.dFechaAlta));

        }

        // setea el remito
        this.Salidas.setRemito(Integer.parseInt(this.tRemito.getText()));

        // si indicò los comentarios
        this.Salidas.setComentarios(this.tComentarios.getText());

        // graba el registro y obtiene la clave
        int clave = this.Salidas.grabaEgreso();

    	// pregunta si desea imprimir el remito
        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Desea imprimir el remito de salida?",
                                   "Entregas",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // imprimimos
            this.imprimeRemito(clave);

        }

        // recarga la grilla
        this.grillaEgresos();

        // limpia el formulario
        this.limpiaFormulario();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al pulsar el botón cancelar o al grabar un
     * registro que limpia el formulario de datos
     */
    protected void limpiaFormulario(){

        // inicializamos las variables y los campos
        this.tId.setText("");
        ComboClave egreso = new ComboClave(0, "");
        this.cDescripcion.setSelectedItem(egreso);
        this.tComentarios.setText("");
        this.sCantidad.setValue(0);
        this.tRemito.setText(Integer.toString(this.Salidas.getRemitoEgreso()));
        Calendar fechaActual = new GregorianCalendar();
        this.dFechaAlta.setCalendar(fechaActual);
        this.tEntrego.setText(Seguridad.Usuario);
        ComboClave elemento = new ComboClave(0,"");
        this.cRecibio.setSelectedItem(elemento);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que carga la grilla de egresos de artículos
     */
    protected void grillaEgresos(){

        // obtenemos la nómina de marcas
        ResultSet nomina = this.Salidas.getEgresos();

        // sobrecargamos el renderer de la tabla
        this.tEgresos.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)this.tEgresos.getModel();

    	// hacemos la tabla se pueda ordenar
		this.tEgresos.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[10];

        try {

            // iniciamos un bucle recorriendo el vector
            while (nomina.next()){

                // fijamos los valores de la fila
                fila[0] = nomina.getInt("id_egreso");
                fila[1] = nomina.getString("item");
                fila[2] = nomina.getString("remito");
                fila[3] = nomina.getInt("cantidad");
                fila[4] = nomina.getString("entrego");
                fila[5] = nomina.getString("recibio");
                fila[6] = nomina.getString("fecha");
                fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));
                fila[9] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro a eliminar
     * Método llamado al pulsar borrar sobre la grilla de
     * egresos, pide confirmación antes de eliminar el registro
     */
    protected void borraEgreso(int clave){

        // pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                                   "Está seguro que desea eliminar el Egreso?",
                                   "Entregas",
                                   JOptionPane.YES_NO_OPTION,
                                   JOptionPane.QUESTION_MESSAGE,
                                   null,
                                   null,
                                   null);

        // si confirmó
        if (respuesta == JOptionPane.YES_OPTION){

            // eliminamos el registro
            this.Salidas.borraEgreso(clave);

            // recargamos la grilla
            this.grillaEgresos();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param clave - clave del registro
     * Método que recibe como parámetro la clave de un egreso
     * y carga los datos del mismo en el formulario
     */
    protected void verEgreso(int clave){

        // obtenemos los datos del registro
        this.Salidas.getDatosEgreso(clave);

        // fijamos los valores en el formulario
        this.tId.setText(Integer.toString(this.Salidas.getIdEgreso()));
        ComboClave egreso = new ComboClave(this.Salidas.getIdItem(), this.Salidas.getItem());
        this.cDescripcion.setSelectedItem(egreso);
        this.sCantidad.setValue(this.Salidas.getCantidad());
        this.tEntrego.setText(this.Salidas.getEntrego());
        ComboClave recibio = new ComboClave(this.Salidas.getIdRecibio(), this.Salidas.getRecibio());
        this.cRecibio.setSelectedItem(recibio);
        this.tRemito.setText(Integer.toString(this.Salidas.getRemito()));
        this.dFechaAlta.setDate(this.Herramientas.StringToDate(this.Salidas.getFechaEgreso()));
        this.tComentarios.setText(this.Salidas.getComentarios());

        // fijamos el foco
        this.cDescripcion.requestFocus();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Evento llamado al pulsar sobre la grilla
     */
    protected void tEgresosMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tEgresos.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tEgresos.rowAtPoint(evt.getPoint());
        int columna = tEgresos.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tEgresos.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id del usuario
            int idegreso = (Integer) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            // si pulsó sobre editar
            if (columna == 7){
                this.verEgreso(idegreso);
            // si pulsó sobre imprimir
            } else if (columna == 8){
                this.imprimeRemito(idegreso);
            // si pulsó sobre eliminar
            } else if (columna == 9){
            	this.borraEgreso(idegreso);
            }

		}

    }

}
