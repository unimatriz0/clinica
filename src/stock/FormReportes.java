/*

    Nombre: FormReportes
    Fecha: 27/02/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que arma el formulario de selección de reporte

*/

// definición del paquete
package stock;

// inclusión de librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import com.toedter.calendar.JDateChooser;
import funciones.Utilidades;
import java.awt.Font;

// definición de la clase
public class FormReportes extends JDialog {

	// generamos el serial id
	private static final long serialVersionUID = -5371458076216428312L;

	// definición de variables
    private JDateChooser dFechaInicio;
    private JDateChooser dFechaFin;

	// constructor de la clase
	public FormReportes(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// fijamos las propiedades del formulario
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Reportes del Sistema");
		setBounds(150, 150, 545, 290);
		getContentPane().setLayout(null);

		// inicializamos el formulario
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com
	 * Método que inicializa el formulario
	 */
	protected void initForm(){

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// fijamos el título
		JLabel lTitulo = new JLabel("<html><b>Seleccione el tipo de reporte que desea generar, recuerde que algunos informes requieren la fecha de inicio y finalización</b></html>");
		lTitulo.setBounds(7, 12, 461, 39);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// pedimos la fecha de inicio
		JLabel lFechaInicio = new JLabel("Fecha de Inicio:");
		lFechaInicio.setBounds(10, 60, 120, 26);
		lFechaInicio.setFont(Fuente);
		getContentPane().add(lFechaInicio);
        this.dFechaInicio = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaInicio.setToolTipText("Fecha inicial del reporte");
		this.dFechaInicio.setBounds(125, 60, 110, 26);
		this.dFechaInicio.setFont(Fuente);
        getContentPane().add(this.dFechaInicio);

		// presenta la fecha de finalización
		JLabel lFechaFin = new JLabel("Fecha de Finalización:");
		lFechaFin.setBounds(245, 60, 165, 26);
		lFechaFin.setFont(Fuente);
		getContentPane().add(lFechaFin);
        this.dFechaFin = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
        this.dFechaFin.setToolTipText("Fecha final a reportar");
		this.dFechaFin.setBounds(410, 60, 110, 26);
		this.dFechaFin.setFont(Fuente);
        getContentPane().add(this.dFechaFin);

		// presenta el botón artículos
		JButton btnArticulos = new JButton("Artículos");
		btnArticulos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mquimica.png")));
		btnArticulos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				articulos();
			}
		});
		btnArticulos.setBounds(10, 100, 155, 30);
		btnArticulos.setFont(Fuente);
		getContentPane().add(btnArticulos);

		// presenta el botón inventario
		JButton btnInventario = new JButton("Inventario");
		btnInventario.setIcon(new ImageIcon(getClass().getResource("/Graficos/minventario.png")));
		btnInventario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				inventario();
			}
		});
		btnInventario.setBounds(190, 100, 155, 30);
		btnInventario.setFont(Fuente);
		getContentPane().add(btnInventario);

		// presenta el botón financiamiento
		JButton btnFinanciamiento = new JButton("Financiamiento");
		btnFinanciamiento.setIcon(new ImageIcon(getClass().getResource("/Graficos/mdolar.jpeg")));
		btnFinanciamiento.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				financiamiento();
			}
		});
		btnFinanciamiento.setBounds(366, 100, 155, 30);
		btnFinanciamiento.setFont(Fuente);
		getContentPane().add(btnFinanciamiento);

		// presenta el botón ingresos
		JButton btnIngresos = new JButton("Ingresos");
		btnIngresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mchecklist.png")));
		btnIngresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresos();
			}
		});
		btnIngresos.setBounds(10, 150, 155, 30);
		btnIngresos.setFont(Fuente);
		getContentPane().add(btnIngresos);

		// presenta el botón egresos
		JButton btnEgresos = new JButton("Egresos");
		btnEgresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
		btnEgresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				egresos();
			}
		});
		btnEgresos.setBounds(10, 200, 155, 30);
		btnEgresos.setFont(Fuente);
		getContentPane().add(btnEgresos);

		// presenta el conciliado ingresos
		JButton btnConciliadoIngresos = new JButton("Conciliado Ingresos");
		btnConciliadoIngresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/m81.png")));
		btnConciliadoIngresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				conciliadoIngresos();
			}
		});
		btnConciliadoIngresos.setBounds(190, 150, 180, 30);
		btnConciliadoIngresos.setFont(Fuente);
		getContentPane().add(btnConciliadoIngresos);

		// presenta el conciliado de egresos
		JButton btnConciliadoEgresos = new JButton("Conciliado Egresos");
		btnConciliadoEgresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/manalitica.png")));
		btnConciliadoEgresos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				conciliadoEgresos();
			}
		});
		btnConciliadoEgresos.setBounds(190, 200, 180, 30);
		btnConciliadoEgresos.setFont(Fuente);
		getContentPane().add(btnConciliadoEgresos);

	}

	// método que instancia el reporte de artículos
	protected void articulos(){

		// instanciamos la clase de items
		new XLSItems();

	}

	// método que instancia el reporte de inventario
	protected void inventario(){

        // instanciamos la clase
        new XLSInventario();

	}

	// método que instancia el reporte por fuente de financiamiento
	protected void financiamiento(){

	}

	// método que instancia el reporte de ingresos
	protected void ingresos(){

    	// instanciamos las herramientas y las variables
    	Utilidades Herramientas = new Utilidades();
    	String fechaInicio = "";
    	String fechaFin = "";

		// verificamos se halla seleccionado
		// la fecha inicial
    	if (Herramientas.fechaJDate(this.dFechaInicio) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de inicial del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaInicio.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaInicio = Herramientas.fechaJDate(this.dFechaInicio);

    	}

		// verificamos se halla indicado la
		// fecha final
    	if (Herramientas.fechaJDate(this.dFechaFin) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de final del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaFin.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaFin = Herramientas.fechaJDate(this.dFechaFin);

    	}

		// instanciamos el objeto y obtenemos el
		// resultado
    	Ingresos Entradas = new Ingresos();
    	ResultSet Conciliado = Entradas.getIngresos(fechaInicio, fechaFin);

		// instanciamos el reporte
    	new XLSIngresos(Conciliado);

	}

	// método que instancia el reporte de egresos
	protected void egresos(){

    	// instanciamos las herramientas y las variables
    	Utilidades Herramientas = new Utilidades();
    	String fechaInicio = "";
    	String fechaFin = "";

		// verificamos se halla seleccionado
		// la fecha inicial
    	if (Herramientas.fechaJDate(this.dFechaInicio) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de inicial del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaInicio.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaInicio = Herramientas.fechaJDate(this.dFechaInicio);

    	}

		// verificamos se halla indicado la
		// fecha final
    	if (Herramientas.fechaJDate(this.dFechaFin) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de final del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaFin.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaFin = Herramientas.fechaJDate(this.dFechaFin);

    	}

		// instanciamos el objeto y obtenemos el
		// resultado
    	Egresos Salidas = new Egresos();
    	ResultSet Conciliado = Salidas.getEgresos(fechaInicio, fechaFin);

		// instanciamos el reporte
    	new XLSEgresos(Conciliado);

	}

	// método que instancia el conciliado de ingresos
	protected void conciliadoIngresos(){

    	// instanciamos las herramientas y las variables
    	Utilidades Herramientas = new Utilidades();
    	String fechaInicio = "";
    	String fechaFin = "";

		// verificamos se halla seleccionado
		// la fecha inicial
    	if (Herramientas.fechaJDate(this.dFechaInicio) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de inicial del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaInicio.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaInicio = Herramientas.fechaJDate(this.dFechaInicio);

    	}

		// verificamos se halla indicado la
		// fecha final
    	if (Herramientas.fechaJDate(this.dFechaFin) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de final del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaFin.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaFin = Herramientas.fechaJDate(this.dFechaFin);

    	}

		// instanciamos el objeto y obtenemos el
		// resultado
    	Ingresos Entradas = new Ingresos();
    	ResultSet Conciliado = Entradas.conciliadoIngresos(fechaInicio, fechaFin);

		// instanciamos el reporte
    	new XLSConcIngresos(Conciliado);

	}

	// método que instancia el conciliado de egresos
	protected void conciliadoEgresos(){

    	// instanciamos las herramientas y las variables
    	Utilidades Herramientas = new Utilidades();
    	String fechaInicio = "";
    	String fechaFin = "";

		// verificamos se halla seleccionado
		// la fecha inicial
    	if (Herramientas.fechaJDate(this.dFechaInicio) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de inicial del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaInicio.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaInicio = Herramientas.fechaJDate(this.dFechaInicio);

    	}

		// verificamos se halla indicado la
		// fecha final
    	if (Herramientas.fechaJDate(this.dFechaFin) == null){

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		                      "Indique la fecha de final del reporte",
            		                      "Error",
            		                      JOptionPane.ERROR_MESSAGE);
            this.dFechaFin.requestFocus();
            return;

    	// si seleccionó
    	} else {

    		// lo pasamos a string y lo asignamos en la clase
    		fechaFin = Herramientas.fechaJDate(this.dFechaFin);

    	}

		// instanciamos el objeto y obtenemos el
		// resultado
    	Egresos Salidas = new Egresos();
    	ResultSet Conciliado = Salidas.conciliadoEgresos(fechaInicio, fechaFin);

		// instanciamos el reporte
    	new XLSConcEgresos(Conciliado);

	}

}
