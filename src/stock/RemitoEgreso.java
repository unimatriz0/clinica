/*
 * Nombre: remitoEgreso
 * Autor: Lic. Claudio Invernizzi
 * Fecha: 13/11/20108
 * Mail: cinvernizzi@gmail.com
 * Proyecto: Diagnóstico
 * Licencia: GPL
 * Comentarios: Clase que recibe como parámetro en el constructor el id
 *              del egreso y genera el remito en formato pdf
 */

// definición del paquete
package stock;

// inclusión de librerías
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import funciones.Imagenes;
import laboratorios.Laboratorios;
import seguridad.Seguridad;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;

// definición del paquete
public class RemitoEgreso {

    // declaración de variables
    protected String Descripcion;         // descripción del artículo
    protected String CodigoSop;           // clave sop del artículo
    protected java.awt.Image Foto;        // imagen almacenada en la base
    protected int IdEgreso;               // clave del egreso
    protected String Laboratorio;         // nombre del laboratorio
    protected java.awt.Image Logo;        // logo del laboratorio
    protected String Remito;              // número de remito
    protected int Cantidad;               // cantidad entregada
    protected String FechaEgreso;         // fecha de entrega
    protected String Recibio;             // nombre del usuario que recibió
    protected String Entrego;             // nombre del usuario que entregó
    protected String Lote;                // número de lote
    protected String Ubicacion;           // ubicación física del artículo
    protected String Comentarios;         // comentarios y observaciones

    // las variables del pdf
    protected Document documento;            // el documento pdf
    protected FileOutputStream ficheroPdf;   // el archivo físico
    protected PdfWriter docWriter;           // el puntero del documento
    protected PdfContentByte cb;             // puntero al contenido

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idegreso
     * Constructor del paquete
     */
    public RemitoEgreso(int idegreso){

        // obtenemos los datos del remito
        this.datosRemito(idegreso);

        // obtenemos el logo del laboratorio
        this.getLogo();
        
        // generamos el remito
        this.generarRemito();
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idegreso
     * Método protegido que recibe como parámetro la id del egreso
     * y asigna en las variables de clase los datos del remito
     */
    protected void datosRemito(int idegreso){

        // instanciamos la clase
        Egresos Deposito = new Egresos();
        Deposito.getDatosEgreso(idegreso);

        // asignamos en las variables de clase
        this.Cantidad = Deposito.getCantidad();
        this.Comentarios = Deposito.getComentarios();
        this.Descripcion = Deposito.getItem();
        this.CodigoSop = Deposito.getCodigoSop();
        this.Foto = Deposito.getFoto();
        this.FechaEgreso = Deposito.getFechaEgreso();
        this.IdEgreso = Deposito.getIdEgreso();
        this.Laboratorio = Deposito.getLaboratorio();
        this.Remito = Integer.toString(Deposito.getRemito());
        this.Recibio = Deposito.getRecibio();
        this.Entrego = Deposito.getEntrego();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir del usuario activo obtiene el logo
     * del laboratorio
     */
    protected void getLogo() {
    
        // obtenemos el logo del laboratorio
        Laboratorios Instituciones = new Laboratorios();
        Instituciones.getLogoLaboratorio();
        this.Logo = Instituciones.getLogo();
    	
    }

    /**
     * Método protegido que a partir de las variables de clase
     * genera el remito de ingreso
     */
    protected void generarRemito(){

        // creamos el documento
        this.documento = new Document(PageSize.A4);

        // obtenemos la ruta de ejecución y configuramos el path 
        // del archivo destino
        String archivo = (new File (".").getAbsolutePath ()) + "/temp/remito_egreso.pdf";
        
        // creamos el archivo
        try {
            this.docWriter = PdfWriter.getInstance(this.documento, new FileOutputStream(archivo));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RemitoEgreso.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(RemitoEgreso.class.getName()).log(Level.SEVERE, null, ex);
        }

        // establecemos las propiedades del documento
        this.documento.addAuthor("Lic. Claudio Invernizzi");
        this.documento.addCreationDate();
        this.documento.addCreator("http://fatalachaben.info.tm");

        // los márgenes van left - right - top - bottom en puntos
        // 72 puntos es 1 pulgada 2,5 cm (superior lo dejamos en
        // 10 porque el encabezado de página ocupa espacio
        this.documento.setMargins(90, 36, 10, 72);

        // abrimos el documento
        documento.open();

        // imprimimos el encabezado
        this.encabezado();

        // ahora agregamos los datos del remito
        try {
			this.imprimirCuerpo();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

        // cerramos el documento
        this.documento.close();

        // lo mostramos en el visor por defecto
        try {
            File path = new File (archivo);
            Desktop.getDesktop().open(path);
        }catch (IOException ex) {
            ex.printStackTrace();
        }
        
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @throws DocumentException
     * Método que imprime los datos del remito de ingreso
     */
    protected void imprimirCuerpo() throws DocumentException{

    	// presenta el número de remito
        Paragraph Parrafo = new Paragraph("Remito Nro: " + this.Remito,
                                          FontFactory.getFont("arial", 12));
        Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

    	// presenta la descripción del item
        Parrafo = new Paragraph("Descripción: " + this.Descripcion,
				                FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

    	// presenta el código sop
        Parrafo = new Paragraph("Código Sop: " + this.CodigoSop,
				                FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);
		
    	// presenta la cantidad entregada
        Parrafo = new Paragraph("Cantidad: " + Integer.toString(this.Cantidad),
     		                	FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

    	// presenta la fecha de egreso
        Parrafo = new Paragraph("Fecha Entrega: " + this.FechaEgreso,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

        // presenta el usuario que entregó
        Parrafo = new Paragraph("Entregó: " + this.Entrego,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

        // presenta el usuario que recibió
        Parrafo = new Paragraph("Recibió: " + this.Recibio,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

        // presenta los comentarios
        Parrafo = new Paragraph(this.Comentarios,
		                		FontFactory.getFont("arial", 12));
		Parrafo.setAlignment(Element.ALIGN_LEFT);
		this.documento.add(Parrafo);

		// agrega los codigos qr y de barras
		this.Codigos();
		
		// agrega la imagen del artículo
		this.agregaImagen();
		
    }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que define una tabla en la que presenta alineados
	 * el logo y los códigos de barras
	 */
	protected void encabezado(){

        try {

            // creamos el título
            Paragraph Parrafo = new Paragraph("Sistema de Control de Stock",
                                    FontFactory.getFont("arial", 14));
                                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
        	
            // agregamos el título
            Parrafo = new Paragraph("Entrega de Materiales del Depósito",
                                     FontFactory.getFont("arial", 14));
                                     Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
            
            // si el laboratorio no tiene logo
            if (this.Logo == null) {
            	
	            // obtenemos la imagen y la escalamos
	            Image logoFatala = Image.getInstance("Graficos/sin_imagen.jpg");
	            logoFatala.scaleToFit(92f, 67f);
	            logoFatala.setAlignment(Chunk.ALIGN_MIDDLE);
	            logoFatala.setAbsolutePosition(10f, 750f);
	            this.documento.add(logoFatala);

	        // si tiene logo
            } else {          	

            	// obtenemos la ruta temporal del logo y lo grabamos
                String archivo = (new File (".").getAbsolutePath ()) + "/temp/logo.jpg";
                Imagenes logo = new Imagenes();
                logo.guardarImagen(this.Logo, archivo, 92, 67);
                
				// agrega la imagen por defecto
	            Image archivologo = Image.getInstance(archivo);
	            archivologo.scaleToFit(92f, 67f);
	            archivologo.setAlignment(Chunk.ALIGN_MIDDLE);
	            archivologo.setAbsolutePosition(10f, 750f);
				this.documento.add(archivologo);
    				            	
            }
            
            // insertamos un separador
            Parrafo = new Paragraph(" ",
                    FontFactory.getFont("arial", 18));
                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
            
            // agregamos el separador
            Image Separador = Image.getInstance("Graficos/separador.png");
            Separador.setAlignment(Chunk.ALIGN_MIDDLE);
            Separador.scaleToFit(1000f, 5f);
            this.documento.add(Separador);
            
        } catch (BadElementException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que a partir de las variables de clase genera los 
	 * códigos de barras y qr y los agrega al documento
	 */
	protected void Codigos() {

		// instanciamos el puntero al documento
		PdfContentByte cb = this.docWriter.getDirectContent();
		Barcode39 barcode39 = new Barcode39();
		String clave = Integer.toString(this.IdEgreso) + "-" + Integer.toString(Seguridad.Laboratorio);

		// creamos el código y lo convertimos a imagen
		barcode39.setCode(clave);
		Image code39Image = barcode39.createImageWithBarcode(cb, null, null);

        code39Image.scaleToFit(92f, 67f);
        code39Image.setAlignment(Chunk.ALIGN_MIDDLE);
        code39Image.setAbsolutePosition(400f, 600f);
        
		// inicializamos las variables
		Image codeQrImage = null;
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(clave, 1000, 1000, null);

		// creamos la imagen
		try {
			codeQrImage = barcodeQRCode.getImage();
		} catch (BadElementException e) {
			e.printStackTrace();
		}

		// escalamos la imagen y la agregamos al documento
		codeQrImage.scaleAbsolute(100, 100);
		codeQrImage.setAbsolutePosition(400f, 500f);

		// agregamos las imágenes
        try {
			this.documento.add(code39Image);
			this.documento.add(codeQrImage);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	}
	
	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método protegido que agrega la imagen del artículo al 
	 * remito
	 */
	protected void agregaImagen() {

		// si no hay imagen
		if (this.Foto == null) {

            try {           
            	
				// agrega la imagen por defecto
	            Image articulo = Image.getInstance("Graficos/sin_imagen.jpg");
	            articulo.scaleToFit(92f, 92f);
	            articulo.setAlignment(Chunk.ALIGN_MIDDLE);
	            articulo.setAbsolutePosition(200f,450f);
				this.documento.add(articulo);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
 
        // si tiene imagen
		} else {

            try {            	

            	// obtenemos la ruta temporal de la imagen y la grabamos
                String archivo = (new File (".").getAbsolutePath ()) + "/temp/foto.jpg";
                Imagenes foto = new Imagenes();
                foto.guardarImagen(this.Foto, archivo, 100, 100);
                
				// agrega la imagen por defecto
	            Image articulo = Image.getInstance(archivo);
	            articulo.scaleToFit(100f, 100f);
	            articulo.setAlignment(Chunk.ALIGN_MIDDLE);
	            articulo.setAbsolutePosition(200f, 450f);
				this.documento.add(articulo);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
