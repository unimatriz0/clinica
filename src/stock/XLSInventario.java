/*

    Nombre: XLSInventario
    Fecha: 21/02/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que genera el reporte de inventario

 */

// definición del paquete
package stock;

// importamos las librerías
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Picture;
import java.sql.ResultSet;
import java.sql.SQLException;
import funciones.Imagenes;
import laboratorios.Laboratorios;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class XLSInventario {

    // declaración de variables
    protected Sheet Hoja;                   // puntero a la hoja
    protected Workbook Libro;               // puntero al libro
    protected int Contador;                 // contador de filas
    protected CellStyle negrita;            // el estilo de la fuente
    protected java.awt.Image Logo;          // logo del laboratorio

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Constructor de la clase, recibe como parámetros la fecha inicial
     * y final a reportar
     */
    public XLSInventario(){

        // obtenemos el logo del laboratorio
        Laboratorios Instituciones = new Laboratorios();
        Instituciones.getLogoLaboratorio();
        this.Logo = Instituciones.getLogo();
            	
    	// llamamos al método de impresión
    	this.imprimirReporte();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el constructor que pide el nombre
     * del archivo a grabar y luego lo genera
     */
    protected void imprimirReporte(){

    	Stock Articulos = new Stock();
        ResultSet Nomina = Articulos.getInventario();

        // obtenemos la ruta de ejecución y configuramos el path 
        // del archivo destino
        String rutaArchivo = (new File (".").getAbsolutePath ()) + "/temp/inventario.xls";
        
    	// inicializamos el contador de filas
    	this.Contador = 8;

        // abrimos el archivo
        File archivoXLS = new File(rutaArchivo);
        FileOutputStream archivo = null;

        // si existe el archivo lo elimina
        if(archivoXLS.exists()) archivoXLS.delete();

        // creamos el archivo
        try {
            archivoXLS.createNewFile();
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        // creamos el libro
		this.Libro = new HSSFWorkbook();

        // creamos el estilo en negrita
        this.negrita = this.Libro.createCellStyle();
        Font fuente = this.Libro.createFont();
        fuente.setFontHeightInPoints((short) 12);
        this.negrita.setFont(fuente);

        // abrimos la salida
        try {
            archivo = new FileOutputStream(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

        // creamos la hoja
        this.Hoja = this.Libro.createSheet("Inventario");

        // fijamos el ancho de las columnas
        this.Hoja.setColumnWidth(0, 16000);       // la descripción
        this.Hoja.setColumnWidth(1, 4500);        // el código sop
        this.Hoja.setColumnWidth(2, 2500);        // la cantidad crítica
        this.Hoja.setColumnWidth(3, 2500);        // la cantidad existente
        this.Hoja.setColumnWidth(4, 2700);        // el importe unitario
        this.Hoja.setColumnWidth(5, 2700);        // el valor calculado

        // insertamos el logo
        try {
			this.insertaLogo();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

        // imprimimos el encabazado
        this.imprimeEncabezado();

        // imprimimos los encabezados de columna
        this.imprimeTitulos();

        // recorremos el vector
        try {

            // nos aseguramos de estar en el primer registro
            Nomina.beforeFirst();

			while (Nomina.next()){

				// agregamos la fila
				Row fila = this.Hoja.createRow(this.Contador);

				// presenta los datos
				Cell celda = fila.createCell(0);
                celda.setCellValue(Nomina.getString("item"));
				celda = fila.createCell(1);
				celda.setCellValue(Nomina.getString("codigosop"));
				celda = fila.createCell(2);
				celda.setCellValue(Nomina.getFloat("critico"));
				celda = fila.createCell(3);
				celda.setCellValue(Nomina.getInt("existencia"));
				celda = fila.createCell(4);
				celda.setCellValue(Nomina.getFloat("importe"));
				celda = fila.createCell(5);
				celda.setCellValue(Nomina.getInt("existencia") * Nomina.getFloat("importe"));

				// incrementamos el contador
				this.Contador++;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

        // cerramos el archivo y lo mostramos
        // con la aplicación predeterminada
        try {
            this.Libro.write(archivo);
            archivo.close();
            Desktop.getDesktop().open(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime el encabezado de la hoja de cálculo
     */
    protected void imprimeEncabezado(){

        // agregamos la fila del título y le aplicamos el estilo
        Row fila = this.Hoja.createRow(2);
        Cell celda = fila.createCell(2);
        celda.setCellValue("Sistema de Control de Inventario");
        celda.setCellStyle(this.negrita);

        // agregamos la fila de la descripción
        fila = this.Hoja.createRow(4);
        celda = fila.createCell(2);
        celda.setCellValue("Inventario en Depósito");
        celda.setCellStyle(this.negrita);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime los títulos de las columnas
     */
    protected void imprimeTitulos(){

        // agregamos la fila
        Row fila = this.Hoja.createRow(this.Contador);

        // incrementamos el contador
        this.Contador++;

        // agregamos la fila
        fila = this.Hoja.createRow(this.Contador);

        // presenta los encabezados
        Cell celda = fila.createCell(0);
        celda.setCellValue("Descripcion");
        celda = fila.createCell(1);
        celda.setCellValue("Sop");
        celda = fila.createCell(2);
        celda.setCellValue("Crítico");
        celda = fila.createCell(3);
        celda.setCellValue("Existente");
        celda = fila.createCell(4);
        celda.setCellValue("Importe");
        celda = fila.createCell(5);
        celda.setCellValue("Total");

        // incrementamos el contador
        this.Contador++;

    }

    protected void insertaLogo() throws IOException{

    	// declaración de variables
    	String archivo = "";
    	
        // si el laboratorio no tiene logo
        if (this.Logo == null) {
        	
            // creamos el path
            archivo = "Graficos/sin_imagen.jpg";

        // si tiene logo
        } else {          	

        	// obtenemos la ruta temporal del logo y lo grabamos
            archivo = (new File (".").getAbsolutePath ()) + "/temp/logo.jpg";
            Imagenes logo = new Imagenes();
            logo.guardarImagen(this.Logo, archivo, 92, 67);
            				            	
        }

        // leemos el archivo
        FileInputStream stream = new FileInputStream(archivo);

        // creamos el objeto en el libro
        CreationHelper helper = this.Libro.getCreationHelper();
        @SuppressWarnings("static-access")
		int pictureIndex = this.Libro.addPicture(IOUtils.toByteArray(stream),this.Libro.PICTURE_TYPE_PNG);
        Drawing<?> drawing = this.Hoja.createDrawingPatriarch();

        // posicionamos el contenddor de la imagen
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0);
        anchor.setRow1(0);
        anchor.setRow2(16);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

        // agregamos la imagen y la redimensionamos
        Picture pict = drawing.createPicture(anchor, pictureIndex);
        pict.resize(0.5);

    }

}
