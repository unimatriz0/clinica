/*

    Nombre: etiquetas
    Fecha: 18/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Método que genera la etiqueda del item

 */

// definición del paquete
package stock;

//importamos las librerías
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.Barcode39;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import funciones.Imagenes;
import java.awt.Desktop;
import java.io.*;
import seguridad.Seguridad;
import laboratorios.Laboratorios;

// definición de la clase
public class Etiquetas {

	// declaración de variables de clase
	protected int IdItem;                // clave del registro
	protected String Descripcion;        // descripcion del item
	protected String CodigoSop;          // código sop del elemento
    protected int Critico;               // número crítico del stock
    protected float Costo;               // costo unitario del artículo
    protected java.awt.Image Foto;       // imagen almacenada en la base
    protected String FechaAlta;          // fecha de alta del item
    protected String Usuario;            // nombre del usuario
    protected java.awt.Image Logo;       // logo del laboratorio
	protected Document documento;        // el documento pdf
    protected PdfWriter docWriter;       // el puntero del documento
    protected PdfContentByte cb;         // puntero al contenido

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param iditem
	 * Constructor de la clase, recibe como parámetro la
	 * clave de la heladera a imprimir
	 */
	public Etiquetas (int iditem){

		// instanciamos la clase y obtenemos los datos del registro
        Stock Item = new Stock();
        Item.getDatosItem(iditem);

        // obtenemos el logo del laboratorio
        Laboratorios Instituciones = new Laboratorios();
        Instituciones.getLogoLaboratorio();
        this.Logo = Instituciones.getLogo();
        
        // asignamos en las variables de clase
        this.IdItem = Item.getIdItem();
        this.Descripcion = Item.getItem();
        this.CodigoSop = Item.getCodigoSop();
        this.Critico = Item.getCritico();
        this.Costo = Item.getValor();
        this.Foto = Item.getImagen();
        this.FechaAlta = Item.getFechaAlta();
        this.Usuario = Item.getUsuario();

        // creamos el documento
        this.documento = new Document(PageSize.A4);

        // obtenemos la ruta de ejecución y configuramos el path 
        // del archivo destino
        String archivo = (new File (".").getAbsolutePath ()) + "/temp/etiqueta.pdf";
        
        // creamos el archivo
        try {
            this.docWriter = PdfWriter.getInstance(this.documento, new FileOutputStream(archivo));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        }

        // establecemos las propiedades del documento
        this.documento.addAuthor("Lic. Claudio Invernizzi");
        this.documento.addCreationDate();
        this.documento.addCreator("http://fatalachaben.info.tm");

        // los márgenes van left - right - top - bottom en puntos
        // 72 puntos es 1 pulgada 2,5 cm (superior lo dejamos en
        // 10 porque el encabezado de página ocupa espacio
        this.documento.setMargins(90, 36, 10, 72);

        // abrimos el documento
        this.documento.open();

        // imprimimos el registro
        try {
			this.imprimeFicha();
		} catch (DocumentException e) {
			e.printStackTrace();
		}

        // Cerramos el documento
        this.documento.close();

        // lo mostramos en el visor por defecto
        try {
            File path = new File (archivo);
            Desktop.getDesktop().open(path);
        }catch (IOException ex) {
            ex.printStackTrace();
        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @throws DocumentException
	 * Método que imprime los datos de la ficha del item
	 */
	protected void imprimeFicha() throws DocumentException{

		// presenta el encabezado
		this.encabezado();

        // bajamos el puntero
        Paragraph Parrafo = new Paragraph(" ",
                                FontFactory.getFont("arial", 18));
                                Parrafo.setAlignment(Element.ALIGN_CENTER);
        this.documento.add(Parrafo);
    			
        // la clave del artículo
        Parrafo = new Paragraph("ID: " + Integer.toString(this.IdItem),
                      FontFactory.getFont("arial", 12));
                      Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la descripción
        Parrafo = new Paragraph("Descripcion: " + this.Descripcion,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // el valor
        Parrafo = new Paragraph("Costo Unitario: " + Float.toString(this.Costo),
                      FontFactory.getFont("arial", 12));
                      Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);
        
        // el código sop
        Parrafo = new Paragraph("CodigoSop: " + this.CodigoSop,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // la cantidad crítica
        Parrafo = new Paragraph("Critico: " + Integer.toString(this.Critico),
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // el usuario y la fecha de alta
        Parrafo = new Paragraph("Usuario: " + this.Usuario,
                  FontFactory.getFont("arial", 12));
                  Parrafo.setAlignment(Element.ALIGN_LEFT);
        this.documento.add(Parrafo);

        // agregamos los códigos 
        this.codigos();
        
        // imprimimos la imagen
        this.agregaImagen();
        
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que define una tabla en la que presenta alineados
	 * el logo y los códigos de barras
	 */
	protected void encabezado(){

        try {

            // creamos el título
            Paragraph Parrafo = new Paragraph("Sistema de Control de Stock",
                                    FontFactory.getFont("arial", 14));
                                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
        	
            // agregamos el título
            Parrafo = new Paragraph("Registro de Artículos",
                                     FontFactory.getFont("arial", 14));
                                     Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
            
            // si el laboratorio no tiene logo
            if (this.Logo == null) {
            	
	            // obtenemos la imagen y la escalamos
	            Image logoFatala = Image.getInstance("Graficos/sin_imagen.jpg");
	            logoFatala.scaleToFit(92f, 67f);
	            logoFatala.setAlignment(Chunk.ALIGN_MIDDLE);
	            logoFatala.setAbsolutePosition(10f, 750f);
	            this.documento.add(logoFatala);

	        // si tiene logo
            } else {          	

            	// obtenemos la ruta temporal del logo y lo grabamos
                String archivo = (new File (".").getAbsolutePath ()) + "/temp/logo.jpg";
                Imagenes logo = new Imagenes();
                logo.guardarImagen(this.Logo, archivo, 92, 67);
                
				// agrega la imagen por defecto
	            Image archivologo = Image.getInstance(archivo);
	            archivologo.scaleToFit(92f, 67f);
	            archivologo.setAlignment(Chunk.ALIGN_MIDDLE);
	            archivologo.setAbsolutePosition(10f, 750f);
				this.documento.add(archivologo);
    				            	
            }
            
            // insertamos un separador
            Parrafo = new Paragraph(" ",
                    FontFactory.getFont("arial", 18));
                    Parrafo.setAlignment(Element.ALIGN_CENTER);
            this.documento.add(Parrafo);
            
            // agregamos el separador
            Image Separador = Image.getInstance("Graficos/separador.png");
            Separador.setAlignment(Chunk.ALIGN_MIDDLE);
            Separador.scaleToFit(1000f, 5f);
            this.documento.add(Separador);
            
        } catch (BadElementException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Etiquetas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que genera los códigos qr y de barras y los 
	 * presenta en el documento
	 */
	protected void codigos() {

		// instanciamos el puntero al documento
		PdfContentByte cb = this.docWriter.getDirectContent();
		Barcode39 barcode39 = new Barcode39();
		String clave = Integer.toString(this.IdItem) + "-" + Integer.toString(Seguridad.Laboratorio);

		// creamos el código y lo convertimos a imagen
		barcode39.setCode(clave);
		Image code39Image = barcode39.createImageWithBarcode(cb, null, null);

        code39Image.scaleToFit(92f, 67f);
        code39Image.setAlignment(Chunk.ALIGN_MIDDLE);
        code39Image.setAbsolutePosition(400f, 600f);
        
		// inicializamos las variables
		Image codeQrImage = null;
		BarcodeQRCode barcodeQRCode = new BarcodeQRCode(clave, 1000, 1000, null);

		// creamos la imagen
		try {
			codeQrImage = barcodeQRCode.getImage();
		} catch (BadElementException e) {
			e.printStackTrace();
		}

		// escalamos la imagen y la agregamos al documento
		codeQrImage.scaleAbsolute(100, 100);
		codeQrImage.setAbsolutePosition(400f, 500f);

		// agregamos las imágenes
        try {
			this.documento.add(code39Image);
			this.documento.add(codeQrImage);
		} catch (DocumentException e1) {
			e1.printStackTrace();
		}
		
	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que agrega la imagen al documento
	 */
	protected void agregaImagen() {
	
		// si no hay imagen
		if (this.Foto == null) {

            try {            	
				// agrega la imagen por defecto
	            Image articulo = Image.getInstance("Graficos/sin_imagen.jpg");
	            articulo.scaleToFit(92f, 92f);
	            articulo.setAlignment(Chunk.ALIGN_MIDDLE);
	            articulo.setAbsolutePosition(200f, 500f);
				this.documento.add(articulo);				
			} catch (DocumentException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
 
        // si tiene imagen
		} else {

            try {            	

            	// obtenemos la ruta temporal de la imagen y la grabamos
                String archivo = (new File (".").getAbsolutePath ()) + "/temp/foto.jpg";
                Imagenes foto = new Imagenes();
                foto.guardarImagen(this.Foto, archivo, 100, 100);
                
				// agrega la imagen por defecto
	            Image articulo = Image.getInstance(archivo);
	            articulo.scaleToFit(100f, 100f);
	            articulo.setAlignment(Chunk.ALIGN_MIDDLE);
	            articulo.setAbsolutePosition(200f, 500f);
				this.documento.add(articulo);
				
			} catch (DocumentException e) {
				e.printStackTrace();
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
}
