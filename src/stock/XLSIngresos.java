/*

    Nombre: XLSIngresos
    Fecha: 13/11/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que genera el reporte de ingresos al depósito

 */

// definición del paquete
package stock;

// importamos las librerías
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Picture;
import java.sql.ResultSet;
import java.sql.SQLException;
import funciones.Imagenes;
import laboratorios.Laboratorios;

/**
 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
 * Definición de la clase
 */
public class XLSIngresos {

    // declaración de variables
	protected Workbook Libro;               // puntero al libro
    protected Sheet Hoja;                   // puntero a la hoja
    protected int Contador;                 // contador de filas
    protected CellStyle negrita;            // el estilo de la fuente
    protected java.awt.Image Logo;          // logo del laboratorio

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param nomina - resultset con los datos a imprimir
     * Constructor de la clase, recibe como parámetros el resultset
     */
    public XLSIngresos(ResultSet nomina){

        // obtenemos el logo del laboratorio
        Laboratorios Instituciones = new Laboratorios();
        Instituciones.getLogoLaboratorio();
        this.Logo = Instituciones.getLogo();
    	
    	// llamamos al método de impresión
    	this.imprimirReporte(nomina);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado desde el constructor que pide el nombre
     * del archivo a grabar y luego lo genera
     */
    protected void imprimirReporte(ResultSet nomina){

        // obtenemos la ruta de ejecución y configuramos el path 
        // del archivo destino
        String rutaArchivo = (new File (".").getAbsolutePath ()) + "/temp/ingresos.xls";

    	// inicializamos el contador de filas
    	this.Contador = 8;

        // abrimos el archivo
        File archivoXLS = new File(rutaArchivo);
        FileOutputStream archivo = null;

        // si existe el archivo lo elimina
        if(archivoXLS.exists()) archivoXLS.delete();

        // creamos el archivo
        try {
            archivoXLS.createNewFile();
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        // creamos el libro
		this.Libro = new HSSFWorkbook();

        // creamos el estilo en negrita
        this.negrita = this.Libro.createCellStyle();
        Font fuente = this.Libro.createFont();
        fuente.setFontHeightInPoints((short) 12);
        this.negrita.setFont(fuente);

        // abrimos la salida
        try {
            archivo = new FileOutputStream(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

        // creamos la hoja
        this.Hoja = this.Libro.createSheet("Egresos");

        // fijamos el ancho de las columnas
        this.Hoja.setColumnWidth(0, 14000);
        this.Hoja.setColumnWidth(1, 2500);
        this.Hoja.setColumnWidth(2, 2500);
        this.Hoja.setColumnWidth(3, 2700);
        this.Hoja.setColumnWidth(4, 8000);
        this.Hoja.setColumnWidth(5, 2500);
        this.Hoja.setColumnWidth(6, 3000);
        this.Hoja.setColumnWidth(7, 2700);
        this.Hoja.setColumnWidth(8, 3000);

        // insertamos el logo
        try {
			this.insertaLogo();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

        // imprime el encabezado del reporte
        this.imprimeEncabezado();

        // imprimimos los encabezados de columna
        this.imprimeTitulos();

        // recorremos el vector
        try {

            // nos aseguramos de estar en el primer registro
            nomina.beforeFirst();

			while (nomina.next()){

				// agregamos la fila
				Row fila = this.Hoja.createRow(this.Contador);

				// presenta los datos
				Cell celda = fila.createCell(0);
                celda.setCellValue(nomina.getString("item"));
				celda = fila.createCell(1);
				celda.setCellValue(nomina.getString("codigosop"));
				celda = fila.createCell(2);
				celda.setCellValue(nomina.getFloat("importe"));
				celda = fila.createCell(3);
				celda.setCellValue(nomina.getString("remito"));
				celda = fila.createCell(4);
                celda.setCellValue(nomina.getString("financiamiento"));
                celda = fila.createCell(5);
                celda.setCellValue(nomina.getInt("cantidad"));
                celda = fila.createCell(6);
                celda.setCellValue(nomina.getString("lote"));
                celda = fila.createCell(7);
                celda.setCellValue(nomina.getString("vencimiento"));
                celda = fila.createCell(8);
                celda.setCellValue(nomina.getString("usuario"));

				// incrementamos el contador
				this.Contador++;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

        // cerramos el archivo y lo mostramos
        // con la aplicación predeterminada
        try {
            this.Libro.write(archivo);
            archivo.close();
            Desktop.getDesktop().open(archivoXLS);
        } catch (IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime el encabezado de la hoja de cálculo
     */
    protected void imprimeEncabezado(){

        // agregamos la fila del título y le aplicamos el estilo
        Row fila = this.Hoja.createRow(2);
        Cell celda = fila.createCell(2);
        celda.setCellValue("Sistema de Control de Inventario");
        celda.setCellStyle(this.negrita);

        // agregamos la fila de la descripción
        fila = this.Hoja.createRow(4);
        celda = fila.createCell(2);
        celda.setCellValue("Ingresos de Materiales al Depósito");
        celda.setCellStyle(this.negrita);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que imprime los títulos de las columnas
     */
    protected void imprimeTitulos(){

        // agregamos la fila
        Row fila = this.Hoja.createRow(this.Contador);

        // incrementamos el contador
        this.Contador++;

        // agregamos la fila
        fila = this.Hoja.createRow(this.Contador);

        // presenta los encabezados
        Cell celda = fila.createCell(0);
        celda.setCellValue("Descripcion");
        celda = fila.createCell(1);
        celda.setCellValue("Sop");
        celda = fila.createCell(2);
        celda.setCellValue("Imp.");
        celda = fila.createCell(3);
        celda.setCellValue("Remito");
        celda = fila.createCell(4);
        celda.setCellValue("Financ.");
        celda = fila.createCell(5);
        celda.setCellValue("Cant.");
        celda = fila.createCell(6);
        celda.setCellValue("Lote");
        celda = fila.createCell(7);
        celda.setCellValue("Venc.");
        celda = fila.createCell(8);
        celda.setCellValue("Usuario");

        // incrementamos el contador
        this.Contador++;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que presenta el logo del laboratorio
     */
    @SuppressWarnings("static-access")
	protected void insertaLogo() throws IOException{

    	// declaración de variables
    	String archivo = "";
    	
        // si el laboratorio no tiene logo
        if (this.Logo == null) {
        	
            // creamos el path
            archivo = "Graficos/sin_imagen.jpg";

        // si tiene logo
        } else {          	

        	// obtenemos la ruta temporal del logo y lo grabamos
            archivo = (new File (".").getAbsolutePath ()) + "/temp/logo.jpg";
            Imagenes logo = new Imagenes();
            logo.guardarImagen(this.Logo, archivo, 92, 67);
            				            	
        }

        // leemos el archivo
        FileInputStream stream = new FileInputStream(archivo);

        // creamos el objeto en el libro
        CreationHelper helper = this.Libro.getCreationHelper();
        @SuppressWarnings("static-access")
		int pictureIndex = this.Libro.addPicture(IOUtils.toByteArray(stream),this.Libro.PICTURE_TYPE_PNG);
        Drawing<?> drawing = this.Hoja.createDrawingPatriarch();

        // posicionamos el contenddor de la imagen
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(0);
        anchor.setRow1(0);
        anchor.setRow2(16);
        anchor.setAnchorType(ClientAnchor.AnchorType.MOVE_AND_RESIZE);

        // agregamos la imagen y la redimensionamos
        Picture pict = drawing.createPicture(anchor, pictureIndex);
        pict.resize(0.5);

    }

}
