/*

    Nombre: FormSTock
    Fecha: 24/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: stock
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que presenta el formulario con los botones
                 de acción del inventario

 */

// definición del paquete
package stock;

// importamos las librerías
import javax.swing.JPanel;
import contenedor.FormContenedor;
import javax.swing.JOptionPane;
import seguridad.Seguridad;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;

// definición de la clase
public class FormStock extends JPanel {

	// definimos el serial id del formulario
	private static final long serialVersionUID = -8046739924923800912L;

	// definición de variables de clase
	private FormContenedor Contenedor;          // frame principal

	// constructor de la clase
	public FormStock(FormContenedor contenedor) {

		// fijamos el layour
		this.setLayout(null);

        // fijamos el contenedor
        this.Contenedor = contenedor;

		// llamamos la rutina de inicialización
		this.initForm();

	}

	// método que inicializa los componentes del formulario
	protected void initForm() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Sistema de Control de Stock</b></html>");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(171, 10, 231, 15);
		this.add(lTitulo);

		// presenta el encabezado
		JLabel lEncabezado = new JLabel("Pulse sobre la acción que desea realizar");
		lEncabezado.setBounds(34, 45, 439, 28);
		lEncabezado.setFont(Fuente);
		this.add(lEncabezado);

		// el botón del diccionario de artículos
		JButton btnArticulos = new JButton("Artículos");
		btnArticulos.setToolTipText("Altas y Bajas del diccionario de elementos");
		btnArticulos.setBounds(365, 90, 130, 30);
		btnArticulos.setFont(Fuente);
		btnArticulos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mquimica.png")));
        btnArticulos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                articulos();
            }
        });
		this.add(btnArticulos);

		// el botón de pedidos
		JButton btnPedidos = new JButton("Pedidos");
		btnPedidos.setToolTipText("Realizar pedidos de mercadería o autorizarlos");
		btnPedidos.setBounds(30, 145, 130, 30);
		btnPedidos.setFont(Fuente);
		btnPedidos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mmarcador.png")));
        btnPedidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pedidos();
            }
        });
		this.add(btnPedidos);

		// el botón de inventario
		JButton btnInventario = new JButton("Inventario");
		btnInventario.setToolTipText("Ver existencias en depósito");
		btnInventario.setBounds(200, 145, 130, 30);
		btnInventario.setFont(Fuente);
		btnInventario.setIcon(new ImageIcon(getClass().getResource("/Graficos/minventario.png")));
        btnInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inventario();
            }
        });
		this.add(btnInventario);

		// si el usuario está autorizado
		if (Seguridad.Stock.equals("Si")) {

			// agrega el botón ingresar mercadería
			JButton btnIngresos = new JButton("Ingresos");
			btnIngresos.setToolTipText("Ingresar mercadería al depósito");
			btnIngresos.setBounds(30, 200, 130, 30);
			btnIngresos.setFont(Fuente);
			btnIngresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/mchecklist.png")));
	        btnIngresos.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ingresos();
	            }
	        });
			this.add(btnIngresos);

			// el botón entregar mercadería
			JButton btnEgresos = new JButton("Egresos");
			btnEgresos.setToolTipText("Entregar mercadería");
			btnEgresos.setBounds(200, 200, 130, 30);
			btnEgresos.setFont(Fuente);
			btnEgresos.setIcon(new ImageIcon(getClass().getResource("/Graficos/msalida.png")));
	        btnEgresos.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                egresos();
	            }
	        });
			this.add(btnEgresos);

			// agrega el botón de reportes
			JButton btnReportes = new JButton("Reportes");
			btnReportes.setToolTipText("Reportes del Sistema");
			btnReportes.setBounds(200, 255, 130, 30);
			btnReportes.setFont(Fuente);
			btnReportes.setIcon(new ImageIcon(getClass().getResource("/Graficos/mbase-de-datos.png")));
	        btnReportes.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                reportes();
	            }
	        });
			this.add(btnReportes);

		}

        // verifica el stock crítico
        this.verificaCritico();

	}

	// método llamado al pulsar el botón artículos
	protected void articulos() {
        FormGrillaItems Items = new FormGrillaItems(this.Contenedor, true);
        Items.setVisible(true);
	}

	// método llamado al pulsar el botón pedidos
	protected void pedidos() {
		FormPedidos Pedidos = new FormPedidos(this.Contenedor, true);
		Pedidos.setVisible(true);
	}

	// método llamado al pulsar el botón inventario
	protected void inventario() {
		FormGrillaInventario Inventario = new FormGrillaInventario(this.Contenedor, true);
		Inventario.setVisible(true);
	}

	// método llamado al pulsar el botón ingresos
	protected void ingresos() {
		FormGrillaIngresos Ingresos = new FormGrillaIngresos(this.Contenedor, true);
		Ingresos.setVisible(true);
	}

	// método llamado al pulsar el botón egresos
	protected void egresos() {
		FormGrillaEgresos Egresos = new FormGrillaEgresos(this.Contenedor, true);
		Egresos.setVisible(true);
	}

	// método llamado al pulsar el botón reportes
	protected void reportes(){

		// instanciamos el formulario y lo mostramos
		FormReportes Informes = new FormReportes(this.Contenedor, true);
		Informes.setVisible(true);

	}

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método llamado al iniciar el formulario que verifica
     * si existe stock crítico y presenta el alerta
     */
    protected void verificaCritico(){

        // si el usuario está autorizado
        if (Seguridad.Stock.equals("Si")){

			// instanciamos la clase y obtenemos el crítico
			Stock Deposito = new Stock();
			int Critico = Deposito.verificaCritico();

			// si hay elementos
			if (Critico != 0){

				// presenta el mensaje
				String mensaje = "Existen artículos por debajo del\n";
				mensaje += "valor crítico. Verifique por favor";
				JOptionPane.showMessageDialog(this,
											  mensaje,
											  "Atención", JOptionPane.INFORMATION_MESSAGE);

			}

        }

    }

}
