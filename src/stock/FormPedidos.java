/*

    Nombre: FormPedidos
    Fecha: 24/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que arma el formulario para la solicitud y
                 aprobación de materiales
 */

// definición del paquete
package stock;

// importamos las librerías
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.JComboBox;
import funciones.ComboClave;
import funciones.Imagenes;
import funciones.RendererTabla;
import funciones.Utilidades;
import funciones.Mensaje;
import javax.swing.ImageIcon;
import seguridad.Seguridad;
import java.awt.Font;

// definición de la clase
public class FormPedidos extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -6899726810596899434L;

	// definición de las variables de clase
	private JTextField tId;
	private JComboBox<Object> tDescripcion;
	private JTextField tSop;
	private JSpinner jSolicitado;
	private JTextField tDisponible;
	private JTextField tUsuario;
	private JTextField tFechaAlta;
	private JLabel lImagen;
	private JTable tPedidos;
	private JScrollPane Scroll;
	private Pedidos Solicitudes;             // clase de la base de datos

	// constructor de la clase
	public FormPedidos(java.awt.Frame parent, boolean modal) {

        // setea el padre e inicia los componentes
        super(parent, modal);

		// fijamos las propiedades del formulario
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.setTitle("Pedidos de Materiales");
		setBounds(100, 100, 724, 600);
		getContentPane().setLayout(null);

		// inicializamos el formulario
		this.initForm();

	}

	// inicializamos los componentes del formulario
	protected void initForm() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// instanciamos la clase de pedidos
		Solicitudes = new Pedidos();

		// presenta el título
		JLabel lTitulo = new JLabel("Solicitudes de Materiales");
		lTitulo.setBounds(10, 10, 369, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// presenta la id del registro
		JLabel lId = new JLabel("ID:");
		lId.setBounds(10, 45, 35, 26);
		lId.setFont(Fuente);
		getContentPane().add(lId);
		this.tId = new JTextField();
		this.tId.setToolTipText("Clave del pedido");
		this.tId.setBounds(35, 45, 58, 26);
		this.tId.setFont(Fuente);
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// presenta la descripción del item
		this.tDescripcion = new JComboBox<>();
		this.tDescripcion.setBounds(110, 45, 420, 26);
		this.tDescripcion.setFont(Fuente);
		this.tDescripcion.setEditable(true);
		this.tDescripcion.setToolTipText("Artículo a solicitar");
		getContentPane().add(this.tDescripcion);

		// cargamos la nómina de artículos
		this.cargaArticulos();

		// fijamos el listener para cuando cambia la selección
		this.tDescripcion.addItemListener(new java.awt.event.ItemListener(){
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                seleccionaArticulo();
            }
        });

		// presenta la clave sop
		JLabel lSop = new JLabel("SOP:");
		lSop.setBounds(10, 80, 40, 26);
		lSop.setFont(Fuente);
		getContentPane().add(lSop);
		this.tSop = new JTextField();
		this.tSop.setBounds(50, 80, 82, 26);
		this.tSop.setFont(Fuente);
		this.tSop.setToolTipText("Clave SOP del Artículo");
		this.tSop.setEditable(false);
		getContentPane().add(this.tSop);

		// el spin con la cantidad solicitada
		JLabel lSolicitado = new JLabel("Solicitado:");
		lSolicitado.setBounds(145, 80, 87, 26);
		lSolicitado.setFont(Fuente);
		getContentPane().add(lSolicitado);
		this.jSolicitado = new JSpinner();
		this.jSolicitado.setBounds(227, 80, 50, 26);
		this.jSolicitado.setFont(Fuente);
		this.jSolicitado.setToolTipText("Cantidad de artículos");
		getContentPane().add(this.jSolicitado);

		// el texto de la cantidad disponible
		JLabel lDisponible = new JLabel("Disponible:");
		lDisponible.setBounds(290, 80, 93, 26);
		lDisponible.setFont(Fuente);
		getContentPane().add(lDisponible);
		this.tDisponible = new JTextField();
		this.tDisponible.setBounds(375, 80, 50, 26);
		this.tDisponible.setFont(Fuente);
		this.tDisponible.setToolTipText("N° de artículos en depósito");
		this.tDisponible.setEditable(false);
		getContentPane().add(this.tDisponible);

		// presenta el texto con el usuario
		JLabel lUsuario = new JLabel("Usuario:");
		lUsuario.setBounds(10, 115, 70, 26);
		lUsuario.setFont(Fuente);
		getContentPane().add(lUsuario);
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(75, 115, 75, 26);
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setToolTipText("Usuario que realizó la solicitud");
		this.tUsuario.setEditable(false);
		getContentPane().add(this.tUsuario);

		// presenta la fecha de alta
		JLabel lFechaAlta = new JLabel("Fecha Alta:");
		lFechaAlta.setBounds(160, 115, 87, 26);
		lFechaAlta.setFont(Fuente);
		getContentPane().add(lFechaAlta);
		this.tFechaAlta = new JTextField();
		this.tFechaAlta.setBounds(250, 115, 75, 26);
		this.tFechaAlta.setFont(Fuente);
		this.tFechaAlta.setToolTipText("Fecha de la solicitud");
		this.tFechaAlta.setEditable(false);
		getContentPane().add(this.tFechaAlta);

		// presenta la etiqueta con la imagen
		this.lImagen = new JLabel();
		this.lImagen.setBounds(600, 10, 120, 120);
		this.lImagen.setToolTipText("Imagen del Artículo");
        this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
		getContentPane().add(this.lImagen);

		// presenta el botón grabar
		JButton btnSolicitar = new JButton("Solicitar");
		btnSolicitar.setBounds(349, 115, 110, 26);
		btnSolicitar.setFont(Fuente);
		btnSolicitar.setToolTipText("Graba el pedido en la base");
		btnSolicitar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
        btnSolicitar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaPedido();
            }
        });
		getContentPane().add(btnSolicitar);

		// presenta el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(475, 115, 110, 26);
		btnCancelar.setFont(Fuente);
		btnCancelar.setToolTipText("Reinicia el formulario");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaPedido();
            }
        });
		getContentPane().add(btnCancelar);

		// fijamos la fecha de alta y el usuario
		Utilidades Herramientas = new Utilidades();
		this.tUsuario.setText(Seguridad.Usuario);
		this.tFechaAlta.setText(Herramientas.FechaActual());

		// si puede editar stock
		if (Seguridad.Stock.equals("Si")) {

			// agregamos la tabla con todos los pedidos
			this.stockAdmin();

		// si es un usuario
		} else {

			// agregamos los pedidos propios
			this.stockUsuario();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga en el combo la nómina de artículos
	 */
	protected void cargaArticulos() {

		// instanciamos y obtenemos la nómina de elementos
		Stock Articulos = new Stock();
		ResultSet nominaArticulos = Articulos.getNominaItems();

		// agregamos el primer elemento
		this.tDescripcion.addItem(new ComboClave(0, ""));

		// ahora agregamos los elementos al combo
		try {

			while(nominaArticulos.next()) {

				// agregamos el par valor
				this.tDescripcion.addItem(new ComboClave(nominaArticulos.getInt("iditem"), nominaArticulos.getString("descripcion")));

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al cambiar el valor seleccionado del combo
	 * de artículos que actualiza el disponible y la imagen del
	 * artículo
	 */
	protected void seleccionaArticulo() {

		// obtenemos la id del item
        ComboClave articulo = (ComboClave) this.tDescripcion.getSelectedItem();
        int idarticulo = articulo.getClave();

        // si la clave es cero simplemente retorna
        if (idarticulo == 0) {
        	return;
        }

        // obtenemos el disponible
        Stock Inventario = new Stock();
        int disponible = Inventario.getDisponible(idarticulo);
        this.tDisponible.setText(Integer.toString(disponible));

        // obtenemos la imagen del item
        Inventario.getDatosItem(idarticulo);
        if (Inventario.getImagen() != null) {

            // leemos de la base de datos
            ImageIcon Foto = new ImageIcon(Inventario.getImagen());

            // la escalamos
            Imagenes foto = new Imagenes();
            ImageIcon ajustada = foto.redimensionar(Foto, 120, 120);

            // la asignamos al formulario
            this.lImagen.setIcon(ajustada);

        // si no hay cargada una imagen
        } else {
            this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
        }

        // almacenamos el código sop
        this.tSop.setText(Inventario.getCodigoSop());

        // seteamos el foco
        this.jSolicitado.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado si el usuario está autorizado en la tabla de stock
	 * presenta la nómina completa de pedidos para el laboratorio y
	 * agrega la columna que permite autorizar los pedidos
	 */
	@SuppressWarnings({ "serial" })
	protected void stockAdmin() {

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// define la tabla y la agrega al scroll
		this.tPedidos = new JTable();
		this.tPedidos.setFont(Fuente);
		this.tPedidos.setModel(new DefaultTableModel(
			new Object[][] {
				            {null,
				             null,
				             null,
				             null,
				             null,
				             null,
				             null,
				             null,
				             null},
			},
			new String[] {
				           "ID",
				           "Descripcion",
				           "Usuario",
				           "Cant.",
				           "Exist.",
				           "Fecha",
				           "Ed.",
				           "El.",
				           "Ap."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				                  Integer.class,
				                  String.class,
				                  String.class,
				                  Integer.class,
				                  Integer.class,
				                  String.class,
				                  Object.class,
				                  Object.class,
				                  Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
            boolean[] columnEditables = new boolean[] {false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false };

            public boolean isCellEditable(int row, int column) {
            	return columnEditables[column];
            }

		});

		// fijamos el tooltip
		this.tPedidos.setToolTipText("Pulse para ver el detalle del remito");

        // fijamos el alto de las filas
        this.tPedidos.setRowHeight(25);

        // fijamos el ancho de las columnas
        this.tPedidos.getColumn("ID").setPreferredWidth(30);
        this.tPedidos.getColumn("ID").setMaxWidth(30);
        this.tPedidos.getColumn("Usuario").setPreferredWidth(85);
        this.tPedidos.getColumn("Usuario").setMaxWidth(85);
        this.tPedidos.getColumn("Cant.").setPreferredWidth(50);
        this.tPedidos.getColumn("Cant.").setMaxWidth(50);
        this.tPedidos.getColumn("Exist.").setPreferredWidth(50);
        this.tPedidos.getColumn("Exist.").setMaxWidth(50);
        this.tPedidos.getColumn("Fecha").setPreferredWidth(80);
        this.tPedidos.getColumn("Fecha").setMaxWidth(80);
        this.tPedidos.getColumn("Ed.").setMaxWidth(30);
        this.tPedidos.getColumn("Ed.").setPreferredWidth(30);
        this.tPedidos.getColumn("El.").setMaxWidth(30);
        this.tPedidos.getColumn("El.").setPreferredWidth(30);
        this.tPedidos.getColumn("Ap.").setMaxWidth(30);
        this.tPedidos.getColumn("Ap.").setPreferredWidth(30);

		// define el scroll
		this.Scroll = new JScrollPane(this.tPedidos);
		this.Scroll.setViewportView(this.tPedidos);
		this.Scroll.setBounds(10, 150, 698, 410);
		getContentPane().add(this.Scroll);

        // fijamos el evento click
        this.tPedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tPedidosClick(evt);
            }

        });

		// carga todos los pedidos
		this.cargaStockAdmin();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado si el usuario no está autorizado en la tabla de
	 * stock, entonces presenta los pedidos del usuario y solo permite
	 * agregar pedidos y eliminar
	 */
	@SuppressWarnings({ "serial" })
	protected void stockUsuario() {

        // definimos la fuente
        Font Fuente = new Font("DejaVu Sans", 0, 12);

		// define la tabla y la agrega al scroll
		this.tPedidos = new JTable();
		this.tPedidos.setFont(Fuente);
		this.tPedidos.setModel(new DefaultTableModel(
			new Object[][] {
				            {null,
				             null,
				             null,
				             null,
				             null,
				             null,
				             null,
				             null},
			},
			new String[] {
				           "ID",
				           "Descripcion",
				           "Usuario",
				           "Cant.",
				           "Exist.",
				           "Fecha",
				           "Ed.",
				           "El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				                  Integer.class,
				                  String.class,
				                  String.class,
				                  Integer.class,
				                  Integer.class,
				                  String.class,
				                  Object.class,
				                  Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
            boolean[] columnEditables = new boolean[] {false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false,
            					false };

            public boolean isCellEditable(int row, int column) {
            	return columnEditables[column];
            }

		});

		// fijamos el tooltip
		this.tPedidos.setToolTipText("Pulse para ver el detalle del remito");

        // fijamos el alto de las filas
        this.tPedidos.setRowHeight(25);

        // fijamos el ancho de las columnas
        this.tPedidos.getColumn("ID").setPreferredWidth(30);
        this.tPedidos.getColumn("ID").setMaxWidth(30);
        this.tPedidos.getColumn("Usuario").setPreferredWidth(85);
        this.tPedidos.getColumn("Usuario").setMaxWidth(85);
        this.tPedidos.getColumn("Cant.").setPreferredWidth(50);
        this.tPedidos.getColumn("Cant.").setMaxWidth(50);
        this.tPedidos.getColumn("Exist.").setPreferredWidth(50);
        this.tPedidos.getColumn("Exist.").setMaxWidth(50);
        this.tPedidos.getColumn("Fecha").setPreferredWidth(80);
        this.tPedidos.getColumn("Fecha").setMaxWidth(80);
        this.tPedidos.getColumn("Ed.").setMaxWidth(30);
        this.tPedidos.getColumn("Ed.").setPreferredWidth(30);
        this.tPedidos.getColumn("El.").setMaxWidth(30);
        this.tPedidos.getColumn("El.").setPreferredWidth(30);

		// define el scroll
		this.Scroll = new JScrollPane(this.tPedidos);
		this.Scroll.setViewportView(this.tPedidos);
		this.Scroll.setBounds(10, 150, 698, 410);
		getContentPane().add(this.Scroll);

        // fijamos el evento click
        this.tPedidos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tPedidosClick(evt);
            }

        });

		// carga los pedidos
		this.cargaStockUsuario();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga los pedidos de todos los usuarios
	 */
	protected void cargaStockAdmin() {

		// obtenemos la nómina de todos los pedidos
		ResultSet nominaPedidos = this.Solicitudes.pedidosPendientes();

        // sobrecargamos el renderer de la tabla
        this.tPedidos.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) tPedidos.getModel();

        // hacemos la tabla se pueda ordenar
        tPedidos.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[9];

        try {

            // nos desplazamos al inicio del resultset
            nominaPedidos.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nominaPedidos.next()){

                // fijamos los valores de la fila
                fila[0] = nominaPedidos.getInt("id");
                fila[1] = nominaPedidos.getString("descripcion");
                fila[2] = nominaPedidos.getString("usuario");
                fila[3] = nominaPedidos.getInt("cantidad");
                fila[4] = nominaPedidos.getInt("existencia");
                fila[5] = nominaPedidos.getString("fecha");
                fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
                fila[8] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mImprimir.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga en la tabla los pedidos del usuario actual
	 */
	protected void cargaStockUsuario() {

		// obtenemos la nómina de los pedidos del usuario
		ResultSet nominaPedidos = this.Solicitudes.pedidosPendientes(Seguridad.Id);

        // sobrecargamos el renderer de la tabla
        this.tPedidos.setDefaultRenderer(Object.class, new RendererTabla());

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) tPedidos.getModel();

        // hacemos la tabla se pueda ordenar
        tPedidos.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

        // limpiamos la tabla
        modeloTabla.setRowCount(0);

        // definimos el objeto de las filas
        Object [] fila = new Object[8];

        try {

            // nos desplazamos al inicio del resultset
            nominaPedidos.beforeFirst();

            // iniciamos un bucle recorriendo el vector
            while (nominaPedidos.next()){

                // fijamos los valores de la fila
                fila[0] = nominaPedidos.getInt("id");
                fila[1] = nominaPedidos.getString("descripcion");
                fila[2] = nominaPedidos.getString("usuario");
                fila[3] = nominaPedidos.getInt("cantidad");
                fila[4] = nominaPedidos.getInt("existencia");
                fila[5] = nominaPedidos.getString("fecha");
                fila[6] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
                fila[7] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

                // lo agregamos
                modeloTabla.addRow(fila);

            }

        // si hubo un error
        } catch (SQLException ex){

            // presenta el mensaje
            System.out.println(ex.getMessage());

        }

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idpedido - entero con la clave del registro
	 * Método que recibe como parámetro la clave de un registro
	 * y obtiene los datos del pedido y los presenta en el
	 * formulario principal
	 */
	protected void verPedido(int idpedido) {

		// obtenemos los datos del pedido
		this.Solicitudes.getDatosPedido(idpedido);

		// asignamos los valores
		this.tId.setText(Integer.toString(this.Solicitudes.getId()));
        ComboClave solicitud = new ComboClave(this.Solicitudes.getIdItem(), this.Solicitudes.getItem());
        this.tDescripcion.setSelectedItem(solicitud);
		this.tSop.setText(this.Solicitudes.getCodigoSop());
		this.jSolicitado.setValue(this.Solicitudes.getCantidad());
		this.tDisponible.setText(Integer.toString(this.Solicitudes.getExistentes()));
		this.tUsuario.setText(this.Solicitudes.getUsuario());
		this.tFechaAlta.setText(this.Solicitudes.getFechaAlta());

		// si tiene imagen la cargamos
        if (this.Solicitudes.getFoto() != null) {

            // leemos de la base de datos
            ImageIcon Foto = new ImageIcon(this.Solicitudes.getFoto());

            // la escalamos
            Imagenes foto = new Imagenes();
            ImageIcon ajustada = foto.redimensionar(Foto, 120, 120);

            // la asignamos al formulario
            this.lImagen.setIcon(ajustada);

        // si no hay cargada una imagen
        } else {
            this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));
        }

		// fijamos el foco
		this.tDescripcion.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idpedido - entero con la clave del pedido
	 * Método que recibe como parámetro la clave de un pedido
	 * y lo elimina de la base de datos luego de pedir
	 * confirmación
	 */
	protected void eliminaPedido(int idpedido) {

		// pedimos confirmación
        int respuesta = JOptionPane.showOptionDialog(this,
                "Está seguro que desea eliminar el Pedido?",
                "Solicitudes de Materiales",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION){

			// eliminamos el registro
			this.Solicitudes.borraPedido(idpedido);

			// verificamos de limpiar el formulario
			this.cancelaPedido();

			// si es administrador
			if (Seguridad.Administrador.equals("Si")) {
				this.cargaStockAdmin();
			} else {
				this.cargaStockUsuario();
			}

		}

	}


	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que reinicia el formulario de datos
	 */
	protected void cancelaPedido() {

		// reiniciamos los elementos del formulario
		this.tId.setText("");
        ComboClave solicitud = new ComboClave(0, "");
        this.tDescripcion.setSelectedItem(solicitud);
		this.jSolicitado.setValue(0);
		this.tDisponible.setText("");
		this.tUsuario.setText(Seguridad.Usuario);
		Utilidades Herramientas = new Utilidades();
		this.tFechaAlta.setText(Herramientas.FechaActual());
		this.lImagen.setIcon(new ImageIcon(getClass().getResource("/Graficos/sin_imagen.jpg")));

		// setea el foco
		this.tDescripcion.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón grabar que verifica
	 * los datos del formulario
	 */
	protected void validaPedido() {

		// si no seleccionó artículo
		ComboClave articulo = (ComboClave) this.tDescripcion.getSelectedItem();
		if (articulo == null) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "Seleccione el artículo a pedir",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.tDescripcion.requestFocus();
            return;

        // verificamos que el id no sea 0
		} else if (articulo.getClave() == 0) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "Debe seleccionar un artículo",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.tDescripcion.requestFocus();
            return;

		}

		// si no indicó la cantidad solicitada
		if (this.jSolicitado.getValue().toString().equals("0")) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "Indique la cantidad solicitada",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.jSolicitado.requestFocus();
            return;

		}

		// verifica que exista un disponible cargado
		if (this.tDisponible.getText().isEmpty()) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "No hay datos de disponible en depósito",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.tDescripcion.requestFocus();
            return;

		}

		// si la cantidad solicitada excede el stock
		if (Integer.parseInt(this.jSolicitado.getValue().toString()) > Integer.parseInt(this.tDisponible.getText())) {

            // presenta el mensaje
            JOptionPane.showMessageDialog(this,
            		    "No hay suficientes artículos disponibles",
            		    "Error",
            		    JOptionPane.ERROR_MESSAGE);
            this.jSolicitado.requestFocus();
            return;

		}

		// si está insertando
		if (!this.tId.getText().equals("")) {

			// verifica que no está repetido
			if (!this.Solicitudes.validaPedido(articulo.getClave())) {

	            // presenta el mensaje
	            JOptionPane.showMessageDialog(this,
	            		    "Ya existe un pedido para ese artículo",
	            		    "Error",
	            		    JOptionPane.ERROR_MESSAGE);
	            this.tDescripcion.requestFocus();
	            return;

			}

		}

		// si llegó hasta aquí graba el registro
		this.grabaPedido();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de validar el pedido que ejecuta
	 * la consulta en la base y luego actualiza la grilla y
	 * limpia el formulario
	 */
	protected void grabaPedido() {

		// si está editando
		if (!this.tId.getText().equals("")) {
			this.Solicitudes.setId(Integer.parseInt(this.tId.getText()));
		} else {
			this.Solicitudes.setId(0);
		}

		// asigna las variables
        ComboClave articulo = (ComboClave) this.tDescripcion.getSelectedItem();
        this.Solicitudes.setIdItem(articulo.getClave());
		this.Solicitudes.setCantidad(Integer.parseInt(this.jSolicitado.getValue().toString()));

		// graba el pedido
		this.Solicitudes.grabaPedido();

		// si es administrador
		if (Seguridad.Administrador.equals("Si")) {

			// recarga la grilla
			this.cargaStockAdmin();

		// si es usuario
		} else {

			// recarga la grilla
			this.cargaStockUsuario();

		}

		// presenta el mensaje
		new Mensaje("Registro grabado ...");

		// limpiamos el formulario
		this.cancelaPedido();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idpedido - clave del registro
	 * Método que recibe como parámetro la clave de un pedido
	 * y directamente genera la entrega del mismo
	 */
	protected void apruebaPedido(int idpedido) {

		// obtenemos la id del usuario que solicitó el artículo
		this.Solicitudes.getDatosPedido(idpedido);
		int idSolicito = this.Solicitudes.getIdSolicito();

		// instanciamos la clase egresos
		Egresos Entregas = new Egresos();

		// obtenemos el número del nuevo remito
		int Remito = Entregas.getRemitoEgreso();

		// asignamos los valores en la clase egresos
		Entregas.setIdItem(this.Solicitudes.getIdItem());
		Entregas.setRemito(Remito);
		Entregas.setCantidad(this.Solicitudes.getCantidad());
		Entregas.setIdRecibio(idSolicito);
		Utilidades Herramientas = new Utilidades();
		Entregas.setFechaEgreso(Herramientas.FechaActual());

		// grabamos el egreso y obtenemos la id del remito
		int idEntrega = Entregas.grabaEgreso();

		// instanciamos la clase remito y lo generamos
		new RemitoEgreso(idEntrega);

		// eliminamos el pedido de los pendientes
		this.Solicitudes.borraPedido(idpedido);

		// si es administrador
		if (Seguridad.Administrador.equals("Si")){

			// recarga la grilla
			this.cargaStockAdmin();

		// si es ususario
		} else {

			// recarga la grilla
			this.cargaStockUsuario();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param evt - evento del mouse
	 * Método llamado al pulsar sobre una fila de la tabla
	 */
	private void tPedidosClick(MouseEvent evt) {

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel) this.tPedidos.getModel();

        // obtenemos la fila y columna pulsados
        int fila = this.tPedidos.rowAtPoint(evt.getPoint());
        int columna = this.tPedidos.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tPedidos.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)) {

            // obtenemos la clave del item
            int clave = (int) modeloTabla.getValueAt(indice, 0);

            // si pulsó en editar
            if (columna == 6){

                // cargamos el registro
                this.verPedido(clave);

            // si pulsó en eliminar
            } else if (columna == 7){

                // imprimimos
                this.eliminaPedido(clave);

            // si pulsó en aprobar
            } else if (columna == 8){

                // eliminamos
                this.apruebaPedido(clave);

            }

        }

	}

}
