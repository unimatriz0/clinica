/*

    Nombre: Utilidades
    Fecha: 17/11/2016
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Utilidades del sistema, herramientas para conversión de
                 tipos de datos y validación de fechas

 */

// definición del paquete
package funciones;

// importamos las librerías
import java.awt.geom.Point2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import java.util.Date;
import com.toedter.calendar.JDateChooser;
import maps.java.Geocoding;

/**
 * @author Lic. Claudio Invernizzi
 */
public class Utilidades {

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return fechaActual un string con la fecha en formato dd/mm/YYYY
     * Método que obtiene la fecha actual del sistema, la formatea y
     * la retorna como una cadena de texto
     */
    public String FechaActual(){

        // declaración de variables
        String fechaActual;

        // instanciamos el objeto calendario
        Calendar fecha = new GregorianCalendar();

        // obtenemos el año mes y día
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);

        // componemos la cadena
        fechaActual = dia + "/" + mes + "/" + anio;

        // retornamos la fecha
        return fechaActual;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return fechaCadena un string con la fecha en letras
     * Método que genera la fecha en letras a partir de la fecha actual
     */
    public String fechaLetras(){

        // declaración de variables
        String fechaCadena;
        String mesLetras = "";

        // instanciamos el objeto calendario
        Calendar fecha = new GregorianCalendar();

        // obtenemos el año mes y día (como el mes cuenta desde 0
        // hay que recordar sumarle uno
        int anio = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH) + 1;
        int dia = fecha.get(Calendar.DAY_OF_MONTH);

        // según el mes
        switch (mes) {

            // actualizamos la variable
            case 0:
                mesLetras = "Enero";
                break;
            case 1:
                mesLetras = "Febrero";
                break;
            case 2:
                mesLetras = "Marzo";
                break;
            case 3:
                mesLetras = "Abril";
                break;
            case 4:
                mesLetras = "Mayo";
                break;
            case 5:
                mesLetras = "Junio";
                break;
            case 6:
                mesLetras = "Julio";
                break;
            case 7:
                mesLetras = "Agosto";
                break;
            case 8:
                mesLetras = "Septiembre";
                break;
            case 9:
                mesLetras = "Octubre";
                break;
            case 10:
                mesLetras = "Noviembre";
                break;
            case 11:
                mesLetras = "Diciembre";
                break;

        }

        // obtenemos la cadena
        fechaCadena = Integer.toString(dia) + " de " + mesLetras + " de " + Integer.toString(anio);

        // retorna la cadena
        return fechaCadena;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param jd un objeto jdatechooser
     * @return fecha una cadena formateada
     */
    public String fechaJDate(JDateChooser jd){

        // definimos el formato de la fecha
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        // si la fecha es válida
        if(jd.getDate() != null){
            return formato.format(jd.getDate());
        } else {
            return null;
        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param fecha - cadena con una fecha
     * @return date - un objeto fecha
     * Método que recibe como parámetro una cadena de texto
     * formateada y retorna un objeto fecha, si no puede
     * convertirlo retorna null
     */
    public Date StringToDate(String fecha){

    	
        // definimos el formato e inicializamos las variables
        try {
			Date fechaParseada= new SimpleDateFormat("dd/MM/yyyy").parse(fecha);
			return fechaParseada;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
                
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param padre objeto con el padre de la ventana
     * @return string con el nombre del archivo
     * Método genérico que recibe como parámetro la ventana padre y
     * retorna una cadena con el path completo del archivo, usado en las
     * rutinas que piden el nombre de un archivo a grabar
     */
    public String grabaArchivo(java.awt.Frame padre){

        // pedimos el archivo a generar
        // definimos el filechooser
        JFileChooser dialogo = new JFileChooser();

        // seleccionamos el directorio del usuario por defecto
        dialogo.setCurrentDirectory(new File(System.getProperty("user.home")));

        // fijamos las propiedades del diálogo
        dialogo.setDialogTitle("Indique el nombre del archivo");
        dialogo.setApproveButtonText("Aceptar");

        // abrimos el dialogo
        int result = dialogo.showSaveDialog(padre);

        // si seleccionó un archivo
        if (result == JFileChooser.APPROVE_OPTION) {

            // lo asignamos
            File archivo = dialogo.getSelectedFile();

            // retornamos el path absoluto
            return archivo.getAbsolutePath();

        // si canceló
        } else {

            // simplemente retorna
            return null;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param padre objeto con el padre de la ventana
     * @return string con el nombre del archivo
     * Método genérico que recibe como parámetro la ventana padre y
     * retorna una cadena con el path completo del archivo, usado en las
     * rutinas que piden el nombre de un archivo a grabar, sobrecarga el
     * método anterior pero recibiendo un dialog como padre
     */
    public String grabaArchivo(javax.swing.JDialog padre){

        // pedimos el archivo a generar
        // definimos el filechooser
        JFileChooser dialogo = new JFileChooser();

        // seleccionamos el directorio del usuario por defecto
        dialogo.setCurrentDirectory(new File(System.getProperty("user.home")));

        // fijamos las propiedades del diálogo
        dialogo.setDialogTitle("Indique el nombre del archivo");
        dialogo.setApproveButtonText("Aceptar");

        // abrimos el dialogo
        int result = dialogo.showSaveDialog(padre);

        // si seleccionó un archivo
        if (result == JFileChooser.APPROVE_OPTION) {

            // lo asignamos
            File archivo = dialogo.getSelectedFile();

            // retornamos el path absoluto
            return archivo.getAbsolutePath();

        // si canceló
        } else {

            // simplemente retorna
            return null;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return string con el nombre del archivo
     * Método genérico que recibe como parámetro la ventana padre y
     * retorna una cadena con el path completo del archivo, usado en las
     * rutinas que piden el nombre de un archivo a grabar, sobrecarga el
     * método anterior pero estableciendo a null el padre
     */
    public String grabaArchivo(){

        // pedimos el archivo a generar
        // definimos el filechooser
        JFileChooser dialogo = new JFileChooser();

        // seleccionamos el directorio del usuario por defecto
        dialogo.setCurrentDirectory(new File(System.getProperty("user.home")));

        // fijamos las propiedades del diálogo
        dialogo.setDialogTitle("Indique el nombre del archivo");
        dialogo.setApproveButtonText("Aceptar");

        // abrimos el dialogo
        int result = dialogo.showSaveDialog(null);

        // si seleccionó un archivo
        if (result == JFileChooser.APPROVE_OPTION) {

            // lo asignamos
            File archivo = dialogo.getSelectedFile();

            // retornamos el path absoluto
            return archivo.getAbsolutePath();

        // si canceló
        } else {

            // simplemente retorna
            return null;

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param direccion cadena con la dirección a referenciar
     * @return coordenadas string con las coordenadas halladas
     * Método que recibe como parámetro una cadena con la dirección postal
     * en el formato direccion - localidad - jurisdiccion - pais y
     * retorna un string con las coordenadas gps
     */
    public String getCoordenadas(String direccion){

        // declaramos las variables
        String Coordenadas = null;

        // instanciamos el objeto
        Geocoding ObjGeocod = new Geocoding();

        try {

            // obtenemos las coordenadas
            Point2D.Double resultadoCD = ObjGeocod.getCoordinates(direccion);
            Coordenadas = "(" + resultadoCD.x + "," + resultadoCD.y + ")";

        // si hubo un error
        } catch (Exception e) {
            System.out.println("Error en la codificación");
        }

        // retornamos las coordenadas
        return Coordenadas;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param email strin con el correo a verificar
     * @return true si es correcta o false si no lo es. Valida si es correcta la
     *         dirección de correo electrónica dada.
     */
    public boolean esEmailCorrecto(String email) {

        // inicializa las variables
        String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        // fijamos el patrón con expresiones regulares
        Pattern patronEmail = Pattern.compile(PATTERN_EMAIL);

        // buscamos la ocurrencia del patrón
        Matcher mEmail = patronEmail.matcher(email.toLowerCase());

        // retornamos el resultado
        return mEmail.matches();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param archivo - string con la url del archivo
     * @param destino - string con el destino
     * Método que recibe como parámetro la url de un archivo remoto 
     * y la ubicación y el nombre del archivo destino, abre 
     * una conexión, descarga y guarda el archivo
     */
    public void descargaArchivo(String archivo, String destino) {

    	// definimos la url, la conexión de entrada y de salida
    	URL ficheroUrl;
    	InputStream inputStream = null;
    	OutputStream outputStream = null;

    	// definimos las variables
    	byte[] b = new byte[2048];
    	int longitud;
    	
		try {
			
			// abrimos las conexión y lanzamos una excepción
			// si hubo error
			ficheroUrl = new URL(archivo);
			inputStream = ficheroUrl.openStream();
	    	outputStream = new FileOutputStream(destino);
		} catch (IOException e) {
			e.printStackTrace();
		}	

		try {
			
    	  	// leemos como un array el archivo de entrada y 
   	  		// lo escribimos
  	      	while ((longitud = inputStream.read(b)) != -1) {  	    	  
				outputStream.write(b, 0, longitud);
  	      	}

  	      	// cerramos las conexiones
  	      	inputStream.close();
  	      	outputStream.close();

  	    // si hubo un error
		} catch (IOException e) {
				e.printStackTrace();
    	}
    	 
    	   
    }
    
}
