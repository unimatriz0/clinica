/*

    Nombre: Imagenes
    Fecha: 17/10/2018
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Conjunto de funciones que permiten seleccionar y
                 leer imágenes del disco

 */

// definición del paquete
package funciones;

import javax.imageio.ImageIO;
// importamos las librerías
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

// definición de la clase
public class Imagenes {

    // declaración de variables
    private String Archivo;              // path absoluto del archivo de imagen
    private int Longitud;                // longitud del archivo leido
    private FileInputStream Contenido;   // contenido del archivo

    // constructor de la clase
    public Imagenes(){

        // inicializamos las variables
        this.Archivo = "";

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que abre el cuadro de diálogo y pide seleccione
     * una imagen, que luego asigna a las variables de clase
     */
    public void leerImagen(){

        // inicializamos el filtro
        FileNameExtensionFilter Filtro = new FileNameExtensionFilter("Archivo de Imagen", "jpg", "png");
        JFileChooser Dialogo = new JFileChooser();
        Dialogo.setFileFilter(Filtro);

        // seleccionamos el directorio del usuario por defecto
        Dialogo.setCurrentDirectory(new File(System.getProperty("user.home")));

        // fijamos las propiedades del diálogo
        Dialogo.setDialogTitle("Seleccione la imagen");
        Dialogo.setApproveButtonText("Aceptar");

        // abrimos el diálogo
        int result = Dialogo.showOpenDialog(null);

        // si aceptó
        if (result == JFileChooser.APPROVE_OPTION) {

            // obtenemos el archivo seleccionado
            File archivo = Dialogo.getSelectedFile();

            // retornamos el path absoluto
            this.Archivo = archivo.getAbsolutePath();

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return el objeto como una imagen
     * Método que a partir de la variable de clase lee el archivo
     * y retorna la imagen
     */
    public ImageIcon cargarImagen() {

        // obtenemos la imagen
        ImageIcon Imagen = new ImageIcon(this.Archivo);
        return Imagen;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return el objeto como una imagen
     * Método similar al anterior pero recibe como parámetros
     * el ancho y alto de forma que retorna la imagen redimensionada
     */
    public ImageIcon cargarImagen(int ancho, int alto){

        ImageIcon Imagen = new ImageIcon(this.Archivo);
        Image original = Imagen.getImage();
        ImageIcon escalada = new ImageIcon(original.getScaledInstance(alto, ancho, Image.SCALE_SMOOTH));

        // retornamos
        return escalada;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return Archivo
     * Método que retorna el path al archivo seleccionado
     */
    public String getArchivo(){

        // retornamos la ruta
        return this.Archivo;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que a partir de la ruta del archivo lo lee
     * como un input stream y asigna el fileinput y la
     * longitud a la variable de clase
     */
    public void obtenerImagen(){

         // leemos el archivo
         File entrada = new File(this.Archivo);

         try {

             // leemos el archivo y asignamos los valores
             this.Contenido = new FileInputStream(entrada);
             this.Longitud = (int) entrada.length();

         } catch (FileNotFoundException e) {
             e.printStackTrace();
         }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return FileInputStream contenido del archivo
     * Método que retorna el contenido del archivo
     */
    public FileInputStream getContenido(){
        return this.Contenido;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return int la longitud del archivo
     * Método que retorna la longitud del archivo leído
     * que luego utilizamos para grabar en la base
     */
    public int getLongitud(){
        return this.Longitud;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param original imagen original
     * @param alto entero
     * @param ancho entero
     * @return ajustada imagen redimensionada
     * Método que recibe como parámetro un imageicon y lo
     * redimensiona al tamaño que recibe como parámetro
     */
    public ImageIcon redimensionar (ImageIcon original, int ancho, int alto){

        // obtenemos la imagen y creamos el nuevo imageicon redimensionado
        Image imagen = original.getImage();
        ImageIcon escalada = new ImageIcon(imagen.getScaledInstance(alto, ancho, Image.SCALE_SMOOTH));

        // retornamos
        return escalada;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param imagen 
     * @param ruta - string con el path absoluto
     * @param ancho - entero con el ancho que tendrá la imagen
     * @param alto - entero con el alto que tendrá la imagen
     * Método que recibe como parámetro una imagen obtenida
     * de la base de datos y el path donde grabar la imagen
     * y guarda la imagen en el disco
     */
    public void guardarImagen(Image imagen, String ruta, int alto, int ancho) {

    	// creamos el buffer de imagen y creamos el gráfico con
    	// un tamaño de 100 pixeles
    	BufferedImage archivo = new BufferedImage(alto, ancho, BufferedImage.TYPE_INT_RGB);
    	Graphics2D g2 = archivo.createGraphics();
    	
    	// agregamos la imagen al buffer y lo cerramos
    	g2.drawImage(imagen, 0, 0, alto, ancho, null);
    	g2.dispose();
    	
    	// escribimos el archivo en disco en formato jpg
    	try {
			ImageIO.write(archivo, "jpg", new File(ruta));
		} catch (IOException e) {
			e.printStackTrace();
		}
    	    	
    }
    
}