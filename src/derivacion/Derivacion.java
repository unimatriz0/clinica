/*

    Nombre: Derivacion
    Fecha: 14/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre el diccionario
                 de tipos de derivación

*/

 // declaración del paquete
package derivacion;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Derivacion {

    // declaración de las variables de clase
    protected Conexion Enlace;              // puntero a la base de datos
    protected int Id;                       // clave del registro
    protected String Derivacion;            // tipo de derivación
    protected int IdUsuario;                // clave del usuario
    protected String Usuario;               // nombre del usuario
    protected String Fecha;                 // fecha de alta del registro

    // constructor de la clase
    public Derivacion(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.Derivacion = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de variables
    public void setId(int id){
        this.Id = id;
    }
    public void setDerivacion(String derivacion){
        this.Derivacion = derivacion;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public String getDerivacion(){
        return this.Derivacion;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion entero con la clave del registro
     * Método que compone la consulta y asigna a las variables
     * de clase los valores del registro
     */
    public void getDatosDerivacion(int idderivacion){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_derivacion.id AS id, " +
                   "       diagnostico.v_derivacion.derivacion AS derivacion, " +
                   "       diagnostico.v_derivacion.usuario AS usuario, " +
                   "       diagnostico.v_derivacion.fecha AS fecha " +
                   "FROM diagnostico.v_derivacion " +
                   "WHERE diagnostico.v_derivacion.id = '" + idderivacion + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // desplazamos al primer registro y asignamos
            // los valores
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.Derivacion = Resultado.getString("derivacion");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los registros
     * Método que retorna el vector con el diccionario de
     * tipos de derivación
     */
    public ResultSet nominaDerivacion(){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diagnostico.v_derivacion.id AS id, " +
                   "       diagnostico.v_derivacion.derivacion AS derivacion, " +
                   "       diagnostico.v_derivacion.usuario AS usuario, " +
                   "       diagnostico.v_derivacion.fecha AS fecha " +
                   "FROM diagnostico.v_derivacion " +
                   "ORDER BY diagnostico.v_derivacion.derivacion; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de inserción o edición
     * según corresponda y retorna la clave del registro
     * afectado
     */
    public int grabaDerivacion(){

        // si está insertando
        if (this.Id == 0){
            this.nuevaDerivacion();
        } else {
            this.editaDerivacion();
        }

        // retornamos la clave
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevaDerivacion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diagnostico.derivacion " +
                   "       (derivacion, " +
                   "        usuario) " +
                   "       VALUES " +
                   "       (?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Derivacion);
            psInsertar.setInt(2, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaDerivacion(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diagnostico.derivacion SET " +
                   "       derivacion = ?, " +
                   "       usuario = ? " +
                   "WHERE diagnostico.derivacion.id = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.Derivacion);
            psInsertar.setInt(2, this.IdUsuario);
            psInsertar.setInt(3, this.Id);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion clave del registro
     * @return boolean si tiene hijos
     * Método que verifica si se puede eliminar una fuente
     * de derivación
     */
    public Boolean puedeBorrar(int idderivacion){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Borrar = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros " +
                   "FROM diagnostico.personas " +
                   "WHERE diagnostico.personas.derivacion = '" + idderivacion + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // si encontró registros
            if (Resultado.getInt("registros") != 0) {
                Borrar = false;
            } else {
                Borrar = true;
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos
        return Borrar;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idderivacion clave del registro
     * Método que recibe como parámetro la clave del registro
     * y ejecuta la consulta de eliminación
     */
    public void borraDerivacion(int idderivacion){

        // declaración de variables
        String Consulta;

        // compone y ejecuta la consulta
        Consulta = "DELETE FROM diagnostico.derivacion " +
                   "WHERE diagnostico.derivacion.id = '" + idderivacion + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param derivacion cadena con el tipo de derivación
     * @return boolean si ya está declarada
     * Método que recibe como parámetro el nombre de una derivación
     * y retorna verdadero si ya está declarada en la base, utilizada
     * para evitar los registros duplicados
     */
    public Boolean validaDerivacion(String derivacion){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Existe = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.derivacion.id) AS registros " +
                   "FROM diagnostico.derivacion " +
                   "WHERE diagnostico.derivacion.derivacion = '" + derivacion + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();

            // si encontró registros
            if (Resultado.getInt("registros") != 0) {
                Existe = true;
            } else {
                Existe = false;
            }

        // si hubo un error
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // retornamos
        return Existe;

    }

}