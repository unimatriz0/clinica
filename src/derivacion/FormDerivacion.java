/*

    Nombre: FormDerivacion
    Fecha: 16/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que implementa el formulario para el
                 abm de tipos de derivación

*/

// definición del paquete
package derivacion;

// importamos las librerías
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import derivacion.Derivacion;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormDerivacion extends JDialog {

	// establece el serial id
	private static final long serialVersionUID = 2838340301535652825L;

	// definimos las variables
	private JTextField tId;
	private JTextField tDerivacion;
	private JTextField tUsuario;
	private JTextField tFecha;
	private JTable tablaDerivacion;
	private Derivacion Destinos;

	// constructor de la clase
	public FormDerivacion(Frame parent, boolean modal) {

		// setea el padre e inicia los componentes
		super(parent, modal);

		// instanciamos la clase
		this.Destinos = new Derivacion();

		// fijamos las propiedades
		setBounds(120, 150, 580, 320);
		getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		// inicializamos los componentes
		this.initForm();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa los componentes del formulario
	 */
	@SuppressWarnings("serial")
	protected void initForm(){

		// define la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// establecemos el título
		JLabel lTitulo = new JLabel("Tipos de Derivación");
		lTitulo.setBounds(10, 10, 488, 26);
		lTitulo.setFont(Fuente);
		getContentPane().add(lTitulo);

		// pide la clave
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 40, 26);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setFont(Fuente);
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// pide el tipo de derivación
		this.tDerivacion = new JTextField();
		this.tDerivacion.setBounds(60, 45, 265, 26);
		this.tDerivacion.setToolTipText("Descripción de la derivación");
		this.tDerivacion.setFont(Fuente);
		getContentPane().add(this.tDerivacion);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(335, 45, 90, 26);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setEditable(false);
		this.tUsuario.setFont(Fuente);
		getContentPane().add(this.tUsuario);

		// presenta la fecha de alta
		this.tFecha = new JTextField();
		this.tFecha.setBounds(430, 45, 90, 26);
		this.tFecha.setToolTipText("Fecha de alta del registro");
		this.tFecha.setEditable(false);
		this.tFecha.setFont(Fuente);
		getContentPane().add(this.tFecha);

		// presenta el botón grabar
		JButton btnGrabar = new JButton("");
		btnGrabar.setBounds(535, 45, 26, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verificaDerivacion();
            }
        });
		getContentPane().add(btnGrabar);

		JScrollPane scrollDerivacion = new JScrollPane();
		scrollDerivacion.setBounds(10, 75, 560, 205);
		getContentPane().add(scrollDerivacion);

		this.tablaDerivacion = new JTable();
		this.tablaDerivacion.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"ID",
				"Derivacion",
				"Usuario",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class,
				String.class,
				String.class,
				Object.class,
				Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

        // establecemos el ancho de las columnas
        this.tablaDerivacion.getColumn("ID").setPreferredWidth(35);
        this.tablaDerivacion.getColumn("ID").setMaxWidth(35);
        this.tablaDerivacion.getColumn("Usuario").setPreferredWidth(80);
        this.tablaDerivacion.getColumn("Usuario").setMaxWidth(80);
        this.tablaDerivacion.getColumn("Ed.").setPreferredWidth(35);
        this.tablaDerivacion.getColumn("Ed.").setMaxWidth(35);
        this.tablaDerivacion.getColumn("El.").setPreferredWidth(35);
        this.tablaDerivacion.getColumn("El.").setMaxWidth(35);

        // establecemos el tooltip
        this.tablaDerivacion.setToolTipText("Pulse para editar / borrar");

        // fijamos el alto de las filas
        this.tablaDerivacion.setRowHeight(25);

        // fijamos el evento click
        this.tablaDerivacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tDerivacionMouseClicked(evt);
            }
        });

		// fijamos la fuente
		this.tablaDerivacion.setFont(Fuente);

		// agregamos la tabla al scroll
		scrollDerivacion.setViewportView(this.tablaDerivacion);

		// cargamos las fuentes de derivación
		this.cargaDerivaciones();

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla con los tipos de derivación
	 * declarados en la base de datos
	 */
	protected void cargaDerivaciones(){

		// obtenemos la nómina
		ResultSet Nomina = this.Destinos.nominaDerivacion();

		// sobrecargamos el renderer de la tabla
		this.tablaDerivacion.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) tablaDerivacion.getModel();

		// hacemos la tabla se pueda ordenar
		tablaDerivacion.setRowSorter(new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object[] fila = new Object[5];

		try {

			// nos desplazamos al inicio del resultset
			Nomina.beforeFirst();

			// iniciamos un bucle recorriendo el vector
			while (Nomina.next()) {

				// fijamos los valores de la fila
				fila[0] = Nomina.getInt("id");
				fila[1] = Nomina.getString("derivacion");
				fila[2] = Nomina.getString("usuario");
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

			// si hubo un error
		} catch (SQLException ex) {

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que verifica el formulario de datos antes de
	 * enviarlo al servidor
	 */
	protected void verificaDerivacion(){

		// verifica si declaró la fuente
		if (this.tDerivacion.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Indique el tipo de derivación",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			return;

		}

		// grabamos el registro
		this.grabaDerivacion();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que ejecuta la consulta en el servidor
	 */
	protected void grabaDerivacion(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Destinos.setId(0);
		} else {
			this.Destinos.setId(Integer.parseInt(this.tId.getText()));
		}

		// asignamos el destino
		this.Destinos.setDerivacion(this.tDerivacion.getText());

		// grabamos el registro
		this.Destinos.grabaDerivacion();

		// limpiamos el formulario
		this.limpiaFormulario();

		// recarga la grilla
		this.cargaDerivaciones();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que limpia el formulario de datos luego de
	 * grabar o borrar
	 */
	protected void limpiaFormulario(){

		// limpiamos los campos
		this.tId.setText("");
		this.tDerivacion.setText("");
		this.tUsuario.setText("");
		this.tFecha.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idderivacion entero con la clave del registro
	 * Método que presenta en el formulario los datos
	 * correspondientes al registro que recibe como parámetro
	 */
	protected void verDerivacion(int idderivacion){

		// obtenemos los datos del registro
		this.Destinos.getDatosDerivacion(idderivacion);

		// asignamos en el formulario
		this.tId.setText(Integer.toString(this.Destinos.getId()));
		this.tDerivacion.setText(this.Destinos.getDerivacion());
		this.tUsuario.setText(this.Destinos.getUsuario());
		this.tFecha.setText(this.Destinos.getFecha());

		// fijamos el foco
		this.tDerivacion.requestFocus();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idderivacion entero con la clave del registro
	 * Método que ejecuta la consulta de eliminación luego
	 * de pedir confirmación al usuario
	 */
	protected void borraDerivacion(int idderivacion){

		// verifica si puede borrar
		if (!this.Destinos.puedeBorrar(idderivacion)){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ese derivación está asignada",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
								   "Está seguro que desea eliminar el registro?",
								   "Derivaciones",
								   JOptionPane.YES_NO_OPTION,
								   JOptionPane.QUESTION_MESSAGE,
								   null, null, null);

		// si confirmó
		if (respuesta == JOptionPane.YES_OPTION) {

			// eliminamos el registro
			this.Destinos.borraDerivacion(idderivacion);

			// limpiamos el formulario por las dudas
			this.limpiaFormulario();

			// recargamos la grilla
			this.cargaDerivaciones();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla
	 */
    protected void tDerivacionMouseClicked(MouseEvent evt){

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tablaDerivacion.getModel();

		// obtenemos la fila y columna pulsados
		int fila = this.tablaDerivacion.rowAtPoint(evt.getPoint());
		int columna = this.tablaDerivacion.columnAtPoint(evt.getPoint());

		// como tenemos la tabla ordenada nos aseguramos de convertir
		// la fila pulsada (vista) a la fila de datos (modelo)
		int indice = this.tablaDerivacion.convertRowIndexToModel(fila);

		// si está dentro de los límites de la tabla
		if ((fila > -1) && (columna > -1)) {

			// obtenemos la clave del item
			int clave = (int) modeloTabla.getValueAt(indice, 0);

			// si pulsó en editar
			if (columna == 3) {

				// cargamos el registro
				this.verDerivacion(clave);

				// si pulsó en eliminar
			} else if (columna == 4) {

				// eliminamos
				this.borraDerivacion(clave);

			}

		}

	}

}
