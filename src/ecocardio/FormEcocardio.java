/*

    Nombre: FormEcocardio
    Fecha: 24/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Clase que arma el formulario para el abm de
	             datos de los ecocardiogramas

*/

// definición del paquete
package ecocardio;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import com.toedter.calendar.JDateChooser;
import java.awt.Dialog;
import java.awt.Font;
import java.util.Calendar;
import java.util.GregorianCalendar;
import funciones.Utilidades;
import ecocardio.Ecocardio;

// definición de la clase
public class FormEcocardio extends JDialog {

	// definimos el serial id
	private static final long serialVersionUID = -9156621487531170778L;

	// definimos las variables de clase
	private JDateChooser dFecha;
	private JComboBox<Object> cNormal;
	private JTextField tDdvi;
	private JTextField tDsvi;
	private JTextField tFac;
	private JTextField tSiv;
	private JTextField tPp;
	private JTextField tAi;
	private JTextField tAo;
	private JTextField tFey;
	private JTextField tDdvd;
	private JTextField tAd;
	private JTextField tMovilidad;
	private JTextField tFsvi;
	private Ecocardio Ecografia;            // objeto de la base de datos
	private int IdEcocardio;                // clave del registro
	private int Visita;                     // clave de la visita
	private int Protocolo;                  // clave del protocolo
	private Utilidades Herramientas;        // manejo de fechas

	// constructor de la clase cuando no recibe parámetros
	// y es un alta
	public FormEcocardio(Dialog parent, boolean modal, int protocolo, int idvisita) {

		// fijamos el padre y lo hacemos modal
		super(parent, modal);

		// fijamos las propiedades
		this.setTitle("Ecocardiograma");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setBounds(150, 150, 630, 250);
		this.setLayout(null);

		// asignamos el protocolo e inicializamos las clases
		this.Protocolo = protocolo;
		this.Herramientas = new Utilidades();
		this.Visita = idvisita;
		this.Ecografia = new Ecocardio();
		this.IdEcocardio = 0;

		// iniciamos el formulario
		this.initForm();

		// mostramos el registro
		this.verEcocardio(idvisita);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa el formulario
	 */
	private void initForm(){

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// presenta el título
		JLabel lTitulo = new JLabel("<html><b>Datos del Ecocardiograma:</b></html>");
		lTitulo.setBounds(10, 10, 190, 26);
		lTitulo.setFont(Fuente);
		this.add(lTitulo);

		// pide la fecha
		JLabel lFecha = new JLabel("Fecha:");
		lFecha.setBounds(10, 45, 55, 26);
		lFecha.setFont(Fuente);
		this.add(lFecha);
		this.dFecha = new JDateChooser("dd/MM/yyyy", "####/##/##", '_');
		this.dFecha.setBounds(50, 45, 100, 26);
		this.dFecha.setToolTipText("Indique la fecha del estudio");
		this.dFecha.setFont(Fuente);
		this.add(this.dFecha);

        // por defecto fijamos la fecha actual
        Calendar fechaActual = new GregorianCalendar();
        this.dFecha.setCalendar(fechaActual);

		// pide si es normal
		JLabel lNormal = new JLabel("Normal:");
		lNormal.setBounds(160, 45, 55, 26);
		lNormal.setFont(Fuente);
		this.add(lNormal);
		this.cNormal = new JComboBox<>();
		this.cNormal.setBounds(210, 45, 55, 26);
		this.cNormal.setToolTipText("Indique si el resultado es normal");
		this.cNormal.setFont(Fuente);
		this.add(this.cNormal);

		// agrega los elementos al combo
		this.cNormal.addItem("");
		this.cNormal.addItem("Si");
		this.cNormal.addItem("No");

		// el ddvi
		JLabel lDdvi = new JLabel("DDVI");
		lDdvi.setBounds(10, 80, 55, 26);
		lDdvi.setFont(Fuente);
		this.add(lDdvi);
		this.tDdvi = new JTextField();
		this.tDdvi.setBounds(55, 80, 70, 26);
		this.tDdvi.setFont(Fuente);
		this.add(this.tDdvi);

		// el ldsvi
		JLabel lDsvi = new JLabel("DSVI:");
		lDsvi.setBounds(145, 80, 55, 26);
		lDsvi.setFont(Fuente);
		this.add(lDsvi);
		this.tDsvi = new JTextField();
		this.tDsvi.setBounds(190, 80, 70, 26);
		this.tDsvi.setFont(Fuente);
		this.add(this.tDsvi);

		// el fac
		JLabel lFac = new JLabel("FAC:");
		lFac.setBounds(285, 80, 55, 26);
		lFac.setFont(Fuente);
		this.add(lFac);
		this.tFac = new JTextField();
		this.tFac.setBounds(330, 80, 70, 26);
		this.tFac.setFont(Fuente);
		this.add(this.tFac);

		// el siv
		JLabel lSiv = new JLabel("SIV:");
		lSiv.setBounds(425, 80, 55, 26);
		lSiv.setFont(Fuente);
		this.add(lSiv);
		this.tSiv = new JTextField();
		this.tSiv.setBounds(470, 80, 70, 26);
		this.tSiv.setFont(Fuente);
		this.add(this.tSiv);

		// el pp
		JLabel lPp = new JLabel("PP:");
		lPp.setBounds(10, 115, 55, 26);
		lPp.setFont(Fuente);
		this.add(lPp);
		this.tPp = new JTextField();
		this.tPp.setBounds(55, 115, 70, 26);
		this.tPp.setFont(Fuente);
		this.add(this.tPp);

		// el ai
		JLabel lAi = new JLabel("AI:");
		lAi.setBounds(145, 115, 55, 26);
		lAi.setFont(Fuente);
		this.add(lAi);
		this.tAi = new JTextField();
		this.tAi.setBounds(190, 115, 70, 26);
		this.tAi.setFont(Fuente);
		this.add(this.tAi);

		// el ao
		JLabel lAo = new JLabel("AO:");
		lAo.setBounds(285, 115, 55, 26);
		lAo.setFont(Fuente);
		this.add(lAo);
		this.tAo = new JTextField();
		this.tAo.setBounds(330, 115, 70, 26);
		this.tAo.setFont(Fuente);
		this.add(this.tAo);

		// el fey
		JLabel lFey = new JLabel("FEY:");
		lFey.setBounds(425, 115, 55, 26);
		lFey.setFont(Fuente);
		this.add(lFey);
		this.tFey = new JTextField();
		this.tFey.setBounds(470, 115, 70, 26);
		this.tFey.setFont(Fuente);
		this.add(this.tFey);

		// el ddve
		JLabel lDdvd = new JLabel("DDVD:");
		lDdvd.setBounds(10, 150, 55, 26);
		lDdvd.setFont(Fuente);
		this.add(lDdvd);
		this.tDdvd = new JTextField();
		this.tDdvd.setBounds(55, 150, 70, 26);
		this.tDdvd.setFont(Fuente);
		this.add(this.tDdvd);

		// el ad
		JLabel lAd = new JLabel("AD:");
		lAd.setBounds(145, 150, 55, 26);
		lAd.setFont(Fuente);
		this.add(lAd);
		this.tAd = new JTextField();
		this.tAd.setBounds(190, 150, 70, 26);
		this.tAd.setFont(Fuente);
		this.add(this.tAd);

		// el fsvi
		JLabel lFsvi = new JLabel("FSVI:");
		lFsvi.setBounds(285, 150, 55, 26);
		lFsvi.setFont(Fuente);
		this.add(lFsvi);
		this.tFsvi = new JTextField();
		this.tFsvi.setBounds(330, 150, 70, 26);
		this.tFsvi.setFont(Fuente);
		this.add(this.tFsvi);

		// la movilidad
		JLabel lMovilidad = new JLabel("Movilidad:");
		lMovilidad.setBounds(425, 150, 70, 26);
		lMovilidad.setFont(Fuente);
		this.add(lMovilidad);
		this.tMovilidad= new JTextField();
		this.tMovilidad.setBounds(490, 150, 80, 26);
		this.tMovilidad.setFont(Fuente);
		this.add(this.tMovilidad);

		// el botón grabar
		JButton btnGrabar = new JButton("Grabar");
		btnGrabar.setBounds(357, 185, 110, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                validaEcocardio();
            }
        });
		this.add(btnGrabar);

		// el botón cancelar
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(490, 185, 110, 26);
		btnCancelar.setToolTipText("Cierra el formulario sin grabar");
		btnCancelar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));
		btnCancelar.setFont(Fuente);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelaEcocardio();
            }
        });
		this.add(btnCancelar);

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idvisita - clave de la visita
	 * Método llamado desde el constructor cuando se trata
	 * de la edición de un registro
	 */
	private void verEcocardio(int idvisita){

		// obtenemos los datos del registro
		this.Ecografia.getDatosEcocardio(this.Protocolo);

		// asignamos en las variables de clase
		this.IdEcocardio = this.Ecografia.getId();

		// cargamos los datos en el formulario
		this.dFecha.setDate(this.Herramientas.StringToDate(this.Ecografia.getFecha()));
		this.cNormal.setSelectedItem(this.Ecografia.getNormal());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que valida el formulario antes de enviarlo
	 * al servidor
	 */
	private void validaEcocardio(){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que envía los datos del formulario al
	 * servidor
	 */
	private void grabaEcocardio(){

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar el botón cancelar que
	 * limpia el formulario o recarga el registro
	 */
	private void cancelaEcocardio(){

		this.verEcocardio(this.IdEcocardio);

	}

}
