/*

    Nombre: Ecocardio
    Fecha: 24/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que provee los métodos para operar con la
                 tabla de ecocardiogramas

 */

// definición del paquete
package ecocardio;

// importamos las librerías
import dbApi.Conexion;
import seguridad.Seguridad;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// definición de la clase
public class Ecocardio {

  // definimos las variables de clase
  private Conexion Enlace;              // puntero a la base de datos
  private int IdUsuario;                // clave del usuario activo
  private int Id;                       // clave del registro
  private int Paciente;                 // clave del paciente
  private int Visita;                   // clave de la visita
  private String Fecha;                 // fecha de la administración
  private int Normal;                   // 0 - falso 1 - verdadero
  private int Ddvi;                     // entero en milímetros
  private int Dsvi;                     // entero en milímetros
  private float Fac;                    // decimal
  private int Siv;                      // en milímetros
  private int Pp;                       // en milímetros
  private int Ai;                       // en milímetros
  private int Ao;                       // en milímetros
  private float Fey;                    // dos decimales
  private int Ddvd;                     // entero
  private int Ad;                       // en milímetros
  private String Movilidad;             // texto libre
  private String Fsvi;                  // texto libre
  private String Usuario;               // nombre del usuario
  private String FechaAlta;             // fecha de alta del registro

  // constructor de la clase
  public Ecocardio() {

    // inicializamos las variables
    this.Enlace = new Conexion();
    this.IdUsuario = Seguridad.Id;

    // inicializamos las variables
    this.initEcocardio();
    
  }

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que inicializa las variables de clase, llamado
	 * desde el constructor o al no encontrar el registro
	 */
	private void initEcocardio() {

		// inicializamos las variables
		this.Id = 0;
		this.Paciente = 0;
		this.Visita = 0;
		this.Fecha = "";
		this.Normal = 0;
		this.Ddvi = 0;
		this.Dsvi = 0;
		this.Fac = 0;
		this.Siv = 0;
		this.Pp = 0;
		this.Ai = 0;
		this.Ao = 0;
		this.Fey = 0;
		this.Ddvd = 0;
		this.Ad = 0;
		this.Movilidad = "";
		this.Fsvi = "";
		this.Usuario = "";
		this.FechaAlta = "";
	  
  }
  
  // métodos de asignación de valores
  public void setId(int id) {
    this.Id = id;
  }

  public void setPaciente(int paciente) {
    this.Paciente = paciente;
  }

  public void setVisita(int visita) {
    this.Visita = visita;
  }

  public void setFecha(String fecha) {
    this.Fecha = fecha;
  }

  public void setNormal(int normal) {
    this.Normal = normal;
  }

  public void setDdvi(int ddvi) {
    this.Ddvi = ddvi;
  }

  public void setDsvi(int dsvi) {
    this.Dsvi = dsvi;
  }

  public void setFac(float fac) {
    this.Fac = fac;
  }

  public void setSiv(int siv) {
    this.Siv = siv;
  }

  public void setPp(int pp) {
    this.Pp = pp;
  }

  public void setAi(int ai) {
    this.Ai = ai;
  }

  public void setAo(int ao) {
    this.Ao = ao;
  }

  public void setFey(float fey) {
    this.Fey = fey;
  }

  public void setDdvd(int ddvd) {
    this.Ddvd = ddvd;
  }

  public void setAd(int ad) {
    this.Ad = ad;
  }

  public void setMovilidad(String movilidad) {
    this.Movilidad = movilidad;
  }

  public void setFsvi(String fsvi) {
    this.Fsvi = fsvi;
  }

  // métodos de retorno de valores
  public int getId() {
    return this.Id;
  }

  public int getPaciente() {
    return this.Paciente;
  }

  public int getVisita() {
    return this.Visita;
  }

  public String getFecha() {
    return this.Fecha;
  }

  public int getNormal() {
    return this.Normal;
  }

  public int getDdvi() {
    return this.Ddvi;
  }

  public int getDsvi() {
    return this.Dsvi;
  }

  public float getFac() {
    return this.Fac;
  }

  public int getSiv() {
    return this.Siv;
  }

  public int getPp() {
    return this.Pp;
  }

  public int getAi() {
    return this.Ai;
  }

  public int getAo() {
    return this.Ao;
  }

  public float getFey() {
    return this.Fey;
  }

  public int getDdvd() {
    return this.Ddvd;
  }

  public int getAd() {
    return this.Ad;
  }

  public String getMovilidad() {
    return this.Movilidad;
  }

  public String getFsvi() {
    return this.Fsvi;
  }

  public String getUsuario() {
    return this.Usuario;
  }

  public String getFechaAlta() {
    return this.FechaAlta;
  }

  /**
   * @author Claudio Invernizzi <cinvernizzi@gmail.com>
   * @param paciente - entero con la clave del paciente
   * @return resultado - vector con los registros Método que recibe como parámetro
   *         la clave de un paciente y retorna el vector con todos los ecocardio
   *         de ese paciente (utilizado en la impresión de historias clínica
   *         clasificadas por estudio)
   */
  public ResultSet nominaEcocardio(int paciente) {

    // declaramos las variables
    String Consulta;
    ResultSet Resultado;

    // componemos la consulta
    Consulta = "SELECT diagnostico.v_ecocardio.id AS id, " 
        + "       diagnostico.v_ecocardio.paciente AS paciente, "
        + "       diagnostico.v_ecocardio.visita AS visita, " 
        + "       diagnostico.v_ecocardio.fecha AS fecha, "
        + "       diagnostico.v_ecocardio.normal AS normal, " 
        + "       diagnostico.v_ecocardio.ddvi AS ddvi, "
        + "       diagnostico.v_ecocardio.dsvi AS dsvi, " 
        + "       diagnostico.v_ecocardio.fac AS fac, "
        + "       diagnostico.v_ecocardio.siv AS siv, " 
        + "       diagnostico.v_ecocardio.pp AS pp, "
        + "       diagnostico.v_ecocardio.ai AS ai, " 
        + "       diagnostico.v_ecocardio.ao AS ao, "
        + "       diagnostico.v_ecocardio.fey AS fey, " 
        + "       diagnostico.v_ecocardio.ddvd AS ddvd, "
        + "       diagnostico.v_ecocardio.ad AS ad, " 
        + "       diagnostico.v_ecocardio.movilidad AS movilidad, "
        + "       diagnostico.v_ecocardio.fsvi AS fsvi, " 
        + "       diagnostico.v_ecocardio.usuario AS usuario, "
        + "       diagnostico.v_ecocardio.fecha_alta AS fecha_alta " 
        + "FROM diagnostico.v_ecocardio "
        + "WHERE diagnostico.v_ecocardio.paciente = '" + paciente + "'; ";

    // obtenemos el vector y retornamos
    Resultado = this.Enlace.Consultar(Consulta);
    return Resultado;

  }

  /**
   * @author Claudio Invernizzi <cinvernizzi@gmail.com>
   * @param ideco - clave del registro Método que recibe como parámetro la clave
   *              del registro y asigna los valores de la base en las variables de
   *              clase
   */
  public void getDatosEcocardio(int ideco) {

    // declaramos las variables
    String Consulta;
    ResultSet Resultado;

    // componemos la consulta
    Consulta = "SELECT diagnostico.v_ecocardio.id AS id, " 
        + "       diagnostico.v_ecocardio.paciente AS paciente, "
        + "       diagnostico.v_ecocardio.visita AS visita, " 
        + "       diagnostico.v_ecocardio.fecha AS fecha, "
        + "       diagnostico.v_ecocardio.normal AS normal, " 
        + "       diagnostico.v_ecocardio.ddvi AS ddvi, "
        + "       diagnostico.v_ecocardio.dsvi AS dsvi, " 
        + "       diagnostico.v_ecocardio.fac AS fac, "
        + "       diagnostico.v_ecocardio.siv AS siv, " 
        + "       diagnostico.v_ecocardio.pp AS pp, "
        + "       diagnostico.v_ecocardio.ai AS ai, " 
        + "       diagnostico.v_ecocardio.ao AS ao, "
        + "       diagnostico.v_ecocardio.fey AS fey, " 
        + "       diagnostico.v_ecocardio.ddvd AS ddvd, "
        + "       diagnostico.v_ecocardio.ad AS ad, " 
        + "       diagnostico.v_ecocardio.movilidad AS movilidad, "
        + "       diagnostico.v_ecocardio.fsvi AS fsvi, " 
        + "       diagnostico.v_ecocardio.usuario AS usuario, "
        + "       diagnostico.v_ecocardio.fecha_alta AS fecha_alta " 
        + "FROM diagnostico.v_ecocardio "
        + "WHERE diagnostico.v_ecocardio.id = '" + ideco + "'; ";

    // obtenemos el registro
    Resultado = this.Enlace.Consultar(Consulta);

    try {

      // nos movemos al primer registro
      if (Resultado.next()) {
    	  
    	  // si hay registros
          this.Id = Resultado.getInt("id");
          this.Paciente = Resultado.getInt("paciente");
          this.Visita = Resultado.getInt("visita");
          this.Fecha = Resultado.getString("fecha");
          this.Normal = Resultado.getInt("normal");
          this.Ddvi = Resultado.getInt("ddvi");
          this.Dsvi = Resultado.getInt("dsvi");
          this.Fac = Resultado.getFloat("fac");
          this.Siv = Resultado.getInt("siv");
          this.Pp = Resultado.getInt("pp");
          this.Ai = Resultado.getInt("ai");
          this.Ao = Resultado.getInt("ao");
          this.Fey = Resultado.getFloat("fey");
          this.Ddvd = Resultado.getInt("ddvd");
          this.Ad = Resultado.getInt("ad");
          this.Movilidad = Resultado.getString("movilidad");
          this.Fsvi = Resultado.getString("fsvi");
          this.Usuario = Resultado.getString("usuario");
          this.FechaAlta = Resultado.getString("fecha_alta");

      // si no hay registros
      } else {
    	  
    	  // inicializamos las variables
    	  this.initEcocardio();
    	  
      }
  
    // si hubo un error
    } catch (SQLException e) {
      e.printStackTrace();
    }

  }

  /**
    * @author Claudio Invernizzi <cinvernizzi@gmail.com>
    * @return id - clave del registro
    * Método que ejecuta la consulta de edición o inserción
    * según corresponda, retorna la clave del registro afectado
    */
  public int grabaEcocardio(){

    // si está insertando
    if (this.Id == 0){
      this.nuevoEcocardio();
    } else {
      this.editaEcocardio();
    }

    // retornamos la id
    return this.Id;

  }

  /**
   * @author Claudio Invernizzi <cinvernizzi@gmail.com>
   * Método que ejecuta la consulta de inserción
   */
  public void nuevoEcocardio(){

    // declaración de variables
    String Consulta;
    PreparedStatement preparedStmt;
    Connection Puntero = this.Enlace.getConexion();

    // componemos la consulta
    Consulta = "INSERT INTO diagnostico.ecocardiograma " +
                "       (paciente, " +
                "        visita, " +
                "        fecha, " +
                "        normal, " +
                "        ddvi, " +
                "        dsvi, " +
                "        fac, " +
                "        siv, " +
                "        pp, " +
                "        ai, " +
                "        ao, " +
                "        fey, " +
                "        ddvd, " +
                "        ad, " +
                "        movilidad, " +
                "        fsvi, " +
                "        usuario) " +
                "       VALUES " +
                "       (?, ?, STR_TO_DATE(?, '%d/%m/%Y'), ?, ?, ?, ?, ?, " +
                "        ?, ?, ?, ?, ?, ?, ?, ?, ?); ";

    try {

      // asignamos el puntero a la consulta
      preparedStmt = Puntero.prepareStatement(Consulta);

      // asignamos los valores
      preparedStmt.setInt(1,    this.Paciente);
      preparedStmt.setInt(2,    this.Visita);
      preparedStmt.setString(3, this.Fecha);
      preparedStmt.setInt(4,    this.Normal);
      preparedStmt.setInt(5,    this.Ddvi);
      preparedStmt.setInt(6,    this.Dsvi);
      preparedStmt.setFloat(7,  this.Fac);
      preparedStmt.setInt(8,    this.Siv);
      preparedStmt.setInt(9,    this.Pp);
      preparedStmt.setInt(10,   this.Ai);
      preparedStmt.setInt(11,   this.Ao);
      preparedStmt.setFloat(12, this.Fey);
      preparedStmt.setInt(13,   this.Ddvd);
      preparedStmt.setInt(14,   this.Ad);
      preparedStmt.setString(15,this.Movilidad);
      preparedStmt.setString(16,this.Fsvi);
      preparedStmt.setInt(17,   this.IdUsuario);

      // ejecutamos la consulta
      preparedStmt.execute();

      // obtenemos la id
      this.Id = this.Enlace.UltimoInsertado();

    // si hubo un error
    } catch (SQLException e) {
        e.printStackTrace();
    }
                          
  }

  /**
   * @author Claudio Invernizzi <cinvernizzi@gmail.com>
   * Método que ejecuta la consulta de edición
   */
  public void editaEcocardio(){

    // declaración de variables
    String Consulta;
    PreparedStatement preparedStmt;
    Connection Puntero = this.Enlace.getConexion();
    
    // componemos la consulta
    Consulta = "UPDATE diagnostico.ecocardiograma SET " +
                "       paciente = ?, " +
                "       visita = ?, " +
                "       fecha = STR_TO_DATE(?, '%d/%m/%Y'), " +
                "       normal = ?, " +
                "       ddvi = ?, " +
                "       dsvi = ?, " +
                "       fac = ?, " +
                "       siv = ?, " +
                "       pp = ?, " +
                "       ai = ?, " +
                "       ao = ?, " +
                "       fey = ?, " +
                "       ddvd = ?, " +
                "       ad = ?, " +
                "       movilidad = ?, " +
                "       fsvi = ?, " +
                "       usuario = ? " +
                "WHERE diagnostico.ecocardiograma.id = ?; ";

    try {

      // asignamos el puntero a la consulta
      preparedStmt = Puntero.prepareStatement(Consulta);

      // asignamos los valores
      preparedStmt.setInt(1,    this.Paciente);
      preparedStmt.setInt(2,    this.Visita);
      preparedStmt.setString(3, this.Fecha);
      preparedStmt.setInt(4,    this.Normal);
      preparedStmt.setInt(5,    this.Ddvi);
      preparedStmt.setInt(6,    this.Dsvi);
      preparedStmt.setFloat(7,  this.Fac);
      preparedStmt.setInt(8,    this.Siv);
      preparedStmt.setInt(9,    this.Pp);
      preparedStmt.setInt(10,   this.Ai);
      preparedStmt.setInt(11,   this.Ao);
      preparedStmt.setFloat(12, this.Fey);
      preparedStmt.setInt(13,   this.Ddvd);
      preparedStmt.setInt(14,   this.Ad);
      preparedStmt.setString(15,this.Movilidad);
      preparedStmt.setString(16,this.Fsvi);
      preparedStmt.setInt(17,   this.IdUsuario);

      // ejecutamos la consulta
      preparedStmt.execute();

    // si hubo un error
    } catch (SQLException e) {
        e.printStackTrace();
    }
      
  }

}