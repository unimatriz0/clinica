/*

    Nombre: FormEstados
    Fecha: 16/01/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: diagnostico
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
	Comentarios: Método que arma el formulario para el ABM
	             de los estasdos civiles

 */

// definición del paquete
package estados;

// importamos las librerías
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import estados.Estados;
import funciones.RendererTabla;
import java.awt.Font;

// definición de la clase
public class FormEstados extends JDialog {

	// creamos el serial id
	private static final long serialVersionUID = -2510785441011916816L;

	// definimos las variables de clase
	private JTextField tId;
	private JTextField tEstado;
	private JTextField tUsuario;
	private JTextField tFecha;
	private JTable tNomina;
	private Estados Civiles;

	// constructor de la clase
	@SuppressWarnings({ "serial" })
	public FormEstados(Frame parent, boolean modal) {

		// fijamos el formulario padre
		super(parent, modal);

		// inicializamos la clase
		this.Civiles = new Estados();

		// definimos la fuente
		Font Fuente = new Font("DejaVu Sans", 0, 12);

		// fijamos las propiedades
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setBounds(130, 150, 475, 300);
		getContentPane().setLayout(null);

		// presenta el título
		JLabel lTitulo = new JLabel("Estados Civiles Registrados");
		lTitulo.setFont(Fuente);
		lTitulo.setBounds(10, 10, 415, 26);
		getContentPane().add(lTitulo);

		// presenta la clave
		this.tId = new JTextField();
		this.tId.setBounds(10, 45, 40, 26);
		this.tId.setFont(Fuente);
		this.tId.setToolTipText("Clave del registro");
		this.tId.setEditable(false);
		getContentPane().add(this.tId);

		// pide el estado civil
		this.tEstado = new JTextField();
		this.tEstado.setBounds(70, 45, 160, 26);
		this.tEstado.setFont(Fuente);
		this.tEstado.setToolTipText("Indique el nombre del estado civil");
		getContentPane().add(this.tEstado);

		// presenta el usuario
		this.tUsuario = new JTextField();
		this.tUsuario.setBounds(240, 45, 85, 26);
		this.tUsuario.setToolTipText("Usuario que ingresó el registro");
		this.tUsuario.setFont(Fuente);
		this.tUsuario.setEditable(false);
		getContentPane().add(this.tUsuario);

		// presenta la fecha de alta
		this.tFecha = new JTextField();
		this.tFecha.setBounds(340, 45, 85, 26);
		this.tFecha.setToolTipText("Fecha de alta del registro");
		this.tFecha.setFont(Fuente);
		this.tFecha.setEditable(false);
		getContentPane().add(this.tFecha);

		JButton btnGrabar = new JButton("");
		btnGrabar.setBounds(427, 45, 26, 26);
		btnGrabar.setToolTipText("Pulse para grabar el registro");
		btnGrabar.setIcon(new ImageIcon(getClass().getResource("/Graficos/mgrabar.png")));
		btnGrabar.setFont(Fuente);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verificaEstado();
            }
        });
		getContentPane().add(btnGrabar);

		// define el scroll
		JScrollPane scrollEstados = new JScrollPane();
		scrollEstados.setBounds(10, 75, 450, 180);
		getContentPane().add(scrollEstados);

		// definimos la tabla
		this.tNomina = new JTable();
		this.tNomina.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null},
			},
			new String[] {
				"ID",
				"Estado",
				"Usuario",
				"Ed.",
				"El."
			}
		) {
			@SuppressWarnings("rawtypes")
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, String.class, Object.class, Object.class
			};
			@SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// establecemos el ancho de las columnas
		this.tNomina.getColumn("ID").setPreferredWidth(30);
		this.tNomina.getColumn("ID").setMaxWidth(30);
		this.tNomina.getColumn("Usuario").setPreferredWidth(70);
		this.tNomina.getColumn("Usuario").setMaxWidth(70);
		this.tNomina.getColumn("Ed.").setPreferredWidth(35);
		this.tNomina.getColumn("Ed.").setMaxWidth(35);
		this.tNomina.getColumn("El.").setPreferredWidth(35);
		this.tNomina.getColumn("El.").setMaxWidth(35);

    	// establecemos el tooltip
		this.tNomina.setToolTipText("Pulse para editar / borrar");

		// fijamos el alto de las filas
		this.tNomina.setRowHeight(25);

		// fijamos la fuente
		this.tNomina.setFont(Fuente);

		// agregamos la tabla al scroll
		scrollEstados.setViewportView(this.tNomina);

		// cargamos la nómina
		this.cargaEstados();

		// agregamos el evento de la tabla
		this.tNomina.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tNominaMouseClicked(evt);
            }
	    });

		// mostramos el formulario
		this.setVisible(true);

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método que carga la grilla con los estados declarados
	 */
	protected void cargaEstados(){

		// sobrecargamos el renderer de la tabla
		this.tNomina.setDefaultRenderer(Object.class, new RendererTabla());

		// obtenemos el modelo de la tabla
		DefaultTableModel modeloTabla = (DefaultTableModel) this.tNomina.getModel();

		// hacemos la tabla se pueda ordenar
		this.tNomina.setRowSorter (new TableRowSorter<DefaultTableModel>(modeloTabla));

		// limpiamos la tabla
		modeloTabla.setRowCount(0);

		// definimos el objeto de las filas
		Object [] fila = new Object[5];

		// ahora obtenemos la nomina
		ResultSet nomina = this.Civiles.nominaEstados();

		try{

			// nos desplazamos al primer registro
			nomina.beforeFirst();

			// recorremos el vector
			while(nomina.next()){

				// agregamos los elementos al array
				fila[0] = nomina.getInt("id");
				fila[1] = nomina.getString("estadocivil");
				fila[2] = nomina.getString("usuario");
				fila[3] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/meditar.png")));
				fila[4] = new JLabel(new ImageIcon(getClass().getResource("/Graficos/mBorrar.png")));

				// lo agregamos
				modeloTabla.addRow(fila);

			}

		// si hubo un error
		} catch (SQLException ex){

			// presenta el mensaje
			System.out.println(ex.getMessage());

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre el botón grabar que
	 * verifica los datos del formulario
	 */
	protected void verificaEstado(){

		// verificamos que halla ingresado el estado
		if (this.tEstado.getText().isEmpty()){

			// presenta el mensaje
			JOptionPane.showMessageDialog(this,
						"Ingrese el estado civil",
						"Error",
						JOptionPane.ERROR_MESSAGE);
			this.tEstado.requestFocus();
			return;

		}

		// grabamos el registro
		this.grabaEstado();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de verificar el formulario que
	 * ejecuta la consulta en el servidor
	 */
	protected void grabaEstado(){

		// si está insertando
		if (this.tId.getText().isEmpty()){
			this.Civiles.setId(0);
		} else {
			this.Civiles.setId(Integer.parseInt(this.tId.getText()));
		}

		// asignamos el estado y grabamos
		this.Civiles.setEstadoCivil(this.tEstado.getText());
		this.Civiles.grabaEstado();

		// limpiamos el formulario
		this.limpiaFormulario();

		// recargamos la grilla
		this.cargaEstados();

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado luego de borrar o grabar que limpia
	 * el formulario de datos
	 */
	protected void limpiaFormulario(){

		// limpiamos los valores
		this.tId.setText("");
		this.tEstado.setText("");
		this.tUsuario.setText("");
		this.tFecha.setText("");

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idestado clave del registro
	 * Método que recibe como parámetro la clave del
	 * registro y lo presenta en el formulario
	 */
	protected void verEstado(int idestado){

		// obtenemos el registro
		this.Civiles.getDatosEstado(idestado);

		// presentamos el registro
		this.tId.setText(Integer.toString(this.Civiles.getId()));
		this.tEstado.setText(this.Civiles.getEstado());
		this.tUsuario.setText(this.Civiles.getUsuario());
		this.tFecha.setText(this.Civiles.getFecha());

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * @param idestado entero con la clave del registro
	 * Método que recibe como parámetro la clave del
	 * registro y luego de pedir confirmación ejecuta la
	 * consulta de eliminación
	 */
	protected void borraEstado(int idestado){

		// verifica si puede borrar
		if (!this.Civiles.puedeBorrar(idestado)){

			// presenta el mensaje y retorna
			JOptionPane.showMessageDialog(this,
					   "Ese estado está asignado a pacientes",
					   "Error",
					   JOptionPane.ERROR_MESSAGE);
			return;

		}

		// pide confirmación
		int respuesta = JOptionPane.showOptionDialog(this,
								   "Está seguro que desea eliminar el registro?",
								   "Estados Civiles",
								   JOptionPane.YES_NO_OPTION,
								   JOptionPane.QUESTION_MESSAGE,
								   null, null, null);

		// si respondió que sí
		if (respuesta == JOptionPane.YES_OPTION){

			// eliminamos el registro
			this.Civiles.borraEstado(idestado);

			// recargamos la grilla
			this.cargaEstados();

			// limpiamos el formulario
			this.limpiaFormulario();

		}

	}

	/**
	 * @author Claudio Invernizzi <cinvernizzi@gmail.com>
	 * Método llamado al pulsar sobre la grilla de localidades
	 */
	protected void tNominaMouseClicked(MouseEvent evt){

        // obtenemos el modelo de la tabla
        DefaultTableModel modeloTabla = (DefaultTableModel)tNomina.getModel();

        // obtenemos la fila y columna pulsados
        int fila = tNomina.rowAtPoint(evt.getPoint());
        int columna = tNomina.columnAtPoint(evt.getPoint());

        // como tenemos la tabla ordenada nos aseguramos de convertir
        // la fila pulsada (vista) a la fila de datos (modelo)
        int indice = this.tNomina.convertRowIndexToModel (fila);

        // si está dentro de los límites de la tabla
        if ((fila > -1) && (columna > -1)){

            // obtenemos la id del registro
            int id = (int) modeloTabla.getValueAt(indice, 0);

            // según la columna pulsada
            if (columna == 3){
            	this.verEstado(id);
            } else if (columna == 4){
            	this.borraEstado(id);
            }

		}

	}

}
