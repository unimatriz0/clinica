/*

    Nombre: Estados
    Fecha: 11/04/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Clínica
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: Clase que controla las operaciones sobre la tabla de
                 estados civiles

*/

 // declaración del paquete
package estados;

// importamos las librerías
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import dbApi.Conexion;
import seguridad.Seguridad;

// declaración de la clase
public class Estados {

    // declaración de variables de clase
    protected Conexion Enlace;               // puntero a la base de datos
    protected int Id;                        // clave del registro
    protected String EstadoCivil;            // descripción del estado
    protected int IdUsuario;                 // clave del usuario
    protected String Usuario;                // nombre del usuario
    protected String Fecha;                  // fecha de alta del registro

    // constructor de la clase
    public Estados(){

        // inicializamos las variables
        this.Enlace = new Conexion();
        this.Id = 0;
        this.EstadoCivil = "";
        this.IdUsuario = Seguridad.Id;
        this.Usuario = "";
        this.Fecha = "";

    }

    // métodos de asignación de variables
    public void setId(int id){
        this.Id = id;
    }
    public void setEstadoCivil(String estado){
        this.EstadoCivil = estado;
    }

    // métodos de retorno de valores
    public int getId(){
        return this.Id;
    }
    public String getEstado(){
        return this.EstadoCivil;
    }
    public String getUsuario(){
        return this.Usuario;
    }
    public String getFecha(){
        return this.Fecha;
    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idestado - clave del registro
     * Método que recibe como parámetro la clave del registro
     * y asigna los valores del mismo en las variables de clase
     */
    public void getDatosEstado(int idestado){

        // definimos las variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta y la ejecutamos
        Consulta = "SELECT diccionarios.v_estados.id AS id, " +
                   "       diccionarios.v_estados.estadocivil AS estadocivil, " +
                   "       diccionarios.v_estados.usuario AS usuario, " +
                   "       diccionarios.v_estados.fecha AS fecha " +
                   "FROM diccionarios.v_estados " +
                   "WHERE diccionarios.v_estados.id = '" + idestado + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos movemos al primer registro
            Resultado.next();
            this.Id = Resultado.getInt("id");
            this.EstadoCivil = Resultado.getString("estadocivil");
            this.Usuario = Resultado.getString("usuario");
            this.Fecha = Resultado.getString("fecha");

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return vector con los resultados
     * Método que retorna un vector con la nómina de estados
     * declarados en la base
     */
    public ResultSet nominaEstados(){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;

        // componemos la consulta
        Consulta = "SELECT diccionarios.v_estados.id AS id, " +
                   "       diccionarios.v_estados.estadocivil AS estadocivil, " +
                   "       diccionarios.v_estados.usuario AS usuario, " +
                   "       diccionarios.v_estados.fecha AS fecha " +
                   "FROM diccionarios.v_estados " +
                   "ORDER BY diccionarios.v_estados.estadocivil; ";

        // ejecutamos la consulta y retornamos el vector
        Resultado = this.Enlace.Consultar(Consulta);
        return Resultado;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @return entero con la clave del registro
     * Método que ejecuta la consulta de edición o inserción
     * según corresponda
     */
    public int grabaEstado(){

        // si está insertando
        if (this.Id == 0){
            this.nuevoEstado();
        } else {
            this.editaEstado();
        }

        // retorna la id
        return this.Id;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de inserción
     */
    protected void nuevoEstado(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "INSERT INTO diccionarios.estado_civil " +
                   "       (estado_civil, " +
                   "        id_usuario) " +
                   "       VALUES " +
                   "       (?, ?); ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.EstadoCivil);
            psInsertar.setInt(2, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // asigna la id del registro
        this.Id = this.Enlace.UltimoInsertado();

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * Método que ejecuta la consulta de edición
     */
    protected void editaEstado(){

        // declaración de variables
        String Consulta;
        PreparedStatement psInsertar;
        Connection Puntero;

        // obtenemos la conexión a la base
        Puntero = this.Enlace.getConexion();

        // componemos la consulta
        Consulta = "UPDATE diccionarios.estado_civil SET " +
                   "       estado_civil = ?, " +
                   "       id_usuario = ? " +
                   "WHERE diccionarios.estado_civil.id_estado = ?; ";

        try {

            // ahora preparamos la consulta
            psInsertar = Puntero.prepareStatement(Consulta);

            // asignamos los parámetros de la consulta
            psInsertar.setString(1, this.EstadoCivil);
            psInsertar.setInt(2, this.IdUsuario);
            psInsertar.setInt(3, this.IdUsuario);

            // ejecutamos la edición
            psInsertar.execute();

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idestado entero con la clave del registro
     * @return boolean si puede o no borrar
     * Método que verifica si puede eliminar un estado civil
     */
    public boolean puedeBorrar(int idestado){

        // declaración de variables
        String Consulta;
        ResultSet Resultado;
        Boolean Borrar = false;

        // componemos la consulta
        Consulta = "SELECT COUNT(diagnostico.personas.protocolo) AS registros " +
                   "FROM diagnostico.personas " +
                   "WHERE diagnostico.personas.estado_civil = '" + idestado + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos desplazamos al primer registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") != 0){
                Borrar = false;
            } else {
                Borrar = true;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // retornamos
        return Borrar;

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param idestado entero con la clave del registro
     * Método que ejecuta la consulta de eliminación
     */
    public void borraEstado(int idestado){

        // declaración de variables
        String Consulta;

        // componemos y ejecutamos la consulta
        Consulta = "DELETE FROM diccionarios.estado_civil " +
                   "WHERE diccionarios.estado_civil.id_estado = '" + idestado + "'; ";
        this.Enlace.Ejecutar(Consulta);

    }

    /**
     * @author Claudio Invernizzi <cinvernizzi@gmail.com>
     * @param estado cadena con el nombre del estado civil
     * @return boolean si existe el registro
     * Método que verifica que no exista el registro en la
     * base, utilizado para evitar el ingreso de registros
     * duplicados
     */
    public Boolean validaEstado(String estado){

        // declaramos las variables
        String Consulta;
        ResultSet Resultado;
        Boolean Existe = false;

        // componemos y ejecutamos la consulta
        Consulta = "SELECT COUNT(diccionarios.estado_civile.id_estado) AS registros " +
                   "FROM diccionarios.estado_civile " +
                   "WHERE diccionarios.estado_civile.estado_civil = '" + estado + "'; ";
        Resultado = this.Enlace.Consultar(Consulta);

        try {

            // nos desplazamos al primer registro
            Resultado.next();

            // si hay registros
            if (Resultado.getInt("registros") != 0){
                Existe = true;
            } else {
                Existe = false;
            }

        // si hubo un error
        } catch (SQLException ex) {

            // presenta el mensaje de error
            System.out.println(ex.getMessage());

        }

        // retornamos
        return Existe;

    }

}
