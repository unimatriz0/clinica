-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- seleccionamos la base de datos
USE diccionarios;

--
-- Estructura de tabla para la tabla paises
--

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS paises;

-- la recreamos
CREATE TABLE IF NOT EXISTS paises (
  NOMBRE varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  ID smallint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  FECHA_ALTA timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  USUARIO smallint(4) UNSIGNED NOT NULL,
  PRIMARY KEY (ID),
  UNIQUE KEY pais (NOMBRE)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Nacionalidades';

--
-- Volcado de datos para la tabla paises
--

INSERT INTO paises (NOMBRE, ID, FECHA_ALTA, USUARIO) VALUES
('Argentina', 1, '2011-09-05', 1),
('Chile', 2, '2011-09-05', 1),
('Brasil', 3, '2011-09-05', 1),
('Uruguay', 4, '2011-09-05', 1),
('Paraguay', 5, '2011-09-05', 1),
('Perú', 6, '2011-09-05', 1),
('Ecuador', 7, '2011-09-05', 1),
('Nicaragua', 8, '2011-09-05', 1),
('Costa Rica', 9, '2011-09-05', 1),
('Bolivia', 10, '2011-09-05', 1),
('Cuba', 11, '2011-09-05', 1),
('México', 12, '2011-09-05', 1),
('Estados Unidos', 13, '2011-09-05', 1),
('Canadá', 14, '2011-09-05', 1),
('Portugal', 16, '2011-09-05', 1),
('España', 17, '2011-09-05', 1),
('Italia', 18, '2011-09-05', 1),
('Francia', 19, '2011-09-05', 1),
('Rusia', 26, '2011-09-05', 1),
('China', 30, '2011-09-05', 1),
('Corea', 31, '2011-09-05', 1),
('Japón', 32, '2011-09-05', 1),
('Irán', 39, '2011-09-05', 1),
('Puerto Rico', 45, '2011-09-05', 1),
('Colombia', 85, '2011-09-05', 1),
('República Dominicana', 95, '2011-09-05', 1),
('El Salvador', 96, '2011-09-05', 1),
('Guatemala', 116, '2011-09-05', 1),
('Haití', 122, '2011-09-05', 1),
('Hawai', 123, '2011-09-05', 1),
('Honduras', 125, '2011-09-05', 1),
('Hungría', 126, '2011-09-05', 1),
('Israel', 129, '2011-09-05', 1),
('Venezuela', 220, '2011-09-05', 1),
('NO DECLARADA', 232, '2011-09-05', 1),
('Antártida', 233, '2011-09-05', 1),
('Antigua', 234, '2011-09-05', 1),
('Barbados', 235, '2011-09-05', 1),
('Aruba', 236, '2011-09-05', 1),
('Surinam', 237, '2011-09-05', 1),
('Trinidad & Tobago', 238, '2011-09-05', 1),
('Granada', 239, '2011-09-05', 1),
('Guyana', 240, '2011-09-05', 1),
('Dominica', 241, '2011-09-05', 1),
('Martinica', 242, '2011-09-05', 1),
('Montserrat', 243, '2011-09-05', 1),
('Guadalupe', 244, '2011-09-05', 1),
('Antillas Holandesas', 245, '2011-09-05', 1),
('San Cristobal', 246, '2011-09-05', 1),
('Santa Lucía', 247, '2011-09-05', 1),
('San Vicente y las Granadinas', 248, '2011-09-05', 1),
('Mozambiquelandia', 249, '2018-02-15', 1);

-- eliminamos la tabla de autoría si existe
DROP TABLE IF EXISTS auditoria_paises;

-- la recreamos
CREATE TABLE IF NOT EXISTS auditoria_paises (
  NOMBRE varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '',
  ID smallint(3) UNSIGNED NOT NULL,
  FECHA_ALTA date NOT NULL,
  USUARIO smallint(4) UNSIGNED NOT NULL,
  FECHA_EVENTO timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  EVENTO enum("Edicion", "Eliminacion"),
  KEY ID(ID),
  kEY PAIS(NOMBRE)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Nacionalidades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_paises;

-- lo recreamos
CREATE TRIGGER edicion_paises
AFTER UPDATE ON paises
FOR EACH ROW
INSERT INTO auditoria_paises
       (NOMBRE,
        ID,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.NOMBRE,
        OLD.ID,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_paises;

-- lo recreamos
CREATE TRIGGER eliminacion_paises
AFTER DELETE ON paises
FOR EACH ROW
INSERT INTO auditoria_paises
       (NOMBRE,
        ID,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.NOMBRE,
        OLD.ID,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Eliminacion");

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_paises;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_paises AS
  SELECT diccionarios.paises.nombre AS pais,
         diccionarios.paises.id AS id,
         DATE_FORMAT(diccionarios.paises.fecha_alta, '%d/%m/%Y') AS fecha_alta,
         diccionarios.paises.usuario AS idusuario,
         cce.responsables.usuario AS usuario
  FROM diccionarios.paises INNER JOIN cce.responsables ON diccionarios.paises.usuario = cce.responsables.id;
