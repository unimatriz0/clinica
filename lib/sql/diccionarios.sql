/*

    Nombre: diccionarios.sql
    Fecha: 23/06/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definición de las tablas auxiliares de diagnóstico y cce

*/

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- seleccionamos
USE diccionarios;


/**********************************************************************************************/
/*                                                                                            */
/*                          COMPANIAS DE CELULAR                                              */
/*                                                                                            */
/**********************************************************************************************/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS celulares;

-- la recreamos
CREATE TABLE celulares (
    id int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    compania VARCHAR(50) NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY compania(compania),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Compañias de celular';

-- agregamos los valores iniciales
INSERT INTO celulares (compania, id_usuario) VALUES ("Claro", 1);
INSERT INTO celulares (compania, id_usuario) VALUES ("Movistar", 1);
INSERT INTO celulares (compania, id_usuario) VALUES ("Personal", 1);

-- eliminamos la auditoría si existe
DROP TABLE IF EXISTS auditoria_celulares;

-- la recreamos
CREATE TABLE auditoria_celulares (
    id int(1) UNSIGNED NOT NULL,
    compania VARCHAR(50) NOT NULL,
    fecha_alta date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY compania(compania),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de celulares';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_celulares;

-- lo recreamos
CREATE TRIGGER edicion_celulares
AFTER UPDATE ON celulares
FOR EACH ROW
INSERT INTO auditoria_celulares
       (id,
        compania,
        fecha_alta,
        id_usuario,
        evento)
       VALUES
       (OLD.id,
        OLD.compania,
        OLD.fecha_alta,
        OLD.id_usuario,
        'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_celulares;

-- lo recreamos
CREATE TRIGGER eliminacion_celulares
AFTER DELETE ON celulares
FOR EACH ROW
INSERT INTO auditoria_celulares
       (id,
        compania,
        fecha_alta,
        id_usuario,
        evento)
       VALUES
       (OLD.id,
        OLD.compania,
        OLD.fecha_alta,
        OLD.id_usuario,
        'Eliminacion');

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_celulares;

CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_celulares AS
       SELECT diccionarios.celulares.id AS id, 
              diccionarios.celulares.compania AS compania, 
              DATE_FORMAT(diccionarios.celulares.fecha_alta, '%d/%m/%Y') AS fecha_alta,
              diccionarios.celulares.id_usuario AS idusuario,
              cce.responsables.usuario AS usuario
       FROM diccionarios.celulares INNER JOIN cce.responsables ON diccionarios.celulares.id_usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                  Diccionario de Dependencias                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de dependencias
 id_dependencia entero pequeño (1) autonumerico, clave unica del registro
 dependencia varchar(50) nombre de la dependencia
 desc_abr varchar(10) descripcion abreviada de la dependencia
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS dependencias;

-- la recreamos desde cero
CREATE TABLE dependencias (
    id_dependencia tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    dependencia VARCHAR(50) DEFAULT NULL,
    des_abr VARCHAR(10) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_dependencia),
    KEY dependencia(dependencia),
    KEY des_abr(des_abr),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con los tipos dependencia';

-- cargamos los registros iniciales
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (1, "Nacional", "Nac.", 1);
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (2, "Provincial", "Prov.", 1);
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (3, "Municipal", "Mun.", 1);
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (4, "Universitario", "Unv.", 1);
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (5, "Obra Social", "OS.", 1);
INSERT INTO dependencias (id_dependencia, dependencia, des_abr, id_usuario) VALUES (6, "Privado", "Priv.", 1);

-- eliminamos la tabla de auditorías
DROP TABLE IF EXISTS auditoria_dependencias;

-- la recreamos
CREATE TABLE auditoria_dependencias (
    id_dependencia tinyint(1) UNSIGNED NOT NULL,
    dependencia VARCHAR(50) DEFAULT NULL,
    des_abr VARCHAR(10) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id_dependencia),
    KEY dependencia(dependencia),
    KEY des_abr(des_abr),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de los tipos dependencia';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_dependencias;

-- lo recreamos
CREATE TRIGGER edicion_dependencias
AFTER UPDATE ON dependencias
FOR EACH ROW
INSERT INTO auditoria_dependencias
       (id_dependencia,
        dependencia,
        des_abr,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_dependencia,
        OLD.dependencia,
        OLD.des_abr,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_dependencias;

-- lo recreamos
CREATE TRIGGER eliminacion_dependencias
AFTER DELETE ON dependencias
FOR EACH ROW
INSERT INTO auditoria_dependencias
       (id_dependencia,
        dependencia,
        des_abr,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_dependencia,
        OLD.dependencia,
        OLD.des_abr,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_dependencias;

-- la recreamos 
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_dependencias AS
       SELECT diccionarios.dependencias.id_dependencia AS id, 
              diccionarios.dependencias.dependencia AS dependencia, 
              diccionarios.dependencias.des_abr AS descripcion,
              diccionarios.dependencias.id_usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diccionarios.dependencias.fecha, '%d/%m/%Y') AS fecha
       FROM diccionarios.dependencias INNER JOIN cce.responsables ON diccionarios.dependencias.id_usuario = cce.responsables.id;

       
/***************************************************************************/
/*                                                                         */
/*                         Tipos de Documento                              */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de tipos de documento
 id_documento entero pequeño(1) autonumerico clave del registro
 tipo_documento varchar(50) descripcion del documento
 des_abreviada varchar(10) descripcion abreviada del tipo de documento
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tipo_documento;

-- la recreamos desde cero
CREATE TABLE tipo_documento (
    id_documento int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    tipo_documento VARCHAR(50) DEFAULT NULL,
    des_abreviada VARCHAR(10) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_documento),
    KEY tipo_documento(tipo_documento),
    KEY descripcion(des_abreviada),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con los tipos de documentos';

-- ahora cargamos los valores iniciales
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (1, "Documento Nacional de Identidad", "D.N.I.", 1);
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (2, "Cedula de Identidad", "C.I.", 1);
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (3, "Librera de Enrolamiento", "L.E.", 1);
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (4, "Librera Cívica", "L.C.", 1);
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (5, "Pasaporte", "Pas.", 1);
INSERT INTO tipo_documento (id_documento, tipo_documento, des_abreviada, id_usuario) VALUES (6, "Otro", "Otro", 1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tipo_documento;

-- la recreamos
CREATE TABLE auditoria_tipo_documento (
    id_documento int(1) UNSIGNED NOT NULL,
    tipo_documento VARCHAR(50) DEFAULT NULL,
    des_abreviada VARCHAR(10) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum ("Edicion", "Eliminacion"),
    KEY id_documento(id_documento),
    KEY tipo_documento(tipo_documento),
    KEY descripcion(des_abreviada),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los tipos de documentos';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_documentos;

-- lo recreamos
CREATE TRIGGER edicion_documentos
AFTER UPDATE ON tipo_documento
FOR EACH ROW
INSERT INTO auditoria_tipo_documento
       (id_documento,
        tipo_documento,
        des_abreviada,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_documento,
        OLD.tipo_documento,
        OLD.des_abreviada,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_documentos;

-- lo recreamos
CREATE TRIGGER eliminacion_documentos
AFTER DELETE ON tipo_documento
FOR EACH ROW
INSERT INTO auditoria_tipo_documento
       (id_documento,
        tipo_documento,
        des_abreviada,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_documento,
        OLD.tipo_documento,
        OLD.des_abreviada,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_documentos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_documentos AS
       SELECT diccionarios.tipo_documento.id_documento AS id,
              diccionarios.tipo_documento.tipo_documento AS tipodocumento,
              diccionarios.tipo_documento.des_abreviada AS descripcion,
              diccionarios.tipo_documento.id_usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diccionarios.tipo_documento.fecha, '%d/%m/%Y') AS fecha
       FROM diccionarios.tipo_documento INNER JOIN cce.responsables ON diccionarios.tipo_documento.id_usuario = cce.responsables.id;


/***************************************************************************/
/*                                                                         */
/*                            Estado Civil                                 */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de estados civiles
 id_estado entero pequeño(1) autonumerico clave del registro
 estado_civil varchar(50) descripcion del estado
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS estado_civil;

-- la recreamos desde cero
CREATE TABLE estado_civil (
    id_estado int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    estado_civil VARCHAR(30) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id_estado),
    KEY estado_civil(estado_civil),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con los estados civiles';

-- ahora cargamos los valores iniciales
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (1, "Soltero", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (2, "Casado", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (3, "Divorciado", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (4, "Separado", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (5, "Viudo", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (6, "Concubinato", 1);
INSERT INTO estado_civil (id_estado, estado_civil, id_usuario) VALUES (7, "Otro", 1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_estado_civil;

-- la recreamos
CREATE TABLE auditoria_estado_civil (
    id_estado int(1) UNSIGNED NOT NULL,
    estado_civil VARCHAR(30) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id_estado(id_estado),
    KEY estado_civil(estado_civil),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los estados civiles';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_estado_civil;

-- lo recreamos
CREATE TRIGGER edicon_estado_civil
AFTER UPDATE ON estado_civil
FOR EACH ROW
INSERT INTO auditoria_estado_civil
       (id_estado,
        estado_civil,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_estado,
        OLD.estado_civil,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_estado_civil;

-- lo recreamos
CREATE TRIGGER eliminacion_estado_civil
AFTER DELETE ON estado_civil
FOR EACH ROW
INSERT INTO auditoria_estado_civil
       (id_estado,
        estado_civil,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id_estado,
        OLD.estado_civil,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");

-- eliminamos el registro si existe
DROP VIEW IF EXISTS v_estados;

-- lo recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_estados AS
       SELECT diccionarios.estado_civil.id_estado AS id, 
              diccionarios.estado_civil.estado_civil AS estadocivil,
              diccionarios.estado_civil.id_usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diccionarios.estado_civil.fecha, '%d/%m/%Y') As fecha
       FROM diccionarios.estado_civil INNER JOIN cce.responsables ON diccionarios.estado_civil.id_usuario = cce.responsables.id;
