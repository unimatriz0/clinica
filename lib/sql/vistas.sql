/*

    Nombre: vistas.sql
    Fecha: 16/05/2019
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definición de las vistas utilizadas por el sistema
                 de clínica y de diagnóstico

*/


-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- seleccionamos
USE diagnostico;

/***********************************************************************************************/
/*                                                                                             */
/*                                Vistas del Sistema                                           */
/*                                                                                             */
/***********************************************************************************************/

/***********************************************************************************************/
/*                                                                                             */
/*                                Usuarios Autorizados                                         */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de usuarios si existe
DROP VIEW IF EXISTS v_usuarios;

-- la creamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_usuarios AS
    SELECT cce.responsables.id AS idusuario,
        UCASE(cce.responsables.nombre) AS nombreusuario,
        cce.responsables.password AS password,
        UCASE(cce.responsables.cargo) AS cargousuario,
        cce.responsables.institucion AS institucion,
        cce.responsables.telefono AS telefono,
        cce.responsables.e_mail AS e_mail,
        cce.responsables.direccion AS direccion,
        cce.responsables.codigo_postal AS codigo_postal,
        diagnostico.usuarios.laboratorio AS idlaboratorio,
        cce.laboratorios.nombre AS nombrelaboratorio,
        cce.responsables.usuario AS usuario,
        cce.responsables.coordenadas AS coordenadas,
        cce.responsables.activo AS activo,
        DATE_FORMAT(cce.responsables.fecha_alta, '%d/%d/%Y') AS fecha_alta,
        autorizo.usuario AS autorizo,
        diagnostico.usuarios.administrador AS administrador,
        cce.responsables.nivel_central AS nivelcentral,
        diagnostico.usuarios.personas AS personas,
        diagnostico.usuarios.congenito AS congenito,
        diagnostico.usuarios.resultados AS resultados,
        diagnostico.usuarios.muestras AS muestras,
        diagnostico.usuarios.entrevistas AS entrevistas,
        diagnostico.usuarios.auxiliares AS auxiliares,
        diagnostico.usuarios.protocolos AS protocolos,
        diagnostico.usuarios.stock AS stock,
        diagnostico.usuarios.usuarios AS usuarios,
        diagnostico.usuarios.clinica AS clinica,
        diccionarios.localidades.NOMLOC AS localidad,
        diccionarios.localidades.CODLOC AS codloc,
        diccionarios.provincias.NOM_PROV AS provincia,
        diccionarios.provincias.COD_PROV AS codprov,
        diccionarios.paises.NOMBRE AS pais,
        diccionarios.paises.ID AS idpais,
        diagnostico.usuarios.firma AS firma,
        cce.responsables.observaciones AS comentarios
    FROM cce.responsables LEFT JOIN diagnostico.usuarios ON cce.responsables.ID = diagnostico.usuarios.id
                          LEFT JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.ID
                          INNER JOIN diccionarios.localidades ON cce.responsables.LOCALIDAD = diccionarios.localidades.CODLOC
                          INNER JOIN diccionarios.provincias ON diccionarios.localidades.CODPCIA = diccionarios.provincias.COD_PROV
                          INNER JOIN diccionarios.paises ON diccionarios.provincias.PAIS = diccionarios.paises.ID
                          LEFT JOIN cce.responsables AS autorizo ON diagnostico.usuarios.autorizo = autorizo.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Marcas y Técnicas                                            */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de marcas y técnicas si existe
DROP VIEW IF EXISTS v_tecnicas;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tecnicas AS
    SELECT diagnostico.tecnicas.id AS idtecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombretecnica,
           diagnostico.marcas.id AS idmarca,
           diagnostico.marcas.marca AS marca
    FROM diagnostico.tecnicas INNER JOIN diagnostico.marcas ON diagnostico.tecnicas.id = diagnostico.marcas.tecnica
    ORDER BY diagnostico.tecnicas.tecnica ASC;


/***********************************************************************************************/
/*                                                                                             */
/*                                      Inventario                                             */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de inventario si existe
DROP VIEW IF EXISTS v_inventario;

-- creamos la vista desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_inventario AS
    SELECT diagnostico.items.id AS iditem,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.items.critico AS critico,
           diagnostico.inventario.existencia AS existencia,
           diagnostico.inventario.valor AS importe,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS idlaboratorio
    FROM diagnostico.items INNER JOIN diagnostico.inventario ON diagnostico.items.id = diagnostico.inventario.id
                           INNER JOIN cce.laboratorios ON diagnostico.inventario.id_laboratorio = cce.laboratorios.id
    ORDER BY diagnostico.items.descripcion ASC;


/***********************************************************************************************/
/*                                                                                             */
/*                                Ingresos al Depósito                                         */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de ingresos si existe
DROP VIEW IF EXISTS v_ingresos;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_ingresos AS
    SELECT diagnostico.items.id AS iditem,
        diagnostico.items.descripcion AS descripcion,
        diagnostico.items.imagen AS imagen,
        diagnostico.items.codigosop AS codigosop,
        diagnostico.ingresos.id AS idingreso,
        diagnostico.ingresos.importe AS importe,
        diagnostico.ingresos.id_laboratorio AS idlaboratorio,
        diagnostico.ingresos.id_financiamiento AS id_financiamiento,
        diagnostico.financiamiento.fuente AS financiamiento,
        cce.laboratorios.nombre AS laboratorio,
        diagnostico.ingresos.remito AS remito,
        diagnostico.ingresos.cantidad AS cantidad,
        DATE_FORMAT(diagnostico.ingresos.vencimiento, '%d/%m/%Y') AS vencimiento,
        DATE_FORMAT(diagnostico.ingresos.fecha, '%d/%m/%Y') AS fecha_ingreso,
        diagnostico.ingresos.lote AS lote,
        diagnostico.ingresos.ubicacion AS ubicacion,
        cce.responsables.usuario AS usuario,
        cce.responsables.nombre AS nombre_usuario,
        cce.responsables.id AS idusuario,
        diagnostico.ingresos.comentarios AS comentarios
    FROM diagnostico.ingresos INNER JOIN diagnostico.items ON diagnostico.ingresos.item = diagnostico.items.id
                              INNER JOIN cce.laboratorios ON diagnostico.ingresos.id_laboratorio = cce.laboratorios.id
                              INNER JOIN cce.responsables ON diagnostico.ingresos.id_usuario = cce.responsables.id
                              INNER JOIN diagnostico.financiamiento ON diagnostico.ingresos.id_financiamiento = diagnostico.financiamiento.id
    ORDER BY diagnostico.items.descripcion ASC,
             diagnostico.ingresos.fecha ASC;


/***********************************************************************************************/
/*                                                                                             */
/*                                Salidas del Depósito                                         */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de egresos si existe
DROP VIEW IF EXISTS v_egresos;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_egresos AS
    SELECT diagnostico.items.id AS iditem,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.egresos.id AS idegreso,
           diagnostico.egresos.id_laboratorio AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.egresos.remito AS remito,
           diagnostico.egresos.cantidad AS cantidad,
           DATE_FORMAT(diagnostico.egresos.fecha, '%d/%m/%Y') AS fecha_egreso,
           cce.responsables.usuario AS entrego,
           cce.responsables.nombre AS nombre_entrego,
           cce.responsables.id AS id_entrego,
           recepcion.usuario AS recibio,
           recepcion.nombre AS nombre_recibio,
           recepcion.id AS idrecibio,
           diagnostico.egresos.comentarios AS comentarios
    FROM diagnostico.egresos INNER JOIN diagnostico.items ON diagnostico.egresos.item = diagnostico.items.id
                             INNER JOIN cce.laboratorios ON diagnostico.egresos.id_laboratorio = cce.laboratorios.id
                             INNER JOIN cce.responsables ON diagnostico.egresos.id_entrego = cce.responsables.id
                             INNER JOIN cce.responsables AS recepcion ON diagnostico.egresos.id_recibio = recepcion.id
    ORDER BY diagnostico.items.descripcion ASC,
             diagnostico.egresos.fecha ASC;


/***********************************************************************************************/
/*                                                                                             */
/*                                Jurisdicciones                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de jurisdicciones
DROP VIEW IF EXISTS v_jurisdicciones;

-- la recreamos desde cero
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_jurisdicciones AS
    SELECT diccionarios.paises.id AS idpais,
           diccionarios.paises.nombre AS pais,
           diccionarios.provincias.cod_prov AS codprovincia,
           diccionarios.provincias.nom_prov AS provincia,
           diccionarios.provincias.poblacion AS poblacionprovincia,
           diccionarios.localidades.codloc AS codlocalidad,
           diccionarios.localidades.nomloc AS localidad,
           diccionarios.localidades.poblacion AS poblacionlocalidad
    FROM diccionarios.paises INNER JOIN diccionarios.provincias ON diccionarios.paises.id = diccionarios.provincias.pais
                INNER JOIN diccionarios.localidades ON diccionarios.provincias.cod_prov = diccionarios.localidades.codpcia
    ORDER BY diccionarios.paises.nombre,
             diccionarios.provincias.nom_prov,
             diccionarios.localidades.nomloc;


/***********************************************************************************************/
/*                                                                                             */
/*                                Nómina de Artículos                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de items si existe
DROP VIEW IF EXISTS v_items;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_items AS
    SELECT diagnostico.items.id AS id_item,
           diagnostico.items.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.valor AS valor,
           diagnostico.items.imagen AS imagen,
           diagnostico.items.critico AS critico,
           DATE_FORMAT(diagnostico.items.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.items.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario
    FROM diagnostico.items INNER JOIN cce.responsables ON diagnostico.items.id_usuario = cce.responsables.id
                           INNER JOIN cce.laboratorios ON diagnostico.items.id_laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Fuentes de Financiamiento                                     */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de fuentes de financiamiento
DROP VIEW IF EXISTS v_financiamiento;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_financiamiento AS
    SELECT diagnostico.financiamiento.id AS id_financiamiento,
           diagnostico.financiamiento.fuente AS fuente,
           diagnostico.financiamiento.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.financiamiento.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.financiamiento.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.financiamiento INNER JOIN cce.laboratorios ON diagnostico.financiamiento.id_laboratorio = cce.laboratorios.id
                                    INNER JOIN cce.responsables ON diagnostico.financiamiento.id_usuario = cce.responsables.id
    ORDER BY diagnostico.financiamiento.fuente;


/***********************************************************************************************/
/*                                                                                             */
/*                                Pedidos de Materiales                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de pedidos si existe
DROP VIEW IF EXISTS v_pedidos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_pedidos AS
    SELECT diagnostico.pedidos.id AS id_pedido,
           diagnostico.pedidos.id_item AS id_item,
           diagnostico.items.descripcion AS descripcion,
           diagnostico.items.codigosop AS codigosop,
           diagnostico.items.imagen AS imagen,
           diagnostico.pedidos.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           diagnostico.pedidos.cantidad AS cantidad,
           diagnostico.inventario.existencia AS existencia,
           cce.laboratorios.id AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           DATE_FORMAT(diagnostico.pedidos.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.pedidos INNER JOIN diagnostico.items ON diagnostico.pedidos.id_item = diagnostico.items.id
                             INNER JOIN cce.responsables ON diagnostico.pedidos.id_usuario = cce.responsables.id
                             INNER JOIN diagnostico.inventario ON diagnostico.pedidos.id_item = diagnostico.inventario.item
                             INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                             INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id
    ORDER BY diagnostico.pedidos.fecha_alta;


/***********************************************************************************************/
/*                                                                                             */
/*                                      Laboratorios                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de laboratorios si existe
DROP VIEW IF EXISTS v_laboratorios;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_laboratorios AS
    SELECT cce.laboratorios.id AS id,
        UPPER(cce.laboratorios.nombre) AS laboratorio,
        UPPER(cce.laboratorios.responsable) AS responsable,
        cce.laboratorios.pais AS idpais,
        diccionarios.paises.nombre AS pais,
        cce.laboratorios.localidad AS codloc,
        diccionarios.localidades.nomloc AS localidad,
        diccionarios.provincias.nom_prov AS provincia,
        diccionarios.provincias.cod_prov AS codprov,
        cce.laboratorios.direccion AS direccion,
        cce.laboratorios.codigo_postal AS codigo_postal,
        cce.laboratorios.coordenadas AS coordenadas,
        cce.laboratorios.dependencia AS iddependencia,
        diccionarios.dependencias.dependencia AS dependencia,
        cce.laboratorios.e_mail AS email,
        DATE_FORMAT(cce.laboratorios.fecha_alta, '%d/%m/%Y') AS fecha_alta,
        cce.laboratorios.activo AS activo,
        cce.laboratorios.recibe_muestras_chagas AS recibe_muestras,
        cce.laboratorios.id_recibe AS id_recibe,
        usuarios.usuario AS nombre_recibe,
        cce.laboratorios.observaciones AS observaciones,
        cce.laboratorios.logo AS logo,
        cce.laboratorios.usuario AS id_usuario,
        cce.responsables.usuario AS usuario
    FROM cce.laboratorios INNER JOIN diccionarios.paises ON cce.laboratorios.pais = diccionarios.paises.id
                          INNER JOIN diccionarios.localidades ON cce.laboratorios.localidad = diccionarios.localidades.codloc
                          INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                          LEFT JOIN cce.responsables AS usuarios ON cce.laboratorios.id_recibe = usuarios.id
                          INNER JOIN diccionarios.dependencias ON cce.laboratorios.dependencia = diccionarios.dependencias.id_dependencia
                          INNER JOIN cce.responsables ON cce.laboratorios.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Entrevistas                                             */
/*                                                                                             */
/***********************************************************************************************/

-- elimina la vista de entrevistas si existe
DROP VIEW IF EXISTS v_entrevistas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_entrevistas AS
    SELECT diagnostico.entrevistas.id_entrevista AS id_entrevista,
           diagnostico.entrevistas.id_protocolo AS id_protocolo,
           diagnostico.entrevistas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.entrevistas.resultado AS resultado,
           diagnostico.entrevistas.actitud AS actitud,
           diagnostico.entrevistas.concurrio AS concurrio,
           diagnostico.entrevistas.tipo AS tipo_entrevista,
           DATE_FORMAT(diagnostico.entrevistas.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.entrevistas.comentarios AS comentarios,
           diagnostico.entrevistas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario
    FROM diagnostico.entrevistas INNER JOIN cce.responsables ON diagnostico.entrevistas.id_usuario = cce.responsables.id
                                 INNER JOIN cce.laboratorios ON diagnostico.entrevistas.id_laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Chagas Congénito                                             */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la consulta de congenito
DROP VIEW IF EXISTS v_congenito;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_congenito AS
    SELECT diagnostico.congenito.id AS id_congenito,
           diagnostico.congenito.id_protocolo AS id_protocolo,
           diagnostico.congenito.id_madre AS id_madre,
           diagnostico.congenito.id_sivila AS id_sivila,
           DATE_FORMAT(diagnostico.congenito.reportado, '%d/%m/%Y') AS reportado,
           diagnostico.congenito.parto AS parto,
           diagnostico.congenito.peso AS peso,
           diagnostico.congenito.prematuro AS prematuro,
           diagnostico.congenito.institucion AS id_institucion,
           diagnostico.centros_asistenciales.nombre AS institucion,
           diagnostico.congenito.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.congenito.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.congenito.comentarios AS comentarios
    FROM diagnostico.congenito INNER JOIN cce.responsables ON diagnostico.congenito.id_usuario = cce.responsables.id
                               INNER JOIN diagnostico.centros_asistenciales ON diagnostico.congenito.institucion = diagnostico.centros_asistenciales.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                         Pacientes                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de pacientes
DROP VIEW IF EXISTS v_personas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_personas AS
    SELECT diagnostico.personas.protocolo AS protocolo,
           diagnostico.personas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.personas.historia_clinica AS historia_clinica,
           UPPER(diagnostico.personas.apellido) AS apellido,
           UPPER(diagnostico.personas.nombre) AS nombre,
           diagnostico.personas.documento AS documento,
           diagnostico.personas.tipo_documento AS id_documento,
           diccionarios.tipo_documento.des_abreviada AS tipo_documento,
           DATE_FORMAT(diagnostico.personas.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento,
           diagnostico.personas.edad AS edad,
           diagnostico.personas.sexo AS id_sexo,
           diccionarios.sexos.sexo AS sexo,
           diagnostico.personas.estado_civil AS id_estado_civil,
           diccionarios.estado_civil.estado_civil AS estado_civil,
           diagnostico.personas.hijos AS hijos,
           UPPER(diagnostico.personas.direccion) AS direccion,
           diagnostico.personas.telefono AS telefono,
           diagnostico.personas.celular AS celular,
           diagnostico.personas.compania AS id_compania,
           diccionarios.celulares.compania AS compania,
           diagnostico.personas.mail AS mail,
           diagnostico.personas.localidad_nacimiento AS id_localidad_nacimiento,
           localidad_nacimiento.nomloc AS localidad_nacimiento,
           jurisdiccion_nacimiento.cod_prov AS id_jurisdiccion_nacimiento,
           jurisdiccion_nacimiento.nom_prov AS jurisdiccion_nacimiento,
           pais_nacimiento.id AS id_pais_nacimiento,
           pais_nacimiento.nombre AS pais_nacimiento,
           diagnostico.personas.coordenadas_nacimiento AS coordenadas_nacimiento,
           diagnostico.personas.localidad_residencia AS id_localidad_residencia,
           localidad_residencia.nomloc AS localidad_residencia,
           jurisdiccion_residencia.cod_prov AS id_jurisdiccion_residencia,
           jurisdiccion_residencia.nom_prov AS jurisdiccion_residencia,
           pais_residencia.id AS id_pais_residencia,
           pais_residencia.nombre AS pais_residencia,
           diagnostico.personas.coordenadas_residencia AS coordenadas_residencia,
           diagnostico.personas.localidad_madre AS id_localidad_madre,
           localidad_madre.nomloc AS localidad_madre,
           jurisdiccion_madre.cod_prov AS id_jurisdiccion_madre,
           jurisdiccion_madre.nom_prov AS jurisdiccion_madre,
           pais_madre.id AS id_pais_madre,
           pais_madre.nombre AS pais_madre,
           diagnostico.personas.madre_positiva AS madre_positiva,
           diagnostico.personas.ocupacion AS ocupacion,
           diagnostico.personas.obra_social AS obra_social,
           diagnostico.personas.motivo AS id_motivo,
           diagnostico.motivos.motivo AS motivo,
           diagnostico.personas.derivacion AS id_derivacion,
           diagnostico.derivacion.derivacion AS derivacion,
           diagnostico.personas.tratamiento AS tratamiento,
           diagnostico.personas.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.personas.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.personas.comentarios AS comentarios
    FROM diagnostico.personas INNER JOIN cce.responsables ON diagnostico.personas.usuario = cce.responsables.id
                              INNER JOIN diccionarios.localidades AS localidad_nacimiento ON diagnostico.personas.localidad_nacimiento = localidad_nacimiento.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_nacimiento ON localidad_nacimiento.codpcia = jurisdiccion_nacimiento.cod_prov
                              INNER JOIN diccionarios.paises AS pais_nacimiento ON jurisdiccion_nacimiento.pais = pais_nacimiento.id
                              INNER JOIN diccionarios.localidades AS localidad_residencia ON diagnostico.personas.localidad_residencia = localidad_residencia.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_residencia ON localidad_residencia.codpcia = jurisdiccion_residencia.cod_prov
                              INNER JOIN diccionarios.paises AS pais_residencia ON jurisdiccion_residencia.pais = pais_residencia.id
                              INNER JOIN diccionarios.localidades AS localidad_madre ON diagnostico.personas.localidad_madre = localidad_madre.codloc
                              INNER JOIN diccionarios.provincias AS jurisdiccion_madre ON localidad_madre.codpcia = jurisdiccion_madre.cod_prov
                              INNER JOIN diccionarios.paises AS pais_madre ON jurisdiccion_madre.pais = pais_madre.id
                              INNER JOIN cce.laboratorios ON diagnostico.personas.id_laboratorio = cce.laboratorios.id
                              INNER JOIN diccionarios.tipo_documento ON diagnostico.personas.tipo_documento = diccionarios.tipo_documento.id_documento
                              INNER JOIN diccionarios.sexos ON diagnostico.personas.sexo = diccionarios.sexos.id
                              INNER JOIN diccionarios.estado_civil ON diagnostico.personas.estado_civil = diccionarios.estado_civil.id_estado
                              INNER JOIN diccionarios.celulares ON diagnostico.personas.compania = diccionarios.celulares.id
                              INNER JOIN diagnostico.motivos ON diagnostico.personas.motivo = diagnostico.motivos.id_motivo
                              INNER JOIN diagnostico.derivacion ON diagnostico.personas.derivacion = diagnostico.derivacion.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Marcas de Reactivos                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de marcas si existe
DROP VIEW IF EXISTS v_marcas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_marcas AS
    SELECT diagnostico.marcas.id AS id_marca,
           diagnostico.marcas.marca AS marca,
           diagnostico.marcas.tecnica AS id_tecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.marcas.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.marcas.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.marcas INNER JOIN diagnostico.tecnicas ON diagnostico.marcas.tecnica = diagnostico.tecnicas.id
                            INNER JOIN cce.responsables ON diagnostico.marcas.usuario = cce.responsables.id;

/***********************************************************************************************/
/*                                                                                             */
/*                             Transplantes del Paciente                                       */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de transplantes
DROP VIEW IF EXISTS v_transplantes;

-- la recreamos incluyendo el laboratorio y la clave para luego
-- poder determinar que laboratorio ingresó el registro (y por
-- tanto, puede eliminarlo)
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_transplantes AS
    SELECT diagnostico.transplantes.id AS id_transplante,
           diagnostico.transplantes.id_protocolo AS protocolo,
           diagnostico.transplantes.organo AS id_organo,
           diagnostico.organos.organo AS organo,
           diagnostico.transplantes.positivo AS positivo,
           DATE_FORMAT(diagnostico.transplantes.fecha_transplante, '%d/%m/%Y') AS fecha_transplante,
           diagnostico.transplantes.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS id_laboratorio,
           DATE_FORMAT(diagnostico.transplantes.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.transplantes INNER JOIN cce.responsables ON diagnostico.transplantes.id_usuario = cce.responsables.id
                                  INNER JOIN diagnostico.organos ON diagnostico.transplantes.organo = diagnostico.organos.id
                                  INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                                  INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Transfusiones Recibidas                                       */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de transfusiones
DROP VIEW IF EXISTS v_transfusiones;

-- la recreamos (agregamos la clave y el nombre del laboratorio
-- del usuario para identificar quien denunció la transfusión)
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_transfusiones AS
    SELECT diagnostico.transfusiones.id AS id_transfusion,
           diagnostico.transfusiones.id_protocolo AS id_protocolo,
           DATE_FORMAT(diagnostico.transfusiones.fecha_transfusion, '%d/%m/%Y') AS fecha_transfusion,
           diagnostico.transfusiones.motivo AS motivo,
           diagnostico.transfusiones.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.id AS id_laboratorio,
           DATE_FORMAT(diagnostico.transfusiones.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.transfusiones INNER JOIN cce.responsables ON diagnostico.transfusiones.id_usuario = cce.responsables.id
                                   INNER JOIN diagnostico.usuarios ON cce.responsables.id = diagnostico.usuarios.id
                                   INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Centros Asistenciales                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de centros asistenciales
DROP VIEW IF EXISTS v_instituciones;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_instituciones AS
    SELECT diagnostico.centros_asistenciales.id AS id_centro,
           UPPER(diagnostico.centros_asistenciales.nombre) AS nombre_centro,
           diagnostico.centros_asistenciales.localidad AS cod_loc,
           diccionarios.localidades.nomloc AS localidad,
           diccionarios.provincias.cod_prov AS cod_prov,
           diccionarios.provincias.nom_prov AS provincia,
           diccionarios.paises.id AS id_pais,
           diccionarios.paises.nombre AS pais,
           diagnostico.centros_asistenciales.direccion AS direccion,
           diagnostico.centros_asistenciales.codigo_postal AS codigo_postal,
           diagnostico.centros_asistenciales.telefono AS telefono,
           diagnostico.centros_asistenciales.mail AS mail,
           UPPER(diagnostico.centros_asistenciales.responsable) AS responsable,
           diagnostico.centros_asistenciales.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           diagnostico.centros_asistenciales.dependencia AS id_dependencia,
           diccionarios.dependencias.dependencia AS dependencia,
           DATE_FORMAT(diagnostico.centros_asistenciales.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.centros_asistenciales.comentarios AS comentarios,
           diagnostico.centros_asistenciales.coordenadas AS coordenadas
    FROM diagnostico.centros_asistenciales INNER JOIN cce.responsables ON diagnostico.centros_asistenciales.id_usuario = cce.responsables.id
                                           INNER JOIN diccionarios.localidades ON diagnostico.centros_asistenciales.localidad = diccionarios.localidades.codloc
                                           INNER JOIN diccionarios.provincias ON diccionarios.localidades.codpcia = diccionarios.provincias.cod_prov
                                           INNER JOIN diccionarios.paises ON diccionarios.provincias.pais = diccionarios.provincias.id
                                           INNER JOIN diccionarios.dependencias ON diagnostico.centros_asistenciales.dependencia = diccionarios.dependencias.id_dependencia;


/***********************************************************************************************/
/*                                                                                             */
/*                                Técnicas Diagnósticas                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de técnicas
DROP VIEW IF EXISTS v_tecnicas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tecnicas AS
    SELECT diagnostico.tecnicas.id AS id_tecnica,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.tecnicas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.tecnicas.fecha, '%d/%m/%Y') AS fecha
    FROM diagnostico.tecnicas INNER JOIN cce.responsables ON diagnostico.tecnicas.id_usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                    Valores Aceptados                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de valores
DROP VIEW IF EXISTS v_valores;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_valores AS
    SELECT diagnostico.valores_tecnicas.id AS id_valor,
           diagnostico.valores_tecnicas.id_tecnica AS id_tecnica,
           diagnostico.valores_tecnicas.valor AS valor,
           diagnostico.tecnicas.tecnica AS tecnica,
           diagnostico.tecnicas.nombre AS nombre,
           diagnostico.valores_tecnicas.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.valores_tecnicas.fecha, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.valores_tecnicas INNER JOIN diagnostico.tecnicas ON diagnostico.valores_tecnicas.id_tecnica = diagnostico.tecnicas.id
                                      INNER JOIN cce.responsables ON diagnostico.valores_tecnicas.id_usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                             Diccionario de Enfermedades                                     */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista del diccionario de enfermedades
DROP VIEW IF EXISTS v_enfermedades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_enfermedades AS
    SELECT diagnostico.enfermedades.id AS id_enfermedad,
           diagnostico.enfermedades.enfermedad AS enfermedad,
           diagnostico.enfermedades.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.enfermedades.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.enfermedades INNER JOIN cce.responsables ON diagnostico.enfermedades.id_usuario = cce.responsables.id
    ORDER BY diagnostico.enfermedades.enfermedad;


/***********************************************************************************************/
/*                                                                                             */
/*                              Enfermedades del Paciente                                      */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de otras enfermedades
DROP VIEW IF EXISTS v_otras_enfermedades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_otras_enfermedades AS
    SELECT diagnostico.otras_enfermedades.id AS id_registro,
           diagnostico.otras_enfermedades.id_protocolo AS protocolo,
           diagnostico.personas.historia_clinica AS historia_clinica,
           diagnostico.personas.id_laboratorio AS id_laboratorio,
           cce.laboratorios.nombre AS laboratorio,
           CONCAT(diagnostico.personas.apellido, ', ', diagnostico.personas.nombre) AS nombre,
           diccionarios.tipo_documento.des_abreviada AS tipo_documento,
           diagnostico.personas.documento AS documento,
           DATE_FORMAT(diagnostico.personas.fecha_nacimiento, '%d/%m/%Y') AS fecha_nacimiento,
           diagnostico.personas.madre_positiva AS madre_positiva,
           diagnostico.personas.tratamiento AS tratamiento,
           diagnostico.otras_enfermedades.id_enfermedad AS id_enfermedad,
           diagnostico.enfermedades.enfermedad AS enfermedad,
           diagnostico.otras_enfermedades.id_usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.otras_enfermedades.fecha, '%d/%m/%Y') AS fecha,
           DATE_FORMAT(diagnostico.otras_enfermedades.fecha_alta, '%d/%m/%Y') AS fecha_alta
    FROM diagnostico.otras_enfermedades INNER JOIN cce.responsables ON diagnostico.otras_enfermedades.id_usuario = cce.responsables.id
                                        INNER JOIN diagnostico.enfermedades ON diagnostico.otras_enfermedades.id_enfermedad = diagnostico.enfermedades.id
                                        INNER JOIN diagnostico.personas ON diagnostico.otras_enfermedades.id_protocolo = diagnostico.personas.protocolo
                                        INNER JOIN diccionarios.tipo_documento ON diagnostico.personas.tipo_documento = diccionarios.tipo_documento.id_documento
                                        INNER JOIN cce.laboratorios ON diagnostico.personas.id_laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     No Conformidades                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de no conformidades
DROP VIEW IF EXISTS v_conformidades;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_conformidades AS
    SELECT diagnostico.noconformidad.id AS id,
           diagnostico.noconformidad.usuario AS idusuario,
           cce.responsables.usuario AS usuario,
           cce.responsables.nombre AS nombre,
           cce.laboratorios.id AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           cce.laboratorios.responsable AS jefelaboratorio,
           cce.laboratorios.logo AS logo,
           DATE_FORMAT(diagnostico.noconformidad.fecha, '%d/%m/%Y') AS fecha,
           diagnostico.noconformidad.titulo AS titulo,
           diagnostico.noconformidad.detalle AS detalle,
           diagnostico.noconformidad.causa AS causa,
           DATE_FORMAT(diagnostico.noconformidad.respuesta, '%d/%m/%Y') AS respuesta,
           diagnostico.noconformidad.accion AS accion,
           DATE_FORMAT(diagnostico.noconformidad.ejecucion, '%d/%m/%Y') AS ejecucion
    FROM diagnostico.noconformidad INNER JOIN cce.responsables ON diagnostico.noconformidad.usuario = cce.responsables.id
                                   INNER JOIN diagnostico.usuarios ON diagnostico.noconformidad.usuario = diagnostico.usuarios.id
                                   INNER JOIN cce.laboratorios ON diagnostico.usuarios.laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Heladeras y Temperaturas                                      */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de temperaturas y heladeras
DROP VIEW IF EXISTS v_heladeras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_heladeras AS
    SELECT diagnostico.heladeras.id AS id_heladera,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.heladeras.laboratorio AS idlaboratorio,
           UCASE(diagnostico.heladeras.marca) AS marca,
           diagnostico.heladeras.ubicacion AS ubicacion,
           diagnostico.heladeras.patrimonio AS patrimonio,
           diagnostico.heladeras.temperatura AS temperatura,
           diagnostico.heladeras.temperatura2 AS temperatura2,
           diagnostico.heladeras.tolerancia AS tolerancia,
           diagnostico.heladeras.usuario AS id_usuario,
           cce.responsables.usuario AS usuario,
           DATE_FORMAT(diagnostico.heladeras.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.temperaturas.id AS id_registro,
           DATE_FORMAT(diagnostico.temperaturas.fecha, '%d/%m/%Y') AS fecha_lectura,
           DATE_FORMAT(diagnostico.temperaturas.fecha, '%H:%i') AS hora_lectura,
           diagnostico.temperaturas.temperatura AS valor_lectura,
           diagnostico.temperaturas.temperatura2 AS valor_lectura2,
           diagnostico.temperaturas.controlado AS id_controlado,
           controladores.usuario AS controlado
    FROM diagnostico.heladeras INNER JOIN cce.responsables ON diagnostico.heladeras.usuario = cce.responsables.id
                               LEFT JOIN cce.responsables AS controladores ON controladores.id = cce.responsables.id
                               LEFT JOIN diagnostico.temperaturas ON diagnostico.heladeras.id = diagnostico.temperaturas.heladera
                               INNER JOIN cce.laboratorios ON diagnostico.heladeras.laboratorio = cce.laboratorios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Muestras Tomadas                                             */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de muestras si existe
DROP VIEW IF EXISTS v_muestras;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_muestras AS
    SELECT diagnostico.muestras.id_muestra AS idmuestra,
           diagnostico.muestras.id_protocolo AS id_protocolo,
           diagnostico.personas.protocolo AS protocolo,
           diagnostico.muestras.id_laboratorio AS idlaboratorio,
           cce.laboratorios.nombre AS laboratorio,
           diagnostico.muestras.id_genero AS idgenero,
           cce.responsables.usuario AS genero,
           DATE_FORMAT(diagnostico.muestras.fecha_alta, '%d/%m/%Y') AS fecha_alta,
           diagnostico.muestras.id_tomo AS idtomo,
           extraccion.usuario AS tomo,
           DATE_FORMAT(diagnostico.muestras.fecha_toma, '%d/%m/%Y') AS fecha_toma,
           CONCAT(diagnostico.personas.apellido, ", ", diagnostico.personas.nombre) AS nombre,
           diagnostico.personas.documento AS documento,
           diccionarios.sexos.sexo AS sexo,
           diagnostico.muestras.comentarios AS comentarios
    FROM diagnostico.muestras INNER JOIN diagnostico.personas ON diagnostico.muestras.id_protocolo = diagnostico.personas.protocolo
                              INNER JOIN cce.laboratorios ON diagnostico.muestras.id_laboratorio = cce.laboratorios.id
                              INNER JOIN cce.responsables ON diagnostico.muestras.id_genero = cce.responsables.id
                              INNER JOIN diccionarios.sexos ON diagnostico.personas.sexo = diccionarios.sexos.id
                              LEFT JOIN cce.responsables AS extraccion ON diagnostico.muestras.id_tomo = extraccion.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Tipos de Derivacióm                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_derivacion;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_derivacion AS
       SELECT diagnostico.derivacion.id AS id,
              diagnostico.derivacion.derivacion AS derivacion,
              diagnostico.derivacion.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.derivacion.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.derivacion INNER JOIN cce.responsables ON diagnostico.derivacion.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Chagas Agudo                                            */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_agudo;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_agudo AS
       SELECT diagnostico.agudo.id AS id,
              diagnostico.agudo.paciente AS paciente,
              diagnostico.agudo.sintomas AS sintomas,
              diagnostico.agudo.usuario AS idusuario,
              diagnostico.agudo.observaciones AS observaciones,
              cce.responsables.usuario As usuario,
              DATE_FORMAT(diagnostico.agudo.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.agudo INNER JOIN cce.responsables ON diagnostico.agudo.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Diccionario de Drogas                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_drogas;

-- creamos la vista
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_drogas AS
       SELECT diagnostico.drogas.id AS id,
              diagnostico.drogas.droga AS droga,
              diagnostico.drogas.dosis AS dosis,
              diagnostico.drogas.comercial AS comercial,
              diagnostico.drogas.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.drogas.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.drogas INNER JOIN cce.responsables ON diagnostico.drogas.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Efectos Adversos                                        */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_adversos;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_adversos AS
       SELECT diagnostico.adversos.id AS id,
              diagnostico.adversos.descripcion AS descripcion,
              diagnostico.adversos.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.adversos.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.adversos INNER JOIN cce.responsables ON diagnostico.adversos.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                Tratamiento Recibido                                         */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_tratamiento;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_tratamiento AS
       SELECT diagnostico.tratamiento.id AS id,
              diagnostico.tratamiento.paciente AS paciente,
              diagnostico.drogas.id AS iddroga,
              diagnostico.drogas.droga AS droga,
              diagnostico.drogas.dosis AS dosis,
              diagnostico.drogas.comercial AS comercial,
              diagnostico.tratamiento.comprimidos AS comprimidos,
              DATE_FORMAT(diagnostico.tratamiento.inicio, '%d/%m/%Y') AS inicio,
              DATE_FORMAT(diagnostico.tratamiento.fin, '%d/%m/%Y') AS fin,
              diagnostico.tratamiento.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.tratamiento.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.tratamiento INNER JOIN cce.responsables ON diagnostico.tratamiento.usuario = cce.responsables.id
                                    LEFT JOIN diagnostico.drogas ON diagnostico.tratamiento.droga = diagnostico.drogas.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Antecedentes Tóxicos                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_antecedentes;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_antecedentes AS
       SELECT diagnostico.antecedentes.id AS id,
              diagnostico.antecedentes.paciente AS paciente,
              diagnostico.antecedentes.fuma AS fuma,
              diagnostico.antecedentes.alcohol AS alcohol,
              diagnostico.antecedentes.bebida AS bebida,
              diagnostico.antecedentes.adicciones AS adicciones,
              diagnostico.antecedentes.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.antecedentes.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.antecedentes INNER JOIN cce.responsables ON diagnostico.antecedentes.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Antecedentes Familiares                                       */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_familiares;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_familiares AS
       SELECT diagnostico.familiares.id AS id,
              diagnostico.familiares.paciente AS paciente,
              diagnostico.familiares.subita As subita,
              diagnostico.familiares.cardiopatia As cardiopatia,
              diagnostico.familiares.disfagia AS disfagia,
              diagnostico.familiares.nosabe AS nosabe,
              diagnostico.familiares.notiene AS notiene,
              diagnostico.familiares.otra AS otra,
              diagnostico.familiares.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.familiares.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.familiares INNER JOIN cce.responsables ON diagnostico.familiares.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Síntomas del Paciente                                         */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_sintomas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_sintomas AS
       SELECT diagnostico.sintomas.id AS id,
              diagnostico.sintomas.paciente AS paciente,
              diagnostico.sintomas.visita AS idvisita,
              DATE_FORMAT(diagnostico.visitas.fecha, '%d/%m/%Y') AS fechavisita,
              diagnostico.sintomas.disnea AS disnea,
              diagnostico.sintomas.palpitaciones AS palpitaciones,
              diagnostico.sintomas.precordial AS precordial,
              diagnostico.sintomas.conciencia AS conciencia,
              diagnostico.sintomas.presincope AS presincope,
              diagnostico.sintomas.edema AS edema,
              diagnostico.sintomas.observaciones AS observaciones,
              diagnostico.sintomas.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.sintomas.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.sintomas INNER JOIN cce.responsables ON diagnostico.sintomas.usuario = cce.responsables.id
                                 INNER JOIN diagnostico.visitas ON diagnostico.sintomas.visita = diagnostico.visitas.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Disautonomía                                                  */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_disautonomia;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_disautonomia AS
       SELECT diagnostico.disautonomia.id AS id,
              diagnostico.disautonomia.paciente AS paciente,
              diagnostico.disautonomia.hipotension AS hipotension,
              diagnostico.disautonomia.bradicardia AS bradicardia,
              diagnostico.disautonomia.astenia AS astenia,
              diagnostico.disautonomia.notiene AS notiene,
              diagnostico.disautonomia.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.disautonomia.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.disautonomia INNER JOIN cce.responsables ON diagnostico.disautonomia.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                               Compromiso Digestivo                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_digestivo;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_digestivo AS
       SELECT diagnostico.digestivo.id AS id,
              diagnostico.digestivo.paciente AS paciente,
              diagnostico.digestivo.disfagia AS disfagia,
              diagnostico.digestivo.pirosis AS pirosis,
              diagnostico.digestivo.regurgitacion AS regurgitacion,
              diagnostico.digestivo.constipacion AS constipacion,
              diagnostico.digestivo.bolo AS bolo,
              diagnostico.digestivo.notiene AS notiene,
              diagnostico.digestivo.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.digestivo.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.digestivo INNER JOIN cce.responsables ON diagnostico.digestivo.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                   Cardiovasculares                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_cardiovascular;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_cardiovascular AS
       SELECT diagnostico.cardiovascular.id AS id,
              diagnostico.cardiovascular.paciente AS paciente,
              diagnostico.cardiovascular.visita AS idvisita,
              DATE_FORMAT(diagnostico.visitas.fecha, '%d/%m/%Y') AS fechavisita,
              diagnostico.cardiovascular.ausnormal AS ausnormal,
              diagnostico.cardiovascular.irregular AS irregular,
              diagnostico.cardiovascular.3er AS 3er,
              diagnostico.cardiovascular.4to AS 4to,
              diagnostico.cardiovascular.eyectivo AS eyectivo,
              diagnostico.cardiovascular.regurgitativo AS regurgitativo,
              diagnostico.cardiovascular.sinsistolico AS sinsistolico,
              diagnostico.cardiovascular.aortico AS aortico,
              diagnostico.cardiovascular.diastolicomitral AS diastolicomitral,
              diagnostico.cardiovascular.sindiastolico As sindiastolico,
              diagnostico.cardiovascular.hepatomegalia AS hepatomegalia,
              diagnostico.cardiovascular.esplenomegalia AS esplenomegalia,
              diagnostico.cardiovascular.ingurgitacion AS ingurgitacion,
              diagnostico.cardiovascular.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.cardiovascular.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.cardiovascular INNER JOIN cce.responsables ON diagnostico.cardiovascular.usuario = cce.responsables.id
                                       INNER JOIN diagnostico.visitas ON diagnostico.cardiovascular.visita = diagnostico.visitas.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                      Exámen Físico                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista de examen físico
DROP VIEW IF EXISTS v_fisico;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_fisico AS
       SELECT diagnostico.fisico.id AS id,
              diagnostico.fisico.protocolo AS protocolo,
              diagnostico.fisico.visita AS idvisita,
              DATE_FORMAT(diagnostico.visitas.fecha, '%d/%m/%Y') AS fechavisita,
              diagnostico.fisico.ta AS ta,
              diagnostico.fisico.peso AS peso,
              diagnostico.fisico.fc AS fc,
              diagnostico.fisico.spo AS spo,
              diagnostico.fisico.talla AS talla,
              diagnostico.fisico.bmi AS bmi,
              diagnostico.fisico.edema AS edema,
              diagnostico.fisico.sp AS sp,
              diagnostico.fisico.mvdisminuido AS mvdisminuido,
              diagnostico.fisico.crepitantes AS crepitantes,
              diagnostico.fisico.sibilancias AS sibilancias,
              diagnostico.fisico.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              cce.responsables.nombre AS nombre,
              diagnostico.usuarios.firma AS firma,
              DATE_FORMAT(diagnostico.fisico.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.fisico INNER JOIN cce.responsables ON diagnostico.fisico.usuario = cce.responsables.id
                               INNER JOIN diagnostico.visitas ON diagnostico.fisico.visita = diagnostico.visitas.id
                               INNER JOIN diagnostico.usuarios ON diagnostico.fisico.usuario = diagnostico.usuarios.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                       Radiografías                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_rx;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_rx AS
       SELECT diagnostico.rx.id AS id,
              diagnostico.rx.paciente AS paciente,
              diagnostico.rx.visita AS idvisita,
              DATE_FORMAT(diagnostico.rx.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.rx.ict AS ict,
              diagnostico.rx.cardiomegalia AS cardiomegalia,
              diagnostico.rx.pleuro AS pleuro,
              diagnostico.rx.epoc AS epoc,
              diagnostico.rx.calcificaciones AS calcificaciones,
              diagnostico.rx.notiene AS notiene,
              diagnostico.rx.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.rx.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.rx INNER JOIN cce.responsables ON diagnostico.rx.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                 Electrocardiograma                                          */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_electro;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_electro AS
       SELECT diagnostico.electrocardiograma.id AS id,
              diagnostico.electrocardiograma.protocolo AS protocolo,
              diagnostico.electrocardiograma.visita AS visita,
              DATE_FORMAT(diagnostico.electrocardiograma.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.electrocardiograma.normal AS normal,
              diagnostico.electrocardiograma.bradicardia AS bradicardia,
              diagnostico.electrocardiograma.fc AS fc,
              diagnostico.electrocardiograma.arritmia AS arritmia,
              diagnostico.electrocardiograma.qrs AS qrs,
              diagnostico.electrocardiograma.ejeqrs AS ejeqrs,
              diagnostico.electrocardiograma.pr AS pr,
              diagnostico.electrocardiograma.brd AS brd,
              diagnostico.electrocardiograma.brdmoderado AS brdmoderado,
              diagnostico.electrocardiograma.bcrd AS bcrd,
              diagnostico.electrocardiograma.tendenciahbai AS tendenciahbai,
              diagnostico.electrocardiograma.hbai AS hbai,
              diagnostico.electrocardiograma.tciv AS tciv,
              diagnostico.electrocardiograma.bcri AS bcri,
              diagnostico.electrocardiograma.bav1g AS bav1g,
              diagnostico.electrocardiograma.bav2g AS bav2g,
              diagnostico.electrocardiograma.bav3g AS bav3g,
              diagnostico.electrocardiograma.fibrosis AS fibrosis,
              diagnostico.electrocardiograma.aumentoai AS aumentoai,
              diagnostico.electrocardiograma.wpv AS wpv,
              diagnostico.electrocardiograma.hvi AS hvi,
              diagnostico.electrocardiograma.notiene AS notiene,
              diagnostico.electrocardiograma.sintranstornos AS sintranstornos,
              diagnostico.electrocardiograma.repolarizacion AS repolarizacion,
              diagnostico.electrocardiograma.usuario AS idusaurio,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.electrocardiograma.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.electrocardiograma INNER JOIN cce.responsables ON diagnostico.electrocardiograma.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                 Ecocardiograma                                              */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_ecocardio;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_ecocardio AS
       SELECT diagnostico.ecocardiograma.id AS id,
              diagnostico.ecocardiograma.paciente AS paciente,
              diagnostico.ecocardiograma.visita AS visita,
              DATE_FORMAT(diagnostico.ecocardiograma.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.ecocardiograma.normal AS normal,
              diagnostico.ecocardiograma.ddvi AS ddvi,
              diagnostico.ecocardiograma.dsvi AS dsvi,
              diagnostico.ecocardiograma.fac AS fac,
              diagnostico.ecocardiograma.siv AS siv,
              diagnostico.ecocardiograma.pp AS pp,
              diagnostico.ecocardiograma.ai AS ai,
              diagnostico.ecocardiograma.ao AS ao,
              diagnostico.ecocardiograma.fey AS fey,
              diagnostico.ecocardiograma.ddvd AS ddvd,
              diagnostico.ecocardiograma.ad AS ad,
              diagnostico.ecocardiograma.movilidad AS movilidad,
              diagnostico.ecocardiograma.fsvi AS fsvi,
              diagnostico.ecocardiograma.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.ecocardiograma.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.ecocardiograma INNER JOIN cce.responsables ON diagnostico.ecocardiograma.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Holter                                                  */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_holter;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_holter AS
       SELECT diagnostico.holter.id AS id,
              diagnostico.holter.paciente AS paciente,
              diagnostico.holter.visita AS visita,
              DATE_FORMAT(diagnostico.holter.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.holter.media AS media,
              diagnostico.holter.minima AS minima,
              diagnostico.holter.maxima AS maxima,
              diagnostico.holter.latidos AS latidos,
              diagnostico.holter.ev AS ev,
              diagnostico.holter.nev AS nev,
              diagnostico.holter.tasaev AS tasaev,
              diagnostico.holter.esv AS esv,
              diagnostico.holter.nesv AS nesv,
              diagnostico.holter.tasaesv AS tasaesv,
              diagnostico.holter.normal AS normal,
              diagnostico.holter.bradicardia AS bradicardia,
              diagnostico.holter.rtacronotropica AS rtacronotropica,
              diagnostico.holter.arritmiasevera AS arritmiasevera,
              diagnostico.holter.arritmiasimple AS arritmiasimple,
              diagnostico.holter.arritmiasupra AS arritmiasupra,
              diagnostico.holter.bderama AS bderama,
              diagnostico.holter.bav2g AS bav2g,
              diagnostico.holter.disociacion AS disociacion,
              diagnostico.holter.comentarios AS comentarios,
              diagnostico.holter.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.holter.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.holter INNER JOIN cce.responsables ON diagnostico.holter.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Ergometría                                              */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_ergometria;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_ergometria AS
       SELECT diagnostico.ergometria.id AS id,
              diagnostico.ergometria.paciente AS paciente,
              diagnostico.ergometria.visita AS visita,
              DATE_FORMAT(diagnostico.ergometria.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.ergometria.fcbasal AS fcbasal,
              diagnostico.ergometria.fcmax AS fcmax,
              diagnostico.ergometria.tabasal AS tabasal,
              diagnostico.ergometria.tamax AS tamax,
              diagnostico.ergometria.kpm AS kpm,
              diagnostico.ergometria.itt AS itt,
              diagnostico.ergometria.observaciones AS observaciones,
              diagnostico.ergometria.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.ergometria.fecha_alta, '%d/%m/%Y') AS fecha_alta
       FROM diagnostico.ergometria INNER JOIN cce.responsables ON diagnostico.ergometria.usuario = cce.responsables.id;


/***********************************************************************************************/
/*                                                                                             */
/*                                     Clasificación                                           */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista
DROP VIEW IF EXISTS v_clasificacion;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_clasificacion AS
       SELECT diagnostico.clasificacion.id AS id,
              diagnostico.clasificacion.paciente AS paciente,
              diagnostico.visitas.id AS idvisita,
              DATE_FORMAT(diagnostico.visitas.fecha, '%d/%m/%Y') AS fechavisita,
              diagnostico.clasificacion.estadio AS estadio,
              diagnostico.clasificacion.observaciones AS observaciones,
              diagnostico.clasificacion.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.clasificacion.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.clasificacion INNER JOIN cce.responsables ON diagnostico.clasificacion.usuario = cce.responsables.id
                                      INNER JOIN diagnostico.visitas ON diagnostico.clasificacion.visita = diagnostico.visitas.id;


/***********************************************************************************************/
/*                                                                                             */
/*                           Antecedentes Patologicos                                          */
/*                                                                                             */
/***********************************************************************************************/

/*

       Esta vista se diferencia de la otra de antecedentes que contiene todos los datos
       del formulario de antecedentes patológicos del paciente

*/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_form_antecedentes;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_form_antecedentes AS
       SELECT diagnostico.antecedentes.id AS idantecedente,
              diagnostico.antecedentes.paciente AS protocolo,
              diagnostico.antecedentes.fuma AS fuma,
              diagnostico.antecedentes.alcohol AS alcohol,
              diagnostico.antecedentes.bebida AS bebida,
              diagnostico.antecedentes.adicciones AS adicciones,
              diagnostico.agudo.sintomas AS sintomas,
              diagnostico.agudo.observaciones AS observaciones,
              diagnostico.antecedentes.usuario AS idusuario,
              diagnostico.disautonomia.hipotension AS hipotension,
              diagnostico.disautonomia.bradicardia AS bradicardia,
              diagnostico.disautonomia.astenia AS astenia,
              diagnostico.disautonomia.notiene AS notiene,
              diagnostico.digestivo.disfagia AS disfagia,
              diagnostico.digestivo.pirosis AS pirosis,
              diagnostico.digestivo.regurgitacion AS regurgitacion,
              diagnostico.digestivo.constipacion AS constipacion,
              diagnostico.digestivo.bolo AS bolo,
              diagnostico.digestivo.notiene AS sindigestivo,
              cce.responsables.usuario AS usuario,
              DATE_FORMAT(diagnostico.antecedentes.fecha, '%d/%m/%Y') AS fecha
       FROM diagnostico.antecedentes INNER JOIN cce.responsables ON diagnostico.antecedentes.usuario = cce.responsables.id
                                     INNER JOIN diagnostico.agudo ON diagnostico.antecedentes.paciente = diagnostico.agudo.paciente
                                     INNER JOIN diagnostico.disautonomia ON diagnostico.antecedentes.paciente = diagnostico.disautonomia.paciente
                                     INNER JOIN diagnostico.digestivo ON diagnostico.antecedentes.paciente = diagnostico.digestivo.paciente;


/***********************************************************************************************/
/*                                                                                             */
/*                                         Visitas                                             */
/*                                                                                             */
/***********************************************************************************************/

-- eliminamos la vista si existe
DROP VIEW IF EXISTS v_visitas;

-- la recreamos
CREATE ALGORITHM = UNDEFINED
       DEFINER = CURRENT_USER
       SQL SECURITY INVOKER
       VIEW v_visitas AS
       SELECT diagnostico.visitas.id AS id,
              DATE_FORMAT(diagnostico.visitas.fecha, '%d/%m/%Y') AS fecha,
              diagnostico.visitas.usuario AS idusuario,
              cce.responsables.usuario AS usuario,
              diagnostico.clasificacion.paciente AS paciente,
              diagnostico.clasificacion.estadio AS estadio,
              diagnostico.fisico.peso AS peso,
              DATE_FORMAT(diagnostico.visitas.alta, '%d/%m/%Y') AS fecha_alta,
              CASE WHEN diagnostico.cardiovascular.ausnormal = 1 THEN 1
                   WHEN diagnostico.cardiovascular.irregular = 1 THEN 1
                   WHEN diagnostico.cardiovascular.3er = 1 THEN 1
                   WHEN diagnostico.cardiovascular.4to = 1 THEN 1
                   ELSE 0
              END AS auscultacion,
              CASE WHEN diagnostico.cardiovascular.eyectivo = 1 THEN 1
                   WHEN diagnostico.cardiovascular.regurgitativo = 1 THEN 1
                   WHEN diagnostico.cardiovascular.aortico = 1 THEN 1
                   WHEN diagnostico.cardiovascular.diastolicomitral = 1 THEN 1
                   ELSE 0
              END AS soplos
       FROM diagnostico.visitas INNER JOIN diagnostico.clasificacion ON diagnostico.visitas.id = diagnostico.clasificacion.visita
                                INNER JOIN diagnostico.fisico ON diagnostico.visitas.id = fisico.visita
                                INNER JOIN diagnostico.cardiovascular ON diagnostico.visitas.id = diagnostico.cardiovascular.visita
                                INNER JOIN cce.responsables ON diagnostico.visitas.usuario = cce.responsables.id;
