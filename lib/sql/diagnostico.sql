/*

    Nombre: diagnostico.sql
    Fecha: 20/03/2017
    Autor: Lic. Claudio Invernizzi
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Producido en: INP - Dr. Mario Fatala Chaben
    Buenos Aires - Argentina
    Comentarios: definicion de la estructura de base de datos del sistema
                 de diagnostico

    Se crean tablas de auditoria y los triggers porque el control de
    auditoría se hace demasiado complejo por código e introduce
    errores

    Para la distribución es necesario analizar
    1. Distribuir toda la plataforma en una sola base de datos
    2. Utilizar una base de datos para Calidad, otra para Diagnóstico
       y una tercera para las tablas auxiliares (localidades, documentos, etc.)

    Crear las bases para control de stock

    El acceso de los usuarios es independiente entre el sistema de diagnóstico
    y el de control de calidad (ya que un laboratorio puede participar del
    CCE y tener un sistema propio de diagnóstico)

    Así vamos a tener:

    En la base de datos CCE
    1. La tabla de Laboratorios
    2. La tabla de Usuarios Autorizados de CCE
    3. La tabla de determinaciones
    4. La tabla de otras determinaciones
    5. La tabla de etiquetas
    6. La tabla de notas recibidas
    7. La tabla de notas enviadas a los responsables
    8. La tabla de operativos chagas
    9. La tabla de palets (pivot con la de muestras / etiquetas)
   10. La tabla de técnicas
   11. La tabla de valores de las técnicas
   12. La tabla de no conformidades
   13. Las tablas de auditoría correspondientes

    En la base de datos de diagnóstico
    1. La tabla de pacientes / personas
    2. La tabla de chagas congénito
    3. La tabla de resultados de las pruebas
    4. La tabla de toma de muestras
    5. La tabla de entrevistas
    6. La tabla de motivos de consulta
    7. La tabla de transfusiones recibidas
    8. La tabla de transplantes recibidos
    9. El diccionario de órganos de transplante
   10. La tabla de usuarios y sus permisos
   11. La tabla de técnicas (la independizamos de CCE)
   12. La tabla de valores de corte (independiente de CCE)
   13. La tabla de centros asistenciales
   14. La tabla de protocolos o certificados
   15. La tabla de marcas de reactivos (porque usamos una tabla de tecnicas distinta)
   16. Tabla con las fechas de entrega de resultados según fecha de toma

   Usa como diccionario la tabla de laboratorios del CCE

   En la base de datos de stock tenemos
   1. Los items del stock
   2. La tabla de ingresos
   3. La tabla de egresos
   4. La tabla de inventario
   5. La tabla de fuentes de financiamiento
   6. La tabla de solicitudes de stock

   Usa la tabla de usuarios autorizados de diagnóstico

   En la base de Diccionarios tendremos (comunes a las dos plataformas)
   1. La tabla de países
   2. La tabla de jurisdicciones
   3. La tabla de localidades
   4. El diccionario de tipos de documento
   5. El diccionario de dependencias

   Las tablas de clínica son

    Derivacion diccionario de derivaciones de consulta
    ChagasAgudo antecedentes de chagas agudo
    Drogas diccionario de drogas utilizadas en el tratamiento
    Adversos diccionario de efectos adversos
    Tratamiento también, debe incluir droga
    Antecedentes antecedentes de la anamnesis
    Familiares antecedentes familiares
    Sintomas sintomas del paciente
    disautonomia otros síntomas del paciente
    Digestivo tipo de compromiso digestivo
    Examen examen físico del paciente
    Cardiovascular examen del aparato cardiovascular
    Rx evaluación de las radiografías
    Electro resultados del electrocardiograma
    Eco resultados del ecocardiograma
    Holter resultados del holter
    Ergometría resultados de la ergometría
    Clasificacion clasificación de la enfermedad según kuscknir

*/

-- Establecemos la página de códigos
SET CHARACTER_SET_CLIENT=utf8;
SET CHARACTER_SET_RESULTS=utf8;
SET COLLATION_CONNECTION=utf8_spanish_ci;

-- eliminamos la base si existe
DROP DATABASE IF EXISTS diagnostico;

-- la creamos
CREATE DATABASE IF NOT EXISTS diagnostico
DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

-- seleccionamos
USE diagnostico;

/*************************************************************************/
/*                                                                       */
/*                        Tabla de Personas                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de personas
 protocolo, entero 8 digitos autonumerico, numero de protocolo
 id_laboratorio, entero 6 digitos, laboratorio propietario del registro
 historia_clinica, varchar(20) numero identificador de la historia clinica
 apellido, varchar(50) apellido del paciente
 nombre, varchar(50) nombre del paciente
 documento, varchar(12) numero de documento del paciente (para poder adaptarlo
            a otros paises donde quiza usen letras)
 tipo_documento, entero pequeño (1) clave con el tipo de documento
 fecha_nacimiento, date fecha de nacimiento del paciente
 edad entero(2) edad del paciente puede ser calculado o ingresado manualmente
 sexo entero pequeño (1) clave con el sexo del paciente
 estado_civil entero pequeño (1) clave con el estado civil del paciente
 hijos entero pequeño(2) numero de hijos del paciente
 direccion varchar(100) direccion postal del paciente
 telefono varchar(20) numero de telefono
 celular varchar(30) número de celular
 compania tinyint(1) clave con la compañía de celular para enviar textos
 localidad_nacimiento cadena(9) clave con la localidad de nacimiento
 coordenadas_nacimiento varchar(50) coordenadas gps
 localidad_residencia cadena(9) clave con la localidad de residencia
 coordenadas_residencia varchar(50) coordenadas gps
 localidad_madre cadena(9) clave con la localidad de origen de la madre
 madre_positiva enum (si no no sabe) indica si la madre era positiva
 ocupacion varchar(250) descripcion de la ocupacion
 obra_social varchar(100) nombre de la obra social
 motivo entero pequeño(1) clave con el motivo de consulta
 derivacion int(1) clave del tipo de derivación
 tratamiento enum (si no no sabe) recibio tratamiento
 otras_enfermedades indica si tiene otras enfermedades
 usuario entero(4) clave de la tabla de usuarios autorizados
 fecha_alta date fecha en que ingreso el registro
 comentarios: texto, comentarios y observaciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS personas;

-- creamos la tabla
CREATE TABLE personas (
    protocolo int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento varchar(9) NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia varchar(9) NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre varchar(9) DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion tinyint(1) UNSIGNED NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY derivacion(derivacion),
    KEY tipo_documento(tipo_documento),
    KEY compania(compania),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_personas;

-- creamos la tabla
CREATE TABLE auditoria_personas (
    protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    historia_clinica varchar(20) DEFAULT NULL,
    apellido varchar(50) NOT NULL,
    nombre varchar(50) NOT NULL,
    documento varchar(12) NOT NULL,
    tipo_documento tinyint(1) UNSIGNED NOT NULL,
    fecha_nacimiento date DEFAULT NULL,
    edad int(2) UNSIGNED DEFAULT NULL,
    sexo tinyint(1) UNSIGNED NOT NULL,
    estado_civil tinyint(1) UNSIGNED NOT NULL,
    hijos tinyint(2) UNSIGNED DEFAULT NULL,
    direccion varchar(100) DEFAULT NULL,
    telefono varchar(20) DEFAULT NULL,
    celular varchar(30) DEFAULT NULL,
    compania int(1) DEFAULT NULL,
    mail varchar(50) DEFAULT NULL,
    localidad_nacimiento varchar(9) NOT NULL,
    coordenadas_nacimiento varchar(50) DEFAULT NULL,
    localidad_residencia varchar(9) NOT NULL,
    coordenadas_residencia varchar(50) DEFAULT NULL,
    localidad_madre varchar(9) DEFAULT NULL,
    madre_positiva enum("Si", "No", "No Sabe") DEFAULT "No Sabe",
    ocupacion varchar(250) DEFAULT NULL,
    obra_social varchar(100) DEFAULT NULL,
    motivo tinyint(1) UNSIGNED NOT NULL,
    derivacion tinyint(1) UNSIGNED NOT NULL,
    tratamiento enum ("Si", "No", "No Sabe") NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY protocolo(protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY historia_clinica(historia_clinica),
    KEY apellido(apellido),
    KEY nombre(nombre),
    KEY nombre_completo(apellido, nombre),
    KEY documento(documento),
    KEY compania(compania),
    KEY derivacion(derivacion),
    KEY tipo_documento(tipo_documento),
    KEY sexo(sexo),
    KEY estado_civil(estado_civil),
    KEY localidad_nacimiento(localidad_nacimiento),
    KEY localidad_residencia(localidad_residencia),
    KEY localidad_madre(localidad_madre),
    KEY motivo(motivo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de los pacientes';

-- eliminamos el trigger de edición de personas
DROP TRIGGER IF EXISTS edicion_personas;

-- creamos el trigger
CREATE TRIGGER edicion_personas
AFTER UPDATE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_personas;

-- creamos el trigger
CREATE TRIGGER eliminacion_personas
AFTER DELETE ON personas
FOR EACH ROW
INSERT INTO auditoria_personas
     (protocolo,
      id_laboratorio,
      historia_clinica,
      apellido,
      nombre,
      documento,
      tipo_documento,
      fecha_nacimiento,
      edad,
      sexo,
      estado_civil,
      hijos,
      direccion,
      telefono,
      celular,
      compania,
      mail,
      localidad_nacimiento,
      coordenadas_nacimiento,
      localidad_residencia,
      coordenadas_residencia,
      localidad_madre,
      madre_positiva,
      ocupacion,
      obra_social,
      motivo,
      derivacion,
      tratamiento,
      usuario,
      fecha_alta,
      comentarios,
      evento)
     VALUES
     (OLD.protocolo,
      OLD.id_laboratorio,
      OLD.historia_clinica,
      OLD.apellido,
      OLD.nombre,
      OLD.documento,
      OLD.tipo_documento,
      OLD.fecha_nacimiento,
      OLD.edad,
      OLD.sexo,
      OLD.estado_civil,
      OLD.hijos,
      OLD.direccion,
      OLD.telefono,
      OLD.celular,
      OLD.compania,
      OLD.mail,
      OLD.localidad_nacimiento,
      OLD.coordenadas_nacimiento,
      OLD.localidad_residencia,
      OLD.coordenadas_residencia,
      OLD.localidad_madre,
      OLD.madre_positiva,
      OLD.ocupacion,
      OLD.obra_social,
      OLD.motivo,
      OLD.derivacion,
      OLD.tratamiento,
      OLD.usuario,
      OLD.fecha_alta,
      OLD.comentarios,
      "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*              Fechas de entrega de resultados                          */
/*                                                                       */
/*************************************************************************/

/*

    Estructura de la tabla
    id int(4) clave del registro (a una entrada por día tenemos tabla
              para 25 años)
    recepcion date fecha de recepción de la muestra
    entrega date fecha de entrega de resultados
    laboratorio int(6) clave del laboratorio (cada laboratorio puede
                tener una fecha distinta de entrega)
    usuario int(4) usuario que ingresó el registro

    En esta tabla no usamos registro de transacciones

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entregas;

-- la recreamos
CREATE TABLE IF NOT EXISTS entregas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    recepcion date NOT NULL,
    entrega date NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(laboratorio),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Fechas de entregas de resultados';


/*************************************************************************/
/*                                                                       */
/*                Diccionario de Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*

  Estructura de la tabla con el diccionario de enfermedades
  id entero(3) autonumérico, clave del registro
  enfermedad varchar(100) descripción de la enfermedad
  id_usuario entero(4) clave del usuario
  fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS enfermedades;

-- la recreamos
CREATE TABLE enfermedades (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_enfermedades;

-- la recreamos
CREATE TABLE auditoria_enfermedades (
    id int(3) UNSIGNED NOT NULL,
    enfermedad varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta DATE NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY enfermedad(enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de enfermedades';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_enfermedades
AFTER UPDATE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_enfermedades
AFTER DELETE ON enfermedades
FOR EACH ROW
INSERT INTO auditoria_enfermedades
    (id,
     enfermedad,
     id_usuario,
     fecha_alta,
     evento)
    VALUES
    (OLD.id,
     OLD.enfermedad,
     OLD.id_usuario,
     OLD.fecha_alta,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Otras Enfermedades                            */
/*                                                                       */
/*************************************************************************/

/*
   Estructura de la tabla de otras enfermedades, esta tabla es un pivot
   entre el diccionario de enfermedades y el de pacientes, agrega un
   registro por cada enfermedad sufrida por el paciente
   id int(9) clave del registro
   id_protocoloi int(8) clave del paciente
   id_enfermedad int(3) clave de la enfermedad
   id_usuario int(4) clave del usuario
   fecha date fecha de la enfermedad
   fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS otras_enfermedades;

-- la recreamos
CREATE TABLE otras_enfermedades(
    id int(9) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Otras Enfermedades de los pacientes';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_otras_enfermedades;

-- la recreamos
CREATE TABLE auditoria_otras_enfermedades(
    id int(9) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_enfermedad int(3) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_enfermedad(id_enfermedad),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de otras enfermedades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER edicion_otras_enfermedades
AFTER UPDATE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_otras_enfermedades;

-- lo recreamos
CREATE TRIGGER eliminacion_otras_enfermedades
AFTER DELETE ON otras_enfermedades
FOR EACH ROW
INSERT INTO auditoria_otras_enfermedades
       (id,
        id_protocolo,
        id_enfermedad,
        id_usuario,
        fecha,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_protocolo,
        OLD.id_enfermedad,
        OLD.id_usuario,
        OLD.fecha,
        OLD.fecha_alta,
        "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                         Chagas Congenito                              */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla congenito, esta tabla al ser menor, agrega como
 registro auxiliar los datos de chagas congenito
 id entero(8) autonumerico clave unica del registro
 id_protocolo entero(8) clave del paciente
 id_madre entero(8) clave del protocolo de la madre
 id_sivila varchar(20) clave del sivila
 reportado date fecha en que fue informado al sivila
 parto enum (normal / cesarea / otro) tipo de parto
 peso entero(4) peso al nacer en gramos
 prematuro enum (si / no) indica si el bebe fue prematuro
 institucion entero(8) clave de la institucion
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha_alta date fecha de alta del registro
 comentarios texto, comentarios especificos

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS congenito;

-- creamos la tabla
CREATE TABLE congenito (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas congenito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_congenito;

-- la recreamos
CREATE TABLE auditoria_congenito (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_madre int(8) UNSIGNED NOT NULL,
    id_sivila varchar(20) DEFAULT NULL,
    reportado date DEFAULT NULL,
    parto enum("Normal", "Cesárea", "No Sabe") NOT NULL,
    peso int(4) UNSIGNED NOT NULL,
    prematuro enum("Si", "No", "No Sabe"),
    institucion int(8) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_madre(id_madre),
    KEY id_sivila(id_sivila),
    KEY institucion(institucion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de chagas congenito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_congenito;

-- lo recreamos
CREATE TRIGGER edicion_congenito
AFTER UPDATE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_congenito;

-- lo recreamos
CREATE TRIGGER eliminacion_congenito
AFTER DELETE ON congenito
FOR EACH ROW
INSERT INTO auditoria_congenito
    (id,
     id_protocolo,
     id_madre,
     id_sivila,
     reportado,
     parto,
     peso,
     prematuro,
     institucion,
     id_usuario,
     fecha_alta,
     comentarios,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.id_madre,
     OLD.id_sivila,
     OLD.reportado,
     OLD.parto,
     OLD.peso,
     OLD.prematuro,
     OLD.institucion,
     OLD.id_usuario,
     OLD.fecha_alta,
     OLD.comentarios,
     "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                      Resultados de las Pruebas                           */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de resultados
 id_resultado entero(10) autonumerico, clave del registro
 id_muestra entero(8) clave de la muestra
 resultado enum (Reactivo / No Reactivo / Indeterminado) resultado obtenido
 valor_corte varchar(10) valor de corte utilizado
 valor_lectura varchar(10) valor de lectura obtenido
 id_tecnica entero pequeño (3) clave de la tecnica utilizada
 fecha_determinacion fecha en que se realizo la determinacion
 comentarios texto comentarios y observaciones
 id_usuario entero(4) usuario que realizo la determinacion
 id_verifico entero(4) usuario que verificó el resultado
 id_firmo entero(4) usuario que firmó o autorizó el protocolo
 fecha date fecha de alta del registro

*/

-- elimina la tabla
DROP TABLE IF EXISTS resultados;

-- creamos la tabla
CREATE TABLE resultados (
    id_resultado int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Resultados de las pruebas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_resultados;

-- creamos la tabla
CREATE TABLE auditoria_resultados (
    id_resultado int(8) UNSIGNED NOT NULL,
    id_muestra int(8) UNSIGNED NOT NULL,
    resultado enum("Reactivo", "No Reactivo", "Indeterminado") NOT NULL,
    valor_corte varchar(10) NOT NULL,
    valor_lectura varchar(10) NOT NULL,
    id_tecnica tinyint(3) UNSIGNED NOT NULL,
    fecha_determinacion date NOT NULL,
    comentarios text DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    id_verifico int(4) UNSIGNED DEFAULT NULL,
    id_firmo int(4) UNSIGNED DEFAULT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id_resultado(id_resultado),
    KEY id_muestra(id_muestra),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario),
    KEY id_verifico(id_verifico),
    KEY id_firmo(id_firmo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los resultados';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_resultados;

-- lo recreamos
CREATE TRIGGER edicion_resultados
AFTER UPDATE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_resultados;

-- lo recreamos
CREATE TRIGGER eliminacion_resultados
AFTER DELETE ON resultados
FOR EACH ROW
INSERT INTO auditoria_resultados
    (id_resultado,
     id_muestra,
     resultado,
     valor_corte,
     valor_lectura,
     id_tecnica,
     fecha_determinacion,
     comentarios,
     id_usuario,
     id_verifico,
     id_firmo,
     fecha,
     evento)
    VALUES
    (OLD.id_resultado,
     OLD.id_muestra,
     OLD.resultado,
     OLD.valor_corte,
     OLD.valor_lectura,
     OLD.id_tecnica,
     OLD.fecha_determinacion,
     OLD.comentarios,
     OLD.id_usuario,
     OLD.id_verifico,
     OLD.id_firmo,
     OLD.fecha,
     "Eliminacion");


/*************************************************************************/
/*                                                                       */
/*                       Toma de Muestras                                */
/*                                                                       */
/*************************************************************************/

/*

 Estructura de la tabla de muestras
 id_muestra entero(8) autonumerico
 id_protocolo entero(8) clave del paciente o protocolo
 id_laboratorio entero(6) laboratorio donde se tomó la muestra
 comentarios texto comentarios y observaciones
 id_genero entero(4) clave del usuario que generó el registro
 fecha_alta date fecha en que fue tomada la muestra
 id_tomo entero(4) clave del usuario que tomó la muestra
 fecha_toma date fecha en que fue tomada la muestra

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS muestras;

-- la recreamos desde cero
CREATE TABLE muestras (
    id_muestra int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_tomo int(4) UNSIGNED DEFAULT NULL,
    fecha_toma date DEFAULT NULL,
    PRIMARY KEY(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Muestras de los pacientes';

-- eliminamos la auditoria de las muestras
DROP TABLE IF EXISTS auditoria_muestras;

-- la recreamos desde cero
CREATE TABLE auditoria_muestras (
    id_muestra int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    comentarios TEXT,
    id_genero int(4) UNSIGNED NOT NULL,
    fecha_alta date,
    id_tomo int(4) DEFAULT NULL,
    fecha_toma date,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_muestra(id_muestra),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_genero(id_genero),
    KEY id_tomo(id_tomo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las muestras';

-- eliminamos el trigger de edición si existe
DROP TRIGGER IF EXISTS edicion_muestras;

-- lo recreamos
CREATE TRIGGER edicion_muestras
AFTER UPDATE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_muestras;

-- lo recreamos
CREATE TRIGGER eliminacion_muestras
AFTER DELETE ON muestras
FOR EACH ROW
INSERT INTO auditoria_muestras
    (id_muestra,
     id_protocolo,
     id_laboratorio,
     comentarios,
     id_genero,
     fecha_alta,
     id_tomo,
     fecha_toma,
     evento)
    VALUES
    (OLD.id_muestra,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.comentarios,
     OLD.id_genero,
     OLD.fecha_alta,
     OLD.id_tomo,
     OLD.fecha_toma,
     "Eliminacion");


/************************************************************************/
/*                                                                      */
/*                  Entrevistas a Pacientes                             */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de entrevistas
 id_entrevista entero(6) autonumerico, clave del registro
 id_protocolo entero(8) clave del paciente
 id_laboratorio int(6) laboratorio donde se realizó la entrevista
 resultado enum(positivo / negativo / otro) resultado de la entrevista
 actitud enum (positiva / negativa / otra) actitud del paciente
 concurrio enum (si / no) indica si concurrio a la entrevista
 tipo (presencial / telefonica) modalidad de la entrevista
 fecha date fecha de la entrevista
 comentarios text comentarios y observaciones del entrevistador
 id_usuario entero(4) clave del usuario que entrevisto

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS entrevistas;

-- la recreamos
CREATE TABLE entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Entrevistas a pacientes';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_entrevistas;

-- la recreamos
CREATE TABLE auditoria_entrevistas (
    id_entrevista int(6) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    resultado enum("Positivo", "Negativo", "Otro") NOT NULL,
    actitud enum("Positiva", "Negativa", "Indiferente") NOT NULL,
    concurrio enum("Si", "No") NOT NULL,
    tipo enum("Presencial", "Telefonica") NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    comentarios text,
    id_usuario int(4) UNSIGNED NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id_entrevista(id_entrevista),
    KEY id_protocolo(id_protocolo),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de entrevistas a pacientes';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_entrevistas;

-- lo recreamos
CREATE TRIGGER edicion_entrevistas
AFTER UPDATE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_entrevistas;

-- lo recreamos
CREATE TRIGGER eliminacion_entrevistas
AFTER DELETE ON entrevistas
FOR EACH ROW
INSERT INTO auditoria_entrevistas
    (id_entrevista,
     id_protocolo,
     id_laboratorio,
     resultado,
     actitud,
     concurrio,
     tipo,
     comentarios,
     id_usuario,
     evento)
    VALUES
    (OLD.id_entrevista,
     OLD.id_protocolo,
     OLD.id_laboratorio,
     OLD.resultado,
     OLD.actitud,
     OLD.concurrio,
     OLD.tipo,
     OLD.comentarios,
     OLD.id_usuario,
     "Eliminacion");


/************************************************************************/
/*                                                                      */
/*                     Motivos de Consulta                              */
/*                                                                      */
/************************************************************************/

/*

 Estructura de la tabla de motivos de consulta
 id_motivo entero pequeño (2) autonumerico, clave del registro
 motivo varchar(50) descripcion del motivo de la consulta
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS motivos;

-- la recreamos
CREATE TABLE motivos (
    id_motivo tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    motivo varchar(50) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_motivo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Motivos de consulta';

-- insertamos los valores iniciales
INSERT INTO motivos (motivo, id_usuario) VALUES ("Atención Médica", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Banco de Sangre", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Embarazo", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("HIV", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Niño", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Pre ocupacional", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Transplante", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Laboral", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Migraciones", 1);
INSERT INTO motivos (motivo, id_usuario) VALUES ("Otro", 1);


/***************************************************************************/
/*                                                                         */
/*                       Transfusiones Recibidas                           */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de transfusiones
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 fecha_transfusion date fecha de la transfusion
 motivo varchar(200) descripcion del motivo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transfusiones;

-- la recreamos
CREATE TABLE transfusiones (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transfusiones recibidas';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transfusiones;

-- la recreamos
CREATE TABLE auditoria_transfusiones (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    fecha_transfusion date NOT NULL,
    motivo varchar(200) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de las Transfusiones';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_transfusiones;

-- lo recreamos
CREATE TRIGGER edicion_transfusiones
AFTER UPDATE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transfusiones;

-- lo recreamos
CREATE TRIGGER eliminacion_transfusiones
AFTER DELETE ON transfusiones
FOR EACH ROW
INSERT INTO auditoria_transfusiones
    (id,
     id_protocolo,
     fecha_transfusion,
     motivo,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.fecha_transfusion,
     OLD.motivo,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                            Transplantes Recibidos                      */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de transplantes
 id entero(8) autonumerico, clave del registro
 id_protocolo entero(8) clave del protocolo
 organo entero pequeño (2) clave del organo recibido
 positivo enum (si / no / no sabe) indica si el organo recibido era
          positivo para chagas
 fecha_transplante date fecha del transplante
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS transplantes;

-- la creamos
CREATE TABLE transplantes (
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Transplantes recibidos';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_transplantes;

-- la creamos
CREATE TABLE auditoria_transplantes (
    id int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    organo tinyint(2) UNSIGNED NOT NULL,
    positivo enum("Si", "No", "No Sabe") NOT NULL,
    fecha_transplante date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_protocolo(id_protocolo),
    KEY organo(organo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Transplantes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_transplantes;

-- lo recreamos
CREATE TRIGGER edicion_transplantes
AFTER UPDATE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_transplantes;

-- lo recreamos
CREATE TRIGGER eliminacion_transplantes
AFTER DELETE ON transplantes
FOR EACH ROW
INSERT INTO auditoria_transplantes
    (id,
     id_protocolo,
     organo,
     positivo,
     fecha_transplante,
     id_usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.id_protocolo,
     OLD.organo,
     OLD.positivo,
     OLD.fecha_transplante,
     OLD.id_usuario,
     OLD.fecha,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                       Diccionario de Organos                           */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de organos
 id entero pequeño (2) clave del registro
 organo varchar(100) nombre del organo
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS organos;

-- la creamos
CREATE TABLE organos (
    id tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    organo varchar(100) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de Organos';

-- cargamos los registros iniciales
INSERT INTO organos (organo, id_usuario) VALUES ("Corazon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Higado", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Riñon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Pulmon", 1);
INSERT INTO organos (organo, id_usuario) VALUES ("Medula", 1);


/**************************************************************************/
/*                                                                        */
/*                         Usuarios Autorizados                           */
/*                                                                        */
/**************************************************************************/

/*
 Para evitar complicaciones, vamos a usar a tabla de usuarios de control
 de calidad y relacionarlo con la tabla de permisos

 Estructura de la tabla de usuarios

 id, entero(4) clave de la tabla de usuarios sincronizada con la tabla
     de usuarios del sistema cce
 autorizo, clave del usuario que autorizo
 laboratorio, entero clave del laboratorio al que pertenece (es distinto
              de la clave laboratorio del sistema cce)
 administrador enum (es o no administrador)
 personas enum(si o no puede editar personas)
 congenito enum(si / no) si puede editar chagas congenito
 resultados enum(si / no) si puede cargar resultados
 muestras enum(si / no) si puede cargar muestras
 entrevistas enum(si / no) si puede cargar entrevistas
 auxiliares enum(si / no) si puede editar tablas auxiliares
 protocolos enum(si / no) si puede firmar protocolos
 stock enum (si / no) si puede editar el stock
 usuarios enum (si / no) si puede editar usuarios del sistema
 clinica enum (si / no) si puede editar datos de historia clínica
 firma blob contenido de la firma (si puede firmar protocolos)

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS usuarios;

-- la recreamos desde cero
CREATE TABLE usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    clinica enum("Si", "No") DEFAULT "No",
    firma mediumblob DEFAULT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Usuarios autorizados del sistema';

-- insertamos el primer registro en la base
INSERT INTO usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica)
    VALUES
    ('1',
     '1',
     '397',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si',
     'Si');

-- elimina la tabla de auditoria de usuarios si existe
DROP TABLE IF EXISTS auditoria_usuarios;

-- creacion de la tabla de auditoria de usuarios
CREATE TABLE auditoria_usuarios(
    id int(4) UNSIGNED NOT NULL,
    autorizo int(4) UNSIGNED NOT NULL,
    laboratorio int(6) UNSIGNED NOT NULL,
    administrador enum("Si", "No") DEFAULT "No",
    personas enum("Si", "No") DEFAULT "No",
    congenito enum("Si", "No") DEFAULT "No",
    resultados enum("Si", "No") DEFAULT "No",
    muestras enum("Si", "No") DEFAULT "No",
    entrevistas enum("Si", "No") DEFAULT "No",
    auxiliares enum("Si", "No") DEFAULT "No",
    protocolos enum("Si", "No") DEFAULT "No",
    stock enum("Si", "No") DEFAULT "No",
    usuarios enum("Si", "No") DEFAULT "No",
    clinica enum("Si", "No") DEFAULT "No",
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY laboratorio(laboratorio),
    KEY autorizo(autorizo)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de usuarios del sistema';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_usuarios;

-- lo recreamos
CREATE TRIGGER edicion_usuarios
AFTER UPDATE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.laboratorio,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     OLD.clinica,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_usuarios;

-- lo recreamos
CREATE TRIGGER eliminacion_usuarios
AFTER DELETE ON usuarios
FOR EACH ROW
INSERT INTO auditoria_usuarios
    (id,
     autorizo,
     laboratorio,
     administrador,
     personas,
     congenito,
     resultados,
     muestras,
     entrevistas,
     auxiliares,
     protocolos,
     stock,
     usuarios,
     clinica,
     evento)
    VALUES
    (OLD.id,
     OLD.autorizo,
     OLD.laboratorio,
     OLD.administrador,
     OLD.personas,
     OLD.congenito,
     OLD.resultados,
     OLD.muestras,
     OLD.entrevistas,
     OLD.auxiliares,
     OLD.protocolos,
     OLD.stock,
     OLD.usuarios,
     OLD.clinica,
     "Eliminacion");


/**************************************************************************/
/*                                                                        */
/*                      Pruebas Reconocidas                               */
/*                                                                        */
/**************************************************************************/

/*

 Estructura de la tabla de pruebas o tecnicas
 id entero pequeño(3) clave del registro
 tecnica varchar(30) nombre genérico de la técnica
 nombre varchar(30) nombre completo de la técnica
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tecnicas;

-- la recreamos
CREATE TABLE tecnicas (
    id int(3) UNSIGNED NOT NULL AUTO_INCREMENT,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de técnicas';

-- cargamos los valores iniciales
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (1, 'AP', 'Aglutinacion de Particulas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (2, 'ELISA', 'Ensayo por Inmunoadsorción Ligado a Enzimas', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (3, 'HAI', 'Hemaglutinacion Indirecta', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (4, 'IFI', 'Inmunofluorescencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (5, 'Quimioluminiscencia', 'Quimioluminiscencia', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (6, 'OTRA', 'Otra', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (7, 'PCR', 'Reaccion en Cadena de la Polimerasa', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (8, 'Micrometodo', 'Micrometodo', 1);
INSERT INTO tecnicas (id, tecnica, nombre, id_usuario) VALUES (9, 'XENO', 'Xenodiagnostico', 1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tecnicas;

-- la recreamos
CREATE TABLE auditoria_tecnicas (
    id int(3) UNSIGNED NOT NULL,
    tecnica VARCHAR(30) DEFAULT NULL,
    nombre VARCHAR(60) DEFAULT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY tecnica(tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de técnicas';

-- eliminamos el trigger de edicion
DROP TRIGGER IF EXISTS edicion_tecnicas;

-- lo recreamos
CREATE TRIGGER edicion_tecnicas
AFTER UPDATE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tecnicas;

-- lo recreamos
CREATE TRIGGER eliminacion_tecnicas
AFTER DELETE ON tecnicas
FOR EACH ROW
INSERT INTO auditoria_tecnicas
       (id,
        tecnica,
        nombre,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.tecnica,
        OLD.nombre,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Marcas de Reactivos                              */
/*                                                                          */
/****************************************************************************/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS `marcas`;

-- recreamos desde cero
CREATE TABLE IF NOT EXISTS `marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL AUTO_INCREMENT,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de las marcas utilizadas';

-- agregamos los registros iniciales
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (1,'Serodia','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (3,'Biomerieux','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (4,'Lemos','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (5,'Bioschile','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (6,'Biozima','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (7,'Lisado de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (8,'Polychaco','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (9,'Fatalakit','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (10,'Hemave','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (11,'Biocientifica','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (12,'Inmunofluor','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (13,'Ag Fatala','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (14,'Otra','4','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (15,'Otra','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (19,'Otra','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (21,'Otra','1','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (24,'Recombinante 3.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (25,'Recombinante 4.0 de Wiener','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (26,'Wiener','3','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (27,'ABBOT Architect','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (29,'Otra','6','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (30,'Lemos Recombinante','2','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (31,'ABBOT Architect','5','1');
INSERT INTO marcas (ID,MARCA,TECNICA,USUARIO) VALUES (32,'Otra','5','1');

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_marcas;

-- creamos la tabla de auditoría marcas
CREATE TABLE IF NOT EXISTS `auditoria_marcas` (
  `ID` tinyint(2) UNSIGNED NOT NULL,
  `MARCA` varchar(40) NOT NULL,
  `TECNICA` smallint(3) UNSIGNED NOT NULL,
  `FECHA_ALTA` date NOT NULL,
  `FECHA_EVENTO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `USUARIO` int(6) UNSIGNED NOT NULL,
  `EVENTO` enum("Edicion", "Eliminacion"),
  KEY `ID`(`ID`),
  KEY `MARCA` (`MARCA`),
  KEY TECNICA(TECNICA),
  KEY USUARIO(USUARIO)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las marcas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_marcas;

-- lo recreamos
CREATE TRIGGER edicion_marcas
AFTER UPDATE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Edicion");

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS eliminacion_marcas;

-- lo recreamos
CREATE TRIGGER eliminacion_marcas
AFTER DELETE ON marcas
FOR EACH ROW
INSERT INTO auditoria_marcas
       (ID,
        MARCA,
        TECNICA,
        FECHA_ALTA,
        USUARIO,
        EVENTO)
       VALUES
       (OLD.ID,
        OLD.MARCA,
        OLD.TECNICA,
        OLD.FECHA_ALTA,
        OLD.USUARIO,
        "Eliminacion");


/****************************************************************************/
/*                                                                          */
/*                         Valores de Corte                                 */
/*                                                                          */
/****************************************************************************/

/*

 Estructura de la tabla de valores de corte de cada prueba, si una técnica
 tiene varios valores el sistema debe presentar un desplegable, si tiene
 solo un valor entiende que se trata de una máscara de entrada
 id entero(4) autonumerico, clave unica del registro
 id_tecnica entero pequeño(3) clave de la tecnica
 valor varchar(10) uno de los valores aceptados o la mascara de entrada
 id_usuario entero(4) clave del usuario
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS valores_tecnicas;

-- la recreamos
CREATE TABLE valores_tecnicas (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de valores de las técnicas';

-- insertamos los valores iniciales
INSERT INTO valores_tecnicas
        (id_tecnica, valor, id_usuario) VALUES
        (1,"1/4",1),
        (1,"1/8",1),
        (1,"1/16",1),
        (1,"1/32",1),
        (1,"1/64",1),
        (1,"1/128",1),
        (1,"1/256",1),
        (1,"1/512",1),
        (1,"1/1024",1),
        (1,"1/2048",1),
        (1,"1/4096",1),
        (3,"1/4",1),
        (3,"1/8",1),
        (3,"1/16",1),
        (3,"1/32",1),
        (3,"1/64",1),
        (3,"1/128",1),
        (3,"1/256",1),
        (3,"1/512",1),
        (3,"1/1024",1),
        (3,"1/2048",1),
        (3,"1/4096",1),
        (4,"1/4",1),
        (4,"1/8",1),
        (4,"1/16",1),
        (4,"1/32",1),
        (4,"1/64",1),
        (4,"1/128",1),
        (4,"1/256",1),
        (4,"1/512",1),
        (4,"1/1024",1),
        (4,"1/2048",1),
        (4,"1/4096",1),
        (2,"9.999",1),
        (5,"99.999",1);

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_valores;

-- la recreamos
CREATE TABLE auditoria_valores (
    id int(4) UNSIGNED NOT NULL,
    id_tecnica smallint(3) UNSIGNED NOT NULL,
    valor varchar(10) NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_tecnica(id_tecnica),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de valores de las técnicas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_valores;

-- lo recreamos
CREATE TRIGGER edicion_valores
AFTER UPDATE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_valores;

-- lo recreamos
CREATE TRIGGER eliminacion_valores
AFTER DELETE ON valores_tecnicas
FOR EACH ROW
INSERT INTO auditoria_valores
       (id,
        id_tecnica,
        valor,
        id_usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.id_tecnica,
        OLD.valor,
        OLD.id_usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                  Instituciones Asistenciales                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de instituciones asistenciales
 id_institucion entero(6) autonumerico, clave del registro
 siisa varchar(20) clave siisa de la institución
 nombre varchar(250) nombre del centro asistencial
 id_localidad varchar(9) clave de la localidad del centro (a traves de la
              localidad obtenemos la jurisdiccion y el pais)
 direccion varchar(250) direccion postal de la institucion
 codigo_postal varchar(20) codigo postal de la direccion
 telefono varchar(20) telefono de la institucion
 mail varchar(50) direccion de correo de la institucion
 responsable varchar(150) nombre del responsable o director
 dependencia entero pequeño (1) clave de la dependencia
 coordenadas varchar(100) coordenadas gps
 comentarios texto comentarios del usuario
 id_usuario entero(4) clave del usuario que ingreso el registro
 fecha date fecha de alta del registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS centros_asistenciales;

-- creamos la tabla
CREATE TABLE `centros_asistenciales` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Centros Asistenciales';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_centros_asistenciales;

-- creamos la tabla
CREATE TABLE `auditoria_centros_asistenciales` (
  `id` int(6) unsigned NOT NULL,
  `nombre` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `siisa` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `localidad` varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `codigo_postal` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `mail` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `responsable` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(4) unsigned NOT NULL,
  `dependencia` tinyint(1) unsigned NOT NULL,
  `fecha_alta` date NOT NULL,
  `fecha_evento` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `comentarios` text COLLATE utf8_spanish_ci,
  `coordenadas` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `evento` enum('Edicion', 'Eliminacion') NOT NULL,
  KEY `id`(`id`),
  KEY `siisa` (`siisa`),
  KEY `responsable` (`responsable`),
  KEY `id_usuario` (`id_usuario`),
  KEY `dependencia` (`dependencia`),
  KEY `nombre` (`nombre`),
  KEY `localidad` (`localidad`)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Centros Asistenciales';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_instituciones;

-- lo recreamos
CREATE TRIGGER edicion_centros_asistenciales
AFTER UPDATE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_instituciones;

-- lo recreamos
CREATE TRIGGER eliminacion_centros_asistenciales
AFTER DELETE ON centros_asistenciales
FOR EACH ROW
INSERT INTO auditoria_centros_asistenciales
    (id,
     nombre,
     siisa,
     localidad,
     direccion,
     codigo_postal,
     telefono,
     mail,
     responsable,
     id_usuario,
     dependencia,
     fecha_alta,
     comentarios,
     coordenadas,
     evento)
    VALUES
    (OLD.id,
     OLD.nombre,
     OLD.siisa,
     OLD.localidad,
     OLD.direccion,
     OLD.codigo_postal,
     OLD.telefono,
     OLD.mail,
     OLD.responsable,
     OLD.id_usuario,
     OLD.dependencia,
     OLD.fecha_alta,
     OLD.comentarios,
     OLD.coordenadas,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                    Protocolos o Certificados                            */
/*                                                                         */
/***************************************************************************/

/*

 Estructura de la tabla de protocolos
 id_informe entero(8) autonumérico clave del registro
 id_protocolo entero(8) clave del registro de pacientes
 informe blob contenido del resultado en pdf
 id_usuario entero(4) usuario que imprimió el registro
 fecha date fecha de impresión o entrega

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS protocolos;

-- la creamos
CREATE TABLE protocolos (
    id_informe int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_protocolos;

-- la creamos
CREATE TABLE auditoria_protocolos (
    id_informe int(8) UNSIGNED NOT NULL,
    id_protocolo int(8) UNSIGNED NOT NULL,
    informe blob,
    id_usuario int(8) UNSIGNED NOT NULL,
    fecha_impresion date DEFAULT NULL,
    fecha date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id_informe(id_informe),
    KEY id_protocolo(id_protocolo),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Protocolos de pacientes';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_protocolos;

-- lo recreamos
CREATE TRIGGER edicion_protocolos
AFTER UPDATE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_protocolos;

-- lo recreamos
CREATE TRIGGER eliminacion_protocolos
AFTER DELETE ON protocolos
FOR EACH ROW
INSERT INTO auditoria_protocolos
    (id_informe,
     id_protocolo,
     informe,
     id_usuario,
     fecha_impresion,
     fecha,
     evento)
    VALUES
    (OLD.id_informe,
     OLD.id_protocolo,
     OLD.informe,
     OLD.id_usuario,
     OLD.fecha_impresion,
     OLD.fecha,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de No Conformidades                                     */
/*                                                                                          */
/********************************************************************************************/

/*

    Estructura de la tabla
    id int(6) clave del registro
    usuario int(4) clave del usuario que generó la no conformidad
    titulo varchar(100) descripción breve de la no conformidad
    fecha date fecha de emisión de la no conformidad
    detalle blob descripción de la no conformidad
    causa blob análisis de las causas
    respuesta date fecha de la respuesta
    accion blob detalle de las acciones correctivas
    ejecucion date fecha límite para la ejecución de la acción correctiva

*/

-- eliminamos si existe
DROP TABLE IF EXISTS noconformidad;

-- creamos la tabla
CREATE TABLE noconformidad(
    id int(6) unsigned NOT NULL AUTO_INCREMENT,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    PRIMARY KEY (id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Incidencias y no conformidades';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_conformidades;

-- la recreamos
CREATE TABLE auditoria_conformidades(
    id int(6) unsigned NOT NULL ,
    usuario int(4) unsigned NOT NULL,
    titulo varchar(100) NOT NULL,
    fecha date DEFAULT NULL,
    detalle blob DEFAULT NULL,
    causa blob DEFAULT NULL,
    respuesta date DEFAULT NULL,
    accion blob DEFAULT NULL,
    ejecucion date DEFAULT NULL,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario),
    KEY fecha(fecha),
    KEY respuesta(respuesta),
    KEY ejecucion(ejecucion)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de no conformidades';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_conformidades;

-- lo recreamos
CREATE TRIGGER edicion_conformidades
AFTER UPDATE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_conformidades;

-- lo recreamos
CREATE TRIGGER eliminacion_conformidades
AFTER DELETE ON noconformidad
FOR EACH ROW
INSERT INTO auditoria_conformidades
    (id,
     usuario,
     fecha,
     titulo,
     detalle,
     causa,
     respuesta,
     accion,
     ejecucion,
     evento)
    VALUES
    (OLD.id,
     OLD.usuario,
     OLD.fecha,
     OLD.titulo,
     OLD.detalle,
     OLD.causa,
     OLD.respuesta,
     OLD.accion,
     OLD.ejecucion,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Tablas de Control de Stock                                     */
/*                                                                                          */
/********************************************************************************************/

/*

  Vamos a crear tablas para
  1. Items y su descripción
  2. Entradas al depósito
  3. Salidas del Depósito
  4. Inventario en existencia (que se actualiza en tiempo real)
  5. La tabla de fuentes de financiamiento (diccionario)

  Se crean triggers para los eventos y el inventario es controlado directamente por los
  triggers (un ingreso al depósito incrementará el inventario existente y una salida
  lo actualizará) esto permite simplificar el código (el inventario solo se consultará)
  y evitar errores de diseño

*/


/********************************************************************************************/
/*                                                                                          */
/*                               Items del Depósito                                         */
/*                                                                                          */
/********************************************************************************************/

/*
   id entero(4) autonumérico
   id_laboratorio entero(6) laboratorio que utiliza este item
   descripcion varchar(200)
   valor decimal valor de la unidad
   codigosop varchar(20) código del sistema de la apn
   imagen blob imagen del artículo
   critico int(4) valor mínimo crítico
   fecha date
   id_usuario int(4) usuario que editó el registro

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS items ;

-- la creamos
CREATE TABLE items (
    id int(4) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    imagen blob DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY laboratorio(id_laboratorio),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Elementos del inventario';

-- eliminamos la tabla
DROP TABLE IF EXISTS auditoria_items ;

-- la creamos
CREATE TABLE auditoria_items (
    id int(4) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    descripcion varchar(200) NOT NULL,
    valor decimal(10,2) UNSIGNED DEFAULT NULL,
    codigosop varchar(20) DEFAULT NULL,
    critico int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    KEY id(id),
    KEY laboratorio(id_laboratorio),
    KEY descripcion(descripcion),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los items';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_items;

-- lo recreamos
CREATE TRIGGER edicion_items
AFTER UPDATE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Edicion",
     OLD.id_usuario);

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_items;

-- lo recreamos
CREATE TRIGGER eliminacion_items
AFTER DELETE ON items
FOR EACH ROW
INSERT INTO auditoria_items
    (id,
     id_laboratorio,
     descripcion,
     valor,
     codigosop,
     critico,
     fecha,
     evento,
     id_usuario)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.descripcion,
     OLD.valor,
     OLD.codigosop,
     OLD.critico,
     OLD.fecha,
     "Eliminacion",
     OLD.id_usuario);


/********************************************************************************************/
/*                                                                                          */
/*                               Entradas del Depósito                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Ingresos al depósito
    id entero(6) clave del registro
    item entero(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    remito varchar(20) número o identificación del remito de entrada
    cantidad entero(5) número de unidades
    importe decimal(10,2) importe de la factura
    vencimiento fecha de vencimiento del producto
    lote varchar(20) número de lote del artículo
    ubicacion varchar(30) ubicación física del artículo
    fecha date fecha de entrada al depósito
    id_usuario int(4) usuario que ingresó el registro
    comentarios text observaciones

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS ingresos;

-- la recreamos
CREATE TABLE ingresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_usuario int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Ingresos al Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_ingresos;

-- la recreamos
CREATE TABLE auditoria_ingresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    importe decimal(10,2) UNSIGNED DEFAULT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_financiamiento int(1) UNSIGNED NOT NULL,
    remito varchar(20) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    vencimiento date DEFAULT NULL,
    lote varchar(20) DEFAULT NULL,
    ubicacion varchar(30) DEFAULT NULL,
    fecha date NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    comentarios text DEFAULT NULL,
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY id_financiamiento(id_financiamiento),
    KEY remito(remito),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de Ingresos al Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_ingresos
AFTER UPDATE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Edicion",
        OLD.comentarios);

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + NEW.cantidad - OLD.cantidad,
                                    inventario.valor = (inventario.existencia + NEW.cantidad - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
        inventario.item = NEW.item;

END; //

-- cambiamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ingresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_ingresos
AFTER DELETE ON ingresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_ingresos
        (id,
        item,
        importe,
        id_laboratorio,
        id_financiamiento,
        remito,
        cantidad,
        vencimiento,
        lote,
        ubicacion,
        fecha,
        id_usuario,
        evento,
        comentarios)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.importe,
        OLD.id_laboratorio,
        OLD.id_financiamiento,
        OLD.remito,
        OLD.cantidad,
        OLD.vencimiento,
        OLD.lote,
        OLD.ubicacion,
        OLD.fecha,
        OLD.id_usuario,
        "Eliminacion",
        OLD.comentarios);

    -- actualizamos el inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - OLD.cantidad,
                                    inventario.valor = (inventario.existencia - OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
        inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                               Salidas del Depósito                                       */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla con la información de salidas de material del depósito
    id entero(6) clave del registro
    item int(6) clave del item
    id_laboratorio entero(6) clave del laboratorio
    remito varchar(20) remito de salida
    cantidad int(5) numero de unidades salientes
    fecha date fecha de la operación
    id_entrego int(4) id del usuario que entregó el material
    id_recibio int(4) id del usuario que recibió el material
    comentarios text

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS egresos;

-- la recreamos
CREATE TABLE egresos (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    PRIMARY KEY(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Egresos del Depósito';

-- eliminamos la tabla de auditoria
DROP TABLE IF EXISTS auditoria_egresos;

CREATE TABLE auditoria_egresos (
    id int(6) UNSIGNED NOT NULL,
    item int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    remito int(6) DEFAULT NULL,
    cantidad int(5) UNSIGNED NOT NULL,
    fecha date DEFAULT NULL,
    id_entrego int(4) UNSIGNED NOT NULL,
    id_recibio int(4) UNSIGNED NOT NULL,
    comentarios text DEFAULT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY item(item),
    KEY id_laboratorio(id_laboratorio),
    KEY remito(remito),
    KEY id_entrego(id_entrego),
    KEY id_recibio(id_recibio)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de Egresos del Depósito';

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS edicion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER edicion_egresos
AFTER UPDATE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Edicion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia - NEW.cantidad + OLD.cantidad,
                                    inventario.valor = (inventario.existencia - NEW.cantidad + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
        inventario.item = NEW.item;

END; //

-- actualizamos el delimitador
DELIMITER ;

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_egresos;

-- cambiamos el delimitador
DELIMITER //

-- lo recreamos
CREATE TRIGGER eliminacion_egresos
AFTER DELETE ON egresos
FOR EACH ROW
BEGIN

    -- actualizamos la tabla de auditoría
    INSERT INTO auditoria_egresos
        (id,
        item,
        id_laboratorio,
        remito,
        cantidad,
        fecha,
        id_entrego,
        id_recibio,
        comentarios,
        evento)
        VALUES
        (OLD.id,
        OLD.item,
        OLD.id_laboratorio,
        OLD.remito,
        OLD.cantidad,
        OLD.fecha,
        OLD.id_entrego,
        OLD.id_recibio,
        OLD.comentarios,
        "Eliminacion");

    -- actualizamos la tabla de inventario
    UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                    inventario.item = items.id
                                SET inventario.existencia = inventario.existencia + OLD.cantidad,
                                    inventario.valor = (inventario.existencia + OLD.cantidad) * items.valor
    WHERE inventario.id_laboratorio = OLD.id_laboratorio AND
          inventario.item = OLD.item;

END; //

-- actualizamos el delimitador
DELIMITER ;


/********************************************************************************************/
/*                                                                                          */
/*                                      Inventario                                          */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de inventario, contiene el resumen y a través de la auditoría el historial de
    elementos en depósito
    id entero(6) clave del registro
    id_laboratorio entero(6) clave del laboratorio
    valor decimal(10,2) valor en existencia (calculado al momento de actualizar la tabla)
    item entero(6) clave del item
    existencia entero(5) número de unidades en existencia

*/

-- eliminamos la tabla
DROP TABLE IF EXISTS inventario;

-- la creamos
CREATE TABLE inventario (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Inventario en existencia';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_inventario;

-- la recreamos (la clave primaria aquí es el registro de la
-- transacción)
CREATE TABLE auditoria_inventario (
    id int(6) UNSIGNED NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    valor decimal(10,2) DEFAULT NULL,
    item int(6) UNSIGNED NOT NULL,
    existencia int(5) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Ingreso", "Egreso", "Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY item(item)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del Inventario';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_inventario;

-- lo recreamos
CREATE TRIGGER edicion_inventario
AFTER UPDATE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_inventario;

-- lo recreamos
CREATE TRIGGER eliminacion_inventario
AFTER DELETE ON inventario
FOR EACH ROW
INSERT INTO auditoria_inventario
    (id,
     id_laboratorio,
     valor,
     item,
     existencia,
     evento)
    VALUES
    (OLD.id,
     OLD.id_laboratorio,
     OLD.valor,
     OLD.item,
     OLD.existencia,
     "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           La tabla de pedidos                                            */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de pedidos al depósito, esta tabla permite que los usuarios realicen pedidos de
    material al depósito y luego el usuario autorizado pueda ver la nómina de pedidos
    y aprobarlos (con lo que genera un egreso automáticamente) o rechazarlos
    id int(8) clave del registro
    id_item int(4) clave del item solicitado
    id_usuario int(4) clave del usuario que solicitó
    cantidad int(4) número de unidades solicitadas
    fecha date fecha de la solicitud

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS pedidos;

-- la recreamos
CREATE TABLE pedidos(
    id int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pedidos al depósito';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_pedidos;

-- la recreamos
CREATE TABLE auditoria_pedidos(
    id int(8) UNSIGNED NOT NULL,
    id_item int(4) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    cantidad int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_item(id_item),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de pedidos al depósito';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_pedidos;

-- lo recreamos
CREATE TRIGGER edicion_pedidos
AFTER UPDATE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_pedidos;

-- lo recreamos
CREATE TRIGGER eliminacion_pedidos
AFTER DELETE ON pedidos
FOR EACH ROW
INSERT INTO auditoria_pedidos
       (id,
        id_item,
        id_usuario,
        cantidad,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.id_item,
        OLD.id_usuario,
        OLD.cantidad,
        OLD.fecha_alta,
        "Eliminacion");


/********************************************************************************************/
/*                                                                                          */
/*                           Fuentes de Financiamiento                                      */
/*                                                                                          */
/********************************************************************************************/

/*

    Tabla de fuentes de financiamiento, no es necesario que sea exclusiva del stock pero
    aquí lo utilizamos para indicar la fuente de financiamiento de los ingresos al
    depósito, este diccionario es individual para cada laboratorio
    id int(1) clave del registro
    fuente varchar(50) descripcion del financiamiento
    id_laboratorio int(6) clave del laboratorio
    id_usuario int(4) clave del usuario
    fecha_alta date fecha del registro

*/

-- la eliminamos si no existe
DROP TABLE IF EXISTS financiamiento;

-- la recreamos
CREATE TABLE financiamiento(
    id int(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario con las fuentes de financiamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_financiamiento;

-- la recreamos
CREATE TABLE auditoria_financiamiento(
    id int(1) UNSIGNED NOT NULL,
    fuente varchar(50) NOT NULL,
    id_laboratorio int(6) UNSIGNED NOT NULL,
    id_usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY id_laboratorio(id_laboratorio),
    KEY id_usuario(id_usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las fuentes de financiamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_financiamiento;

-- lo recreamos
CREATE TRIGGER edicion_financiamiento
AFTER UPDATE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_financiamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_financiamiento
AFTER DELETE ON financiamiento
FOR EACH ROW
INSERT INTO auditoria_financiamiento
       (id,
        fuente,
        id_laboratorio,
        id_usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.fuente,
        OLD.id_laboratorio,
        OLD.id_usuario,
        OLD.fecha_alta,
        "Eliminacion");


/*

    Aquí creamos los triggers que se van a encargar de controlar el inventario, usamos el
    disparador BEFORE (antes) para usar el otro evento ya que MySQL solo soporta un trigger
    por evento, el concepto es
    - Cuando se elimina una entrada al depósito, resta esa entrada del existente
    - Cuando se elimina una salida al depósito, suma esa salida al existente
    - Cuando se inserta una entrada al depósito, suma esa entrada al existente
    - Cuando se inserta una salida del depósito, resta esa entrada al existente
    - Cuando se edita una entrada, resta la entrada anterior y suma la nueva
    - Cuando se edita una salida, suma la salida anterior y resta la nueva
    Tendremos entonces seis triggers (tres para la tabla de entradas y tres para la tabla
    de salidas)

    Creamos también un trigger para la inserción de items en el diccionario que inicializa
    a cero la tabla de inventario, de tal forma nos aseguramos que todos los items del
    diccionario tienen su correspondencia en el inventario

*/

-- eliminamos el trigger si existe
DROP TRIGGER IF EXISTS insercion_items;

-- lo recreamos desde cero
CREATE TRIGGER insercion_items
AFTER INSERT ON items
FOR EACH ROW
INSERT INTO inventario
    (id_laboratorio,
     valor,
     item,
     existencia)
    VALUES
    (NEW.id_laboratorio,
     NEW.valor,
     NEW.id,
     '0');

-- eliminamos el trigger de inserción de entradas al depósito
DROP TRIGGER IF EXISTS nueva_entrada_inventario;

-- lo recreamos
CREATE TRIGGER nueva_entrada_inventario
AFTER INSERT ON ingresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia + NEW.cantidad,
                                 inventario.valor = (inventario.existencia + NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;

-- eliminamos el trigger de inserción de salidas al depósito
DROP TRIGGER IF EXISTS nueva_salida_inventario;

-- lo recreamos
CREATE TRIGGER nueva_salida_inventario
AFTER INSERT ON egresos
FOR EACH ROW
UPDATE inventario JOIN items ON inventario.id_laboratorio = items.id_laboratorio AND
                                inventario.item = items.id
                             SET inventario.existencia = inventario.existencia - NEW.cantidad,
                                 inventario.valor = (inventario.existencia - NEW.cantidad) * items.valor
WHERE inventario.id_laboratorio = NEW.id_laboratorio AND
      inventario.item = NEW.item;


/***********************************************************************************************/
/*                                                                                             */
/*                              Freezers y Heladeras                                           */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla que contiene la información sobre las heladeras a controlar
    id int(6) clave del registro
    laboratorio int(6) clave del laboratorio de la heladera
    marca varchar(50) marca de la heladera
    ubicación varchar(100) descripción de donde se encuentra
    patrimonio varchar(100) cadena con la id de patrimonio
    temperatura int(3) temperatura que que debe estar (con signo)
    temperatura2 int(3) otra temperatura por defecto (para heladeras de dos)
    tolerancia int(2) porcentaje de tolerancia en la temperatura
    usuario int(4) clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS heladeras;

-- la recreamos
CREATE TABLE heladeras (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    laboratorio int(6) UNSIGNED NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de heladeras';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_heladeras;

-- la recreamos
CREATE TABLE auditoria_heladeras (
    id int(6) UNSIGNED NOT NULL,
    laboratorio int(6) NOT NULL,
    marca varchar(50) NOT NULL,
    ubicacion varchar(100) DEFAULT NULL,
    patrimonio varchar(100) DEFAULT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    tolerancia int(2) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de auditoría de heladeras';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_heladeras;

-- lo recreamos
CREATE TRIGGER edicion_heladeras
AFTER UPDATE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_heladeras;

CREATE TRIGGER eliminacion_heladeras
AFTER DELETE ON heladeras
FOR EACH ROW
INSERT INTO auditoria_heladeras
       (id,
        laboratorio,
        marca,
        ubicacion,
        patrimonio,
        temperatura,
        temperatura2,
        tolerancia,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.laboratorio,
        OLD.marca,
        OLD.ubicacion,
        OLD.patrimonio,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.tolerancia,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***********************************************************************************************/
/*                                                                                             */
/*                                     Temperaturas                                            */
/*                                                                                             */
/***********************************************************************************************/

/*

    Tabla con la información de las temperaturas de las heladeras
    id int(6) clave del registro
    heladera int(2) clave de la heladera
    fecha date fecha y hora del registro
    temperatura int(3) temperatura medida con signo
    temperatura2 int(3) segunda temperatura
    controlado int(4) clave del usuario

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS temperaturas;

-- la recreamos
CREATE TABLE temperaturas(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    heladera int(2) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    PRIMARY KEY(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tabla con datos de temperaturas';

-- eliminamos la tabla de auditorías si existe
DROP TABLE IF EXISTS auditoria_temperaturas;

-- la recreamos
CREATE TABLE auditoria_temperaturas(
    id int(6) UNSIGNED NOT NULL,
    heladera int(2) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    temperatura int(3) NOT NULL,
    temperatura2 int(3) NOT NULL,
    controlado int(4) UNSIGNED NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY heladera(heladera),
    KEY controlado(controlado)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditorìa de la tabla de temperaturas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_temperaturas;

-- lo recreamos
CREATE TRIGGER edicion_temperaturas
AFTER UPDATE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_temperaturas;

-- lo recreamos
CREATE TRIGGER eliminacion_temperaturas
AFTER DELETE ON temperaturas
FOR EACH ROW
INSERT INTO auditoria_temperaturas
       (id,
        heladera,
        fecha,
        temperatura,
        temperatura2,
        controlado,
        evento)
       VALUES
       (OLD.id,
        OLD.heladera,
        OLD.fecha,
        OLD.temperatura,
        OLD.temperatura2,
        OLD.controlado,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                       Tipos de Derivación                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla es el diccionario de derivaciones su edición quedará
    restringida a los usuarios del nivel central, no tiene auditoría
    id entero pequeño, clave del registro
    derivacion texto descripción del motivo
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla de motivos si existe
DROP TABLE IF EXISTS derivacion;

-- la recreamos
CREATE TABLE derivacion (
    id tinyint(1) UNSIGNED NOT NULL AUTO_INCREMENT,
    derivacion varchar(100) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Tipos de derivación';

-- insertamos los valores iniciales
INSERT INTO derivacion (derivacion, usuario) VALUES ("Institución", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Profesional", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Personal", 1);
INSERT INTO derivacion (derivacion, usuario) VALUES ("Familiar", 1);


/***************************************************************************/
/*                                                                         */
/*                              Chagas Agudo                               */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los antecedentes de chagas agudo, solo podrá ser
    editada por administradores del nivel central, debería haber un solo
    registro por cada paciente

    id int(6) clave del registro
    paciente int(8) clave del paciente
    sintomas si ha tenido síntomas y signos anteriormente
    observaciones texto, observaciones del usuario
    usuario clave del usuario
    fecha date, fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS agudo;

-- la recreamos
CREATE TABLE agudo (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de chagas agudo';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_agudo;

-- la recreamos
CREATE TABLE auditoria_agudo (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    sintomas tinyint(1) UNSIGNED DEFAULT 0,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum ("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoria de chagas agudo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_agudo;

-- lo recreamos
CREATE TRIGGER edicion_agudo
AFTER UPDATE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.sintomas,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_agudo;

-- lo recreamos
CREATE TRIGGER eliminacion_agudo
AFTER DELETE ON agudo
FOR EACH ROW
INSERT INTO auditoria_agudo
    (id,
     paciente,
     sintomas,
     observaciones,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.paciente,
     OLD.sintomas,
     OLD.observaciones,
     OLD.usuario,
     OLD.fecha,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Drogas                                     */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de drogas utilizadas para el tratamiento

    id int clave del registro
    droga varchar nombre de la droga
    dosis int dosis en miligramos
    comercial nombre comercial del medicamento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS drogas;

-- la recreamos
CREATE TABLE drogas (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de drogas utilizadas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_drogas;

-- la recreamos
CREATE TABLE auditoria_drogas (
    id int(2) UNSIGNED NOT NULL,
    droga varchar(200) DEFAULT NULL,
    dosis int(3) UNSIGNED NOT NULL,
    comercial varchar(200) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de drogas utilizadas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_drogras;

-- lo recreamos
CREATE TRIGGER edicion_drogas
AFTER UPDATE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_drogas;

-- lo recreamos
CREATE TRIGGER eliminacion_drogas
AFTER DELETE ON drogas
FOR EACH ROW
INSERT INTO auditoria_drogas
       (id,
        droga,
        dosis,
        comercial,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.droga,
        OLD.dosis,
        OLD.comercial,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                          Efectos Adversos                               */
/*                                                                         */
/***************************************************************************/

/*

    Diccionario de efectos adversos al tratamiento

    id clave del registro
    descripcion texto descripción del efecto
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS adversos;

-- la recreamos
CREATE TABLE adversos (
    id int(2) UNSIGNED NOT NULL AUTO_INCREMENT,
    descripcion varchar(250) NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Diccionario de efectos adversos';


/***************************************************************************/
/*                                                                         */
/*                              Tratamiento                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene el esquema de tratamiento de cada paciente, puede
    tener varias entradas por cada paciente

    id clave del registro
    paciente clave del paciente
    droga clave de la droga utilizada
    comprimidos cantidad de comprimidos diarios
    inicio date fecha de inicio del tratamiento
    fin date fecha de finalización del tratamiento
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS tratamiento;

-- la recreamos
CREATE TABLE tratamiento (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    droga int(2) UNSIGNED DEFAULT NULL,
    comprimidos tinyint(1) UNSIGNED DEFAULT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY paciente(paciente),
    KEY droga(droga),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del tratamiento';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_tratamiento;

-- la recreamos
CREATE TABLE auditoria_tratamiento (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    droga int(2) UNSIGNED DEFAULT NULL,
    comprimidos tinyint(1) UNSIGNED DEFAULT NULL,
    inicio date NOT NULL,
    fin date NOT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del tratamiento';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_tratamiento;

-- lo recreamos
CREATE TRIGGER edicion_tratamiento
AFTER UPDATE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             droga,
             comprimidos,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.droga,
             OLD.comprimidos,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_tratamiento;

-- lo recreamos
CREATE TRIGGER eliminacion_tratamiento
AFTER DELETE ON tratamiento
FOR EACH ROW
INSERT INTO auditoria_tratamiento
            (id,
             paciente,
             droga,
             comprimidos,
             inicio,
             fin,
             usuario,
             fecha,
             evento)
            VALUES
            (OLD.id,
             OLD.paciente,
             OLD.droga,
             OLD.comprimidos,
             OLD.inicio,
             OLD.fin,
             OLD.usuario,
             OLD.fecha,
             'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                              Antecedentes                               */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes tóxicos del paciente esta tendría que haber
    un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    fuma entero (0 si no fuma)
    alcohol entero (o si no toma)
    bebida enum tipo de bebida
    adicciones texto descripción de las adicciones
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS antecedentes;

-- la recreamos
CREATE TABLE antecedentes(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_antecedentes;

-- la recreamos
CREATE TABLE auditoria_antecedentes(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    fuma tinyint(1) UNSIGNED DEFAULT 0,
    alcohol tinyint(1) UNSIGNED DEFAULT 0,
    bebida enum('Fermentada', 'Vino', 'Destilada', 'Otra') DEFAULT NULL,
    adicciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes del paciente';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_antecedentes;

-- lo recreamos
CREATE TRIGGER edicion_antecedentes
AFTER UPDATE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        alcohol,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.alcohol,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_antecedentes;

-- lo recreamos
CREATE TRIGGER eliminacion_antecedentes
AFTER DELETE ON antecedentes
FOR EACH ROW
INSERT INTO auditoria_antecedentes
       (id,
        paciente,
        fuma,
        alcohol,
        bebida,
        adicciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.fuma,
        OLD.alcohol,
        OLD.bebida,
        OLD.adicciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                                 Síntomas                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los síntomas del paciente, esta tabla debería tener una entrada
    por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    disnea enum tipo de disnea
    palpitaciones enum tipo de palpitaciones
    precordial enum si tiene dolor precordial
    conciencia int 0 false 1 verdadero
    presincope int 0 false 1 verdadero
    edema int 0 false 1 verdadero si tiene edema de miembros inferiores
    observaciones texto observaciones del usuario
    usuario entero clave del registro
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS sintomas;

-- la recreamos
CREATE TABLE sintomas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia tinyint(1) UNSIGNED DEFAULT 0,
    presincope tinyint(1) UNSIGNED DEFAULT 0,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Síntomas del paciente';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_sintomas;

-- la recreamos
CREATE TABLE auditoria_sintomas (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    disnea enum('Esfuerzos Leves', 'Esfuerzos Moderados', 'Grandes Esfuerzos', 'En Reposo', 'DPN', 'No Tiene') DEFAULT 'No Tiene',
    palpitaciones enum('Raramente', 'Frecuentes', 'No Tiene') DEFAULT 'No Tiene',
    precordial enum('Tipico', 'Atipico', 'No Tiene') DEFAULT 'No Tiene',
    conciencia tinyint(1) UNSIGNED DEFAULT 0,
    presincope tinyint(1) UNSIGNED DEFAULT 0,
    edema enum('Si', 'No') DEFAULT 'No',
    observaciones text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum('Edicion', 'Eliminacion'),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de síntomas del paciente';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_sintomas;

-- lo recreamos
CREATE TRIGGER edicion_sintomas
AFTER UPDATE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        visita,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Edicion');

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_sintomas;

-- lo recreamos
CREATE TRIGGER eliminacion_sintomas
AFTER DELETE ON sintomas
FOR EACH ROW
INSERT INTO auditoria_sintomas
       (id,
        paciente,
        visita,
        disnea,
        palpitaciones,
        precordial,
        conciencia,
        presincope,
        edema,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.disnea,
        OLD.palpitaciones,
        OLD.precordial,
        OLD.conciencia,
        OLD.presincope,
        OLD.edema,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        'Eliminacion');


/***************************************************************************/
/*                                                                         */
/*                              disautonomia                               */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de disautonía del paciente, esta también debería
    haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    hipotensión tinyint si tiene hipotensión (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    astenia tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS disautonomia;

-- la recreamos
CREATE TABLE disautonomia(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_disautonomia;

-- la recreamos
CREATE TABLE auditoria_disautonomia(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    hipotension tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    astenia tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de disautonía';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_disautonomia;

-- lo recreamos
CREATE TRIGGER edicion_disautonomia
AFTER UPDATE ON disautonomia
FOR EACH ROW
INSERT INTO auditoria_disautonomia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_disautonomia;

-- lo recreamos
CREATE TRIGGER eliminacion_disautonomia
AFTER DELETE ON disautonomia
FOR EACH ROW
INSERT INTO auditoria_disautonomia
       (id,
        paciente,
        hipotension,
        bradicardia,
        astenia,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.hipotension,
        OLD.bradicardia,
        OLD.astenia,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Digestivo                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes del compromiso digestivo, esta tabla tendría
    una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    disfagia tinyint (0 falso - 1 verdadero)
    pirosis tinyint (0 falso - 1 verdadero)
    regurgitacion tinyint (0 falso - 1 verdadero)
    constipacion tinyint (0 falso - 1 verdadero)
    bolo tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS digestivo;

-- la recreamos
CREATE TABLE digestivo(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de compromiso digestivo';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_digestivo;

-- la recreamos
CREATE TABLE auditoria_digestivo(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    disfagia tinyint(1) DEFAULT 0,
    pirosis tinyint(1) DEFAULT 0,
    regurgitacion tinyint(1) DEFAULT 0,
    constipacion tinyint(1) DEFAULT 0,
    bolo tinyint(1) DEFAULT 0,
    notiene tinyint(1) DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion") NOT NULL,
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del compromiso digestivo';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_digestivo;

-- lo recreamos
CREATE TRIGGER edicion_digestivo
AFTER UPDATE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_digestivo;

-- lo recreamos
CREATE TRIGGER eliminacion_digestivo
AFTER DELETE ON digestivo
FOR EACH ROW
INSERT INTO auditoria_digestivo
       (id,
        paciente,
        disfagia,
        pirosis,
        regurgitacion,
        constipacion,
        bolo,
        notiene,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.disfagia,
        OLD.pirosis,
        OLD.regurgitacion,
        OLD.constipacion,
        OLD.bolo,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                          Examan Físico                                  */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con el resultado del examen físico

    id entero clave del registro
    protocolo entero clave del paciente
    visita clave de la visita
    ta string presión arterial
    peso entero peso en kilos
    fc entero frecuencia cardíaca
    spo entero saturación de oxígeno en porcentaje
    talla flotante con dos decimales altura
    bmi flotante índice de masa corporal
    edema entero pequeño si hay edema de miembros inferiores
          0 falso - 1 verdadero
    sp entero pequeño 0 falso - 1 verdadero
    mvdisminuido entero pequeño 0 falso - 1 verdadero
    crepitantes entero pequeño 0 falso - 1 verdadero
    sibilancias entero pequeño 0 falso - 1 verdadero

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS fisico;

-- la recreamos
CREATE TABLE fisico(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ta varchar(7) NOT NULL,
    peso decimal(5,2) UNSIGNED NOT NULL,
    fc int(3) UNSIGNED NOT NULL,
    spo int(3) UNSIGNED NOT NULL,
    talla decimal(3,2) UNSIGNED NOT NULL,
    bmi decimal(4,2) UNSIGNED NOT NULL,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    sp tinyint(1) UNSIGNED DEFAULT 0,
    mvdisminuido tinyint(1) UNSIGNED DEFAULT 0,
    crepitantes tinyint(1) UNSIGNED DEFAULT 0,
    sibilancias tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Resultados del examen fìsico';

-- eliminamos la tabla de auditorìa si existe
DROP TABLE IF EXISTS auditoria_fisico;

-- la recreamos
CREATE TABLE auditoria_fisico(
    id int(6) UNSIGNED NOT NULL,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ta varchar(7) NOT NULL,
    peso decimal(5,2) UNSIGNED NOT NULL,
    fc int(3) UNSIGNED NOT NULL,
    spo int(3) UNSIGNED NOT NULL,
    talla decimal(3,2) UNSIGNED NOT NULL,
    bmi decimal(4,2) UNSIGNED NOT NULL,
    edema tinyint(1) UNSIGNED DEFAULT 0,
    sp tinyint(1) UNSIGNED DEFAULT 0,
    mvdisminuido tinyint(1) UNSIGNED DEFAULT 0,
    crepitantes tinyint(1) UNSIGNED DEFAULT 0,
    sibilancias tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY visita(visita),
    KEY protocolo(protocolo),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del examen fìsico';

-- eliminamos el trigger de ediciòn si existe
DROP TRIGGER IF EXISTS edicion_fisico;

-- lo recreamos
CREATE TRIGGER edicion_fisico
AFTER UPDATE ON fisico
FOR EACH ROW
INSERT INTO auditoria_fisico
    (id,
     protocolo,
     visita,
     ta,
     peso,
     fc,
     spo,
     talla,
     bmi,
     edema,
     sp,
     mvdisminuido,
     crepitantes,
     sibilancias,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.protocolo,
     OLD.visita,
     OLD.ta,
     OLD.peso,
     OLD.fc,
     OLD.spo,
     OLD.talla,
     OLD.bmi,
     OLD.edema,
     OLD.sp,
     OLD.mvdisminuido,
     OLD.crepitantes,
     OLD.sibilancias,
     OLD.usuario,
     OLD.fecha,
     "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_fisico;

-- lo recreamos
CREATE TRIGGER eliminacion_fisico
AFTER DELETE ON fisico
FOR EACH ROW
INSERT INTO auditoria_fisico
    (id,
     protocolo,
     visita,
     ta,
     peso,
     fc,
     spo,
     talla,
     bmi,
     edema,
     sp,
     mvdisminuido,
     crepitantes,
     sibilancias,
     usuario,
     fecha,
     evento)
    VALUES
    (OLD.id,
     OLD.protocolo,
     OLD.visita,
     OLD.ta,
     OLD.peso,
     OLD.fc,
     OLD.spo,
     OLD.talla,
     OLD.bmi,
     OLD.edema,
     OLD.sp,
     OLD.mvdisminuido,
     OLD.crepitantes,
     OLD.sibilancias,
     OLD.usuario,
     OLD.fecha,
     "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                         Cardiovascular                                  */
/*                                                                         */
/***************************************************************************/

/*

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    ausnormal tinyint(1) (0 falso - 1 verdadero) auscultación normal
    irregular tinyint(1) (0 falso - 1 verdadero) auscultación irregular
    3er tinyint(1) (0 falso - 1 verdadero)
    4to tinyint(1) (0 falso - 1 verdadero)
    eyectivo tinyint(1) (0 falso - 1 verdadero) soplo eyectivo
    regurgitativo tinyint(1) (0 falso - 1 verdadero) soplo mitral
    sinsistolico tinyint(1) (0 falso - 1 verdadero) no tiene soplo sistólico
    aortico tinyint(1) (0 falso - 1 verdadero) soplo diastólico aórtico
    diastolicomitral tinyint(1) (0 falso - 1 verdadero) soplo diastólico mitral
    sindiastolico tinyint(1) (0 falso - 1 verdadero)
    hepatomegalia tinyint (0 falso - 1 verdadero)
    esplenomegalia tinyint (0 falso - 1 verdadero)
    ingurgitacion tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS cardiovascular;

-- la recreamos
CREATE TABLE cardiovascular(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ausnormal tinyint(1) UNSIGNED DEFAULT 0,
    irregular tinyint(1) UNSIGNED DEFAULT 0,
    3er tinyint(1) UNSIGNED DEFAULT 0,
    4to tinyint(1) UNSIGNED DEFAULT 0,
    eyectivo tinyint(1) UNSIGNED DEFAULT 0,
    regurgitativo tinyint(1) UNSIGNED DEFAULT 0,
    sinsistolico tinyint(1) UNSIGNED DEFAULT 0,
    aortico tinyint(1) UNSIGNED DEFAULT 0,
    diastolicomitral tinyint(1) UNSIGNED DEFAULT 0,
    sindiastolico tinyint(1) UNSIGNED DEFAULT 0,
    hepatomegalia tinyint(1) UNSIGNED DEFAULT 0,
    esplenomegalia tinyint(1) UNSIGNED DEFAULT 0,
    ingurgitacion tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del aparato cardiovascular';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_cardiovascular;

-- la recreamos
CREATE TABLE auditoria_cardiovascular(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    ausnormal tinyint(1) UNSIGNED DEFAULT 0,
    irregular tinyint(1) UNSIGNED DEFAULT 0,
    3er tinyint(1) UNSIGNED DEFAULT 0,
    4to tinyint(1) UNSIGNED DEFAULT 0,
    eyectivo tinyint(1) UNSIGNED DEFAULT 0,
    regurgitativo tinyint(1) UNSIGNED DEFAULT 0,
    sinsistolico tinyint(1) UNSIGNED DEFAULT 0,
    aortico tinyint(1) UNSIGNED DEFAULT 0,
    diastolicomitral tinyint(1) UNSIGNED DEFAULT 0,
    sindiastolico tinyint(1) UNSIGNED DEFAULT 0,
    hepatomegalia tinyint(1) UNSIGNED DEFAULT 0,
    esplenomegalia tinyint(1) UNSIGNED DEFAULT 0,
    ingurgitacion tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del aparato cardiovascular';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS auditoria_cardiovascular;

-- lo recreamos
CREATE TRIGGER edicion_cardiovascular
AFTER UPDATE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        visita,
        ausnormal,
        irregular,
        3er,
        4to,
        eyectivo,
        regurgitativo,
        sinsistolico,
        aortico,
        diastolicomitral,
        sindiastolico,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.ausnormal,
        OLD.irregular,
        OLD.3er,
        OLD.4to,
        OLD.eyectivo,
        OLD.regurgitativo,
        OLD.sinsistolico,
        OLD.aortico,
        OLD.diastolicomitral,
        OLD.sindiastolico,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_cardivascular;

-- lo recreamos
CREATE TRIGGER eliminacion_cardiovascular
AFTER DELETE ON cardiovascular
FOR EACH ROW
INSERT INTO auditoria_cardiovascular
       (id,
        paciente,
        visita,
        ausnormal,
        irregular,
        3er,
        4to,
        eyectivo,
        regurgitativo,
        sinsistolico,
        aortico,
        diastolicomitral,
        sindiastolico,
        hepatomegalia,
        esplenomegalia,
        ingurgitacion,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.ausnormal,
        OLD.irregular,
        OLD.3er,
        OLD.4to,
        OLD.eyectivo,
        OLD.regurgitativo,
        OLD.sinsistolico,
        OLD.aortico,
        OLD.diastolicomitral,
        OLD.sindiastolico,
        OLD.hepatomegalia,
        OLD.esplenomegalia,
        OLD.ingurgitacion,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                               Rx                                        */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene información sobre las radiografías y puede haber
    mas de una entrada por paciente

    id clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma de la radiografía
    ict tinyint (0 falso - 1 verdadero)
    cardiomegalia tinyint (0 falso - 1 verdadero)
    pleuro tinyint (0 falso - 1 verdadero)
    epoc tinyint (0 falso - 1 verdadero)
    calcificaciones tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS rx;

-- la recreamos
CREATE TABLE rx (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    ict tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Información de las radiografías de torax';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_rx;

-- la recreamos
CREATE TABLE auditoria_rx (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    ict tinyint(1) UNSIGNED DEFAULT 0,
    cardiomegalia tinyint(1) UNSIGNED DEFAULT 0,
    pleuro tinyint(1) UNSIGNED DEFAULT 0,
    epoc tinyint(1) UNSIGNED DEFAULT 0,
    calcificaciones tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de las radiografías';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_rx;

-- lo recreamos
CREATE TRIGGER edicion_rx
AFTER UPDATE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        visita,
        fecha,
        ict,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.ict,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_rx;

-- lo recreamos
CREATE TRIGGER eliminacion_rx
AFTER DELETE ON rx
FOR EACH ROW
INSERT INTO auditoria_rx
       (id,
        paciente,
        visita,
        fecha,
        ict,
        cardiomegalia,
        pleuro,
        epoc,
        calcificaciones,
        notiene,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.ict,
        OLD.cardiomegalia,
        OLD.pleuro,
        OLD.epoc,
        OLD.calcificaciones,
        OLD.notiene,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                         Electrocardiograma                              */
/*                                                                         */
/***************************************************************************/

/*

    Datos de los electrocardiograma, esta tabla debe tener una entrada
    por cada visita

    id entero clave del registro
    protocolo entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma del electro
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    fc entero frecuencia cardìaca
    arritmia enum
    qrs decimal 3,2 fracción de segundo
    ejeqrs entero (desviación en grados)
    pr decimal 3,2 fracción de segundo
    brd tinyint (0 falso - 1 verdadero)
    brdmoderado tinyint (0 falso - 1 verdadero)
    bcrd tinyint (0 falso - 1 verdadero)
    tendenciahbai tinyint (0 falso - 1 verdadero)
    hbai tinyint (0 falso - 1 verdadero)
    tciv tinyint (0 falso - 1 verdadero)
    bcri tinyint (0 falso - 1 verdadero)
    bav1g tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    bav3g tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    fibrosis tinyint (0 falso - 1 verdadero)
    aumentoai tinyint (0 falso - 1 verdadero)
    wpv tinyint (0 falso - 1 verdadero)
    hvi tinyint (0 falso - 1 verdadero)
    sintranstornos tinyint(1) (0 falso - 1 verdadero)
    repolarizacion enum
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS electrocardiograma;

-- la recreamos
CREATE TABLE electrocardiograma (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    fc int(3) UNSIGNED NOT NULL,
    arritmia enum("FA", "EVF", "Pares", "TV", "No Tiene") DEFAULT "No Tiene",
    qrs decimal(3,2) UNSIGNED NOT NULL,
    ejeqrs int(2) UNSIGNED NOT NULL,
    pr decimal(3,2) UNSIGNED NOT NULL,
    brd tinyint(1) UNSIGNED DEFAULT 0,
    brdmoderado tinyint(1) UNSIGNED DEFAULT 0,
    bcrd tinyint(1) UNSIGNED DEFAULT 0,
    tendenciahbai tinyint(1) UNSIGNED DEFAULT 0,
    hbai tinyint(1) UNSIGNED DEFAULT 0,
    tciv tinyint(1) UNSIGNED DEFAULT 0,
    bcri tinyint(1) UNSIGNED DEFAULT 0,
    bav1g tinyint(1) UNSIGNED DEFAULT 0,
    bav2g tinyint(1) UNSIGNED DEFAULT 0,
    bav3g tinyint(1) UNSIGNED DEFAULT 0,
    fibrosis tinyint(1) UNSIGNED DEFAULT 0,
    aumentoai tinyint(1) UNSIGNED DEFAULT 0,
    wpv tinyint(1) UNSIGNED DEFAULT 0,
    hvi tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    sintranstornos tinyint(1) UNSIGNED DEFAULT 0,
    repolarizacion enum("Transt. Lábil", "Transt. Isquémico", "Difusos Primarios", "No Tiene") DEFAULT "No Tiene",
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los electrocardiogramas';

-- eliminamos la tabla de auditoría si existe
DROP TABLE IF EXISTS auditoria_electro;

-- la recreamos
CREATE TABLE auditoria_electro (
    id int(6) UNSIGNED NOT NULL ,
    protocolo int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(1) UNSIGNED DEFAULT 0,
    fc int(3) UNSIGNED NOT NULL,
    arritmia enum("FA", "EVF", "Pares", "TV", "No Tiene") DEFAULT "No Tiene",
    qrs decimal(3,2) UNSIGNED NOT NULL,
    ejeqrs int(2) UNSIGNED NOT NULL,
    pr decimal(3,2) UNSIGNED NOT NULL,
    brd tinyint(1) UNSIGNED DEFAULT 0,
    brdmoderado tinyint(1) UNSIGNED DEFAULT 0,
    bcrd tinyint(1) UNSIGNED DEFAULT 0,
    tendenciahbai tinyint(1) UNSIGNED DEFAULT 0,
    hbai tinyint(1) UNSIGNED DEFAULT 0,
    tciv tinyint(1) UNSIGNED DEFAULT 0,
    bcri tinyint(1) UNSIGNED DEFAULT 0,
    bav1g tinyint(1) UNSIGNED DEFAULT 0,
    bav2g tinyint(1) UNSIGNED DEFAULT 0,
    bav3g tinyint(1) UNSIGNED DEFAULT 0,
    fibrosis tinyint(1) UNSIGNED DEFAULT 0,
    aumentoai tinyint(1) UNSIGNED DEFAULT 0,
    wpv tinyint(1) UNSIGNED DEFAULT 0,
    hvi tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    sintranstornos tinyint(1) UNSIGNED DEFAULT 0,
    repolarizacion enum("Transt. Lábil", "Transt. Isquémico", "Difusos Primarios", "No Tiene") DEFAULT "No Tiene",
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY protocolo(protocolo),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los electrocardiogramas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_electro;

-- lo recreamos
CREATE TRIGGER edicion_electro
AFTER UPDATE ON electrocardiograma
FOR EACH ROW
INSERT INTO auditoria_electro
       (id,
        protocolo,
        visita,
        fecha,
        normal,
        bradicardia,
        fc,
        arritmia,
        ars,
        ejeqrs,
        pr,
        brd,
        brdmoderado,
        bcrd,
        tendenciahbai,
        hbai,
        tciv,
        bcri,
        bav1g,
        bav2,
        bav3,
        fibrosis,
        aumentoai,
        wpv,
        hvi,
        notiene,
        sintranstornos,
        repolarizacion,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.protocolo,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.bradicardia,
        OLD.fc,
        OLD.arritmia,
        OLD.qrs,
        OLD.ejeqrs,
        OLD.pr,
        OLD.brd,
        OLD.brdmoderado,
        OLD.bcrd,
        OLD.tendenciahbai,
        OLD.hbai,
        OLD.tciv,
        OLD.bcri,
        OLD.bav1g,
        OLD.bav2g,
        OLD.bav3g,
        OLD.fibrosis,
        OLD.aumentoai,
        OLD.wpv,
        OLD.hvi,
        OLD.notiene,
        OLD.sintranstornos,
        OLD.repolarizacion,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_electro;

-- lo recreamos
CREATE TRIGGER eliminacion_electro
AFTER DELETE ON electrocardiograma
FOR EACH ROW
INSERT INTO auditoria_electro
       (id,
        protocolo,
        visita,
        fecha,
        normal,
        bradicardia,
        fc,
        arritmia,
        ars,
        ejeqrs,
        pr,
        brd,
        brdmoderado,
        bcrd,
        tendenciahbai,
        hbai,
        tciv,
        bcri,
        bav1g,
        bav2,
        bav3,
        fibrosis,
        aumentoai,
        wpv,
        hvi,
        notiene,
        sintranstornos,
        repolarizacion,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.protocolo,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.bradicardia,
        OLD.fc,
        OLD.arritmia,
        OLD.qrs,
        OLD.ejeqrs,
        OLD.pr,
        OLD.brd,
        OLD.brdmoderado,
        OLD.bcrd,
        OLD.tendenciahbai,
        OLD.hbai,
        OLD.tciv,
        OLD.bcri,
        OLD.bav1g,
        OLD.bav2g,
        OLD.bav3g,
        OLD.fibrosis,
        OLD.aumentoai,
        OLD.wpv,
        OLD.hvi,
        OLD.notiene,
        OLD.sintranstornos,
        OLD.repolarizacion,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                           Ecocardiograma                                */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los datos de los ecocardiogramas, los trae el paciente y pueden
    haber entradas (varias) o ninguna por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de toma
    normal tinyint (0 falso - 1 verdadero)
    ddvi entero 2 en milímetros
    dsvi (verificar tipo de dato)
    fac decimal
    siv entero milímetros
    pp entero milímetros
    ai entero milímetros
    ao entero milímetros
    fey decimal
    ddvd (verificar tipo de dato)
    ad entero milímetros
    movilidad varchar(200) texto libre
    fsvi varchar(200) texto libre
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ecocardiograma;

-- la recreamos
CREATE TABLE ecocardiograma (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    ddvi int(2) UNSIGNED DEFAULT 0,
    dsvi int(2) UNSIGNED DEFAULT 0,
    fac decimal(3,2) UNSIGNED DEFAULT 0.00,
    siv int(2) UNSIGNED DEFAULT 0,
    pp int(2) UNSIGNED DEFAULT 0,
    ai int(2) UNSIGNED DEFAULT 0,
    ao int(2) UNSIGNED DEFAULT 0,
    fey decimal(3,2) UNSIGNED DEFAULT 0.00,
    ddvd int(2) UNSIGNED DEFAULT 0,
    ad int(2) UNSIGNED DEFAULT 0,
    movilidad varchar(200) DEFAULT NULL,
    fsvi varchar(200) DEFAULT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos de los ecocardiogramas';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_ecocardiograma;

-- creamos la tabla de auditoría
CREATE TABLE auditoria_ecocardiograma (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    ddvi int(2) UNSIGNED DEFAULT 0,
    dsvi int(2) UNSIGNED DEFAULT 0,
    fac decimal(3,2) UNSIGNED DEFAULT 0.00,
    siv int(2) UNSIGNED DEFAULT 0,
    pp int(2) UNSIGNED DEFAULT 0,
    ai int(2) UNSIGNED DEFAULT 0,
    ao int(2) UNSIGNED DEFAULT 0,
    fey decimal(3,2) UNSIGNED DEFAULT 0.00,
    ddvd int(2) UNSIGNED DEFAULT 0,
    ad int(2) UNSIGNED DEFAULT 0,
    movilidad varchar(200) DEFAULT NULL,
    fsvi varchar(200) DEFAULT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta DATE NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)

)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de los ecocardiogramas';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_ecocardiograma;

-- lo recreamos
CREATE TRIGGER edicion_ecocardiograma
AFTER UPDATE ON ecocardiograma
FOR EACH ROW
INSERT INTO auditoria_ecocardiograma
       (id,
        paciente,
        visita,
        fecha,
        normal,
        ddvi,
        dsvi,
        fac,
        siv,
        pp,
        ai,
        ao,
        fey,
        ddvd,
        ad,
        movilidad,
        fsvi,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.ddvi,
        OLD.dsvi,
        OLD.fac,
        OLD.siv,
        OLD.pp,
        OLD.ai,
        OLD.ao,
        OLD.fey,
        OLD.ddvd,
        OLD.ad,
        OLD.movilidad,
        OLD.fsvi,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ecocardiograma;

-- lo recreamos
CREATE TRIGGER eliminacion_ecocardiograma
AFTER DELETE ON ecocardiograma
FOR EACH ROW
INSERT INTO auditoria_ecocardiograma
       (id,
        paciente,
        visita,
        fecha,
        normal,
        ddvi,
        dsvi,
        fac,
        siv,
        pp,
        ai,
        ao,
        fey,
        ddvd,
        ad,
        movilidad,
        fsvi,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.normal,
        OLD.ddvi,
        OLD.dsvi,
        OLD.fac,
        OLD.siv,
        OLD.pp,
        OLD.ai,
        OLD.ao,
        OLD.fey,
        OLD.ddvd,
        OLD.ad,
        OLD.movilidad,
        OLD.fsvi,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                                 Holter                                  */
/*                                                                         */
/***************************************************************************/

/*

    Esta es la tabla con los datos del holter, puede traerlo el paciente
    se le puede tomar en el instituto o puede no tener

    id clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de adminstración
    media entero frecuencia cardìaca media
    minima entero frecuencia cardíaca mínima
    máxima entero frecuencia cardíaca máxima
    latidos entero número total de latidos
    ev tinyint si tuvo episodios ventriculares
    nev entero número de episodios
    tasaev decimal porcentaje de episodios
    esv tinyint si tuvo episodios extraventriculares
    nesv entero número de episodios extraventriculares
    tasa esv decimal, porcentaje de episodios
    normal tinyint (0 falso - 1 verdadero)
    bradicardia tinyint (0 falso - 1 verdadero)
    rtacronotropica tinyint (0 falso - 1 verdadero)
    arritmiasevera tinyint (0 falso - 1 verdadero)
    arritmiasimple tinyint (0 falso - 1 verdadero)
    arritmiasupra tinyint (0 falso - 1 verdadero)
    bderama tinyint (0 falso - 1 verdadero)
    bav2g tinyint (0 falso - 1 verdadero)
    disociacion tinyint (0 falso - 1 verdadero)
    comentarios texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS holter;

-- la recreamos
CREATE TABLE holter (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    media int(3) UNSIGNED NOT NULL,
    minima int(3) UNSIGNED NOT NULL,
    maxima int(3) UNSIGNED NOT NULL,
    latidos int(5) UNSIGNED NOT NULL,
    ev tinyint(1) UNSIGNED DEFAULT 0,
    nev int(3) UNSIGNED DEFAULT 0,
    tasaev decimal(4,2) UNSIGNED DEFAULT 0.00,
    esv tinyint(1) UNSIGNED DEFAULT 0,
    nesv int(3) UNSIGNED DEFAULT 0,
    tasaesv decimal(4,2) UNSIGNED DEFAULT 0.00,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(0) UNSIGNED DEFAULT 0,
    rtacronotropica tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasevera tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasimple tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasupra tinyint(0) UNSIGNED DEFAULT 0,
    bderama tinyint(0) UNSIGNED DEFAULT 0,
    bav2g tinyint(0) UNSIGNED DEFAULT 0,
    disociacion tinyint(0) UNSIGNED DEFAULT 0,
    comentarios mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del holter';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_holter;

-- creamos la tabla de auditoría
CREATE TABLE auditoria_holter (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    media int(3) UNSIGNED NOT NULL,
    minima int(3) UNSIGNED NOT NULL,
    maxima int(3) UNSIGNED NOT NULL,
    latidos int(5) UNSIGNED NOT NULL,
    ev tinyint(1) UNSIGNED DEFAULT 0,
    nev int(3) UNSIGNED DEFAULT 0,
    tasaev decimal(4,2) UNSIGNED DEFAULT 0.00,
    esv tinyint(1) UNSIGNED DEFAULT 0,
    nesv int(3) UNSIGNED DEFAULT 0,
    tasaesv decimal(4,2) UNSIGNED DEFAULT 0.00,
    normal tinyint(1) UNSIGNED DEFAULT 0,
    bradicardia tinyint(0) UNSIGNED DEFAULT 0,
    rtacronotropica tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasevera tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasimple tinyint(0) UNSIGNED DEFAULT 0,
    arritmiasupra tinyint(0) UNSIGNED DEFAULT 0,
    bderama tinyint(0) UNSIGNED DEFAULT 0,
    bav2g tinyint(0) UNSIGNED DEFAULT 0,
    disociacion tinyint(0) UNSIGNED DEFAULT 0,
    comentarios mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría del holter';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_holter;

-- lo recreamos
CREATE TRIGGER edicion_holter
AFTER UPDATE ON holter
FOR EACH ROW
INSERT INTO auditoria_holter
       (id,
        paciente,
        visita,
        fecha,
        media,
        minima,
        maxima,
        latidos,
        ev,
        nev,
        tasaev,
        esv,
        nesv,
        tasanev,
        normal,
        bradicardia,
        rtacronotropica,
        arritmiasevera,
        arritmiasimple,
        arritmiasupra,
        bderama,
        bav2g,
        disociacion,
        comentarios,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.media,
        OLD.minima,
        OLD.maxima,
        OLD.latidos,
        OLD.ev,
        OLD.nev,
        OLD.tasaev,
        OLD.esv,
        OLD.nesv,
        OLD.tasaesv,
        OLD.normal,
        OLD.bradicardia,
        OLD.rtacronotropica,
        OLD.arritmiasevera,
        OLD.arritmiasimple,
        OLD.arritmiasupra,
        OLD.bderama,
        OLD.bav2g,
        OLD.disociacion,
        OLD.comentarios,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_holter;

-- lo recreamos
CREATE TRIGGER eliminacion_holter
AFTER DELETE ON holter
FOR EACH ROW
INSERT INTO auditoria_holter
       (id,
        paciente,
        visita,
        fecha,
        media,
        minima,
        maxima,
        latidos,
        ev,
        nev,
        tasaev,
        esv,
        nesv,
        tasanev,
        normal,
        bradicardia,
        rtacronotropica,
        arritmiasevera,
        arritmiasimple,
        arritmiasupra,
        bderama,
        bav2g,
        disociacion,
        comentarios,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.media,
        OLD.minima,
        OLD.maxima,
        OLD.latidos,
        OLD.ev,
        OLD.nev,
        OLD.tasaev,
        OLD.esv,
        OLD.nesv,
        OLD.tasaesv,
        OLD.normal,
        OLD.bradicardia,
        OLD.rtacronotropica,
        OLD.arritmiasevera,
        OLD.arritmiasimple,
        OLD.arritmiasupra,
        OLD.bderama,
        OLD.bav2g,
        OLD.disociacion,
        OLD.comentarios,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                            Ergometría                                   */
/*                                                                         */
/***************************************************************************/

/*

    Estos son los datos de la ergometría, los trae el paciente y puede
    haber mas de un registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    fecha date fecha de administración
    fcbasal entero frecuencia basal
    fcmax entero frecuencia màxima
    tabasal varchar con màscara tensión basal
    tamax varchar con máscara tensión máxima
    kpm entero km recorridos
    itt varchar(200) por ahora texto libre
    observaciones texto comentarios del usuario
    usuario entero clave del usuario
    fecha_alta date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS ergometria;

-- la recreamos
CREATE TABLE ergometria(
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fcbasal int(3) UNSIGNED DEFAULT 0,
    fcmax int(3) UNSIGNED DEFAULT 0,
    tabasal varchar(20) NOT NULL,
    tamax varchar(20) NOT NULL,
    kpm int(4) UNSIGNED NOT NULL,
    itt varchar(200) DEFAULT NULL,
    observaciones mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Datos del Ergometría';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_ergometria;

-- la recreamos
CREATE TABLE auditoria_ergometria(
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fcbasal int(3) UNSIGNED DEFAULT 0,
    fcmax int(3) UNSIGNED DEFAULT 0,
    tabasal varchar(20) NOT NULL,
    tamax varchar(20) NOT NULL,
    kpm int(4) UNSIGNED NOT NULL,
    itt varchar(200) DEFAULT NULL,
    observaciones mediumtext,
    usuario int(6) UNSIGNED NOT NULL,
    fecha_alta date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la Ergometría';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_ergometria;

-- lo recreamos
CREATE TRIGGER edicion_ergometria
AFTER UPDATE ON ergometria
FOR EACH ROW
INSERT INTO auditoria_ergometria
       (id,
        paciente,
        visita,
        fecha,
        fcbasal,
        fcmax,
        tabasal,
        tamax,
        kpm,
        itt,
        observaciones,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.fcbasal,
        OLD.fcmax,
        OLD.tabasal,
        OLD.tamax,
        OLD.kpm,
        OLD.itt,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha_alta,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_ergometria;

-- lo recreamos
CREATE TRIGGER eliminacion_ergometria
AFTER DELETE ON ergometria
FOR EACH ROW
INSERT INTO auditoria_ergometria
       (id,
        paciente,
        visita,
        fecha,
        fcbasal,
        fcmax,
        tabasal,
        tamax,
        kpm,
        itt,
        observaciones,
        usuario,
        fecha_alta,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.fecha,
        OLD.fcbasal,
        OLD.fcmax,
        OLD.tabasal,
        OLD.tamax,
        OLD.kpm,
        OLD.itt,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha_alta,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                           Visitas del Paciente                          */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla funciona como pivot de las visitas del paciente
    id entero clave del registro
    fecha date fecha de la visita
    usuario usuario que ingresó el registro
    alta timestamp fecha de alta del registro

    No tenemos registros de auditoría de esta tabla

*/

-- la eliminamos si existe
DROP TABLE IF EXISTS visitas;

-- la recreamos
CREATE TABLE visitas (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    fecha date NOT NULL,
    usuario int(6) UNSIGNED NOT NULL,
    alta timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Pivot de visitas';


/***************************************************************************/
/*                                                                         */
/*                            Clasificación                                */
/*                                                                         */
/***************************************************************************/

/*

    Esta tabla contiene los datos de la clasificación de la enfermedad de
    Chagas según Kuscknir debería haber una entrada por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    visita clave de la visita
    estadio enum estadio en que se encuentra
    observaciones comentarios del usuario
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS clasificacion;

-- la recreamos
CREATE TABLE clasificacion (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Clasificacion según Kuscknir';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_clasificacion;

-- la recreamos
CREATE TABLE auditoria_clasificacion (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    visita int(6) UNSIGNED NOT NULL,
    estadio enum("Estadio 0", "Estadio I", "Estadio II", "Estadio III") DEFAULT NULL,
    observaciones mediumtext,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY visita(visita),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de la clasificación';

-- eliminamos el trigger de edición
DROP TRIGGER IF EXISTS edicion_clasificacion;

-- lo recreamos
CREATE TRIGGER edicion_clasificacion
AFTER UPDATE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        visita,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_clasificacion;

-- lo recreamos
CREATE TRIGGER eliminacion_clasificacion
AFTER DELETE ON clasificacion
FOR EACH ROW
INSERT INTO auditoria_clasificacion
       (id,
        paciente,
        visita,
        estadio,
        observaciones,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.visita,
        OLD.estadio,
        OLD.observaciones,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");


/***************************************************************************/
/*                                                                         */
/*                              Familiares                                 */
/*                                                                         */
/***************************************************************************/

/*

    Tabla con los antecedentes familiares de cada paciente, esta tendría
    que haber un solo registro por cada paciente

    id entero clave del registro
    paciente entero clave del paciente
    subita tinyint si hay muerte súbita (0 falso - 1 verdadero)
    cardiopatia tinyint si hay cardiopatía (0 falso - 1 verdadero)
    disfagia tinyint si ha disfagia, constipación o evidencia de megas
    nosabe tinyint (0 falso - 1 verdadero)
    notiene tinyint (0 falso - 1 verdadero)
    otra texto descripción de la enfermedad
    usuario entero clave del usuario
    fecha date fecha de alta del registro

*/

-- eliminamos la tabla si existe
DROP TABLE IF EXISTS familiares;

-- la recreamos
CREATE TABLE familiares (
    id int(6) UNSIGNED NOT NULL AUTO_INCREMENT,
    paciente int(8) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Antecedentes familiares';

-- eliminamos la tabla de auditoría
DROP TABLE IF EXISTS auditoria_familiares;

-- la recreamos
CREATE TABLE auditoria_familiares (
    id int(6) UNSIGNED NOT NULL,
    paciente int(8) UNSIGNED NOT NULL,
    subita tinyint(1) UNSIGNED DEFAULT 0,
    cardiopatia tinyint(1) UNSIGNED DEFAULT 0,
    disfagia tinyint(1) UNSIGNED DEFAULT 0,
    nosabe tinyint(1) UNSIGNED DEFAULT 0,
    notiene tinyint(1) UNSIGNED DEFAULT 0,
    otra text DEFAULT NULL,
    usuario int(4) UNSIGNED NOT NULL,
    fecha date NOT NULL,
    fecha_evento timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    evento enum("Edicion", "Eliminacion"),
    KEY id(id),
    KEY paciente(paciente),
    KEY usuario(usuario)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_spanish_ci
COMMENT='Auditoría de antecedentes familiares';

-- eliminamos el trigger
DROP TRIGGER IF EXISTS edicion_familiares;

-- lo recreamos
CREATE TRIGGER edicion_familiares
AFTER UPDATE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Edicion");

-- eliminamos el trigger de eliminación
DROP TRIGGER IF EXISTS eliminacion_familiares;

-- lo recreamos
CREATE TRIGGER eliminacion_familiares
AFTER DELETE ON familiares
FOR EACH ROW
INSERT INTO auditoria_familiares
       (id,
        paciente,
        subita,
        cardiopatia,
        disfagia,
        nosabe,
        notiene,
        otra,
        usuario,
        fecha,
        evento)
       VALUES
       (OLD.id,
        OLD.paciente,
        OLD.subita,
        OLD.cardiopatia,
        OLD.disfagia,
        OLD.nosabe,
        OLD.notiene,
        OLD.otra,
        OLD.usuario,
        OLD.fecha,
        "Eliminacion");
